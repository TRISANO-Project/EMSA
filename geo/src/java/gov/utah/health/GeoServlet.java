package gov.utah.health;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author UDOH
 */
public class GeoServlet extends HttpServlet {

    private static final Log log = LogFactory.getLog(GeoServlet.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Geo Location Lookup Service</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Geo Location Lookup Service " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }


    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletOutputStream out = response.getOutputStream();
        String streetName = request.getParameter("street_name");
        String zipCode = request.getParameter("zip");
        String state = request.getParameter("state");
        String city = request.getParameter("city");

        if (streetName == null) {
            streetName = "";
        }
        if (zipCode == null) {
            zipCode = "";
        }
        if (state == null) {
            state = "TX";
        }
        if (city == null) {
            city = "";
        }
        this.lookup(out, streetName, zipCode, city, state);
    }

    /**
     * Wrap and http client to ignore SSL security cert errors
     * 
     */
    private static HttpClient wrapClient(HttpClient base) {
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            X509TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            ctx.init(null, new TrustManager[]{tm}, null);
            SSLSocketFactory ssf = new SSLSocketFactory(ctx);
            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            ClientConnectionManager ccm = base.getConnectionManager();
            SchemeRegistry sr = ccm.getSchemeRegistry();
            sr.register(new Scheme("https", ssf, 443));
            return new DefaultHttpClient(ccm, base.getParams());
        } catch (Exception ex) {
        }
        return null;
    }
    
    /**
     * Do a geo lookup using SmartyStreets
     * 
     * @param out
     * @param streetName
     * @param zipCode
     * @param city
     * @param state
     * @param candidates 
     */
    public void lookup(ServletOutputStream out, String streetName, String zipCode, String city, String state) {

        try {
            log.error("Using TCPH Geo");
            
            String strJson = "";
            String strQuery =  "https://gisitin.tarrantcounty.com/WSGIS/api/Parcel911?Address=" +
                URLEncoder.encode(streetName + " " + city + " " + state + " " + zipCode, "UTF-8");
            
            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 10000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            
            // use an http client which will ignore untrusted SSL errors for the tcph geo service
            httpclient = wrapClient(httpclient);
            HttpGet httpGet = new HttpGet(strQuery);
            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            StatusLine sl = response.getStatusLine();

            Integer statusCode = sl.getStatusCode();
            if (statusCode.equals(HttpStatus.SC_OK)
                    || statusCode.equals(HttpStatus.SC_ACCEPTED)) {
                if (entity != null) {
                    InputStream is = entity.getContent();
                    strJson = IOUtils.toString(is, "UTF-8");
                }
            } else {
                strJson = "Remote Status: " + statusCode + ", " + sl.getReasonPhrase();
            }

            // extract the retrieved information from the TCPH query
            // and create SmartyStreets like json
            String smartyJson = smartyFromTcph(strJson, streetName, zipCode, city, state);
            out.write(smartyJson.getBytes());
           
        } catch (Exception e) {
            log.error("Geo lookup Error: " + e.getMessage());
            try {
                out.write(e.getMessage().getBytes());
            } catch (IOException ioe) {
                log.error(ioe);
            }
        }
    }
    
    /**
     *   Create a the subset of the SmartyStreets json that we use
     *     from the json returned by TCPH web geo service
     * @param strJsonTcph
     * @param candidates
     * @return 
     */
    private String smartyFromTcph(String strJsonTcph, String streetName, String zipCode, String city, String state) {

        String smartyJson = "[]";
        int maxResults;
        try {
            JSONArray arrayS = new JSONArray();
            JSONObject objRootT = new JSONObject(strJsonTcph);
            JSONArray arrayT = objRootT.getJSONArray("results");
            maxResults = arrayT.length();
            
            // find the first result with a match score of 100
            for (int i = 0; i < maxResults; i++) {
                JSONObject objT = arrayT.getJSONObject(i);
                String score = objT.getString("candidateScore");
                
                // if this is a perfect match
                if(score.equals("100.0")) {
                    JSONObject objSRoot = new JSONObject();
                    JSONObject objSComp = new JSONObject();
                    String[] keysToBlank = { "street_predirection", "street_postdirection", "street_suffix", 
                       "secondary_number", "secondary_designator", "street_number", "secondary_number"};
                    for(String key: keysToBlank) {
                        objSComp.put(key, "");
                    }
                    objSComp.put("street_name", streetName);
                    objSComp.put("city_name", city);
                    objSComp.put("state_abbreviation", state);
                    objSComp.put("zipcode", zipCode);
                    objSRoot.put("components", objSComp);
                    
                    String lat = objT.getString("lat");
                    String lon = objT.getString("lon");
                    JSONObject objSMeta = new JSONObject();
                    objSMeta.put("latitude", lat);
                    objSMeta.put("longitude", lon);
                    objSRoot.put("metadata", objSMeta);

                    arrayS.put(objSRoot);
                    smartyJson = arrayS.toString();
                    break;
                }
            }
        } catch (Exception e) {
            smartyJson = "Error translating TCPH Json to SmartyStreets Json: " + e.getMessage();
            log.error(smartyJson);
        }
        return smartyJson;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Trisnao Data Export Utility, Servlet Implementation";
    }
}
