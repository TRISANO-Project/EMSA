package gov.utah.health;

import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.service.dashboard.DashboardService;
import gov.utah.health.service.lims.LimsServiceImpl;
import gov.utah.health.service.master.MasterService;
import gov.utah.health.service.trisano.TrisanoService;
import gov.utah.health.util.HealthMarshal;
import java.net.URLEncoder;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Web service source file for this project. Handles marshaling and unmarshaling
 * of the HealthMessage object.
 *
 * @author UDOH
 */
@WebService()
public class SqlAgentService {

    private static final Log log = LogFactory.getLog(SqlAgentService.class);
    ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    /**
     * Web service operation to return many people similar to search criteria
     */
    @WebMethod(operationName = "searchPerson")
    public String searchPerson(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.searchPerson(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation to find a person given exact match first last names
     * birthday.
     */
    @WebMethod(operationName = "findPerson")
    public String findPerson(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.findPerson(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation to find an event given event id.
     */
    @WebMethod(operationName = "findEvent")
    public String findEvent(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.findEvent(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "addCmr")
    public String addCmr(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.addCmr(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "updateCmr")
    public String updateCmr(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.updateCmr(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "deepCopyCmr")
    public String deepCopyCmr(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.deepCopyCmr(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "findId")
    public String findId(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.findId(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation to find questions for a disease.
     */
    @WebMethod(operationName = "findQuestions")
    public String findQuestions(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.findQuestions(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation to add and update answers for an event. <p> Each
     * answer submitted is required to have a question_id and event_id set.<br
     * /> The form associated to the question referenced by each answer must
     * also exist.<br /> If the formReference does not exist linking the event
     * to the question - form, A new formReference will be created before the
     * answers are saved.<br /> Answers without an id set will be added and
     * answers with an id will be uodated. </p>
     */
    @WebMethod(operationName = "addAnswers")
    public String addAnswers(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.addAnswers(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation to add attachments.
     */
    @WebMethod(operationName = "addAttachments")
    public String addAttachments(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.addAttachments(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation to find answers for a given event id.
     */
    @WebMethod(operationName = "findAnswers")
    public String findAnswers(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.findAnswers(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getExample")
    public String getExample(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getHealthDataExample(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getTableData")
    public String getTableData(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getTableData(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "addNote")
    public String addNote(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.addNote(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "exportTreatmentNotes")
    public String exportTreatmentNotes(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.exportTreatmentNotes(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
//    @WebMethod(operationName = "exportForms")
//    public String exportForms(@WebParam(name = "healthMessage") String healthMessage) {
//        HealthMessage hm = getHealthMessage(healthMessage);
//        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
//        hs.exportForms(hm);
//        return getHealthMessageXml(hm);
//    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "findForms")
    public String findForms(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.findForms(hm);
        return getHealthMessageXml(hm);
    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "addTask")
    public String addTask(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.addTask(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "attachPdf")
    public String attachPdf(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.attachFile(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getCaseStatusReport")
    public String getCaseStatusReport(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getCaseStatusReport(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getDiseaseReport")
    public String getDiseaseReport(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getDiseaseReport(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUdohClinicalResults")
    public String getUdohClinicalResults(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        LimsServiceImpl hs = (LimsServiceImpl) appContext.getBean(hm.getSystem());//LIMS
        hs.getUdohClinicalResults(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUserRoles")
    public String getUserRoles(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getUserRoles(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getUsers")
    public String getUsers(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getUsers(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getRoles")
    public String getRoles(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getRoles(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "testAccess")
    public String testAccess(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.testAccess(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getCareTimeReport")
    public String getReport(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getCareTimeReport(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getCdcExport")
    public String getCdcExport(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        hs.getCdcExport(hm);
        return getHealthMessageXml(hm);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getMaster")
    public String getMaster(@WebParam(name = "masterId") String masterId) {
        MasterService ms = (MasterService) appContext.getBean("master");
        return ms.getMaster(masterId);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getGeocode")
    public String getGeocode(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        TrisanoService hs = (TrisanoService) appContext.getBean(hm.getSystem());
        return hs.getGeocode(hm);
    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "getDashboardReport")
    public String getDashboardReport(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        DashboardService ds = (DashboardService) appContext.getBean("DASHBOARD");
        ds.getReport(hm);
        String encodedMsg = null;
        try {
            encodedMsg = URLEncoder.encode(getHealthMessageXml(hm), "UTF-8");
        } catch (Exception e) {
            log.error(e);
        }
        return encodedMsg;
    }

    private HealthMessage getHealthMessage(String hmXml) {

        HealthMarshal marshal = (HealthMarshal) appContext.getBean("msgMarshal");
        HealthMessage h;
        try {
            h = marshal.getHealthMessageObject(hmXml);
            if (h.getSystem() == null) {
                h.addStatusMessage(new StatusMessage("getHealthMessage",Status.FAILURE_MISSING_DATA.toString(),"health_message.system is missing"));
            }

        } catch (Exception e) {
            h = new HealthMessage();
            h.addStatusMessage(new StatusMessage("getHealthMessage",Status.FAILURE_EXCEPTION.toString(),e.getMessage()));
            log.error("unable to unmarshal healthMessage xml=" + hmXml, e);
        }

        return h;
    }

    private String getHealthMessageXml(HealthMessage hm) {

        HealthMarshal marshal = (HealthMarshal) appContext.getBean("msgMarshal");
        String h = null;
        try {
            h = marshal.getHealthMessageXml(hm);
        } catch (Exception e) {

            hm = new HealthMessage();
            hm.addStatusMessage(new StatusMessage("getHealthMessageXml",Status.FAILURE_EXCEPTION.toString(),e.getMessage()));
            try {
                h = marshal.getHealthMessageXml(hm);
            } catch (Exception e2) {
                log.error("Unable to marshal healthMessage", e2);
            }
            log.error("Unable to marshal healthMessage" + hm, e);

        }

        return h;
    }
}
