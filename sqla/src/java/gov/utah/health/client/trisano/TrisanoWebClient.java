package gov.utah.health.client.trisano;

import gov.utah.health.model.HealthMessage;

/**
 * Interface for general web-based data access.
 * @author UDOH
 */
public interface TrisanoWebClient {

    public void addCmr(HealthMessage hm);
    public void updateCmr(HealthMessage hm);
    public void deepCopyCmr(HealthMessage hm);
    public void attachFile(HealthMessage hm);
    public void addNote(HealthMessage hm);
    public void addTask(HealthMessage hm);
    public void addForm(HealthMessage hm);
    public void testAccess(HealthMessage hm);

}

