package gov.utah.health.client.trisano;

import gov.utah.health.data.trisano.TrisanoDaoImpl;
import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.model.master.Esp;
import gov.utah.health.model.master.Treatments;
import gov.utah.health.model.trisano.Addresses;
import gov.utah.health.model.trisano.CliniciansAttributes;
import gov.utah.health.model.trisano.ContactChildEventsAttributes;
import gov.utah.health.model.trisano.DiagnosticFacilitiesAttributes;
import gov.utah.health.model.trisano.DiseaseEvents;
import gov.utah.health.model.trisano.Events;
import gov.utah.health.model.trisano.FormReferences;
import gov.utah.health.model.trisano.HospitalizationFacilitiesAttributes;
import gov.utah.health.model.trisano.HospitalsParticipations;
import gov.utah.health.model.trisano.InterestedPartyAttributes;
import gov.utah.health.model.trisano.JurisdictionAttributes;
import gov.utah.health.model.trisano.LabResults;
import gov.utah.health.model.trisano.LabsAttributes;
import gov.utah.health.model.trisano.Notes;
import gov.utah.health.model.trisano.Participations;
import gov.utah.health.model.trisano.ParticipationsContacts;
import gov.utah.health.model.trisano.ParticipationsRiskFactors;
import gov.utah.health.model.trisano.ParticipationsTreatments;
import gov.utah.health.model.trisano.People;
import gov.utah.health.model.trisano.PeopleRaces;
import gov.utah.health.model.trisano.PlaceAttributes;
import gov.utah.health.model.trisano.Places;
import gov.utah.health.model.trisano.ReporterAttributes;
import gov.utah.health.model.trisano.ReportingAgencyAttributes;
import gov.utah.health.model.trisano.Tasks;
import gov.utah.health.model.trisano.Telephones;
import gov.utah.health.model.trisano.TrisanoHealth;
import gov.utah.health.model.trisano.Type;
import gov.utah.health.util.AppProperties;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * This class provides the web client for interfacing the trisano web
 * application. All updates and insertions of data to trisano are handled
 * through this class.
 *
 * @author UDOH
 */
@Repository
public class TrisanoWebClientImpl implements TrisanoWebClient {

    private static final Log log = LogFactory.getLog(TrisanoWebClientImpl.class);
    private static AppProperties props = AppProperties.getInstance();
    private static int MODE_ADD = 0;
    private static int MODE_UPDATE = 1;
    private SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
    // TRISANO_UID";
    private String scheme = "http";
    private String host = props.getProperty("trisano_host");
    private int port = props.getIntProperty("trisano_port");
//    private int port = 8080;
    private String context = props.getProperty("trisano_context");
//    private String context = "/trisano";
    private String uid_header_name = props.getProperty("trisano_uid_header_name");
    private boolean useApiKey = props.getBooleanProperty("use_api_key");
    private boolean dontUpdateExistingReporter = props.getBooleanProperty("dont_update_existing_reporter");
    private String apiKey = props.getProperty("api_key");
    private String emsaEnvironment = props.getProperty("emsa_environment");
    private String defaultJurisdiction = props.getProperty("default_jurisdiction");
    private String referringSite = "http://" + this.host + ":" + this.port + this.context + "/cmrs";
    // keys for response map
    private Integer STATUS_KEY = 1;
    private Integer RESPONSE_KEY = 2;

    /**
     * *
     * Makes a single get request to the trisano app root context.
     *
     * @param hm HealthMessage object. The healthMessage object modified to
     * included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Autowired
    @Override
    public void testAccess(HealthMessage hm) {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        URI uri = null;
        try {

            uri = new URI(this.scheme, null, this.host, this.port, this.context, null, null);
            HttpGet httpGet = new HttpGet(uri);

            Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
            httpGet.setHeader(header);
// TODO Jay, this method still needs to be modified to use the api_key

            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                Float flen = new Float(entity.getContentLength());
                InputStream is = entity.getContent();
                byte[] ba = new byte[Math.round(flen)];
                is.read(ba);
                String out = new String(ba);

                if (response.getStatusLine().toString().indexOf("200 OK") > 0) {
                    hm.addStatusMessage(new StatusMessage("testAccess", Status.SUCCESS.toString(), "TEST ACCESS [" + uri + "] RESPONSE STATUS [" + response.getStatusLine().toString() + "]"));
                } else {
                    hm.addStatusMessage(new StatusMessage("testAccess", Status.FAILURE_EXCEPTION.toString(), "TEST ACCESS [" + uri + "] RESPONSE STATUS [" + response.getStatusLine().toString() + "]"));
                }
                log.info("\nTEST ACCESS [" + uri + "]\nRESPONSE STATUS [" + response.getStatusLine() + "]\n CONTENT...\n" + out);

            }
            EntityUtils.consume(entity);

        } catch (URISyntaxException | IOException | IllegalStateException e) {

            hm.addStatusMessage(new StatusMessage("testAccess", Status.FAILURE_EXCEPTION.toString(), "TEST ACCESS [" + uri + "]\n " + e.getMessage()));
            log.error(e);

        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }

    }

    /**
     * *
     * Adds a new cmr using posts to the trisano web application. Will add a new
     * person with event if the person does not exist. Will add event if person
     * is found. Automatic routing of cmr... If a jurisdiction is provided(id)
     * the cmr will be routed to that jurisdiction.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set with all cmr data to be added. The HealthMessage
     * parameter is modified to include an event id in events. If errors or
     * exceptions take place information is included in the healthMessage.status
     * and healthMessage.statusMessage attributes.
     */
    @Autowired
    @Override
    public void addCmr(HealthMessage hm) {
        try {
            this.add(hm);
        } catch (Exception e) {
            log.error(e);
            hm.addStatusMessage(new StatusMessage("addCmr", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }
    }

    /**
     * *
     * Updates cmr data for an existing event using posts to the trisano web
     * application. Updates an existing cmr or adds a new cmr to an existing
     * person. Will update data that is supplied with ids. Will add data for
     * objects that do not have ids assuming higher level objects have ids.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set with all cmr data to be added or updated.
     * <p>
     * <strong>Updating an existing CMR</strong> requires providing ids at the
     * level that the update is to be applied. Providing a lab_result with no id
     * will cause the lab to be added. Providing a lab_result with an id will
     * cause the properties of that lab to be updated. </p>
     * <p>
     * <strong>Adding a CMR to an existing person</strong> requires providing an
     * InterestedPartyAttributes object with a people containing a valid
     * entity_id. Also, the event can not contain an event id as this function
     * creates a new event. </p>
     *
     */
    @Autowired
    @Override
    public void updateCmr(HealthMessage hm) {
        try {
            this.update(hm);
        } catch (Exception e) {
            log.error(e);
            hm.addStatusMessage(new StatusMessage("updateCmr", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }
    }

    @Autowired
    @Override
    public void deepCopyCmr(HealthMessage hm) {
        try {

            DefaultHttpClient httpclient = new DefaultHttpClient();
            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            for (TrisanoHealth th : thList) {

                if (!th.hasEventId()) {

                    hm.addStatusMessage(new StatusMessage("deepCopyCmr", Status.FAILURE_MISSING_DATA.toString(), "events.id"));
                    throw new Exception("events.id is required to deep copy an event.");
                }

                List<Events> events = th.getEvents();

                Events event = events.get(0);
                Integer eventId = event.getId();
                if (eventId != null && eventId > 0) {
                    try {

                        URI uri = new URI(this.scheme, null, this.host, this.port,
                                this.context + "/cmrs", "return=true", null);

                        HttpPost httpPost = new HttpPost(uri);
                        Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                        httpPost.setHeader(header);

                        // Set up parameters to add cmr
                        List<NameValuePair> qparams = new ArrayList<>();


                        qparams.add(new BasicNameValuePair("from_event", eventId.toString()));
                        qparams.add(new BasicNameValuePair("event_components[]", "clinical"));
                        qparams.add(new BasicNameValuePair("event_components[]", "lab"));
                        qparams.add(new BasicNameValuePair("event_components[]", "reporting"));
                        qparams.add(new BasicNameValuePair("event_components[]", "disease_specific"));
                        qparams.add(new BasicNameValuePair("event_components[]", "notes"));

                        HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                        HttpEntity entity = response.getEntity();

                        if (entity != null) {

                            Float flen = new Float(entity.getContentLength());
                            InputStream is = entity.getContent();
                            byte[] ba = new byte[Math.round(flen)];
                            is.read(ba);
                            String out = new String(ba);
                            int copyEventId;

                            if (out.trim().startsWith("<html><body>You are being <a href")) {

                                String eId = out.substring(out.indexOf("/cmrs/") + 6, out.indexOf("redirected") - 7);
                                copyEventId = Integer.parseInt(eId);
                                hm.addStatusMessage(new StatusMessage("deepCopyCmr", Status.SUCCESS.toString(), "eventId=" + eventId));
                                th.getEvents().get(0).setId(copyEventId);
                                log.info("CMR DEEP COPIED [eventId=" + eventId + ",copyEventId=" + copyEventId + "]");

                            } else {
                                hm.addStatusMessage(new StatusMessage("deepCopyCmr", Status.FAILURE_TRISANO_ERROR.toString(), "Failure copying CMR [eventId=" + eventId + "]...\n"
                                        + response.getStatusLine()));
                                log.error("Failure copying CMR [eventId=" + eventId + "]...\n"
                                        + response.getStatusLine() + "\n" + out);
                            }

                        }
                        EntityUtils.consume(entity);

                    } finally {
                        // When HttpClient instance is no longer needed,
                        // shut down the connection manager to ensure
                        // immediate deallocation of all system resources
                        httpclient.getConnectionManager().shutdown();
                    }



                } else {
                    hm.addStatusMessage(new StatusMessage("deepCopyCmr", Status.FAILURE_MISSING_DATA.toString(), "eventId missing"));
                }

            }

        } catch (Exception e) {
            log.error(e);
            hm.addStatusMessage(new StatusMessage("deepCopyCmr", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }
    }

    /**
     * *
     * Attaches a file to an event using posts to the trisano web application.
     *
     * @param hm HealthMessage object. The healthMessage object including a
     * single master health object or a list. The HealthMessage. parameter is
     * required to include an event id. If errors or exceptions take place
     * information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    @Autowired
    @Override
    public void attachFile(HealthMessage hm) {
        try {

            TrisanoAttachmentClient.attachFile(hm, host, scheme, port, uid_header_name, context);

        } catch (Exception e) {
            log.error(e);
            hm.addStatusMessage(new StatusMessage("attachFile", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }
    }

    /**
     * *
     * Adds a new task using posts to the trisano web application.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set with all task data to be added. The
     * HealthMessage.TrisanoHealth.Tasks parameter is required to include an
     * event id in tasks. If errors or exceptions take place information is
     * included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Autowired
    @Override
    public void addTask(HealthMessage hm) {
        try {
            this.createTask(hm);
        } catch (Exception e) {
            hm.addStatusMessage(new StatusMessage("addTask", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            log.error(e);
        }
    }

    /**
     * *
     * Adds a note to an event using posts to the trisano web application.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set with all note data to be added. The
     * HealthMessage.TrisanoHealth.Tasks parameter is required to include an
     * event id in note. If errors or exceptions take place information is
     * included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Autowired
    @Override
    public void addNote(HealthMessage hm) {

        List<TrisanoHealth> thList = hm.getTrisanoHealth();
        for (TrisanoHealth th : thList) {

            Notes note = th.getNotes();

            if (note != null) {

                DefaultHttpClient httpclient = new DefaultHttpClient();

                try {

                    String etfn = th.getEventTypeFormName();
                    if (etfn == null) {
                        hm.addStatusMessage(new StatusMessage("addNote", Status.FAILURE_MISSING_DATA.toString(), "EventTypeFormName missing"));
                        throw new Exception("events.type is missing");

                    }

                    Integer eventId = note.getEventId();

                    if (eventId == null || eventId.equals(0)) {
                        hm.addStatusMessage(new StatusMessage("addNote", Status.FAILURE_MISSING_DATA.toString(), "trisano_health.notes.event_id missing"));
                        throw new Exception("Unable to add note. event id missing in trisano_health.notes.event_id");
                    }

                    URI uri = new URI(this.scheme, null, this.host, this.port,
                            this.context + "/cmrs/" + eventId, null, null);

                    HttpPost httpPost = new HttpPost(uri);

                    Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                    httpPost.setHeader(header);

                    List<NameValuePair> qparams = new ArrayList<>();
                    qparams.add(new BasicNameValuePair("_method", "put"));

                    setNotes(qparams, th, hm, etfn);

                    HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        Float flen = new Float(entity.getContentLength());
                        InputStream is = entity.getContent();
                        byte[] ba = new byte[Math.round(flen)];
                        is.read(ba);
                        String out = new String(ba);

                        if (out.trim().startsWith("<html><body>You are being <a href")) {
                            log.info("NOTE ADDED [eventId=" + eventId + "]");
                            hm.addStatusMessage(new StatusMessage("addNote", Status.SUCCESS.toString(), "eventId=" + eventId));
                        } else {
                            hm.addStatusMessage(new StatusMessage("addNote", Status.FAILURE_TRISANO_ERROR.toString(), out));
                            log.error("Failure adding note [eventId=" + eventId + "]... \n"
                                    + uri + "\n" + response.getStatusLine() + "\n" + out);
                        }

                    }
                    EntityUtils.consume(entity);

                } catch (Exception e) {

                    log.error(e);

                } finally {
                    // When HttpClient instance is no longer needed,
                    // shut down the connection manager to ensure
                    // immediate deallocation of all system resources
                    httpclient.getConnectionManager().shutdown();
                }

            }

        }

    }

    /**
     * *
     * Adds a form to an event.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set with all task data to be added. The
     * HealthMessage. parameter is required to include an event id. If errors or
     * exceptions take place information is included in the healthMessage.status
     * and healthMessage.statusMessage attributes.
     */
    @Autowired
    @Override
    public void addForm(HealthMessage hm) {

        TrisanoHealth th = hm.getTrisanoHealth().get(0);

        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<FormReferences> frList = th.getFormReferences();

        if (frList != null && !frList.isEmpty()
                && frList.get(0) != null && frList.get(0).getEventId() != null
                && frList.get(0).getEventId() > 0) {

            Integer eventId = frList.get(0).getEventId();
            Integer formId;

            try {

                ///trisano/events/2/forms
                URI uri = new URI(this.scheme, null, this.host, this.port,
                        this.context + "/events/" + eventId, "forms", null);

                HttpPost httpPost = new HttpPost(uri);

                Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                httpPost.setHeader(header);

                List<NameValuePair> qparams = new ArrayList<>();

                for (FormReferences fr : frList) {
                    formId = fr.getFormId();

                    if (formId != null && formId > 0) {

                        // name="forms_to_add[]"
                        qparams.add(new BasicNameValuePair("forms_to_add[]", formId.toString()));

                    } else {
                        hm.addStatusMessage(new StatusMessage("addNote", Status.FAILURE_MISSING_DATA.toString(), "FormReferences formId missing"));
                        throw new Exception("FormReferences formId is required to add forms to event.");
                    }

                }
                HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                HttpEntity entity = response.getEntity();

                if (entity != null) {
                    Float flen = new Float(entity.getContentLength());
                    InputStream is = entity.getContent();
                    byte[] ba = new byte[Math.round(flen)];
                    is.read(ba);
                    String out = new String(ba);
                    if (out.trim().startsWith("<html><body>You are being <a href")) {
                        hm.addStatusMessage(new StatusMessage("addForm", Status.SUCCESS.toString(), "FORM ADDED TO EVENT [eventId=" + eventId + "]"));
                        th.setEventId(eventId);

                        log.info("FORM ADDED TO EVENT [eventId=" + eventId + "]");

                    } else {
                        hm.addStatusMessage(new StatusMessage("addForm", Status.FAILURE_TRISANO_ERROR.toString(), "Failure adding form to event. [eventId=" + eventId + "] \n"
                                + uri + "\n" + response.getStatusLine() + "\n" + out));

                        log.error("Failure adding form to event. [eventId=" + eventId + "] \n"
                                + uri + "\n" + response.getStatusLine() + "\n" + out);
                    }

                }

                EntityUtils.consume(entity);

            } catch (Exception e) {

                log.error(e);

            } finally {
                // When HttpClient instance is no longer needed,
                // shut down the connection manager to ensure
                // immediate deallocation of all system resources
                httpclient.getConnectionManager().shutdown();
            }

        } else {
            hm.addStatusMessage(new StatusMessage("addForm", Status.FAILURE_MISSING_DATA.toString(), "FormReferences with eventId and formId set is required"));

        }

    }

    private void add(HealthMessage hm) throws Exception {
        log.error("TODO Jay>>>>>> Adding a new CMR");
        DefaultHttpClient httpclient = new DefaultHttpClient();
        Integer eventId = null;

        List<TrisanoHealth> thList = hm.getTrisanoHealth();
        for (TrisanoHealth th : thList) {

            if (th.getCreateAssessmentEvent()) {
                th.setEventType(Type.ASSESSMENT_EVENT.getName());
            }

            String etfn = th.getEventTypeFormName();
            if (etfn == null) {
                hm.addStatusMessage(new StatusMessage("addCmr", Status.FAILURE_MISSING_DATA.toString(), "events.type is missing"));
                throw new Exception("events.type is missing");

            }
            if (!th.hasFirstReportedPhDate()) {
                hm.addStatusMessage(new StatusMessage("addCmr", Status.FAILURE_MISSING_DATA.toString(), "events.first_reported_PH_date is missing"));
                throw new Exception("events.first_reported_PH_date is required to create an event.");
            }

            this.preparePlacesAttributes(th, hm);

            try {
                URI uri = new URI(this.scheme, null, this.host, this.port,
                        this.context + th.getWebPage(), null, null);

                HttpPost httpPost = new HttpPost(uri);
                Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                httpPost.setHeader(header);

                // Set up parameters to add cmr
                List<NameValuePair> qparams = new ArrayList<>();
                this.setDemographic(qparams, th, hm, MODE_ADD, etfn);
                this.setClinical(qparams, th, hm, MODE_ADD, etfn);
                this.setLaboratory(qparams, th, hm, MODE_ADD, etfn);
                this.setContacts(qparams, th, hm, MODE_ADD, etfn);
                //       this.setEncounters(qparams, th, hm);
                this.setEpidemiological(qparams, th, hm, etfn);
                this.setReporting(qparams, th, hm, MODE_ADD, -1, etfn);
                //       this.setInvestigation(qparams, th, hm);
                this.setNotes(qparams, th, hm, etfn);
                this.setAdministrative(qparams, th, hm, etfn);
                this.setEsp(qparams, th, hm, etfn, null);

                // set the default jurisdiction (not routed in the Utah case)
                qparams.add(new BasicNameValuePair(etfn + "[jurisdiction_attributes]"
                        + "[secondary_entity_id]", defaultJurisdiction));

                HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                HttpEntity entity = response.getEntity();

                if (entity != null) {
                    Float flen = new Float(entity.getContentLength());

                    InputStream is = entity.getContent();
                    byte[] ba = new byte[Math.round(flen)];
                    is.read(ba);
                    String out = new String(ba);

                    if (out.trim().startsWith("<html><body>You are being <a href")) {
                        String eId = out.substring(out.indexOf(th.getWebPage()) + th.getWebPage().length(), out.indexOf("redirected") - 2);
                        eventId = Integer.parseInt(eId);
                        hm.addStatusMessage(new StatusMessage("addCmr", Status.SUCCESS.toString(), "eventId=" + eventId));
                        th.getEvents().get(0).setId(eventId);
                        log.info("EVENT ADDED [eventId=" + eventId + "]");
                        addEspCustomFormFields(eId, th);
                   } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failure adding CMR...\n");
                        sb.append(response.getStatusLine());
                        sb.append("\n");
                        String[] outarr = out.split("id=\"errorExplanation\">");
                        if (outarr.length > 1) {
                            String msg = outarr[1];
                            out = msg.substring(0, msg.indexOf("</div>"));
                        }
                        sb.append(out);
                        String eMsg = sb.toString();
                        log.error(eMsg);
                        hm.addStatusMessage(new StatusMessage("addCmr", Status.FAILURE_TRISANO_ERROR.toString(), eMsg));
                    }
                }
                EntityUtils.consume(entity);

            } catch (Exception e) {
                log.error(e);
            } finally {
                // When HttpClient instance is no longer needed,
                // shut down the connection manager to ensure
                // immediate deallocation of all system resources
                httpclient.getConnectionManager().shutdown();
            }

            if (th.hasJurisdiction()) {
                String eventType = th.getEventType();
                if ((eventType.equals(Type.MORBIDITY_EVENT.getName()) || eventType.equals(Type.ASSESSMENT_EVENT.getName()))
                        && eventId != null && eventId > 0
                        && hm.addUpdateSuccess()) {
                    routeToJurisdiction(eventId, th, hm);
                }
            }

            // Add to queue here if needed 
            routeToDestinationQueue(eventId, th, hm);
        }
    }

    private void update(HealthMessage hm) throws Exception {
        log.error("TODO Jay >>>>>> Updating a CMR");

        String errorStatusMsg = "eventId or person.entityId missing";
        DefaultHttpClient httpclient = new DefaultHttpClient();
        TrisanoDaoImpl.fixReportNumberInEventId(hm);
        List<TrisanoHealth> thList = hm.getTrisanoHealth();
        for (TrisanoHealth th : thList) {

            // Make sure Event type name and web page are all ssynced up
            //  MorbidityEvent/cmrs/morbidity_event AssessmentEvent/aes/assessment_event ContactEvent/contact_events/contact_event
            th.setEventType(th.getEventType());
            String eventType = th.getEventTypeFormName();
            if (eventType == null) {
                hm.addStatusMessage(new StatusMessage("updateCmr", Status.FAILURE_MISSING_DATA.toString(), "events.type missing"));
                throw new Exception("events.type is missing");

            }
            if (th.hasPersonEntityId() && !th.hasEventId()) {

                if (!th.hasFirstReportedPhDate()) {
                    hm.addStatusMessage(new StatusMessage("updateCmr.createEvent", Status.FAILURE_MISSING_DATA.toString(), "events.first_reported_PH_date"));
                    throw new Exception("events.first_reported_PH_date is required to create an event.");
                }
                this.createEvent(th, hm);
            }

//log.error("TODO Jay >>>>>> update() about to prepare place attributes");
            this.preparePlacesAttributes(th, hm);

            List<Events> events = th.getEvents();
            if (events != null && !events.isEmpty()) {
                Events event = events.get(0);
                Integer eventId = event.getId();
//log.error("TODO Jay >>>>>> update() working on event: " + eventId);
                if (eventId != null && eventId > 0) {
                    try {

                        URI uri = uri = new URI(this.scheme, null, this.host, this.port,
                                this.context + th.getWebPage() + eventId, null, null);

                        HttpPost httpPost = new HttpPost(uri);
                        Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                        httpPost.setHeader(header);

                        // Set up parameters to add cmr
                        List<NameValuePair> qparams = new ArrayList<>();
                        qparams.add(new BasicNameValuePair("_method", "put"));
                        this.setDemographic(qparams, th, hm, MODE_UPDATE, eventType);
                        this.setClinical(qparams, th, hm, MODE_UPDATE, eventType);
                        this.setLaboratory(qparams, th, hm, MODE_UPDATE, eventType);
                        this.setContacts(qparams, th, hm, MODE_UPDATE, eventType);
                        //       this.setEncounters(qparams, th, hm);
                        this.setEpidemiological(qparams, th, hm, eventType);
                        this.setReporting(qparams, th, hm, MODE_UPDATE, eventId, eventType);
                        //       this.setInvestigation(qparams, th, hm);
                        this.setNotes(qparams, th, hm, eventType);
                        this.setAdministrative(qparams, th, hm, eventType);
                        this.setEsp(qparams, th, hm, eventType, eventId);
//log.error("TODO Jay>>>>>> update() about to post");
                        HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                        HttpEntity entity = response.getEntity();

                        if (entity != null) {

                            Float flen = new Float(entity.getContentLength());
                            InputStream is = entity.getContent();
                            byte[] ba = new byte[Math.round(flen)];
                            is.read(ba);
                            String out = new String(ba);

                            if (out.trim().startsWith("<html><body>You are being <a href")) {
                                hm.addStatusMessage(new StatusMessage("updateCmr", Status.SUCCESS.toString(), "eventId=" + eventId));
                                log.info("Event UPDATED [eventId=" + eventId + "]");
                                addEspCustomFormFields(eventId.toString(), th);
                            } else {
                                hm.addStatusMessage(new StatusMessage("updateCmr", Status.FAILURE_TRISANO_ERROR.toString(), "Failure updating CMR [eventId=" + eventId + "]...\n"
                                        + response.getStatusLine()));
                                log.error("Failure updating CMR [eventId=" + eventId + "]...\n"
                                        + response.getStatusLine() + "\n" + out);
                            }

                        }
                        EntityUtils.consume(entity);

                    } finally {
                        // When HttpClient instance is no longer needed,
                        // shut down the connection manager to ensure
                        // immediate deallocation of all system resources
                        httpclient.getConnectionManager().shutdown();
                    }

                    if ((eventType.equals(Type.MORBIDITY_EVENT.getName()) || eventType.equals(Type.ASSESSMENT_EVENT.getName()))
                            && eventId != null && eventId > 0
                            && hm.addUpdateSuccess()) {
                        if (th.hasJurisdiction()) {
                            routeToJurisdiction(eventId, th, hm);
                        }
                    }
                    // Add to queue here if needed
                    errorStatusMsg = "error routing message to queue";
                    routeToDestinationQueue(eventId, th, hm);

                    // Create a notification task for the investigator if needed
                    errorStatusMsg = "error creating positive lab assignment task";
                    addPositiveLabNotification(th, event);

                } else {
                    hm.addStatusMessage(new StatusMessage("updateCmr", Status.FAILURE_MISSING_DATA.toString(), errorStatusMsg));
                }

            } else {
                hm.addStatusMessage(new StatusMessage("updateCmr", Status.FAILURE_MISSING_DATA.toString(), errorStatusMsg));
            }

        }

    }

    private void routeToJurisdiction(
            Integer eventId, TrisanoHealth th, HealthMessage hm) throws Exception {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<JurisdictionAttributes> jaList = th.getJurisdictionAttributes();
        JurisdictionAttributes ja = null;
        ja = jaList.get(0);
        Places j = ja.getPlace();

        try {

            ///trisano/cmrs/47886/jurisdiction
            URI uri = new URI(this.scheme, null, this.host, this.port, this.context + th.getWebPage() + eventId + "/jurisdiction", null, null);

            HttpPost httpPost = new HttpPost(uri);

            Header header_uid = new BasicHeader(this.uid_header_name, hm.getUsername());
            Header header_referrer = new BasicHeader("Referer", referringSite);
            httpPost.setHeader(header_uid);
            httpPost.setHeader(header_referrer);

            List<NameValuePair> qparams = new ArrayList<>();

            qparams.add(new BasicNameValuePair("routing[jurisdiction_id]", j.getId().toString()));
            qparams.add(new BasicNameValuePair("commit", "Route event"));
            HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
            HttpEntity entity = response.getEntity();

            if (entity != null) {

                Float flen = new Float(entity.getContentLength());
                InputStream is = entity.getContent();
                byte[] ba = new byte[Math.round(flen)];
                is.read(ba);
                String out = new String(ba);

                if (out.trim().startsWith("<html><body>You are being <a href")) {
                    hm.addStatusMessage(new StatusMessage("routeToJurisdiction", Status.SUCCESS.toString(), "eventId=" + eventId + ", jurisdictionId=" + j.getId().toString()));
                    log.info("ROUTED TO JURISDICTION [eventId=" + eventId + ", jurisdictionId=" + j.getId().toString() + "]");
                } else {
                    hm.addStatusMessage(new StatusMessage("routeToJurisdiction", Status.FAILURE_TRISANO_ERROR.toString(), "eventId=" + eventId + ", jurisdictionId=" + j.getId().toString() + "\n"
                            + response.getStatusLine() + "\n" + out));
                    log.error("Trouble routing event.[eventId=" + eventId + "]...\n"
                            + response.getStatusLine() + "\n" + out);
                }

            }
            EntityUtils.consume(entity);
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }

    }

    /**
     * If a valid destination queue is specified, assign this event to the queue
     *
     * @param eventId
     * @param destinationQueue
     * @throws Exception
     */
    private void routeToDestinationQueue(Integer eventId, TrisanoHealth th, HealthMessage hm) throws Exception {
        String queue = th.getDestinationQueue();
        String etfn = th.getEventTypeFormName();
        Integer queueId = TrisanoDaoImpl.getQueueId(queue);
        if (queueId != -1) {
            log.error("TODO Jay >>>>>> routeToDestinationQueue ");
            
            // NOTE:  For the params, the morbidity event URI is used for this action for cmr and contact events
            if(etfn.equals("contact_event")) {
                etfn = "morbidity_event";
            }

            String workflowState = TrisanoDaoImpl.getWorkflowState(eventId);
            if (workflowState.equals("assigned_to_lhd")) {
                setWorkflow(hm.getUsername(), etfn, eventId, th.getEventType(), "accept");
            } else if (workflowState.equals("approved_by_lhd") || workflowState.equals("closed")) {
                setWorkflow(hm.getUsername(), etfn, eventId, th.getEventType(), "reopen");
            }
                 
            DefaultHttpClient httpclient = new DefaultHttpClient();

            try {
                URI uri = new URI(this.scheme, null, this.host, this.port,
                        this.context + th.getWebPage() + eventId + "/state", null, null);

                HttpPost httpPost = new HttpPost(uri);
                Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                Header header_referrer = new BasicHeader("Referer", referringSite);
                httpPost.setHeader(header);
                httpPost.setHeader(header_referrer);
//                httpPost.getHeaders();

                // Set up parameters 
                List<NameValuePair> qparams = new ArrayList<>();
                qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", "assign_to_queue"));
                qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
                qparams.add(new BasicNameValuePair(etfn + "[event_queue_id]", queueId.toString()));
//log.error("TODO Jay >>>>>> routeToDestinationQueue() about to post");
                HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                HttpEntity entity = response.getEntity();

                if (entity != null) {

                    Float flen = new Float(entity.getContentLength());
                    InputStream is = entity.getContent();
                    byte[] ba = new byte[Math.round(flen)];
                    is.read(ba);
                    String out = new String(ba);

                    if (out.trim().startsWith("<html><body>You are being <a href")) {
                        hm.addStatusMessage(new StatusMessage("routeToDestinationQueue", Status.SUCCESS.toString(), "eventId=" + eventId));
                        log.info("Event [eventId=" + eventId + "] " + "routed to queue: " + queue);

                    } else {
                        hm.addStatusMessage(new StatusMessage("routeToDestinationQueue", Status.FAILURE_TRISANO_ERROR.toString(), "Failure sending event to queue [eventId=" + eventId + "]...\n"
                                + response.getStatusLine()));
                        log.error("Failure sending event to queue [eventId=" + eventId + "]...\n"
                                + response.getStatusLine() + "\n" + out);
                    }

                }
                EntityUtils.consume(entity);

            } finally {
                // When HttpClient instance is no longer needed,
                // shut down the connection manager to ensure
                // immediate deallocation of all system resources
                httpclient.getConnectionManager().shutdown();
            }
        }
    }

    /**
     * If we are supposed to add a task to notify the investigator that a
     * positive lab has been added to an event do so
     *
     * @param eventId
     * @param th
     * @throws Exception
     */
    private void addPositiveLabNotification(TrisanoHealth th, Events event) throws Exception {

        if(!emsaEnvironment.equals("SNHD") || th.getAddPositiveLabTask() == null) {
            return;
        }
        
        // If a positive lab was added to this event and the 
        // event has an investigator assigned to it
        String queue = TrisanoDaoImpl.getQueue(event.getId());
        Integer investigatorId = validInvestigatorIdForTask(TrisanoDaoImpl.getInvestigatorId(event.getId()), 
                th.getAddPositiveLabTask());
        String fullName = TrisanoDaoImpl.fullPersonNameFromEvent(event.getId());
        
        // if the task is not assigned to the Hep C chronic or Hep B chronic queues
        if (investigatorId > 0 && !queue.equals("HepCChronic-Snhdooe") && !queue.equals("HepBChronic-Snhdooe")) {

            // add the task info
            List<Tasks> taskList = new ArrayList<Tasks>();
            Tasks task = new Tasks();
            task.setEventId(event.getId());
            task.setName(fullName + ": Positive lab added to event by EMSA.");
            task.setNotes("");
            task.setUserId(investigatorId);
            task.setPriority("medium");
            task.setDueDate(new Date());
            task.setRepeatingInterval("");
            taskList.add(task);

            th.setTasks(taskList);
            HealthMessage hm = new HealthMessage();
            List<TrisanoHealth> thList = new ArrayList<TrisanoHealth>();
            thList.add(th);
            hm.setTrisanoHealth(thList);
            createTask(hm);
        }
    }

    /**
     *
     * Closing a cmr...
     *
     * 1. Accept:
     * morbidity_event[workflow_action]=accept&morbidity_event[note]=&accept=accept
     * 2. Assign to an investigator:
     * morbidity_event[workflow_action]=assign_to_investigator&morbidity_event[note]=&morbidity_event[investigator_id]=33&morbidity_event[event_queue_id]=
     * 3. Accept again:
     * morbidity_event[workflow_action]=accept&morbidity_event[note]=&accept=accept&morbidity_event[event_queue_id]=22&morbidity_event[investigator_id]=33
     * 4. Complete:
     * morbidity_event[workflow_action]=complete&morbidity_event[note]=&morbidity_event[event_queue_id]=22&morbidity_event[investigator_id]=33
     * 5. Approve:
     * morbidity_event[workflow_action]=approve&morbidity_event[note]=&approve=approve&morbidity_event[event_queue_id]=22&morbidity_event[investigator_id]=33
     * 6. Approve once more:
     * morbidity_event[workflow_action]=approve&morbidity_event[note]=&approve=approve
     *
     */
    private void closeCmr(
            Integer eventId, TrisanoHealth th, HealthMessage hm) throws Exception {

        String etfn = th.getEventTypeFormName();
        if (etfn == null) {

            throw new Exception("events.type is missing");

        }

        String uid = hm.getUsername();
        String investigatorId = "268";
        URI uri = new URI(this.scheme, null, this.host, this.port,
                this.context + "/cmrs/" + eventId + "/state", null, null);

        HttpPost httpPost = new HttpPost(uri);

        List<NameValuePair> qparams = new ArrayList<>();

        log.info("CLOSING CMR... [eventId=" + eventId + "]");

        // Step 1
        qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", "accept"));
        qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
        qparams.add(new BasicNameValuePair("accept", "accept"));

        Map<Integer, String> out = post(uid, uri, qparams);
        String response = out.get(RESPONSE_KEY);
        String status = out.get(STATUS_KEY);

        if (response != null && response.trim().startsWith("<html><body>You are being <a href")) {
            log.info("Step 1: Success  ");
        } else {
            log.error("Step 1: Failure...\n" + status + "\n" + response);
            throw new Exception(response);
        }

        // Step 2
        qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", "assign_to_investigator"));
        qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
        qparams.add(new BasicNameValuePair(etfn + "[investigator_id]", investigatorId));
        qparams.add(new BasicNameValuePair(etfn + "[event_queue_id]", ""));

        out = post(uid, uri, qparams);
        response = out.get(RESPONSE_KEY);
        status = out.get(STATUS_KEY);

        if (response != null && response.trim().startsWith("<html><body>You are being <a href")) {
            log.info("Step 2: Success  ");
        } else {
            log.error("Step 2: Failure...\n" + status + "\n" + response);
            throw new Exception(response);
        }

        // Step 3
        qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", "accept"));
        qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
        qparams.add(new BasicNameValuePair("accept", "accept"));
        qparams.add(new BasicNameValuePair(etfn + "[event_queue_id]", ""));
        qparams.add(new BasicNameValuePair(etfn + "[investigator_id]", investigatorId));

        out = post(uid, uri, qparams);
        response = out.get(RESPONSE_KEY);
        status = out.get(STATUS_KEY);

        if (response != null && response.trim().startsWith("<html><body>You are being <a href")) {
            log.info("Step 3: Success  ");
        } else {
            log.error("Step 3: Failure...\n" + status + "\n" + response);
            throw new Exception(response);
        }

        // Step 4
        qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", "complete"));
        qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
        qparams.add(new BasicNameValuePair(etfn + "[event_queue_id]", ""));
        qparams.add(new BasicNameValuePair(etfn + "[investigator_id]", investigatorId));

        out = post(uid, uri, qparams);
        response = out.get(RESPONSE_KEY);
        status = out.get(STATUS_KEY);

        if (response != null && response.trim().startsWith("<html><body>You are being <a href")) {
            log.info("Step 4: Success  ");
        } else {
            log.error("Step 4: Failure...\n" + status + "\n" + response);
            throw new Exception(response);
        }

        // Step 5
        qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", "approve"));
        qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
        qparams.add(new BasicNameValuePair("approve", "approve"));
        qparams.add(new BasicNameValuePair(etfn + "[event_queue_id]", ""));
        qparams.add(new BasicNameValuePair(etfn + "[investigator_id]", investigatorId));

        out = post(uid, uri, qparams);
        response = out.get(RESPONSE_KEY);
        status = out.get(STATUS_KEY);

        if (response != null && response.trim().startsWith("<html><body>You are being <a href")) {
            log.info("Step 5: Success  ");
        } else {
            log.error("Step 5: Failure...\n" + status + "\n" + response);
            throw new Exception(response);
        }

        // Step 6
        qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", "approve"));
        qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
        qparams.add(new BasicNameValuePair("approve", "approve"));

        out = post(uid, uri, qparams);
        response = out.get(RESPONSE_KEY);
        status = out.get(STATUS_KEY);

        if (response != null && response.trim().startsWith("<html><body>You are being <a href")) {
            log.info("Step 6: Success  ");
        } else {
            log.error("Step 6: Failure...\n" + status + "\n" + response);
            throw new Exception(response);
        }

    }

    private void preparePlacesAttributes(TrisanoHealth th, HealthMessage hm)
            throws Exception {

        getOrAddPlaces(th.getReportingAgencyAttributes(), hm);
        getOrAddPlaces(th.getLabsAttributes(), hm);
        getOrAddPlaces(th.getHospitalizationFacilitiesAttributes(), hm);
        getOrAddPlaces(th.getDiagnosticFacilitiesAttributes(), hm);

    }

    private void getOrAddPlaces(List paList, HealthMessage hm)
            throws Exception {

        if (paList != null && !paList.isEmpty()) {
            for (Object pa : paList) {
                PlaceAttributes pa2 = (PlaceAttributes) pa;
                Places p = pa2.getPlace();
                if (p != null) {
                    Integer entityId = pa2.getPlace().getEntityId();
                    if (entityId == null || entityId <= 0) {
                        TrisanoDaoImpl.findPlaceAttributes(pa2, hm);
                        entityId = pa2.getPlace().getEntityId();
                        if (entityId == null || entityId <= 0) {
                            this.addPlace(pa2, hm);
                            TrisanoDaoImpl.findPlaceAttributes(pa2, hm);
                        }
                    }
                }
            }
        }

    }

    private void addPlace(PlaceAttributes pa, HealthMessage hm)
            throws Exception {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        Places p = pa.getPlace();

        if (p != null) {

            String pName = p.getName();
            if (pName != null && pName.trim().length() > 0) {
                try {

//place_entity[place_attributes][name]
                    //place_entity[place_attributes][place_type_ids][]
                    URI uri = new URI(this.scheme, null, this.host, this.port, this.context + "/places", null, null);

                    HttpPost httpPost = new HttpPost(uri);

                    Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                    httpPost.setHeader(header);

                    List<NameValuePair> qparams = new ArrayList<>();

                    qparams.add(new BasicNameValuePair("place_entity[place_attributes][name]", pName));
                    qparams.add(new BasicNameValuePair("place_entity[place_attributes][place_type_ids][]", pa.placeType.toString()));
                    HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        Float flen = new Float(entity.getContentLength());
                        InputStream is = entity.getContent();
                        byte[] ba = new byte[Math.round(flen)];
                        is.read(ba);
                        String out = new String(ba);

                        if (out.trim().startsWith("<html><body>You are being <a href")) {
// success:
// <html><body>You are being <a href="http://localhost:8080/nedss30/places/165722">redirected</a>.</body></html>
                            String placeEntityId = out.substring(out.indexOf("/places/") + 8, out.indexOf("redirected") - 2);
                            p.setEntityId(Integer.parseInt(placeEntityId));
                            hm.addStatusMessage(new StatusMessage("addPlace", Status.SUCCESS.toString(), "name=" + pName + ", entityId=" + placeEntityId));
                            log.info("PLACE ADDED [name=" + pName + ", entityId=" + placeEntityId + "]");
                        } else {
                            log.error("Failure adding place [" + pName + "]... \n"
                                    + uri + "\n" + response.getStatusLine());
                            hm.addStatusMessage(new StatusMessage("addPlace", Status.FAILURE_TRISANO_ERROR.toString(), "place=" + pName + ", "
                                    + "placeType=" + pa.placeType + "\n" + response.getStatusLine() + "\n" + out));

                        }

                    }
                    EntityUtils.consume(entity);
                } finally {
                    // When HttpClient instance is no longer needed,
                    // shut down the connection manager to ensure
                    // immediate deallocation of all system resources
                    httpclient.getConnectionManager().shutdown();
                }

            } else {
                hm.addStatusMessage(new StatusMessage("addPlace", Status.FAILURE_MISSING_DATA.toString(), "place.name missing"));
                throw new Exception("Place name is not set.");
            }

        }
    }

    private void createEvent(TrisanoHealth th, HealthMessage hm)
            throws Exception {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        InterestedPartyAttributes ipa = th.getInterestedPartyAttributes().get(0);
        People p = ipa.getPerson();

        try {

            URI uri = new URI(this.scheme, null, this.host, this.port,
                    this.context + "/cmrs", "from_person=" + p.getEntityId() + "&return=true", null);

            HttpPost httpPost = new HttpPost(uri);

            Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
            httpPost.setHeader(header);
            List<NameValuePair> qparams = new ArrayList<>();
            HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                Float flen = new Float(entity.getContentLength());
                InputStream is = entity.getContent();
                byte[] ba = new byte[Math.round(flen)];
                is.read(ba);
                String out = new String(ba);
                if (out.trim().startsWith("<html><body>You are being <a href")) {
                    String eventId = out.substring(out.indexOf("/cmrs/") + 6, out.indexOf("redirected") - 7);
                    Integer eId = Integer.parseInt(eventId);
                    hm.addStatusMessage(new StatusMessage("createEvent", Status.SUCCESS.toString(), "eventId=" + eId));
                    th.setEventId(eId);
                    log.info("EVENT CREATED [for entityId=" + p.getEntityId() + ", eventId=" + eventId + "]");

                    Participations part = new Participations();
                    part.setEventId(eId);
                    part.setType(Type.INTERESTED_PARTY.getName());
                    TrisanoDaoImpl.findParticipations(part);
                    ipa.addParticipation(part);

                } else {
                    hm.addStatusMessage(new StatusMessage("createEvent", Status.FAILURE_TRISANO_ERROR.toString(), "entityId=" + p.getEntityId() + ", "
                            + uri + "\n" + response.getStatusLine() + "\n" + out));
                    log.error("Failure creating event. [for entityId=" + p.getEntityId() + "] \n"
                            + uri + "\n" + response.getStatusLine() + "\n" + out);

                }

            }
            EntityUtils.consume(entity);
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }

    }

    private void setDemographic(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, int mode, String etfn)
            throws Exception {

        List<InterestedPartyAttributes> ipaList = th.getInterestedPartyAttributes();
        if (ipaList != null && !ipaList.isEmpty()) {
            InterestedPartyAttributes ipa = ipaList.get(0);

            if (ipa != null) {

                People person = ipa.getPerson();
                if (person != null) {

                    String pLn = person.getLastName();

                    if (mode == MODE_ADD && (pLn == null || pLn.trim().length() == 0)) {
                        hm.addStatusMessage(new StatusMessage(mode + ".setDemographic", Status.FAILURE_MISSING_DATA.toString(), "interestedPartyAttributes.person missing"));
                        throw new Exception("lastName missing in interestedPartyAttributes.person");
                    }

                    if (mode == MODE_ADD) {
                        People personT = new People();
                        // if we are updating an existing person, fetch the existing person Demographics from the database
                        if (person.getId() != null && person.getId() > 0 && 
                                TrisanoDaoImpl.getPersonDemographics(person.getId(), personT)) {
log.info("TODO Jay New CMR, existing person >>>>>>>> person id: " + person.getId().toString());

                            // These are the conditions TriSano uses to determine an "exact match"
                            person.setLastName(personT.getLastName());
                            person.setFirstName(personT.getFirstName());
                            person.setMiddleName(personT.getMiddleName());
                            person.setBirthDate(personT.getBirthDate());
                            person.setBirthGenderId(personT.getBirthGenderId());

                        } 
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][last_name]", pLn));
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][first_name]", person.getFirstName()));
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][middle_name]", person.getMiddleName()));
                    }


                    // TODO Jay, this section which specifies and existing person while adding a CMR is commented
                    // out in the UTAH code, WHY?????
//                    if (mode == MODE_ADD && th.hasPersonEntityId()) {
//                        if (person.getId() != null && person.getId() > 0) {
//log.info("TODO Jay >>>>>>>>>>>>>>>>>>>>> person id: " + person.getId().toString());
//                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][id]", person.getId().toString()));
//                        }
//                        if (person.getEntityId() != null && person.getEntityId() > 0) {
//log.info("TODO Jay >>>>>>>>>>>>>>>>>>>>> person entity id: " + person.getEntityId().toString());
//                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][id]", person.getEntityId().toString()));
//                        }
//
////                        if (ipa.getParticipations() != null && ipa.getParticipations().get(0).getId() != null && ipa.getParticipations().get(0).getId() > 0) {
//                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][id]", ipa.getParticipations().get(0).getId().toString()));
//                        }
//                    } else if (mode == MODE_UPDATE) {
//                        hm.addStatusMessage(new StatusMessage(mode + ".setDemographic", Status.FAILURE_MISSING_DATA.toString(), "person.id and person.entityId"));
//                        throw new Exception("Update failed. person.id and person.entityId required.");
//                    }
                    if (person.getBirthDate() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][birth_date]", dateformatYYYYMMDD.format(person.getBirthDate())));
                    }
                    if (person.getPrimaryLanguageId() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][primary_language_id]", person.getPrimaryLanguageId().toString()));
                    }
                    if (person.getApproximateAgeNoBirthday() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][approximate_age_no_birthday]", person.getApproximateAgeNoBirthday().toString()));
                    }
                    if (person.getBirthGenderId() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][birth_gender_id]", person.getBirthGenderId().toString()));
                    }
                    if (person.getEthnicityId() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][ethnicity_id]", person.getEthnicityId().toString()));
                    }
                    if (person.getDateOfDeath() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][person_attributes][date_of_death]", dateformatYYYYMMDD.format(person.getDateOfDeath())));
                    }

                    List<ParticipationsRiskFactors> prfList = ipa.getRiskFactorAttributes();
                    if (prfList != null && !prfList.isEmpty()) {
                        ParticipationsRiskFactors prf = prfList.get(0);
                        if (prf.getPregnantId() != null) {
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][pregnant_id]", prf.getPregnantId().toString()));
                        }
                        if (prf.getPregnancyDueDate() != null) {
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][pregnancy_due_date]", this.dateformatYYYYMMDD.format(prf.getPregnancyDueDate())));
                        }
                        if (prf.getFoodHandlerId() != null) {
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][food_handler_id]", prf.getFoodHandlerId().toString()));
                        }
                        if (prf.getHealthcareWorkerId() != null) {
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][healthcare_worker_id]", prf.getHealthcareWorkerId().toString()));
                        }
                        if (prf.getGroupLivingId() != null) {
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][group_living_id]", prf.getGroupLivingId().toString()));
                        }
                        if (prf.getDayCareAssociationId() != null) {
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][day_care_association_id]", prf.getDayCareAssociationId().toString()));
                        }
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][occupation]", prf.getOccupation()));

//morbidity_event[imported_from_id]
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][risk_factors]", prf.getRiskFactors()));
                        qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][risk_factor_attributes][risk_factors_notes]", prf.getRiskFactorsNotes()));

                    }
                    List<Telephones> telephones = ipa.getTelephones();
                    if (telephones != null) {
                        int count = 0;
                        for (Telephones t : telephones) {

                            if (t.getId() != null && t.getId() > 0) {
                                qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][telephones_attributes][" + count + "][id]", t.getEntityLocationTypeId().toString()));
                            }
                            if (t.getEntityLocationTypeId() != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][telephones_attributes][" + count + "][entity_location_type_id]", t.getEntityLocationTypeId().toString()));
                            }
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][telephones_attributes][" + count + "][country_code]", t.getCountryCode()));
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][telephones_attributes][" + count + "][area_code]", t.getAreaCode()));
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][telephones_attributes][" + count + "][phone_number]", t.getPhoneNumber()));
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][telephones_attributes][" + count + "][extension]", t.getExtension()));
                            qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][email_addresses_attributes][" + count + "][email_address]", t.getEmailAddress()));
                            count++;
                        }

                    }

                    List<ParticipationsTreatments> ptList = ipa.getTreatmentsAttributes();
                    if (ptList != null && !ptList.isEmpty()) {
                        int count = 0;
                        for (ParticipationsTreatments pt : ptList) {
                            if (pt.getTreatmentGivenYnId() != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][treatments_attributes][" + count + "][treatment_given_yn_id]", pt.getTreatmentGivenYnId().toString()));
                            }
                            if (pt.getTreatmentDate() != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][treatments_attributes][" + count + "][treatment_date]", this.dateformatYYYYMMDD.format(pt.getTreatmentDate())));
                            }
                            if (pt.getStopTreatmentDate() != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][treatments_attributes][" + count + "][stop_treatment_date]", this.dateformatYYYYMMDD.format(pt.getStopTreatmentDate())));
                            }
                            count++;
                        }
                    }

                    List<PeopleRaces> peopleRaces = ipa.getPeopleRaces();
                    if (peopleRaces != null) {
                        for (PeopleRaces pr : peopleRaces) {
                            if (pr.getPeopleRacesPK() != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[interested_party_attributes][person_entity_attributes][race_ids][]", Integer.toString(pr.getPeopleRacesPK().getRaceId())));
                            }
                        }
                    }
                }

            } else {
                if (mode == MODE_ADD) {
                    hm.addStatusMessage(new StatusMessage(mode + ".setDemographic", Status.FAILURE_MISSING_DATA.toString(), "interested_party_attributes missing."));
                    throw new Exception("interested_party_attributes missing in request.");
                }
            }

        } else {
            if (mode == MODE_ADD) {
                hm.addStatusMessage(new StatusMessage(mode + ".setDemographic", Status.FAILURE_MISSING_DATA.toString(), "interested_party_attributes missing."));
                throw new Exception("interested_party_attributes missing in request.");
            }
        }

        List<Events> eventList = th.getEvents();
        if (eventList != null && !eventList.isEmpty()) {
            Events event = eventList.get(0);
            if (event.getParentGuardian() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[parent_guardian]", event.getParentGuardian()));
            }
        }

        if (th.getAddresses() != null && !th.getAddresses().isEmpty()) {
            Addresses addy = th.getAddresses().get(0);
            if (addy.getId() != null && addy.getId() > 0) {

                qParams.add(new BasicNameValuePair(etfn + "[address_attributes][id]]", addy.getId().toString()));

            }

            qParams.add(new BasicNameValuePair(etfn + "[address_attributes][street_name]", addy.getStreetName()));
            qParams.add(new BasicNameValuePair(etfn + "[address_attributes][unit_number]", addy.getUnitNumber()));
            qParams.add(new BasicNameValuePair(etfn + "[address_attributes][city]", addy.getCity()));
            if (addy.getStateId() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[address_attributes][state_id]", addy.getStateId().toString()));
            }
            if (addy.getCountyId() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[address_attributes][county_id]", addy.getCountyId().toString()));
            }
            qParams.add(new BasicNameValuePair(etfn + "[address_attributes][postal_code]", addy.getPostalCode()));
            if (addy.getLatitude() != null && addy.getLongitude() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[address_attributes][latitude]", addy.getLatitude().toString()));
                qParams.add(new BasicNameValuePair(etfn + "[address_attributes][longitude]", addy.getLongitude().toString()));
            }
        }

    }

    private void setClinical(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, int mode, String etfn) throws Exception {

        List<DiseaseEvents> diseaseEvents = th.getDiseaseEvents();
        if (diseaseEvents != null) {
            DiseaseEvents de = diseaseEvents.get(0);

            if (de.getId() != null && de.getId() > 0) {
                qParams.add(new BasicNameValuePair(etfn + "[disease_event_attributes][id]", de.getId().toString()));
            }

            if (de.getDiseaseId() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[disease_event_attributes][disease_id]", de.getDiseaseId().toString()));
            }
            if (de.getDiseaseOnsetDate() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[disease_event_attributes][disease_onset_date]", dateformatYYYYMMDD.format(de.getDiseaseOnsetDate())));
            }
            if (de.getDateDiagnosed() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[disease_event_attributes][date_diagnosed]", dateformatYYYYMMDD.format(de.getDateDiagnosed())));
            }
            if (de.getHospitalizedId() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[disease_event_attributes][hospitalized_id]", de.getHospitalizedId().toString()));
            }
            if (de.getDiedId() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[disease_event_attributes][died_id]", de.getDiedId().toString()));
            }
        }

        List<HospitalizationFacilitiesAttributes> hfaList = th.getHospitalizationFacilitiesAttributes();
        if (hfaList != null && !hfaList.isEmpty()) {
            int count = 0;
            for (HospitalizationFacilitiesAttributes hfa : hfaList) {

                TrisanoDaoImpl.findPlaceAttributes(hfa, hm);

                Places hf;
                HospitalsParticipations hp;

                hf = hfa.getPlace();
                hp = hfa.getHospitalsParticipations();

                if (hf != null && hf.getEntityId() != null && hf.getEntityId() > 0) {
                    qParams.add(new BasicNameValuePair(etfn + "[hospitalization_facilities_attributes][" + count + "][secondary_entity_id]", hf.getEntityId().toString()));
                }
                if (hp != null) {
                    if (hp.getAdmissionDate() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[hospitalization_facilities_attributes][" + count + "][hospitals_participation_attributes][admission_date]", this.dateformatYYYYMMDD.format(hp.getAdmissionDate())));
                    }
                    if (hp.getDischargeDate() != null) {
                        qParams.add(new BasicNameValuePair(etfn + "[hospitalization_facilities_attributes][" + count + "][hospitals_participation_attributes][discharge_date]", this.dateformatYYYYMMDD.format(hp.getDischargeDate())));
                    }
                    qParams.add(new BasicNameValuePair(etfn + "[hospitalization_facilities_attributes][" + count + "][hospitals_participation_attributes][medical_record_number]", hp.getMedicalRecordNumber()));

                }
                count++;
            }

        }

        List<CliniciansAttributes> caList = th.getClinicianAttributes();
        if (caList != null && !caList.isEmpty()) {
            int cc = 0;
            for (CliniciansAttributes ca : caList) {
                People c = ca.getClinician();

                if (c.getEntityId() == null || c.getEntityId() <= 0) {
                    TrisanoDaoImpl.findPersonType(c);
                }

                if (mode == MODE_ADD && c.getEntityId() != null && c.getEntityId() > 0) {
                    // clinician exists use it...
                    qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][secondary_entity_id]", c.getEntityId().toString()));

                } else {

                    String cFn = c.getFirstName();
                    String cLn = c.getLastName();
                    String cPt = c.getPersonType();
                    if(cPt == null || cPt.length() < 1) {
                        cPt = "clinician";
                    }

                    if (cFn != null && cLn != null
                            && cLn.trim().length() > 0 && cFn.trim().length() > 0) {

                        if (c.getEntityId() != null && c.getEntityId() > 0) {
//                        if (mode == MODE_UPDATE && c.getEntityId() != null && c.getEntityId() > 0) {
                            qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][secondary_entity_id]", c.getEntityId().toString()));
                        }

                        qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][person_attributes][person_type]", cPt));
                        qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][person_attributes][last_name]", c.getLastName()));
                        qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][person_attributes][first_name]", c.getFirstName()));
                        qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][person_attributes][middle_name]", c.getMiddleName()));
                        List<Telephones> clinicianTelephones = ca.getTelephones();
                        if (clinicianTelephones != null && !clinicianTelephones.isEmpty()) {
                            int count = 0;
                            for (Telephones t : clinicianTelephones) {

                                if (t.getEntityLocationTypeId() != null) {
                                    qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][0][person_entity_attributes][telephones_attributes][" + count + "][entity_location_type_id]", t.getEntityLocationTypeId().toString()));
                                }
                                qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][telephones_attributes][" + count + "][country_code]", t.getCountryCode()));
                                qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][telephones_attributes][" + count + "][area_code]", t.getAreaCode()));
                                qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][telephones_attributes][" + count + "][phone_number]", t.getPhoneNumber()));
                                qParams.add(new BasicNameValuePair(etfn + "[clinicians_attributes][" + cc + "][person_entity_attributes][telephones_attributes][" + count + "][extension]", t.getExtension()));
                                count++;
                            }
                        }
                        cc++;
                    }
                }
            }
        }

        List<DiagnosticFacilitiesAttributes> dfaList = th.getDiagnosticFacilitiesAttributes();
        if (dfaList != null && !dfaList.isEmpty()) {
            long count = 1297198345;
            for (DiagnosticFacilitiesAttributes dfa : dfaList) {

                Places df = dfa.getPlace();

                if (df != null && df.getEntityId() != null && df.getEntityId() > 0) {

                    qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][secondary_entity_id]", df.getEntityId().toString()));
                    qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][place_attributes][id]", df.getId().toString()));

                    if (dfa.hasValidAddress()) {
                        Integer addyId = dfa.getAddresses().get(0).getId();
                        qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][canonical_address_attributes][id]", addyId.toString()));
                    } else {

                        List<Addresses> dfAddressList = dfa.getAddresses();
                        if (dfAddressList != null && !dfAddressList.isEmpty()) {
                            Addresses dfAddress = dfAddressList.get(0);
                            String streetName = dfAddress.getStreetName();
                            String unitNumber = dfAddress.getUnitNumber();
                            String city = dfAddress.getCity();
                            Integer stateId = dfAddress.getStateId();
                            Integer countryId = dfAddress.getCountyId();
                            String postalCode = dfAddress.getPostalCode();
                            if (streetName != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][canonical_address_attributes][street_name]", streetName));
                            }
                            if (unitNumber != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][canonical_address_attributes][unit_number]", unitNumber));
                            }
                            if (city != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][canonical_address_attributes][city]", city));
                            }
                            if (stateId != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][canonical_address_attributes][state_id]", Integer.toString(stateId)));
                            }
                            if (countryId != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][canonical_address_attributes][county_id]", Integer.toString(countryId)));
                            }
                            if (postalCode != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][canonical_address_attributes][postal_code]", postalCode));
                            }
                        }
                    }
                    qParams.add(new BasicNameValuePair(etfn + "[diagnostic_facilities_attributes][" + count + "][place_entity_attributes][id]", df.getEntityId().toString()));
                    count++;
                }

                /////////////
// Trisano Community input fields for diagnostic facility
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][secondary_entity_id]" value="104781" type="hidden">
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][place_attributes][id]" value="17524" type="hidden">
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][canonical_address_attributes][street_name]" size="50" type="text">
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][canonical_address_attributes][unit_number]" size="10" type="text">
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][canonical_address_attributes][city]" size="30" type="text">
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][canonical_address_attributes][state_id]">
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][canonical_address_attributes][county_id]">
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][canonical_address_attributes][postal_code]"
//name="morbidity_event[diagnostic_facilities_attributes][1360195265][place_entity_attributes][id]" value="104781" type="hidden">
///////////////
            }

        }
    }

    private void setLaboratory(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, int mode, String etfn)
            throws Exception {

        List<LabsAttributes> laList = th.getLabsAttributes();
        if (laList != null && !laList.isEmpty()) {
            String lc = "0";
            String lrc = "1354737922";
            for (LabsAttributes la : laList) {

                Places lab = la.getPlace();
                LabResults lr = la.getLabResults().get(0);

                if (la.isValidForLabResultAddUpdate()) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][id]", la.getParticipationId().toString()));
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][place_entity_attributes][place_attributes][id]", la.getPlaceId().toString()));
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][place_entity_attributes][id]", la.getPlaceEntityId().toString()));

                    if (la.updateLabResult()) {
                        lrc = "0";
                        qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][0][id]", lr.getId().toString()));
                    }
                }
//                else if (la.isValidForLabAdd()) {
//                    lc = "1354737922";
//                }
                qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][place_entity_attributes][place_attributes][name]", lab.getName()));

                if (lr.getLoincCode() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][loinc_code]", lr.getLoincCode().toString()));
                }
                if (lr.getTestTypeId() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][test_type_id]", lr.getTestTypeId().toString()));
                }
                if (lr.getOrganismId() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][organism_id]", lr.getOrganismId().toString()));
                }
                if (lr.getTestResultId() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][test_result_id]", lr.getTestResultId().toString()));
                }
                qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][result_value]", lr.getResultValue()));
                qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][units]", lr.getUnits()));
                qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][reference_range]", lr.getReferenceRange()));
                if (lr.getTestStatusId() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][test_status_id]", lr.getTestStatusId().toString()));
                }
                if (lr.getSpecimenSourceId() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][specimen_source_id]", lr.getSpecimenSourceId().toString()));
                }
                if (lr.getCollectionDate() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][collection_date]", dateformatYYYYMMDD.format(lr.getCollectionDate())));
                }
                if (lr.getLabTestDate() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][lab_test_date]", dateformatYYYYMMDD.format(lr.getLabTestDate())));
                }
                if (lr.getSpecimenSentToStateId() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][specimen_sent_to_state_id]", lr.getSpecimenSentToStateId().toString()));
                }
                if (lr.getAccessionNo() != null) {
                    qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][accession_no]", lr.getAccessionNo()));
                }

                qParams.add(new BasicNameValuePair(etfn + "[labs_attributes][" + lc + "][lab_results_attributes][" + lrc + "][comment]", lr.getComment()));

            }

        }

    }

    private void setEpidemiological(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, String etfn) throws Exception {
        if (th.getEvents().get(0).getOtherData1() != null) {
            qParams.add(new BasicNameValuePair(etfn + "[other_data_1]", th.getEvents().get(0).getOtherData1()));
        }
        if (th.getEvents().get(0).getOtherData2() != null) {
            qParams.add(new BasicNameValuePair(etfn + "[other_data_2]", th.getEvents().get(0).getOtherData2()));
        }
    }

    private void setEncounters(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm) throws Exception {
    }

    private void setContacts(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, int mode, String etfn) throws Exception {

        List<ContactChildEventsAttributes> cceaList = th.getContactChildEventsAttributes();

        if (cceaList != null && !cceaList.isEmpty()) {
            int count = 0;
            for (ContactChildEventsAttributes ccea : cceaList) {

                InterestedPartyAttributes ipa = ccea.getInterestedPartyAttributes();
                if (ipa != null) {

                    People person = ipa.getPerson();
                    if (person != null) {
                        String cFn = person.getFirstName();
                        String cLn = person.getLastName();

                        if (cFn != null && cLn != null
                                && cLn.trim().length() != 0 && cFn.trim().length() != 0) {

                            if (mode == MODE_UPDATE && person.getEntityId() != null && person.getEntityId() > 0) {
                                qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][0][interested_party_attributes][primary_entity_id]", person.getEntityId().toString()));

                            }

                            qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][interested_party_attributes][person_entity_attributes][person_attributes][last_name]", person.getLastName()));
                            qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][interested_party_attributes][person_entity_attributes][person_attributes][first_name]", person.getFirstName()));
                            ParticipationsContacts pcontacts = ccea.getParticipationsContacts();
                            if (pcontacts != null && pcontacts.getDispositionId() != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][participations_contact_attributes][disposition_id]", pcontacts.getDispositionId().toString()));
                            }
//@TODO             qParams.add(new BasicNameValuePair(etfn+"[contact_child_events_attributes]["+count+"][participations_contact_attributes][disposition_date]", pcontacts.));
                            if (pcontacts != null && pcontacts.getContactTypeId() != null) {
                                qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][participations_contact_attributes][contact_type_id]", pcontacts.getContactTypeId().toString()));
                            }

                            List<Telephones> telephonesList = ipa.getTelephones();
                            if (telephonesList != null) {
                                int tc = 0;
                                for (Telephones telephones : telephonesList) {

                                    qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][interested_party_attributes][person_entity_attributes][telephones_attributes][" + tc + "][entity_location_type_id]", telephones.getEntityLocationTypeId().toString()));
                                    qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][interested_party_attributes][person_entity_attributes][telephones_attributes][" + tc + "][area_code]", telephones.getAreaCode()));
                                    qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][interested_party_attributes][person_entity_attributes][telephones_attributes][" + tc + "][phone_number]", telephones.getPhoneNumber()));
                                    qParams.add(new BasicNameValuePair(etfn + "[contact_child_events_attributes][" + count + "][interested_party_attributes][person_entity_attributes][telephones_attributes][" + tc + "][extension]", telephones.getExtension()));
                                    tc++;
                                }
                            }
                        }

                    }
                }

                count++;
            }
        }
    }

    private void setReporting(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, int mode, int eventId, String etfn) throws Exception {

        if(th.getEventType().equals("ContactEvent"))
            return;

        // if this is an update and reporting agency is already set
        if (mode == MODE_UPDATE && eventId > 0) {
            if (TrisanoDaoImpl.hasReporter(eventId) && dontUpdateExistingReporter) {
                // we are done
                return;
            }
        }
        
        List<ReportingAgencyAttributes> raaList = th.getReportingAgencyAttributes();

        if (raaList != null && !raaList.isEmpty()) {
            for (ReportingAgencyAttributes raa : raaList) {
                TrisanoDaoImpl.findPlaceAttributes(raa, hm);
                Places rAgency = raa.getPlace();
                List<Telephones> rTelephones = raa.getTelephones();

                if (rAgency.getEntityId() != null && rAgency.getEntityId() > 0) {

                    if (mode == MODE_UPDATE) {
                        List<Participations> pList = raa.getParticipations();
                        if (pList != null && !pList.isEmpty()) {
                            Participations p = pList.get(0);
                            qParams.add(new BasicNameValuePair(etfn + "[reporting_agency_attributes][id]", p.getId().toString()));
                        }
                        qParams.add(new BasicNameValuePair(etfn + "[reporting_agency_attributes][secondary_entity_id]", rAgency.getEntityId().toString()));
                    }
//@TODO confirm this
                    qParams.add(new BasicNameValuePair(etfn + "[reporting_agency_attributes][secondary_entity_id]", rAgency.getEntityId().toString()));
                } else {
                    qParams.add(new BasicNameValuePair(etfn + "[reporting_agency_attributes][place_entity_attributes][place_attributes][name]", rAgency.getName()));

                    if (rTelephones != null) {
                        int count = 0;
                        for (Telephones t : rTelephones) {

                            qParams.add(new BasicNameValuePair(etfn + "[reporting_agency_attributes][place_entity_attributes][telephones_attributes][" + count + "][area_code]", t.getAreaCode()));
                            qParams.add(new BasicNameValuePair(etfn + "[reporting_agency_attributes][place_entity_attributes][telephones_attributes][" + count + "][phone_number]", t.getPhoneNumber()));
                            qParams.add(new BasicNameValuePair(etfn + "[reporting_agency_attributes][place_entity_attributes][telephones_attributes][" + count + "][extension]", t.getExtension()));
                            count++;
                        }

                    }

                }

            }

        }

        List<ReporterAttributes> raList = th.getReporterAttributes();
        if (raList != null && !raList.isEmpty()) {

            ReporterAttributes ra = raList.get(0);
            People reporter = ra.getReporter();

            if (reporter != null
                    && reporter.getLastName() != null
                    && reporter.getLastName().trim().length() > 0) {

                if (mode == MODE_UPDATE) {
                    List<Participations> pList = ra.getParticipations();
                    if (pList != null && !pList.isEmpty()) {
                        Participations p = pList.get(0);
                        qParams.add(new BasicNameValuePair(etfn + "[reporter_attributes][id]", p.getId().toString()));
                    }
                }

                if (reporter.getFirstName() != null && reporter.getFirstName().trim().length() > 0) {

                    qParams.add(new BasicNameValuePair(etfn + "[reporter_attributes][person_entity_attributes][person_attributes][first_name]", reporter.getFirstName()));
                }

                qParams.add(new BasicNameValuePair(etfn + "[reporter_attributes][person_entity_attributes][person_attributes][last_name]", reporter.getLastName()));

                List<Telephones> raTelephones = ra.getTelephones();
                if (raTelephones != null) {
                    int count = 0;
                    for (Telephones t : raTelephones) {

                        qParams.add(new BasicNameValuePair(etfn + "[reporter_attributes][person_entity_attributes][telephones_attributes]" + count + "][area_code]", t.getAreaCode()));
                        qParams.add(new BasicNameValuePair(etfn + "[reporter_attributes][person_entity_attributes][telephones_attributes][" + count + "][phone_number]", t.getPhoneNumber()));
                        qParams.add(new BasicNameValuePair(etfn + "[reporter_attributes][person_entity_attributes][telephones_attributes][" + count + "][extension]", t.getExtension()));
                        count++;
                    }

                }

            }

        }

        List<Events> eventList = th.getEvents();
        if (eventList != null && !eventList.isEmpty()) {
            Events event = eventList.get(0);
            if (event.getResultsReportedToClinicianDate() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[results_reported_to_clinician_date]", this.dateformatYYYYMMDD.format(event.getResultsReportedToClinicianDate())));
            }
            if (event.getFirstReportedPHdate() != null) {
                qParams.add(new BasicNameValuePair(etfn + "[first_reported_PH_date]", this.dateformatYYYYMMDD.format(event.getFirstReportedPHdate())));
            }
        }

    }

    private void setInvestigation(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm) throws Exception {
    }

    private void setNotes(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, String etfn) throws Exception {

        Notes notes = th.getNotes();
        if (notes != null) {
            qParams.add(new BasicNameValuePair(etfn + "[notes_attributes][1297373041][note]", notes.getNote()));
            qParams.add(new BasicNameValuePair(etfn + "[notes_attributes][1297373041][note_type]", notes.getNoteType()));
        }

    }

    private void setAdministrative(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, String eventType) throws Exception {

        List<Events> events = th.getEvents();
        if (events != null && !events.isEmpty()) {
            Events event = events.get(0);
            Integer lhdCaseStatus = event.getLhdCaseStatusId();
            Integer stateCaseStatus = event.getStateCaseStatusId();

            if (lhdCaseStatus != null && lhdCaseStatus > 0) {
                qParams.add(new BasicNameValuePair(eventType + "[lhd_case_status_id]", lhdCaseStatus.toString()));
            }
            if (stateCaseStatus != null && stateCaseStatus > 0) {
                qParams.add(new BasicNameValuePair(eventType + "[state_case_status_id]", stateCaseStatus.toString()));
            }

        }

    }

    // this function accesses the Trisano "Web API" to add a task to an existing morbidity_event
    private void createTask(HealthMessage hm) throws Exception {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        List<TrisanoHealth> thList = hm.getTrisanoHealth();
        for (TrisanoHealth th : thList) {

            List<Tasks> tList = th.getTasks();

            if (tList != null && !tList.isEmpty()) {

                for (Tasks t : tList) {

                    Integer eventId = t.getEventId();
                    if (eventId != null && eventId > 0) {

                        try {

                            URI uri = new URI(this.scheme, null, this.host, this.port, this.context + "/events/" + eventId + "/tasks", null, null);

                            HttpPost httpPost = new HttpPost(uri);

                            Header header = new BasicHeader(this.uid_header_name, hm.getUsername());
                            httpPost.setHeader(header);

                            List<NameValuePair> qparams = new ArrayList<>();

                            qparams.add(new BasicNameValuePair("task[event_id]", eventId.toString()));
                            qparams.add(new BasicNameValuePair("task[name]", t.getName()));
                            qparams.add(new BasicNameValuePair("task[notes]", t.getNotes()));
                            if (t.getCategoryId() != null) {
                                qparams.add(new BasicNameValuePair("task[category_id]", t.getCategoryId().toString()));
                            }
                            qparams.add(new BasicNameValuePair("task[priority]", t.getPriority()));
                            if (t.getDueDate() != null) {
                                qparams.add(new BasicNameValuePair("task[due_date]", dateformatYYYYMMDD.format(t.getDueDate())));
                            }
                            qparams.add(new BasicNameValuePair("task[repeating_interval]", t.getRepeatingInterval()));
                            if (t.getUntilDate() != null) {
                                qparams.add(new BasicNameValuePair("task[until_date]", dateformatYYYYMMDD.format(t.getUntilDate())));
                            }
                            if (t.getUserId() != null) {
                                qparams.add(new BasicNameValuePair("task[user_id]", t.getUserId().toString()));
                            }
                            HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
                            HttpEntity entity = response.getEntity();

                            if (entity != null) {
                                Float flen = new Float(entity.getContentLength());
                                InputStream is = entity.getContent();
                                byte[] ba = new byte[Math.round(flen)];
                                is.read(ba);
                                String out = new String(ba);

                                if (out.trim().startsWith("<html><body>You are being <a href")) {
                                    hm.addStatusMessage(new StatusMessage("createTask", Status.FAILURE_TRISANO_ERROR.toString(), "eventId=" + eventId));
                                    log.info("TASK ADDED [eventId=" + eventId + "]");
                                } else {
                                    hm.addStatusMessage(new StatusMessage("createTask", Status.FAILURE_TRISANO_ERROR.toString(), "Failure adding task [eventId=" + eventId + "]... \n"
                                            + uri + "\n" + response.getStatusLine() + "\n" + out));
                                    log.error("Failure adding task [eventId=" + eventId + "]... \n"
                                            + uri + "\n" + response.getStatusLine() + "\n" + out);
                                }

                            }
                            EntityUtils.consume(entity);

                        } catch (Exception e) {

                            log.error(e);

                        } finally {
                            // When HttpClient instance is no longer needed,
                            // shut down the connection manager to ensure
                            // immediate deallocation of all system resources
                            httpclient.getConnectionManager().shutdown();
                        }

                    }

                }

            }

        }

    }

    private Map<Integer, String> post(String uid, URI uri, List<NameValuePair> qparams) throws Exception {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        Map<Integer, String> out = new HashMap<>();
        try {

            HttpPost httpPost = new HttpPost(uri);
            Header header_uid = new BasicHeader(this.uid_header_name, uid);
            Header header_referrer = new BasicHeader("Referer", referringSite);
            httpPost.setHeader(header_uid);
            httpPost.setHeader(header_referrer);
            HttpResponse response = postToTrisano(httpPost, httpclient, qparams);
            HttpEntity entity = response.getEntity();

            if (entity != null) {

                Float flen = new Float(entity.getContentLength());
                InputStream is = entity.getContent();
                byte[] ba = new byte[Math.round(flen)];
                is.read(ba);
                out.put(RESPONSE_KEY, new String(ba));
                out.put(STATUS_KEY, response.getStatusLine().toString());
            }

            EntityUtils.consume(entity);

        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }

        return out;

    }

    private HttpResponse postToTrisano(HttpPost httpPost, DefaultHttpClient httpClient, List<NameValuePair> qparams)
            throws IOException, UnsupportedEncodingException {
        if (useApiKey) {
            qparams.add(new BasicNameValuePair("api_key", apiKey));
        }

        httpPost.setEntity(new UrlEncodedFormEntity(qparams, HTTP.UTF_8));
        return httpClient.execute(httpPost);
    }

    /**
     * Change event status to Accepted by LHD
     *
     * @param state accept -> Accepted by LHD reopen -> reopened by manager
     * @param uid
     * @param etfn
     * @param eventId
     * @return
     */
    private Map<Integer, String> setWorkflow(String uid, String etfn, Integer eventId, String eventType, String state) {

        Map<Integer, String> map = new HashMap<Integer, String>();
        try {
            
            // NOTE:  For the params, the morbidity event URI is used for this action for cmr and contact events
            if(etfn.equals("contact_event")) {
                etfn = "morbidity_event";
            }
            
            URI uri = new URI(this.scheme, null, this.host, this.port,
                    this.context + "/cmrs/" + eventId.toString() + "/state", null, null);

            HttpPost httpPost = new HttpPost(uri);

            List<NameValuePair> qparams = new ArrayList<>();

            // Step 1
            qparams.add(new BasicNameValuePair(etfn + "[workflow_action]", state));
            qparams.add(new BasicNameValuePair(etfn + "[note]", ""));
            qparams.add(new BasicNameValuePair(state, state));
            map = post(uid, uri, qparams);
        } catch (Exception ex) {
            log.error("Exception changing workflow state for event", ex);
        }
        return map;
    }
   
    Integer validInvestigatorIdForTask(Integer investigatorId, String jurisdiction) {

        String idRet = null;
        if (TrisanoDaoImpl.validInvestigator(investigatorId, jurisdiction)) {
            return investigatorId;
        } else if (jurisdiction != null && jurisdiction.trim().equals("SNHD-HIV-STD-TB")) {
            return props.getIntProperty("std_lead_id");
        }
        return props.getIntProperty("ooe_lead_id");
    }
    
    /**
     * Set the following information in the event from
     * the ESP data
     *
     * physician notes -> custom form additional_clinical (phys_note question)
     * ICD 10 diagnosis codes -> diag_code question
     * Pregnancy Status -> core field
     * Treatments -> core field
     */
    private void setEsp(List<NameValuePair> qParams, TrisanoHealth th, HealthMessage hm, String eventType, Integer eventId) throws Exception {
        Esp esp = th.getEsp();
                            
        if (esp != null) {
            // if this is an UPDATE operation, we have to add the person entity
            if(eventId != null) {
                HashMap<String, String> map = null;
                String sql = "SELECT id FROM participations WHERE type = 'InterestedParty' AND event_id="+eventId.toString();
                map = TrisanoDaoImpl.getSqlRow(sql);
                String participationId = map.get("id");
                
                // if we don't have a participationId for this event, we don't do any further processing because
                // trying to post the pregnancy status update and treatments with no interested party specified will
                // corrupt the event (delete the interestedParty participation)
                if (participationId == null || participationId.length() < 1) {
                    return;
                }
                qParams.add(new BasicNameValuePair(eventType + "[interested_party_attributes][id]", participationId));
            }
        
            String strValue = esp.getPregnancyStatus();
            if (strValue != null) {
                String id = "118"; // code for unknown
                if (strValue.toLowerCase().equals("positive")) {
                    id = "119";
                } else if (strValue.toLowerCase().equals("negative")) {
                    id = "120";
                }
                qParams.add(new BasicNameValuePair(eventType
                    + "[interested_party_attributes][risk_factor_attributes][pregnant_id]",
                    id));
            }
            List<Treatments> treatments = esp.getTreatments();
            if (treatments != null) {
                int count = 0;
                for (Treatments treatment : treatments) {
                    if (treatment != null) {
                        // the treatment has to already be in the system and associated with the condition for us to add this
                        Integer idTreatment = TrisanoDaoImpl.getTreatmentId(treatment.getName());
                        if (idTreatment > -1) {
                            // yes (119) for treatment given
                            qParams.add(new BasicNameValuePair(eventType
                                + "[interested_party_attributes][treatments_attributes][" + count + "][treatment_given_yn_id]",
                                "119"));
                            // treatment id from dropdown list
                            qParams.add(new BasicNameValuePair(eventType
                                + "[interested_party_attributes][treatments_attributes][" + count + "][treatment_id]",
                                idTreatment.toString()));
                            String start = treatment.getDateOfTreatment();
                            if (start != null && start.length() == 10) { // YYYY-MM-DD
                                qParams.add(new BasicNameValuePair(eventType
                                    + "[interested_party_attributes][treatments_attributes]["
                                    + count + "][treatment_date]", start));
                            }
                            String end = treatment.getTreatmentStopped();
                            if (end != null && end.length() == 10) { // YYYY-MM-DD
                                qParams.add(new BasicNameValuePair(eventType
                                    + "[interested_party_attributes][treatments_attributes]["
                                    + count + "][stop_treatment_date]", end));
                            }
                            count++;
                        }
                    }
                }
            }
        }
    }
    
    private void addEspCustomFormFields(String eventId, TrisanoHealth th) {
        
        // to add a custom repeating form answer
        // add an entry to investigator_form_sections (the id of this row is the repeater_form_object_id
        // in the answer table
        // the section_element_id column in the investigator_form_sections table
        // is the parent_id of the question_element in the form_element table
        // and add the answer to the answers table, linking it to the investigator_form_sections table
        
        Esp esp = th.getEsp();
        String sql;
        ResultSet rs = null;
        Integer questionId = -1;
        Integer parentId = -1;
        Integer repeaterId = -1;
        if(esp != null) {
            try {
                List<String> icd10Codes = esp.getIcd10Diagnoses();
                if(icd10Codes != null) {
                    Integer[] ids = getQuestionParentIds(eventId, "additional_clinical", "diag_code");
                    questionId = ids[0];
                    parentId = ids[1];

                    for (String icd10Code : icd10Codes) {
                        if (icd10Code != null && icd10Code.length() > 0) {
                            repeaterId = getNewRepeaterId(eventId, parentId);
                            insertRepeaterAnswer(eventId, questionId, repeaterId, icd10Code);
                        }
                    }
                }
                List<String> notes = esp.getPhysicianNotes();
                if (notes != null) {
                    Integer[] ids = getQuestionParentIds(eventId, "additional_clinical", "phys_note");
                    questionId = ids[0];
                    parentId = ids[1];
                    
                    for (String note : notes) {
                        if (note != null && note.length() > 0) {
                            repeaterId = getNewRepeaterId(eventId, parentId);
                            insertRepeaterAnswer(eventId, questionId, repeaterId, note);
                        }
                    }
                }
            } catch (Exception e) {
                log.error("Exception adding ESP custom form information: " + e.toString());
            } 
        }
    }
    
    Integer[] getQuestionParentIds(String eventId, String formName, String questionName) {
        Integer[] retValue = null;
        try {
            HashMap<String, String> map = null;
            retValue = new Integer[2];
            String sql = "SELECT q.id, fe.parent_id FROM questions q "
                + "INNER JOIN form_references fr ON fr.event_id = " + eventId + " "
                + "INNER JOIN forms f ON f.id = fr.form_id AND f.short_name = '" + formName + "' "
                + "INNER JOIN form_elements fe ON fe.form_id = f.id "
                + "WHERE q.short_name='" + questionName + "' AND q.form_element_id=fe.id";
            map = TrisanoDaoImpl.getSqlRow(sql);
            retValue[0] = Integer.parseInt(map.get("id"));
            retValue[1] = Integer.parseInt(map.get("parent_id"));
        } catch (Exception e) {
            log.error("Exception in getQuestionParentIds: " + e);
            retValue = null;
        } 
        return retValue;
    }
    
    Integer getNewRepeaterId(String eventId, Integer parentId) {
        Integer repeaterId = null;
        ResultSet rs = null;
        try {
            String sql = "INSERT INTO investigator_form_sections(event_id, section_element_id) "
                + "VALUES (" + eventId + "," + parentId + ")";
            if (TrisanoDaoImpl.insertUpdate(sql)) {
                HashMap<String, String> map = null;
                sql = "SELECT id FROM investigator_form_sections ORDER BY id DESC LIMIT 1";
                map = TrisanoDaoImpl.getSqlRow(sql);
                repeaterId = Integer.parseInt(map.get("id"));
            }
        } catch (Exception ex) {
            log.error("Exception in getNewRepeaterId: " + ex);
        }
        return repeaterId;
    }

    private void insertRepeaterAnswer(String eventId, Integer questionId, Integer repeaterId, String answer) {
        String sql = "INSERT INTO answers(event_id, question_id, created_at, updated_at, "
                + "repeater_form_object_id, repeater_form_object_type, text_answer) "
                + "VALUES(" + eventId + ", " + questionId + ", now(), now(), "
                + repeaterId + ", 'InvestigatorFormSection', '" + answer + "')";
        TrisanoDaoImpl.insertUpdate(sql);
    }
    
    private static String strNonNull(String str) {
        if (str == null) {
            return "";
        }
        return str;
    }

}
