package gov.utah.health.client.trisano;


import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.model.master.Health;
import gov.utah.health.model.trisano.TrisanoHealth;
import gov.utah.health.util.InputStreamKnownSizeBody;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

/**
 * @author UDOH
 */
public class TrisanoAttachmentClient {

    private static final Log log = LogFactory.getLog(TrisanoWebClientImpl.class);

    public static void attachFile(HealthMessage hm,
            String host, String scheme, int port, String uidHeaderName, String context) throws Exception {

        List<Health> mHealthList = hm.getHealth();
        List<TrisanoHealth> tHealthList = hm.getTrisanoHealth();
        Health mHealth = null;
        TrisanoHealth tHealth;
        Integer eventId = null;
        if (mHealthList != null && !mHealthList.isEmpty()) {
            mHealth = mHealthList.get(0);
        } else {
            hm.addStatusMessage(new StatusMessage("attachFile", Status.FAILURE_MISSING_DATA.toString(), "health missing"));                
            throw new Exception();
        }
        if (tHealthList != null && !tHealthList.isEmpty()) {
            tHealth = tHealthList.get(0);
            if (tHealth.hasEventId()) {
                eventId = tHealth.getEvents().get(0).getId();
            } else {
            hm.addStatusMessage(new StatusMessage("attachFile", Status.FAILURE_MISSING_DATA.toString(), "trisano_health.events[0].event.id missing"));                
                throw new Exception();

            }
        } else {
            hm.addStatusMessage(new StatusMessage("attachFile", Status.FAILURE_MISSING_DATA.toString(), "trisano_health.events[0].event.id missing"));                
            throw new Exception();
        }

        DefaultHttpClient httpclient = new DefaultHttpClient();
        ByteArrayOutputStream os = null;

       // LabReport labReport = new LabReport(mHealth);

        try {
       //     os = labReport.getPersonPdf();

        } catch (Exception e) {
            hm.addStatusMessage(new StatusMessage("attachFile", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));                
            throw e;
        }

        boolean localfile = false;
        if (localfile) {
//            try (FileOutputStream fo = new FileOutputStream(new File("C:\\Users\\Administrator\\Documents\\NetBeansProjects\\trunk\\sqla\\" + labReport.getFileNamePdf()))) {
//                os.writeTo(fo);
//            }
            os.close();

        } else {


            try {


                ///trisano/events/2/attachments
                String path = context + "/events/" + eventId + "/attachments";
                String referrer = "http://" + host + ":" + port + context + "/events/" + eventId + "/attachments/new";

              //  String fileName = labReport.getFileNamePdf();
                String mimeType = "application/pdf";

                URI uri = new URI(scheme, null, host, port, path, null, null);

                HttpPost httpPost = new HttpPost(uri);

                Header uidHeader = new BasicHeader(uidHeaderName, hm.getUsername());
                httpPost.setHeader(uidHeader);

                MultipartEntity mEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                byte[] bytes = os.toByteArray();

//                InputStreamBody isBody = new InputStreamKnownSizeBody(new ByteArrayInputStream(bytes),
//                        bytes.length, mimeType, fileName);



             //   mEntity.addPart("attachment[uploaded_data]", isBody);

                mEntity.addPart("attachment[event_id]",
                        new StringBody(Integer.toString(eventId), "text/plain",
                        Charset.forName("UTF-8")));
                mEntity.addPart("attachment[category]",
                        new StringBody("lab", "text/plain",
                        Charset.forName("UTF-8")));




                httpPost.setEntity(mEntity);

                Header header_referrer = new BasicHeader("Referer", referrer);
                httpPost.setHeader(header_referrer);



                HttpResponse response = httpclient.execute(httpPost);
                HttpEntity entity = response.getEntity();

                if (entity != null) {
                    Float flen = new Float(entity.getContentLength());

                    InputStream is = entity.getContent();
                    byte[] ba = new byte[Math.round(flen)];
                    is.read(ba);
                    String out = new String(ba);

                    System.out.print(response.getStatusLine() + "\n" + out);

                }
                EntityUtils.consume(entity);

            } catch (Exception e) {

                log.error(e);

            } finally {
                // When HttpClient instance is no longer needed,
                // shut down the connection manager to ensure
                // immediate deallocation of all system resources
                httpclient.getConnectionManager().shutdown();
                os.close();
            }
        }

            hm.addStatusMessage(new StatusMessage("attachFile", Status.SUCCESS.toString(), null));                
    }
}
