package gov.utah.health.client.master;

import java.net.URLDecoder;
import java.net.URLEncoder;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.WebServiceRef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author UDOH
 */
public class MasterServiceClient {

    @WebServiceRef(wsdlLocation = "http://hlldcpapp:8080/ELRService/ElrService?wsdl")
   
    //@Resource
    protected WebServiceContext context;
    private static final Log log = LogFactory.getLog(MasterServiceClient.class);

    public static String getMaster(java.lang.String masterId) {

        String response = "unset";
        try {

            Integer mId = Integer.parseInt(masterId);
            
            gov.utah.health.client.master.MasterWebService service = new gov.utah.health.client.master.MasterWebService();
            gov.utah.health.client.master.MasterWeb elr = service.getMasterWebPort();
            
            response = URLDecoder.decode(elr.getMasterXml(mId, 1), "UTF-8");

        } catch (Exception e) {

            log.error(e);

        }

        return response;
    }
}
