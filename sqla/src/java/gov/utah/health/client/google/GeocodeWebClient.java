package gov.utah.health.client.google;

import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.model.trisano.Addresses;
import gov.utah.health.model.trisano.TrisanoHealth;
import gov.utah.health.util.AppProperties;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author UDOH
 */
public class GeocodeWebClient {

    private static final Log log = LogFactory.getLog(GeocodeWebClient.class);

    public String getGeocode(HealthMessage hm) {
        String geocodeData = null;;
// TODO Jay commented out until a geocoding service is available
        try {
            TrisanoHealth th = hm.getTrisanoHealth().get(0);
            List<Addresses> addressList = th.getAllAddresses();

            if (addressList != null) {
                for (Addresses addy : addressList) {

                    if (addy.getLatitude() == null || addy.getLongitude() == null) {
                        String streetName = addy.getStreetName();
                        String city = addy.getCity();
                        // String state = addy.getStateId();
                        String zip = addy.getPostalCode();
                        geocodeData = this.getGeocode(streetName, city, "", zip);
                        //this.convertGeocodeData(addy, geocodeData);
                    }
                }
            }
            hm.addStatusMessage(new StatusMessage("getGeocode", Status.SUCCESS.toString(), null));

        } catch (Exception e) {
            hm.addStatusMessage(new StatusMessage("getGeocode", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }
        return geocodeData;

    }

    private void convertGeocodeData(Addresses address, String geocodeData) throws Exception {
        // this method was written for xml structure returned by google
        // this method has been bypassed and the calling service method is now
        // retruning the json structure from smarty streets to the client.
// TODO Jay commented out until a geocoding service is available
        
        try {

            InputStream is = new ByteArrayInputStream(geocodeData.getBytes());
            Document geoXml = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
            //is.close();
            XPath xpath = XPathFactory.newInstance().newXPath();
            NodeList resultNodeList = null;

// a) obtain the formatted_address field for every result
            resultNodeList = (NodeList) xpath.evaluate("/GeocodeResponse/result/formatted_address", geoXml, XPathConstants.NODESET);
            for (int i = 0; i < resultNodeList.getLength(); ++i) {
                address.setGoogleAddress(resultNodeList.item(i).getTextContent());
                log.info(resultNodeList.item(i).getTextContent());
            }

// b) extract the locality for the first result
            resultNodeList = (NodeList) xpath.evaluate("/GeocodeResponse/result[1]/address_component[type/text()='locality']/long_name", geoXml, XPathConstants.NODESET);
            for (int i = 0; i < resultNodeList.getLength(); ++i) {
                log.info(resultNodeList.item(i).getTextContent());
            }

            // c) extract the coordinates of the first result
            resultNodeList = (NodeList) xpath.evaluate("/GeocodeResponse/result[1]/geometry/location/*", geoXml, XPathConstants.NODESET);
            Double lat = Double.NaN;
            Double lng = Double.NaN;
            for (int i = 0; i < resultNodeList.getLength(); ++i) {
                Node node = resultNodeList.item(i);
                if ("lat".equals(node.getNodeName())) {
                    lat = Double.parseDouble(node.getTextContent());
                    if (!Double.isNaN(lat)) {
                        address.setLatitude(new Double(lat));
                    }
                }
                if ("lng".equals(node.getNodeName())) {
                    lng = Double.parseDouble(node.getTextContent());
                    if (!Double.isNaN(lng)) {
                        address.setLongitude(new Double(lng));
                    }
                }
            }
            log.info("lat/lng=" + lat + "," + lng);

            // c) extract the coordinates of the first result
            resultNodeList = (NodeList) xpath.evaluate("/GeocodeResponse/result[1]/address_component[type/text() = 'administrative_area_level_1']/country[short_name/text() = 'US']/*", geoXml, XPathConstants.NODESET);
            Double adlat = Double.NaN;
            Double adlng = Double.NaN;
            for (int i = 0; i < resultNodeList.getLength(); ++i) {
                Node node = resultNodeList.item(i);
                if ("lat".equals(node.getNodeName())) {
                    adlat = Double.parseDouble(node.getTextContent());
                }
                if ("lng".equals(node.getNodeName())) {
                    adlng = Double.parseDouble(node.getTextContent());
                }
            }
            log.info("lat/lng=" + adlat + "," + adlng);

        } catch (Exception e) {
            log.error(e);
            throw e;
        }

    }

    private String getGeocode(String streetName, String city, String state, String zip) throws Exception {

        String geocodeResponse = null;
// TODO Jay commented out until a geocoding service is available

        DefaultHttpClient httpclient = new DefaultHttpClient();

        try {
            //http://hllcnedssapp1:8080/geo/find?street_name=9000+S+1700+W&zip=84088
            //http://maps.googleapis.com/maps/api/geocode/xml?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false
            HttpGet httpGet = new HttpGet("http://hllcnedssapp1:8080/geo/find?street_name="
                    + URLEncoder.encode(streetName, "UTF-8")
                    + "&city=" + city
                    + "&state=" + state
                    + "&zip=" + zip);

            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            StringBuilder out = new StringBuilder();



            if (entity != null) {

                BufferedReader rd = null;
                rd = new BufferedReader(new InputStreamReader(entity.getContent()));
                String line = null;
                while ((line = rd.readLine()) != null) {
                    out.append(line);
                }
            }

            if (response.getStatusLine().toString().indexOf("200 OK") > 0) {
                geocodeResponse = out.toString();
            } else {
                log.error("Geocode failure.\n" + geocodeResponse);
                throw new Exception("Failure accessing maps.googleapis.com");
            }


        } catch (Exception e) {

            log.error(e);
            throw e;


        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }

        return geocodeResponse;

    }
}
