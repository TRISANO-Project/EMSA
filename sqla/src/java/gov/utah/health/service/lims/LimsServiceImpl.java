package gov.utah.health.service.lims;

import gov.utah.health.data.lims.LimsDaoImpl;
import gov.utah.health.model.HealthMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service level implementation class. Calls to this service are configured in
 * the applicationContext.xml file.
 *
 * @author UDOH
 */
@Service
public class LimsServiceImpl {

    @Autowired
    private LimsDaoImpl limsDaoImpl;

    public LimsDaoImpl getLimsDaoImpl() {
        return limsDaoImpl;
    }

    public void setLimsDaoImpl(LimsDaoImpl limsDaoImpl) {
        this.limsDaoImpl = limsDaoImpl;
    }

    public HealthMessage getUdohClinicalResults(HealthMessage healthMessage) {

        limsDaoImpl.getUdohClinicalResults(healthMessage);
        return healthMessage;

    }
}
