package gov.utah.health.service.master;

import gov.utah.health.client.master.MasterServiceClient;
import org.springframework.stereotype.Service;

/**
 * Service level implementation class. Calls to this service are configured in
 * the applicationContext.xml file.
 *
 * @author UDOH
 */
@Service
public class MasterServiceImpl implements MasterService {


    @Override
    public String getMaster(String masterId) {

        return MasterServiceClient.getMaster(masterId);

    }
}
