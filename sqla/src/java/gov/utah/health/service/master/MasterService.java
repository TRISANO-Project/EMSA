package gov.utah.health.service.master;

/**
 *  Service level interface for general operations against data sources.
 *
 * @author UDOH
 */
public interface MasterService {

    public String getMaster(String masterId);
}
