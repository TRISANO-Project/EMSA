package gov.utah.health.service.dashboard;

import gov.utah.health.model.HealthMessage;

/**
 *  Service level interface for general operations against data sources.
 *
 * @author UDOH
 */
public interface DashboardService {

    public HealthMessage getReport(HealthMessage healthMessage);
}
