package gov.utah.health.service.dashboard;

import gov.utah.health.data.emsa.EmsaDaoImpl;
import gov.utah.health.data.trisano.TrisanoDaoImpl;
import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.dashboard.BucketReport;
import gov.utah.health.model.dashboard.Dashboard;
import gov.utah.health.model.trisano.Diseases;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service level implementation class. Calls to this service are configured in
 * the applicationContext.xml file.
 *
 * @author UDOH
 */
@Service
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    private EmsaDaoImpl emsaDao;
    @Autowired
    private TrisanoDaoImpl trisanoDao;

    public EmsaDaoImpl getEmsaDao() {
        return emsaDao;
    }

    public void setEmsaDao(EmsaDaoImpl emsaDao) {
        this.emsaDao = emsaDao;
    }

    public TrisanoDaoImpl getTrisanoDao() {
        return trisanoDao;
    }

    public void setTrisanoDao(TrisanoDaoImpl trisanoDao) {
        this.trisanoDao = trisanoDao;
    }

    @Override
    public HealthMessage getReport(HealthMessage healthMessage) {

        Dashboard d = healthMessage.getDashboard();

        if (d != null) {

            BucketReport br = d.getBucketReport();
            if (br != null) {
                emsaDao.getBucketReport(healthMessage, br);
                
                if(br.getBucketCounts() != null){}
                trisanoDao.getDiseases(healthMessage);
                List<Diseases> diseases = healthMessage.getTrisanoHealth().get(0).getDiseases();
                for(Diseases disease:diseases){
                    br.addDisease(disease.getDiseaseName());
                }
                healthMessage.setTrisanoHealth(null);
            }
            
        }

        return healthMessage;

    }
}
