package gov.utah.health.service.trisano;

import gov.utah.health.client.google.GeocodeWebClient;
import gov.utah.health.client.trisano.TrisanoWebClient;
import gov.utah.health.data.trisano.TrisanoDao;
import gov.utah.health.model.HealthMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service level implementation class. Calls to this service are configured in
 * the applicationContext.xml file.
 *
 * @author UDOH
 */
@Service
public class TrisanoServiceImpl implements TrisanoService {

    @Autowired
    private TrisanoDao trisanoDao;
    @Autowired
    private TrisanoWebClient trisanoWeb;
    @Autowired
    private GeocodeWebClient geocodeWeb;

    public GeocodeWebClient getGeocodeWeb() {
        return geocodeWeb;
    }

    public void setGeocodeWeb(GeocodeWebClient geocodeWeb) {
        this.geocodeWeb = geocodeWeb;
    }

    public TrisanoDao getTrisanoDao() {
        return trisanoDao;
    }

    public void setTrisanoDao(TrisanoDao trisanoDao) {
        this.trisanoDao = trisanoDao;
    }

    public TrisanoWebClient getTrisanoWeb() {
        return trisanoWeb;
    }

    public void setTrisanoWeb(TrisanoWebClient trisanoWeb) {
        this.trisanoWeb = trisanoWeb;
    }

    @Override
    public HealthMessage searchPerson(HealthMessage healthMessage) {

        trisanoDao.searchPerson(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage findPerson(HealthMessage healthMessage) {

        trisanoDao.findPerson(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage findEvent(HealthMessage healthMessage) {

        trisanoDao.findEvent(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage addCmr(HealthMessage healthMessage) {

        trisanoWeb.addCmr(healthMessage);
// TODO Jay        trisanoDao.addAttachments(healthMessage);
        trisanoDao.setLabResults(healthMessage);
        trisanoDao.updateWorkflowState(healthMessage);
        trisanoDao.addAnswers(healthMessage);

        return healthMessage;

    }

    @Override
    public HealthMessage updateCmr(HealthMessage healthMessage) {

        trisanoWeb.updateCmr(healthMessage);
        if (healthMessage.getFirstTrisanoHealth() != null
                && !healthMessage.getFirstTrisanoHealth().hasJurisdiction()) {
            trisanoDao.getJurisdiction(healthMessage);

        }
// TODO Jay       trisanoDao.addAttachments(healthMessage);
        trisanoDao.setLabResults(healthMessage);
        trisanoDao.updateWorkflowState(healthMessage);
        trisanoDao.addAnswers(healthMessage);

        return healthMessage;

    }

    @Override
    public HealthMessage deepCopyCmr(HealthMessage healthMessage) {
        trisanoWeb.deepCopyCmr(healthMessage);
        return healthMessage;
    }

    @Override
    public HealthMessage findId(HealthMessage healthMessage) {
        trisanoDao.findId(healthMessage);
        return healthMessage;
    }

    @Override
    public HealthMessage findQuestions(HealthMessage healthMessage) {
        trisanoDao.findQuestions(healthMessage);
        return healthMessage;
    }

    @Override
    public HealthMessage findAnswers(HealthMessage healthMessage) {
        trisanoDao.findAnswers(healthMessage);
        return healthMessage;
    }

    @Override
    public HealthMessage getTableData(HealthMessage healthMessage) {

        trisanoDao.getTableData(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getHealthDataExample(HealthMessage healthMessage) {

        trisanoDao.getHealthDataExample(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getCaseStatusReport(HealthMessage healthMessage) {

        trisanoDao.getCaseStatusReport(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getDiseaseReport(HealthMessage healthMessage) {

        trisanoDao.getDiseaseReport(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getUserRoles(HealthMessage healthMessage) {

        trisanoDao.getUserRoles(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getUsers(HealthMessage healthMessage) {

        trisanoDao.getUsers(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getRoles(HealthMessage healthMessage) {

        trisanoDao.getRoles(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage attachFile(HealthMessage healthMessage) {

        trisanoWeb.attachFile(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage addAnswers(HealthMessage healthMessage) {

        trisanoDao.addAnswers(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage addAttachments(HealthMessage healthMessage) {

        trisanoDao.addAttachments(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage addTask(HealthMessage healthMessage) {

        trisanoWeb.addTask(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage addNote(HealthMessage healthMessage) {

        trisanoWeb.addNote(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage exportTreatmentNotes(HealthMessage healthMessage) {

        trisanoDao.exportTreatmentNotes(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage exportForms(HealthMessage healthMessage) {

        trisanoDao.exportForms(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage findForms(HealthMessage healthMessage) {

        trisanoDao.findForms(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getDiseaseForms(HealthMessage healthMessage) {

        trisanoDao.getDiseaseForms(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage testAccess(HealthMessage healthMessage) {

        trisanoWeb.testAccess(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getCareTimeReport(HealthMessage healthMessage) {

        trisanoDao.getCareTimeReport(healthMessage);
        return healthMessage;

    }

    @Override
    public HealthMessage getCdcExport(HealthMessage healthMessage) {

        trisanoDao.getCdcExport(healthMessage);
        return healthMessage;

    }

    @Override
    public String getGeocode(HealthMessage healthMessage) {

        String geoData = geocodeWeb.getGeocode(healthMessage);
        return geoData;

    }
}
