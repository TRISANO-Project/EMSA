package gov.utah.health.service.trisano;

import gov.utah.health.model.HealthMessage;

/**
 *  Service level interface for general operations against data sources.
 *
 * @author UDOH
 */
public interface TrisanoService {

    public HealthMessage searchPerson(HealthMessage healthMessage);
    public HealthMessage findPerson(HealthMessage healthMessage);
    public HealthMessage findEvent(HealthMessage healthMessage);
    public HealthMessage addCmr(HealthMessage healthMessage);
    public HealthMessage updateCmr(HealthMessage healthMessage);
    public HealthMessage deepCopyCmr(HealthMessage healthMessage);
    public HealthMessage findId(HealthMessage healthMessage);
    public HealthMessage findQuestions(HealthMessage healthMessage);
    public HealthMessage findAnswers(HealthMessage healthMessage);
    public HealthMessage getTableData(HealthMessage healthMessage);
    public HealthMessage getHealthDataExample(HealthMessage healthMessage);
    public HealthMessage getCaseStatusReport(HealthMessage healthMessage);
    public HealthMessage getDiseaseReport(HealthMessage healthMessage);
    public HealthMessage getUserRoles(HealthMessage healthMessage);
    public HealthMessage getUsers(HealthMessage healthMessage);
    public HealthMessage getRoles(HealthMessage healthMessage);
    public HealthMessage addAnswers(HealthMessage healthMessage);
    public HealthMessage addAttachments(HealthMessage healthMessage);
    public HealthMessage addTask(HealthMessage healthMessage);
    public HealthMessage addNote(HealthMessage healthMessage);
    public HealthMessage attachFile(HealthMessage healthMessage);
    public HealthMessage exportTreatmentNotes(HealthMessage healthMessage);
    public HealthMessage exportForms(HealthMessage healthMessage);
    public HealthMessage findForms(HealthMessage healthMessage);
    public HealthMessage getDiseaseForms(HealthMessage healthMessage);
    public HealthMessage testAccess(HealthMessage healthMessage);
    public HealthMessage getCareTimeReport(HealthMessage healthMessage);
    public HealthMessage getCdcExport(HealthMessage healthMessage);
    public String getGeocode(HealthMessage healthMessage);
}
