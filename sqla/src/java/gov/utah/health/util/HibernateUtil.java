package gov.utah.health.util;

import gov.utah.health.model.trisano.Addresses;
import gov.utah.health.model.trisano.Answers;
import gov.utah.health.model.trisano.Attachments;
import gov.utah.health.model.trisano.Codes;
import gov.utah.health.model.trisano.CommonTestTypes;
import gov.utah.health.model.trisano.DiseaseEvents;
import gov.utah.health.model.trisano.Diseases;
import gov.utah.health.model.trisano.DiseasesForms;
import gov.utah.health.model.trisano.DiseasesFormsPK;
import gov.utah.health.model.trisano.EmailAddresses;
import gov.utah.health.model.trisano.Entities;
import gov.utah.health.model.trisano.Events;
import gov.utah.health.model.trisano.ExternalCodes;
import gov.utah.health.model.trisano.FormElements;
import gov.utah.health.model.trisano.FormReferences;
import gov.utah.health.model.trisano.Forms;
import gov.utah.health.model.trisano.HospitalsParticipations;
import gov.utah.health.model.trisano.LabResults;
import gov.utah.health.model.trisano.Notes;
import gov.utah.health.model.trisano.Organisms;
import gov.utah.health.model.trisano.Participations;
import gov.utah.health.model.trisano.ParticipationsContacts;
import gov.utah.health.model.trisano.ParticipationsEncounters;
import gov.utah.health.model.trisano.ParticipationsRiskFactors;
import gov.utah.health.model.trisano.ParticipationsTreatments;
import gov.utah.health.model.trisano.People;
import gov.utah.health.model.trisano.PeopleRaces;
import gov.utah.health.model.trisano.PeopleRacesPK;
import gov.utah.health.model.trisano.Places;
import gov.utah.health.model.trisano.PlacesTypes;
import gov.utah.health.model.trisano.PlacesTypesPK;
import gov.utah.health.model.trisano.Privileges;
import gov.utah.health.model.trisano.PrivilegesRoles;
import gov.utah.health.model.trisano.Questions;
import gov.utah.health.model.trisano.RoleMemberships;
import gov.utah.health.model.trisano.Roles;
import gov.utah.health.model.trisano.Telephones;
import gov.utah.health.model.trisano.Treatments;
import gov.utah.health.model.trisano.Users;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * Utility class for managing hibernate database session factories.
 *
 * @author UDOH
 */
public class HibernateUtil {

    private static final Map<String, SessionFactory> sessionFactoryMap = buildSessionFactory();
    public static final String TRISANO_SESSION = "TRISANO";
    public static final String EHARS_SESSION = "EHARS";

    private static Map<String, SessionFactory> buildSessionFactory() {
        try {
            Map<String, SessionFactory> map = new HashMap<>();

            SessionFactory tSession =
                    new AnnotationConfiguration()
                    .addAnnotatedClass(Attachments.class)
                    .addAnnotatedClass(Addresses.class)
                    .addAnnotatedClass(Answers.class)
                    .addAnnotatedClass(DiseaseEvents.class)
                    .addAnnotatedClass(Diseases.class)
                    .addAnnotatedClass(DiseasesForms.class)
                    .addAnnotatedClass(DiseasesFormsPK.class)
                    .addAnnotatedClass(Organisms.class)
                    .addAnnotatedClass(EmailAddresses.class)
                    .addAnnotatedClass(Entities.class)
                    .addAnnotatedClass(Events.class)
                    .addAnnotatedClass(Forms.class)
                    .addAnnotatedClass(FormReferences.class)
                    .addAnnotatedClass(FormElements.class)
                    .addAnnotatedClass(Notes.class)
                    .addAnnotatedClass(Participations.class)
                    .addAnnotatedClass(ParticipationsEncounters.class)
                    .addAnnotatedClass(People.class)
                    .addAnnotatedClass(PeopleRaces.class)
                    .addAnnotatedClass(PeopleRacesPK.class)
                    .addAnnotatedClass(Places.class)
                    .addAnnotatedClass(PlacesTypes.class)
                    .addAnnotatedClass(PlacesTypesPK.class)
                    .addAnnotatedClass(Questions.class)
                    .addAnnotatedClass(Telephones.class)
                    .addAnnotatedClass(LabResults.class)
                    .addAnnotatedClass(Codes.class)
                    .addAnnotatedClass(CommonTestTypes.class)
                    .addAnnotatedClass(ExternalCodes.class)
                    .addAnnotatedClass(ParticipationsContacts.class)
                    .addAnnotatedClass(HospitalsParticipations.class)
                    .addAnnotatedClass(Users.class)
                    .addAnnotatedClass(RoleMemberships.class)
                    .addAnnotatedClass(Roles.class)
                    .addAnnotatedClass(ParticipationsRiskFactors.class)
                    .addAnnotatedClass(ParticipationsTreatments.class)
                    .addAnnotatedClass(Treatments.class)
                    .addAnnotatedClass(Privileges.class)
                    .addAnnotatedClass(PrivilegesRoles.class)                    
                    .configure("t_hibernate.cfg.xml").buildSessionFactory();

            map.put(TRISANO_SESSION, tSession);

          //  SessionFactory eSession =
          //          new AnnotationConfiguration()
          //          .addAnnotatedClass(Person.class)
          //          .configure("e_hibernate.cfg.xml").buildSessionFactory();

          //  map.put(EHARS_SESSION, eSession);

            return map;

        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory(String key) {
        return sessionFactoryMap.get(key);
    }
}
