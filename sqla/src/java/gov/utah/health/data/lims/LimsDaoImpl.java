package gov.utah.health.data.lims;

import gov.utah.health.model.Status;
import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.StatusMessage;

import gov.utah.health.model.lims.LimsHealth;
import gov.utah.health.model.lims.UdohClinicalResults;
import gov.utah.health.model.lims.UdohClinicalResultsReport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides implementation for data access to lims
 *
 * @author UDOH
 *
 */
@Repository
public class LimsDaoImpl {

    private static final Log log = LogFactory.getLog(LimsDaoImpl.class);
    private String url = "jdbc:oracle:thin:@itdb28spr.dts.utah.gov:1521:lpd3";
    private String username = "HL_UDOH_CLINICAL";
    private String password = "ud0hr35ult5";

    /**
     * *
     * Performs an exact match lookup based on attributes passed.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * LimsHeahth object with a list of UdohClinicalResultsReport data set as
     * search criteria. The HealthMessage parameter is modified to include all
     * result data. Return data includes data for all of the matching
     * limsp.udoh_clinical_results records. If errors or exceptions take place
     * information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    public void getUdohClinicalResults(HealthMessage hm) {

        List<LimsHealth> lhList = hm.getLimsHealth();
        if (lhList != null && !lhList.isEmpty()) {

            ResultSet rs = null;
            Connection con = null;
            PreparedStatement ps = null;

            try {

                oracle.jdbc.pool.OracleDataSource ds = new oracle.jdbc.pool.OracleDataSource();
                ds.setDriverType("thin");
                ds.setServerName("itdb28spr.dts.utah.gov");
                ds.setPortNumber(1521);
                ds.setDatabaseName("lpd3");
                ds.setUser(username);
                ds.setPassword(password);
                con = ds.getConnection();

                for (LimsHealth lh : lhList) {

                    List<UdohClinicalResultsReport> crrList = lh.getUdohClinicalResultsReport();

                    if (crrList != null && !crrList.isEmpty()) {
                        for (UdohClinicalResultsReport crr : crrList) {
                            List<UdohClinicalResults> foundCrList = new ArrayList<UdohClinicalResults>();
                            if (crr.getTestReleasedDateStart() == null
                                    || crr.getTestReleasedDateEnd() == null) {
                                throw new Exception("Both testReleasedDateStart and testReleasedDateEnd are required.");
                            }
                            Date start = new Date(crr.getTestReleasedDateStart().getTime());
                            Date end = new Date(crr.getTestReleasedDateEnd().getTime());
                            Integer limit = crr.getLimit();
                            String orderBy = crr.getOrderBy();
                            String ascDesc = crr.getAscDesc();
                            String lName = crr.getPatientLastName();
                            String fName = crr.getPatientFirstName();
                            String gender = crr.getPatientGender();
                            Date dob = null;
                            if (crr.getPatientDob() != null) {
                                dob = new Date(crr.getPatientDob().getTime());
                            }
                            String customer = crr.getCustomer();
                            String customerId = crr.getCustomerId();
                            String resultName = crr.getResultName();
                            String result = crr.getResult();
                            String resultReportable = crr.getResultReportable();
                            String analysis = crr.getAnalysis();
                            String analysisReportedName = crr.getAnalysisReportedName();
                            String orderNum = crr.getOrderNum();
                            String specimenSource = crr.getSpecimenSource();
                            String stateOfOrig = crr.getStateOfOrig();
                            String testStatus = crr.getTestStatus();

                            Date sampledDate = null;
                            if (crr.getSampledDate() != null) {
                                sampledDate = new Date(crr.getSampledDate().getTime());
                            }

                            try {

                                StringBuilder qry =
                                        new StringBuilder("SELECT * FROM limsp.udoh_clinical_results ");
                                qry.append("WHERE test_released_date >= ? ");
                                qry.append("AND test_released_date <= ? ");

                                if (lName != null && lName.trim().length() > 0) {
                                    qry.append("AND patient_last_name = ? ");
                                }
                                if (fName != null && fName.trim().length() > 0) {
                                    qry.append("AND patient_first_name = ? ");
                                }
                                if (gender != null && gender.trim().length() > 0) {
                                    qry.append("AND patient_gender = ? ");
                                }
                                if (dob != null) {
                                    qry.append("AND patient_dob = ? ");
                                }
                                if (customer != null && customer.trim().length() > 0) {
                                    qry.append("AND customer = ? ");
                                }
                                if (customerId != null && customerId.trim().length() > 0) {
                                    qry.append("AND customer_id = ? ");
                                }
                                if (resultName != null && resultName.trim().length() > 0) {
                                    qry.append("AND result_name = ? ");
                                }
                                if (result != null && result.trim().length() > 0) {
                                    qry.append("AND result = ? ");
                                }
                                if (resultReportable != null && resultReportable.trim().length() > 0) {
                                    qry.append("AND result_reportable = ? ");
                                }
                                if (analysis != null && analysis.trim().length() > 0) {
                                    qry.append("AND analysis = ? ");
                                }
                                if (analysisReportedName != null && analysisReportedName.trim().length() > 0) {
                                    qry.append("AND analysis_reported_name = ? ");
                                }
                                if (orderNum != null && orderNum.trim().length() > 0) {
                                    qry.append("AND order_num = ? ");
                                }
                                if (specimenSource != null && specimenSource.trim().length() > 0) {
                                    qry.append("AND specimen_source = ? ");
                                }
                                if (stateOfOrig != null && stateOfOrig.trim().length() > 0) {
                                    qry.append("AND state_of_orig = ? ");
                                }
                                if (testStatus != null && testStatus.trim().length() > 0) {
                                    qry.append("AND test_status = ? ");
                                }



                                if (orderBy != null && orderBy.trim().length() > 0) {
                                    qry.append("ORDER BY ");
                                    qry.append(orderBy);
                                    if (ascDesc != null) {
                                        qry.append(" ");
                                        qry.append(ascDesc);
                                    }
                                }
                                int i = 1;
                                ps = con.prepareStatement(qry.toString());
                                ps.setTimestamp(i++, new Timestamp(start.getTime()));
                                ps.setTimestamp(i++, new Timestamp(end.getTime()));
                                if (lName != null && lName.trim().length() > 0) {
                                    ps.setString(i++, lName.trim());
                                }
                                if (fName != null && fName.trim().length() > 0) {
                                    ps.setString(i++, fName.trim());
                                }
                                if (gender != null && gender.trim().length() > 0) {
                                    ps.setString(i++, gender.trim());
                                }
                                if (dob != null) {
                                    ps.setTimestamp(i++, new Timestamp(dob.getTime()));
                                }

                                if (customer != null && customer.trim().length() > 0) {
                                    ps.setString(i++, customer.trim());
                                }
                                if (customerId != null && customerId.trim().length() > 0) {
                                    ps.setString(i++, customerId.trim());
                                }
                                if (resultName != null && resultName.trim().length() > 0) {
                                    ps.setString(i++, resultName.trim());
                                }
                                if (result != null && result.trim().length() > 0) {
                                    ps.setString(i++, result.trim());
                                }
                                if (resultReportable != null && resultReportable.trim().length() > 0) {
                                    ps.setString(i++, resultReportable.trim());
                                }
                                if (analysis != null && analysis.trim().length() > 0) {
                                    ps.setString(i++, analysis.trim());
                                }
                                if (analysisReportedName != null && analysisReportedName.trim().length() > 0) {
                                    ps.setString(i++, analysisReportedName.trim());
                                }
                                if (orderNum != null && orderNum.trim().length() > 0) {
                                    ps.setString(i++, orderNum.trim());
                                }
                                if (specimenSource != null && specimenSource.trim().length() > 0) {
                                    ps.setString(i++, specimenSource.trim());
                                }
                                if (stateOfOrig != null && stateOfOrig.trim().length() > 0) {
                                    ps.setString(i++, stateOfOrig.trim());
                                }
                                if (testStatus != null && testStatus.trim().length() > 0) {
                                    ps.setString(i++, testStatus.trim());
                                }


                                rs = ps.executeQuery();

                                while (rs.next()) {

                                    UdohClinicalResults foundCr = new UdohClinicalResults();
                                    foundCr.setAnalysis(rs.getString("analysis"));
                                    foundCr.setAnalysisReportedName(rs.getString("analysis_reported_name"));
                                    foundCr.setCustomer(rs.getString("customer"));
                                    foundCr.setCustomerId(rs.getString("customer_id"));
                                    foundCr.setOrderNum(rs.getString("order_num"));
                                    foundCr.setPatientAge(rs.getString("patient_age"));
                                    foundCr.setPatientDob(rs.getDate("patient_dob"));
                                    foundCr.setPatientExtraId(rs.getString("patient_extra_id"));
                                    foundCr.setPatientFirstName(rs.getString("patient_first_name"));
                                    foundCr.setPatientGender(rs.getString("patient_gender"));
                                    foundCr.setPatientLastName(rs.getString("patient_last_name"));
                                    foundCr.setPatientMidName(rs.getString("patient_mid_name"));
                                    foundCr.setResult(rs.getString("result"));
                                    foundCr.setResultLong(rs.getString("result_long"));
                                    foundCr.setResultName(rs.getString("result_name"));
                                    foundCr.setResultReportable(rs.getString("result_reportable"));
                                    foundCr.setSampleNumber(rs.getFloat("sample_number"));
                                    foundCr.setSampleRecdDate(rs.getDate("sample_recd_date"));
                                    foundCr.setSampledDate(rs.getDate("sampled_date"));
                                    foundCr.setSpecimenSource(rs.getString("specimen_source"));
                                    foundCr.setStateOfOrig(rs.getString("state_of_orig"));
                                    foundCr.setTestComment(rs.getString("test_comment"));
                                    foundCr.setTestReleasedDate(rs.getDate("test_released_date"));
                                    foundCr.setTestStatus(rs.getString("test_status"));

                                    foundCrList.add(foundCr);

                                }
                            } catch (Exception e) {
                                log.error(e);
                                throw e;
                            } finally {
                                try {
                                    rs.close();
                                    ps.close();
                                } catch (SQLException se) {
                                    log.error(se);
                                }
                            }

                            crr.setMatches(foundCrList.size());
                            if (foundCrList.size() >= limit) {
                                crr.setUdohClinicalResults(foundCrList.subList(0, limit - 1));
                            } else {
                                crr.setUdohClinicalResults(foundCrList);
                            }
                            hm.addStatusMessage(new StatusMessage("getUdohClinicalResults", Status.SUCCESS.toString(), null));
                        }
                    }
                }
            } catch (Exception ex) {
                log.error(ex);
                hm.addStatusMessage(new StatusMessage("getUdohClinicalResults", Status.FAILURE_EXCEPTION.toString(), ex.getMessage()));
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException se) {
                        log.error(se);
                    }

                }
            }



        } else {
            hm.addStatusMessage(new StatusMessage("getUdohClinicalResults", Status.FAILURE_MISSING_DATA.toString(), "limsHealth ,missing"));
            log.error("limsHealth missing in call to getUdohClinicalResults()");
        }


    }
}
