package gov.utah.health.data.emsa;

import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.model.dashboard.BucketCount;
import gov.utah.health.model.dashboard.BucketReport;
import gov.utah.health.util.AppProperties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

/**
 * This class provides implementation for data access to emsa
 *
 * @author UDOH
 *
 */
@Repository
public class EmsaDaoImpl {

    private static final Log log = LogFactory.getLog(EmsaDaoImpl.class);

    /**
     * *
     * Counts exception, pending, entry lists, by day (week, month, year) by lab
     * (or hospital, lab, clinic, or all) by disease category (or all)
     *
     * @param hm HealthMessage object. The healthMessage object including
     * DashboardReport object with a list of UdohClinicalResultsReport data set
     * as search criteria. The HealthMessage parameter is modified to include
     * all result data. Return data includes data for all of the matching
     * limsp.udoh_clinical_results records. If errors or exceptions take place
     * information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    public void getBucketReport(HealthMessage hm, BucketReport br) {

        Date start = br.getStart();
        Date end = br.getEnd();
        String metric = br.getMetric();
        String place = br.getPlace();
        String diseaseCategory = br.getDiseaseCategory();
        List<String> bucketsSelected = br.getBucketsSelected();


        if (start.before(end)) {

            Connection con = null;

            try {

                Class.forName("org.postgresql.Driver").newInstance();
                con = DriverManager.getConnection(null, null, null);


                GregorianCalendar gcStart = new GregorianCalendar();
                gcStart.setTime(start);
                int m = br.getMetricInt();

                while (gcStart.getTime().before(end)) {

                    BucketCount bc = new BucketCount(place, metric, diseaseCategory, bucketsSelected.get(0));
                    bc.setStart(gcStart.getTime());
                    gcStart.add(m, 1);
                    if (gcStart.after(end)) {
                        bc.setEnd(end);
                    } else {
                        bc.setEnd(gcStart.getTime());
                    }

                    this.getBucketCount(con, bc);
                    br.addBucketCount(bc);

                }

                this.getBucketList(con, br);
                this.getDiseaseCategories(con, br);
                this.getPlaces(con, br);

            } catch (Exception e) {
                log.error(e);
                hm.addStatusMessage(new StatusMessage("getBucketReport", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException se) {
                        log.error(se);
                    }

                }
            }

        }
        hm.addStatusMessage(new StatusMessage("getBucketReport", Status.SUCCESS.toString(), null));

    }

    private void getBucketCount(Connection con, BucketCount bc) throws Exception {


        Date start = bc.getStart();
        Date end = bc.getEnd();
        String bucket = bc.getBucket();
        String diseaseCategory = bc.getDiseaseCategory();
        String place = bc.getPlace();



        StringBuilder sb = new StringBuilder("SELECT count(*) FROM ");
        sb.append("elr.system_messages_audits,elr.system_labs,elr.system_statuses ");
        sb.append("WHERE elr.system_messages_audits.lab_id=elr.system_labs.id ");
        sb.append("AND elr.system_statuses.id = elr.system_messages_audits.system_exception_id ");
        sb.append("AND elr.system_messages_audits.action_category_id = 2 ");
        sb.append("AND elr.system_statuses.name ilike ? ");

        if (place != null && place.trim().length() > 0) {
            sb.append("AND elr.system_labs.short_name ilike ? ");
        }
        if (start != null) {
            sb.append("AND elr.system_messages_audits.created_at >= ? ");
        }
        if (end != null) {
            sb.append("AND elr.system_messages_audits.created_at <= ?");
        }

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sb.toString());

            int i = 1;

            ps.setString(i++, bucket);
            if (place != null && place.trim().length() > 0) {
                ps.setString(i++, place);
            }
            if (start != null) {
                ps.setDate(i++, new java.sql.Date(start.getTime()));
            }
            if (end != null) {
                ps.setDate(i++, new java.sql.Date(end.getTime()));
            }
            rs = ps.executeQuery();


            if (rs.next()) {
                // one row
                bc.setCount(rs.getInt("count"));

            }


        } catch (Exception e) {
            log.error(e);
            throw e;
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException se) {
                log.error(se);
            }
        }



    }

    private void getPlaces(Connection con, BucketReport br) throws Exception {

// to only show the labs where there are audits use...
        //         StringBuilder sb = new StringBuilder("select name from system_labs where id in (select distinct lab_id from system_messages_audits)");

        StringBuilder sb = new StringBuilder("select short_name from elr.system_labs");
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sb.toString());
            rs = ps.executeQuery();

            List<String> places = new ArrayList<String>();
            while (rs.next()) {
                places.add(rs.getString("short_name"));
            }
            br.setPlaces(places);


        } catch (Exception e) {
            log.error(e);
            throw e;
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException se) {
                log.error(se);
            }
        }



    }

    private void getDiseaseCategories(Connection con, BucketReport br) throws Exception {

        StringBuilder sb = new StringBuilder("SELECT count(*)");
        PreparedStatement ps = null;
        ResultSet rs = null;
//        try {
//
//            ps = con.prepareStatement(sb.toString());
//            rs = ps.executeQuery();
//
//            List<String> dcList = new ArrayList<String>();
//            while (rs.next()) {
//                dcList.add(rs.getString("category"));
//            }
//            br.setDiseaseCategories(dcList);
//
//        } catch (Exception e) {
//            log.error(e);
//            throw e;
//        } finally {
//            try {
//                rs.close();
//                ps.close();
//            } catch (SQLException se) {
//                log.error(se);
//            }
//        }



    }

    private void getBucketList(Connection con, BucketReport br) throws Exception {

        StringBuilder sb = new StringBuilder("select name from elr.system_statuses");
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sb.toString());
            rs = ps.executeQuery();

            List<String> bList = new ArrayList<String>();
            while (rs.next()) {
                bList.add(rs.getString("name"));
            }
            br.setBuckets(bList);

        } catch (Exception e) {
            log.error(e);
            throw e;
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException se) {
                log.error(se);
            }
        }
    }
}
