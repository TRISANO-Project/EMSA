
package gov.utah.health.data.trisano;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 *
 * @author admin
 */
public final class TrisanoConnect {

    private static DataSource ds = null;    

    static  
    {  
        try
        {
            InitialContext ic = new InitialContext();
            ds = (DataSource) ic.lookup("java:/comp/env/jdbc/trisanoDs"); 

        }
        catch (Exception e)
        {
            e.printStackTrace();
            ds = null;
        }

    }

    public static Connection getSQLConnection() throws SQLException  
    {  
        return ds.getConnection();             
    }
}