package gov.utah.health.data.trisano;

import gov.utah.health.model.HealthMessage;

/**
 *
 * @author UDOH
 */
public interface TrisanoDao {

    public void searchPerson(HealthMessage hm);
    public void findPerson(HealthMessage hm);
    public void findEvent(HealthMessage hm);
    public void addCmr(HealthMessage hm);
    public void addAttachments(HealthMessage hm);
    public void updateAddressesGeocode(HealthMessage hm);
    public void updateCmr(HealthMessage hm);
    public void findId(HealthMessage hm);
    public void addAnswers(HealthMessage hm);
    public void findQuestions(HealthMessage hm);
    public void findAnswers(HealthMessage hm);
    public void getTableData(HealthMessage hm);
    public void getHealthDataExample(HealthMessage hm);
    public void getCaseStatusReport(HealthMessage hm);
    public void getDiseaseReport(HealthMessage hm);
    public void getDiseases(HealthMessage hm);
    public void getUserRoles(HealthMessage hm);
    public void getUsers(HealthMessage hm);
    public void getRoles(HealthMessage hm);   
    public void exportTreatmentNotes(HealthMessage hm);
    public void findForms(HealthMessage hm);
    public void exportForms(HealthMessage hm);
    public void getDiseaseForms(HealthMessage hm);
    public void getCareTimeReport(HealthMessage hm);
    public void getCdcExport(HealthMessage hm);
    public void setLabResults(HealthMessage hm);
    public void updateWorkflowState(HealthMessage hm);
    public void getJurisdiction(HealthMessage hm); 
}

