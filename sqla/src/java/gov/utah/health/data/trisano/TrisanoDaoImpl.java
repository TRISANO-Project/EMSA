package gov.utah.health.data.trisano;

import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.model.trisano.Addresses;
import gov.utah.health.model.trisano.Answers;
import gov.utah.health.model.trisano.Attachments;
import gov.utah.health.model.trisano.CareData;
import gov.utah.health.model.trisano.CareTimeReport;
import gov.utah.health.model.trisano.CaseStatusCount;
import gov.utah.health.model.trisano.CaseStatusReport;
import gov.utah.health.model.trisano.CdcExport;
import gov.utah.health.model.trisano.CliniciansAttributes;
import gov.utah.health.model.trisano.Codes;
import gov.utah.health.model.trisano.CommonTestTypes;
import gov.utah.health.model.trisano.ContactChildEventsAttributes;
import gov.utah.health.model.trisano.DiagnosticFacilitiesAttributes;
import gov.utah.health.model.trisano.DiseaseCount;
import gov.utah.health.model.trisano.DiseaseCriteria;
import gov.utah.health.model.trisano.DiseaseEvents;
import gov.utah.health.model.trisano.DiseaseReport;
import gov.utah.health.model.trisano.Diseases;
import gov.utah.health.model.trisano.DiseasesForms;
import gov.utah.health.model.trisano.EmailAddresses;
import gov.utah.health.model.trisano.Events;
import gov.utah.health.model.trisano.ExternalCodes;
import gov.utah.health.model.trisano.FormElements;
import gov.utah.health.model.trisano.FormReferences;
import gov.utah.health.model.trisano.Forms;
import gov.utah.health.model.trisano.FormsAttributes;
import gov.utah.health.model.trisano.HospitalizationFacilitiesAttributes;
import gov.utah.health.model.trisano.HospitalsParticipations;
import gov.utah.health.model.trisano.InterestedPartyAttributes;
import gov.utah.health.model.trisano.JurisdictionAttributes;
import gov.utah.health.model.trisano.LabResults;
import gov.utah.health.model.trisano.LabsAttributes;
import gov.utah.health.model.trisano.Notes;
import gov.utah.health.model.trisano.Organisms;
import gov.utah.health.model.trisano.Participations;
import gov.utah.health.model.trisano.ParticipationsContacts;
import gov.utah.health.model.trisano.ParticipationsRiskFactors;
import gov.utah.health.model.trisano.ParticipationsTreatments;
import gov.utah.health.model.trisano.People;
import gov.utah.health.model.trisano.PeopleRaces;
import gov.utah.health.model.trisano.PeopleRacesPK;
import gov.utah.health.model.trisano.PlaceAttributes;
import gov.utah.health.model.trisano.Places;
import gov.utah.health.model.trisano.Questions;
import gov.utah.health.model.trisano.ReporterAttributes;
import gov.utah.health.model.trisano.ReportingAgencyAttributes;
import gov.utah.health.model.trisano.RoleMemberships;
import gov.utah.health.model.trisano.Roles;
import gov.utah.health.model.trisano.Telephones;
import gov.utah.health.model.trisano.Treatments;
import gov.utah.health.model.trisano.TrisanoHealth;
import gov.utah.health.model.trisano.Type;
import gov.utah.health.model.trisano.Users;
import gov.utah.health.model.trisano.UsersAttributes;
import gov.utah.health.util.AppProperties;
import gov.utah.health.util.HibernateUtil;
import java.net.URLDecoder;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * This class provides implementation for data access(not insert or update)
 * functions to trisano 3.x. As of this version all of the insertion of data
 * into trisano 3.x is handled by the TrisanoWebClient class in the same
 * package.
 *
 * @author UDOH
 *
 */
@Repository
public class TrisanoDaoImpl implements TrisanoDao {

    private static final Log log = LogFactory.getLog(TrisanoDaoImpl.class);
    private static final String datasource = "TRISANO";
    private static AppProperties props = AppProperties.getInstance();
    private static String emsaEnvironment = props.getProperty("emsa_environment");

    /**
     * *
     * Performs an exact match lookup based on a persons first name, last name
     * and birth date.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set as search criteria. The HealthMessage parameter
     * is modified to include all result data. Return data includes data for all
     * of the matching person's events. If errors or exceptions take place
     * information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void findPerson(HealthMessage hm) {
// TODO dont retrun events where deleted
        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (!thList.isEmpty()) {

                TrisanoHealth th = thList.get(0);
                Query qPeople = null;
                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                List<InterestedPartyAttributes> ipaList = th.getInterestedPartyAttributes();
                People searchPerson = null;
                if (ipaList != null && !ipaList.isEmpty()) {
                    InterestedPartyAttributes ipa = ipaList.get(0);
                    searchPerson = ipa.getPerson();
                }

                if (searchPerson != null) {

                    if (searchPerson.getId() != null && searchPerson.getId() > 0) {
                        qPeople = session.getNamedQuery("People.findById");
                        qPeople.setInteger("id", searchPerson.getId());
                    } else if (searchPerson.nameSearchSet()) {
                        qPeople = session.getNamedQuery("People.find");

                        String fn = searchPerson.getFirstName().trim();
                        String ln = searchPerson.getLastName().trim();
                        Date bday = searchPerson.getBirthDate();
 
                        qPeople.setString("firstName", fn);
                        qPeople.setString("lastName", ln);
                        qPeople.setDate("birthDate", bday);
                        
                    } else {
                        hm.addStatusMessage(new StatusMessage("findPerson", Status.FAILURE_MISSING_DATA.toString(), "No valid criteria in call"));
                        throw new Exception("No valid criteria in call to findPerson");
                    }

                } else {
                    hm.addStatusMessage(new StatusMessage("findPerson", Status.FAILURE_MISSING_DATA.toString(), "person is required."));
                    throw new Exception("Person is null in call to findPerson");
                }

                List<People> people = qPeople.list();
                hm.setTrisanoHealth(null);
                for (People p : people) {
                    TrisanoHealth t = new TrisanoHealth();
                    hm.addTrisanoHealth(t);

                    InterestedPartyAttributes ipa = new InterestedPartyAttributes();
                    ipa.setPerson(p);
                    t.addInterestedPartyAttributes(ipa);

                    Query qAddresses = session.getNamedQuery("Addresses.findByEntityId");
                    qAddresses.setInteger("entityId", p.getEntityId());
                    List<Addresses> addresses = qAddresses.list();
                    t.setAddresses(addresses);

                    Query qTelephones = session.getNamedQuery("Telephones.findByEntityId");
                    qTelephones.setInteger("entityId", p.getEntityId());
                    List<Telephones> telephones = qTelephones.list();
                    ipa.setTelephones(telephones);

                    Query qPeopleRaces = session.getNamedQuery("PeopleRaces.findByEntityId");
                    qPeopleRaces.setInteger("entityId", p.getEntityId());
                    List<PeopleRaces> peopleRaces = qPeopleRaces.list();
                    ipa.setPeopleRaces(peopleRaces);

                    Query qEvents = session.getNamedQuery("Events.findByPersonEntityId");
                    qEvents.setInteger("personEntityId", p.getEntityId());
                    List<Events> events = qEvents.list();
                    t.addEvents(events);
                    
                    Query qParticipations;
                    for(Events event : events) {
                        Query qDiseaseEvents = session.getNamedQuery("DiseaseEvents.findByEventId");
                        qDiseaseEvents.setInteger("eventId", event.getId());
                        List<DiseaseEvents> diseaseEvents = qDiseaseEvents.list();
                        t.addDiseaseEvents(diseaseEvents);
                        for (DiseaseEvents de : diseaseEvents) {
                            Query qDiseases = session.getNamedQuery("Diseases.findById");
                            qDiseases.setInteger("id", de.getDiseaseId());
                            t.addDiseases(qDiseases.list());
                        }
                        qParticipations = session.getNamedQuery("Participations.findByEventIdType");
                        qParticipations.setInteger("eventId", event.getId());
                        qParticipations.setString("type", "InterestedParty");
                        List<Participations> participations = qParticipations.list();
                        Participations participation = participations.get(0);
                        Query qPt = session.getNamedQuery("ParticipationsTreatments.findByParticipationId");

                        qPt.setInteger("participationId", participation.getId());
                        ipa.addTreatmentAttributes(qPt.list());
                        ipa.addParticipation(participation);
                        
                        qParticipations = session.getNamedQuery("Participations.findByEventId");
                        qParticipations.setInteger("eventId", event.getId());
                        participations = qParticipations.list();
                        for (Participations pa : participations) {

                            if (pa.getType().equals(Type.JURISDICTION.getName())) {

                                Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                qPlaces.setInteger("entityId", pa.getSecondaryEntityId());
                                List<Places> pList = qPlaces.list();
                                if (pList != null && !pList.isEmpty()) {
                                    Places place = pList.get(0);
                                    List<JurisdictionAttributes> jaList = new ArrayList<>();
                                    JurisdictionAttributes ja = new JurisdictionAttributes();
                                    ja.setPlace(place);

                                    jaList.add(ja);
                                    t.setJurisdictionAttributes(jaList);
                                }

                            } else if (pa.getType().equals(Type.REPORTING_AGENCY.getName())) {

                                Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                qPlaces.setInteger("entityId", pa.getSecondaryEntityId());
                                List<Places> pList = qPlaces.list();
                                if (pList != null && !pList.isEmpty()) {
                                    Places place = pList.get(0);

                                    List<ReportingAgencyAttributes> raaList = new ArrayList<>();
                                    ReportingAgencyAttributes raa = new ReportingAgencyAttributes();
                                    raa.setPlace(place);
                                    raa.addParticipation(pa);

                                    raaList.add(raa);
                                    t.addReportingAgencyAttributes(raaList);

                                }

                            } else if (pa.getType().equals(Type.DIAGNOSTIC_FACILITY.getName())) {

                                Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                qPlaces.setInteger("entityId", pa.getSecondaryEntityId());
                                List<Places> pList = qPlaces.list();
                                if (pList != null && !pList.isEmpty()) {

                                    Places place = pList.get(0);

                                    List<DiagnosticFacilitiesAttributes> dfList = new ArrayList<>();
                                    DiagnosticFacilitiesAttributes dfa = new DiagnosticFacilitiesAttributes();
                                    dfa.setPlace(place);
                                    dfa.addParticipation(pa);

                                    dfList.add(dfa);
                                    t.addDiagnosticFacilitiesAttributes(dfList);
                                }
                            } else if (pa.getType().equals(Type.HOSPITALIZATION_FACILITY.getName())) {

                                List<HospitalizationFacilitiesAttributes> hfaList = new ArrayList<>();
                                HospitalizationFacilitiesAttributes hfa = new HospitalizationFacilitiesAttributes();
                                Integer paSecondEntityId = pa.getSecondaryEntityId();
                                if (paSecondEntityId != null && paSecondEntityId > 0) {
                                    Query qhfPlaces = session.getNamedQuery("Places.findByEntityId");
                                    qhfPlaces.setInteger("entityId", pa.getSecondaryEntityId());
                                    List<Places> pList = qhfPlaces.list();
                                    if (pList != null && !pList.isEmpty()) {
                                        Places place = pList.get(0);
                                        hfa.setPlace(place);
                                    }
                                }
                                Query qhp = session.getNamedQuery("HospitalsParticipations.findByParticipationId");
                                qhp.setInteger("participationId", pa.getId());
                                List<HospitalsParticipations> hpList = qhp.list();
                                if (hpList != null && !hpList.isEmpty()) {
                                    HospitalsParticipations hp = hpList.get(0);
                                    hfa.setHospitalsParticipations(hp);
                                }
                                hfa.addParticipation(pa);
                                hfaList.add(hfa);
                                t.addHospitalizationFacilitiesAttributes(hfaList);

                            } else if (pa.getType().equals(Type.LAB.getName())) {

                                List<LabsAttributes> laList = new ArrayList<>();

                                Query qlPlaces = session.getNamedQuery("Places.findByEntityId");
                                qlPlaces.setInteger("entityId", pa.getSecondaryEntityId());
                                List<Places> lList = qlPlaces.list();

                                if (lList != null && !lList.isEmpty()) {
                                    Places lab = lList.get(0);

                                    LabsAttributes la = new LabsAttributes();
                                    la.setPlace(lab);
                                    la.addParticipation(pa);

                                    Query qLabResults = session.getNamedQuery("LabResults.findByParticipationId");
                                    qLabResults.setInteger("participationId", pa.getId());
                                    List<LabResults> lrList = qLabResults.list();
                                    la.setLabResults(lrList);
                                    for (LabResults lr : lrList) {

                                        if (lr.getSpecimenSourceId() != null) {
                                            Query qExternalCodes = session.getNamedQuery("ExternalCodes.findById");
                                            qExternalCodes.setInteger("id", lr.getSpecimenSourceId());
                                            List<ExternalCodes> ecList = qExternalCodes.list();
                                            t.addExternalCodes(ecList);
                                        }

                                        if (lr.getTestTypeId() != null) {
                                            Query qCommonTestTypes = session.getNamedQuery("CommonTestTypes.findById");
                                            qCommonTestTypes.setInteger("id", lr.getTestTypeId());
                                            List<CommonTestTypes> cttList = qCommonTestTypes.list();
                                            t.addCommonTestTypes(cttList);
                                        }
                                        if (lr.getTestResultId() != null) {
                                            Query qExternalCodes = session.getNamedQuery("ExternalCodes.findById");
                                            qExternalCodes.setInteger("id", lr.getTestResultId());
                                            List<ExternalCodes> ecList = qExternalCodes.list();
                                            t.addExternalCodes(ecList);
                                        }

                                    }

                                    laList.add(la);
                                }
                                t.addLabAttributes(laList);

                            } else if (pa.getType().equals(Type.CLINICIAN.getName())) {

                                Query qClin = session.getNamedQuery("People.findByEntityId");
                                qClin.setInteger("entityId", pa.getSecondaryEntityId());
                                List<People> pList = qClin.list();
                                if (pList != null && !pList.isEmpty()) {

                                    People clin = pList.get(0);

                                    List<CliniciansAttributes> caList = new ArrayList<>();
                                    CliniciansAttributes ca = new CliniciansAttributes();
                                    ca.setClinician(clin);

                                    Query qcTelephones = session.getNamedQuery("Telephones.findByEntityId");
                                    qcTelephones.setInteger("entityId", clin.getEntityId());
                                    List<Telephones> cPhones = qcTelephones.list();
                                    ca.setTelephones(cPhones);

                                    caList.add(ca);
                                    t.addCliniciansAttributes(caList);
                                }

                            } else if (pa.getType().equals(Type.REPORTER.getName())) {

                                Query qReporter = session.getNamedQuery("People.findByEntityId");
                                qReporter.setInteger("entityId", pa.getSecondaryEntityId());
                                List<People> pList = qReporter.list();
                                if (pList != null && !pList.isEmpty()) {

                                    People reporter = pList.get(0);

                                    List<ReporterAttributes> rList = new ArrayList<>();
                                    ReporterAttributes ra = new ReporterAttributes();
                                    ra.setReporter(reporter);

                                    Query qrTelephones = session.getNamedQuery("Telephones.findByEntityId");
                                    qrTelephones.setInteger("entityId", reporter.getEntityId());
                                    List<Telephones> rPhones = qrTelephones.list();
                                    ra.setTelephones(rPhones);

                                    ra.addParticipation(pa);
                                    rList.add(ra);
                                    t.addReporterAttributes(rList);
                                }
                            }
                        }
                    }
                }

                session.getTransaction().commit();
                hm.addStatusMessage(new StatusMessage("findPerson", Status.SUCCESS.toString(), null));
                thList.remove(th);

            }

        } catch (Exception e) {
            log.error(e);
            hm.addStatusMessage(new StatusMessage("findPerson", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Finds data related to a given event based on event id.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data with events.id set. The HealthMessage parameter is
     * modified to include all result data. Return data includes data for a
     * single event including some associated data. If errors or exceptions take
     * place information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void findEvent(HealthMessage hm) {

        try {

            fixReportNumberInEventId(hm);
            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                TrisanoHealth th = thList.get(0);
                Query qEvents = null;
                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                List<Events> eList = th.getEvents();
                Events event = null;
                if (eList != null && !eList.isEmpty()) {
                    event = eList.get(0);
                }

                if (event != null) {

                    if (event.getId() != null && event.getId() > 0) {
                        qEvents = session.getNamedQuery("Events.findById");
                        qEvents.setInteger("id", event.getId());

                    } else {
                        hm.addStatusMessage(new StatusMessage("findEvent", Status.FAILURE_MISSING_DATA.toString(), "events.id is missing"));
                    }

                } else {
                    hm.addStatusMessage(new StatusMessage("findEvent", Status.FAILURE_MISSING_DATA.toString(), "events.id is missing"));
                }

                if (qEvents != null) {
                    List<Events> el = qEvents.list();
                    for (Events e : el) {
                        TrisanoHealth t = new TrisanoHealth();
                        hm.addTrisanoHealth(t);

                        List<Events> eFoundList = new ArrayList<>();
                        eFoundList.add(e);
                        t.setEvents(eFoundList);

                        Query qParticipations = session.getNamedQuery("Participations.findByEventId");
                        qParticipations.setInteger("eventId", e.getId());
                        List<Participations> pl = qParticipations.list();
                        for (Participations p : pl) {

                            if (p.getType().equals(Type.INTERESTED_PARTY.getName())) {

                                InterestedPartyAttributes ipa = new InterestedPartyAttributes();

                                Query qPeople = session.getNamedQuery("People.findByEntityId");
                                qPeople.setInteger("entityId", p.getPrimaryEntityId());
                                List<People> pList = qPeople.list();
                                if (pList != null && !pList.isEmpty()) {
                                    ipa.setPerson(pList.get(0));
                                }

                                Query qTelephones = session.getNamedQuery("Telephones.findByEntityId");
                                qTelephones.setInteger("entityId", p.getPrimaryEntityId());
                                List<Telephones> telephones = qTelephones.list();
                                ipa.setTelephones(telephones);

                                Query qPeopleRaces = session.getNamedQuery("PeopleRaces.findByEntityId");
                                qPeopleRaces.setInteger("entityId", p.getPrimaryEntityId());
                                List<PeopleRaces> peopleRaces = qPeopleRaces.list();
                                ipa.setPeopleRaces(peopleRaces);

                                List<InterestedPartyAttributes> ipaList = new ArrayList<>();
                                ipaList.add(ipa);
                                t.setInterestedPartyAttributes(ipaList);

                                Query qAddresses = session.getNamedQuery("Addresses.findByEntityId");
                                qAddresses.setInteger("entityId", p.getPrimaryEntityId());
                                List<Addresses> addresses = qAddresses.list();
                                t.setAddresses(addresses);

                            } else if (p.getType().equals(Type.JURISDICTION.getName())) {

                                Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                qPlaces.setInteger("entityId", p.getSecondaryEntityId());
                                List<Places> pList = qPlaces.list();
                                if (pList != null && !pList.isEmpty()) {
                                    Places place = pList.get(0);
                                    List<JurisdictionAttributes> jaList = new ArrayList<>();
                                    JurisdictionAttributes ja = new JurisdictionAttributes();
                                    ja.setPlace(place);
                                    jaList.add(ja);
                                    t.setJurisdictionAttributes(jaList);
                                }

                            } else if (p.getType().equals(Type.REPORTING_AGENCY.getName())) {

                                Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                qPlaces.setInteger("entityId", p.getSecondaryEntityId());
                                List<Places> pList = qPlaces.list();
                                if (pList != null && !pList.isEmpty()) {
                                    Places place = pList.get(0);
                                    List<ReportingAgencyAttributes> raaList = new ArrayList<>();
                                    ReportingAgencyAttributes raa = new ReportingAgencyAttributes();
                                    raa.setPlace(place);
                                    raaList.add(raa);
                                    t.addReportingAgencyAttributes(raaList);
                                }
                            } else if (p.getType().equals(Type.HOSPITALIZATION_FACILITY.getName())) {

                                List<HospitalizationFacilitiesAttributes> raaList = new ArrayList<>();
                                HospitalizationFacilitiesAttributes hfa = null;

                                Query qhp = session.getNamedQuery("HospitalsParticipations.findByParticipationId");
                                qhp.setInteger("participationId", p.getId());
                                List<HospitalsParticipations> hpList = qhp.list();
                                if (hpList != null && !hpList.isEmpty()) {
                                    HospitalsParticipations hp = hpList.get(0);
                                    hfa = new HospitalizationFacilitiesAttributes();
                                    hfa.setHospitalsParticipations(hp);

                                }
                                Integer placeEntityId = p.getSecondaryEntityId();
                                if (placeEntityId != null) {
                                    Query qhfPlaces = session.getNamedQuery("Places.findByEntityId");
                                    qhfPlaces.setInteger("entityId", placeEntityId);
                                    List<Places> pList = qhfPlaces.list();
                                    if (pList != null && !pList.isEmpty()) {
                                        Places place = pList.get(0);
                                        if (hfa == null) {
                                            hfa = new HospitalizationFacilitiesAttributes();
                                        }
                                        hfa.setPlace(place);
                                    }
                                }
                                raaList.add(hfa);
                                t.addHospitalizationFacilitiesAttributes(raaList);

                            } else if (p.getType().equals(Type.LAB.getName())) {

                                List<LabsAttributes> laList = new ArrayList<>();

                                Query qlPlaces = session.getNamedQuery("Places.findByEntityId");
                                qlPlaces.setInteger("entityId", p.getSecondaryEntityId());
                                List<Places> lList = qlPlaces.list();

                                if (lList != null && !lList.isEmpty()) {
                                    Places lab = lList.get(0);

                                    LabsAttributes la = new LabsAttributes();
                                    la.setPlace(lab);

                                    Query qLabResults = session.getNamedQuery("LabResults.findByParticipationId");
                                    qLabResults.setInteger("participationId", p.getId());
                                    List<LabResults> lrList = qLabResults.list();
                                    la.setLabResults(lrList);
                                    for (LabResults lr : lrList) {

                                        if (lr.getSpecimenSourceId() != null) {
                                            Query qExternalCodes = session.getNamedQuery("ExternalCodes.findById");
                                            qExternalCodes.setInteger("id", lr.getSpecimenSourceId());
                                            List<ExternalCodes> ecList = qExternalCodes.list();
                                            t.addExternalCodes(ecList);
                                        }

                                        if (lr.getTestTypeId() != null) {
                                            Query qCommonTestTypes = session.getNamedQuery("CommonTestTypes.findById");
                                            qCommonTestTypes.setInteger("id", lr.getTestTypeId());
                                            List<CommonTestTypes> cttList = qCommonTestTypes.list();
                                            t.addCommonTestTypes(cttList);
                                        }
                                        if (lr.getTestResultId() != null) {
                                            Query qExternalCodes = session.getNamedQuery("ExternalCodes.findById");
                                            qExternalCodes.setInteger("id", lr.getTestResultId());
                                            List<ExternalCodes> ecList = qExternalCodes.list();
                                            t.addExternalCodes(ecList);
                                        }
                                        if (lr.getOrganismId() != null) {
                                            Query qOrganisms = session.getNamedQuery("Organisms.findById");
                                            qOrganisms.setInteger("id", lr.getOrganismId());
                                            List<Organisms> oList = qOrganisms.list();
                                            t.addOrganisms(oList);
                                        }

                                    }

                                    laList.add(la);
                                }
                                t.addLabAttributes(laList);

                            } else if (p.getType().equals(Type.CLINICIAN.getName())) {

                                Query qClin = session.getNamedQuery("People.findByEntityId");
                                qClin.setInteger("entityId", p.getSecondaryEntityId());
                                List<People> pList = qClin.list();
                                if (pList != null && !pList.isEmpty()) {
                                    People clin = pList.get(0);

                                    List<CliniciansAttributes> caList = new ArrayList<>();
                                    CliniciansAttributes ca = new CliniciansAttributes();
                                    ca.setClinician(clin);

                                    Query qcTelephones = session.getNamedQuery("Telephones.findByEntityId");
                                    qcTelephones.setInteger("entityId", clin.getEntityId());
                                    List<Telephones> cPhones = qcTelephones.list();
                                    ca.setTelephones(cPhones);
                                    caList.add(ca);
                                    t.addCliniciansAttributes(caList);
                                }

                            } else if (p.getType().equals(Type.REPORTER.getName())) {

                                Query qReporter = session.getNamedQuery("People.findByEntityId");
                                qReporter.setInteger("entityId", p.getSecondaryEntityId());
                                List<People> pList = qReporter.list();
                                if (pList != null && !pList.isEmpty()) {
                                    People reporter = pList.get(0);

                                    List<ReporterAttributes> rList = new ArrayList<>();
                                    ReporterAttributes ra = new ReporterAttributes();
                                    ra.setReporter(reporter);

                                    Query qrTelephones = session.getNamedQuery("Telephones.findByEntityId");
                                    qrTelephones.setInteger("entityId", reporter.getEntityId());
                                    List<Telephones> rPhones = qrTelephones.list();
                                    ra.setTelephones(rPhones);
                                    rList.add(ra);
                                    t.addReporterAttributes(rList);
                                }
                            } else if (p.getType().equals(Type.DIAGNOSTIC_FACILITY.getName())) {

                                Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                qPlaces.setInteger("entityId", p.getSecondaryEntityId());
                                List<Places> pList = qPlaces.list();
                                if (pList != null && !pList.isEmpty()) {
                                    Places place = pList.get(0);
                                    List<DiagnosticFacilitiesAttributes> dfaList = new ArrayList<>();
                                    DiagnosticFacilitiesAttributes dfa = new DiagnosticFacilitiesAttributes();
                                    dfa.setPlace(place);
                                    dfaList.add(dfa);
                                    t.addDiagnosticFacilitiesAttributes(dfaList);
                                }
                            }

                        }

                        Query qDiseaseEvents = session.getNamedQuery("DiseaseEvents.findByEventId");
                        qDiseaseEvents.setInteger("eventId", event.getId());
                        List<DiseaseEvents> diseaseEvents = qDiseaseEvents.list();
                        t.setDiseaseEvents(diseaseEvents);
                        for (DiseaseEvents de : diseaseEvents) {
                            Query qDiseases = session.getNamedQuery("Diseases.findById");
                            qDiseases.setInteger("id", de.getDiseaseId());
                            t.addDiseases(qDiseases.list());
                        }
                    }
                }
                session.getTransaction().commit();
                hm.addStatusMessage(new StatusMessage("findEvent", Status.SUCCESS.toString(), null));

                thList.remove(th);

            }

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("findEvent", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));

        }

    }

    /**
     * *
     * In this version all inserts or updates to trisano nedss are handled
     * through the TrisanoWebClient
     */
    @Override
    @Autowired
    public void addCmr(HealthMessage hm) {
//        Session session = null;
//        try {
//
//            List<TrisanoHealth> thList = hm.getTrisanoHealth();
//            if (!thList.isEmpty()) {
//
//                session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
//                session.beginTransaction();
//
//
//                for (TrisanoHealth th : thList) {
//
//                    Entities entity = new Entities();
//                    entity.setEntityType("PersonEntity");
//                    entity.setCreatedAt(new Date());
//                    entity.setUpdatedAt(new Date());
//
//                    session.save(entity);
//
//                    List<InterestedPartyAttributes> ipaList = th.getInterestedPartyAttributes();
//
//                    InterestedPartyAttributes ipa = ipaList.get(0);
//
//                    People p = ipa.getPerson();
//                    p.setEntityId(entity.getId());
//                    p.setCreatedAt(new Date());
//                    p.setUpdatedAt(new Date());
//                    session.save(p);
//
//                    List<Events> eventList = th.getEvents();
//                    if (eventList != null && !eventList.isEmpty()) {
//                        Events event = eventList.get(0);
//                        // get max record_number and add one
//                        Query qMaxRecordNumber = session.getNamedQuery("Events.maxRecordNumber");
//                        List mrnl = qMaxRecordNumber.list();
//                        int maxRecordNumber = 0;
//                        if (mrnl.get(0) != null) {
//                            maxRecordNumber = Integer.parseInt((String) mrnl.get(0));
//                        }
//                        event.setRecordNumber(Integer.toString(maxRecordNumber + 1));
//                        event.setCreatedAt(new Date());
//                        event.setUpdatedAt(new Date());
//                        session.save(event);
//
//                        Participations interestedParty = new Participations();
//                        interestedParty.setType("InterestedParty");
//                        interestedParty.setPrimaryEntityId(entity.getId());
//                        interestedParty.setEventId(event.getId());
//                        interestedParty.setCreatedAt(new Date());
//                        interestedParty.setUpdatedAt(new Date());
//                        session.save(interestedParty);
//
//// process places_types
//
//                       List<JurisdictionAttributes> jaList = th.getJurisdictionAttributes();
//                       JurisdictionAttributes ja = jaList.get(0);
//
//
//                        if (ja != null) {
//                            Query qJuris = session.getNamedQuery("Places.findByNameType");
//                            qJuris.setString("name", ja.getJursidiction().getName());
//                            qJuris.setInteger("typeId", ja.getPlacesTypes().get(0).getPlacesTypesPK().getTypeId());
//                            int jSecEntityId = -1;
//                            List<Places> jPlaces = qJuris.list();
//                            if (jPlaces.size() > 0) {
//                                // get the entity
//                                jSecEntityId = jPlaces.get(0).getEntityId();
//                            } else {
//                                // no jurisdiction places exists
//                                hm.setStatus(Status.FAILURE_JURISDICTION_NOT_FOUND.toString());
//                                throw new Exception("Jursidiction not found. name="
//                                        + ja.getJursidiction().getName() + ", type="
//                                        + ja.getPlacesTypes().get(0).getPlacesTypesPK().getTypeId() + "");
//                            }
//// create the participations record for the jurisdiction
//                            Participations jPart = new Participations();
//                            jPart.setType("Jursidiction");
//                            jPart.setPrimaryEntityId(entity.getId());
//                            jPart.setSecondaryEntityId(jSecEntityId);
//                            jPart.setEventId(event.getId());
//                            jPart.setCreatedAt(new Date());
//                            jPart.setUpdatedAt(new Date());
//                            session.save(jPart);
//                        }
//
//                        PlacesPlacesTypes dfPpt = th.findPlacesPlacesTypes(Type.DIAGNOSTIC_FACILITY.getName());
//                        if (dfPpt != null) {
//                            Query dfJuris = session.getNamedQuery("Places.findByNameType");
//                            dfJuris.setString("name", dfPpt.getPlaceName());
//                            dfJuris.setInteger("typeId", dfPpt.getTypeId());
//                            int dfSecEntityId = -1;
//                            List<Places> dfPlaces = dfJuris.list();
//                            if (dfPlaces.size() > 0) {
//                                // get the entity
//                                dfSecEntityId = dfPlaces.get(0).getEntityId();
//                            } else {
//                                // no places exists add entity, palces and places_types
//
//                                Entities pEntity = new Entities();
//                                pEntity.setCreatedAt(new Date());
//                                pEntity.setUpdatedAt(new Date());
//                                pEntity.setEntityType(Type.PLACE_ENTITY.getName());
//                                session.save(pEntity);
//                                dfSecEntityId = pEntity.getId();
//
//                                Places dfPlace = dfPpt.getPlaces();
//                                dfPlace.setEntityId(pEntity.getId());
//                                dfPlace.setId(null);
//                                dfPlace.setCreatedAt(new Date());
//                                dfPlace.setUpdatedAt(new Date());
//                                session.save(dfPlace);
//
//                                PlacesTypes dfPlacesTypes = dfPpt.getPlacesTypes();
//                                dfPlacesTypes.getPlacesTypesPK().setPlaceId(dfPlace.getId());
//                                session.save(dfPlacesTypes);
//
//                            }
//// create the participations record for the df
//                            Participations dfPart = new Participations();
//                            dfPart.setType(Type.DIAGNOSTIC_FACILITY.getName());
//                            dfPart.setPrimaryEntityId(entity.getId());
//                            dfPart.setSecondaryEntityId(dfSecEntityId);
//                            dfPart.setEventId(event.getId());
//                            dfPart.setCreatedAt(new Date());
//                            dfPart.setUpdatedAt(new Date());
//                            session.save(dfPart);
//                        }
//
//                        PlacesPlacesTypes raPpt = th.findPlacesPlacesTypes(Type.REPORTING_AGENCY.getName());
//                        if (raPpt != null) {
//                            Query qAgency = session.getNamedQuery("Places.findByNameType");
//                            qAgency.setString("name", raPpt.getPlaceName());
//                            qAgency.setInteger("typeId", raPpt.getTypeId());
//                            int raSecEntityId = -1;
//                            List<Places> raPlaces = qAgency.list();
//                            if (raPlaces.size() > 0) {
//                                // get the entity
//                                raSecEntityId = raPlaces.get(0).getEntityId();
//                            } else {
//                                // no places exists entity places and the places_types
//                                Entities pEntity = new Entities();
//                                pEntity.setCreatedAt(new Date());
//                                pEntity.setUpdatedAt(new Date());
//                                pEntity.setEntityType(Type.PLACE_ENTITY.getName());
//                                session.save(pEntity);
//                                raSecEntityId = pEntity.getId();
//
//                                Places raPlace = raPpt.getPlaces();
//                                raPlace.setEntityId(pEntity.getId());
//                                raPlace.setId(null);
//                                raPlace.setCreatedAt(new Date());
//                                raPlace.setUpdatedAt(new Date());
//                                session.save(raPlace);
//
//                                PlacesTypes raPlacesTypes = dfPpt.getPlacesTypes();
//                                raPlacesTypes.getPlacesTypesPK().setPlaceId(raPlace.getId());
//                                session.save(raPlacesTypes);
//                            }
//// create the participations record for the reporting agency
//                            Participations raPart = new Participations();
//                            raPart.setType(Type.REPORTING_AGENCY.getName());
//                            raPart.setPrimaryEntityId(entity.getId());
//                            raPart.setSecondaryEntityId(raSecEntityId);
//                            raPart.setEventId(event.getId());
//                            raPart.setCreatedAt(new Date());
//                            raPart.setUpdatedAt(new Date());
//                            session.save(raPart);
//                        }
//
//
//// process lab
//                        PlacesPlacesTypes lPpt = th.findPlacesPlacesTypes(Type.LAB.getName());
//                        if (lPpt != null) {
//                            Query qLab = session.getNamedQuery("Places.findByNameType");
//                            qLab.setString("name", lPpt.getPlaceName());
//                            qLab.setInteger("typeId", lPpt.getTypeId());
//                            int lSecEntityId = -1;
//                            List<Places> lPlaces = qLab.list();
//                            if (lPlaces.size() > 0) {
//                                // get the entity
//                                lSecEntityId = lPlaces.get(0).getEntityId();
//                            } else {
//                                // no places exists add entity places and places_types
//                                Entities pEntity = new Entities();
//                                pEntity.setCreatedAt(new Date());
//                                pEntity.setUpdatedAt(new Date());
//                                pEntity.setEntityType(Type.PLACE_ENTITY.getName());
//                                session.save(pEntity);
//                                lSecEntityId = pEntity.getId();
//
//                                Places lPlace = lPpt.getPlaces();
//                                lPlace.setEntityId(pEntity.getId());
//                                lPlace.setId(null);
//                                lPlace.setCreatedAt(new Date());
//                                lPlace.setUpdatedAt(new Date());
//                                session.save(lPlace);
//
//                                PlacesTypes lPlaceTypes = lPpt.getPlacesTypes();
//                                lPlaceTypes.getPlacesTypesPK().setPlaceId(lPlace.getId());
//                                session.save(lPlaceTypes);
//                            }
//                            Participations partLab = new Participations();
//                            partLab.setType(Type.LAB.getName());
//                            partLab.setPrimaryEntityId(entity.getId());
//                            partLab.setSecondaryEntityId(lSecEntityId);
//                            partLab.setEventId(event.getId());
//                            partLab.setCreatedAt(new Date());
//                            partLab.setUpdatedAt(new Date());
//                            session.save(partLab);
//
//                            List<LabResults> labResults = th.getLabResults();
//                            if (labResults != null) {
//                                for (LabResults lr : labResults) {
//                                    lr.setCreatedAt(new Date());
//                                    lr.setUpdatedAt(new Date());
//                                    lr.setParticipationId(partLab.getId());
//                                    session.save(lr);
//                                }
//                            }
//
//
//                        }
//                        List<People> clinList = th.getClinician();
//                        if (clinList != null && !clinList.isEmpty()) {
//                            People clin = clinList.get(0);
//                            Query qClinician = session.getNamedQuery("People.findByNameType");
//                            qClinician.setString("firstName", clin.getFirstName());
//                            qClinician.setString("lastName", clin.getLastName());
//                            qClinician.setString("personType", clin.getPersonType());
//                            List<People> cList = qClinician.list();
//                            Participations participationClin = new Participations();
//                            participationClin.setType(Type.CLINICIAN.getName());
//                            participationClin.setPrimaryEntityId(entity.getId());
//                            participationClin.setEventId(event.getId());
//                            participationClin.setCreatedAt(new Date());
//                            participationClin.setUpdatedAt(new Date());
//                            if (!cList.isEmpty()) {
//                                participationClin.setSecondaryEntityId(cList.get(0).getEntityId());
//                                session.save(participationClin);
//                            } else {
//// add a clinician then add participations
//                                Entities cEntity = new Entities();
//                                cEntity.setCreatedAt(new Date());
//                                cEntity.setUpdatedAt(new Date());
//                                cEntity.setEntityType(Type.PEOPLE_ENTITY.getName());
//                                session.save(cEntity);
//
//                                clin.setCreatedAt(new Date());
//                                clin.setUpdatedAt(new Date());
//                                clin.setEntityId(cEntity.getId());
//                                session.save(clin);
//
//                                participationClin.setSecondaryEntityId(cEntity.getId());
//                                session.save(participationClin);
//
//                            }
//                        }
//// add a reporter and participations
////                        People searchReporter = th.getReporter();
////                        if (searchReporter != null) {
////                            Query qReporter = null;
////                            if (searchReporter.getBirthDate() != null) {
////                                qReporter = session.getNamedQuery("People.find");
////                                qReporter.setString("firstName", searchReporter.getFirstName());
////                                qReporter.setString("lastName", searchReporter.getLastName());
////                                qReporter.setDate("birthDate", searchReporter.getBirthDate());
////                            } else {
////                                qReporter = session.getNamedQuery("People.findByName");
////                                qReporter.setString("firstName", searchReporter.getFirstName());
////                                qReporter.setString("lastName", searchReporter.getLastName());
////                            }
////
////                            List<People> rList = qReporter.list();
////                            Participations partReporter = new Participations();
////                            partReporter.setType(Type.REPORTER.getName());
////                            partReporter.setPrimaryEntityId(entity.getId());
////                            partReporter.setEventId(event.getId());
////                            partReporter.setCreatedAt(new Date());
////                            partReporter.setUpdatedAt(new Date());
////                            if (!rList.isEmpty()) {
////                                partReporter.setSecondaryEntityId(rList.get(0).getEntityId());
////                                session.save(partReporter);
////                            } else {
////// add a clinician then add participations
////                                Entities rEntity = new Entities();
////                                rEntity.setCreatedAt(new Date());
////                                rEntity.setUpdatedAt(new Date());
////                                rEntity.setEntityType(Type.PEOPLE_ENTITY.getName());
////                                session.save(rEntity);
////
////                                partReporter.setCreatedAt(new Date());
////                                partReporter.setUpdatedAt(new Date());
////                                partReporter.setSecondaryEntityId(rEntity.getId());
////                                session.save(clinList);
////
////                                partReporter.setSecondaryEntityId(rEntity.getId());
////                                session.save(partReporter);
////
////                            }
////                        }
//
//                        Notes notes = th.getNotes();
//                        if (notes != null) {
//                            notes.setCreatedAt(new Date());
//                            notes.setUpdatedAt(new Date());
//                            notes.setEventId(event.getId());
//                            session.save(notes);
//                        }
//
//                        List<Addresses> addresses = th.getAddresses();
//                        if (addresses != null && addresses.size() == 1) {
//                            Addresses address0 = addresses.get(0);
//                            address0.setCreatedAt(new Date());
//                            address0.setUpdatedAt(new Date());
//                            address0.setEntityId(entity.getId());
//                            address0.setEventId(null);
//                            session.save(address0);
//                            Addresses address1 = address0.clone();
//                            address1.setEventId(null);
//                            session.save(address1);
//
//                        }
//
//                        List<PeopleRaces> peopleRaces = th.getPeopleRaces();
//                        if (peopleRaces != null) {
//                            for (PeopleRaces peopleRace : peopleRaces) {
//                                peopleRace.setCreatedAt(new Date());
//                                peopleRace.setUpdatedAt(new Date());
//                                peopleRace.getPeopleRacesPK().setEntityId(entity.getId());
//                                session.save(peopleRace);
//                            }
//                        }
//
//                        List<Telephones> telephones = th.getPersonTelephones();
//                        if (telephones != null) {
//                            for (Telephones telephone : telephones) {
//                                telephone.setCreatedAt(new Date());
//                                telephone.setUpdatedAt(new Date());
//                                telephone.setEntityId(entity.getId());
//                                session.save(telephone);
//                            }
//                        }
//
//
//                        List<DiseaseEvents> deList = th.getDiseaseEvents();
//                        if (deList != null) {
//                            for (DiseaseEvents de : deList) {
//                                de.setCreatedAt(new Date());
//                                de.setUpdatedAt(new Date());
//                                de.setEventId(event.getId());
//                                session.save(de);
//                            }
//                        }
//
//                        FormReferences fr = th.getFormReferences();
//                        if (fr != null) {
//                            fr.setCreatedAt(new Date());
//                            fr.setEventId(event.getId());
//                            session.save(fr);
//                        }
//                    } else {
//                        hm.setStatus(Status.FAILURE_MISSING_DATA.toString());
//                        hm.setStatusMessage("Events is null");
//                    }
//                }
//                session.getTransaction().commit();
//
//                thList.clear();
//                hm.setStatus(Status.SUCCESS.toString());
//
//
//            }
//
//
//        } catch (Exception e) {
//            session.getTransaction().rollback();
//            hm.setStatusMessage(e.getMessage());
//            log.error(e);
//        }
//
    }

    @Override
    @Autowired
    public void addAttachments(HealthMessage hm) {
        Session session = null;
        if (hm.addUpdateSuccess()) {
            try {

                List<TrisanoHealth> thList = hm.getTrisanoHealth();
                if (!thList.isEmpty()) {

                    session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                    session.beginTransaction();

                    for (TrisanoHealth th : thList) {
                        Integer eventId = th.getEventId();
                        Attachments a = th.getAttachment();
                        if (a != null && eventId > 0 && a.getCategory() != null
                                && a.getMasterId() != null && a.getFilename() != null) {
                            a.setEventId(eventId);
                            a.setCreatedAt(new Date());
                            a.setUpdatedAt(new Date());
                            session.save(a);
                        }
                    }
                    session.getTransaction().commit();
                }

            } catch (Exception e) {
                session.getTransaction().rollback();
                hm.addStatusMessage(new StatusMessage("addAttachments", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
                log.error(e);
            }
        }

    }

    @Autowired
    @Override
    @SuppressWarnings("unchecked")
    public void getJurisdiction(HealthMessage hm) {
        Session session = null;
        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (!thList.isEmpty()) {

                session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                for (TrisanoHealth th : thList) {
                    Integer eventId = th.getEventId();

                    if (eventId != null && eventId > 0) {
                        Query qParticipations = session.getNamedQuery("Participations.findByEventIdType");
                        qParticipations.setInteger("eventId", eventId);
                        qParticipations.setString("type", "Jurisdiction");
                        List<Participations> pl = qParticipations.list();
                        if (!pl.isEmpty()) {
                            Participations jPart = pl.get(0);
                            Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                            qPlaces.setInteger("entityId", jPart.getSecondaryEntityId());
                            List<Places> pList = qPlaces.list();
                            if (pList != null && !pList.isEmpty()) {
                                Places place = pList.get(0);
                                List<JurisdictionAttributes> jaList = new ArrayList<>();
                                JurisdictionAttributes ja = new JurisdictionAttributes();
                                ja.setPlace(place);
                                jaList.add(ja);
                                th.setJurisdictionAttributes(jaList);
                            }
                        }
                    }
                }
                session.getTransaction().commit();
            }

        } catch (Exception e) {
            session.getTransaction().rollback();
            hm.addStatusMessage(new StatusMessage("getJurisdiction", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            log.error(e);
        }

    }

    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void updateAddressesGeocode(HealthMessage hm) {
        Session session = null;
        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (!thList.isEmpty()) {

                session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                for (TrisanoHealth th : thList) {

                    List<Addresses> addresses = th.getAllAddresses();
                    if (addresses != null && !addresses.isEmpty()) {

                        for (Addresses a : addresses) {
                            Query qAddresses = session.getNamedQuery("Addresses.findById");
                            qAddresses.setInteger("id", a.getId());
                            List<Addresses> aListFound = qAddresses.list();
                            for (Addresses addyFound : aListFound) {
                                if (addyFound.getLongitude() == null || addyFound.getLatitude() == null) {

                                    addyFound.setLatitude(a.getLatitude());
                                    addyFound.setLongitude(a.getLongitude());
                                    addyFound.setUpdatedAt(new Date());
                                    session.update(addyFound);
                                }

                            }
                        }
                    }
                }
                session.getTransaction().commit();
            }

        } catch (Exception e) {
            session.getTransaction().rollback();
            hm.addStatusMessage(new StatusMessage("updateAddressesGeocode", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            log.error(e);
        }

    }

    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void updateWorkflowState(HealthMessage hm) {

        TrisanoHealth th = hm.getFirstTrisanoHealth();
        boolean addUpdateSuccess = hm.addUpdateSuccess();

        if (th != null) {
            String wfs = th.getWorkflowState();
            Integer eventId = th.getEventId();

            if ((th.getEventType().equals(Type.MORBIDITY_EVENT.getName()) ||
                 th.getEventType().equals(Type.ASSESSMENT_EVENT.getName()))
                    && eventId > 0
                    && wfs != null
                    && addUpdateSuccess) {

                log.info("UPDATE workflowState to [" + wfs + "] for CMR... [eventId=" + eventId + "]");

                Session session = null;
                try {

                    session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                    session.beginTransaction();

                    Query qEvents = session.getNamedQuery("Events.findById");
                    qEvents.setInteger("id", eventId);

                    List<Events> events = qEvents.list();

                    if (events != null && !events.isEmpty()) {

                        Events updateEvent = events.get(0);
                        updateEvent.setWorkflowState(wfs);
                        session.update(updateEvent);

                    }
                    session.getTransaction().commit();

                } catch (Exception e) {
                    session.getTransaction().rollback();
                    hm.addStatusMessage(new StatusMessage("error in update workflowState", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
                    log.error(e);
                }
                hm.addStatusMessage(new StatusMessage("updateWorkflowState", Status.SUCCESS.toString(), "event_id=" + eventId));
                log.info("UPDATED workflowState to [" + wfs + "] for CMR... [eventId=" + eventId + "]");
            }
        }
    }

    /**
     * *
     * In this version all inserts or updates to trisano nedss are handled
     * through the TrisanoWebClient
     */
    @Override
    @Autowired
    public void updateCmr(HealthMessage hm) {
//        Session session = null;
//        try {
//
//            List<TrisanoHealth> thList = hm.getTrisanoHealth();
//            if (!thList.isEmpty()) {
//
//                session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
//                session.beginTransaction();
//
//
//                for (TrisanoHealth th : thList) {
//                }
//                session.getTransaction().commit();
        //
//                thList.clear();
//                hm.setStatus(Status.FAILURE_UNSUPPORTED.toString());
//
//
//            }
//
//
//        } catch (Exception e) {
//            session.getTransaction().rollback();
//            hm.setStatusMessage(e.getMessage());
//            log.error(e);
//        }
//
    }

    /**
     * Performs a search based on a person's first name, last name and birth
     * date. Depending on the criteria supplied, this function performs partial
     * matches and soundex matches. The result set is ordered by calculating the
     * edit distance between name and birth date fields.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set as search criteria. The HealthMessage parameter
     * is modified to include all result data. Return data includes data for all
     * of the matching person's events. If errors or exceptions take place
     * information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void searchPerson(HealthMessage hm) {
        Integer minScoreDefault = 70;
        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (!thList.isEmpty()) {

                // search criteria
                TrisanoHealth th = thList.get(0);

                SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                List<InterestedPartyAttributes> ipaList = th.getInterestedPartyAttributes();

                if (ipaList == null || ipaList.isEmpty()) {
                    hm.addStatusMessage(new StatusMessage("searchPerson", Status.FAILURE_MISSING_DATA.toString(), "interested_party missing"));
                    throw new Exception("Search person failed. interested_party element missing in search xml.");
                }

                InterestedPartyAttributes ipa = ipaList.get(0);

                People p = ipa.getPerson();
                if (p == null) {
                    hm.addStatusMessage(new StatusMessage("searchPerson", Status.FAILURE_MISSING_DATA.toString(), "person missing"));
                    throw new Exception("Search person failed. person element missing in search xml.");
                }

                String fName = p.getFirstName();
                String lName = p.getLastName();
                String bDateString = null;
                Integer minScore = p.getMatchScore();

                Date bDate = p.getBirthDate();
                if (bDate != null) {
                    bDateString = dateformatYYYYMMDD.format(p.getBirthDate());
                }
                if (fName == null) {
                    fName = "";
                } else {
                    fName = fName.trim();
                }
                if (lName == null) {
                    lName = "";
                } else {
                    lName = lName.trim();
                }

                Query qSearch;

                if (minScore == null || minScore < 1) {
                    minScore = minScoreDefault;
                }

                if (bDate == null) {
                    qSearch = session.getNamedQuery("People.searchName");
                    qSearch.setString("firstName", fName + "%");
                    qSearch.setString("lastName", lName + "%");
                } else {
                    qSearch = session.getNamedQuery("People.searchNameBday");
                    qSearch.setString("firstName", fName + "%");
                    qSearch.setString("lastName", lName + "%");
                    qSearch.setDate("birthDate", bDate);
                    qSearch.setString("firstNameScore", fName);
                    qSearch.setString("lastNameScore", lName);
                    qSearch.setDate("birthDateScore", bDate);
                    qSearch.setInteger("minScore", minScore);

                }

                Query qSoundex = session.getNamedQuery("People.searchSoundex");
                if (fName != null && fName.length() > 0) {
                    qSoundex.setString("firstName", fName);
                    qSoundex.setString("firstNameFirstChar", fName.substring(0, 1));
                } else {
                    qSoundex.setString("firstName", "");
                    qSoundex.setString("firstNameFirstChar", "");
                }
                if (lName != null && lName.length() > 0) {
                    qSoundex.setString("lastName", lName);
                    qSoundex.setString("lastNameFirstChar", lName.substring(0, 1));
                } else {
                    qSoundex.setString("lastName", "");
                    qSoundex.setString("lastNameFirstChar", "");
                }

                qSoundex.setString("firstNameScore", fName);
                qSoundex.setString("lastNameScore", lName);
                qSoundex.setDate("birthDateScore", bDate);
                qSoundex.setInteger("minScore", minScore);

                List<People> personsFound = (List<People>) qSearch.list();

                // seems to not be matching c to k...
                // flip names
                // filter encounter events
                List<People> personsSoundex = (List<People>) qSoundex.list();
                for (People ps : personsSoundex) {
                    if (!personsFound.contains(ps)) {
                        personsFound.add(ps);
                    }
                }

                List<People> removePeople = new ArrayList<>();
                TrisanoHealth t;
                for (People pFound : personsFound) {
                    // result data
                    t = new TrisanoHealth();

                    InterestedPartyAttributes ipaFound
                            = new InterestedPartyAttributes();
                    ipaFound.setPerson(pFound);

                    Query qEvents;
                    if (th.hasDiseaseFilter()) {
                        qEvents = session.getNamedQuery("Events.findByEntityIdMorbidityAssessmentContactDiseases");
                    } else {
                        qEvents = session.getNamedQuery("Events.findByEntityIdMorbidityAssessmentContact");
                    }
                    qEvents.setInteger("entityId", pFound.getEntityId());
                    if (th.hasDiseaseFilter()) {
                        qEvents.setParameterList("diseaseNames", th.getDiseaseNames());
                    }
                    Boolean eventFound = false;
                    List<Events> eventsList = qEvents.list();
                    for (Events event : eventsList) {
                        Query qDiseaseEvents = session.getNamedQuery("DiseaseEvents.findByEventId");
                        qDiseaseEvents.setInteger("eventId", event.getId());
                        List<DiseaseEvents> diseaseEvents = qDiseaseEvents.list();
                        for (DiseaseEvents de : diseaseEvents) {
                            Integer dId = de.getDiseaseId();
                            if (dId != null && dId > 0) {
                                eventFound = true;
                                Query qDiseases = session.getNamedQuery("Diseases.findById");
                                qDiseases.setInteger("id", dId);
                                List<Diseases> dList = qDiseases.list();
                                Diseases d = dList.get(0);
                                t.addDisease(d);
                                t.addDiseaseEvent(de);
                                t.addEvent(event);
                            }
                        }
                    }
                    if (eventFound) {
                        Query qTelephones = session.getNamedQuery("Telephones.findByEntityId");
                        qTelephones.setInteger("entityId", pFound.getEntityId());
                        List<Telephones> telephones = qTelephones.list();
                        ipaFound.setTelephones(telephones);

                        List<InterestedPartyAttributes> ipaFoundList
                                = new ArrayList<>();
                        ipaFoundList.add(ipaFound);
                        t.setInterestedPartyAttributes(ipaFoundList);

                        Query qJurisPlace = session.getNamedQuery("Places.findByParticipationTypeEntityId");
                        qJurisPlace.setInteger("entityId", pFound.getEntityId());
                        qJurisPlace.setString("type", Type.JURISDICTION.getName());
                        List<Places> jurisPlaces = qJurisPlace.list();
                        List<JurisdictionAttributes> jaList = new ArrayList<>();
                        if (jurisPlaces != null && !jurisPlaces.isEmpty()) {
                            Places jp = jurisPlaces.get(0);
                            JurisdictionAttributes ja = new JurisdictionAttributes();
                            ja.setPlace(jp);
                            jaList.add(ja);
                        }
                        t.setJurisdictionAttributes(jaList);

                        Query qAddresses = session.getNamedQuery("Addresses.findByEntityId");
                        qAddresses.setInteger("entityId", pFound.getEntityId());
                        List<Addresses> addressesList = qAddresses.list();
                        t.setAddresses(addressesList);

                        hm.addTrisanoHealth(t);
                    } else {
                        removePeople.add(pFound);
                    }
                }

                for (People pr : removePeople) {
                    personsFound.remove(pr);
                }

                for (People pFound : personsFound) {
                    SQLQuery q = session.createSQLQuery("SELECT person_score(:fnIn,:fnMatch,:lnIn,:lnMatch,:bdIn,:bdMatch) as score");
                    q.setString("fnIn", fName);
                    q.setString("fnMatch", pFound.getFirstName());
                    q.setString("lnIn", lName);
                    q.setString("lnMatch", pFound.getLastName());
                    q.setDate("bdIn", bDate);
                    q.setDate("bdMatch", pFound.getBirthDate());
                    Object scr = q.list().get(0);
                    if (scr instanceof Integer) {
                        pFound.setMatchScore((Integer) scr);
                    }
                }

                Collections.sort(personsFound);

                session.getTransaction().commit();
                hm.addStatusMessage(new StatusMessage("searchPerson", Status.SUCCESS.toString(), null));
                thList.remove(th);

            }

        } catch (Exception e) {
            hm.addStatusMessage(new StatusMessage("searchPerson", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            log.error(e);
        }

    }

    /**
     * *
     * Gets the Report based on provided Report object.
     *
     * @param hm HealthMessage object. The HealthMessage object including the
     * Report. The HealthMessage.CaseStatusReport parameter is modified to
     * include all report data. If errors or exceptions take place information
     * is included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Override
    @Autowired
    public void getCareTimeReport(HealthMessage hm) {

        if (hm.getTrisanoHealth() != null && !hm.getTrisanoHealth().isEmpty()) {

            List<CareTimeReport> ctrList = hm.getTrisanoHealth().get(0).getCareTimeReport();

            Connection con = null;

            try {
                con = TrisanoConnect.getSQLConnection();

                for (CareTimeReport ctr : ctrList) {

                    getEventsInCare(con, ctr);
                    getEventsNotInCare(con, ctr);
                    ctr.prepareReport();
                    if (ctr.getGetData() != null && !ctr.getGetData()) {
                        ctr.setCareData(null);
                    }
                }

            } catch (Exception e) {
                log.error(e);
                hm.addStatusMessage(new StatusMessage("getCareTimeReport", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException se) {
                        log.error(se);
                    }

                }
            }

        }

        hm.addStatusMessage(new StatusMessage("getCareTimeReport", Status.SUCCESS.toString(), null));
    }

    private void getEventsInCare(Connection con, CareTimeReport ctr) throws Exception {

        Date start = ctr.getStart();
        Date end = ctr.getEnd();
        List<Diseases> diseases = ctr.getDiseases();

        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append("events.id, events.event_onset_date, disease_events.date_diagnosed,");
        sb.append("lab_results.collection_date, lab_results.test_type_id, lab_results.result_value, lab_results.units ");
        sb.append("FROM disease_events, events, participations, lab_results ");
        sb.append("WHERE ");
        sb.append("disease_events.event_id = events.id ");
        sb.append("AND event.type = ? ");
        sb.append("AND participations.event_id = events.id ");
        sb.append("AND participations.id = lab_results.participation_id ");
        if (diseases != null && !diseases.isEmpty()) {
            sb.append("AND disease_events.disease_id IN(");
            for (Diseases d : diseases) {
                if (d.getId() != null && d.getId() > 0) {
                    sb.append("?,");
                }
            }
            sb.append("0)");
        }
        sb.append("AND lab_results.test_type_id IN (176,177) ");
        if (start != null) {
            sb.append("AND disease_events.date_diagnosed >= ? ");
        }
        if (end != null) {
            sb.append("AND disease_events.date_diagnosed <= ? ");
        }

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sb.toString());

            int i = 1;

            ps.setString(i++, Type.MORBIDITY_EVENT.getName());

            if (diseases != null && !diseases.isEmpty()) {
                for (Diseases d : diseases) {
                    if (d.getId() != null && d.getId() > 0) {
                        ps.setInt(i++, d.getId());
                    }
                }
            }

            if (start != null) {
                ps.setDate(i++, new java.sql.Date(start.getTime()));
            }
            if (end != null) {
                ps.setDate(i++, new java.sql.Date(end.getTime()));
            }
            rs = ps.executeQuery();

            List<CareData> cDataList = new ArrayList<>();
            while (rs.next()) {
                // one row
                CareData cd = new CareData();
                cd.setInCare(Boolean.TRUE);
                cd.setEventOnsetDate(rs.getDate("event_onset_date"));
                cd.setDateDiagnosed(rs.getDate("date_diagnosed"));
                cd.setDateCollected(rs.getDate("collection_date"));
                cd.setEventId(rs.getInt("id"));
                cd.setTest(rs.getString("test_type_id"));
                cd.setUnits(rs.getString("units"));
                cd.setValue(rs.getString("result_value"));
                cDataList.add(cd);

            }

            ctr.addCareData(cDataList);

        } catch (Exception e) {
            log.error(e);
            throw e;
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException se) {
                log.error(se);
            }
        }

    }

    private void getEventsNotInCare(Connection con, CareTimeReport ctr) throws Exception {

        Date start = ctr.getStart();
        Date end = ctr.getEnd();
        List<Diseases> diseases = ctr.getDiseases();

        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append("events.id, events.event_onset_date, disease_events.date_diagnosed ");

        sb.append("FROM disease_events, events ");
        sb.append("WHERE ");
        sb.append("disease_events.event_id = events.id ");
        sb.append("AND NOT exists(SELECT * FROM lab_results, participations WHERE participations.event_id = events.id AND participations.id = lab_results.participation_id ");
        sb.append("AND lab_results.test_type_id IN (176,177)) ");
        if (diseases != null && !diseases.isEmpty()) {
            sb.append("AND disease_events.disease_id IN(");
            for (Diseases d : diseases) {
                if (d.getId() != null && d.getId() > 0) {
                    sb.append("?,");
                }
            }
            sb.append("0)");
        }

        if (start != null) {
            sb.append("AND disease_events.date_diagnosed >= ? ");
        }
        if (end != null) {
            sb.append("AND disease_events.date_diagnosed <= ? ");
        }

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sb.toString());

            int i = 1;

            if (diseases != null && !diseases.isEmpty()) {
                for (Diseases d : diseases) {
                    if (d.getId() != null && d.getId() > 0) {
                        ps.setInt(i++, d.getId());
                    }
                }
            }
            if (start != null) {
                ps.setDate(i++, new java.sql.Date(start.getTime()));
            }
            if (end != null) {
                ps.setDate(i++, new java.sql.Date(end.getTime()));
            }
            rs = ps.executeQuery();

            List<CareData> cDataList = new ArrayList<>();
            while (rs.next()) {
                // one row
                CareData cd = new CareData();
                cd.setInCare(Boolean.FALSE);
                cd.setEventOnsetDate(rs.getDate("event_onset_date"));
                cd.setDateDiagnosed(rs.getDate("date_diagnosed"));
                cd.setEventId(rs.getInt("id"));
                cDataList.add(cd);

            }
            ctr.setNotInCare(cDataList.size());
            ctr.addCareData(cDataList);

        } catch (Exception e) {
            log.error(e);
            throw e;
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException se) {
                log.error(se);
            }
        }

    }

    /**
     * *
     * Gets the CaseStatusReport based on provided CaseStatusReport object.
     *
     * @param hm HealthMessage object. The HealthMessage object including the
     * CaseStatusReport. The HealthMessage.CaseStatusReport parameter is
     * modified to include all report data. If errors or exceptions take place
     * information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    public void getCaseStatusReport(HealthMessage hm) {

        if (hm.getTrisanoHealth() != null && !hm.getTrisanoHealth().isEmpty()) {

            List<CaseStatusReport> csrList = hm.getTrisanoHealth().get(0).getCaseStatusReport();

            Places place;
            Date dos;
            Date doe;
            Date dds;
            Date dde;

            for (CaseStatusReport csr : csrList) {

                place = csr.getPlace();
                dos = csr.getDiseaseOnsetStart();
                doe = csr.getDiseaseOnsetEnd();
                dds = csr.getDateDiagnosedStart();
                dde = csr.getDateDiagnosedEnd();

                Connection con = null;

                try {

                    con = TrisanoConnect.getSQLConnection();

                    StringBuilder sb = new StringBuilder("SELECT ");

                    List<CaseStatusCount> stateStatusList = csr.getStateCaseStatus();
                    List<CaseStatusCount> lhdStatusList = csr.getLhdCaseStatus();
                    String diseaseIdList = csr.getDiseaseIds();
                    if (lhdStatusList != null && !lhdStatusList.isEmpty()) {
                        for (CaseStatusCount lhds : lhdStatusList) {
                            String status = lhds.getStatus();
                            if (status != null && status.trim().length() > 0) {
                                sb.append("sum(case when e.lhd_case_status_id = '");
                                sb.append(status);
                                sb.append("' then 1 else 0 end ) as \"lhd_");
                                sb.append(status);
                                sb.append("\",");
                            }
                        }
                    }
                    if (stateStatusList != null && !stateStatusList.isEmpty()) {
                        for (CaseStatusCount stateds : lhdStatusList) {
                            String status = stateds.getStatus();
                            if (status != null && status.trim().length() > 0) {
                                sb.append("sum(case when e.lhd_case_status_id = '");
                                sb.append(status);
                                sb.append("' then 1 else 0 end ) as \"state_");
                                sb.append(status);
                                sb.append("\",");
                            }
                        }
                    }

                    sb.append("count(*) as cases ");
                    sb.append("from disease_events de, events e ");
                    sb.append("where de.event_id = e.id ");

                    if (diseaseIdList != null) {
                        sb.append("and de.disease_id in(");
                        sb.append(diseaseIdList);
                        sb.append(") ");
                    }

                    if (dos != null && doe != null) {
                        sb.append("and de.disease_onset_date >= ? and de.disease_onset_date <= ? ");
                    }
                    if (dds != null && dde != null) {
                        sb.append("and de.date_diagnosed >= ? and de.date_diagnosed <= ? ");
                    }
                    if (place != null && place.getId() != null && place.getId() > 0) {
                        sb.append("and de.event_id in ");
                        sb.append("(select event_id from participations where secondary_entity_id in ");
                        sb.append("(select entity_id from places where id = ?");
                        sb.append("))");
                    } else if (place != null && place.getName() != null) {
                        sb.append("and de.event_id in ");
                        sb.append("(select event_id from participations where secondary_entity_id in ");
                        sb.append("(select entity_id from places where name ilike ?");
                        sb.append("))");
                    }

                    PreparedStatement ps = null;
                    ResultSet rs = null;
                    try {

                        ps = con.prepareStatement(sb.toString());

                        int i = 1;

                        if (dos != null && doe != null) {
                            ps.setDate(i++, new java.sql.Date(dos.getTime()));
                            ps.setDate(i++, new java.sql.Date(doe.getTime()));
                        }
                        if (dds != null && dde != null) {
                            ps.setDate(i++, new java.sql.Date(dds.getTime()));
                            ps.setDate(i++, new java.sql.Date(dde.getTime()));
                        }
                        if (place != null && place.getId() != null && place.getId() > 0) {
                            ps.setInt(i++, place.getId());
                        } else if (place != null && place.getName() != null) {
                            ps.setString(i++, place.getName());
                        }
                        rs = ps.executeQuery();

                        while (rs.next()) {

                            if (lhdStatusList != null && !lhdStatusList.isEmpty()) {
                                for (CaseStatusCount lhds : lhdStatusList) {
                                    String status = lhds.getStatus();
                                    if (status != null && status.trim().length() > 0) {
                                        lhds.setCount(rs.getString("lhd_" + status));
                                    }
                                }
                            }
                            if (stateStatusList != null && !stateStatusList.isEmpty()) {
                                for (CaseStatusCount stateds : stateStatusList) {
                                    String status = stateds.getStatus();
                                    if (status != null && status.trim().length() > 0) {
                                        stateds.setCount(rs.getString("state_" + status));
                                    }
                                }
                            }

                        }

                    } catch (Exception e) {
                        log.error(e);
                        throw e;
                    } finally {
                        try {
                            rs.close();
                            ps.close();
                        } catch (SQLException se) {
                            log.error(se);
                        }
                    }

                } catch (Exception e) {
                    log.error(e);
                    hm.addStatusMessage(new StatusMessage("getCaseStatusReport", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException se) {
                            log.error(se);
                        }

                    }
                }

            }
            hm.addStatusMessage(new StatusMessage("getCaseStatusReport", Status.SUCCESS.toString(), null));
        } else {
            hm.addStatusMessage(new StatusMessage("getCaseStatusReport", Status.FAILURE_MISSING_DATA.toString(), "criteria missing"));
        }

    }

    /**
     * *
     * Gets the DiseaseReport based on provided DiseaseReport object.
     *
     * @param hm HealthMessage object. The HealthMessage object including the
     * DiseaseReport. The HealthMessage.DiseaseReport parameter is modified to
     * include all report data. If errors or exceptions take place information
     * is included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Override
    @Autowired
    public void getDiseaseReport(HealthMessage hm) {

        if (hm.getTrisanoHealth() != null && !hm.getTrisanoHealth().isEmpty()) {

            List<DiseaseReport> drList = hm.getTrisanoHealth().get(0).getDiseaseReport();

            Places place;
            Date cas;
            Date cae;
            Date dds;
            Date dde;

            for (DiseaseReport dr : drList) {

                place = dr.getPlace();
                cas = dr.getCreatedAtStart();
                cae = dr.getCreatedAtEnd();
                dds = dr.getDateDiagnosedStart();
                dde = dr.getDateDiagnosedEnd();

                Connection con = null;

                try {

                    con = TrisanoConnect.getSQLConnection();

                    StringBuilder sb = new StringBuilder("SELECT ");

                    List<DiseaseCount> diseaseCounts = dr.getDiseaseCount();

                    if (diseaseCounts != null && !diseaseCounts.isEmpty()) {
                        for (DiseaseCount dc : diseaseCounts) {

                            Diseases d = dc.getDisease();

                            if (d != null && d.getId() != null && d.getId() > 0) {

                                sb.append("sum(case when de.disease_id = ");
                                sb.append(d.getId());
                                sb.append(" then 1 else 0 end ) as \"");
                                sb.append(d.getId());
                                sb.append("\",");

                            }
                        }
                    } else {
                        throw new Exception("Unable to run DiseaseReport. diseaseCount required");
                    }

                    sb.append("count(*) as diseases ");
                    sb.append("from disease_events de, events e ");
                    sb.append("where de.event_id = e.id ");
                    // only morbidity events
                    sb.append("and e.type = ? ");

                    if (cas != null && cae != null) {
                        sb.append("and e.created_at >= ? and e.created_at <= ? ");
                    }
                    if (dds != null && dde != null) {
                        sb.append("and de.date_diagnosed >= ? and de.date_diagnosed <= ? ");
                    }

                    if (place != null && place.getId() != null && place.getId() > 0) {
                        sb.append("and de.event_id in ");
                        sb.append("(select event_id from participations where secondary_entity_id in ");
                        sb.append("(select entity_id from places where id = ?");
                        sb.append("))");
                    } else if (place != null && place.getName() != null) {
                        sb.append("and de.event_id in ");
                        sb.append("(select event_id from participations where secondary_entity_id in ");
                        sb.append("(select entity_id from places where name ilike ?");
                        sb.append("))");
                    }

                    PreparedStatement ps = null;
                    ResultSet rs = null;
                    try {

                        ps = con.prepareStatement(sb.toString());

                        int i = 1;

                        ps.setString(i++, Type.MORBIDITY_EVENT.getName());

                        if (cas != null && cae != null) {
                            ps.setDate(i++, new java.sql.Date(cas.getTime()));
                            ps.setDate(i++, new java.sql.Date(cae.getTime()));
                        }
                        if (dds != null && dde != null) {
                            ps.setDate(i++, new java.sql.Date(dds.getTime()));
                            ps.setDate(i++, new java.sql.Date(dde.getTime()));
                        }
                        if (place != null && place.getId() != null && place.getId() > 0) {
                            ps.setInt(i++, place.getId());
                        } else if (place != null && place.getName() != null) {
                            ps.setString(i++, place.getName());
                        }
                        rs = ps.executeQuery();

                        while (rs.next()) {

                            for (DiseaseCount dc : diseaseCounts) {

                                Diseases d = dc.getDisease();
                                dc.setCount(rs.getString(d.getId()));

                            }

                        }

                    } catch (Exception e) {
                        log.error(e);
                        throw e;
                    } finally {
                        try {
                            rs.close();
                            ps.close();
                        } catch (SQLException se) {
                            log.error(se);
                        }
                    }

                } catch (Exception e) {
                    log.error(e);
                    hm.addStatusMessage(new StatusMessage("getDiseaseReport", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException se) {
                            log.error(se);
                        }

                    }
                }

            }
            hm.addStatusMessage(new StatusMessage("getDiseaseReport", Status.SUCCESS.toString(), null));
        } else {
            hm.addStatusMessage(new StatusMessage("getDiseaseReport", Status.FAILURE_MISSING_DATA.toString(), "criteria missing"));
        }

    }

    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void findAnswers(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                TrisanoHealth th = thList.get(0);

                Integer eventId = th.getEventId();
                Integer diseaseId = th.getFirstDiseaseId();
                List<String> questionShortNames = th.getQuestionShortNames();

                Query q = null;
                if (eventId > 0) {
                    if (!questionShortNames.isEmpty()) {

                        q = session.getNamedQuery("Answers.findByEventIdAndQuestionShortNames");
                        q.setInteger("eventId", eventId);
                        q.setParameterList("questionShortNames", questionShortNames);

                    } else {
                        q = session.getNamedQuery("Answers.findByEventId");
                        q.setInteger("eventId", eventId);

                    }
                }

                if (q != null) {

                    List<Answers> list = q.list();
                    for (Answers a : list) {

                        Query qq = session.getNamedQuery("Questions.findById");
                        qq.setInteger("id", a.getQuestionId());
                        List<Questions> qList = qq.list();
                        if (!qList.isEmpty()) {
                            String sn = qList.get(0).getShortName();
                            a.setQuestionShortName(sn);
                        }

                    }

                    th.setAnswers(list);

                    session.getTransaction().commit();
                    hm.addStatusMessage(new StatusMessage("findAnswers", Status.SUCCESS.toString(), null));

                }

            } else {
                hm.addStatusMessage(new StatusMessage("findAnswers", Status.FAILURE_MISSING_DATA.toString(), "trisano_health missing"));
            }

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("findAnswers", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Gets the Roles based on provided uid in the Users object.
     *
     * @param hm HealthMessage object. The HealthMessage object including the
     * DiseaseReport. The HealthMessage.DiseaseReport parameter is modified to
     * include all report data. If errors or exceptions take place information
     * is included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void getUserRoles(HealthMessage hm) {
        if (hm.getTrisanoHealth() != null && !hm.getTrisanoHealth().isEmpty()) {

            List<Users> uList = hm.getTrisanoHealth().get(0).getUsers();

            if (uList != null && !uList.isEmpty()) {
                List<Users> foundUsers = new ArrayList<Users>();
                for (Users u : uList) {

                    if (u != null && u.getUid() != null && u.getUid().trim().length() > 0) {

                        String uid = u.getUid();
                        Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                        session.beginTransaction();

                        try {

                            Query q = session.getNamedQuery("Users.findByUid");
                            q.setString("uid", uid);
                            List<Users> fu = q.list();
                            if (fu.isEmpty()) {
                                hm.addStatusMessage(new StatusMessage("getUserRoles", Status.FAILURE_EXCEPTION.toString(), "no user found for uid=" + uid));
                            } else {

                                Users user = fu.get(0);
                                Integer userId;
                                userId = user.getId();
                                foundUsers.add(user);
                                q = session.getNamedQuery("RoleMemberships.findByUserId");
                                q.setInteger("userId", userId);
                                List<RoleMemberships> rmList = q.list();
                                hm.getTrisanoHealth().get(0).setRoleMemberships(rmList);
                                hm.addStatusMessage(new StatusMessage("getUserRoles", Status.SUCCESS.toString(), null));
                            }
                        } catch (Exception e) {
log.error("TODO JAY ZZZZZZZZZZZZZZZZZZZZ in getUserRoles() ERROR ERROR :"+e.toString());
                            log.error(e);
                            hm.addStatusMessage(new StatusMessage("getUserRoles", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));

                        } finally {
                            session.getTransaction().commit();
                        }

                    } else {
log.error("TODO JAY ZZZZZZZZZZZZZZZZZZZZ in getUserRoles() FAIL FAIL :"+"users.id missing 1");
                        hm.addStatusMessage(new StatusMessage("getUserRoles", Status.FAILURE_MISSING_DATA.toString(), "users.id missing"));
                    }
                }
                hm.getTrisanoHealth().get(0).addUsers(foundUsers);
            } else {
log.error("TODO JAY ZZZZZZZZZZZZZZZZZZZZ in getUserRoles() FAIL FAIL :"+"users.id missing 2");
                hm.addStatusMessage(new StatusMessage("getUserRoles", Status.FAILURE_MISSING_DATA.toString(), "users.id missing"));
            }

        } else {
log.error("TODO JAY ZZZZZZZZZZZZZZZZZZZZ in getUserRoles() FAIL FAIL :"+"trisano_health missing");
            hm.addStatusMessage(new StatusMessage("getUserRoles", Status.FAILURE_MISSING_DATA.toString(), "trisano_health missing"));
        }
    }

    /**
     * *
     * Gets Users based on provided roles and jurisdictions.
     *
     * @param hm HealthMessage object. The HealthMessage object including the
     * DiseaseReport. The HealthMessage.DiseaseReport parameter is modified to
     * include all report data. If errors or exceptions take place information
     * is included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void getUsers(HealthMessage hm) {

        if (hm.getTrisanoHealth() != null && !hm.getTrisanoHealth().isEmpty()) {

            Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
            session.beginTransaction();

            TrisanoHealth th = hm.getTrisanoHealth().get(0);

            List<RoleMemberships> rmList = th.getRoleMemberships();

            for (RoleMemberships rm : rmList) {
                Integer rId = rm.getRoleId();
                Integer jId = rm.getJurisdictionId();
                Integer uId = rm.getUserId();
                Query q = null;
                if (rId != null && rId > 0 && jId != null && jId > 0) {

                    q = session.getNamedQuery("RoleMemberships.findByRoleIdAndJurisdictionId");
                    q.setInteger("roleId", rId);
                    q.setInteger("jurisdictionId", jId);

                } else if (rId != null && rId > 0) {

                    q = session.getNamedQuery("RoleMemberships.findByRoleId");
                    q.setInteger("roleId", rId);

                } else if (jId != null && jId > 0) {

                    q = session.getNamedQuery("RoleMemberships.findByJurisdictionId");
                    q.setInteger("jurisdictionId", jId);

                } else if (uId != null && uId > 0) {

                    q = session.getNamedQuery("RoleMemberships.findByUserId");
                    q.setInteger("userId", uId);

                }
                if (q != null) {

                    try {
                        List<RoleMemberships> foundRmList = q.list();
                        hm.getTrisanoHealth().get(0).setRoleMemberships(foundRmList);

                        List<Users> uniqueList = new ArrayList<Users>();
                        for (RoleMemberships frm : foundRmList) {

                            q = session.getNamedQuery("Users.findById");
                            q.setInteger("id", frm.getUserId());

                            List<Users> foundUsers = q.list();

                            if (foundUsers != null && !foundUsers.isEmpty()) {
                                for (Users fu : foundUsers) {
                                    if (!uniqueList.contains(fu)) {
                                        uniqueList.add(fu);
                                    }
                                }
                            }
                        }
                        for (Users fu : uniqueList) {
                            UsersAttributes ua = new UsersAttributes();
                            ua.setUser(fu);

                            q = session.getNamedQuery("EmailAddresses.findByOwnerIdAndOwnerType");
                            q.setInteger("ownerId", fu.getId());
                            q.setString("ownerType", "User");
                            List<EmailAddresses> addys = q.list();
                            ua.setEmailAddresses(addys);
                            th.addUsersAttribute(ua);
                        }
                        hm.addStatusMessage(new StatusMessage("getUsers", Status.SUCCESS.toString(), null));

                    } catch (Exception e) {

                        log.error(e);
                        hm.addStatusMessage(new StatusMessage("getUsers", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
                    } finally {
                        session.getTransaction().commit();
                    }
                }
            }

        } else {
            hm.addStatusMessage(new StatusMessage("getUsers", Status.FAILURE_MISSING_DATA.toString(), "trisano_health missing"));
        }

    }

    /**
     * *
     * Gets the Roles based on provided uid in the Users object.
     *
     * @param hm HealthMessage object. The HealthMessage object including the
     * DiseaseReport. The HealthMessage.DiseaseReport parameter is modified to
     * include all report data. If errors or exceptions take place information
     * is included in the healthMessage.status and healthMessage.statusMessage
     * attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void getRoles(HealthMessage hm) {

        List<TrisanoHealth> thlist = new ArrayList<>();
        TrisanoHealth th = new TrisanoHealth();
        thlist.add(th);
        hm.setTrisanoHealth(thlist);

        Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
        session.beginTransaction();

        try {

            Query q = session.getNamedQuery("Roles.findAll");
            List<Users> fr = q.list();
            if (fr.isEmpty()) {
                hm.addStatusMessage(new StatusMessage("getRoles", Status.FAILURE_EXCEPTION.toString(), "no roles found in database"));
                throw new Exception("Roles Not Found.");

            }

            List<Roles> rList = q.list();
            th.setRoles(rList);
            hm.addStatusMessage(new StatusMessage("getRoles", Status.SUCCESS.toString(), null));

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("getRoles", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        } finally {
            session.getTransaction().commit();
        }

    }

    /**
     * *
     * Collects the database table data for various tables. Refer to the source
     * code to see supported classes and tables.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.dump_table with the name of the table data to return. The
     * HealthMessage parameter is modified to include all result data. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    public void exportTreatmentNotes(HealthMessage hm) {

        List<Notes> notes = new ArrayList<>();

        String qry
                = "SELECT 'Treatment started: ' || external_codes.code_description || "
                + "' - Treatment Date: ' || coalesce(cast(participations_treatments.treatment_date as varchar),'') || "
                + "' - Treatment: ' || treatments.treatment_name || "
                + "' - Treatment Stop Date:' || coalesce(cast(participations_treatments.stop_treatment_date as varchar),'') AS note,"
                + "268 as user_id,participations.event_id as event_id,'clinical' as note_type "
                + "FROM participations_treatments, treatments, participations, external_codes "
                + "WHERE participations_treatments.treatment_id = treatments.id "
                + "AND participations_treatments.participation_id = participations.id "
                + "AND external_codes.id = participations_treatments.treatment_given_yn_id "
                + "ORDER BY participations.event_id";

        try {

            Connection con = null;

            try {
                con = TrisanoConnect.getSQLConnection();

                PreparedStatement ps = null;
                ResultSet rs = null;
                try {

                    ps = con.prepareStatement(qry);

                    rs = ps.executeQuery();

                    while (rs.next()) {

                        Notes note = new Notes();
                        note.setCreatedAt(new Date());
                        note.setEventId(rs.getInt("event_id"));
                        note.setNote(rs.getString("note"));
                        note.setUserId(rs.getInt("user_id"));
                        note.setNoteType(rs.getString("note_type"));
                        note.setUserId(rs.getInt("user_id"));
                        notes.add(note);

                    }

                } catch (Exception e) {
                    log.error(e);
                    throw e;
                } finally {
                    try {
                        rs.close();
                        ps.close();
                    } catch (SQLException se) {
                        log.error(se);
                    }
                }

            } catch (Exception e) {
                log.error(e);
                hm.addStatusMessage(new StatusMessage("exportTreatmentNotes", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException se) {
                        log.error(se);
                    }

                }
            }

            // consolidate notes per event
            List<Notes> newNotes = new ArrayList<>();
            Integer lastId = 0;
            Notes note = null;

            for (Notes n : notes) {

                Integer eventId = n.getEventId();

                if (eventId.equals(lastId)) {

                    note.setNote(note.getNote() + "\n" + n.getNote());

                } else {

                    newNotes.add(note);
                    note = n;

                }

                lastId = eventId;
            }

            // save consolidated notes
            if (!newNotes.isEmpty()) {
                Session session = null;
                Notes testN = null;
                try {

                    session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                    session.beginTransaction();

                    for (Notes n : newNotes) {

                        if (n != null && n.getUserId() != null && n.getEventId() != null) {
                            session.save(n);
                        }

                    }
                    //session.getTransaction().commit();
                    session.getTransaction().rollback();

                } catch (Exception e) {
                    log.error(e + "\n" + testN);
                    session.getTransaction().rollback();

                } finally {
                }
            }
            hm.addStatusMessage(new StatusMessage("exportTreatmentNotes", Status.SUCCESS.toString(), null));
        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("exportTreatmentNotes", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));

        }

    }

    /**
     * *
     * Finds a people based on provided People object with name and type set.
     *
     * @param clin People object. The People object including first name, last
     * name and person_type The HealthMessage parameter is modified to include
     * all result data. Return data includes original data supplied to function
     * with id values set for found people. If errors or exceptions take place
     * information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    @SuppressWarnings("unchecked")
    public static void findPersonType(People clin) {

        try {

            Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
            session.beginTransaction();

            if (clin != null) {
                Query qClinician = session.getNamedQuery("People.findByNameType");
                qClinician.setString("firstName", clin.getFirstName());
                qClinician.setString("lastName", clin.getLastName());
                qClinician.setString("personType", clin.getPersonType());
                List<People> clinList = qClinician.list();

                if (!clinList.isEmpty()) {
                    clin.setId(clinList.get(0).getId());
                }
            }

            session.getTransaction().commit();

        } catch (Exception e) {
            log.error(e);
        }

    }

    /**
     * *
     * Finds a places based on provided Places object with name set.
     *
     * @param p Place object. The Place object including name. The HealthMessage
     * parameter is modified to include all result data. Return data includes
     * original data supplied to function with id values set for found place. If
     * errors or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @SuppressWarnings("unchecked")
    public static void findPlaces(Places p) {

        try {

            Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
            session.beginTransaction();

            Query qPlaces = session.getNamedQuery("Places.findByName");
            qPlaces.setString("name", p.getName());

            List<Places> jPlaces = qPlaces.list();
            if (jPlaces.size() > 0) {
                p.setId(jPlaces.get(0).getId());
                p.setEntityId(jPlaces.get(0).getEntityId());
            }

            session.getTransaction().commit();

        } catch (Exception e) {
            log.error(e);
        }

    }

    /**
     * *
     * Finds a participations based on provided Participations object with
     * eventId and type set.
     *
     * @param p Place object. The Place object including name. The HealthMessage
     * parameter is modified to include all result data. Return data includes
     * original data supplied to function with id values set for found place. If
     * errors or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @SuppressWarnings("unchecked")
    public static void findParticipations(Participations p) {

        try {

            Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
            session.beginTransaction();

            Query qParticipations = session.getNamedQuery("Participations.findByEventIdType");
            qParticipations.setInteger("eventId", p.getEventId());
            qParticipations.setString("type", p.getType());

            List<Participations> jParticipations = qParticipations.list();
            if (jParticipations.size() > 0) {
                p.setId(jParticipations.get(0).getId());
                p.setPrimaryEntityId(jParticipations.get(0).getPrimaryEntityId());
                p.setSecondaryEntityId(jParticipations.get(0).getSecondaryEntityId());
            }

            session.getTransaction().commit();

        } catch (Exception e) {
            log.error(e);
        }

    }

    @SuppressWarnings("unchecked")
    public static void findPlaceAttributes(PlaceAttributes pa, HealthMessage hm) {

        try {

            Places p = pa.getPlace();
            if (p != null) {

                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                String name = p.getName();

                if (name != null && name.trim().length() > 0) {

                    Query qPlaces = session.getNamedQuery("Places.findByNameNotDeleted");
                    qPlaces.setString("name", p.getName());

                    List<Places> places = qPlaces.list();
                    if (!places.isEmpty()) {
                        Places place = places.get(0);
                        p.setId(place.getId());
                        p.setEntityId(place.getEntityId());

                        Query qAddress = session.getNamedQuery("Addresses.findByEntityId");
                        qAddress.setInteger("entityId", place.getEntityId());
                        List<Addresses> addyList = qAddress.list();
                        if (!addyList.isEmpty()) {
                            pa.setAddresses(addyList);
                        }
                    }

                }

                session.getTransaction().commit();
            }

        } catch (Exception e) {
            hm.addStatusMessage(new StatusMessage("findPlaceAttributes", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            log.error(e);
        }

    }

    /**
     * *
     * Modifies supplied HealthMessage object to a similar state as expected by
     * the addCmr web service function. Used by caller to provide and example of
     * compatible xml format for addCmr.
     *
     * @param hm HealthMessage object. The HealthMessage parameter is modified
     * to include example of an object to be added to trisano. If errors or
     * exceptions take place information is included in the healthMessage.status
     * and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    public void getHealthDataExample(HealthMessage hm) {
        TrisanoHealth th = new TrisanoHealth();

        List<InterestedPartyAttributes> ipaList = new ArrayList<>();
        InterestedPartyAttributes ipa = new InterestedPartyAttributes();
        People p = new People();

        GregorianCalendar gc = new GregorianCalendar(1971, 2, 12);
        p.setBirthDate(gc.getTime());
        p.setFirstName("fnpatient");
        p.setLastName("lnpatient");
        p.setMiddleName("mnpatient");
        ipa.setPerson(p);

        PeopleRaces pr = new PeopleRaces();
        pr.setPeopleRacesPK(new PeopleRacesPK(9, 9));
        List<PeopleRaces> praces = new ArrayList<>();
        praces.add(pr);
        ipa.setPeopleRaces(praces);

        List<ParticipationsRiskFactors> rfaList = new ArrayList<>();
        ParticipationsRiskFactors prf = new ParticipationsRiskFactors();
        prf.setPregnantId(136);
        prf.setPregnancyDueDate(new Date());
        prf.setFoodHandlerId(136);
        prf.setHealthcareWorkerId(136);
        prf.setGroupLivingId(136);
        prf.setDayCareAssociationId(136);
        prf.setOccupation("occupation");
        prf.setRiskFactors("riskfactors");
        prf.setRiskFactorsNotes("riskfactorsnotes");
        rfaList.add(prf);
        ipa.setRiskFactorAttributes(rfaList);

        Telephones t = new Telephones();
        t.setAreaCode("999");
        t.setCountryCode("");
        t.setEntityLocationTypeId(9);
        t.setExtension("");
        t.setPhoneNumber("1234567");
        List<Telephones> tphones = new ArrayList<>();
        tphones.add(t);
        ipa.setTelephones(tphones);

        List<ParticipationsTreatments> taList = new ArrayList<>();
        ParticipationsTreatments ta = new ParticipationsTreatments();
        ta.setParticipationId(9);
        ta.setStopTreatmentDate(new Date());
        ta.setTreatmentDate(new Date());
        ta.setTreatmentGivenYnId(9);
        ta.setTreatmentId(9);
        taList.add(ta);
        ipa.setTreatmentsAttributes(taList);

        ipaList.add(ipa);
        th.setInterestedPartyAttributes(ipaList);

        List<ReporterAttributes> raList = new ArrayList<>();
        ReporterAttributes ra = new ReporterAttributes();
        People r = new People();
        r.setBirthDate(new Date());
        r.setFirstName("fnreporter");
        r.setLastName("lnreporter");
        r.setMiddleName("mnreporter");
        ra.setReporter(r);
        ra.setTelephones(null);
        raList.add(ra);
        th.setReporterAttributes(raList);

        List<CliniciansAttributes> caList = new ArrayList<>();
        CliniciansAttributes ca = new CliniciansAttributes();
        People c = new People();
        c.setBirthDate(new Date());
        c.setFirstName("fnclinician");
        c.setLastName("lnclinician");
        c.setMiddleName("mnclinician");
        c.setPersonType("clinician");
        ca.setClinician(c);

        Telephones ct = new Telephones();
        ct.setAreaCode("999");
        ct.setCountryCode("");
        ct.setEntityLocationTypeId(9);
        ct.setExtension("");
        ct.setPhoneNumber("1234567");
        List<Telephones> ctphones = new ArrayList<>();
        ctphones.add(t);
        ca.setTelephones(ctphones);
        caList.add(ca);
        th.setCliniciansAttributes(caList);

        Events event = new Events();
        event.setCdcUpdatedAt(new Date());
        event.setEventName("");
        event.setEventOnsetDate(new Date());
        event.setEventQueueId(9);
        event.setFirstReportedPHdate(new Date());
        event.setResultsReportedToClinicianDate(new Date());
        event.setIbisUpdatedAt(new Date());
        event.setInvestigatorId(9);
        event.setLhdCaseStatusId(9);
        event.setMMWRweek(99);
        event.setMMWRyear(99);
        event.setParentGuardian("");
        event.setType(Type.MORBIDITY_EVENT.getName());
        event.setWorkflowState("RE-LOOKUP");

        event.setOtherData1("otherdata1");
        event.setOtherData2("otherdata2");

        th.addEvent(event);

        List<JurisdictionAttributes> jaList = new ArrayList<>();
        JurisdictionAttributes ja = new JurisdictionAttributes();
        Places p2 = new Places();
        p2.setName("Salt Lake Valley Health Department");
        p2.setId(70);
        ja.setPlace(p2);
        jaList.add(ja);
        th.setJurisdictionAttributes(jaList);

        List<ReportingAgencyAttributes> raaList = new ArrayList<>();
        ReportingAgencyAttributes raa = new ReportingAgencyAttributes();

        Places p3 = new Places();
        p3.setName("Tanner Clinic");
        raa.setPlace(p3);
        raaList.add(raa);
        th.setReportingAgencyAttributes(raaList);

        List<DiagnosticFacilitiesAttributes> dfaList = new ArrayList<>();
        DiagnosticFacilitiesAttributes dfa = new DiagnosticFacilitiesAttributes();
        Places p4 = new Places();
        p4.setName("PPAUSL");
        dfa.setPlace(p4);
        dfaList.add(dfa);
        th.setDiagnosticFacilitiesAttributes(dfaList);

        List<HospitalizationFacilitiesAttributes> hfaList = new ArrayList<>();
        HospitalizationFacilitiesAttributes hfa = new HospitalizationFacilitiesAttributes();

        Places p5 = new Places();
        p5.setName("Herfordshire Clinic");

        HospitalsParticipations hp = new HospitalsParticipations();
        hp.setAdmissionDate(new Date());
        hp.setDischargeDate(new Date());
        hp.setHospitalRecordNumber("12345");
        hp.setMedicalRecordNumber("54321");

        hfa.setPlace(p5);
        hfa.setHospitalsParticipations(hp);
        hfaList.add(hfa);
        th.setHospitalizationFacilitiesAttributes(hfaList);

        DiseaseEvents de = new DiseaseEvents();
        de.setHospitalizedId(136);
        de.setDiseaseId(93);//Rubella
        de.setDiseaseOnsetDate(new Date());
        de.setDateDiagnosed(new Date());
        List<DiseaseEvents> deList = new ArrayList<>();
        deList.add(de);
        th.setDiseaseEvents(deList);

        List<FormReferences> frList = new ArrayList<>();
        FormReferences fr = new FormReferences();
        fr.setFormId(99);
        fr.setTemplateId(99);
        frList.add(fr);
        th.setFormReferences(frList);

        List<Addresses> alist = new ArrayList<>();
        Addresses a1 = new Addresses();
        a1.setCity("Sandy");
        a1.setCountyId(9);
        a1.setPostalCode("84070");
        a1.setStateId(99);
        a1.setStreetName("8200 S 700 E");
        alist.add(a1);
        th.setAddresses(alist);

        List<LabResults> labList = new ArrayList<>();
        LabResults lab = new LabResults();

        lab.setSpecimenSourceId(99);
        lab.setCollectionDate(new Date());
        lab.setLabTestDate(new Date());

        lab.setComment("");
        lab.setLoincCode("loinccode");
        lab.setReferenceRange("referencerange");
        lab.setResultValue("resultvalue");

        lab.setTestResultId(9);
        lab.setTestTypeId(9);
        lab.setTestStatusId(9);
        lab.setUnits("");
        lab.setOrganismId(9);
        labList.add(lab);

        List<LabsAttributes> laList = new ArrayList<>();
        LabsAttributes la = new LabsAttributes();
        Places p1 = new Places();
        p1.setName("RE-TEST LAB");
        la.setPlace(p1);
        la.setLabResults(labList);
        laList.add(la);
        th.setLabsAttributes(laList);

        List<ContactChildEventsAttributes> cceaList = new ArrayList<>();
        ContactChildEventsAttributes ccea = new ContactChildEventsAttributes();
        InterestedPartyAttributes ipaCcea = new InterestedPartyAttributes();
        People contact = new People();
        contact.setFirstName("contactfn");
        contact.setLastName("contactln");
        ipaCcea.setPerson(contact);

        Telephones cTelephones = new Telephones();
        cTelephones.setAreaCode("999");
        cTelephones.setPhoneNumber("1234567");
        cTelephones.setEntityLocationTypeId(9);
        List<Telephones> cPhones = new ArrayList<>();
        cPhones.add(cTelephones);
        ipaCcea.setTelephones(cPhones);

        ccea.setInterestedPartyAttributes(ipaCcea);
        ParticipationsContacts pc = new ParticipationsContacts();
        pc.setContactTypeId(9);
        pc.setDispositionId(9);
        ccea.setParticipationsContacts(pc);

        cceaList.add(ccea);
        th.setContactChildEventsAttributes(cceaList);

        Notes notes = new Notes();
        notes.setNote("RE-ADDED NOTES");
        notes.setNoteType("RE-administrative");
        th.setNotes(notes);

        List<TrisanoHealth> thgen = new ArrayList<>();
        thgen.add(th);
        hm.setTrisanoHealth(thgen);
        hm.addStatusMessage(new StatusMessage("getHealthDataExample", Status.SUCCESS.toString(), null));

    }

    /**
     * *
     * Collects the database id values for the objects supplied. Refer to the
     * source code to see supported classes and tables.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data set for id lookup. The HealthMessage parameter is
     * modified to include all result data. Return data includes original data
     * supplied to function with id values set for supported tables. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void findId(HealthMessage hm) {
        try {
            Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
            session.beginTransaction();

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            for (TrisanoHealth th : thList) {

                List<Codes> codes = th.getCodes();
                if (codes != null) {
                    for (Codes c : codes) {
                        Query q = session.getNamedQuery("Codes.findByCodeCodeName");
                        q.setString("theCode", c.getTheCode());
                        q.setString("codeName", c.getCodeName());
                        List<Codes> fc = q.list();
                        if (fc.isEmpty()) {
                            c.setId(-1);
                        } else {
                            c.setId(fc.get(0).getId());
                        }
                    }
                }
                List<CommonTestTypes> commonTestTypes = th.getCommonTestTypes();
                if (commonTestTypes != null) {
                    for (CommonTestTypes c : commonTestTypes) {
                        Query q = session.getNamedQuery("CommonTestTypes.findByCommonName");
                        String encodedCommonName = c.getCommonName();
                        q.setString("commonName", URLDecoder.decode(encodedCommonName, "UTF-8"));

                        List<CommonTestTypes> f = q.list();
                        if (f.isEmpty()) {
                            c.setId(-1);
                        } else {
                            c.setId(f.get(0).getId());
                        }
                    }
                }
                List<Diseases> diseases = th.getDiseases();
                if (diseases != null) {
                    for (Diseases d : diseases) {
                        Query q = session.getNamedQuery("Diseases.findByDiseaseName");
                        String encodedDiseaseName = d.getDiseaseName();
                        q.setString("diseaseName", URLDecoder.decode(encodedDiseaseName, "UTF-8"));
String TODO = q.getQueryString();
                        List<Diseases> f = q.list();
                        if (f.isEmpty()) {
                            d.setId(-1);
                        } else {
                            d.setId(f.get(0).getId());
                        }
                    }
                }
                List<ExternalCodes> externalCodes = th.getExternalCodes();
                if (externalCodes != null) {
                    for (ExternalCodes e : externalCodes) {
                        Query q = session.getNamedQuery("ExternalCodes.findByCodeCodeNameCodeDescription");
                        q.setString("theCode", e.getTheCode());
                        q.setString("codeName", e.getCodeName());
                        q.setString("codeDescription", e.getCodeDescription());
                        List<ExternalCodes> f = q.list();
                        if (f.isEmpty()) {
                            e.setId(-1);
                        } else {
                            e.setId(f.get(0).getId());
                            e.setTheCode(f.get(0).getTheCode());
                            e.setCodeName(f.get(0).getCodeName());
                            e.setCodeDescription(f.get(0).getCodeDescription());
                        }
                    }
                }
                List<Organisms> organisms = th.getOrganisms();
                if (organisms != null) {
                    for (Organisms o : organisms) {
                        Query q = session.getNamedQuery("Organisms.findByOrganismName");

                        String encodedOrganismName = o.getOrganismName();
                        q.setString("organismName", URLDecoder.decode(encodedOrganismName, "UTF-8"));
                        List<Organisms> f = q.list();
                        if (f.isEmpty()) {
                            o.setId(-1);
                        } else {
                            o.setId(f.get(0).getId());
                        }
                    }
                }

                List<Treatments> treatments = th.getTreatments();
                if (treatments != null) {
                    for (Treatments t : treatments) {
                        Query q = session.getNamedQuery("Treatments.findByTreatmentName");

                        String treatmentName = t.getTreatmentName();
                        q.setString("treatmentName", URLDecoder.decode(treatmentName, "UTF-8"));
                        List<Treatments> f = q.list();
                        if (f.isEmpty()) {
                            t.setId(-1);
                        } else {
                            t.setId(f.get(0).getId());
                        }
                    }
                }

            }

            session.getTransaction().commit();
            hm.addStatusMessage(new StatusMessage("findId", Status.SUCCESS.toString(), null));

        } catch (Exception e) {
log.error("TODO Jay ERROR ERROR XXXXXXXXXXXXXX  in findId() "+e.toString());

            log.error(e);
            hm.addStatusMessage(new StatusMessage("findId", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Collects the database table data for various tables. Refer to the source
     * code to see supported classes and tables.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.dump_table with the name of the table data to return. The
     * HealthMessage parameter is modified to include all result data. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void getTableData(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                TrisanoHealth th = thList.get(0);

                String tableName = th.getDumpTable();
                Query q;

                if (tableName != null) {
                    switch (tableName) {
                        case "test_result":
                        case "case":
                        case "specimen":
                        case "loinc_scale": {
                            q = session.getNamedQuery("ExternalCodes.findByCodeName");
                            q.setString("codeName", tableName);
                            List<ExternalCodes> list = q.list();
                            StringBuilder sb = new StringBuilder();
                            for (ExternalCodes ec : list) {
                                sb.append(ec.getId());
                                sb.append("=");
                                sb.append(ec.getCodeDescription());
                                sb.append(" (");
                                sb.append(ec.getTheCode());
                                sb.append(")");
                                sb.append("|");
                            }
                            th.setDumpTable(sb.toString());
                            break;
                        }
                        case "external_codes": {
                            q = session.getNamedQuery("ExternalCodes.findAll");
                            List<ExternalCodes> list = q.list();
                            th.setExternalCodes(list);
                            //                        StringBuilder sb = new StringBuilder();
                            //                        for (ExternalCodes ec : list) {
                            //                            sb.append(ec.getId());
                            //                            sb.append("=");
                            //                            sb.append(ec.getCodeDescription());
                            //                            sb.append(" (");
                            //                            sb.append(ec.getTheCode());
                            //                            sb.append(")");
                            //                            sb.append("|");
                            //                        }
                            //                        th.setDumpTable(sb.toString());

                            //                     ExternalCodes codeById = th.getExternalCodeById(214);
                            //                     ExternalCodes codeByDesc = th.getExternalCodeByDescription("Serology - IgM");
                            //                     ExternalCodes codeByTheCode = th.getExternalCodeByTheCode("IgM");
                            //
                            //               String sbr = "asdf";
                            break;
                        }
                        case "codes": {
                            q = session.getNamedQuery("Codes.findAll");
                            List<Codes> list = q.list();
                            StringBuilder sb = new StringBuilder();
                            for (Codes c : list) {
                                sb.append(c.getId());
                                sb.append("=");
                                sb.append(c.getCodeDescription());
                                sb.append(" (");
                                sb.append(c.getTheCode());
                                sb.append(")");
                                sb.append("|");
                            }
                            th.setDumpTable(sb.toString());
                            break;
                        }
                        case "diseases": {
                            q = session.getNamedQuery("Diseases.findAllActive");
                            List<Diseases> list = q.list();
                            StringBuilder sb = new StringBuilder();
                            for (Diseases d : list) {
                                sb.append(d.getId());
                                sb.append("=");
                                sb.append(d.getDiseaseName());
                                sb.append("|");
                            }
                            th.setDumpTable(sb.toString());
                            break;
                        }
                        case "organisms": {
                            q = session.getNamedQuery("Organisms.findAll");
                            List<Organisms> list = q.list();
                            StringBuilder sb = new StringBuilder();
                            for (Organisms o : list) {
                                sb.append(o.getId());
                                sb.append("=");
                                sb.append(o.getOrganismName());
                                sb.append("|");
                            }
                            th.setDumpTable(sb.toString());
                            break;
                        }
                        case "common_test_types": {
                            q = session.getNamedQuery("CommonTestTypes.findAll");
                            List<CommonTestTypes> list = q.list();
                            StringBuilder sb = new StringBuilder();
                            for (CommonTestTypes ctt : list) {
                                sb.append(ctt.getId());
                                sb.append("=");
                                sb.append(ctt.getCommonName());
                                sb.append("|");
                            }
                            th.setDumpTable(sb.toString());
                            break;
                        }
                        default:
                            hm.addStatusMessage(new StatusMessage("getTableData", Status.FAILURE_EXCEPTION.toString(), "Unsupported table name [" + tableName + "]"));
                            break;
                    }
                    session.getTransaction().commit();
                    hm.addStatusMessage(new StatusMessage("getTableData", Status.SUCCESS.toString(), null));
                } else {
                    hm.addStatusMessage(new StatusMessage("getTableData", Status.FAILURE_MISSING_DATA.toString(), "dump_table is missing"));
                }

            } else {
                hm.addStatusMessage(new StatusMessage("getTableData", Status.FAILURE_MISSING_DATA.toString(), "trisano_health is missing"));
                throw new Exception("TrisanoHealth object not found in request.");
            }

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("getTableData", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));

        }

    }

    /**
     * *
     * Finds the forms for a given disease.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.disease with the ids set for forms to return. The
     * HealthMessage parameter is modified to include all result data. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void findQuestions(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();

                TrisanoHealth th = thList.get(0);

                List<Diseases> dList = th.getDiseases();
                if (dList != null && !dList.isEmpty()
                        && dList.get(0) != null && dList.get(0).getId() != null
                        && dList.get(0).getId() > 0) {

                    Integer diseaseId = dList.get(0).getId();
                    Query q;

                    q = session.getNamedQuery("Questions.findByDiseaseId");
                    q.setInteger("diseaseId", diseaseId);

                    List<Questions> list = q.list();
                    th.setQuestions(list);

                    session.getTransaction().commit();
                    hm.addStatusMessage(new StatusMessage("findQuestions", Status.SUCCESS.toString(), null));

                } else {
                    hm.addStatusMessage(new StatusMessage("findQuestions", Status.FAILURE_MISSING_DATA.toString(), "disease.id missing"));
                }

            } else {
                hm.addStatusMessage(new StatusMessage("findQuestions", Status.FAILURE_MISSING_DATA.toString(), "trisano_health missing"));
            }

        } catch (Exception e) {
            log.error(e);
            hm.addStatusMessage(new StatusMessage("findQuestions", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Returns all active diseases
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.disease with the ids set for forms to return. The
     * HealthMessage parameter is modified to include all result data. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void getDiseases(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null) {

                thList = new ArrayList<>();
            }
            hm.setTrisanoHealth(thList);
            Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
            session.beginTransaction();

            TrisanoHealth th = new TrisanoHealth();

            Query q = session.getNamedQuery("Diseases.findAllActive");

            List<Diseases> list = q.list();
            th.setDiseases(list);
            thList.add(th);
            session.getTransaction().commit();
            hm.addStatusMessage(new StatusMessage("getDiseases", Status.SUCCESS.toString(), null));

        } catch (Exception e) {
            log.error(e);
            hm.addStatusMessage(new StatusMessage("getDiseases", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Creates (in the trisano health object passed) missing formRerences
     * objects. FormReferences objects are created for the answers submitted
     * where the formReferences for the answer.questionId does not exist in the
     * db.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.event[0].id and trisano_health.answers[0].questionId.
     *
     * If errors or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void addAnswers(HealthMessage hm) {

        List<TrisanoHealth> thList = hm.getTrisanoHealth();
        TrisanoHealth th = thList.get(0);

        try {
            Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
            session.beginTransaction();

            Query qry;

            // get formReferences for the event
            Integer eventId = th.getEventId();
            if (eventId > 0) {

                List<FormReferences> frList = new ArrayList<>();
                qry = session.getNamedQuery("FormReferences.findByEventId");
                qry.setInteger("eventId", eventId);
                frList.addAll(qry.list());

                // find all froms for questions
                List<Answers> answers = th.getAnswers();
                List<Forms> frForms = new ArrayList<>();

                if (answers != null && !answers.isEmpty()) {

                    //get formreference by event id                
                    for (FormReferences fr : frList) {
                        qry = session.getNamedQuery("Forms.findById");
                        qry.setInteger("id", fr.getFormId());
                        List<Forms> tfList = qry.list();
                        if (!tfList.isEmpty()) {
                            frForms.addAll(qry.list());
                        }
                    }

                    for (Answers a : answers) {
                        if (a.getId() != null && a.getId() > 0) {
                            qry = session.getNamedQuery("Answers.findById");
                            qry.setInteger("id", a.getId());
                            List<Answers> alist = qry.list();
                            if (!alist.isEmpty()) {
                                Answers answ = alist.get(0);
                                if (answ != null) {
                                    answ.setTextAnswer(a.getTextAnswer());
                                    answ.setUpdatedAt(new Date());
                                    session.save(answ);
                                }
                            }
                        } else {
                            if (a.hasShortNames()) {
                                Forms foundForm = null;
                                for (Forms f : frForms) {
                                    if (f.getShortName() != null && a.getFormShortName().toLowerCase().equals(f.getShortName().toLowerCase())) {
                                        foundForm = f;
                                        break;
                                    }
                                }
                                if (foundForm != null) {

                                    qry = session.getNamedQuery("Questions.findByShortNameFormId");
                                    qry.setString("shortName", a.getQuestionShortName());
                                    qry.setInteger("formId", foundForm.getId());
                                    List<Questions> foundQuestionList = qry.list();
                                    if (!foundQuestionList.isEmpty()) {
                                        Questions q = foundQuestionList.get(0);
                                        a.setQuestionId(q.getId());
                                    } else {
                                        log.error("Question not found for short_name=" + a.getQuestionShortName() + ", form_id=" + foundForm.getId());
                                    }
                                } else {
                                    // no reference get the live form
                                    qry = session.getNamedQuery("Forms.findByShortNameLive");
                                    qry.setString("shortName", a.getFormShortName());
                                    List<Forms> foundShortNameForms = qry.list();
                                    if (!foundShortNameForms.isEmpty()) {
                                        foundForm = foundShortNameForms.get(0);
                                        if (foundForm != null) {

                                            //add the form reference as there was none found
                                            FormReferences newFr = new FormReferences();
                                            newFr.setCreatedAt(new Date());
                                            newFr.setEventId(eventId);
                                            newFr.setFormId(foundForm.getId());
                                            newFr.setTemplateId(foundForm.getTemplateId());
                                            session.save(newFr);

                                            qry = session.getNamedQuery("Questions.findByShortNameFormId");
                                            qry.setString("shortName", a.getQuestionShortName());
                                            qry.setInteger("formId", foundForm.getId());
                                            List<Questions> foundQuestionList = qry.list();
                                            if (!foundQuestionList.isEmpty()) {
                                                Questions q = foundQuestionList.get(0);
                                                a.setQuestionId(q.getId());
                                            } else {
                                                log.error("Question not found for short_name=" + a.getQuestionShortName() + ", form_id=" + foundForm.getId());
                                            }
                                        }
                                    } else {
                                        log.error("Live form not found for short_name=" + a.getFormShortName());
                                    }
                                }
                            }
                            if (a.getQuestionId() != null && a.getQuestionId() > 0) {
                                qry = session.getNamedQuery("Answers.findByEventIdAndQuestionId");
                                qry.setInteger("eventId", eventId);
                                qry.setInteger("questionId", a.getQuestionId());
                                List<Answers> fAnswerList = qry.list();
                                Answers ans = null;
                                if (!fAnswerList.isEmpty()) {
                                    ans = fAnswerList.get(0);
                                }
                                if (ans == null) {
                                    ans = new Answers();
                                    ans.setQuestionId(a.getQuestionId());
                                    ans.setEventId(eventId);
                                    ans.setCreatedAt(new Date());
                                } else {
                                    ans.setUpdatedAt(new Date());
                                }
                                ans.setTextAnswer(a.getTextAnswer());
                                session.save(ans);
                            } else {
                                //TODO more info
                                log.error("no question id found");
                            }
                        }
                    }
                }

                session.getTransaction().commit();

            }
        } catch (HibernateException e) {
            log.error("Unable to save form answers.", e);
        }

    }

    /**
     * *
     * Exports all forms and related formElements, diseasesforms and questions.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.disease with the ids set for forms to return. The
     * HealthMessage parameter is modified to include all result data. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void exportForms(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                TrisanoHealth th = thList.get(0);
                List<FormsAttributes> faList = th.getFormsAttributes();

                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();
                Query q = session.getNamedQuery("Forms.findAll");

                if (faList != null && !faList.isEmpty()
                        && faList.get(0) != null && faList.get(0).getForm() != null) {

                    Forms findForm = faList.get(0).getForm();
                    String shortName = findForm.getShortName();
                    String name = findForm.getName();
                    Integer jurisId = findForm.getJurisdictionId();
                    if (shortName != null && shortName.trim().length() > 0) {

                        q = session.getNamedQuery("Forms.findByShortName");
                        q.setString("shortName", shortName);

                    } else if (name != null && name.trim().length() > 0) {

                        q = session.getNamedQuery("Forms.findByName");
                        q.setString("name", name);

                    } else if (jurisId != null && jurisId > 0) {

                        q = session.getNamedQuery("Forms.findByJurisdictionId");
                        q.setInteger("jurisdictionId", jurisId);

                    }
                } else {

                    faList = new ArrayList<>();
                }

                List<Forms> forms = q.list();
                for (Forms form : forms) {

                    FormsAttributes fa = new FormsAttributes();
                    fa.setForm(form);

                    q = session.getNamedQuery("DiseasesForms.findByFormId");
                    q.setInteger("formId", form.getId());
                    List<DiseasesForms> diseaseForms = q.list();
                    fa.setDiseasesForms(diseaseForms);

                    q = session.getNamedQuery("FormElements.findByFormId");
                    q.setInteger("formId", form.getId());
                    List<FormElements> formElements = q.list();
                    fa.setFormElements(formElements);

                    List<Questions> questions = new ArrayList<>();
//                    for (FormElements fe : formElements) {
//
//                        q = session.getNamedQuery("Questions.findByFormElementId");
//                        q.setInteger("formElementId", fe.getId());
//                        List<Questions> qlist = q.list();
//                        for (Questions qu : qlist) {
//                            questions.add(qu);
//                        }
//
//                    }
                    fa.setFormElements(formElements);
                    fa.setQuestions(questions);
                    faList.add(fa);
                }

                session.getTransaction().commit();
                hm.addStatusMessage(new StatusMessage("exportForms", Status.SUCCESS.toString(), null));

            } else {
                hm.addStatusMessage(new StatusMessage("exportForms", Status.FAILURE_MISSING_DATA.toString(), "trisano_health missing"));
            }

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("exportForms", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Exports all forms with no other data.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.disease with the ids set for forms to return. The
     * HealthMessage parameter is modified to include all result data. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void findForms(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                TrisanoHealth th = thList.get(0);
                Boolean includeQuestions = th.getIncludeQuestions();

                String formShortName = th.getFirstFormShortName();

                List<Integer> dids = th.getDiseaseIds();
                List<FormsAttributes> faList = new ArrayList<>();

                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();
                Query qForms = null;

                if (!dids.isEmpty()) {
                    qForms = session.getNamedQuery("Forms.findByDiseaseIds");
                    qForms.setParameterList("diseaseIds", dids);
                } else if (formShortName != null) {
                    qForms = session.getNamedQuery("Forms.findByShortNameLive");
                    qForms.setString("shortName", formShortName);
                } else {
                    qForms = session.getNamedQuery("Forms.findAllLive");
                }

                List<Forms> forms = qForms.list();
                for (Forms form : forms) {
                    FormsAttributes fa = new FormsAttributes();
                    fa.setForm(form);

                    if (includeQuestions) {
                        Query qQuestions = session.getNamedQuery("Questions.findByFormId");
                        qQuestions.setInteger("formId", form.getId());
                        List<Questions> qList = qQuestions.list();
                        fa.setQuestions(qList);
                    }

                    faList.add(fa);
                }
                th.setFormsAttributes(faList);
                session.getTransaction().commit();
                hm.addStatusMessage(new StatusMessage("getForms", Status.SUCCESS.toString(), null));

            } else {
                hm.addStatusMessage(new StatusMessage("getForms", Status.FAILURE_MISSING_DATA.toString(), "trisano_health missing"));
            }

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("exportForms", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Exports all forms and related formElements, diseasesforms and questions.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health.disease with the ids set for forms to return. The
     * HealthMessage parameter is modified to include all result data. If errors
     * or exceptions take place information is included in the
     * healthMessage.status and healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void getDiseaseForms(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                TrisanoHealth th = thList.get(0);
                List<FormsAttributes> faList = th.getFormsAttributes();

                Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                session.beginTransaction();
                Query q = session.getNamedQuery("Forms.findAll");

                if (faList != null && !faList.isEmpty()
                        && faList.get(0) != null && faList.get(0).getForm() != null) {

                    Forms findForm = faList.get(0).getForm();
                    String shortName = findForm.getShortName();
                    String name = findForm.getName();
                    Integer jurisId = findForm.getJurisdictionId();
                    if (shortName != null && shortName.trim().length() > 0) {

                        q = session.getNamedQuery("Forms.findByShortName");
                        q.setString("shortName", shortName);

                    } else if (name != null && name.trim().length() > 0) {

                        q = session.getNamedQuery("Forms.findByName");
                        q.setString("name", name);

                    } else if (jurisId != null && jurisId > 0) {

                        q = session.getNamedQuery("Forms.findByJurisdictionId");
                        q.setInteger("jurisdictionId", jurisId);

                    }
                } else {

                    faList = new ArrayList<>();
                }

                List<Forms> forms = q.list();
                for (Forms form : forms) {

                    FormsAttributes fa = new FormsAttributes();
                    fa.setForm(form);

                    q = session.getNamedQuery("DiseasesForms.findByFormId");
                    q.setInteger("formId", form.getId());
                    List<DiseasesForms> diseaseForms = q.list();
                    fa.setDiseasesForms(diseaseForms);

                    q = session.getNamedQuery("FormElements.findByFormId");
                    q.setInteger("formId", form.getId());
                    List<FormElements> formElements = q.list();
                    fa.setFormElements(formElements);

                    List<Questions> questions = new ArrayList<>();
//                    for (FormElements fe : formElements) {
//
//                        q = session.getNamedQuery("Questions.findByFormElementId");
//                        q.setInteger("formElementId", fe.getId());
//                        List<Questions> qlist = q.list();
//                        for (Questions qu : qlist) {
//                            questions.add(qu);
//                        }
//
//                    }
                    fa.setFormElements(formElements);
                    fa.setQuestions(questions);
                    faList.add(fa);
                }

                session.getTransaction().commit();
                hm.addStatusMessage(new StatusMessage("exportForms", Status.SUCCESS.toString(), null));

            } else {
                hm.addStatusMessage(new StatusMessage("exportForms", Status.FAILURE_MISSING_DATA.toString(), "trisano_health missing"));
            }

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("exportForms", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
        }

    }

    /**
     * *
     * Finds case data within a range of dates or updated since last export to
     * be exported to cdc.
     *
     * @param hm HealthMessage object. The healthMessage object including
     * trisano_health data with events.id set. The HealthMessage parameter is
     * modified to include all result data. Return data includes data for a
     * single event including some associated data. If errors or exceptions take
     * place information is included in the healthMessage.status and
     * healthMessage.statusMessage attributes.
     */
    @Override
    @Autowired
    @SuppressWarnings("unchecked")
    public void getCdcExport(HealthMessage hm) {

        try {

            List<TrisanoHealth> thList = hm.getTrisanoHealth();
            if (thList != null && !thList.isEmpty()) {

                TrisanoHealth th = thList.get(0);
                CdcExport cdcExport = th.getCdcExport();
                thList.remove(th);

                if (cdcExport != null) {

                    Integer startWeek = cdcExport.getStartWeek();
                    Integer startYear = cdcExport.getStartYear();
                    Integer endWeek = cdcExport.getEndWeek();
                    Integer endYear = cdcExport.getEndYear();
                    Date lastExport = cdcExport.getLastExport();
                    List<DiseaseCriteria> dCriteria = cdcExport.getDiseaseCriteria();

                    if (lastExport != null || (startWeek != null && startYear != null && endWeek != null && endYear != null)) {

                        Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
                        session.beginTransaction();

                        Query qEvents;
                        if (lastExport != null) {
                            qEvents = session.getNamedQuery("Events.getCdcDataSinceLastExport");
                        } else {
                            qEvents = session.getNamedQuery("Events.getCdcDataInRange");
                        }

                        if (dCriteria != null && !dCriteria.isEmpty()) {

                            StringBuilder qsb = new StringBuilder(qEvents.getQueryString());
                            qsb.append(" AND (");

                            for (DiseaseCriteria dc : dCriteria) {
                                if (dc.getDiseaseId() != null && dc.getDiseaseId() > 0
                                        && dc.getStateCaseStatusId() != null && dc.getStateCaseStatusId() > 0) {
                                    qsb.append("(de.diseaseId=");
                                    qsb.append(dc.getDiseaseId());
                                    qsb.append(" AND ");
                                    qsb.append("e.stateCaseStatusId=");
                                    qsb.append(dc.getStateCaseStatusId());
                                    qsb.append(") OR ");
                                }
                            }
                            qsb.delete(qsb.lastIndexOf("OR"), qsb.length());
                            qsb.append(")");

                            qEvents = session.createQuery(qsb.toString());

                        }

                        if (lastExport != null) {
                            qEvents.setDate("lastExport", lastExport);

                        } else {

                            qEvents.setInteger("startWeek", startWeek);
                            qEvents.setInteger("startYear", startYear);
                            qEvents.setInteger("endWeek", endWeek);
                            qEvents.setInteger("endYear", endYear);
                        }
                        qEvents.setString("eventType", Type.MORBIDITY_EVENT.getName());

                        List<Events> eventsList = qEvents.list();
                        for (Events event : eventsList) {

                            TrisanoHealth t = new TrisanoHealth();
                            hm.addTrisanoHealth(t);

                            List<Events> eFoundList = new ArrayList<>();
                            eFoundList.add(event);
                            t.setEvents(eFoundList);

                            Query qParticipations = session.getNamedQuery("Participations.findByEventId");
                            qParticipations.setInteger("eventId", event.getId());
                            List<Participations> pl = qParticipations.list();
                            for (Participations p : pl) {

                                if (p.getType().equals(Type.INTERESTED_PARTY.getName())) {

                                    InterestedPartyAttributes ipa = new InterestedPartyAttributes();

                                    Query qPeople = session.getNamedQuery("People.findByEntityId");
                                    qPeople.setInteger("entityId", p.getPrimaryEntityId());
                                    List<People> pList = qPeople.list();
                                    if (pList != null && !pList.isEmpty()) {
                                        ipa.setPerson(pList.get(0));
                                    }

                                    Query qTelephones = session.getNamedQuery("Telephones.findByEntityId");
                                    qTelephones.setInteger("entityId", p.getPrimaryEntityId());
                                    List<Telephones> telephones = qTelephones.list();
                                    ipa.setTelephones(telephones);

                                    Query qPeopleRaces = session.getNamedQuery("PeopleRaces.findByEntityId");
                                    qPeopleRaces.setInteger("entityId", p.getPrimaryEntityId());
                                    List<PeopleRaces> peopleRaces = qPeopleRaces.list();
                                    ipa.setPeopleRaces(peopleRaces);

                                    Query qParticipationsRiskFactors = session.getNamedQuery("ParticipationsRiskFactors.findByParticipationId");
                                    qParticipationsRiskFactors.setInteger("participationId", p.getId());
                                    List<ParticipationsRiskFactors> participationsRiskFactors = qParticipationsRiskFactors.list();
                                    ipa.setRiskFactorAttributes(participationsRiskFactors);

                                    List<InterestedPartyAttributes> ipaList = new ArrayList<>();
                                    ipaList.add(ipa);
                                    t.setInterestedPartyAttributes(ipaList);

                                    Query qAddresses = session.getNamedQuery("Addresses.findByEntityId");
                                    qAddresses.setInteger("entityId", p.getPrimaryEntityId());
                                    List<Addresses> addresses = qAddresses.list();
                                    t.setAddresses(addresses);

                                } else if (p.getType().equals(Type.JURISDICTION.getName())) {

                                    Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                    qPlaces.setInteger("entityId", p.getSecondaryEntityId());
                                    List<Places> pList = qPlaces.list();
                                    if (pList != null && !pList.isEmpty()) {
                                        Places place = pList.get(0);
                                        List<JurisdictionAttributes> jaList = new ArrayList<>();
                                        JurisdictionAttributes ja = new JurisdictionAttributes();
                                        ja.setPlace(place);
                                        jaList.add(ja);
                                        t.setJurisdictionAttributes(jaList);
                                    }
                                } else if (p.getType().equals(Type.REPORTING_AGENCY.getName())) {

                                    Query qPlaces = session.getNamedQuery("Places.findByEntityId");
                                    qPlaces.setInteger("entityId", p.getSecondaryEntityId());
                                    List<Places> pList = qPlaces.list();
                                    if (pList != null && !pList.isEmpty()) {
                                        Places place = pList.get(0);
                                        List<ReportingAgencyAttributes> raaList = new ArrayList<>();
                                        ReportingAgencyAttributes raa = new ReportingAgencyAttributes();
                                        raa.setPlace(place);
                                        raaList.add(raa);
                                        t.addReportingAgencyAttributes(raaList);
                                    }
                                } else if (p.getType().equals(Type.HOSPITALIZATION_FACILITY.getName())) {

                                    Query qhfPlaces = session.getNamedQuery("Places.findByEntityId");
                                    qhfPlaces.setInteger("entityId", p.getSecondaryEntityId());
                                    List<Places> pList = qhfPlaces.list();
                                    if (pList != null && !pList.isEmpty()) {
                                        Places place = pList.get(0);
                                        List<HospitalizationFacilitiesAttributes> hfaList = new ArrayList<>();
                                        HospitalizationFacilitiesAttributes hfa = new HospitalizationFacilitiesAttributes();
                                        hfa.setPlace(place);

                                        Query qhp = session.getNamedQuery("HospitalsParticipations.findByParticipationId");
                                        qhp.setInteger("participationId", p.getId());
                                        List<HospitalsParticipations> hpList = qhp.list();
                                        if (hpList != null && !hpList.isEmpty()) {
                                            HospitalsParticipations hp = hpList.get(0);
                                            hfa.setHospitalsParticipations(hp);
                                            hfaList.add(hfa);
                                            t.addHospitalizationFacilitiesAttributes(hfaList);
                                        }

                                    }
                                }
                            }

                            Query qDiseaseEvents = session.getNamedQuery("DiseaseEvents.findByEventId");
                            qDiseaseEvents.setInteger("eventId", event.getId());
                            List<DiseaseEvents> diseaseEvents = qDiseaseEvents.list();
                            t.setDiseaseEvents(diseaseEvents);
                            for (DiseaseEvents de : diseaseEvents) {
                                Query qDiseases = session.getNamedQuery("Diseases.findById");
                                Integer diseaseId = de.getDiseaseId();
                                if (diseaseId != null) {
                                    qDiseases.setInteger("id", de.getDiseaseId());
                                    t.addDiseases(qDiseases.list());
                                }
                            }

                        }
                        session.getTransaction().commit();
                        hm.addStatusMessage(new StatusMessage("getCdcExport", Status.SUCCESS.toString(), null));

                    } else {
                        hm.addStatusMessage(new StatusMessage("getCdcExport", Status.FAILURE_MISSING_DATA.toString(), "lastExport date or valid start and end dates missing"));
                        throw new Exception("Call to getCdcExport requires lastExport date or valid start and end dates.");
                    }

                } else {
                    hm.addStatusMessage(new StatusMessage("getCdcExport", Status.FAILURE_MISSING_DATA.toString(), "cdc_export missing"));
                    throw new Exception("Call to getCdcExport missing cdc_export");
                }
            }

        } catch (Exception e) {

            log.error(e);
            hm.addStatusMessage(new StatusMessage("getCdcExport", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));

        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public void setLabResults(HealthMessage hm) {

        List<TrisanoHealth> thList = hm.getTrisanoHealth();

        Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
        session.beginTransaction();

        for (TrisanoHealth th : thList) {
            th.setLabsAttributes(null);
            List<Events> eList = th.getEvents();
            for (Events event : eList) {
                if (event.getId() != null) {
                    Query qParticipations = session.getNamedQuery("Participations.findByEventIdType");
                    qParticipations.setInteger("eventId", event.getId());
                    qParticipations.setString("type", Type.LAB.getName());
                    List<Participations> pList = qParticipations.list();
                    List<LabsAttributes> laList = new ArrayList<LabsAttributes>();

                    for (Participations p : pList) {

                        LabsAttributes la = new LabsAttributes();
                        la.addParticipation(p);
                        Integer participationId = p.getId();
                        Query qLabResults = session.getNamedQuery("LabResults.findByParticipationId");
                        qLabResults.setInteger("participationId", participationId);
                        List<LabResults> lrList = qLabResults.list();
                        la.setLabResults(lrList);
                        laList.add(la);
                    }

                    th.addLabAttributes(laList);
                }
            }
        }
        session.getTransaction().commit();
    }
    
    /**
     * execute an SQL query through hibernate and return the String of the requested element
     * @param sql
     * @return 
     */
    public static String getSqlElement(String sql, String element) {
        
        String value = null;
        if (sql != null && sql.trim().length() > 0) {
            ResultSet rs = null;
            Connection con = null;
            PreparedStatement ps = null;
            try {
                con = TrisanoConnect.getSQLConnection();
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next()) {
                    return rs.getString(element);
                }
            } catch (Exception e) {
                log.error(e);
            } finally {
                try {
                    if (con != null) {
                        con.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException se) {
                    log.error(se);
                }
            }
        }
        return value;
    }
        
    /**
     * return the id from the events-queue table for the queue return -1 if the
     * queue is not found
     *
     * @param queue
     * @return
     */
    public static int getQueueId(String queue) {
        if (queue != null && queue.length() > 0) {
            try {
                String id = getSqlElement("SELECT id FROM event_queues WHERE queue_name = '" + queue + "'", "id");
                return Integer.parseInt(id);
            } catch (Exception e) {
                log.error(e);
            }
        }
        return -1;
    }

    public static String getWorkflowState(Integer eventId) {
        if (eventId != null) {
            String workflowState = getSqlElement("SELECT workflow_state FROM events WHERE id = " + eventId.toString(), "workflow_state");
            if(workflowState != null) {
                return workflowState;
            }
        }
        return "";
    }
    
    public static Integer getInvestigatorId(Integer eventId) {
        if (eventId != null) {
            try {
                String investigatorId = getSqlElement("SELECT investigator_id FROM events WHERE id = " + eventId.toString(), "investigator_id");
                return Integer.parseInt(investigatorId);
            } catch (Exception e) {
                log.error(e);
            }
        }
        return -1;
    }
    
    public static boolean validInvestigator(Integer investigatorId, String jurisdiction) {
        if(investigatorId != null && jurisdiction != null) {
            String id = getSqlElement("SELECT u.id FROM users u " +
                                        "INNER JOIN places p ON p.short_name = '" + jurisdiction + "' " +
                                        "INNER JOIN places_types pt ON pt.place_id = p.id AND pt.type_id = " +
                                          "(SELECT id FROM codes where code_name = 'placetype' AND code_description = 'Jurisdiction') " +
                                        "INNER JOIN role_memberships rm ON rm.user_id = " + investigatorId.toString() + 
                                          " AND rm.jurisdiction_id = p.entity_id " + 
                                        "WHERE u.status = 'active' AND u.id = " + investigatorId.toString() +
                                        " LIMIT 1", "id");
            if (id != null) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean hasReporter(Integer eventId) {
        if (eventId != null && eventId > 0) {
            String id = getSqlElement("SELECT id FROM participations WHERE type = 'ReportingAgency' AND event_id = " + eventId.toString() + " LIMIT 1", "id");
            if (id != null) {
                return true;
            }
        }
        return false;
    }
    /**
     * Return the queue name if the event is assigned to a queue, "" otherwise
     *
     * @param queue
     * @return
     */
    public static String getQueue(Integer eventId) {
        
        String workflowState = TrisanoDaoImpl.getWorkflowState(eventId);
        if(workflowState.equals("assigned_to_queue")) {
            try {
                String queueId = getSqlElement("SELECT event_queue_id from events where id = "+eventId.toString(), "event_queue_id");
                return getSqlElement("SELECT queue_name FROM event_queues WHERE id = " + queueId, "queue_name");
            } catch (Exception e) {
                log.error(e);
            }
        }
        return "";
    }


    
    public static void fixReportNumberInEventId(HealthMessage hm) {
        
        if(!emsaEnvironment.equals("SNHD")) {
            return;
        }
        
        List<TrisanoHealth> thList = hm.getTrisanoHealth();
        Query qEvents = null;
        Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
        session.beginTransaction();

        for (TrisanoHealth th : thList) {

            List<Events> eList = th.getEvents();
            Events event = null;
            if (eList != null && !eList.isEmpty()) {
                event = eList.get(0);
            }

            if (event != null) {
                if (event.getId() != null && event.getId() > 0) {
                    // check to see if the eventId is actually a record number
                    // and if so, derive the actual eventId and set it
                    Integer eventId = event.getId();
                    qEvents = session.getNamedQuery("Events.findIdByRecordNumber");
                    qEvents.setParameter("recordNumber", eventId.toString());
                    List<Integer> idl = qEvents.list();
                    if(!idl.isEmpty()) {
                        event.setId(idl.get(0));
                    }
                }
            }
        }
        session.getTransaction().commit();
    }

    public static String fullPersonNameFromEvent(Integer eventId) {
        String fullName = "";
        Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
        session.beginTransaction();
        Query qParticipations = session.getNamedQuery("Participations.findByEventId");
        qParticipations.setInteger("eventId", eventId);
        List<Participations> pl = qParticipations.list();
        for (Participations p : pl) {

           if (p.getType().equals(Type.INTERESTED_PARTY.getName())) {
               Query qPeople = session.getNamedQuery("People.findByEntityId");
               qPeople.setInteger("entityId", p.getPrimaryEntityId());
               List<People> pList = qPeople.list();
               if (pList != null && !pList.isEmpty()) {
                   fullName = pList.get(0).getFullName();
               }
           }
        }
        return fullName;
    }
    
    public static boolean getPersonDemographics(Integer personId, People person) {
        Session session = HibernateUtil.getSessionFactory(datasource).getCurrentSession();
        session.beginTransaction();
        Query qPersons = session.getNamedQuery("People.findById");
        qPersons.setInteger("id", personId);
        List<People> pList = qPersons.list();
        if (pList != null && !pList.isEmpty()) {
            person.setId(personId);
            person.setLastName(pList.get(0).getLastName());
            person.setFirstName(pList.get(0).getFirstName());
            person.setMiddleName(pList.get(0).getMiddleName());
            person.setBirthGenderId(pList.get(0).getBirthGenderId());
            person.setBirthDate(pList.get(0).getBirthDate());
            return true;
        }
        return false;
    }
    
    public static Integer getTreatmentId(String strTreatment) {
        try {
                String id = getSqlElement("SELECT id FROM treatments WHERE '" 
                    + strTreatment.toLowerCase() + "' = lower(treatment_name)", "id");
                if(id != null) {
                    return Integer.parseInt(id);
                }
            } catch (Exception e) {
                log.error("Exception in getEnsureTreatmentId: " + e);
            }
        return -1;
    }
    
    public static boolean insertUpdate(String sql) {
        String value = null;
        boolean retValue = false;
        if (sql != null && sql.trim().length() > 0) {
            Connection con = null;
            PreparedStatement ps = null;
            try {
                con = TrisanoConnect.getSQLConnection();
                ps = con.prepareStatement(sql);
                ps.executeUpdate();
                retValue=true;
            } catch (Exception e) {
                log.error("Exception in insertUpdate: " + e);
            } finally {
                try {
                    if (con != null) {
                        con.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException se) {
                    log.error("SQL Exception in insertUpdate: " + se);
                }
            }
        }
        return retValue;
    }
    
    public static HashMap<String, String> getSqlRow(String sql) {
        HashMap<String, String> map = new HashMap<String, String>();
        ResultSet rs = null;
        if (sql != null && sql.trim().length() > 0) {
            Connection con = null;
            PreparedStatement ps = null;
            try {
                con = TrisanoConnect.getSQLConnection();
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rs.next();
                ResultSetMetaData rsmd = rs.getMetaData();
                for(int i = 1; i <= rsmd.getColumnCount(); i++) {
                    map.put(rsmd.getColumnName(i), rs.getString(i));
                }
            } catch (Exception e) {
                log.error("Exception in getSqlRow: " + e);
                map = null;
            } finally {
                try {
                    if (con != null) {
                        con.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if(rs != null) {
                        rs.close();
                    }
                } catch (SQLException se) {
                    log.error("SQL Exception in executeQuery: " + se);
                }
            }
        }
        return map;
    }
}
