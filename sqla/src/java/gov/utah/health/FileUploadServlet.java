package gov.utah.health;

import java.io.File;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileUploadServlet extends HttpServlet {

    private static final Log logger = LogFactory.getLog(FileUploadServlet.class);

    public FileUploadServlet() {
        super();
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        DiskFileItemFactory factory = new DiskFileItemFactory(); 
        // maximum size that will be stored in memory
        factory.setSizeThreshold(4096);
        // the location for saving data that is larger than getSizeThreshold()
        factory.setRepository(new File("/tmp"));

        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum size before a FileUploadException will be thrown
        upload.setSizeMax(1000000);


        try {
            List fileItems = upload.parseRequest(req);


            String fileName = null;
            FileItem fi;
            if (!fileItems.isEmpty()) {
                fi = (FileItem) fileItems.get(0);
                fileName = fi.getName();

            }
            logger.info(fileName);
            //           fi.write(new File("/www/uploads/", fileName));

        } catch (Exception e) {
            logger.error(e);
        }
    }
}