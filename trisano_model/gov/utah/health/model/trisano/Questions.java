/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */



@Entity
@Table(name = "questions")
@NamedQueries({
    @NamedQuery(name = "Questions.findByDiseaseId", query =
    "SELECT q FROM Questions q WHERE q.formElementId IN "
    + "(SELECT f.id FROM FormElements f WHERE f.formId IN "
    + "(SELECT df.diseasesFormsPK.formId FROM DiseasesForms df WHERE df.diseasesFormsPK.diseaseId = :diseaseId))"),
    @NamedQuery(name = "Questions.findAll", query = "SELECT q FROM Questions q"),
    @NamedQuery(name = "Questions.findById", query = "SELECT q FROM Questions q WHERE q.id = :id"),
    @NamedQuery(name = "Questions.findByFormElementId", query = "SELECT q FROM Questions q WHERE q.formElementId = :formElementId"),
    @NamedQuery(name = "Questions.findByFormId", query = "SELECT q FROM Questions q, FormElements fe WHERE q.formElementId = fe.id AND fe.formId = :formId"),
    @NamedQuery(name = "Questions.findByQuestionText", query = "SELECT q FROM Questions q WHERE q.questionText = :questionText"),
    @NamedQuery(name = "Questions.findByHelpText", query = "SELECT q FROM Questions q WHERE q.helpText = :helpText"),
    @NamedQuery(name = "Questions.findByDataType", query = "SELECT q FROM Questions q WHERE q.dataType = :dataType"),
    @NamedQuery(name = "Questions.findByIsRequired", query = "SELECT q FROM Questions q WHERE q.isRequired = :isRequired"),
    @NamedQuery(name = "Questions.findByCreatedAt", query = "SELECT q FROM Questions q WHERE q.createdAt = :createdAt"),
    @NamedQuery(name = "Questions.findByUpdatedAt", query = "SELECT q FROM Questions q WHERE q.updatedAt = :updatedAt"),
    @NamedQuery(name = "Questions.findByCoreData", query = "SELECT q FROM Questions q WHERE q.coreData = :coreData"),
    @NamedQuery(name = "Questions.findByCoreDataAttr", query = "SELECT q FROM Questions q WHERE q.coreDataAttr = :coreDataAttr"),
    @NamedQuery(name = "Questions.findByShortName", query = "SELECT q FROM Questions q WHERE q.shortName = :shortName"),
    @NamedQuery(name = "Questions.findByShortNameFormId", query = "SELECT q FROM Questions q, FormElements fe WHERE q.formElementId = fe.id AND fe.formId = :formId AND lower(q.shortName) = lower(:shortName)"),
    @NamedQuery(name = "Questions.findByStyle", query = "SELECT q FROM Questions q WHERE q.style = :style")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "formElementId",
    "questionText",
    "helpText",
    "dataType",
    "isRequired",
    "createdAt",
    "updatedAt",
    "coreData",
    "coreDataAttr",
    "shortName",
    "style"
})
@XmlRootElement(name = "questions")
public class Questions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "form_element_id")
    @XmlElement(name = "form_element_id", required = true)
    private Integer formElementId;
    @Column(name = "question_text")
    @XmlElement(name = "question_text", required = true)
    private String questionText;
    @Column(name = "help_text")
    @XmlElement(name = "help_text", required = true)
    private String helpText;
    @Column(name = "data_type")
    @XmlElement(name = "data_type", required = true)
    private String dataType;
    @Column(name = "is_required")
    @XmlElement(name = "is_required", required = true)
    private Boolean isRequired;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "core_data")
    @XmlElement(name = "core_data", required = true)
    private Boolean coreData;
    @Column(name = "core_data_attr")
    @XmlElement(name = "core_data_attr", required = true)
    private String coreDataAttr;
    @Column(name = "short_name")
    @XmlElement(name = "short_name", required = true)
    private String shortName;
    @Column(name = "style")
    @XmlElement(name = "style", required = true)
    private String style;
    
    public Questions() {
    }

    public Questions(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFormElementId() {
        return formElementId;
    }

    public void setFormElementId(Integer formElementId) {
        this.formElementId = formElementId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getCoreData() {
        return coreData;
    }

    public void setCoreData(Boolean coreData) {
        this.coreData = coreData;
    }

    public String getCoreDataAttr() {
        return coreDataAttr;
    }

    public void setCoreDataAttr(String coreDataAttr) {
        this.coreDataAttr = coreDataAttr;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questions)) {
            return false;
        }
        Questions other = (Questions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Questions[ id=" + id + " ]";
    }
}
