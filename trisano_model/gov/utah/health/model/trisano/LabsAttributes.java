package gov.utah.health.model.trisano;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LabsAttributes", propOrder = {
    "labResults",
    "labs"
})
@XmlRootElement(name = "labs_attributes")
public class LabsAttributes extends PlaceAttributes {

    @XmlElement(name = "lab_results", required = true)
    private List<LabResults> labResults;

    @XmlElement(name = "labs", required = true)
    private List<Lab> labs;

    public LabsAttributes() {
        super.placeType = Type.LAB.getNumber();
    }

    public List<LabResults> getLabResults() {
        return labResults;
    }

    public void setLabResults(List<LabResults> labResults) {
        this.labResults = labResults;
    }

    public boolean updateLabResult() {
        boolean update = false;
        if (this.getLabResultId() != null) {
            update = true;
        }
        return update;
    }

    public boolean isValidForLabResultAddUpdate() {
        boolean update = false;
        if (this.getLabResults() != null
                && !this.getLabResults().isEmpty()
                && this.getParticipationId() != null
                && this.getPlaceId() != null
                && this.getPlaceEntityId() != null) {
            update = true;
        }
        return update;
    }
    public boolean isValidForLabAdd() {
        boolean add = false;
        if (this.getLabResults() != null
                && !this.getLabResults().isEmpty()
                && this.getPlace() != null
                && this.getPlace().getName() != null) {
            add = true;
        }
        return add;
    }

    public Integer getParticipationId() {

        Integer pid = null;
        if (this.getParticipations() != null
                && !this.getParticipations().isEmpty()
                && this.getParticipations().get(0).getId() != null) {
            if (this.getParticipations().get(0).getId() > 0) {
                pid = this.getParticipations().get(0).getId();
            }
        }
        if (pid == null
                && this.labResults != null
                && !this.labResults.isEmpty()
                && this.labResults.get(0).getParticipationId() != null) {
            if (this.labResults.get(0).getParticipationId() > 0) {
                pid = this.labResults.get(0).getParticipationId();
            }
        }
        return pid;
    }

    public Integer getPlaceId() {
        Integer pid = null;
        if (this.getPlace() != null
                && this.getPlace().getId() != null
                && this.getPlace().getId() > 0) {
            pid = this.getPlace().getId();
        }
        return pid;
    }

    public Integer getPlaceEntityId() {
        Integer pid = null;
        if (this.getPlace() != null
                && this.getPlace().getEntityId() != null
                && this.getPlace().getEntityId() > 0) {
            pid = this.getPlace().getEntityId();
        }
        if (pid == null
                && this.getParticipations() != null
                && !this.getParticipations().isEmpty()
                && this.getParticipations().get(0).getSecondaryEntityId() != null) {
            if (this.getParticipations().get(0).getSecondaryEntityId() > 0) {
                pid = this.getParticipations().get(0).getSecondaryEntityId();
            }
        }
        return pid;
    }

    public Integer getLabResultId() {
        Integer pid = null;
        if (this.getLabResults() != null
                && !this.getLabResults().isEmpty()
                && this.getLabResults().get(0) != null
                && this.getLabResults().get(0).getId() != null
                && this.getLabResults().get(0).getId() > 0) {
            pid = this.getLabResults().get(0).getId();
        }
        return pid;
    }
}
