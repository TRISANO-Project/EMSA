package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reporter",
    "telephones",
    "participations"
})
@XmlRootElement(name = "reporter_attributes")

public class ReporterAttributes {


    @XmlElement(name="reporter", required = true)
    private People reporter;
    @XmlElement(name="telephones",required = true)
    private List<Telephones> telephones;
    @XmlElement(name="participations",required = true)
    private List<Participations> participations;


    public ReporterAttributes() {
    }

    public People getReporter() {
        return reporter;
    }

    public void setReporter(People reporter) {
        this.reporter = reporter;
    }

    public List<Telephones> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephones> telephones) {
        this.telephones = telephones;
    }

    public List<Participations> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participations> participations) {
        this.participations = participations;
    }

    public void addParticipation(Participations participation) {
        if(this.participations == null){
            this.participations = new ArrayList<Participations>();
        }
        this.participations.add(participation);
    }

}
