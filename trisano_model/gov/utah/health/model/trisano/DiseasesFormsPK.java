/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author UDOH
 */
@Embeddable
public class DiseasesFormsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "form_id")
    private int formId;
    @Basic(optional = false)
    @Column(name = "disease_id")
    private int diseaseId;

    public DiseasesFormsPK() {
    }

    public DiseasesFormsPK(int formId, int diseaseId) {
        this.formId = formId;
        this.diseaseId = diseaseId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getDiseaseId() {
        return diseaseId;
    }

    public void setDiseaseId(int diseaseId) {
        this.diseaseId = diseaseId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) formId;
        hash += (int) diseaseId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiseasesFormsPK)) {
            return false;
        }
        DiseasesFormsPK other = (DiseasesFormsPK) object;
        if (this.formId != other.formId) {
            return false;
        }
        if (this.diseaseId != other.diseaseId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.DiseasesFormsPK[ formId=" + formId + ", diseaseId=" + diseaseId + " ]";
    }

}
