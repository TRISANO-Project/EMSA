/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "hospitals_participations")
@NamedQueries({
    @NamedQuery(name = "HospitalsParticipations.findAll", query = "SELECT h FROM HospitalsParticipations h"),
    @NamedQuery(name = "HospitalsParticipations.findById", query = "SELECT h FROM HospitalsParticipations h WHERE h.id = :id"),
    @NamedQuery(name = "HospitalsParticipations.findByParticipationId", query = "SELECT h FROM HospitalsParticipations h WHERE h.participationId = :participationId"),
    @NamedQuery(name = "HospitalsParticipations.findByHospitalRecordNumber", query = "SELECT h FROM HospitalsParticipations h WHERE h.hospitalRecordNumber = :hospitalRecordNumber"),
    @NamedQuery(name = "HospitalsParticipations.findByAdmissionDate", query = "SELECT h FROM HospitalsParticipations h WHERE h.admissionDate = :admissionDate"),
    @NamedQuery(name = "HospitalsParticipations.findByDischargeDate", query = "SELECT h FROM HospitalsParticipations h WHERE h.dischargeDate = :dischargeDate"),
    @NamedQuery(name = "HospitalsParticipations.findByCreatedAt", query = "SELECT h FROM HospitalsParticipations h WHERE h.createdAt = :createdAt"),
    @NamedQuery(name = "HospitalsParticipations.findByUpdatedAt", query = "SELECT h FROM HospitalsParticipations h WHERE h.updatedAt = :updatedAt"),
    @NamedQuery(name = "HospitalsParticipations.findByMedicalRecordNumber", query = "SELECT h FROM HospitalsParticipations h WHERE h.medicalRecordNumber = :medicalRecordNumber")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "participationId",
    "hospitalRecordNumber",
    "admissionDate",
    "dischargeDate",
    "createdAt",
    "updatedAt",
    "medicalRecordNumber"
})
@XmlRootElement(name = "hospitals_participations")
public class HospitalsParticipations implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "participation_id")
    @XmlElement(name = "participation_id", required = true)
    private Integer participationId;
    @Column(name = "hospital_record_number")
    @XmlElement(name = "hospital_record_number", required = true)
    private String hospitalRecordNumber;
    @Column(name = "admission_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "admission_date", required = true)
    private Date admissionDate;
    @Column(name = "discharge_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "discharge_date", required = true)
    private Date dischargeDate;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "medical_record_number")
    @XmlElement(name = "medical_record_number", required = true)
    private String medicalRecordNumber;

    public HospitalsParticipations() {
    }

    public HospitalsParticipations(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParticipationId() {
        return participationId;
    }

    public void setParticipationId(Integer participationId) {
        this.participationId = participationId;
    }

    public String getHospitalRecordNumber() {
        return hospitalRecordNumber;
    }

    public void setHospitalRecordNumber(String hospitalRecordNumber) {
        this.hospitalRecordNumber = hospitalRecordNumber;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    public Date getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(Date dischargeDate) {
        this.dischargeDate = dischargeDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof HospitalsParticipations)) {
            return false;
        }
        HospitalsParticipations other = (HospitalsParticipations) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.HospitalsParticipations[id=" + id + "]";
    }
}
