package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "labs")
@NamedQueries({
    @NamedQuery(name = "Lab.findAll", query = "SELECT l FROM Lab l"),
    @NamedQuery(name = "Lab.findByParticipationId", query = "SELECT l FROM Lab l WHERE l.participationId = :participationId"),
    @NamedQuery(name = "Lab.findByEntityId", query = "SELECT l FROM Lab l WHERE l.participationId IN ( SELECT p.id FROM Participations p WHERE p.primaryEntityId = :entityId )")
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "specimenSourceId",
    "collectionDate",
    "specimenSentToStateId",
    "createdAt",
    "updatedAt",
    "participationId",
    "stagedMessageId",
    "accessionNo",
    "labResults"
})
@XmlRootElement(name = "labs")
public class Lab implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "labs_id_seq")
    @SequenceGenerator(name = "labs_id_seq", sequenceName = "labs_id_seq", allocationSize = 1)
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "specimen_source_id")
    @XmlElement(name = "specimen_source_id", required = true)
    private Integer specimenSourceId;
    @Column(name = "collection_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "collection_date", required = true)
    private Date collectionDate;
    @Column(name = "specimen_sent_to_state_id")
    @XmlElement(name = "specimen_sent_to_state_id", required = true)
    private Integer specimenSentToStateId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "participation_id")
    @XmlElement(name = "participation_id", required = true)
    private Integer participationId;
    @Column(name = "staged_message_id")
    @XmlElement(name = "staged_message_id", required = true)
    private Integer stagedMessageId;
    @Column(name = "accession_no")
    @XmlElement(name = "accession_no", required = true)
    private String accessionNo;
    @XmlElement(name = "lab_results", required = true)
    private List<LabResults> labResults;

    public Lab() {
    }

    public String getAccessionNo() {
        return accessionNo;
    }

    public void setAccessionNo(String accessionNo) {
        this.accessionNo = accessionNo;
    }

    public Lab(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpecimenSourceId() {
        return specimenSourceId;
    }

    public void setSpecimenSourceId(Integer specimenSourceId) {
        this.specimenSourceId = specimenSourceId;
    }

    public Date getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(Date collectionDate) {
        this.collectionDate = collectionDate;
    }

    public Integer getSpecimenSentToStateId() {
        return specimenSentToStateId;
    }

    public void setSpecimenSentToStateId(Integer specimenSentToStateId) {
        this.specimenSentToStateId = specimenSentToStateId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getParticipationId() {
        return participationId;
    }

    public void setParticipationId(Integer participationId) {
        this.participationId = participationId;
    }

    public Integer getStagedMessageId() {
        return stagedMessageId;
    }

    public void setStagedMessageId(Integer stagedMessageId) {
        this.stagedMessageId = stagedMessageId;
    }

    public List<LabResults> getLabResults() {
        return labResults;
    }

    public void setLabResults(List<LabResults> labResults) {
        this.labResults = labResults;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Lab)) {
            return false;
        }
        Lab other = (Lab) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.trisano.Lab[id=" + id + "]";
    }
}
