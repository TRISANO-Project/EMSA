package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "person",
    "telephones",
    "peopleRaces",
    "riskFactorAttributes",
    "treatmentsAttributes",
    "participations",
    "personAddresses"
})
@XmlRootElement(name = "interested_party_attributes")
public class InterestedPartyAttributes {
    
    @XmlElement(name = "person", required = true)
    private People person;
    @XmlElement(name = "telephones", required = true)
    private List<Telephones> telephones;
    @XmlElement(name = "people_races", required = true)
    private List<PeopleRaces> peopleRaces;
    @XmlElement(name = "risk_factor_attributes", required = true)
    private List<ParticipationsRiskFactors> riskFactorAttributes;
    @XmlElement(name = "treatments_attributes", required = true)
    private List<ParticipationsTreatments> treatmentsAttributes;
    @XmlElement(name = "participations", required = true)
    private List<Participations> participations;
    @XmlElement(name = "person_addresses", required = true)
    private List<Addresses> personAddresses;

    public List<Addresses> getPersonAddresses() {
        return personAddresses;
    }

    public void setPersonAddresses(List<Addresses> personAddresses) {
        this.personAddresses = personAddresses;
    }

    public List<Participations> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participations> participations) {
        this.participations = participations;
    }
    
   public void addParticipations(List<Participations> participations){
       if(this.participations == null){
           this.participations = new ArrayList<Participations>();
       }
       this.participations.addAll(participations);
   }
   public void addParticipation(Participations participation){
       if(this.participations == null){
           this.participations = new ArrayList<Participations>();
       }
       this.participations.add(participation);
   }
    
    public List<PeopleRaces> getPeopleRaces() {
        return peopleRaces;
    }
    
    public void setPeopleRaces(List<PeopleRaces> peopleRaces) {
        this.peopleRaces = peopleRaces;
    }
    
    public People getPerson() {
        return person;
    }
    
    public void setPerson(People person) {
        this.person = person;
    }
    
    public List<ParticipationsRiskFactors> getRiskFactorAttributes() {
        return riskFactorAttributes;
    }
    
    public void setRiskFactorAttributes(List<ParticipationsRiskFactors> riskFactorAttributes) {
        this.riskFactorAttributes = riskFactorAttributes;
    }
    
    public List<Telephones> getTelephones() {
        return telephones;
    }
    
    public void setTelephones(List<Telephones> telephones) {
        this.telephones = telephones;
    }
    
    public List<ParticipationsTreatments> getTreatmentsAttributes() {
        return treatmentsAttributes;
    }
    
    public void setTreatmentsAttributes(List<ParticipationsTreatments> treatmentsAttributes) {
        this.treatmentsAttributes = treatmentsAttributes;
    }
    
    public void addTreatmentAttributes(List<ParticipationsTreatments> ptList) {
        
        if (this.treatmentsAttributes == null) {   
            this.treatmentsAttributes = new ArrayList<ParticipationsTreatments>();
        }
        this.treatmentsAttributes.addAll(ptList);
    }
}
