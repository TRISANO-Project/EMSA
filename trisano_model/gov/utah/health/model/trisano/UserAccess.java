package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author UDOH
 */
public class UserAccess implements Serializable {

    private Users user;
    private List<RoleMemberships> roleMemberships;

    public UserAccess() {
    }

    public UserAccess(Users user, List<RoleMemberships> roleMemberships) {
        this.user = user;
        this.roleMemberships = roleMemberships;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<RoleMemberships> getRoleMemberships() {
        return roleMemberships;
    }

    public void setRoleMemberships(List<RoleMemberships> roleMemberships) {
        this.roleMemberships = roleMemberships;
    }

    public Boolean isUidEqual(String uid) {

        Boolean equal = false;
        if (this.user != null && this.user.getUid() != null && this.user.getUid().equals(uid)) {
            equal = true;
        }
        return equal;
    }

    public Boolean hasJurisdiction(Integer jurisdictionId) {

        Boolean has = false;
        List<RoleMemberships> rmList = this.getRoleMemberships();
        for (RoleMemberships rm : rmList) {
            if (rm.getJurisdiction().getId().equals(jurisdictionId)) {
                has = true;
                break;
            }
        }
        return has;
    }

    public Boolean getCanPrintOutbreakReport(Outbreak o) {
        Boolean can = true;
        if (o != null && this.user != null && this.roleMemberships != null) {

            for (RoleMemberships rm : this.roleMemberships) {

                if (rm.canViewCmr() && rm.getJurisdictionId().equals(o.getJurisdictionId())) {
                    can = true;
                    break;
                }

            }
        }

        return can;
    }

    public Boolean canEditJurisdiction(Integer jurisdictionId) {

        Boolean edit = false;
        List<Integer> editRoleIds = new ArrayList();
        editRoleIds.add(3);
        editRoleIds.add(4);
        editRoleIds.add(5);
        editRoleIds.add(6);
        editRoleIds.add(7);
        editRoleIds.add(8);
        editRoleIds.add(9);
        editRoleIds.add(10);
        editRoleIds.add(11);
        editRoleIds.add(12);
        editRoleIds.add(13);


        List<RoleMemberships> rmList = this.getRoleMemberships();
        for (RoleMemberships rm : rmList) {

            if (editRoleIds.contains(rm.getRoleId()) && rm.getJurisdiction().getId().equals(jurisdictionId)) {
                edit = true;
                break;
            }
        }
        return edit;
    }

    public Boolean canApproveReport(Outbreak outbreak) {

        Boolean can = false;

        Integer lhdManagerRoleId = 7;
        Integer stateManagerRoleId = 3;
        if (outbreak != null && outbreak.getLocationType() != null) {
            Integer locationType = outbreak.getLocationType();
            List<RoleMemberships> rmList = this.getRoleMemberships();
            if (locationType.equals(OutbreakType.LOCATION_TYPE_LOCAL.getId())) {
                for (RoleMemberships rm : rmList) {
                    if (lhdManagerRoleId.equals(rm.getRoleId())) {
                        can = true;
                        break;
                    }
                }
            } else if (locationType.equals(OutbreakType.LOCATION_TYPE_STATE.getId())) {
                for (RoleMemberships rm : rmList) {
                    if (stateManagerRoleId.equals(rm.getRoleId())) {
                        can = true;
                        break;
                    }
                }
            }
        }
        return can;
    }

    public Integer getDefaultJurisdictionId() {

        Integer jid = -1;

        if (this.roleMemberships != null && !this.roleMemberships.isEmpty()) {

            for (RoleMemberships rm : this.roleMemberships) {
                if (JurisdictionType.getStateType(rm.getJurisdictionId())) {
                    jid = rm.getJurisdictionId();
                    break;
                }
            }
            if (jid <= 0) {
                jid = roleMemberships.get(0).getJurisdiction().getId();
            }
        }

        return jid;

    }

    public Boolean getSensitive() {
        Boolean s = false;
        List<RoleMemberships> roles = this.getRoleMemberships();
        for (RoleMemberships rm : roles) {
            if (rm.getSensitive() != null && rm.getSensitive()) {
                s = true;
                break;
            }
        }
        return s;
    }

    public Boolean getHeadNewCmr() {
        Boolean newCmr = false;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canCreateCmr()) {
                newCmr = true;
                break;
            }
        }
        return newCmr;
    }

    public Boolean getHeadEvents() {
        Boolean viewCmr = false;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canViewCmr()) {
                viewCmr = true;
                break;
            }
        }
        return viewCmr;
    }

    public Boolean getHeadOutbreaks() {
        Boolean viewCmr = false;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canViewCmr()) {
                viewCmr = true;
                break;
            }
        }
        return viewCmr;
    }

    public Boolean getHeadSearch() {
        Boolean viewCmr = false;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canViewCmr()) {
                viewCmr = true;
                break;
            }
        }
        return viewCmr;
    }

    public Boolean getHeadPeople() {
        Boolean people = true;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canEntities()) {
                people = true;
                break;
            }
        }
        return people;
    }

    public Boolean getHeadPlaces() {
        Boolean places = true;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canEntities()) {
                places = true;
                break;
            }
        }
        return places;
    }

    public Boolean getHeadAvr() {
        Boolean avr = false;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canAvr()) {
                avr = true;
                break;
            }
        }
        return avr;
    }

    public Boolean getHeadAdmin() {
        Boolean admin = false;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (rm.canAdmin()) {
                admin = true;
                break;
            }
        }
        return admin;
    }

    public Boolean getHeadSettings() {
        Boolean settings = true;

        return settings;
    }

    public List<Integer> getUserJurisdictionIds() {

        List<Integer> ujids = new ArrayList<Integer>();

        for (RoleMemberships rm : getRoleMemberships()) {
            Integer thisId = rm.getJurisdiction().getId();
            if (!ujids.contains(thisId)) {
                ujids.add(thisId);
            }

        }

        return ujids;
    }

    public Boolean getStateUser() {
        Boolean stateUser = false;

        for (RoleMemberships rm : getRoleMemberships()) {
            if (JurisdictionType.getStateType(rm.getJurisdiction().getId())) {
                stateUser = true;
                break;
            }
        }
        return stateUser;
    }

    public Boolean getLocalUser() {
        Boolean localUser = false;
        for (RoleMemberships rm : getRoleMemberships()) {
            if (!JurisdictionType.getStateType(rm.getJurisdiction().getId())) {
                localUser = true;
                break;
            }
        }
        return localUser;
    }

    public Boolean getCanEditLocationType(Integer obJurisdictionId) {
        Boolean can = false;
        if (this.canEditJurisdiction(obJurisdictionId) && getStateUser() && getLocalUser()) {
            can = true;
        }
        return can;
    }
}
