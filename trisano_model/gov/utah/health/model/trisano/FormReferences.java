package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "form_references")
@NamedQueries({
    @NamedQuery(name = "FormReferences.findAll", query = "SELECT f FROM FormReferences f"),
    @NamedQuery(name = "FormReferences.findById", query = "SELECT f FROM FormReferences f WHERE f.id = :id"),
    @NamedQuery(name = "FormReferences.findByEventId", query = "SELECT f FROM FormReferences f WHERE f.eventId = :eventId"),
    @NamedQuery(name = "FormReferences.findByOutbreakId", query = "SELECT f FROM FormReferences f WHERE f.outbreakId = :outbreakId"),
    @NamedQuery(name = "FormReferences.findByFormId", query = "SELECT f FROM FormReferences f WHERE f.formId = :formId"),
    @NamedQuery(name = "FormReferences.findByOutbreakIdAndFormId", query = "SELECT f FROM FormReferences f WHERE f.formId = :formId and f.outbreakId = :outbreakId"),
    @NamedQuery(name = "FormReferences.findByTemplateId", query = "SELECT f FROM FormReferences f WHERE f.templateId = :templateId"),
    @NamedQuery(name = "FormReferences.findByCreatedAt", query = "SELECT f FROM FormReferences f WHERE f.createdAt = :createdAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "eventId",
    "formId",
    "outbreakId",
    "templateId",
    "createdAt"
})
@XmlRootElement(name = "form_references")
public class FormReferences implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "form_references_id_seq")
    @SequenceGenerator(name = "form_references_id_seq", sequenceName = "form_references_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "form_id")
    @XmlElement(name = "form_id", required = true)
    private Integer formId;
    @Column(name = "outbreak_id")
    @XmlElement(name = "outbreak_id", required = true)
    private Integer outbreakId;
    @Basic(optional = false)
    @Column(name = "template_id")
    @XmlElement(name = "template_id", required = true)
    private int templateId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;

    public FormReferences() {
    }

    public FormReferences(Integer id) {
        this.id = id;
    }
    public FormReferences(Integer eventId,Integer formId) {
        this.eventId = eventId;
        this.formId = formId;
    }

    public FormReferences(Integer id, int templateId) {
        this.id = id;
        this.templateId = templateId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof FormReferences)) {
            return false;
        }
        FormReferences other = (FormReferences) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.FormReferences[id=" + id + "]";
    }
}
