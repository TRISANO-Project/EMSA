package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author UDOH
 */
@Entity
@Table(name = "audit_user")
@NamedQueries({
    @NamedQuery(name = "AuditUser.findAll", query = "SELECT a FROM AuditUser a ORDER BY a.actionDate DESC"),
    @NamedQuery(name = "AuditUser.findById", query = "SELECT a FROM AuditUser a"),
    @NamedQuery(name = "AuditUser.findByUserId", query = "SELECT a FROM AuditUser a WHERE a.userId = :userId ORDER BY a.actionDate DESC"),
    @NamedQuery(name = "AuditUser.findByOutbreakId", query = "SELECT a FROM AuditUser a WHERE a.outbreakId = :outbreakId ORDER BY a.actionDate DESC"),
    @NamedQuery(name = "AuditUser.findByEventId", query = "SELECT a FROM AuditUser a WHERE a.eventId = :eventId ORDER BY a.actionDate DESC")
})
public class AuditUser implements Serializable {

    public AuditUser() {
    }

    public AuditUser(Users user, Integer eventId, Integer outbreakId, String auditAction, String auditData, Date actionDate) {
        if(user != null){
            this.user = user;
            this.userId = user.getId();
        }
        this.eventId = eventId;
        this.outbreakId = outbreakId;
        this.auditAction = auditAction;
        this.auditData = auditData;
        this.actionDate = actionDate;
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "audit_user_id_seq")
    @SequenceGenerator(name = "audit_user_id_seq", sequenceName = "audit_user_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "event_id")
    private Integer eventId;
    @Column(name = "outbreak_id")
    private Integer outbreakId;
    @Column(name = "audit_action")
    private String auditAction;
    @Column(name = "audit_data")
    private String auditData;
    @Column(name = "action_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actionDate;
    @Transient
    private Users user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public String getAuditAction() {
        return auditAction;
    }

    public void setAuditAction(String auditAction) {
        this.auditAction = auditAction;
    }

    public String getAuditData() {
        return auditData;
    }

    public void setAuditData(String auditData) {
        this.auditData = auditData;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }


    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

}
