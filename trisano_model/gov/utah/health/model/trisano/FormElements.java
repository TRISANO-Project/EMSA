/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author UDOH
 */


@Entity
@Table(name = "form_elements")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormElements.findAll", query = "SELECT f FROM FormElements f"),
    @NamedQuery(name = "FormElements.findById", query = "SELECT f FROM FormElements f WHERE f.id = :id"),
    @NamedQuery(name = "FormElements.findByFormId", query = "SELECT f FROM FormElements f WHERE f.formId = :formId"),
    @NamedQuery(name = "FormElements.findByFormIdAndType", query = "SELECT f FROM FormElements f WHERE f.formId = :formId AND f.type = :type"),
    @NamedQuery(name = "FormElements.findByType", query = "SELECT f FROM FormElements f WHERE f.type = :type"),
    @NamedQuery(name = "FormElements.findByName", query = "SELECT f FROM FormElements f WHERE f.name = :name"),
    @NamedQuery(name = "FormElements.findByDescription", query = "SELECT f FROM FormElements f WHERE f.description = :description"),
    @NamedQuery(name = "FormElements.findByParentId", query = "SELECT f FROM FormElements f WHERE f.parentId = :parentId ORDER BY f.lft ASC"),
    @NamedQuery(name = "FormElements.findByParentIdAndType", query = "SELECT f FROM FormElements f WHERE f.parentId = :parentId AND f.type = :type ORDER BY f.lft ASC"),
    @NamedQuery(name = "FormElements.findByLft", query = "SELECT f FROM FormElements f WHERE f.lft = :lft"),
    @NamedQuery(name = "FormElements.findByRgt", query = "SELECT f FROM FormElements f WHERE f.rgt = :rgt"),
    @NamedQuery(name = "FormElements.findByCreatedAt", query = "SELECT f FROM FormElements f WHERE f.createdAt = :createdAt"),
    @NamedQuery(name = "FormElements.findByUpdatedAt", query = "SELECT f FROM FormElements f WHERE f.updatedAt = :updatedAt"),
    @NamedQuery(name = "FormElements.findByIsTemplate", query = "SELECT f FROM FormElements f WHERE f.isTemplate = :isTemplate"),
    @NamedQuery(name = "FormElements.findByTemplateId", query = "SELECT f FROM FormElements f WHERE f.templateId = :templateId"),
    @NamedQuery(name = "FormElements.findByIsActive", query = "SELECT f FROM FormElements f WHERE f.isActive = :isActive"),
    @NamedQuery(name = "FormElements.findByTreeId", query = "SELECT f FROM FormElements f WHERE f.treeId = :treeId"),
    @NamedQuery(name = "FormElements.findByCorePath", query = "SELECT f FROM FormElements f WHERE f.corePath = :corePath"),
    @NamedQuery(name = "FormElements.findByIsConditionCode", query = "SELECT f FROM FormElements f WHERE f.isConditionCode = :isConditionCode"),
    @NamedQuery(name = "FormElements.findByHelpText", query = "SELECT f FROM FormElements f WHERE f.helpText = :helpText"),
    @NamedQuery(name = "FormElements.findByExportColumnId", query = "SELECT f FROM FormElements f WHERE f.exportColumnId = :exportColumnId"),
    @NamedQuery(name = "FormElements.findByExportConversionValueId", query = "SELECT f FROM FormElements f WHERE f.exportConversionValueId = :exportConversionValueId"),
    @NamedQuery(name = "FormElements.findByCode", query = "SELECT f FROM FormElements f WHERE f.code = :code")})
public class FormElements implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "form_id")
    private Integer formId;
    @Column(name = "type")
    private String type;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "parent_id")
    private Integer parentId;
    @Column(name = "lft")
    private Integer lft;
    @Column(name = "rgt")
    private Integer rgt;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "is_template")
    private Boolean isTemplate;
    @Column(name = "template_id")
    private Integer templateId;
    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "tree_id")
    private Integer treeId;
    @Column(name = "core_path")
    private String corePath;
    @Column(name = "condition")
    private String condition;
    @Column(name = "is_condition_code")
    private Boolean isConditionCode;
    @Column(name = "help_text")
    private String helpText;
    @Column(name = "export_column_id")
    private Integer exportColumnId;
    @Column(name = "export_conversion_value_id")
    private Integer exportConversionValueId;
    @Column(name = "code")
    private String code;
    public transient List<FormElements> childElements;
    public transient Questions question;
    public transient Answers answer;

    public FormElements() {
    }

    public FormElements(Integer id) {
        this.id = id;
    }

    public List<FormElements> getChildElements() {
        return childElements;
    }

    public void setChildElements(List<FormElements> childElements) {
        this.childElements = childElements;
    }

    public void addChildEelment(FormElements childElement) {
        if (this.childElements == null) {
            this.childElements = new ArrayList<FormElements>();
        }
        this.childElements.add(childElement);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getLft() {
        return lft;
    }

    public void setLft(Integer lft) {
        this.lft = lft;
    }

    public Integer getRgt() {
        return rgt;
    }

    public void setRgt(Integer rgt) {
        this.rgt = rgt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsTemplate() {
        return isTemplate;
    }

    public void setIsTemplate(Boolean isTemplate) {
        this.isTemplate = isTemplate;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getTreeId() {
        return treeId;
    }

    public void setTreeId(Integer treeId) {
        this.treeId = treeId;
    }

    public String getCorePath() {
        return corePath;
    }

    public void setCorePath(String corePath) {
        this.corePath = corePath;
    }

    public Boolean getIsConditionCode() {
        return isConditionCode;
    }

    public void setIsConditionCode(Boolean isConditionCode) {
        this.isConditionCode = isConditionCode;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public Integer getExportColumnId() {
        return exportColumnId;
    }

    public void setExportColumnId(Integer exportColumnId) {
        this.exportColumnId = exportColumnId;
    }

    public Integer getExportConversionValueId() {
        return exportConversionValueId;
    }

    public void setExportConversionValueId(Integer exportConversionValueId) {
        this.exportConversionValueId = exportConversionValueId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
//types...
//    CoreViewElementContainer
//QuestionElement
//CoreFieldElement
//ViewElement
//BeforeCoreFieldElement
//CoreFieldElementContainer
//ValueElement
//FollowUpElement
//GroupElement
//AfterCoreFieldElement
//CoreViewElement
//ValueSetElement
//SectionElement
//InvestigatorViewElementContainer
//FormBaseElement

    public List<FormElements> getInvestigatorViewElementContainers() {
        return getChildrenOfType("InvestigatorViewElementContainer");
    }

    public List<FormElements> getViewElements() {
        return getChildrenOfType("ViewElement");
    }

    public List<FormElements> getGroupElements() {
        return getChildrenOfType("GroupElement");
    }

    public List<FormElements> getQuestionElements() {
        return getChildrenOfType("QuestionElement");
    }

    public List<FormElements> getFollowUpElements() {
        return getChildrenOfType("FollowUpElement");
    }

    public FormElements getFollowUpElement() {
        FormElements fe = null;
        List<FormElements> fuEls = this.getFollowUpElements();
        if (fuEls != null && !fuEls.isEmpty()) {
            fe = fuEls.get(0);
        }
        return fe;
    }

    public String getFollowUpDisplay() {
        String display = "none";

        if (this.answer != null && this.answer.getTextAnswer() != null) {

            String textAnswer = this.answer.getTextAnswer();
            FormElements fe = this.getFollowUpElement();
            String cond = fe.getCondition();
            if (textAnswer.equals(cond)) {
                display = "block";
            }

        }
        return display;
    }

    public List<FormElements> getValueSetElements() {
        return getChildrenOfType("ValueSetElement");
    }

    public List<FormElements> getValueElements() {
        return getChildrenOfType("ValueElement");
    }

    private List<FormElements> getChildrenOfType(String type) {
        List<FormElements> eList = new ArrayList<FormElements>();
        if (this.childElements != null) {
            for (FormElements fe : this.childElements) {
                if (fe.getType().equals(type)) {
                    eList.add(fe);
                }
            }
        }
        return eList;
    }

    public Questions getQuestion() {
        return question;
    }

    public void setQuestion(Questions question) {
        this.question = question;
    }

    public Answers getAnswer() {
        return answer;
    }

    public void setAnswer(Answers answer) {
        this.answer = answer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormElements)) {
            return false;
        }
        FormElements other = (FormElements) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.FormElements[ id=" + id + " ]";
    }
}
