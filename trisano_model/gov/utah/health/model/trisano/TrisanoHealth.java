package gov.utah.health.model.trisano;

import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.master.Esp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Highest level of organization for trisano data. Contains all data used in
 * accessing and updating trisano 3.x. HealthMessage contains a collection of
 * objects of this type for interchange of information with trisano 3.x.
 * Supports JAXB marshaling and unmarshaling. Members of this class are
 * comprised of both database-level classes as well as higher level classes used
 * to organize data. All of the members of this class support xml marshaling and
 * unmarshaling. The database-level classes are annotated with JPA and support
 * use by Hibernate for data access.
 *
 * @author UDOH
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "data",
    "interestedPartyAttributes",
    "cliniciansAttributes",
    "jurisdictionAttributes",
    "labsAttributes",
    "diseaseEvents",
    "diseases",
    "organisms",
    "entities",
    "events",
    "participations",
    "addresses",
    "formReferences",
    "notes",
    "codes",
    "externalCodes",
    "commonTestTypes",
    "dumpTable",
    "hospitalizationFacilitiesAttributes",
    "contactChildEventsAttributes",
    "diagnosticFacilitiesAttributes",
    "reportingAgencyAttributes",
    "reporterAttributes",
    "tasks",
    "caseStatusReport",
    "diseaseReport",
    "careTimeReport",
    "questions",
    "answers",
    "formsAttributes",
    "users",
    "usersAttributes",
    "roleMemberships",
    "roles",
    "attachments",
    "cdcExport",
    "treatments",
    "destinationQueue",
    "createAssessmentEvent",
    "closeIfNotHospital",
    "addPositiveLabTask",
    "esp"
})
@XmlRootElement(name = "trisano_health")
public class TrisanoHealth {

    @XmlElement(name = "data", required = true)
    protected THData data;
    @XmlElement(name = "attachments", required = true)
    protected List<Attachments> attachments;
    @XmlElement(name = "answers", required = true)
    protected List<Answers> answers;
    @XmlElement(name = "questions", required = true)
    protected List<Questions> questions;
    @XmlElement(name = "form_references", required = true)
    protected List<FormReferences> formReferences;
    @XmlElement(required = true)
    protected List<Diseases> diseases;
    @XmlElement(name = "disease_events", required = true)
    protected List<DiseaseEvents> diseaseEvents;
    @XmlElement(required = true)
    protected List<Organisms> organisms;
    @XmlElement(required = true)
    protected List<Codes> codes;
    @XmlElement(name = "external_codes", required = true)
    protected List<ExternalCodes> externalCodes;
    @XmlElement(name = "common_test_types", required = true)
    protected List<CommonTestTypes> commonTestTypes;
    @XmlElement(required = true)
    protected Entities entities;
    @XmlElement(name = "events", required = true)
    protected List<Events> events;
    @XmlElement(required = true)
    protected List<Participations> participations;
    @XmlElement(name = "addresses", required = true)
    private List<Addresses> addresses;
    @XmlElement(required = true)
    private Notes notes;
    @XmlElement(name = "dump_table", required = true)
    private String dumpTable;
    @XmlElement(name = "contact_child_events_attributes", required = true)
    private List<ContactChildEventsAttributes> contactChildEventsAttributes;
    @XmlElement(name = "hospitalization_facilities_attributes", required = true)
    private List<HospitalizationFacilitiesAttributes> hospitalizationFacilitiesAttributes;
    @XmlElement(name = "diagnostic_facilities_attributes", required = true)
    private List<DiagnosticFacilitiesAttributes> diagnosticFacilitiesAttributes;
    @XmlElement(name = "labs_attributes", required = true)
    private List<LabsAttributes> labsAttributes;
    @XmlElement(name = "reporting_agency_attributes", required = true)
    private List<ReportingAgencyAttributes> reportingAgencyAttributes;
    @XmlElement(name = "reporter_attributes", required = true)
    private List<ReporterAttributes> reporterAttributes;
    @XmlElement(name = "interested_party_attributes", required = true)
    private List<InterestedPartyAttributes> interestedPartyAttributes;
    @XmlElement(name = "clinician_attributes", required = true)
    private List<CliniciansAttributes> cliniciansAttributes;
    @XmlElement(name = "jurisdiction_attributes", required = true)
    private List<JurisdictionAttributes> jurisdictionAttributes;
    @XmlElement(name = "tasks", required = true)
    private List<Tasks> tasks;
    @XmlElement(name = "case_status_report", required = true)
    private List<CaseStatusReport> caseStatusReport;
    @XmlElement(name = "disease_report", required = true)
    private List<DiseaseReport> diseaseReport;
    @XmlElement(name = "care_time_report", required = true)
    private List<CareTimeReport> careTimeReport;
    @XmlElement(name = "forms_attributes", required = true)
    private List<FormsAttributes> formsAttributes;
    @XmlElement(name = "users", required = true)
    private List<Users> users;
    @XmlElement(name = "users_attributes", required = true)
    private List<UsersAttributes> usersAttributes;
    @XmlElement(name = "role_memberships", required = true)
    private List<RoleMemberships> roleMemberships;
    @XmlElement(name = "treatments", required = true)
    private List<Treatments> treatments;
    @XmlElement(name = "destination_queue", required = true)
    private String destinationQueue;
    @XmlElement(name = "create_assessment_event", required = true)
    private boolean createAssessmentEvent;
    @XmlElement(name = "close_if_not_hospital", required = true)
    private boolean closeIfNotHospital;
    @XmlElement(name = "add_positive_lab_task", required = true)
    private String addPositiveLabTask;
    @XmlElement(name = "esp", required = false)
    private Esp esp;
    @XmlElement(name = "roles", required = true)
    private List<Roles> roles;
    @XmlElement(name = "cdc_export", required = true)
    private CdcExport cdcExport;
    private transient String eventTypeFormName;
    private transient String eventType;
    private transient boolean eventExists = false;
    private transient boolean labSame = false;
    private transient boolean updateCmr = false;
    private transient boolean addCmr = false;
    private transient boolean addLab = false;
    private transient String webPage = "/cmrs/";

    private static final HashMap<String, String> mapTypeToPage;
    static
    {
        mapTypeToPage = new HashMap<String, String>();
        mapTypeToPage.put("MorbidityEvent", "/cmrs/");
        mapTypeToPage.put("ContactEvent", "/contact_events/");
        mapTypeToPage.put("AssessmentEvent", "/aes/");
    }
    
    public Boolean getIncludeQuestions(){
    Boolean include = false;
    if(this.formsAttributes != null && !this.formsAttributes.isEmpty()){
       FormsAttributes fa = this.formsAttributes.get(0);
       if(fa.getIncludeQuestions() != null && fa.getIncludeQuestions()){
           include = true;
       }
    }
    return include;
    }
    public String getFirstFormShortName(){
    String shortName = null;
    if(this.formsAttributes != null && !this.formsAttributes.isEmpty()){
       FormsAttributes fa = this.formsAttributes.get(0);
       if(fa.getForm() != null && fa.getForm().getShortName() != null && fa.getForm().getShortName().trim().length() > 0){
           shortName = fa.getForm().getShortName();
       }
    }
    return shortName;
    }
    
    public Boolean getHasAnswers(){
        Boolean has = false;
        if(this.answers != null){
            for(Answers a : this.answers){
                if((a.getId() != null && a.getId() > 0) || 
                        ((a.getFormShortName() != null && a.getFormShortName().trim().length() > 0) 
                        &&(a.getQuestionShortName() != null && a.getQuestionShortName().trim().length() > 0))){
                has = true;
                break;
                }
            }
            
        }
        return has;
    }
    
    public Boolean getContainsDisease(String diseaseName) {
        Boolean containsDisease = false;
        if (this.diseases != null && diseaseName != null) {
            for (Diseases disease : this.diseases) {
                if (disease.getDiseaseName() != null
                        && disease.getDiseaseName().trim().toLowerCase().equals(diseaseName.trim().toLowerCase())) {
                    containsDisease = true;

                }
            }
        }

        return containsDisease;
    }

    public Diseases getDiseaseByDiseaseName(String n) {

        Diseases disease = null;

        if (this.diseases != null) {

            for (Diseases d : this.diseases) {

                if (d.getDiseaseName() != null && d.getDiseaseName().toLowerCase().equals(n.toLowerCase())) {
                    disease = d;
                    break;
                }
            }
        }

        return disease;
    }

    public List<Treatments> getTreatments() {
        return treatments;
    }

    public void setTreatments(List<Treatments> treatments) {
        this.treatments = treatments;
    }

    public void addTreatment(Treatments treatment) {
        if (this.treatments == null) {
            this.treatments = new ArrayList<Treatments>();
        }
        this.treatments.add(treatment);
    }

    public Treatments getTreatmentByTreatmentName(String n) {

        Treatments treatment = null;

        if (this.treatments != null) {

            for (Treatments t : this.treatments) {

                if (t.getTreatmentName() != null && t.getTreatmentName().toLowerCase().equals(n.toLowerCase())) {
                    treatment = t;
                    break;
                }
            }
        }

        return treatment;
    }

    public List<UsersAttributes> getUsersAttributes() {
        return usersAttributes;
    }

    public void setUsersAttributes(List<UsersAttributes> usersAttributes) {
        this.usersAttributes = usersAttributes;
    }

    public void addUsersAttribute(UsersAttributes usersAttributes) {
        if (this.usersAttributes == null) {
            this.usersAttributes = new ArrayList<UsersAttributes>();
        }
        this.usersAttributes.add(usersAttributes);
    }

    public boolean getUpdateCmr() {
        return updateCmr;
    }

    public void setUpdateCmr(boolean updateCmr) {
        this.updateCmr = updateCmr;
    }

    public boolean getAddCmr() {
        return addCmr;
    }

    public void setAddCmr(boolean addCmr) {
        this.addCmr = addCmr;
    }

    public boolean getAddLab() {
        return addLab;
    }

    public void setAddLab(boolean addLab) {
        this.addLab = addLab;
    }

    public boolean getLabSame() {
        return labSame;
    }

    public void setLabSame(boolean labSame) {
        this.labSame = labSame;
    }

    public boolean getEventExists() {
        return eventExists;
    }

    public void setEventExists(boolean eventExists) {
        this.eventExists = eventExists;
    }

    public CdcExport getCdcExport() {
        return cdcExport;
    }

    public void setCdcExport(CdcExport cdcExport) {
        this.cdcExport = cdcExport;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public Attachments getAttachment() {
        Attachments attach = null;

        if (this.attachments != null
                && !this.attachments.isEmpty()
                && this.attachments.get(0).getMasterId() != null
                && this.events != null
                && !this.events.isEmpty()
                && this.events.get(0).getId() != null
                && this.events.get(0).getId() > 0) {

            Integer masterId = this.attachments.get(0).getMasterId();
            Integer eventId = this.events.get(0).getId();

            attach = new Attachments();

            attach.setCategory("lab");
            attach.setContentType("application/pdf");
            attach.setEventId(eventId);
            attach.setMasterId(masterId);
            attach.setFilename(this.getPDFFileName());



        }
        return attach;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    public void addAttachment(Attachments attachment) {
        if (this.attachments == null) {
            this.attachments = new ArrayList<Attachments>();
        }
        this.attachments.add(attachment);
    }

    public List<CareTimeReport> getCareTimeReport() {
        return careTimeReport;
    }

    public void setCareTimeReport(List<CareTimeReport> careTimeReport) {
        this.careTimeReport = careTimeReport;
    }

    public List<RoleMemberships> getRoleMemberships() {
        return roleMemberships;
    }

    public void setRoleMemberships(List<RoleMemberships> roleMemberships) {
        this.roleMemberships = roleMemberships;
    }

    public List<Roles> getRoles() {
        return roles;
    }

    public void setRoles(List<Roles> roles) {
        this.roles = roles;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }

    public void addUsers(List<Users> users) {
        if (this.users == null) {
            this.users = new ArrayList<Users>();
        }
        for (Users user : users) {
            if (!this.users.contains(user)) {
                this.users.add(user);
            }
        }
    }

    public void addUser(Users users) {
        if (this.users == null) {
            this.users = new ArrayList<Users>();
        }
        this.users.add(users);

    }

    public List<FormsAttributes> getFormsAttributes() {
        return formsAttributes;
    }

    public void setFormsAttributes(List<FormsAttributes> formsAttributes) {
        this.formsAttributes = formsAttributes;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }

    public List<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }

    public List<Tasks> getTasks() {
        return tasks;
    }

    public void setTasks(List<Tasks> tasks) {
        this.tasks = tasks;
    }

    public List<DiseaseReport> getDiseaseReport() {
        return diseaseReport;
    }

    public void setDiseaseReport(List<DiseaseReport> diseaseReport) {
        this.diseaseReport = diseaseReport;
    }

    public List<CaseStatusReport> getCaseStatusReport() {
        return caseStatusReport;
    }

    public void setCaseStatusReport(List<CaseStatusReport> caseStatusReport) {
        this.caseStatusReport = caseStatusReport;
    }

    public List<InterestedPartyAttributes> getInterestedPartyAttributes() {
        return interestedPartyAttributes;
    }

    public People getFirstPerson() {
        People firstPerson = null;
        if (this.interestedPartyAttributes != null
                && !this.interestedPartyAttributes.isEmpty()) {
            firstPerson = this.interestedPartyAttributes.get(0).getPerson();

        }
        return firstPerson;
    }

    public void setFirstPerson(People firstPerson) {
        if (this.interestedPartyAttributes == null) {
            this.interestedPartyAttributes = new ArrayList<InterestedPartyAttributes>();
        }
        if (this.interestedPartyAttributes.isEmpty()) {
            InterestedPartyAttributes ipa = new InterestedPartyAttributes();
            ipa.setPerson(firstPerson);
            this.interestedPartyAttributes.add(ipa);
        } else {
            this.interestedPartyAttributes.get(0).setPerson(firstPerson);
        }
    }

    public void setInterestedPartyAttributes(List<InterestedPartyAttributes> interestedPartyAttributes) {
        this.interestedPartyAttributes = interestedPartyAttributes;
    }

    public void addInterestedPartyAttributes(InterestedPartyAttributes interestedPartyAttributes) {
        if (this.interestedPartyAttributes == null) {
            this.interestedPartyAttributes = new ArrayList<InterestedPartyAttributes>();
        }
        this.interestedPartyAttributes.add(interestedPartyAttributes);
    }

    public List<CliniciansAttributes> getClinicianAttributes() {
        return cliniciansAttributes;
    }

    public void setCliniciansAttributes(List<CliniciansAttributes> cliniciansAttributes) {
        this.cliniciansAttributes = cliniciansAttributes;
    }

    public void addCliniciansAttributes(List<CliniciansAttributes> cliniciansAttributes) {
        if (this.cliniciansAttributes == null) {
            this.cliniciansAttributes = new ArrayList<CliniciansAttributes>();
        }
        this.cliniciansAttributes.addAll(cliniciansAttributes);
    }

    public List<ReporterAttributes> getReporterAttributes() {
        return reporterAttributes;
    }

    public void setReporterAttributes(List<ReporterAttributes> reporterAttributes) {
        this.reporterAttributes = reporterAttributes;
    }

    public void addReporterAttributes(List<ReporterAttributes> reporterAttributes) {
        if (this.reporterAttributes == null) {
            this.reporterAttributes = new ArrayList<ReporterAttributes>();
        }
        this.reporterAttributes.addAll(reporterAttributes);
    }

    public List<JurisdictionAttributes> getJurisdictionAttributes() {
        return jurisdictionAttributes;
    }

    public void setJurisdictionAttributes(List<JurisdictionAttributes> jurisdictionAttributes) {
        this.jurisdictionAttributes = jurisdictionAttributes;
    }

    public Boolean hasJurisdiction() {

        Boolean has = false;


        if (this.jurisdictionAttributes != null
                && !this.jurisdictionAttributes.isEmpty()) {

            if (this.jurisdictionAttributes.get(0).getPlace() != null
                    && this.jurisdictionAttributes.get(0).getPlace().getId() != null
                    && this.jurisdictionAttributes.get(0).getPlace().getId() > 0) {
                has = true;
            }

        }

        return has;

    }

    public List<ReportingAgencyAttributes> getReportingAgencyAttributes() {
        return reportingAgencyAttributes;
    }

    public void setReportingAgencyAttributes(List<ReportingAgencyAttributes> reportingAgencyAttributes) {
        this.reportingAgencyAttributes = reportingAgencyAttributes;
    }

    public void addReportingAgencyAttributes(List<ReportingAgencyAttributes> reportingAgencyAttributes) {
        if (this.reportingAgencyAttributes == null) {
            this.reportingAgencyAttributes = new ArrayList<ReportingAgencyAttributes>();
        }
        this.reportingAgencyAttributes.addAll(reportingAgencyAttributes);
    }

    public List<LabsAttributes> getLabsAttributes() {
        return labsAttributes;
    }

    public void setLabsAttributes(List<LabsAttributes> labsAttributes) {
        this.labsAttributes = labsAttributes;
    }

    public void addLabAttributes(List<LabsAttributes> labsAttributes) {
        if (this.labsAttributes == null) {
            this.labsAttributes = new ArrayList<LabsAttributes>();
        }
        this.labsAttributes.addAll(labsAttributes);
    }

    public List<DiagnosticFacilitiesAttributes> getDiagnosticFacilitiesAttributes() {
        return diagnosticFacilitiesAttributes;
    }

    public void setDiagnosticFacilitiesAttributes(List<DiagnosticFacilitiesAttributes> diagnosticFacilitiesAttributes) {
        this.diagnosticFacilitiesAttributes = diagnosticFacilitiesAttributes;
    }

    public void addDiagnosticFacilitiesAttributes(List<DiagnosticFacilitiesAttributes> diagnosticFacilitiesAttributes) {
        if (this.diagnosticFacilitiesAttributes == null) {
            this.diagnosticFacilitiesAttributes = new ArrayList<DiagnosticFacilitiesAttributes>();
        }
        this.diagnosticFacilitiesAttributes.addAll(diagnosticFacilitiesAttributes);
    }

    public List<HospitalizationFacilitiesAttributes> getHospitalizationFacilitiesAttributes() {
        return hospitalizationFacilitiesAttributes;
    }

    public void setHospitalizationFacilitiesAttributes(List<HospitalizationFacilitiesAttributes> hospitalizationFacilitiesAttributes) {
        this.hospitalizationFacilitiesAttributes = hospitalizationFacilitiesAttributes;
    }

    public void addHospitalizationFacilitiesAttributes(List<HospitalizationFacilitiesAttributes> hospitalizationFacilitiesAttributes) {
        if (this.hospitalizationFacilitiesAttributes == null) {
            this.hospitalizationFacilitiesAttributes = new ArrayList<HospitalizationFacilitiesAttributes>();
        }
        this.hospitalizationFacilitiesAttributes.addAll(hospitalizationFacilitiesAttributes);
    }

    public List<ContactChildEventsAttributes> getContactChildEventsAttributes() {
        return contactChildEventsAttributes;
    }

    public void setContactChildEventsAttributes(List<ContactChildEventsAttributes> contactChildEventsAttributes) {
        this.contactChildEventsAttributes = contactChildEventsAttributes;
    }

    public void addContactChildEventsAttributes(List<ContactChildEventsAttributes> contactChildEventsAttributes) {
        if (this.contactChildEventsAttributes == null) {
            this.contactChildEventsAttributes = new ArrayList<ContactChildEventsAttributes>();
        }
        this.contactChildEventsAttributes.addAll(contactChildEventsAttributes);
    }

    public String getDumpTable() {
        return dumpTable;
    }

    public void setDumpTable(String dumpTable) {
        this.dumpTable = dumpTable;
    }

    public Notes getNotes() {
        return notes;
    }

    public void setNotes(Notes notes) {
        this.notes = notes;
    }

    public List<FormReferences> getFormReferences() {
        return formReferences;
    }

    public void setFormReferences(List<FormReferences> formReferences) {
        this.formReferences = formReferences;
    }

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    public void addAddresses(List<Addresses> addresses) {
        if (this.addresses == null) {
            this.addresses = new ArrayList<Addresses>();
        }
        this.addresses.addAll(addresses);
    }

    public List<Diseases> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<Diseases> diseases) {
        this.diseases = diseases;
    }

    public List<String> getDiseaseNames() {
        List<String> diseaseNames = new ArrayList<String>();
        if (this.diseases != null) {
            for (Diseases d : this.diseases) {
                if (d.getDiseaseName() != null && d.getDiseaseName().trim().length() > 0) {
                    diseaseNames.add(d.getDiseaseName());
                }
            }
        }
        return diseaseNames;
    }
    public List<Integer> getDiseaseIds() {
        List<Integer> diseaseIds = new ArrayList<Integer>();
        if (this.diseases != null) {
            for (Diseases d : this.diseases) {
                if (d.getId() != null && d.getId() > 0) {
                    diseaseIds.add(d.getId());
                }
            }
        }
        return diseaseIds;
    }
    public Integer getFirstDiseaseId() {
        Integer diseaseId = null;
        if (this.diseases != null && !this.diseases.isEmpty()) {
            Diseases d = this.diseases.get(0);
                if (d.getId() != null && d.getId() > 0) {
                    diseaseId = d.getId();
                }
            
        }
        return diseaseId;
    }
    public List<String> getQuestionShortNames() {
        List<String> shortNames = new ArrayList<String>();
        if (this.questions != null) {
            for (Questions q : this.questions) {
                if (q.getShortName() != null && q.getShortName().trim().length() > 0) {
                    shortNames.add(q.getShortName().trim());
                }
            }
        }
        if (this.answers != null) {
            for (Answers a : this.answers) {
                if (a.getQuestionShortName() != null && a.getQuestionShortName().trim().length() > 0) {
                    if(!shortNames.contains(a.getQuestionShortName())){
                        shortNames.add(a.getQuestionShortName().trim());
                    }                    
                }
            }
        }
        return shortNames;
    }

    public Boolean hasDiseaseFilter() {

        Boolean has = false;

        if (this.diseases != null) {

            for (Diseases d : this.diseases) {

                if (d.getDiseaseName() != null && d.getDiseaseName().trim().length() > 0) {
                    has = true;
                    break;
                }
//                if (d.getId() != null && d.getId() > 0) {
//                    has = true;
//                    break;
//                }
            
            
            }
        }
        return has;
    }

    public Boolean hasDiseaseInFilter(String n) {

        Boolean hasDisease = false;

        if (this.diseases != null) {

            for (Diseases d : this.diseases) {
                if (d.getDiseaseName() != null && d.getDiseaseName().toLowerCase().equals(n.toLowerCase())) {
                    hasDisease = true;
                    break;
                }
            }
        }

        return hasDisease;
    }

    public void addDiseases(List<Diseases> diseases) {
        if (this.diseases == null) {
            this.diseases = new ArrayList<Diseases>();
        }
        this.diseases.addAll(diseases);
    }

    public void addDisease(Diseases disease) {
        if (this.diseases == null) {
            this.diseases = new ArrayList<Diseases>();
        }
        this.diseases.add(disease);
    }

    public List<DiseaseEvents> getDiseaseEvents() {
        return diseaseEvents;
    }

    public void setDiseaseEvents(List<DiseaseEvents> diseaseEvents) {
        this.diseaseEvents = diseaseEvents;
    }

    public void addDiseaseEvent(DiseaseEvents diseaseEvent) {
        if (this.diseaseEvents == null) {
            this.diseaseEvents = new ArrayList<DiseaseEvents>();
        }
        this.diseaseEvents.add(diseaseEvent);
    }

    public void addDiseaseEvents(List<DiseaseEvents> diseaseEvents) {
        if (this.diseaseEvents == null) {
            this.diseaseEvents = new ArrayList<DiseaseEvents>();
        }
        this.diseaseEvents.addAll(diseaseEvents);
    }

    public List<Codes> getCodes() {
        return codes;
    }

    public Codes getCodeByTheCode(String appCat, String n) {


        Codes code = null;

        if (this.codes != null) {

            for (Codes c : this.codes) {

                if ((c.getCodeName() != null && c.getCodeName().toLowerCase().equals(appCat.toLowerCase()))
                        && (c.getTheCode() != null && c.getTheCode().toLowerCase().equals(n.toLowerCase()))) {
                    code = c;
                    break;
                }
            }
        }

        return code;
    }

    public void setCodes(List<Codes> codes) {
        this.codes = codes;
    }

    public void addCode(Codes code) {
        if (this.codes == null) {
            this.codes = new ArrayList<Codes>();
        }
        this.codes.add(code);
    }

    public List<CommonTestTypes> getCommonTestTypes() {
        return commonTestTypes;
    }

    public void setCommonTestTypes(List<CommonTestTypes> commonTestTypes) {
        this.commonTestTypes = commonTestTypes;
    }

    public CommonTestTypes getCommonTestTypeByCommonName(String n) {

        CommonTestTypes ctt = null;

        if (this.commonTestTypes != null) {

            for (CommonTestTypes c : this.commonTestTypes) {

                if (c.getCommonName() != null && c.getCommonName().toLowerCase().equals(n.toLowerCase())) {
                    ctt = c;
                    break;
                }
            }
        }

        return ctt;
    }

    public void addCommonTestTypes(List<CommonTestTypes> commonTestTypes) {
        if (this.commonTestTypes == null) {
            this.commonTestTypes = new ArrayList<CommonTestTypes>();
        }
        this.commonTestTypes.addAll(commonTestTypes);
    }

    public void addCommonTestType(CommonTestTypes commonTestType) {
        if (this.commonTestTypes == null) {
            this.commonTestTypes = new ArrayList<CommonTestTypes>();
        }
        this.commonTestTypes.add(commonTestType);
    }

    public List<ExternalCodes> getExternalCodes() {
        return externalCodes;
    }

    public void setExternalCodes(List<ExternalCodes> externalCodes) {
        this.externalCodes = externalCodes;
    }

    public void addExternalCode(ExternalCodes externalCode) {
        if (this.externalCodes == null) {
            this.externalCodes = new ArrayList<ExternalCodes>();
        }
        this.externalCodes.add(externalCode);
    }

    public void addExternalCodes(List<ExternalCodes> externalCodes) {
        if (this.externalCodes == null) {
            this.externalCodes = new ArrayList<ExternalCodes>();
        }
        this.externalCodes.addAll(externalCodes);
    }

    public List<Organisms> getOrganisms() {
        return organisms;
    }

    public void setOrganisms(List<Organisms> organisms) {
        this.organisms = organisms;
    }

    public Organisms getOrganismByOrganismName(String n) {

        Organisms org = null;

        if (this.organisms != null) {

            for (Organisms o : this.organisms) {

                if (o.getOrganismName() != null && o.getOrganismName().toLowerCase().equals(n.toLowerCase())) {
                    org = o;
                    break;
                }
            }
        }

        return org;
    }

    public void addOrganisms(List<Organisms> organisms) {
        if (this.organisms == null) {
            this.organisms = new ArrayList<Organisms>();
        }
        this.organisms.addAll(organisms);
    }

    public void addOrganism(Organisms organism) {
        if (this.organisms == null) {
            this.organisms = new ArrayList<Organisms>();
        }
        this.organisms.add(organism);
    }

    public Entities getEntities() {
        return entities;
    }

    public void setEntities(Entities entities) {
        this.entities = entities;
    }

    public List<Events> getEvents() {
        return events;
    }

    public void setEvents(List<Events> events) {
        this.events = events;
    }

    public void addEvent(Events event) {
        if (this.events == null) {
            this.events = new ArrayList<Events>();
        }
        this.events.add(event);
    }

    public void addEvents(List<Events> events) {
        if (this.events == null) {
            this.events = new ArrayList<Events>();
        }
        this.events.addAll(events);
    }

    public List<Participations> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participations> participations) {
        this.participations = participations;
    }

    public void addClinicianAttributes(CliniciansAttributes ca) {
        if (this.cliniciansAttributes == null) {
            this.cliniciansAttributes = new ArrayList<CliniciansAttributes>();
        }
        this.cliniciansAttributes.add(ca);
    }

    public boolean hasPersonEntityId() {
        boolean hasId = false;

        if (this.interestedPartyAttributes != null && !this.interestedPartyAttributes.isEmpty()) {

            People person = this.interestedPartyAttributes.get(0).getPerson();
            if (person != null && person.getEntityId() != null
                    && person.getEntityId() > 0 && person.getId() != null
                    && person.getId() > 0) {
                hasId = true;
            }
        }
        return hasId;
    }

    public boolean hasEventId() {
        boolean hasId = false;

        if (this.events != null && !this.events.isEmpty()) {

            Integer eId = this.events.get(0).getId();
            if (eId != null && eId > 0) {
                hasId = true;

            }

        }
        return hasId;
    }

    public boolean hasFirstReportedPhDate() {
        boolean has = false;
        if (events != null && !events.isEmpty()) {
            if (events.get(0).getFirstReportedPHdate() != null) {
                has = true;
            }
        }
        return has;
    }

    public void setEventId(Integer eventId) {

        if (this.events == null) {
            this.events = new ArrayList<Events>();
            this.events.add(new Events(eventId));
        } else if (this.events.isEmpty()) {
            this.events.add(new Events(eventId));
        } else {
            this.getEvents().get(0).setId(eventId);
        }

    }

    public String getWorkflowState() {

        String wfs = null;

        if (this.events != null && !this.events.isEmpty()) {

            wfs = this.events.get(0).getWorkflowState();

        }
        return wfs;

    }

    public ExternalCodes getExternalCodeById(Integer id) {

        ExternalCodes ec = null;

        if (this.externalCodes != null) {

            for (ExternalCodes e : this.externalCodes) {

                if (e.getId() != null && e.getId().equals(id)) {
                    ec = e;
                    break;
                }
            }
        }

        return ec;
    }

    public ExternalCodes getExternalCodeByDescription(String appCat, String d) {

        ExternalCodes ec = null;

        if (this.externalCodes != null) {

            for (ExternalCodes e : this.externalCodes) {

                if ((e.getCodeName() != null && e.getCodeName().toLowerCase().equals(appCat.toLowerCase()))
                        && (e.getCodeDescription() != null && e.getCodeDescription().toLowerCase().equals(d.toLowerCase()))) {
                    ec = e;
                    break;
                }
            }
        }

        return ec;
    }

    public ExternalCodes getExternalCodeByTheCode(String appCat, String n) {

        ExternalCodes ec = null;

        if (this.externalCodes != null && appCat != null && n != null) {

            for (ExternalCodes e : this.externalCodes) {

                if ((e.getCodeName() != null && e.getCodeName().toLowerCase().equals(appCat.toLowerCase()))
                        && (e.getTheCode() != null && e.getTheCode().toLowerCase().equals(n.toLowerCase()))) {
                    ec = e;
                    break;
                }
            }
        }

        return ec;
    }

    public String getEventTypeFormName() {

        this.initEventType();
        return this.eventTypeFormName;

    }

    public String getEventType() {

        this.initEventType();
        return this.eventType;

    }
    
    // Make sure type name/web page/form element name are all ssynced up
    //  MorbidityEvent/cmrs/morbidity_event AssessmentEvent/aes/assessment_event ContactEvent/contact_events/contact_event
    public void setEventType(String typeName) {
        
        if (this.events != null && !this.events.isEmpty()) {
            Events e = this.events.get(0);
            e.setType(typeName);
            this.setWebPage(typeName);
        }
        this.initEventType();
    }

    private void initEventType() {

        if (this.eventTypeFormName == null || this.eventType == null) {

            Events e;

            if (this.events != null && !this.events.isEmpty()) {
                e = this.events.get(0);

                if (e.getType() != null) {

                    if (e.getType().equals(Type.MORBIDITY_EVENT.getName())) {

                        this.eventTypeFormName = "morbidity_event";
                        this.eventType = Type.MORBIDITY_EVENT.getName();

                    } else if (e.getType().equals(Type.CONTACT_EVENT.getName())) {

                        this.eventTypeFormName = "contact_event";
                        this.eventType = Type.CONTACT_EVENT.getName();

                    } else if (e.getType().equals(Type.ASSESSMENT_EVENT.getName())) {

                        this.eventTypeFormName = "assessment_event";
                        this.eventType = Type.ASSESSMENT_EVENT.getName();

                    }

                }

            }

        }


    }

    public THData getData() {
        return data;
    }

    public List<THDataItem> getDataItems() {
        if (this.data != null) {
            return this.data.getItems();
        }
        return null;
    }

    public String getDataByName(String name) {
        if (this.data != null) {
            for (THDataItem di : this.data.items) {
                if (di.name.equals(name)) {
                    return di.getValue();
                }
            }
        }
        return null;
    }

    public void setData(THData data) {
        this.data = data;
    }

    public List<Addresses> getAllAddresses() {
        List<Addresses> all = new ArrayList<Addresses>();

        if (this.addresses != null) {
            all.addAll(this.addresses);
        }
        if (this.diagnosticFacilitiesAttributes != null) {
            for (DiagnosticFacilitiesAttributes dfa : this.diagnosticFacilitiesAttributes) {
                if (dfa.getAddresses() != null) {
                    all.addAll(dfa.getAddresses());
                }
            }
        }
        if (this.hospitalizationFacilitiesAttributes != null) {
            for (HospitalizationFacilitiesAttributes hfa : this.hospitalizationFacilitiesAttributes) {
                if (hfa.getAddresses() != null) {
                    all.addAll(hfa.getAddresses());
                }
            }
        }
        if (this.jurisdictionAttributes != null) {
            for (JurisdictionAttributes ja : this.jurisdictionAttributes) {
                if (ja.getAddresses() != null) {
                    all.addAll(ja.getAddresses());
                }
            }
        }
        if (this.labsAttributes != null) {
            for (LabsAttributes la : this.labsAttributes) {
                if (la.getAddresses() != null) {
                    all.addAll(la.getAddresses());
                }
            }
        }
        if (this.reportingAgencyAttributes != null) {
            for (ReportingAgencyAttributes raa : this.reportingAgencyAttributes) {
                if (raa.getAddresses() != null) {
                    all.addAll(raa.getAddresses());
                }
            }
        }

        return all;
    }

    public boolean eventExists(List<TrisanoHealth> triN) {

        boolean found = false;
        System.out.println("in Event exists RULES");
        if (this.diseaseEvents != null) {
            if (this.getDiseaseEvents().size() > 0) {

                TrisanoHealth tri = triN.get(0);
                if (tri.getDiseaseEvents() != null) {
                    for (int i = 0; i < this.getDiseaseEvents().size(); i++) {
                        if (tri.getDiseaseEvents().get(0).getDiseaseId() == this.getDiseaseEvents().get(i).getDiseaseId()) {
                            //System.out.println("Event exists!");
                            this.setEventExists(true);
                            return true;
                        }
                    }
                }
            }
        }
        return found;
    }

    public Events getEvent(HealthMessage heme) {
        Events event = null;
        int event_id = getEventId(heme);
        List<Events> eventList = this.getEvents();
        for (Events eve : eventList) {
            if (eve.getId() == event_id) {
                return eve;
            }
        }

        return event;
    }

    public int getEventId(HealthMessage heme) {
        if (this.getDiseaseEvents().size() > 0) {
            List<TrisanoHealth> triN = heme.getTrisanoHealth();
            TrisanoHealth tri = triN.get(0);
            if (tri.getDiseaseEvents() != null) {
                for (int i = 0; i < this.getDiseaseEvents().size(); i++) {
                    if (tri.getDiseaseEvents().get(0).getDiseaseId() == this.getDiseaseEvents().get(i).getDiseaseId()) {
                        return this.getDiseaseEvents().get(i).getEventId();
                    }
                }
            }
        }
        return 0;
    }

    public int getEventId() {
        int eventId = 0;
        if (this.events != null && !this.events.isEmpty()
                && this.events.get(0).getId() != null
                && this.events.get(0).getId() > 0) {
            eventId = this.events.get(0).getId();
        }
        return eventId;
    }

    public boolean isLabSame(HealthMessage heme) {
        boolean found = false;
        List<LabsAttributes> labattrs = this.getLabsAttributes();
        List<LabsAttributes> nlabattrs = heme.getTrisanoHealth().get(0).getLabsAttributes();

        if (labattrs.size() > 0) {
            for (PlaceAttributes pa : labattrs) {
                LabsAttributes la = (LabsAttributes) pa;
                if (la.getLabResults() != null) {
                    if (la.getLabResults().size() > 0) {
                        List<LabResults> labresults = la.getLabResults();
                        for (LabResults lr : labresults) {
                            int test_type_id = 0;
                            int specimen_id = 0;
                            int organism_id = 0;

                            if (lr.getTestTypeId() != null) {
                                test_type_id = lr.getTestTypeId();
                            }
                            if (lr.getSpecimenSourceId() != null) {
                                specimen_id = lr.getSpecimenSourceId();
                            }
                            if (lr.getOrganismId() != null) {
                                organism_id = lr.getOrganismId();
                            }

                            if (nlabattrs != null) {
                                for (LabsAttributes npa : nlabattrs) {
                                    List<LabResults> nlabresults = npa.getLabResults();
                                    for (LabResults nlr : nlabresults) {
                                        int nspecimen_id = 0;
                                        int norganism_id = 0;
                                        int ntest_type_id = 0;
                                        if (nlr.getSpecimenSourceId() != null) {
                                            nspecimen_id = nlr.getSpecimenSourceId();
                                        }
                                        if (nlr.getOrganismId() != null) {
                                            norganism_id = nlr.getOrganismId();
                                        }

                                        if (nlr.getTestTypeId() != null) {
                                            ntest_type_id = nlr.getTestTypeId();
                                        }

                                        //specimen_id is optional now as per request of Jon Reid.

                                        if (organism_id != 0 && test_type_id != 0) {
                                            if ((organism_id == norganism_id) && (test_type_id == ntest_type_id)) {
                                                this.setLabSame(true);
                                                //System.out.println("Lab is the Same");
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return found;
    }

    public int getParticipationId(int event_id) {
        if (this.getParticipations() != null) {
            List<Participations> parts = this.getParticipations();
            if (parts.size() > 0) {
                for (Participations pa : parts) {
                    if (pa.getEventId() == event_id) {
                        return pa.getId();
                    }
                }
            }
        }
        return 0;
    }

    private String getPDFFileName() {
        StringBuilder sb = new StringBuilder();
        sb.append("ELR");

//        if (this.diagnosticFacilitiesAttributes != null && !this.diagnosticFacilitiesAttributes.isEmpty()
//                && this.diagnosticFacilitiesAttributes.get(0).getPlace() != null) {
//            Places df = this.diagnosticFacilitiesAttributes.get(0).getPlace();
//            String diagnosticName = null;
//            if (df.getShortName() != null && df.getShortName().trim().length() > 0) {
//                diagnosticName = df.getShortName();
//            } else {
//                diagnosticName = df.getName();
//            }
//            if (diagnosticName != null && diagnosticName.trim().length() > 0) {
//                sb.append(diagnosticName.toUpperCase());
//            }
//        }
        if (this.getLabsAttributes() != null && !this.getLabsAttributes().isEmpty()
                && this.getLabsAttributes().get(0).getPlace() != null) {
            Places lab = this.getLabsAttributes().get(0).getPlace();
            String labName = null;
            if (lab.getShortName() != null && lab.getShortName().trim().length() > 0) {
                labName = lab.getShortName();
            } else {
                labName = lab.getName();
            }
            if (labName != null) {
                sb.append(labName.replaceAll(" ", ""));
            }
        }

        sb.append(formatDateTimeFileName(new Date()));

//        if (this.interestedPartyAttributes != null && !this.interestedPartyAttributes.isEmpty()
//                && this.interestedPartyAttributes.get(0).getPerson() != null) {
//            People p = this.interestedPartyAttributes.get(0).getPerson();
//            if (p.getFirstName() != null && p.getLastName() != null) {
//                sb.append(getPersonInitial(p.getFirstName(), p.getLastName()));
//            }
//        }
        String fileNamePdf = sb.toString().toUpperCase().replaceAll("[^a-zA-Z0-9]", "") + ".pdf";
        return fileNamePdf;
    }

    private String getPersonInitial(String fName, String lName) {
        StringBuilder sb = new StringBuilder();
        sb.append(fName.trim().substring(0, 1));
        sb.append(lName.trim().substring(0, 1));
        return sb.toString();
    }

    private String formatDateTimeFileName(Date date) {
        String dateFormatted = "";
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyHHmmss");
        if (date != null) {
            try {
                dateFormatted = sdf.format(date);
            } catch (Exception e) {
                dateFormatted = date.toString();
            }
        }
        return dateFormatted;
    }
    
    public String getDestinationQueue() {
        if(destinationQueue != null) {
            return destinationQueue.trim();
        }
        return destinationQueue;
    }
    public boolean getCreateAssessmentEvent() {
        return createAssessmentEvent;
    }
    public boolean getCloseIfNotHospital() {
        return closeIfNotHospital;
    }
    public String getAddPositiveLabTask() {
        return addPositiveLabTask;
    }
    public Esp getEsp() {
        return esp;
    }
    private void setWebPage(String typeName) {
        this.webPage = mapTypeToPage.get(typeName);
    }
    public String getWebPage() {
        return this.webPage;
    }
}
