/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "participations")
@NamedQueries({
    @NamedQuery(name = "Participations.findAll", query = "SELECT p FROM Participations p"),
    @NamedQuery(name = "Participations.findById", query = "SELECT p FROM Participations p WHERE p.id = :id"),
    @NamedQuery(name = "Participations.findByPrimaryEntityId", query = "SELECT p FROM Participations p, Events e WHERE p.eventId = e.id AND e.deletedAt is null AND p.primaryEntityId = :primaryEntityId"),
    @NamedQuery(name = "Participations.findBySecondaryEntityId", query = "SELECT p FROM Participations p WHERE p.secondaryEntityId = :secondaryEntityId"),
    @NamedQuery(name = "Participations.findByParticipationStatusId", query = "SELECT p FROM Participations p WHERE p.participationStatusId = :participationStatusId"),
    @NamedQuery(name = "Participations.findByComment", query = "SELECT p FROM Participations p WHERE p.comment = :comment"),
    @NamedQuery(name = "Participations.findByCreatedAt", query = "SELECT p FROM Participations p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "Participations.findByUpdatedAt", query = "SELECT p FROM Participations p WHERE p.updatedAt = :updatedAt"),
    @NamedQuery(name = "Participations.findByEventId", query = "SELECT p FROM Participations p WHERE p.eventId = :eventId"),
    @NamedQuery(name = "Participations.findByEventIdType", query = "SELECT p FROM Participations p WHERE p.eventId = :eventId AND p.type = :type"),
    @NamedQuery(name = "Participations.findByType", query = "SELECT p FROM Participations p WHERE p.type = :type")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "primaryEntityId",
    "secondaryEntityId",
    "participationStatusId",
    "comment",
    "createdAt",
    "updatedAt",
    "eventId",
    "type"
})
@XmlRootElement(name = "participations")
public class Participations implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="participations_id_seq")
    @SequenceGenerator(name="participations_id_seq", sequenceName="participations_id_seq", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "primary_entity_id")
    @XmlElement(name = "primary_entity_id", required = true)
    private Integer primaryEntityId;
    @Column(name = "secondary_entity_id")
    @XmlElement(name = "secondary_entity_id", required = true)
    private Integer secondaryEntityId;
    @Column(name = "participation_status_id")
    @XmlElement(name = "participation_status_id", required = true)
    private Integer participationStatusId;
    @Column(name = "comment")
    @XmlElement(name = "comment", required = true)
    private String comment;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "type")
    @XmlElement(name = "type", required = true)
    private String type;

    public Participations() {
    }

    public Participations(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPrimaryEntityId() {
        return primaryEntityId;
    }

    public void setPrimaryEntityId(Integer primaryEntityId) {
        this.primaryEntityId = primaryEntityId;
    }

    public Integer getSecondaryEntityId() {
        return secondaryEntityId;
    }

    public void setSecondaryEntityId(Integer secondaryEntityId) {
        this.secondaryEntityId = secondaryEntityId;
    }

    public Integer getParticipationStatusId() {
        return participationStatusId;
    }

    public void setParticipationStatusId(Integer participationStatusId) {
        this.participationStatusId = participationStatusId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Participations)) {
            return false;
        }
        Participations other = (Participations) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.Participations[id=" + id + "]";
    }

}
