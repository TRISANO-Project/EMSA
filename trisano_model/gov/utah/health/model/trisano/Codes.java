/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "codes")
@NamedQueries({
    @NamedQuery(name = "Codes.findAll", query = "SELECT c FROM Codes c"),
    @NamedQuery(name = "Codes.findById", query = "SELECT c FROM Codes c WHERE c.id = :id"),

    // find for RE
    @NamedQuery(name = "Codes.findByCodeCodeName", query = "SELECT c FROM Codes c WHERE lower(c.codeName) like lower(:codeName) AND  lower(c.theCode) like lower(:theCode)"),

    @NamedQuery(name = "Codes.findByCodeName", query = "SELECT c FROM Codes c WHERE lower(c.codeName) like lower(:codeName)"),
    @NamedQuery(name = "Codes.findByTheCode", query = "SELECT c FROM Codes c WHERE lower(c.theCode) like lower(:theCode)"),
    @NamedQuery(name = "Codes.findByCodeDescription", query = "SELECT c FROM Codes c WHERE c.codeDescription = :codeDescription"),
    @NamedQuery(name = "Codes.findBySortOrder", query = "SELECT c FROM Codes c WHERE c.sortOrder = :sortOrder"),
    @NamedQuery(name = "Codes.findByDeletedAt", query = "SELECT c FROM Codes c WHERE c.deletedAt = :deletedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "codeName",
    "theCode",
    "codeDescription",
    "sortOrder",
    "deletedAt"
})
@XmlRootElement(name = "codes")
public class Codes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(required = true)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code_name")
    @XmlElement(name="code_name",required = true)
    private String codeName;
    @Basic(optional = false)
    @Column(name = "the_code")
    @XmlElement(name="the_code",required = true)
    private String theCode;
    @Column(name = "code_description")
    @XmlElement(name="code_description",required = true)
    private String codeDescription;
    @Column(name = "sort_order")
    @XmlElement(name="sort_order",required = true)
    private Integer sortOrder;
    @Column(name = "deleted_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name="deleted_at",required = true)
    private Date deletedAt;

    public Codes() {
    }

    public Codes(Integer id) {
        this.id = id;
    }

    public Codes(Integer id, String codeName, String theCode) {
        this.id = id;
        this.codeName = codeName;
        this.theCode = theCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getTheCode() {
        return theCode;
    }

    public void setTheCode(String theCode) {
        this.theCode = theCode;
    }

    public String getCodeDescription() {
        return codeDescription;
    }

    public void setCodeDescription(String codeDescription) {
        this.codeDescription = codeDescription;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Codes)) {
            return false;
        }
        Codes other = (Codes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Codes[id=" + id + "]";
    }

}
