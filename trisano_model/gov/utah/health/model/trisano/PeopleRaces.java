/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "people_races")
@NamedQueries({
    @NamedQuery(name = "PeopleRaces.findAll", query = "SELECT p FROM PeopleRaces p"),
    @NamedQuery(name = "PeopleRaces.findByRaceId", query = "SELECT p FROM PeopleRaces p WHERE p.peopleRacesPK.raceId = :raceId"),
    @NamedQuery(name = "PeopleRaces.findByEntityId", query = "SELECT p FROM PeopleRaces p WHERE p.peopleRacesPK.entityId = :entityId"),
    @NamedQuery(name = "PeopleRaces.findByCreatedAt", query = "SELECT p FROM PeopleRaces p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "PeopleRaces.findByUpdatedAt", query = "SELECT p FROM PeopleRaces p WHERE p.updatedAt = :updatedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "peopleRacesPK",
    "createdAt",
    "updatedAt"
})
@XmlRootElement(name = "people_races")
public class PeopleRaces implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    @XmlElement(name = "serial_version_uid", required = true)
    protected PeopleRacesPK peopleRacesPK;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;

    public PeopleRaces() {
    }

    public PeopleRaces(PeopleRacesPK peopleRacesPK) {
        this.peopleRacesPK = peopleRacesPK;
    }

    public PeopleRaces(int raceId, int entityId) {
        this.peopleRacesPK = new PeopleRacesPK(raceId, entityId);
    }

    public PeopleRacesPK getPeopleRacesPK() {
        return peopleRacesPK;
    }

    public void setPeopleRacesPK(PeopleRacesPK peopleRacesPK) {
        this.peopleRacesPK = peopleRacesPK;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (peopleRacesPK != null ? peopleRacesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PeopleRaces)) {
            return false;
        }
        PeopleRaces other = (PeopleRaces) object;
        if ((this.peopleRacesPK == null && other.peopleRacesPK != null) || (this.peopleRacesPK != null && !this.peopleRacesPK.equals(other.peopleRacesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.PeopleRaces[peopleRacesPK=" + peopleRacesPK + "]";
    }
}
