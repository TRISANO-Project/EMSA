package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "core_section")
@NamedQueries({
    @NamedQuery(name = "CoreSection.findAll", query = "SELECT c FROM CoreSection c ORDER BY c.seq"),
    @NamedQuery(name = "CoreSection.findById", query = "SELECT c FROM CoreSection c"),
    @NamedQuery(name = "CoreSection.findByType", query = "SELECT c FROM CoreSection c WHERE c.type = :type ORDER BY c.seq")})
public class CoreSection implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "core_section_id_seq")
    @SequenceGenerator(name = "core_section_id_seq", sequenceName = "core_section_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "type")
    private Integer type;
    @Column(name = "name")
    private String name;
    @Column(name = "label")
    private String label;
    @Column(name = "seq")
    private Integer seq;

    @Transient
    private List<CoreSection> sections;
    @Transient
    private List<CoreElement> elements;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public List<CoreSection> getSections() {
        return sections;
    }

    public void setSections(List<CoreSection> sections) {
        this.sections = sections;
    }

    public List<CoreElement> getElements() {
        return elements;
    }

    public void setElements(List<CoreElement> elements) {
        this.elements = elements;
    }

    public void addElement(CoreElement element) {
        if (this.elements == null) {
            this.elements = new ArrayList<CoreElement>();
        }
        if (!this.elements.contains(element)) {
            this.elements.add(element);
        }

    }

    public Boolean getIsAddress() {
        Boolean isAddy = Boolean.FALSE;
        if (this.name != null && this.name.indexOf("address") >= 0) {
            isAddy = Boolean.TRUE;
        }
        return isAddy;
    }
}
