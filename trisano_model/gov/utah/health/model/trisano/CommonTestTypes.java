/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "common_test_types")
@NamedQueries({
    @NamedQuery(name = "CommonTestTypes.findAll", query = "SELECT c FROM CommonTestTypes c ORDER BY c.commonName"),
    @NamedQuery(name = "CommonTestTypes.findById", query = "SELECT c FROM CommonTestTypes c WHERE c.id = :id"),
    @NamedQuery(name = "CommonTestTypes.findByCommonName", query = "SELECT c FROM CommonTestTypes c WHERE lower(c.commonName) like lower(:commonName)"),
    @NamedQuery(name = "CommonTestTypes.findByCreatedAt", query = "SELECT c FROM CommonTestTypes c WHERE c.createdAt = :createdAt"),
    @NamedQuery(name = "CommonTestTypes.findByUpdatedAt", query = "SELECT c FROM CommonTestTypes c WHERE c.updatedAt = :updatedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "commonName",
    "createdAt",
    "updatedAt"
})
@XmlRootElement(name = "common_test_types")
public class CommonTestTypes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(required = true)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "common_name")
    @XmlElement(name = "common_name", required = true)
    private String commonName;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;

    public CommonTestTypes() {
    }

    public CommonTestTypes(Integer id) {
        this.id = id;
    }

    public CommonTestTypes(Integer id, String commonName) {
        this.id = id;
        this.commonName = commonName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof CommonTestTypes)) {
            return false;
        }
        CommonTestTypes other = (CommonTestTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.CommonTestTypes[id=" + id + "]";
    }
}
