package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@Entity
@Table(name = "attachments")
@NamedQueries({
    @NamedQuery(name = "Attachments.findAll", query = "SELECT a FROM Attachments a"),
    @NamedQuery(name = "Attachments.findById", query = "SELECT a FROM Attachments a WHERE a.id = :id"),
    @NamedQuery(name = "Attachments.findByDbFileId", query = "SELECT a FROM Attachments a WHERE a.dbFileId = :dbFileId"),
    @NamedQuery(name = "Attachments.findByMasterId", query = "SELECT a FROM Attachments a WHERE a.masterId = :masterId"),
    @NamedQuery(name = "Attachments.findByOutbreakUpdateId", query = "SELECT a FROM Attachments a WHERE a.outbreakUpdateId = :outbreakUpdateId ORDER BY a.createdAt DESC"),
    @NamedQuery(name = "Attachments.findByFilename", query = "SELECT a FROM Attachments a WHERE a.filename = :filename"),
    @NamedQuery(name = "Attachments.findByCategory", query = "SELECT a FROM Attachments a WHERE a.category = :category"),
    @NamedQuery(name = "Attachments.findByCreatedAt", query = "SELECT a FROM Attachments a WHERE a.createdAt = :createdAt")
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "masterId",
    "outbreakId",
    "outbreakUpdateId",
    "eventId",
    "filename",
    "dbFileId",
    "category",
    "createdAt",
    "updatedAt",
    "contentType",
    "dbFile"
})
@XmlRootElement(name = "attachments")
public class Attachments implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "people_id_seq")
    @SequenceGenerator(name = "people_id_seq", sequenceName = "people_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "master_id")
    @XmlElement(name = "master_id", required = true)
    private Integer masterId;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "outbreak_id")
    @XmlElement(name = "outbreak_id", required = true)
    private Integer outbreakId;
    @Column(name = "outbreak_update_id")
    @XmlElement(name = "outbreak_update_id", required = true)
    private Integer outbreakUpdateId;
    @Column(name = "db_file_id")
    @XmlElement(name = "db_file_id", required = true)
    private Integer dbFileId;
    @Column(name = "filename")
    @XmlElement(name = "filename", required = true)
    private String filename;
    @Column(name = "category")
    @XmlElement(name = "category", required = true)
    private String category;
    @Column(name = "content_type")
    @XmlElement(name = "content_type", required = true)
    private String contentType;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;

    @Transient
    @XmlElement(name = "db_file")
    DbFiles dbFile;
    
    
    
    public Attachments() {
    }

    public Attachments(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DbFiles getDbFile() {
        return dbFile;
    }

    public void setDbFile(DbFiles dbFile) {
        this.dbFile = dbFile;
    }

    public Integer getOutbreakUpdateId() {
        return outbreakUpdateId;
    }

    public void setOutbreakUpdateId(Integer outbreakUpdateId) {
        this.outbreakUpdateId = outbreakUpdateId;
    }

    public Integer getMasterId() {
        return masterId;
    }

    public void setMasterId(Integer masterId) {
        this.masterId = masterId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getDbFileId() {
        return dbFileId;
    }

    public void setDbFileId(Integer dbFileId) {
        this.dbFileId = dbFileId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attachments)) {
            return false;
        }
        Attachments other = (Attachments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Attachments[ id=" + id + " ]";
    }
}
