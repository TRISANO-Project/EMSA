package gov.utah.health.model.trisano;

/**
 * @author UDOH
 */
public class AuditAction {

    public static AuditAction OUTBREAK_CREATE = new AuditAction(1,"Create Outbreak");
    public static AuditAction OUTBREAK_UPDATE = new AuditAction(2,"Update Outbreak");
    public static AuditAction OUTBREAK_ADD_EVENT = new AuditAction(3,"Add Event");
    public static AuditAction OUTBREAK_REMOVE_EVENT = new AuditAction(4,"Remove Event");
    public static AuditAction OUTBREAK_ADD_OUTBREAK = new AuditAction(5,"Add Associated Outbreak");
    public static AuditAction OUTBREAK_REMOVE_OUTBREAK = new AuditAction(6,"Remove Associated Outbreak");

    public AuditAction(){}
    
    AuditAction(Integer number,String action){
        this.number=number;
        this.action=action;
    }

    private Integer number;
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public boolean equals(Type Type){

            boolean equals = false;
            if(Type.getNumber().equals(Type.getNumber())){
                equals = true;
            }

            return equals;
    }

    @Override
    public String toString(){
        return action.toString();
    }
}
