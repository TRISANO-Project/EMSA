package gov.utah.health.model.trisano;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clinician",
    "telephones"
})
@XmlRootElement(name = "clinicians_attributes")



public class CliniciansAttributes {

    @XmlElement(name="clinician", required = true)
    private People clinician;
    @XmlElement(name="telephones",required = true)
    private List<Telephones> telephones;

    public People getClinician() {
        return clinician;
    }

    public void setClinician(People clinician) {
        this.clinician = clinician;
    }

    public List<Telephones> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephones> telephones) {
        this.telephones = telephones;
    }



}
