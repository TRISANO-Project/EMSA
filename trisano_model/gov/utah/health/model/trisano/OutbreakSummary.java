package gov.utah.health.model.trisano;

import java.util.Date;
import java.util.List;

/**
 * @author UDOH
 */
public class OutbreakSummary {
    
    private Date begin;
    private Date end;
    private List<OutbreakJurisdictionCount> jurisdictionCounts;    
    private OutbreakJurisdictionCount outbreakCounts;  
    private List<OutbreakEventAttributes> events;
    private Outbreak outbreak;
    
    public OutbreakSummary() {}

    public Outbreak getOutbreak() {
        return outbreak;
    }

    public void setOutbreak(Outbreak outbreak) {
        this.outbreak = outbreak;
    }


    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public List<OutbreakJurisdictionCount> getJurisdictionCounts() {
        return jurisdictionCounts;
    }

    public void setJurisdictionCounts(List<OutbreakJurisdictionCount> jurisdictionCounts) {
        this.jurisdictionCounts = jurisdictionCounts;
    }

    public OutbreakJurisdictionCount getOutbreakCounts() {
        return outbreakCounts;
    }

    public void setOutbreakCounts(OutbreakJurisdictionCount outbreakCounts) {
        this.outbreakCounts = outbreakCounts;
    }

    public List<OutbreakEventAttributes> getEvents() {
        return events;
    }

    public void setEvents(List<OutbreakEventAttributes> events) {
        this.events = events;
    }
    
    
    
}
