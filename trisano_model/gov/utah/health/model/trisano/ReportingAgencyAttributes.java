package gov.utah.health.model.trisano;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingAgencyAttributes", propOrder = {

})
@XmlRootElement(name = "reporting_agency_attributes")

public class ReportingAgencyAttributes extends PlaceAttributes {

    public ReportingAgencyAttributes() {
        super.placeType = Type.REPORTING_AGENCY.getNumber();
    }

}
