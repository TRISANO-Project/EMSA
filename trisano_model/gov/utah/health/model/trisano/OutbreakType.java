package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;

public class OutbreakType {

    public static final OutbreakType ATTACHMENT_SUMMARY = new OutbreakType(1, "Summary");
    public static final OutbreakType ATTACHMENT_REPORT = new OutbreakType(2, "Report");
    public static final OutbreakType ATTACHMENT_OTHER = new OutbreakType(3, "Other");
    public static final List<OutbreakType> attachment_types = new ArrayList<OutbreakType>();
    public static final OutbreakType UPDATE_NEWS_REPORT = new OutbreakType(1, "Lab");
    public static final OutbreakType UPDATE_ENCOUNTER = new OutbreakType(2, "Environmental Health");
    public static final OutbreakType UPDATE_OTHER = new OutbreakType(3, "Media");
    public static final List<OutbreakType> update_types = new ArrayList<OutbreakType>();
    public static final OutbreakType LOCATION_TYPE_STATE = new OutbreakType(1, "State Outbreak");
    public static final OutbreakType LOCATION_TYPE_LOCAL = new OutbreakType(2, "Local Outbreak");
    public static final List<OutbreakType> location_types = new ArrayList<OutbreakType>();
    public static final OutbreakType ACCESS_TYPE_NONE = new OutbreakType(0, "None");
    public static final OutbreakType ACCESS_TYPE_VIEW = new OutbreakType(1, "View");
    public static final OutbreakType ACCESS_TYPE_EDIT = new OutbreakType(2, "Edit");
    public static final List<OutbreakType> access_types = new ArrayList<OutbreakType>();

    static {
        attachment_types.add(ATTACHMENT_REPORT);
        attachment_types.add(ATTACHMENT_OTHER);
        attachment_types.add(ATTACHMENT_SUMMARY);

        update_types.add(UPDATE_NEWS_REPORT);
        update_types.add(UPDATE_ENCOUNTER);
        update_types.add(UPDATE_OTHER);

        location_types.add(LOCATION_TYPE_STATE);
        location_types.add(LOCATION_TYPE_LOCAL);

        access_types.add(ACCESS_TYPE_NONE);
        access_types.add(ACCESS_TYPE_VIEW);
        access_types.add(ACCESS_TYPE_EDIT);
    }

    OutbreakType(Integer id, String label) {
        this.id = id;
        this.label = label;
    }
    private Integer id;
    private String label;

    public static String getUpdateType(Integer id) {
        String label = null;
        for (OutbreakType t : OutbreakType.update_types) {
            if (t.getId().equals(id)) {
                label = t.getLabel();
                break;
            }
        }
        return label;
    }

    public static String getLocationType(Integer id) {
        String label = null;
        for (OutbreakType t : OutbreakType.location_types) {
            if (t.getId().equals(id)) {
                label = t.getLabel();
                break;
            }
        }
        return label;
    }
    public static String getAccessType(Integer id) {
        String label = null;
        for (OutbreakType t : OutbreakType.access_types) {
            if (t.getId().equals(id)) {
                label = t.getLabel();
                break;
            }
        }
        return label;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean equals(OutbreakType t) {

        boolean equals = false;
        if (t.getId().equals(t.getId())) {
            equals = true;
        }

        return equals;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
