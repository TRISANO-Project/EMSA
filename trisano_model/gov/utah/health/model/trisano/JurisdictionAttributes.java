package gov.utah.health.model.trisano;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
})
@XmlRootElement(name = "jurisdiction_attributes")

public class JurisdictionAttributes extends PlaceAttributes {

    public JurisdictionAttributes() {
        super.placeType = Type.JURISDICTION.getNumber();
    }
}
