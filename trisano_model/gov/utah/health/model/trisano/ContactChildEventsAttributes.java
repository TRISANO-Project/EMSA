package gov.utah.health.model.trisano;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "interestedPartyAttributes",
    "participationsContacts"
})
@XmlRootElement(name = "contact_child_events_attributes")

public class ContactChildEventsAttributes {

    @XmlElement(name="interested_party_attributes", required = true)
    private InterestedPartyAttributes interestedPartyAttributes;
    @XmlElement(name="participations_contacts",required = true)
    private ParticipationsContacts participationsContacts;

    public ContactChildEventsAttributes() {
    }

    public InterestedPartyAttributes getInterestedPartyAttributes() {
        return interestedPartyAttributes;
    }

    public void setInterestedPartyAttributes(InterestedPartyAttributes interestedPartyAttributes) {
        this.interestedPartyAttributes = interestedPartyAttributes;
    }

    public ParticipationsContacts getParticipationsContacts() {
        return participationsContacts;
    }

    public void setParticipationsContacts(ParticipationsContacts participationsContacts) {
        this.participationsContacts = participationsContacts;
    }


}
