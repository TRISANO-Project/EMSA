/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "diseases")
@NamedQueries({
    @NamedQuery(name = "Diseases.findAll", query = "SELECT d FROM Diseases d ORDER BY d.diseaseName"),
    @NamedQuery(name = "Diseases.findAllActive", query = "SELECT d FROM Diseases d WHERE d.active = true ORDER BY d.diseaseName"),
    @NamedQuery(name = "Diseases.findAllActiveAndSensitive", query = "SELECT d FROM Diseases d WHERE d.active = true AND d.sensitive = :sensitive ORDER BY d.diseaseName"),
    @NamedQuery(name = "Diseases.findById", query = "SELECT d FROM Diseases d WHERE d.id = :id"),
    @NamedQuery(name = "Diseases.findNameById", query = "SELECT d.diseaseName FROM Diseases d WHERE d.id = :id"),
    // find for RE
    @NamedQuery(name = "Diseases.findByDiseaseName", query = "SELECT d FROM Diseases d WHERE lower(d.diseaseName) like lower(:diseaseName)"),
    @NamedQuery(name = "Diseases.findByContactLeadIn", query = "SELECT d FROM Diseases d WHERE d.contactLeadIn = :contactLeadIn"),
    @NamedQuery(name = "Diseases.findByPlaceLeadIn", query = "SELECT d FROM Diseases d WHERE d.placeLeadIn = :placeLeadIn"),
    @NamedQuery(name = "Diseases.findByTreatmentLeadIn", query = "SELECT d FROM Diseases d WHERE d.treatmentLeadIn = :treatmentLeadIn"),
    @NamedQuery(name = "Diseases.findByActive", query = "SELECT d FROM Diseases d WHERE d.active = :active"),
    @NamedQuery(name = "Diseases.findByCdcCode", query = "SELECT d FROM Diseases d WHERE d.cdcCode = :cdcCode")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "diseaseName",
    "contactLeadIn",
    "placeLeadIn",
    "treatmentLeadIn",
    "active",
    "cdcCode",
    "sensitive"
})
@XmlRootElement(name = "diseases")
public class Diseases implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "diseases_id_seq")
    @SequenceGenerator(name = "diseases_id_seq", sequenceName = "diseases_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "disease_name")
    @XmlElement(name = "disease_name", required = true)
    private String diseaseName;
    @Column(name = "contact_lead_in")
    @XmlElement(name = "contact_lead_in", required = true)
    private String contactLeadIn;
    @Column(name = "place_lead_in")
    @XmlElement(name = "place_lead_in", required = true)
    private String placeLeadIn;
    @Column(name = "treatment_lead_in")
    @XmlElement(name = "treatment_lead_in", required = true)
    private String treatmentLeadIn;
    @Column(name = "active")
    @XmlElement(name = "active", required = true)
    private Boolean active;
    @Column(name = "cdc_code")
    @XmlElement(name = "cdc_code", required = true)
    private String cdcCode;
    @Column(name = "sensitive")
    @XmlElement(name = "sensitive", required = true)
    private Boolean sensitive;

    public Diseases() {
    }

    public Diseases(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSensitive() {
        return sensitive;
    }

    public void setSensitive(Boolean sensitive) {
        this.sensitive = sensitive;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getContactLeadIn() {
        return contactLeadIn;
    }

    public void setContactLeadIn(String contactLeadIn) {
        this.contactLeadIn = contactLeadIn;
    }

    public String getPlaceLeadIn() {
        return placeLeadIn;
    }

    public void setPlaceLeadIn(String placeLeadIn) {
        this.placeLeadIn = placeLeadIn;
    }

    public String getTreatmentLeadIn() {
        return treatmentLeadIn;
    }

    public void setTreatmentLeadIn(String treatmentLeadIn) {
        this.treatmentLeadIn = treatmentLeadIn;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCdcCode() {
        return cdcCode;
    }

    public void setCdcCode(String cdcCode) {
        this.cdcCode = cdcCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Diseases)) {
            return false;
        }
        Diseases other = (Diseases) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.Diseases[id=" + id + "]";
    }
}
