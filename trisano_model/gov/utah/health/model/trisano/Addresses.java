
package gov.utah.health.model.trisano;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "addresses")
@NamedQueries({
    @NamedQuery(name = "Addresses.findAll", query = "SELECT a FROM Addresses a"),
    @NamedQuery(name = "Addresses.findById", query = "SELECT a FROM Addresses a WHERE a.id = :id"),
    @NamedQuery(name = "Addresses.findByEntityId", query = "SELECT a FROM Addresses a WHERE a.entityId = :entityId"),
    @NamedQuery(name = "Addresses.findByEntityIdNullEventId", query = "SELECT a FROM Addresses a WHERE a.entityId = :entityId AND a.eventId IS NULL"),
    @NamedQuery(name = "Addresses.findByEventId", query = "SELECT a FROM Addresses a WHERE a.eventId = :eventId"),
    @NamedQuery(name = "Addresses.findByLocationId", query = "SELECT a FROM Addresses a WHERE a.locationId = :locationId"),
    @NamedQuery(name = "Addresses.findByCountyId", query = "SELECT a FROM Addresses a WHERE a.countyId = :countyId"),
    @NamedQuery(name = "Addresses.findByStateId", query = "SELECT a FROM Addresses a WHERE a.stateId = :stateId"),
    @NamedQuery(name = "Addresses.findByStreetName", query = "SELECT a FROM Addresses a WHERE a.streetName = :streetName"),
    @NamedQuery(name = "Addresses.findByUnitNumber", query = "SELECT a FROM Addresses a WHERE a.unitNumber = :unitNumber"),
    @NamedQuery(name = "Addresses.findByPostalCode", query = "SELECT a FROM Addresses a WHERE a.postalCode = :postalCode"),
    @NamedQuery(name = "Addresses.findByCreatedAt", query = "SELECT a FROM Addresses a WHERE a.createdAt = :createdAt"),
    @NamedQuery(name = "Addresses.findByUpdatedAt", query = "SELECT a FROM Addresses a WHERE a.updatedAt = :updatedAt"),
    @NamedQuery(name = "Addresses.findByCity", query = "SELECT a FROM Addresses a WHERE a.city = :city"),
    @NamedQuery(name = "Addresses.findByEntityLocationTypeId", query = "SELECT a FROM Addresses a WHERE a.entityLocationTypeId = :entityLocationTypeId"),
    @NamedQuery(name = "Addresses.findByLongitude", query = "SELECT a FROM Addresses a WHERE a.longitude = :longitude"),
    @NamedQuery(name = "Addresses.findByLatitude", query = "SELECT a FROM Addresses a WHERE a.latitude = :latitude")})

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = {
    "id",
    "locationId",
    "countyId",
    "stateId",
    "streetName",
    "unitNumber",
    "postalCode",
    "createdAt",
    "updatedAt",
    "city",
    "entityLocationTypeId",
    "longitude",
    "latitude",
    "eventId",
    "entityId",
    "googleAddress"
})
@XmlRootElement(name = "addresses")

    public class Addresses implements Cloneable, Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="addresses_id_seq")
    @SequenceGenerator(name="addresses_id_seq", sequenceName="addresses_id_seq", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "location_id")
    @XmlElement(name = "location_id", required = true)
    private Integer locationId;
    @Column(name = "county_id")
    @XmlElement(name = "county_id", required = true)
    private Integer countyId;
    @Column(name = "state_id")
    @XmlElement(name = "state_id", required = true)
    private Integer stateId;
    @Column(name = "street_name")
    @XmlElement(name = "street_name", required = true)
    private String streetName;
    @Column(name = "unit_number")
    @XmlElement(name = "unit_number", required = true)
    private String unitNumber;
    @Column(name = "postal_code")
    @XmlElement(name = "postal_code", required = true)
    private String postalCode;
    @Column(name = "created_at")
    @XmlElement(name = "created_at", required = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "city")
    @XmlElement(name = "city", required = true)
    private String city;
    @Column(name = "entity_location_type_id")
    @XmlElement(name = "entity_location_type_id", required = true)
    private Integer entityLocationTypeId;
    @Column(name = "longitude")
    @XmlElement(name = "longitude", required = true)
    private Double longitude;
    @Column(name = "latitude")
    @XmlElement(name = "latitude", required = true)
    private Double latitude;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "entity_id")
    @XmlElement(name = "entity_id", required = true)
    private Integer entityId;
    @XmlElement(name = "google_address", required = true)
    @Transient
    private String googleAddress;

    public Addresses() {
    }

    public Addresses(Integer id) {
        this.id = id;
    }

    public String getGoogleAddress() {
        return googleAddress;
    }

    public void setGoogleAddress(String googleAddress) {
        this.googleAddress = googleAddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getEntityLocationTypeId() {
        return entityLocationTypeId;
    }

    public void setEntityLocationTypeId(Integer entityLocationTypeId) {
        this.entityLocationTypeId = entityLocationTypeId;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Addresses)) {
            return false;
        }
        Addresses other = (Addresses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public Addresses clone() throws  CloneNotSupportedException {
        return (Addresses) super.clone();
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.Addresses[id=" + id + "]";
    }

}
