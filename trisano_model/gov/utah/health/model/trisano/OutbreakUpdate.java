package gov.utah.health.model.trisano;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@Entity
@Table(name = "outbreak_updates")
@NamedQueries({
    @NamedQuery(name = "OutbreakUpdate.maxRecordNumber", query = "SELECT max(ou.id) FROM OutbreakUpdate ou"),
    @NamedQuery(name = "OutbreakUpdate.findAll", query = "SELECT o FROM OutbreakUpdate o"),
    @NamedQuery(name = "OutbreakUpdate.findById", query = "SELECT o FROM OutbreakUpdate o WHERE o.id = :id"),
    @NamedQuery(name = "OutbreakUpdate.findByOutbreakId", query = "SELECT o FROM OutbreakUpdate o WHERE o.outbreakId =:outbreakId ORDER BY o.createdAt DESC"),
    @NamedQuery(name = "OutbreakUpdate.findByUserId", query = "SELECT o FROM OutbreakUpdate o WHERE o.userId =:userId"),})
/**
 *
 * @author UDOH
 */
public class OutbreakUpdate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "outbreak_updates_id")
    @SequenceGenerator(name = "outbreak_updates_id", sequenceName = "outbreak_updates_id", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "outbreak_id")
    private Integer outbreakId;
    @Column(name = "type")
    private Integer type;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "description")
    private String description;
    @Transient
    private Users user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeLabel() {
        String typeLabel = null;
        if (this.type != null) {
            typeLabel = OutbreakType.getUpdateType(this.type);
        }
        return typeLabel;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getCreatedAtFormatted() {
        String date = null;
        if (this.createdAt != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm aaa");
            date = formatter.format(this.createdAt);
        }
        return date;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
