package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "people")
@NamedQueries({
    @NamedQuery(name = "People.find",
            query = "SELECT p FROM People p WHERE trim(both ' ' from lower(p.firstName)) = lower(:firstName) AND trim(both ' ' from lower(p.lastName)) = lower(:lastName) AND p.birthDate = :birthDate"),
    @NamedQuery(name = "People.findByName",
            query = "SELECT p FROM People p WHERE trim(both ' ' from lower(p.firstName)) = lower(:firstName) AND trim(both ' ' from lower(p.lastName)) = lower(:lastName)"),
    @NamedQuery(name = "People.findByNameType",
            query = "SELECT p FROM People p WHERE trim(both ' ' from lower(p.firstName)) = lower(:firstName) AND trim(both ' ' from lower(p.lastName)) = lower(:lastName) AND p.personType = :personType"),
    @NamedQuery(name = "People.search",
            query = "SELECT p FROM People p WHERE trim(both ' ' from lower(p.firstName)) like lower(:firstName) AND trim(both ' ' from lower(p.lastName)) like lower(:lastName)"),
    @NamedQuery(name = "People.searchName",
            query = "SELECT p FROM People p WHERE "
            + "trim(both ' ' from lower(p.firstName)) like lower(:firstName) "
            + "AND trim(both ' ' from lower(p.lastName)) like lower(:lastName) "),
    @NamedQuery(name = "People.searchNameBday",
            query = "SELECT p FROM People p WHERE "
            + "(trim(both ' ' from lower(p.firstName)) like lower(:firstName) AND trim(both ' ' from lower(p.lastName)) like lower(:lastName)) "
            + "AND (p.birthDate = :birthDate) "
            + "AND person_score(:firstNameScore,p.firstName,:lastNameScore,p.lastName,:birthDateScore,p.birthDate) >= :minScore"),
    @NamedQuery(name = "People.searchSoundex",
            query = "SELECT p FROM People p WHERE "
            + "(soundex(p.firstName) = soundex(:firstName) OR soundex(p.lastName) = soundex(:lastName))"
            + "AND (lower(substring(p.firstName,1,1)) = lower(:firstNameFirstChar) "
            + "AND lower(substring(p.lastName,1,1)) = lower(:lastNameFirstChar)) "
            + "AND person_score(:firstNameScore,p.firstName,:lastNameScore,p.lastName,:birthDateScore,p.birthDate) >= :minScore"),
    @NamedQuery(name = "People.birthDate",
            query = "SELECT p FROM People p WHERE p.birthDate = :birthDate"),
    @NamedQuery(name = "People.findAll", query = "SELECT p FROM People p"),
    @NamedQuery(name = "People.findById", query = "SELECT p FROM People p WHERE p.id = :id"),
    @NamedQuery(name = "People.findByEntityId", query = "SELECT p FROM People p WHERE p.entityId = :entityId")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "entityId",
    "birthGenderId",
    "ethnicityId",
    "primaryLanguageId",
    "firstName",
    "middleName",
    "lastName",
    "birthDate",
    "dateOfDeath",
    "createdAt",
    "updatedAt",
    "foodHandlerId",
    "ageTypeId",
    "approximateAgeNoBirthday",
    "live",
    "nextVer",
    "previousVer",
    "personType",
    "matchScore"
})
@XmlRootElement(name = "people")
public class People implements Serializable, Comparable<People> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "people_id_seq")
    @SequenceGenerator(name = "people_id_seq", sequenceName = "people_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "entity_id")
    @XmlElement(name = "entity_id", required = true)
    private Integer entityId;
    @Column(name = "birth_gender_id")
    @XmlElement(name = "birth_gender_id", required = true)
    private Integer birthGenderId;
    @Column(name = "ethnicity_id")
    @XmlElement(name = "ethnicity_id", required = true)
    private Integer ethnicityId;
    @Column(name = "primary_language_id")
    @XmlElement(name = "primary_language_id", required = true)
    private Integer primaryLanguageId;
    @Column(name = "first_name")
    @XmlElement(name = "first_name", required = true)
    private String firstName;
    @Column(name = "middle_name")
    @XmlElement(name = "middle_name", required = true)
    private String middleName;
    @Column(name = "last_name")
    @XmlElement(name = "last_name", required = true)
    private String lastName;
    @Column(name = "birth_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "birth_date", required = true)
    private Date birthDate;
    @Column(name = "date_of_death")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "date_of_death", required = true)
    private Date dateOfDeath;
    @Column(name = "food_handler_id")
    @XmlElement(name = "food_handler_id", required = true)
    private Integer foodHandlerId;
    @Column(name = "age_type_id")
    @XmlElement(name = "age_type_id", required = true)
    private Integer ageTypeId;
    @Column(name = "approximate_age_no_birthday")
    @XmlElement(name = "approximate_age_no_birthday", required = true)
    private Integer approximateAgeNoBirthday;
    @Column(name = "live")
    @XmlElement(name = "live", required = true)
    private String live;
    @Column(name = "next_ver")
    @XmlElement(name = "next_ver", required = true)
    private String nextVer;
    @Column(name = "previous_ver")
    @XmlElement(name = "previous_ver", required = true)
    private String previousVer;
    @Column(name = "person_type")
    @XmlElement(name = "person_type", required = true)
    private String personType;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Transient
    @XmlElement(name = "match_score", required = true)
    private Integer matchScore;
    @Transient
    private transient int nameDistance;
    @Transient
    private transient int bdayDistance;

    public People() {
    }

    public People(Integer id) {
        this.id = id;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public Integer getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(Integer matchScore) {
        this.matchScore = matchScore;
    }

    public int getBdayDistance() {
        return bdayDistance;
    }

    public void setBdayDistance(int bdayDistance) {
        this.bdayDistance = bdayDistance;
    }

    public String getFullName() {
        String fullname = null;
        if (this.firstName != null) {
            fullname = this.firstName.trim();
        }
        if (this.lastName != null && fullname != null) {
            fullname = fullname + " " + this.lastName.trim();
        } else if (this.lastName != null) {
            fullname = this.lastName.trim();
        }
        if (fullname != null) {
            fullname = fullname.toLowerCase();
        }
        return fullname;
    }

    public String getLastNameFirstName() {

        StringBuilder sb = new StringBuilder();
        if (this.lastName != null) {
            sb.append(this.lastName);
        }
        if (this.firstName != null) {
            sb.append(", ");
            sb.append(this.firstName);
        }
        return sb.toString();
    }

    public int getNameDistance() {
        return nameDistance;
    }

    public void setNameDistance(int nameDistance) {
        this.nameDistance = nameDistance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBirthGenderId() {
        return birthGenderId;
    }

    public void setBirthGenderId(Integer birthGenderId) {
        this.birthGenderId = birthGenderId;
    }

    public Integer getEthnicityId() {
        return ethnicityId;
    }

    public void setEthnicityId(Integer ethnicityId) {
        this.ethnicityId = ethnicityId;
    }

    public Integer getPrimaryLanguageId() {
        return primaryLanguageId;
    }

    public void setPrimaryLanguageId(Integer primaryLanguageId) {
        this.primaryLanguageId = primaryLanguageId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(Date dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public Integer getFoodHandlerId() {
        return foodHandlerId;
    }

    public void setFoodHandlerId(Integer foodHandlerId) {
        this.foodHandlerId = foodHandlerId;
    }

    public Integer getAgeTypeId() {
        return ageTypeId;
    }

    public void setAgeTypeId(Integer ageTypeId) {
        this.ageTypeId = ageTypeId;
    }

    public Integer getApproximateAgeNoBirthday() {
        return approximateAgeNoBirthday;
    }

    public void setApproximateAgeNoBirthday(Integer approximateAgeNoBirthday) {
        this.approximateAgeNoBirthday = approximateAgeNoBirthday;
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live;
    }

    public String getNextVer() {
        return nextVer;
    }

    public void setNextVer(String nextVer) {
        this.nextVer = nextVer;
    }

    public String getPreviousVer() {
        return previousVer;
    }

    public void setPreviousVer(String previousVer) {
        this.previousVer = previousVer;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean nameSearchSet() {

        boolean set = false;

        if (this.getFirstName() != null && this.getFirstName().trim().length() > 0
                && this.getLastName() != null && this.getLastName().trim().length() > 0
                && this.getBirthDate() != null) {
            set = true;
        }
        return set;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof People)) {
            return false;
        }
        People other = (People) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {

        return "nameLd:" + nameDistance + " - " + firstName + " " + lastName + " - bdayLd:" + bdayDistance + " - " + birthDate;
    }

    @Override
    public int compareTo(People o) {

        int d = o.getMatchScore();

        if (this.getMatchScore() > d) {
            return 1;
        } else if (this.getMatchScore() < d) {
            return -1;
        } else {
            return 0;
        }

    }
}
