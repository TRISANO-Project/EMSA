/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "disease_events")
@NamedQueries({
    @NamedQuery(name = "DiseaseEvents.findAll", query = "SELECT d FROM DiseaseEvents d"),
    @NamedQuery(name = "DiseaseEvents.findById", query = "SELECT d FROM DiseaseEvents d WHERE d.id = :id"),
    @NamedQuery(name = "DiseaseEvents.findByEventId", query = "SELECT d FROM DiseaseEvents d WHERE d.eventId = :eventId"),
    @NamedQuery(name = "DiseaseEvents.findByDiseaseId", query = "SELECT d FROM DiseaseEvents d WHERE d.diseaseId = :diseaseId"),
    @NamedQuery(name = "DiseaseEvents.findByHospitalizedId", query = "SELECT d FROM DiseaseEvents d WHERE d.hospitalizedId = :hospitalizedId"),
    @NamedQuery(name = "DiseaseEvents.findByDiedId", query = "SELECT d FROM DiseaseEvents d WHERE d.diedId = :diedId"),
    @NamedQuery(name = "DiseaseEvents.findByDiseaseOnsetDate", query = "SELECT d FROM DiseaseEvents d WHERE d.diseaseOnsetDate = :diseaseOnsetDate"),
    @NamedQuery(name = "DiseaseEvents.findByCreatedAt", query = "SELECT d FROM DiseaseEvents d WHERE d.createdAt = :createdAt"),
    @NamedQuery(name = "DiseaseEvents.findByUpdatedAt", query = "SELECT d FROM DiseaseEvents d WHERE d.updatedAt = :updatedAt")})

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "eventId",
    "diseaseId",
    "hospitalizedId",
    "diedId",
    "diseaseOnsetDate",
    "dateDiagnosed",
    "createdAt",
    "updatedAt"
})
@XmlRootElement(name = "disease_events")

    public class DiseaseEvents implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="addresses_id_seq")
    @SequenceGenerator(name="addresses_id_seq", sequenceName="addresses_id_seq", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "disease_id")
    @XmlElement(name = "disease_id", required = true)
    private Integer diseaseId;
    @Column(name = "hospitalized_id")
    @XmlElement(name = "hospitalized_id", required = true)
    private Integer hospitalizedId;
    @Column(name = "died_id")
    @XmlElement(name = "died_id", required = true)
    private Integer diedId;
    @Column(name = "disease_onset_date")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "disease_onset_date", required = true)
    private Date diseaseOnsetDate;
    @Column(name = "date_diagnosed")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "date_diagnosed", required = true)
    private Date dateDiagnosed;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;

    public DiseaseEvents() {
    }

    public DiseaseEvents(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getDiseaseId() {
        return diseaseId;
    }

    public void setDiseaseId(Integer diseaseId) {
        this.diseaseId = diseaseId;
    }

    public Integer getHospitalizedId() {
        return hospitalizedId;
    }

    public void setHospitalizedId(Integer hospitalizedId) {
        this.hospitalizedId = hospitalizedId;
    }

    public Integer getDiedId() {
        return diedId;
    }

    public void setDiedId(Integer diedId) {
        this.diedId = diedId;
    }

    public Date getDiseaseOnsetDate() {
        return diseaseOnsetDate;
    }

    public void setDiseaseOnsetDate(Date diseaseOnsetDate) {
        this.diseaseOnsetDate = diseaseOnsetDate;
    }

    public Date getDateDiagnosed() {
        return dateDiagnosed;
    }

    public void setDateDiagnosed(Date dateDiagnosed) {
        this.dateDiagnosed = dateDiagnosed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof DiseaseEvents)) {
            return false;
        }
        DiseaseEvents other = (DiseaseEvents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.DiseaseEvents[id=" + id + "]";
    }

}
