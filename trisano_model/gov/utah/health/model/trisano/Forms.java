/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@Entity
@Table(name = "forms")
@NamedQueries({
    @NamedQuery(name = "Forms.findByQuestionId", query =
    "SELECT f FROM Forms f WHERE f.id IN "
    + "(SELECT f.formId FROM FormElements f WHERE f.id IN "
    + "(SELECT q.formElementId FROM Questions q WHERE q.id = :questionId))"),
    @NamedQuery(name = "Forms.findByEventTypeAndDiseaseIdNoFormReferences", query = "SELECT f FROM Forms f WHERE f.eventType = :eventType AND f.id IN "
    + "(SELECT df.diseasesFormsPK.formId FROM DiseasesForms df WHERE df.diseasesFormsPK.diseaseId = :diseaseId)"
    + " AND f.status = 'Live'"
    + " AND f.id NOT IN (SELECT fr.formId FROM FormReferences fr WHERE fr.outbreakId = :outbreakId)"),
    @NamedQuery(name = "Forms.findByEventTypeAndNoDiseaseIdAndNoFormReferences", query = "SELECT f FROM Forms f WHERE f.eventType = :eventType AND f.id NOT IN "
    + "(SELECT df.diseasesFormsPK.formId FROM DiseasesForms df WHERE df.diseasesFormsPK.formId = f.id)"
    + " AND f.status = 'Live'"
    + " AND f.id NOT IN (SELECT fr.formId FROM FormReferences fr WHERE fr.outbreakId = :outbreakId)"),
    @NamedQuery(name = "Forms.findAll", query = "SELECT f FROM Forms f"),
    @NamedQuery(name = "Forms.findAllLive", query = "SELECT f FROM Forms f WHERE f.status = 'Live'"),
    @NamedQuery(name = "Forms.findById", query = "SELECT f FROM Forms f WHERE f.id = :id"),
    @NamedQuery(name = "Forms.findByName", query = "SELECT f FROM Forms f WHERE f.name = :name"),
    @NamedQuery(name = "Forms.findByDescription", query = "SELECT f FROM Forms f WHERE f.description = :description"),
    @NamedQuery(name = "Forms.findByJurisdictionId", query = "SELECT f FROM Forms f WHERE f.jurisdictionId = :jurisdictionId"),
    @NamedQuery(name = "Forms.findByCreatedAt", query = "SELECT f FROM Forms f WHERE f.createdAt = :createdAt"),
    @NamedQuery(name = "Forms.findByUpdatedAt", query = "SELECT f FROM Forms f WHERE f.updatedAt = :updatedAt"),
    @NamedQuery(name = "Forms.findByIsTemplate", query = "SELECT f FROM Forms f WHERE f.isTemplate = :isTemplate"),
    @NamedQuery(name = "Forms.findByTemplateId", query = "SELECT f FROM Forms f WHERE f.templateId = :templateId"),
    @NamedQuery(name = "Forms.findByVersion", query = "SELECT f FROM Forms f WHERE f.version = :version"),
    @NamedQuery(name = "Forms.findByStatus", query = "SELECT f FROM Forms f WHERE f.status = :status"),
    @NamedQuery(name = "Forms.findByRolledBackFromId", query = "SELECT f FROM Forms f WHERE f.rolledBackFromId = :rolledBackFromId"),
    @NamedQuery(name = "Forms.findByEventType", query = "SELECT f FROM Forms f WHERE f.eventType = :eventType"),
    @NamedQuery(name = "Forms.findByShortName", query = "SELECT f FROM Forms f WHERE f.shortName = :shortName"),
    @NamedQuery(name = "Forms.findByDiseaseIds", query = "SELECT f FROM Forms f, DiseasesForms df WHERE f.id = df.diseasesFormsPK.formId AND df.diseasesFormsPK.diseaseId IN (:diseaseIds) AND f.status = 'Live'"),
    @NamedQuery(name = "Forms.findByShortNameLive", query = "SELECT f FROM Forms f WHERE lower(f.shortName) = lower(:shortName) AND f.status = 'Live'")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "name",
    "description",
    "jurisdictionId",
    "createdAt",
    "updatedAt",
    "isTemplate",
    "templateId",
    "version",
    "status",
    "rolledBackFromId",
    "eventType",
    "shortName"})
@XmlRootElement(name = "forms")
public class Forms implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "jurisdiction_id")
    private Integer jurisdictionId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "is_template")
    private Boolean isTemplate;
    @Column(name = "template_id")
    private Integer templateId;
    @Column(name = "version")
    private Integer version;
    @Column(name = "status")
    private String status;
    @Column(name = "rolled_back_from_id")
    private Integer rolledBackFromId;
    @Column(name = "event_type")
    private String eventType;
    @Column(name = "short_name")
    @XmlElement(name = "short_name")    
    private String shortName;

    public Forms() {
    }

    public Forms(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsTemplate() {
        return isTemplate;
    }

    public void setIsTemplate(Boolean isTemplate) {
        this.isTemplate = isTemplate;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRolledBackFromId() {
        return rolledBackFromId;
    }

    public void setRolledBackFromId(Integer rolledBackFromId) {
        this.rolledBackFromId = rolledBackFromId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Forms)) {
            return false;
        }
        Forms other = (Forms) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Forms[ id=" + id + " ]";
    }
}
