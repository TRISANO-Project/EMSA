package gov.utah.health.model.trisano;

import gov.utah.health.model.trisano.Diseases;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "start",
    "end",
    "diseases",
    "inCare",
    "notInCare",
    "average",
    "median",
    "rangeSummary",
    "getData",
    "careData"


})
@XmlRootElement(name = "disease_report")
public class CareTimeReport {

    @XmlElement(name = "start", required = true)
    private Date start;
    @XmlElement(name = "end", required = true)
    private Date end;
    @XmlElement(name = "in_care", required = true)
    private Integer inCare;
    @XmlElement(name = "not_in_care", required = true)
    private Integer notInCare;
    @XmlElement(name = "average", required = true)
    private Double average;
    @XmlElement(name = "median", required = true)
    private Integer median;
    @XmlElement(name = "care_data", required = true)
    private List<CareData> careData;
    @XmlElement(name = "diseases", required = true)
    private List<Diseases> diseases;
    @XmlElement(name = "range_summary", required = true)
    private List<RangeSummary> rangeSummary;
    @XmlElement(name = "get_data", required = true)
    private Boolean getData;

    public List<Diseases> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<Diseases> diseases) {
        this.diseases = diseases;
    }

    public Boolean getGetData() {
        return getData;
    }

    public void setGetData(Boolean getData) {
        this.getData = getData;
    }

    public Integer getInCare() {
        return inCare;
    }

    public void setInCare(Integer inCare) {
        this.inCare = inCare;
    }

    public Date getEnd() {
        return end;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public Integer getMedian() {
        return median;
    }

    public void setMedian(Integer median) {
        this.median = median;
    }

    public Integer getNotInCare() {
        return notInCare;
    }

    public void setNotInCare(Integer notInCare) {
        this.notInCare = notInCare;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public List<CareData> getCareData() {
        return careData;
    }

    public void setCareData(List<CareData> careData) {
        this.careData = careData;
    }

    public void addCareData(List<CareData> careData) {

        if (this.careData == null) {
            this.careData = new ArrayList<CareData>();
        }
        this.careData.addAll(careData);
    }

    public List<RangeSummary> getRangeSummary() {
        return rangeSummary;
    }

    public void setRangeSummary(List<RangeSummary> rangeSummary) {
        this.rangeSummary = rangeSummary;
    }

    /****
     * Calculates range summary values for the provided rangeSummary elements.
     * Performs other report calculations such as totals, averages, medians etc
     */
    public void prepareReport() {
        this.calculateRanges();
        // this.average
        //this.median
    }

    public void calculateRanges() {

        Map<Integer, CareData> earlyDates = new HashMap<Integer, CareData>();

        for (CareData cd : this.careData) {
            if (cd.getInCare()) {
                if (earlyDates.containsKey(cd.getEventId())
                        && earlyDates.get(cd.getEventId()).getDateCollected().after(cd.getDateCollected())) {
                    earlyDates.put(cd.getEventId(), cd);
                } else if (!earlyDates.containsKey(cd.getEventId())) {
                    earlyDates.put(cd.getEventId(), cd);
                }
            }
        }
        Integer totalCareDays = 0;
        for (Integer i : earlyDates.keySet()) {
            totalCareDays = totalCareDays + earlyDates.get(i).getDaysToCare();
        }
        this.inCare = earlyDates.size();
        this.average = 0.00;
        if (totalCareDays > 0 && this.inCare > 0) {
            this.average = (double) totalCareDays / (double) this.inCare;
        }

        if (this.rangeSummary != null && !this.rangeSummary.isEmpty()
                && this.getCareData() != null && !this.getCareData().isEmpty()) {

            List<RangeSummary> rsList = this.rangeSummary;
            for (int i = 0; i < rsList.size(); i++) {

                RangeSummary rs = rsList.get(i);
                rs.setCount(0);
                rs.setAverage(0);
                if (rs.getBegin() != null && rs.getBegin() >= 0 && rs.getEnd() != null) {

                    for (Integer edKey : earlyDates.keySet()) {
                        CareData cd = earlyDates.get(edKey);
                        if (cd.getDaysToCare() >= rs.getBegin() && cd.getDaysToCare() < rs.getEnd()) {

                            if (rs.getCount() == null || rs.getCount() <= 0) {
                                rs.setCount(new Integer(1));
                            } else {
                                rs.setCount(rs.getCount() + 1);
                            }
                        }
                    }

                } else if (rs.getBegin() != null && rs.getBegin() >= 0 && rs.getEnd() == null) {
                    for (Integer edKey : earlyDates.keySet()) {
                        CareData cd = earlyDates.get(edKey);
                        if (cd.getDaysToCare() >= rs.getBegin()) {
                            if (rs.getCount() == null || rs.getCount() <= 0) {
                                rs.setCount(new Integer(1));
                            } else {
                                rs.setCount(rs.getCount() + 1);
                            }
                        }
                    }
                }

            }

        }


    }
}
