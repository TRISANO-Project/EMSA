package gov.utah.health.model.trisano;

import gov.utah.health.model.trisano.Diseases;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "disease",
    "count"
})
@XmlRootElement(name = "disease_count")
public class DiseaseCount {

    @XmlElement(name = "disease", required = true)
    private Diseases disease;
    @XmlElement(name = "count", required = true)
    private String count;

    public Diseases getDisease() {
        return disease;
    }

    public void setDisease(Diseases disease) {
        this.disease = disease;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

}
