package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@Entity
@Table(name = "users")
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id"),
    @NamedQuery(name = "Users.findByUid", query = "SELECT u FROM Users u WHERE u.uid = :uid"),
    @NamedQuery(name = "Users.findByGivenName", query = "SELECT u FROM Users u WHERE u.givenName = :givenName"),
    @NamedQuery(name = "Users.findByFirstName", query = "SELECT u FROM Users u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "Users.findByLastName", query = "SELECT u FROM Users u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "Users.findByInitials", query = "SELECT u FROM Users u WHERE u.initials = :initials"),
    @NamedQuery(name = "Users.findByGenerationalQualifer", query = "SELECT u FROM Users u WHERE u.generationalQualifer = :generationalQualifer"),
    @NamedQuery(name = "Users.findByUserName", query = "SELECT u FROM Users u WHERE u.userName = :userName"),
    @NamedQuery(name = "Users.findByCreatedAt", query = "SELECT u FROM Users u WHERE u.createdAt = :createdAt"),
    @NamedQuery(name = "Users.findByUpdatedAt", query = "SELECT u FROM Users u WHERE u.updatedAt = :updatedAt"),
    @NamedQuery(name = "Users.findByEventViewSettings", query = "SELECT u FROM Users u WHERE u.eventViewSettings = :eventViewSettings"),
    @NamedQuery(name = "Users.findByShortcutSettings", query = "SELECT u FROM Users u WHERE u.shortcutSettings = :shortcutSettings"),
    @NamedQuery(name = "Users.findByTaskViewSettings", query = "SELECT u FROM Users u WHERE u.taskViewSettings = :taskViewSettings"),
    @NamedQuery(name = "Users.findByStatus", query = "SELECT u FROM Users u WHERE u.status = :status"),
    @NamedQuery(name = "Users.findByJurisdictionIdsRoleName", query = "SELECT u FROM Users u WHERE u.id IN (SELECT userId FROM RoleMemberships rm WHERE rm.jurisdictionId IN (:jurisdictionIds) AND rm.roleId IN (SELECT r.id FROM Roles r WHERE lower(r.roleName) = lower(:roleName))) ORDER BY u.lastName ASC")})

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "uid",
    "givenName",
    "firstName",
    "lastName",
    "initials",
    "generationalQualifer",
    "userName",
    "createdAt",
    "updatedAt",
    "eventViewSettings",
    "shortcutSettings",
    "taskViewSettings",
    "status"
})
@XmlRootElement(name = "users")
public class Users implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "uid")
    @XmlElement(name = "uid", required = true)
    private String uid;
    @Column(name = "given_name")
    private String givenName;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "initials")
    private String initials;
    @Column(name = "generational_qualifer")
    private String generationalQualifer;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "event_view_settings")
    private String eventViewSettings;
    @Column(name = "shortcut_settings")
    private String shortcutSettings;
    @Column(name = "task_view_settings")
    private String taskViewSettings;
    @Column(name = "status")
    private String status;

    public Users() {
    }

    public Users(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getGenerationalQualifer() {
        return generationalQualifer;
    }

    public void setGenerationalQualifer(String generationalQualifer) {
        this.generationalQualifer = generationalQualifer;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEventViewSettings() {
        return eventViewSettings;
    }

    public void setEventViewSettings(String eventViewSettings) {
        this.eventViewSettings = eventViewSettings;
    }

    public String getShortcutSettings() {
        return shortcutSettings;
    }

    public void setShortcutSettings(String shortcutSettings) {
        this.shortcutSettings = shortcutSettings;
    }

    public String getTaskViewSettings() {
        return taskViewSettings;
    }

    public void setTaskViewSettings(String taskViewSettings) {
        this.taskViewSettings = taskViewSettings;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Users[ id=" + id + " ]";
    }

}
