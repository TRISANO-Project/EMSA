package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "events")
@NamedQueries({
    @NamedQuery(name = "Events.maxRecordNumber", query = "SELECT max(e.recordNumber) FROM Events e"),
    @NamedQuery(name = "Events.findAll", query = "SELECT e FROM Events e"),
    @NamedQuery(name = "Events.findByIds", query = "SELECT e FROM Events e WHERE e.id IN (:ids)"),
    @NamedQuery(name = "Events.findByEntityId", query = "SELECT e FROM Events e WHERE e.deletedAt is null AND e.id IN (SELECT DISTINCT eventId FROM Participations p WHERE p.primaryEntityId = :entityId)"),
    @NamedQuery(name = "Events.findByEntityIdMorbidityAssessmentContact", query = "SELECT e FROM Events e "
        + "WHERE e.type IN('MorbidityEvent', 'AssessmentEvent', 'ContactEvent') "
        + "AND e.deletedAt is null "
        + "AND e.id IN (SELECT DISTINCT eventId FROM Participations p WHERE p.primaryEntityId = :entityId)"),
    @NamedQuery(name = "Events.findByEntityIdMorbidityAssessmentContactDiseases", query = "SELECT e FROM Events e "
        + "WHERE e.type IN('MorbidityEvent', 'AssessmentEvent', 'ContactEvent') "
        + "AND e.deletedAt is null "
        + "AND e.id IN (SELECT DISTINCT eventId FROM Participations p WHERE p.primaryEntityId = :entityId) "
        + "AND e.id IN (SELECT DISTINCT eventId FROM DiseaseEvents de "
                     + "WHERE de.diseaseId IN(SELECT id FROM Diseases d WHERE d.diseaseName IN (:diseaseNames) ))"),
    @NamedQuery(name = "Events.getCdcDataSinceLastExport", query = "SELECT e FROM Events e, DiseaseEvents de WHERE e.id = de.eventId AND e.type = :eventType AND e.updatedAt > :lastExport"),
    @NamedQuery(name = "Events.getCdcDataInRange", query = "SELECT e FROM Events e, DiseaseEvents de WHERE e.id = de.eventId AND e.type = :eventType AND ((e.mMWRweek >= :startWeek AND e.mMWRyear >= :startYear) AND (e.mMWRweek <= :endWeek AND e.mMWRyear <= :endYear))"),
    @NamedQuery(name = "Events.findById", query = "SELECT e FROM Events e WHERE e.id = :id"),
    @NamedQuery(name = "Events.findIdByRecordNumber", query = "SELECT e.id FROM Events e WHERE e.recordNumber = :recordNumber AND " +
                "e.deletedAt IS NULL AND (e.type = 'AssessmentEvent' OR e.type = 'MorbidityEvent' OR e.type = 'ContactEvent')"),
    @NamedQuery(name = "Events.findByOutbreakId", query = "SELECT e FROM Events e WHERE e.outbreakId = :outbreakId"),
    @NamedQuery(name = "Events.findByPersonEntityId", query = "SELECT e FROM Events e, Participations p WHERE e.id = p.eventId AND p.type = 'InterestedParty' AND p.primaryEntityId = :personEntityId")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "importedFromId",
    "stateCaseStatusId",
    "eventName",
    "eventOnsetDate",
    "createdAt",
    "updatedAt",
    "outbreakAssociatedId",
    "outbreakName",
    "investigationLHDstatusid",
    "investigationStartedDate",
    "investigationCompletedLHDdate",
    "reviewCompletedByStateDate",
    "firstReportedPHdate",
    "resultsReportedToClinicianDate",
    "recordNumber",
    "mMWRweek",
    "mMWRyear",
    "lhdCaseStatusId",
    "type",
    "eventQueueId",
    "sentToCdc",
    "ageAtOnset",
    "ageTypeId",
    "investigatorId",
    "sentToIbis",
    "acuity",
    "otherData1",
    "otherData2",
    "deletedAt",
    "parentId",
    "cdcUpdatedAt",
    "ibisUpdatedAt",
    "parentGuardian",
    "workflowState",
    "participationsContactId",
    "participationsPlaceId",
    "participationsEncounters",
    "undergoneFormAssignment",
    "outbreakDate",
    "outbreakDescription",
    "outbreakEventId",
    "stateManagerId",
    "outbreakId"
        
})
@XmlRootElement(name = "events")
public class Events implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "events_id_seq")
    @SequenceGenerator(name = "events_id_seq", sequenceName = "events_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "imported_from_id")
    @XmlElement(name = "imported_from_id", required = true)
    private Integer importedFromId;
    @Column(name = "state_case_status_id")
    @XmlElement(name = "state_case_status_id", required = true)
    private Integer stateCaseStatusId;
    @Column(name = "event_name")
    @XmlElement(name = "event_name", required = true)
    private String eventName;
    @Column(name = "event_onset_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "event_onset_date", required = true)
    private Date eventOnsetDate;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "outbreak_associated_id")
    @XmlElement(name = "outbreak_associated_id", required = true)
    private Integer outbreakAssociatedId;
    @Column(name = "outbreak_name")
    @XmlElement(name = "outbreak_name", required = true)
    private String outbreakName;
    @Column(name = "`investigation_LHD_status_id`")
    @XmlElement(name = "investigation_LHD_status_id", required = true)
    private Integer investigationLHDstatusid;
    @Column(name = "investigation_started_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "investigation_started_date", required = true)
    private Date investigationStartedDate;
    @Column(name = "`investigation_completed_LHD_date`")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "investigation_completed_LHD_date", required = true)
    private Date investigationCompletedLHDdate;
    @Column(name = "review_completed_by_state_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "review_completed_by_state_date", required = true)
    private Date reviewCompletedByStateDate;
    @Column(name = "`first_reported_PH_date`")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "first_reported_PH_date", required = true)
    private Date firstReportedPHdate;
    @Column(name = "results_reported_to_clinician_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "results_reported_to_clinician_date", required = true)
    private Date resultsReportedToClinicianDate;
    @Column(name = "record_number")
    @XmlElement(name = "record_number", required = true)
    private String recordNumber;
    @Column(name = "`MMWR_week`")
    @XmlElement(name = "MMWR_week", required = true)
    private Integer mMWRweek;
    @Column(name = "`MMWR_year`")
    @XmlElement(name = "MMWR_year", required = true)
    private Integer mMWRyear;
    @Column(name = "lhd_case_status_id")
    @XmlElement(name = "lhd_case_status_id", required = true)
    private Integer lhdCaseStatusId;
    @Column(name = "type")
    @XmlElement(name = "type", required = true)
    private String type;
    @Column(name = "event_queue_id")
    @XmlElement(name = "event_queue_id", required = true)
    private Integer eventQueueId;
    @Column(name = "sent_to_cdc")
    @XmlElement(name = "sent_to_cdc", required = true)
    private Boolean sentToCdc;
    @Column(name = "age_at_onset")
    @XmlElement(name = "age_at_onset", required = true)
    private Integer ageAtOnset;
    @Column(name = "age_type_id")
    @XmlElement(name = "age_type_id", required = true)
    private Integer ageTypeId;
    @Column(name = "investigator_id")
    @XmlElement(name = "investigator_id", required = true)
    private Integer investigatorId;
    @Column(name = "sent_to_ibis")
    @XmlElement(name = "sent_to_ibis", required = true)
    private Boolean sentToIbis;
    @Column(name = "acuity")
    @XmlElement(name = "acuity", required = true)
    private Integer acuity;
    @Column(name = "other_data_1")
    @XmlElement(name = "other_data_1", required = true)
    private String otherData1;
    @Column(name = "other_data_2")
    @XmlElement(name = "other_data_2", required = true)
    private String otherData2;
    @Column(name = "deleted_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "deleted_at", required = true)
    private Date deletedAt;
    @Column(name = "parent_id")
    @XmlElement(name = "parent_id", required = true)
    private Integer parentId;
    @Column(name = "cdc_updated_at")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "cdc_updated_at", required = true)
    private Date cdcUpdatedAt;
    @Column(name = "ibis_updated_at")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "ibis_updated_at", required = true)
    private Date ibisUpdatedAt;
    @Column(name = "parent_guardian")
    @XmlElement(name = "parent_guardian", required = true)
    private String parentGuardian;
    @Column(name = "workflow_state")
    @XmlElement(name = "workflow_state", required = true)
    private String workflowState;
    @Column(name = "participations_contact_id")
    @XmlElement(name = "participations_contact_id", required = true)
    private Integer participationsContactId;
    @Column(name = "participations_place_id")
    @XmlElement(name = "participations_place_id", required = true)
    private Integer participationsPlaceId;
    @Column(name = "undergone_form_assignment")
    @XmlElement(name = "undergone_form_assignment", required = true)
    private Boolean undergoneFormAssignment;
    @Column(name = "outbreak_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "outbreak_date", required = true)
    private Date outbreakDate;
    @Column(name = "outbreak_description")
    @XmlElement(name = "outbreak_description", required = true)
    private String outbreakDescription;
    @Column(name = "outbreak_event_id")
    @XmlElement(name = "outbreak_event_id", required = true)
    private Integer outbreakEventId;
    @Column(name = "outbreak_id")
    @XmlElement(name = "outbreak_id", required = true)
    private Integer outbreakId;
    @Column(name = "state_manager_id")
    @XmlElement(name = "state_manager_id", required = true)
    private Integer stateManagerId;
    @JoinColumn(name = "participations_encounter_id", referencedColumnName = "id")
    @ManyToOne
    @XmlElement(name = "participations_encounters", required = true)
    private ParticipationsEncounters participationsEncounters;

    public Events() {
    }

    public Events(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getImportedFromId() {
        return importedFromId;
    }

    public void setImportedFromId(Integer importedFromId) {
        this.importedFromId = importedFromId;
    }

    public Integer getStateCaseStatusId() {
        return stateCaseStatusId;
    }

    public void setStateCaseStatusId(Integer stateCaseStatusId) {
        this.stateCaseStatusId = stateCaseStatusId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getEventOnsetDate() {
        return eventOnsetDate;
    }

    public void setEventOnsetDate(Date eventOnsetDate) {
        this.eventOnsetDate = eventOnsetDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getOutbreakAssociatedId() {
        return outbreakAssociatedId;
    }

    public void setOutbreakAssociatedId(Integer outbreakAssociatedId) {
        this.outbreakAssociatedId = outbreakAssociatedId;
    }

    public String getOutbreakName() {
        return outbreakName;
    }

    public void setOutbreakName(String outbreakName) {
        this.outbreakName = outbreakName;
    }

    public Integer getInvestigationLHDstatusid() {
        return investigationLHDstatusid;
    }

    public void setInvestigationLHDstatusid(Integer investigationLHDstatusid) {
        this.investigationLHDstatusid = investigationLHDstatusid;
    }

    public Date getInvestigationStartedDate() {
        return investigationStartedDate;
    }

    public void setInvestigationStartedDate(Date investigationStartedDate) {
        this.investigationStartedDate = investigationStartedDate;
    }

    public Date getInvestigationCompletedLHDdate() {
        return investigationCompletedLHDdate;
    }

    public void setInvestigationCompletedLHDdate(Date investigationCompletedLHDdate) {
        this.investigationCompletedLHDdate = investigationCompletedLHDdate;
    }

    public Date getReviewCompletedByStateDate() {
        return reviewCompletedByStateDate;
    }

    public void setReviewCompletedByStateDate(Date reviewCompletedByStateDate) {
        this.reviewCompletedByStateDate = reviewCompletedByStateDate;
    }

    public Date getFirstReportedPHdate() {
        return firstReportedPHdate;
    }

    public void setFirstReportedPHdate(Date firstreportedPHdate) {
        this.firstReportedPHdate = firstreportedPHdate;
    }

    public Date getResultsReportedToClinicianDate() {
        return resultsReportedToClinicianDate;
    }

    public void setResultsReportedToClinicianDate(Date resultsReportedToClinicianDate) {
        this.resultsReportedToClinicianDate = resultsReportedToClinicianDate;
    }

    public String getRecordNumber() {
        return recordNumber;
    }

    public void setRecordNumber(String recordNumber) {
        this.recordNumber = recordNumber;
    }

    public Integer getMMWRweek() {
        return mMWRweek;
    }

    public void setMMWRweek(Integer mMWRweek) {
        this.mMWRweek = mMWRweek;
    }

    public Integer getMMWRyear() {
        return mMWRyear;
    }

    public void setMMWRyear(Integer mMWRyear) {
        this.mMWRyear = mMWRyear;
    }

    public Integer getLhdCaseStatusId() {
        return lhdCaseStatusId;
    }

    public void setLhdCaseStatusId(Integer lhdCaseStatusId) {
        this.lhdCaseStatusId = lhdCaseStatusId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getEventQueueId() {
        return eventQueueId;
    }

    public void setEventQueueId(Integer eventQueueId) {
        this.eventQueueId = eventQueueId;
    }

    public Boolean getSentToCdc() {
        return sentToCdc;
    }

    public void setSentToCdc(Boolean sentToCdc) {
        this.sentToCdc = sentToCdc;
    }

    public Integer getAgeAtOnset() {
        return ageAtOnset;
    }

    public void setAgeAtOnset(Integer ageAtOnset) {
        this.ageAtOnset = ageAtOnset;
    }

    public Integer getAgeTypeId() {
        return ageTypeId;
    }

    public void setAgeTypeId(Integer ageTypeId) {
        this.ageTypeId = ageTypeId;
    }

    public Integer getInvestigatorId() {
        return investigatorId;
    }

    public void setInvestigatorId(Integer investigatorId) {
        this.investigatorId = investigatorId;
    }

    public Boolean getSentToIbis() {
        return sentToIbis;
    }

    public void setSentToIbis(Boolean sentToIbis) {
        this.sentToIbis = sentToIbis;
    }

    public Integer getAcuity() {
        return acuity;
    }

    public void setAcuity(Integer acuity) {
        this.acuity = acuity;
    }

    public String getOtherData1() {
        return otherData1;
    }

    public void setOtherData1(String otherData1) {
        this.otherData1 = otherData1;
    }

    public String getOtherData2() {
        return otherData2;
    }

    public void setOtherData2(String otherData2) {
        this.otherData2 = otherData2;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Date getCdcUpdatedAt() {
        return cdcUpdatedAt;
    }

    public void setCdcUpdatedAt(Date cdcUpdatedAt) {
        this.cdcUpdatedAt = cdcUpdatedAt;
    }

    public Date getIbisUpdatedAt() {
        return ibisUpdatedAt;
    }

    public void setIbisUpdatedAt(Date ibisUpdatedAt) {
        this.ibisUpdatedAt = ibisUpdatedAt;
    }

    public String getParentGuardian() {
        return parentGuardian;
    }

    public void setParentGuardian(String parentGuardian) {
        this.parentGuardian = parentGuardian;
    }

    public String getWorkflowState() {
        return workflowState;
    }

    public void setWorkflowState(String workflowState) {
        this.workflowState = workflowState;
    }

    public Integer getParticipationsContactId() {
        return participationsContactId;
    }

    public void setParticipationsContactId(Integer participationsContactId) {
        this.participationsContactId = participationsContactId;
    }

    public Integer getParticipationsPlaceId() {
        return participationsPlaceId;
    }

    public void setParticipationsPlaceId(Integer participationsPlaceId) {
        this.participationsPlaceId = participationsPlaceId;
    }

    public Boolean getUndergoneFormAssignment() {
        return undergoneFormAssignment;
    }

    public void setUndergoneFormAssignment(Boolean undergoneFormAssignment) {
        this.undergoneFormAssignment = undergoneFormAssignment;
    }

    public Date getOutbreakDate() {
        return outbreakDate;
    }

    public void setOutbreakDate(Date outbreakDate) {
        this.outbreakDate = outbreakDate;
    }

    public String getOutbreakDescription() {
        return outbreakDescription;
    }

    public void setOutbreakDescription(String outbreakDescription) {
        this.outbreakDescription = outbreakDescription;
    }

    public Integer getOutbreakEventId() {
        return outbreakEventId;
    }

    public void setOutbreakEventId(Integer outbreakEventId) {
        this.outbreakEventId = outbreakEventId;
    }

    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public Integer getStateManagerId() {
        return stateManagerId;
    }

    public void setStateManagerId(Integer stateManagerId) {
        this.stateManagerId = stateManagerId;
    }

    public ParticipationsEncounters getParticipationsEncounters() {
        return participationsEncounters;
    }

    public void setParticipationsEncounters(ParticipationsEncounters participationsEncounters) {
        this.participationsEncounters = participationsEncounters;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Events)) {
            return false;
        }
        Events other = (Events) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.Events[id=" + id + "]";
    }
}
