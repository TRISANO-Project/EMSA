package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "person",
    "events"
})
@XmlRootElement(name = "person_events_attributes")
public class PersonEventsAttributes {

    @XmlElement(name = "person", required = true)
    private InterestedPartyAttributes person;
    @XmlElement(name = "events", required = true)
    private List<EventAttributes> events;

    public InterestedPartyAttributes getPerson() {
        return person;
    }

    public void setPerson(InterestedPartyAttributes person) {
        this.person = person;
    }

    public List<EventAttributes> getEvents() {
        return events;
    }

    public void setEvents(List<EventAttributes> events) {
        this.events = events;
    }
    
    public void addEvent(EventAttributes event) {
        if(this.events == null){
            this.events = new ArrayList<EventAttributes>();
        }
        events.add(event);
    }
    
}
