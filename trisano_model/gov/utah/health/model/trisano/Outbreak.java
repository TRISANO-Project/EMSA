package gov.utah.health.model.trisano;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@Entity
@Table(name = "outbreaks")
@NamedQueries({
    @NamedQuery(name = "Outbreak.maxRecordNumber", query = "SELECT max(o.id) FROM Outbreak o"),
    @NamedQuery(name = "Outbreak.findAll", query = "SELECT o FROM Outbreak o"),
    @NamedQuery(name = "Outbreak.findById", query = "SELECT o FROM Outbreak o WHERE o.id = :id"),
    @NamedQuery(name = "Outbreak.findByParentId", query = "SELECT o FROM Outbreak o WHERE o.parentId =:parentId"),
    @NamedQuery(name = "Outbreak.findByDiseaseIdLocationTypeNullParentId", query = "SELECT o FROM Outbreak o WHERE o.diseaseId = :diseaseId and o.locationType = :locationType and o.parentId is null"),
    @NamedQuery(name = "Outbreak.findByLocationTypeNullParentId", query = "SELECT o FROM Outbreak o WHERE o.locationType = :locationType and o.parentId is null order by o.outbreakDate desc"),
    @NamedQuery(name = "Outbreak.findByNumber", query = "SELECT o FROM Outbreak o WHERE o.number =:number"),
    @NamedQuery(name = "Outbreak.findByReportApproved", query = "SELECT o FROM Outbreak o WHERE o.number =:number"),
    @NamedQuery(name = "Outbreak.findByReportRequired", query = "SELECT o FROM Outbreak o WHERE o.number =:number"),
    @NamedQuery(name = "Outbreak.findByLinkToEpiTracker", query = "SELECT o FROM Outbreak o WHERE o.linkToEpiTracker IS TRUE"),
    @NamedQuery(name = "Outbreak.findByName", query = "SELECT o FROM Outbreak o WHERE o.name =:name")
})
/**
 * @author UDOH
 */
public class Outbreak {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "outbreaks_id")
    @SequenceGenerator(name = "outbreaks_id", sequenceName = "outbreaks_id", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "created_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "created_user_id")
    private Integer createdUserId;
    @Column(name = "updated_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "parent_id")
    private Integer parentId;
    @Column(name = "investigator_id")
    private Integer investigatorId;
    @Column(name = "location_type")
    private Integer locationType;
    @Column(name = "number")
    private String number;
    @Column(name = "report_required")
    private Boolean reportRequired;
    @Column(name = "report_approved")
    private Boolean reportApproved;
    @Column(name = "report_reason")
    private String reportReason;
    @Column(name = "report_reason_other")
    private String reportReasonOther;
    @Column(name = "report_approved_user_id")
    private Integer reportApprovedUserId;
    @Column(name = "report_approved_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date reportApprovedDate;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "outbreak_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date outbreakDate;
    @Column(name = "disease_id")
    private Integer diseaseId;
    @Column(name = "jurisdiction_id")
    private Integer jurisdictionId;
    @Column(name = "status")
    private Integer status;
    @Column(name = "link_to_epi_tracker")
    private Boolean linkToEpiTracker;
    @Column(name = "epi_tracker_name")
    private String epiTrackerName;
    @Transient
    private Diseases disease;
    @Transient
    private Places jurisdiction;
    @Transient
    private Outbreak parent;
    @Transient
    private List<Outbreak> children;
    @Transient
    private List<Outbreak> siblings;
    @Transient
    OutbreakSummary eventSummary;
    @Transient
    private List<OutbreakReportSection> reportSections;
    @Transient
    private Users reportApprovedUser;
    @Transient
    private List<OutbreakAccess> outbreakAccess;
    @Transient
    private UserAccess userAccess;
    @Transient
    private Boolean canEdit;
    @Transient
    private Boolean viewOnly;
    @Transient
    private Users creator;
    @Transient
    private List<AuditUser> audits;

    public Outbreak() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AuditUser> getAudits() {
        return audits;
    }

    public void setAudits(List<AuditUser> audits) {
        this.audits = audits;
    }
    public void addAudit(AuditUser audit) {
        if(this.audits == null){
            this.audits = new ArrayList<AuditUser>();
        }
        this.audits.add(0,audit);
    }

    public UserAccess getUserAccess() {
        return userAccess;
    }

    public void setUserAccess(UserAccess userAccess) {
        this.userAccess = userAccess;
    }

    public List<OutbreakAccess> getOutbreakAccess() {
        return outbreakAccess;
    }

    public void setOutbreakAccess(List<OutbreakAccess> outbreakAccess) {
        this.outbreakAccess = outbreakAccess;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Users getCreator() {
        return creator;
    }

    public void setCreator(Users creator) {
        this.creator = creator;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Outbreak getParent() {
        return parent;
    }

    public void setParent(Outbreak parent) {
        this.parent = parent;
    }

    public List<Outbreak> getChildren() {
        return children;
    }

    public void setChildren(List<Outbreak> children) {
        this.children = children;
    }

    public List<Outbreak> getSiblings() {
        return siblings;
    }

    public void setSiblings(List<Outbreak> siblings) {
        this.siblings = siblings;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getLocationType() {
        return locationType;
    }

    public void setLocationType(Integer locationType) {
        this.locationType = locationType;
    }

    public String getLocationTypeLabel() {
        String label = null;
        if (this.locationType != null) {
            label = OutbreakType.getLocationType(this.locationType);
        }
        return label;
    }

    public Boolean getLinkToEpiTracker() {
        return linkToEpiTracker;
    }

    public void setLinkToEpiTracker(Boolean linkToEpiTracker) {
        this.linkToEpiTracker = linkToEpiTracker;
    }

    public String getEpiTrackerName() {
        return epiTrackerName;
    }

    public void setEpiTrackerName(String epiTrackerName) {
        this.epiTrackerName = epiTrackerName;
    }

    public Integer getInvestigatorId() {
        return investigatorId;
    }

    public void setInvestigatorId(Integer investigatorId) {
        this.investigatorId = investigatorId;
    }

    public Boolean getReportRequired() {
        return reportRequired;
    }

    public void setReportRequired(Boolean reportRequired) {
        this.reportRequired = reportRequired;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public String getReportReasonOther() {
        return reportReasonOther;
    }

    public void setReportReasonOther(String reportReasonOther) {
        this.reportReasonOther = reportReasonOther;
    }

    public Boolean getReportApproved() {
        return reportApproved;
    }

    public void setReportApproved(Boolean reportApproved) {
        this.reportApproved = reportApproved;
    }

    public Integer getReportApprovedUserId() {
        return reportApprovedUserId;
    }

    public void setReportApprovedUserId(Integer reportApprovedUserId) {
        this.reportApprovedUserId = reportApprovedUserId;
    }

    public Date getReportApprovedDate() {
        return reportApprovedDate;
    }

    public void setReportApprovedDate(Date reportApprovedDate) {
        this.reportApprovedDate = reportApprovedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getOutbreakDate() {
        return outbreakDate;
    }

    public String getOutbreakDateFormatted() {
        String date = null;
        if (this.outbreakDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            date = formatter.format(this.outbreakDate);
        }
        return date;
    }

    public void setOutbreakDate(Date outbreakDate) {
        this.outbreakDate = outbreakDate;
    }

    public Integer getDiseaseId() {
        return diseaseId;
    }

    public void setDiseaseId(Integer diseaseId) {
        this.diseaseId = diseaseId;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusLabel() {
        return OutbreakStatus.getLabel(status);
    }

    public Diseases getDisease() {
        return disease;
    }

    public void setDisease(Diseases disease) {
        this.disease = disease;
    }

    public Places getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Places jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public OutbreakSummary getEventSummary() {
        return eventSummary;
    }

    public void setEventSummary(OutbreakSummary eventSummary) {
        this.eventSummary = eventSummary;
    }

    public List<OutbreakReportSection> getReportSections() {
        return reportSections;
    }

    public void setReportSections(List<OutbreakReportSection> reportSections) {
        this.reportSections = reportSections;
    }

    public Boolean getReportSectionsEmpty() {
        Boolean empty = true;

        if (reportSections != null) {

            for (OutbreakReportSection rs : reportSections) {
                if (rs.getId() != null && rs.getId() > 0) {
                    empty = false;
                }
            }
        }
        return empty;
    }

    public Boolean getStateOutbreak() {
        boolean state = false;
        if (this.locationType != null && this.locationType.equals(OutbreakType.LOCATION_TYPE_STATE.getId())) {
            state = true;
        }
        return state;
    }

    public Boolean getLocalOutbreak() {
        boolean state = false;
        if (this.locationType != null && this.locationType.equals(OutbreakType.LOCATION_TYPE_LOCAL.getId())) {
            state = true;
        }
        return state;
    }

    public Users getReportApprovedUser() {
        return reportApprovedUser;
    }

    public void setReportApprovedUser(Users reportApprovedUser) {
        this.reportApprovedUser = reportApprovedUser;
    }

    public Boolean getCanEdit() {

        //check that user has rights to lead jurisdiction
        if (this.canEdit == null && this.getJurisdictionId() != null
                && this.getJurisdictionId() > 0) {
            this.canEdit = this.getUserAccess().canEditJurisdiction(this.getJurisdictionId());
        }
        //check that user has been granted edit rights through outbreak_access
        if (this.canEdit == null || !this.canEdit) {
            if (this.getOutbreakAccess() != null) {
                for (OutbreakAccess oa : this.getOutbreakAccess()) {
                    if (oa.getType().equals(OutbreakType.ACCESS_TYPE_EDIT.getId())
                            && this.getUserAccess().hasJurisdiction(oa.getJurisdictionId())) {
                        this.canEdit = true;
                        break;
                    }
                }
            }
        }
        if (this.canEdit == null) {
            this.canEdit = false;
        }

        return this.canEdit;

    }

    public Boolean getViewOnly() {

        if (this.viewOnly == null) {
            if (!getCanEdit()) {
                if (this.getOutbreakAccess() == null) {
                    this.viewOnly = true;
                } else {
                    for (OutbreakAccess oa : this.getOutbreakAccess()) {
                        if (oa.getType().equals(OutbreakType.ACCESS_TYPE_VIEW.getId())
                                && this.getUserAccess().hasJurisdiction(oa.getJurisdictionId())) {
                            this.viewOnly = true;
                            break;
                        }
                    }

                }
            }
        }
        if (this.viewOnly == null) {
            this.viewOnly = false;
        }
        return this.viewOnly;
    }

    public Boolean getCanEditReport() {
        Boolean s = false;
        if (this.getCanEdit()) {
            if (!this.getReportApproved()) {
                s = true;
            }
        }
        return s;
    }

    public Boolean getCanEditLocationType() {
        Boolean can = true;
        if (this.getJurisdictionId() != null) {
            can = this.getUserAccess().getCanEditLocationType(this.getJurisdictionId());
        }
        return can;
    }
}
