/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "external_codes")
@NamedQueries({
    @NamedQuery(name = "ExternalCodes.findAll", query = "SELECT e FROM ExternalCodes e"),
    @NamedQuery(name = "ExternalCodes.findById", query = "SELECT e FROM ExternalCodes e WHERE e.id = :id"),
    // find for RE
    @NamedQuery(name = "ExternalCodes.findByCodeCodeNameCodeDescription", query = "SELECT e FROM ExternalCodes e WHERE lower(e.codeName) like lower(:codeName) AND (lower(e.theCode) like lower(:theCode) OR lower(e.codeDescription) like lower(:codeDescription))"),
    @NamedQuery(name = "ExternalCodes.findByCodeName", query = "SELECT e FROM ExternalCodes e WHERE lower(e.codeName) like lower(:codeName) ORDER BY e.sortOrder"),
    @NamedQuery(name = "ExternalCodes.findByTheCode", query = "SELECT e FROM ExternalCodes e WHERE lower(e.theCode) like lower(:theCode)"),
    @NamedQuery(name = "ExternalCodes.findByCodeDescription", query = "SELECT e FROM ExternalCodes e WHERE lower(e.codeDescription) like lower(:codeDescription)"),
    @NamedQuery(name = "ExternalCodes.findBySortOrder", query = "SELECT e FROM ExternalCodes e WHERE e.sortOrder = :sortOrder"),
    @NamedQuery(name = "ExternalCodes.findByNextVer", query = "SELECT e FROM ExternalCodes e WHERE e.nextVer = :nextVer"),
    @NamedQuery(name = "ExternalCodes.findByPreviousVer", query = "SELECT e FROM ExternalCodes e WHERE e.previousVer = :previousVer"),
    @NamedQuery(name = "ExternalCodes.findByLive", query = "SELECT e FROM ExternalCodes e WHERE e.live = :live"),
    @NamedQuery(name = "ExternalCodes.findByCreatedAt", query = "SELECT e FROM ExternalCodes e WHERE e.createdAt = :createdAt"),
    @NamedQuery(name = "ExternalCodes.findByUpdatedAt", query = "SELECT e FROM ExternalCodes e WHERE e.updatedAt = :updatedAt"),
    @NamedQuery(name = "ExternalCodes.findByJurisdictionId", query = "SELECT e FROM ExternalCodes e WHERE e.jurisdictionId = :jurisdictionId"),
    @NamedQuery(name = "ExternalCodes.findByDeletedAt", query = "SELECT e FROM ExternalCodes e WHERE e.deletedAt = :deletedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "codeName",
    "theCode",
    "codeDescription",
    "sortOrder",
    "nextVer",
    "previousVer",
    "live",
    "createdAt",
    "updatedAt",
    "jurisdictionId",
    "deletedAt",
    "diseaseSpecific"
})
@XmlRootElement(name = "external_codes")
public class ExternalCodes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "code_name")
    @XmlElement(name = "code_name", required = true)
    private String codeName;
    @Basic(optional = false)
    @Column(name = "the_code")
    @XmlElement(name = "the_code", required = true)
    private String theCode;
    @Column(name = "code_description")
    @XmlElement(name = "code_description", required = true)
    private String codeDescription;
    @Column(name = "sort_order")
    @XmlElement(name = "sort_order", required = true)
    private Integer sortOrder;
    @Column(name = "next_ver")
    @XmlElement(name = "next_ver", required = true)
    private Integer nextVer;
    @Column(name = "previous_ver")
    @XmlElement(name = "previous_ver", required = true)
    private Integer previousVer;
    @Column(name = "live")
    @XmlElement(name = "live", required = true)
    private Boolean live;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "jurisdiction_id")
    @XmlElement(name = "jurisdiction_id", required = true)
    private Integer jurisdictionId;
    @Column(name = "deleted_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "deleted_at", required = true)
    private Date deletedAt;
    @Column(name = "disease_specific")
    @XmlElement(name = "disease_specific", required = true)
    private Boolean diseaseSpecific;

    public ExternalCodes() {
    }

    public ExternalCodes(Integer id) {
        this.id = id;
    }

    public ExternalCodes(Integer id, String codeName, String theCode) {
        this.id = id;
        this.codeName = codeName;
        this.theCode = theCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getTheCode() {
        return theCode;
    }

    public void setTheCode(String theCode) {
        this.theCode = theCode;
    }

    public String getCodeDescription() {
        return codeDescription;
    }

    public void setCodeDescription(String codeDescription) {
        this.codeDescription = codeDescription;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getNextVer() {
        return nextVer;
    }

    public void setNextVer(Integer nextVer) {
        this.nextVer = nextVer;
    }

    public Integer getPreviousVer() {
        return previousVer;
    }

    public void setPreviousVer(Integer previousVer) {
        this.previousVer = previousVer;
    }

    public Boolean getLive() {
        return live;
    }

    public void setLive(Boolean live) {
        this.live = live;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Boolean getDiseaseSpecific() {
        return diseaseSpecific;
    }

    public void setDiseaseSpecific(Boolean diseaseSpecific) {
        this.diseaseSpecific = diseaseSpecific;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ExternalCodes)) {
            return false;
        }
        ExternalCodes other = (ExternalCodes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.ExternalCodes[id=" + id + "]";
    }
}
