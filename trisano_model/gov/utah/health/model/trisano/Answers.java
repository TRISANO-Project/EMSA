package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@Entity
@Table(name = "answers")
@NamedQueries({
    @NamedQuery(name = "Answers.findAll", query = "SELECT a FROM Answers a"),
    @NamedQuery(name = "Answers.findById", query = "SELECT a FROM Answers a WHERE a.id = :id"),
    @NamedQuery(name = "Answers.findByEventId", query = "SELECT a FROM Answers a WHERE a.eventId = :eventId"),
    @NamedQuery(name = "Answers.findByEventIdAndQuestionId", query = "SELECT a FROM Answers a WHERE a.eventId = :eventId AND a.questionId = :questionId"),
    @NamedQuery(name = "Answers.findByEventIdAndQuestionShortNames", query = "SELECT a FROM Answers a WHERE a.eventId = :eventId AND a.questionId IN(SELECT q.id FROM Questions q WHERE lower(q.shortName) IN (:questionShortNames))"),
    @NamedQuery(name = "Answers.findByOutbreakId", query = "SELECT a FROM Answers a WHERE a.outbreakId = :outbreakId"),
    @NamedQuery(name = "Answers.findByQuestionId", query = "SELECT a FROM Answers a WHERE a.questionId = :questionId"),
    @NamedQuery(name = "Answers.findByOutbreakIdAndQuestionId", query = "SELECT a FROM Answers a WHERE a.outbreakId = :outbreakId AND a.questionId = :questionId"),
    @NamedQuery(name = "Answers.findByTextAnswer", query = "SELECT a FROM Answers a WHERE a.textAnswer = :textAnswer"),
    @NamedQuery(name = "Answers.findByExportConversionValueId", query = "SELECT a FROM Answers a WHERE a.exportConversionValueId = :exportConversionValueId"),
    @NamedQuery(name = "Answers.findByCode", query = "SELECT a FROM Answers a WHERE a.code = :code")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "eventId",
    "questionId",
    "textAnswer",
    "exportConversionValueId",
    "code",
    "outbreakId",
    "createdAt",
    "updatedAt",
    "questionShortName",
    "formShortName"
})
@XmlRootElement(name = "answers")
public class Answers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "survey_answers_id_seq")
    @SequenceGenerator(name = "survey_answers_id_seq", sequenceName = "survey_answers_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "outbreak_id")
    @XmlElement(name = "outbreak_id", required = true)
    private Integer outbreakId;
    @Column(name = "question_id")
    @XmlElement(name = "question_id", required = true)
    private Integer questionId;
    @Column(name = "text_answer")
    @XmlElement(name = "text_answer", required = true)
    private String textAnswer;
    @Column(name = "export_conversion_value_id")
    @XmlElement(name = "export_conversion_value_id", required = true)
    private Integer exportConversionValueId;
    @Column(name = "code")
    @XmlElement(name = "code", required = true)
    private String code;

    @Transient
    @XmlElement(name = "form_short_name", required = true)
    private String formShortName;
    
    @Transient
    @XmlElement(name = "question_short_name", required = true)
    private String questionShortName;
    
    public Answers() {
    }

    public Answers(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean hasShortNames(){
        Boolean has = false;
        if(this.questionShortName != null && this.questionShortName.trim().length() > 0 && 
                this.formShortName != null && this.formShortName.trim().length() > 0){
        has = true;
        }
        return has;
    }
    
    public String getFormShortName() {
        return formShortName;
    }

    public void setFormShortName(String formShortName) {
        this.formShortName = formShortName;
    }

    public String getQuestionShortName() {
        return questionShortName;
    }

    public void setQuestionShortName(String questionShortName) {
        this.questionShortName = questionShortName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getTextAnswer() {
        return textAnswer;
    }

    public void setTextAnswer(String textAns) {
        this.textAnswer = textAns;
    }

    public Integer getExportConversionValueId() {
        return exportConversionValueId;
    }

    public void setExportConversionValueId(Integer exportConversionValueId) {
        this.exportConversionValueId = exportConversionValueId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Answers)) {
            return false;
        }
        Answers other = (Answers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Answers[ id=" + id + " ]";
    }
}
