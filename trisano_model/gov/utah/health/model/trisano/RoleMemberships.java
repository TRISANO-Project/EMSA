package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author UDOH
 */
@Entity
@Table(name = "role_memberships")
@NamedQueries({
    @NamedQuery(name = "RoleMemberships.findAll", query = "SELECT r FROM RoleMemberships r"),
    @NamedQuery(name = "RoleMemberships.findById", query = "SELECT r FROM RoleMemberships r WHERE r.id = :id"),
    @NamedQuery(name = "RoleMemberships.findByUserId", query = "SELECT r FROM RoleMemberships r WHERE r.userId = :userId order by r.jurisdictionId"),
    @NamedQuery(name = "RoleMemberships.findByRoleId", query = "SELECT r FROM RoleMemberships r WHERE r.roleId = :roleId"),
    @NamedQuery(name = "RoleMemberships.findByJurisdictionId", query = "SELECT r FROM RoleMemberships r WHERE r.jurisdictionId = :jurisdictionId"),
    @NamedQuery(name = "RoleMemberships.findByRoleIdAndJurisdictionId", query = "SELECT r FROM RoleMemberships r WHERE r.roleId = :roleId AND r.jurisdictionId = :jurisdictionId")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "userId",
    "roleId",
    "jurisdictionId",
    "sensitive",
    "privilegesRoles",
    "jurisdiction"
})
@XmlRootElement(name = "role_memberships")
public class RoleMemberships implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "user_id")
    @XmlElement(name = "user_id", required = true)
    private Integer userId;
    @Column(name = "role_id")
    @XmlElement(name = "role_id", required = true)
    private Integer roleId;
    @Column(name = "jurisdiction_id")
    @XmlElement(name = "jurisdiction_id", required = true)
    private Integer jurisdictionId;
    @Transient
    private Boolean sensitive;
    @Transient
    private List<PrivilegesRoles> privilegesRoles;
    @Transient
    private Places jurisdiction;

    public RoleMemberships() {
    }

    public RoleMemberships(Integer id) {
        this.id = id;
    }

    public List<PrivilegesRoles> getPrivilegesRoles() {
        return privilegesRoles;
    }

    public void setPrivilegesRoles(List<PrivilegesRoles> privilegesRoles) {
        this.privilegesRoles = privilegesRoles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSensitive() {
        return sensitive;
    }

    public void setSensitive(Boolean sensitive) {
        this.sensitive = sensitive;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public Places getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Places jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean canCreateCmr() {
        Boolean canCreate = false;
        if (privilegesRoles != null && !privilegesRoles.isEmpty()) {
            for (PrivilegesRoles pr : privilegesRoles) {
                if ("update_event".equals(pr.getPrivName()) || "create_event".equals(pr.getPrivName())) {
                    canCreate = true;
                    break;
                }
            }
        }
        return canCreate;
    }

    public Boolean canViewCmr() {
        Boolean canView = false;
        if (privilegesRoles != null && !privilegesRoles.isEmpty()) {
            for (PrivilegesRoles pr : privilegesRoles) {
                if ("view_event".equals(pr.getPrivName()) || this.canCreateCmr()) {
                    canView = true;
                    break;
                }
            }
        }
        return canView;
    }

    public Boolean canEntities() {
        Boolean can = false;
        if (privilegesRoles != null && !privilegesRoles.isEmpty()) {
            for (PrivilegesRoles pr : privilegesRoles) {
                if ("manage_entities".equals(pr.getPrivName())) {
                    can = true;
                    break;
                }
            }
        }
        return can;
    }

    public Boolean canAvr() {
        Boolean canView = false;
        if (privilegesRoles != null && !privilegesRoles.isEmpty()) {
            for (PrivilegesRoles pr : privilegesRoles) {
                if ("access_avr".equals(pr.getPrivName())) {
                    canView = true;
                    break;
                }
            }
        }
        return canView;
    }

    public Boolean canAdmin() {
        Boolean canView = false;
        if (privilegesRoles != null && !privilegesRoles.isEmpty()) {
            for (PrivilegesRoles pr : privilegesRoles) {
                if ("administer".equals(pr.getPrivName())) {
                    canView = true;
                    break;
                }
            }
        }
        return canView;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleMemberships)) {
            return false;
        }
        RoleMemberships other = (RoleMemberships) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.RoleMemberships[ id=" + id + " ]";
    }
}
