
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "lab_results")
@NamedQueries({
    @NamedQuery(name = "LabResults.findAll", query = "SELECT l FROM LabResults l"),
    @NamedQuery(name = "LabResults.findByParticipationId", query = "SELECT l FROM LabResults l WHERE l.participationId = :participationId"),
    @NamedQuery(name = "LabResults.findByEntityId", query = "SELECT l FROM LabResults l WHERE l.participationId IN ( SELECT p.id FROM Participations p WHERE p.primaryEntityId = :entityId )")
})// select * from lab_results where participation_id in(select id from participations where primary_entity_id = 2);


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "specimenSourceId",
    "collectionDate",
    "labTestDate",
    "specimenSentToStateId",
    "createdAt",
    "updatedAt",
    "participationId",
    "referenceRange",
    "stagedMessageId",
    "loincCode",
    "testTypeId",
    "testResultId",
    "resultValue",
    "units",
    "testStatusId",
    "comment",
    "organismId",
    "accessionNo"
})
@XmlRootElement(name = "lab_results")

public class LabResults implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="lab_results_id_seq")
    @SequenceGenerator(name="lab_results_id_seq", sequenceName="lab_results_id_seq", allocationSize=1)
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "specimen_source_id")
    @XmlElement(name = "specimen_source_id", required = true)
    private Integer specimenSourceId;
    @Column(name = "collection_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "collection_date", required = true)
    private Date collectionDate;
    @Column(name = "lab_test_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "lab_test_date", required = true)
    private Date labTestDate;
    @Column(name = "specimen_sent_to_state_id")
    @XmlElement(name = "specimen_sent_to_state_id", required = true)
    private Integer specimenSentToStateId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "participation_id")
    @XmlElement(name = "participation_id", required = true)
    private Integer participationId;
    @Column(name = "reference_range")
    @XmlElement(name = "reference_range", required = true)
    private String referenceRange;
    @Column(name = "staged_message_id")
    @XmlElement(name = "staged_message_id", required = true)
    private Integer stagedMessageId;
    @Column(name = "loinc_code")
    @XmlElement(name = "loinc_code", required = true)
    private String loincCode;
    @Column(name = "test_type_id")
    @XmlElement(name = "test_type_id", required = true)
    private Integer testTypeId;
    @Column(name = "test_result_id")
    @XmlElement(name = "test_result_id", required = true)
    private Integer testResultId;
    @Column(name = "result_value")
    @XmlElement(name = "result_value", required = true)
    private String resultValue;
    @Column(name = "units")
    @XmlElement(name = "units", required = true)
    private String units;
    @Column(name = "test_status_id")
    @XmlElement(name = "test_status_id", required = true)
    private Integer testStatusId;
    @Column(name = "comment")
    @XmlElement(name = "comment", required = true)
    private String comment;
    @Column(name = "organism_id")
    @XmlElement(name = "organism_id", required = true)
    private Integer organismId;
    @Column(name = "accession_no")
    @XmlElement(name = "accession_no", required = true)
    private String accessionNo;


    public LabResults() {
    }

    public String getAccessionNo() {
        return accessionNo;
    }

    public void setAccessionNo(String accessionNo) {
        this.accessionNo = accessionNo;
    }

    public LabResults(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpecimenSourceId() {
        return specimenSourceId;
    }

    public void setSpecimenSourceId(Integer specimenSourceId) {
        this.specimenSourceId = specimenSourceId;
    }

    public Date getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(Date collectionDate) {
        this.collectionDate = collectionDate;
    }

    public Date getLabTestDate() {
        return labTestDate;
    }

    public void setLabTestDate(Date labTestDate) {
        this.labTestDate = labTestDate;
    }

    public Integer getSpecimenSentToStateId() {
        return specimenSentToStateId;
    }

    public void setSpecimenSentToStateId(Integer specimenSentToStateId) {
        this.specimenSentToStateId = specimenSentToStateId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getParticipationId() {
        return participationId;
    }

    public void setParticipationId(Integer participationId) {
        this.participationId = participationId;
    }

    public String getReferenceRange() {
        return referenceRange;
    }

    public void setReferenceRange(String referenceRange) {
        this.referenceRange = referenceRange;
    }

    public Integer getStagedMessageId() {
        return stagedMessageId;
    }

    public void setStagedMessageId(Integer stagedMessageId) {
        this.stagedMessageId = stagedMessageId;
    }

    public String getLoincCode() {
        return loincCode;
    }

    public void setLoincCode(String loincCode) {
        this.loincCode = loincCode;
    }

    public Integer getTestTypeId() {
        return testTypeId;
    }

    public void setTestTypeId(Integer testTypeId) {
        this.testTypeId = testTypeId;
    }

    public Integer getTestResultId() {
        return testResultId;
    }

    public void setTestResultId(Integer testResultId) {
        this.testResultId = testResultId;
    }

    public String getResultValue() {
        return resultValue;
    }

    public void setResultValue(String resultValue) {
        this.resultValue = resultValue;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Integer getTestStatusId() {
        return testStatusId;
    }

    public void setTestStatusId(Integer testStatusId) {
        this.testStatusId = testStatusId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getOrganismId() {
        return organismId;
    }

    public void setOrganismId(Integer organismId) {
        this.organismId = organismId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof LabResults)) {
            return false;
        }
        LabResults other = (LabResults) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.trisano.LabResults[id=" + id + "]";
    }

}
