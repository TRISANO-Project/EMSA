package gov.utah.health.model.trisano;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"items"
})

@XmlRootElement(name = "data")
public class THData {
	@XmlElement(name="item", required=true)protected List<THDataItem> items;

	public List<THDataItem> getItems() {
		return items;
	}

	public void setItems(List<THDataItem> item) {
		this.items = item;
	}
	
	public THDataItem getItemByName(String name){
		if(items != null){
			for(THDataItem di:items){
				if(di.name.equals(name)){
					return di;
				}
			}
		}
		return null;
	}
}
