package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "privileges_roles")
@NamedQueries({
    @NamedQuery(name = "PrivilegesRoles.findAll", query = "SELECT p FROM PrivilegesRoles p"),
    @NamedQuery(name = "PrivilegesRoles.findById", query = "SELECT p FROM PrivilegesRoles p WHERE p.id = :id"),
    @NamedQuery(name = "PrivilegesRoles.findByRoleId", query = "SELECT p FROM PrivilegesRoles p WHERE p.roleId = :roleId"),
    @NamedQuery(name = "PrivilegesRoles.findByPrivilegeId", query = "SELECT p FROM PrivilegesRoles p WHERE p.privilegeId = :privilegeId"),
    @NamedQuery(name = "PrivilegesRoles.findByJurisdictionId", query = "SELECT p FROM PrivilegesRoles p WHERE p.jurisdictionId = :jurisdictionId")
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "roleId",
    "privilegeId",
    "jurisdictionId",
    "createdAt",
    "updatedAt",
    "privilege"
})
@XmlRootElement(name = "privileges_roles")
public class PrivilegesRoles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "privileges_roles_seq")
    @SequenceGenerator(name = "privileges_roles_seq", sequenceName = "privileges_roles_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "role_id")
    @XmlElement(name = "role_id", required = true)
    private Integer roleId;
    @Column(name = "privilege_id")
    @XmlElement(name = "privilege_id", required = true)
    private Integer privilegeId;
    @Column(name = "jurisdiction_id")
    @XmlElement(name = "jurisdiction_id", required = true)
    private Integer jurisdictionId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Transient
    private Privileges privilege;

    public PrivilegesRoles() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Integer privilegeId) {
        this.privilegeId = privilegeId;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public Privileges getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privileges privilege) {
        this.privilege = privilege;
    }

    public String getPrivName() {
        String privName = null;
        if(this.privilege != null && 
                this.privilege.getPrivName() != null && 
                this.privilege.getPrivName().trim().length() > 0){
        privName = this.privilege.getPrivName().trim();
        }
        return privName;

    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PrivilegesRoles)) {
            return false;
        }
        PrivilegesRoles other = (PrivilegesRoles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Privileges[id=" + id + "]";
    }
}
