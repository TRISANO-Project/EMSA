package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "place",
    "telephones",
    "addresses",
    "participations"
})
@XmlRootElement(name = "place_attributes")
public class PlaceAttributes {

    @XmlElement(name = "place", required = true)
    private Places place;
    @XmlElement(name = "telephones", required = true)
    private List<Telephones> telephones;
    @XmlElement(name = "addresses", required = true)
    private List<Addresses> addresses;
    @XmlElement(name = "participations", required = true)
    private List<Participations> participations;
    public transient Integer placeType;

    public Places getPlace() {
        return place;
    }

    public void setPlace(Places place) {
        this.place = place;
    }

    public List<Telephones> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephones> telephones) {
        this.telephones = telephones;
    }

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    public List<Participations> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participations> participations) {
        this.participations = participations;
    }

    public void addParticipation(Participations participation) {
        if (this.participations == null) {
            this.participations = new ArrayList<Participations>();
        }
        this.participations.add(participation);
    }

    public Boolean hasValidAddress() {

        Boolean valid = false;

        if (this.addresses != null
                && !this.addresses.isEmpty()
                && this.addresses.get(0).getId() != null
                && this.addresses.get(0).getId() > 0) {
            valid = true;
        }

        return valid;
    }
}
