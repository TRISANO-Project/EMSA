
package gov.utah.health.model.trisano;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiagnosticFacilitiesAttributes", propOrder = {
})
@XmlRootElement(name = "diagnostic_facilities_attributes")

public class DiagnosticFacilitiesAttributes extends PlaceAttributes {


    public DiagnosticFacilitiesAttributes() {
        super.placeType = Type.DIAGNOSTIC_FACILITY.getNumber();
    }

}
