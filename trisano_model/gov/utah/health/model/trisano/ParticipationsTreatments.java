/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "participations_treatments")
@NamedQueries({
    @NamedQuery(name = "ParticipationsTreatments.findAll", query = "SELECT p FROM ParticipationsTreatments p"),
    @NamedQuery(name = "ParticipationsTreatments.findById", query = "SELECT p FROM ParticipationsTreatments p WHERE p.id = :id"),
    @NamedQuery(name = "ParticipationsTreatments.findByParticipationId", query = "SELECT p FROM ParticipationsTreatments p WHERE p.participationId = :participationId"),
    @NamedQuery(name = "ParticipationsTreatments.findByTreatmentId", query = "SELECT p FROM ParticipationsTreatments p WHERE p.treatmentId = :treatmentId"),
    @NamedQuery(name = "ParticipationsTreatments.findByTreatmentGivenYnId", query = "SELECT p FROM ParticipationsTreatments p WHERE p.treatmentGivenYnId = :treatmentGivenYnId"),
    @NamedQuery(name = "ParticipationsTreatments.findByTreatmentDate", query = "SELECT p FROM ParticipationsTreatments p WHERE p.treatmentDate = :treatmentDate"),
    @NamedQuery(name = "ParticipationsTreatments.findByCreatedAt", query = "SELECT p FROM ParticipationsTreatments p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "ParticipationsTreatments.findByUpdatedAt", query = "SELECT p FROM ParticipationsTreatments p WHERE p.updatedAt = :updatedAt"),
    @NamedQuery(name = "ParticipationsTreatments.findByStopTreatmentDate", query = "SELECT p FROM ParticipationsTreatments p WHERE p.stopTreatmentDate = :stopTreatmentDate")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "participationId",
    "treatmentId",
    "treatmentGivenYnId",
    "treatmentDate",
    "createdAt",
    "updatedAt",
    "stopTreatmentDate",
    "treatmentComment"
})
@XmlRootElement(name = "participations_treatments")
    public class ParticipationsTreatments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "participation_id")
    @XmlElement(name = "participation_id", required = true)
    private Integer participationId;
    @Column(name = "treatment_id")
    @XmlElement(name = "treatment_id", required = true)
    private Integer treatmentId;
    @Column(name = "treatment_given_yn_id")
    @XmlElement(name = "treatment_given_yn_id", required = true)
    private Integer treatmentGivenYnId;
    @Column(name = "treatment_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "treatment_date", required = true)
    private Date treatmentDate;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "stop_treatment_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "stop_treatment_date", required = true)
    private Date stopTreatmentDate;
    @Column(name = "treatment_comment")
    @XmlElement(name = "treatment_comment", required = true)
    private String treatmentComment;

    public ParticipationsTreatments() {
    }

    public ParticipationsTreatments(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParticipationId() {
        return participationId;
    }

    public void setParticipationId(Integer participationId) {
        this.participationId = participationId;
    }

    public Integer getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(Integer treatmentId) {
        this.treatmentId = treatmentId;
    }

    public Integer getTreatmentGivenYnId() {
        return treatmentGivenYnId;
    }

    public void setTreatmentGivenYnId(Integer treatmentGivenYnId) {
        this.treatmentGivenYnId = treatmentGivenYnId;
    }

    public Date getTreatmentDate() {
        return treatmentDate;
    }

    public void setTreatmentDate(Date treatmentDate) {
        this.treatmentDate = treatmentDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getStopTreatmentDate() {
        return stopTreatmentDate;
    }

    public void setStopTreatmentDate(Date stopTreatmentDate) {
        this.stopTreatmentDate = stopTreatmentDate;
    }

    public String getTreatmentComment() {
        return treatmentComment;
    }

    public void setTreatmentComment(String treatmentComment) {
        this.treatmentComment = treatmentComment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ParticipationsTreatments)) {
            return false;
        }
        ParticipationsTreatments other = (ParticipationsTreatments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.ParticipationsTreatments[id=" + id + "]";
    }

}
