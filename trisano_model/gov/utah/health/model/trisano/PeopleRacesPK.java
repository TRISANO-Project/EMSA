/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Embeddable
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "raceId",
    "entityId"
})
@XmlRootElement(name = "serial_version_uid")
public class PeopleRacesPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "race_id")
    @XmlElement(name = "race_id", required = true)
    private int raceId;
    @Basic(optional = false)
    @Column(name = "entity_id")
    @XmlElement(name = "entity_id", required = true)
    private int entityId;

    public PeopleRacesPK() {
    }

    public PeopleRacesPK(int raceId, int entityId) {
        this.raceId = raceId;
        this.entityId = entityId;
    }

    public int getRaceId() {
        return raceId;
    }

    public void setRaceId(int raceId) {
        this.raceId = raceId;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) raceId;
        hash += (int) entityId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PeopleRacesPK)) {
            return false;
        }
        PeopleRacesPK other = (PeopleRacesPK) object;
        if (this.raceId != other.raceId) {
            return false;
        }
        if (this.entityId != other.entityId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.PeopleRacesPK[raceId=" + raceId + ", entityId=" + entityId + "]";
    }
}
