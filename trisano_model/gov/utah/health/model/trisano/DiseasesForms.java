/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author UDOH
 */
@Entity
@Table(name = "diseases_forms")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DiseasesForms.findAll", query = "SELECT d FROM DiseasesForms d"),
    @NamedQuery(name = "DiseasesForms.findByFormId", query = "SELECT d FROM DiseasesForms d WHERE d.diseasesFormsPK.formId = :formId"),
    @NamedQuery(name = "DiseasesForms.findByDiseaseId", query = "SELECT d FROM DiseasesForms d WHERE d.diseasesFormsPK.diseaseId = :diseaseId"),
    @NamedQuery(name = "DiseasesForms.findByCreatedAt", query = "SELECT d FROM DiseasesForms d WHERE d.createdAt = :createdAt"),
    @NamedQuery(name = "DiseasesForms.findByUpdatedAt", query = "SELECT d FROM DiseasesForms d WHERE d.updatedAt = :updatedAt")})
public class DiseasesForms implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DiseasesFormsPK diseasesFormsPK;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public DiseasesForms() {
    }

    public DiseasesForms(DiseasesFormsPK diseasesFormsPK) {
        this.diseasesFormsPK = diseasesFormsPK;
    }

    public DiseasesForms(int formId, int diseaseId) {
        this.diseasesFormsPK = new DiseasesFormsPK(formId, diseaseId);
    }

    public DiseasesFormsPK getDiseasesFormsPK() {
        return diseasesFormsPK;
    }

    public void setDiseasesFormsPK(DiseasesFormsPK diseasesFormsPK) {
        this.diseasesFormsPK = diseasesFormsPK;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diseasesFormsPK != null ? diseasesFormsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiseasesForms)) {
            return false;
        }
        DiseasesForms other = (DiseasesForms) object;
        if ((this.diseasesFormsPK == null && other.diseasesFormsPK != null) || (this.diseasesFormsPK != null && !this.diseasesFormsPK.equals(other.diseasesFormsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.DiseasesForms[ diseasesFormsPK=" + diseasesFormsPK + " ]";
    }

}
