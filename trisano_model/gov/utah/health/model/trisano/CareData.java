package gov.utah.health.model.trisano;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "eventId",
    "eventOnsetDate",
    "dateDiagnosed",
    "dateCollected",
    "test",
    "value",
    "units",
    "inCare"
})
@XmlRootElement(name = "care_data")
public class CareData {

    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @XmlElement(name = "event_onset_date", required = true)
    private Date dateDiagnosed;
    @XmlElement(name = "date_diagnosed", required = true)
    private Date eventOnsetDate;
    @XmlElement(name = "date_collected", required = true)
    private Date dateCollected;
    @XmlElement(name = "test", required = true)
    private String test;
    @XmlElement(name = "value", required = true)
    private String value;
    @XmlElement(name = "units", required = true)
    private String units;
    @XmlElement(name = "in_care", required = true)
    private Boolean inCare;

    public Boolean getInCare() {
        return inCare;
    }

    public void setInCare(Boolean inCare) {
        this.inCare = inCare;
    }

    public Date getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(Date dateCollected) {
        this.dateCollected = dateCollected;
    }

    public Date getDateDiagnosed() {
        return dateDiagnosed;
    }

    public void setDateDiagnosed(Date dateDiagnosed) {
        this.dateDiagnosed = dateDiagnosed;
    }

    public Integer getDaysToCare() {
        Integer daysBetween = -1;
        //calculate days between event_onset_date
        if (dateDiagnosed != null && dateCollected != null) {

            if (dateDiagnosed.before(dateCollected)) {

                GregorianCalendar gcDiagnosed = new GregorianCalendar();
                gcDiagnosed.setTime(dateDiagnosed);

                GregorianCalendar gcCollected = new GregorianCalendar();
                gcCollected.setTime(dateCollected);

                daysBetween = 0;
                while (gcDiagnosed.before(gcCollected)) {
                    gcDiagnosed.add(Calendar.DAY_OF_MONTH, 1);
                    daysBetween++;
                }

            } else if (dateDiagnosed.equals(dateCollected)) {
                daysBetween = 0;
            }

        }
        return daysBetween;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Date getEventOnsetDate() {
        return eventOnsetDate;
    }

    public void setEventOnsetDate(Date eventOnsetDate) {
        this.eventOnsetDate = eventOnsetDate;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
