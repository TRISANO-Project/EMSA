package gov.utah.health.model.trisano;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the trisano package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: trisano
     *
     */
    public ObjectFactory() {
    }

    public TrisanoHealth createTrisanoHealth() {
        return new TrisanoHealth();
    }

    public Attachments createAttachments() {
        return new Attachments();
    }

    public EventAttributes createEventAttributes() {
        return new EventAttributes();
    }

    public Addresses createAddresses() {
        return new Addresses();
    }

    public Answers createAnswers() {
        return new Answers();
    }

    public EmailAddresses createEmailAddresses() {
        return new EmailAddresses();
    }

    public DiseaseEvents createDiseaseEvents() {
        return new DiseaseEvents();
    }

    public Diseases createDiseases() {
        return new Diseases();
    }

    public Events createEvents() {
        return new Events();
    }

    public Participations createParticipations() {
        return new Participations();
    }

    public ParticipationsEncounters createParticipationsEncounters() {
        return new ParticipationsEncounters();
    }

    public People createPeople() {
        return new People();
    }

    public PeopleRaces createPeopleRaces() {
        return new PeopleRaces();
    }

    public PeopleRacesPK createPeopleRacesPK() {
        return new PeopleRacesPK();
    }

    public Telephones createTelephones() {
        return new Telephones();
    }

    public LabResults createLabResults() {
        return new LabResults();
    }

    public Places createPlaces() {
        return new Places();
    }

    public Notes createNotes() {
        return new Notes();
    }

    public FormReferences createFormReferences() {
        return new FormReferences();
    }

    public Codes createCodes() {
        return new Codes();
    }

    public CommonTestTypes createCommonTestTypes() {
        return new CommonTestTypes();
    }

    public Treatments createTreatments() {
        return new Treatments();
    }

    public ExternalCodes createExternalCodes() {
        return new ExternalCodes();
    }

    public Organisms createOrganisms() {
        return new Organisms();
    }

    public ParticipationsContacts createParticipationsContacts() {
        return new ParticipationsContacts();
    }

    public Questions createQuestions() {
        return new Questions();
    }

    public ContactChildEventsAttributes createContactChildEventsAttributes() {
        return new ContactChildEventsAttributes();
    }

    public HospitalizationFacilitiesAttributes createHospitalizationFacilitiesAttributes() {
        return new HospitalizationFacilitiesAttributes();
    }

    public DiagnosticFacilitiesAttributes createDiagnosticFacilitiesAttributes() {
        return new DiagnosticFacilitiesAttributes();
    }

    public LabsAttributes createLabsAttributes() {
        return new LabsAttributes();
    }

    public ReportingAgencyAttributes createReportingAgencyAttributes() {
        return new ReportingAgencyAttributes();
    }

    public ReporterAttributes createReporterAttributes() {
        return new ReporterAttributes();
    }

    public InterestedPartyAttributes createInterestedPartyAttributes() {
        return new InterestedPartyAttributes();
    }

    public CliniciansAttributes createClinicianAttributes() {
        return new CliniciansAttributes();
    }

    public JurisdictionAttributes createJurisdictionAttributes() {
        return new JurisdictionAttributes();
    }

    public Tasks createTasks() {
        return new Tasks();
    }

    public FormsAttributes createFromsAttributes() {
        return new FormsAttributes();
    }

    public Users createUsers() {
        return new Users();
    }

    public RoleMemberships createRoleMemberships() {
        return new RoleMemberships();
    }

    public Outbreak createOutbreak() {
        return new Outbreak();
    }

    public Privileges createPrivileges() {
        return new Privileges();
    }

}
