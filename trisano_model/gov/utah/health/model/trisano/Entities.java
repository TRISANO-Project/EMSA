package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "entities")
@NamedQueries({
    @NamedQuery(name = "Entities.findAll", query = "SELECT e FROM Entities e"),
    @NamedQuery(name = "Entities.findById", query = "SELECT e FROM Entities e WHERE e.id = :id"),
    @NamedQuery(name = "Entities.findByRecordNumber", query = "SELECT e FROM Entities e WHERE e.recordNumber = :recordNumber")})

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "recordNumber",
    "entityUrlNumber",
    "createdAt",
    "updatedAt",
    "entityType",
    "deletedAt",
    "mergedIntoEntityId",
    "mergeEffectedEvents"

})
@XmlRootElement(name = "entities")
    public class Entities implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="entities_id_seq")
    @SequenceGenerator(name="entities_id_seq", sequenceName="entities_id_seq", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "record_number")
    @XmlElement(name = "record_number", required = true)
    private String recordNumber;
    @Column(name = "entity_url_number")
    @XmlElement(name = "entity_url_number", required = true)
    private String entityUrlNumber;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "entity_type")
    @XmlElement(name = "entity_type", required = true)
    private String entityType;
    @Column(name = "deleted_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "deleted_at", required = true)
    private Date deletedAt;
    @Column(name = "merged_into_entity_id")
    @XmlElement(name = "merged_into_entity_id", required = true)
    private Integer mergedIntoEntityId;
    @Column(name = "merge_effected_events")
    @XmlElement(name = "merge_effected_events", required = true)
    private String mergeEffectedEvents;

    public Entities() {
    }

    public Entities(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRecordNumber() {
        return recordNumber;
    }

    public void setRecordNumber(String recordNumber) {
        this.recordNumber = recordNumber;
    }

    public String getEntityUrlNumber() {
        return entityUrlNumber;
    }

    public void setEntityUrlNumber(String entityUrlNumber) {
        this.entityUrlNumber = entityUrlNumber;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getMergeEffectedEvents() {
        return mergeEffectedEvents;
    }

    public void setMergeEffectedEvents(String mergeEffectedEvents) {
        this.mergeEffectedEvents = mergeEffectedEvents;
    }

    public Integer getMergedIntoEntityId() {
        return mergedIntoEntityId;
    }

    public void setMergedIntoEntityId(Integer mergedIntoEntityId) {
        this.mergedIntoEntityId = mergedIntoEntityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Entities)) {
            return false;
        }
        Entities other = (Entities) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.Entities[id=" + id + "]";
    }

}
