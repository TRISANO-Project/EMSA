package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "places")
@NamedQueries({
    @NamedQuery(name = "Places.findAll", query = "SELECT p FROM Places p"),
    @NamedQuery(name = "Places.findById", query = "SELECT p FROM Places p WHERE p.id = :id"),
    @NamedQuery(name = "Places.findByEntityId", query = "SELECT p FROM Places p WHERE p.entityId = :entityId"),
    @NamedQuery(name = "Places.findByCreatedAt", query = "SELECT p FROM Places p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "Places.findByUpdatedAt", query = "SELECT p FROM Places p WHERE p.updatedAt = :updatedAt"),
    @NamedQuery(name = "Places.findByName", query = "SELECT p FROM Places p WHERE p.name = :name ORDER BY p.id DESC "),
    @NamedQuery(name = "Places.findByNameNotDeleted", query = "SELECT p FROM Places p, Entities e WHERE p.entityId = e.id AND e.deletedAt is null AND e.mergedIntoEntityId is null AND p.name = :name ORDER BY p.id DESC "),
    @NamedQuery(name = "Places.findByShortName", query = "SELECT p FROM Places p WHERE p.shortName = :shortName"),
    @NamedQuery(name = "Places.findByParticipationType", query = "SELECT p FROM Places p WHERE p.entityId IN (SELECT DISTINCT pa.secondaryEntityId FROM Participations pa WHERE pa.type = :type) ORDER BY p.name "),
    @NamedQuery(name = "Places.findByParticipationTypeEntityId", query = "SELECT p FROM Places p WHERE p.entityId IN (SELECT pa.secondaryEntityId FROM Participations pa WHERE pa.primaryEntityId = :entityId AND pa.type = :type)"),
    @NamedQuery(name = "Places.findByPlacesTypeId", query = "SELECT p FROM Places p WHERE p.id IN (SELECT pt.placesTypesPK.placeId FROM PlacesTypes pt WHERE pt.placesTypesPK.typeId = :typeId) ORDER BY p.name ")
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "entityId",
    "createdAt",
    "updatedAt",
    "name",
    "shortName"
})
@XmlRootElement(name = "places")
public class Places implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "places_id_seq")
    @SequenceGenerator(name = "places_id_seq", sequenceName = "places_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "entity_id")
    @XmlElement(name = "entity_id", required = true)
    private Integer entityId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "name")
    @XmlElement(name = "name", required = true)
    private String name;
    @Column(name = "short_name")
    @XmlElement(name = "short_name", required = true)
    private String shortName;

    public Places() {
    }

    public Places(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Places)) {
            return false;
        }
        Places other = (Places) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Places[id=" + id + "]";
    }
}
