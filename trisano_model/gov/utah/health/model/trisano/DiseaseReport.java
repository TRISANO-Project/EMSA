package gov.utah.health.model.trisano;

import gov.utah.health.model.trisano.Places;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "place",
    "createdAtStart",
    "createdAtEnd",
    "dateDiagnosedStart",
    "dateDiagnosedEnd",
    "diseaseCount"
})
@XmlRootElement(name = "disease_report")
public class DiseaseReport {

    @XmlElement(name = "place", required = true)
    private Places place;
    @XmlElement(name = "created_at_start", required = true)
    private Date createdAtStart;
    @XmlElement(name = "created_at_end", required = true)
    private Date createdAtEnd;
    @XmlElement(name = "date_diagnosed_start", required = true)
    private Date dateDiagnosedStart;
    @XmlElement(name = "date_diagnosed_end", required = true)
    private Date dateDiagnosedEnd;
    @XmlElement(name = "disease_count", required = true)
    private List<DiseaseCount> diseaseCount;

    public Date getDateDiagnosedEnd() {
        return dateDiagnosedEnd;
    }

    public void setDateDiagnosedEnd(Date dateDiagnosedEnd) {
        this.dateDiagnosedEnd = dateDiagnosedEnd;
    }

    public Date getDateDiagnosedStart() {
        return dateDiagnosedStart;
    }

    public void setDateDiagnosedStart(Date dateDiagnosedStart) {
        this.dateDiagnosedStart = dateDiagnosedStart;
    }

    public Date getCreatedAtEnd() {
        return createdAtEnd;
    }

    public void setCreatedAtEnd(Date createdAtEnd) {
        this.createdAtEnd = createdAtEnd;
    }

    public Date getCreatedAtStart() {
        return createdAtStart;
    }

    public void setCreatedAtStart(Date createdAtStart) {
        this.createdAtStart = createdAtStart;
    }

    public List<DiseaseCount> getDiseaseCount() {
        return diseaseCount;
    }

    public void setDiseaseCount(List<DiseaseCount> diseaseCount) {
        this.diseaseCount = diseaseCount;
    }

    public Places getPlace() {
        return place;
    }

    public void setPlace(Places place) {
        this.place = place;
    }

}
