package gov.utah.health.model.trisano;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "report_sections")
@NamedQueries({
    @NamedQuery(name = "ReportSection.findAll", query = "SELECT r FROM ReportSection r"),
    @NamedQuery(name = "ReportSection.findById", query = "SELECT r FROM ReportSection r WHERE r.id = :id")
})
/**
 * @author UDOH
 */
public class ReportSection {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "report_sections_id")
    @SequenceGenerator(name = "report_sections_id", sequenceName = "report_sections_id", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "description")
    private String description;

    public ReportSection() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
}
