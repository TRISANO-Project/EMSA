package gov.utah.health.model.trisano;

import java.util.GregorianCalendar;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author UDOH
 */
@ManagedBean(name = "oea")
@RequestScoped
public class OutbreakEventAttributes {

    private Events event;
    private People people;
    private Diseases disease;
    private JurisdictionAttributes jurisdiction;
    private ExternalCodes localStatus;
    private Addresses location;
    private Outbreak outbreak;
    private Integer currentOutbreakId;
    private Boolean removed = Boolean.FALSE;

    public Boolean getRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    public Integer getCurrentOutbreakId() {
        return currentOutbreakId;
    }

    public void setCurrentOutbreakId(Integer currentOutbreakId) {
        this.currentOutbreakId = currentOutbreakId;
    }
    


    public Events getEvent() {
        return event;
    }

    public void setEvent(Events event) {
        this.event = event;
    }

    public ExternalCodes getLocalStatus() {
        return localStatus;
    }

    public void setLocalStatus(ExternalCodes localStatus) {
        this.localStatus = localStatus;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public Diseases getDisease() {
        return disease;
    }

    public void setDisease(Diseases disease) {
        this.disease = disease;
    }

    public JurisdictionAttributes getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(JurisdictionAttributes jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public Addresses getLocation() {
        return location;
    }

    public void setLocation(Addresses location) {
        this.location = location;
    }

    public Integer getAge() {
        Integer age = -1;
        if(this.people != null && this.people.getBirthDate() != null){
            GregorianCalendar bdgc = new GregorianCalendar();
            bdgc.setTime(this.people.getBirthDate());
            GregorianCalendar now = new GregorianCalendar();
            int i = 0;
            while(bdgc.before(now)){
                bdgc.add(GregorianCalendar.YEAR, 1);
                i++;
            }
            age = i;
        }
        return age;
    }

    public Outbreak getOutbreak() {
        return outbreak;
    }

    public void setOutbreak(Outbreak outbreak) {
        this.outbreak = outbreak;
    }
    
     public String getAddRemoveEvent() {
        String action;
        if(this.removed.equals(Boolean.FALSE)){
            action = "Remove";
        }
        else{
            action = "Add";            
        }
        return action;
    }
}
