/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "participations_contacts")
@NamedQueries({
    @NamedQuery(name = "ParticipationsContacts.findAll", query = "SELECT p FROM ParticipationsContacts p"),
    @NamedQuery(name = "ParticipationsContacts.findById", query = "SELECT p FROM ParticipationsContacts p WHERE p.id = :id"),
    @NamedQuery(name = "ParticipationsContacts.findByDispositionId", query = "SELECT p FROM ParticipationsContacts p WHERE p.dispositionId = :dispositionId"),
    @NamedQuery(name = "ParticipationsContacts.findByContactTypeId", query = "SELECT p FROM ParticipationsContacts p WHERE p.contactTypeId = :contactTypeId"),
    @NamedQuery(name = "ParticipationsContacts.findByCreatedAt", query = "SELECT p FROM ParticipationsContacts p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "ParticipationsContacts.findByUpdatedAt", query = "SELECT p FROM ParticipationsContacts p WHERE p.updatedAt = :updatedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "dispositionId",
    "contactTypeId",
    "createdAt",
    "updatedAt",
    "dispositionDate"
})
@XmlRootElement(name = "participations_contacts")
public class ParticipationsContacts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "disposition_id")
    @XmlElement(name = "disposition_id", required = true)
    private Integer dispositionId;
    @Column(name = "contact_type_id")
    @XmlElement(name = "contact_type_id", required = true)
    private Integer contactTypeId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "disposition_date")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "disposition_date", required = true)
    private Date dispositionDate;

    public ParticipationsContacts() {
    }

    public ParticipationsContacts(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDispositionId() {
        return dispositionId;
    }

    public void setDispositionId(Integer dispositionId) {
        this.dispositionId = dispositionId;
    }

    public Integer getContactTypeId() {
        return contactTypeId;
    }

    public void setContactTypeId(Integer contactTypeId) {
        this.contactTypeId = contactTypeId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDispositionDate() {
        return dispositionDate;
    }

    public void setDispositionDate(Date dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ParticipationsContacts)) {
            return false;
        }
        ParticipationsContacts other = (ParticipationsContacts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.ParticipationsContacts[id=" + id + "]";
    }
}
