/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "organisms")
@NamedQueries({
    @NamedQuery(name = "Organisms.findAll", query = "SELECT o FROM Organisms o ORDER BY o.organismName"),
    @NamedQuery(name = "Organisms.findById", query = "SELECT o FROM Organisms o WHERE o.id = :id"),
    @NamedQuery(name = "Organisms.findByOrganismName", query = "SELECT o FROM Organisms o WHERE lower(o.organismName) like lower(:organismName)"),
    @NamedQuery(name = "Organisms.findBySnomedName", query = "SELECT o FROM Organisms o WHERE o.snomedName = :snomedName"),
    @NamedQuery(name = "Organisms.findBySnomedCode", query = "SELECT o FROM Organisms o WHERE o.snomedCode = :snomedCode"),
    @NamedQuery(name = "Organisms.findBySnomedId", query = "SELECT o FROM Organisms o WHERE o.snomedId = :snomedId"),
    @NamedQuery(name = "Organisms.findByCreatedAt", query = "SELECT o FROM Organisms o WHERE o.createdAt = :createdAt"),
    @NamedQuery(name = "Organisms.findByUpdatedAt", query = "SELECT o FROM Organisms o WHERE o.updatedAt = :updatedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "organismName",
    "snomedName",
    "snomedCode",
    "snomedId",
    "createdAt",
    "updatedAt"
})
@XmlRootElement(name = "organisms")
    public class Organisms implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "organism_name")
    @XmlElement(name = "organism_name", required = true)
    private String organismName;
    @Column(name = "snomed_name")
    @XmlElement(name = "snomed_name", required = true)
    private String snomedName;
    @Column(name = "snomed_code")
    @XmlElement(name = "snomed_code", required = true)
    private String snomedCode;
    @Column(name = "snomed_id")
    @XmlElement(name = "snomed_id", required = true)
    private String snomedId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;

    public Organisms() {
    }

    public Organisms(Integer id) {
        this.id = id;
    }

    public Organisms(Integer id, String organismName) {
        this.id = id;
        this.organismName = organismName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrganismName() {
        return organismName;
    }

    public void setOrganismName(String organismName) {
        this.organismName = organismName;
    }

    public String getSnomedName() {
        return snomedName;
    }

    public void setSnomedName(String snomedName) {
        this.snomedName = snomedName;
    }

    public String getSnomedCode() {
        return snomedCode;
    }

    public void setSnomedCode(String snomedCode) {
        this.snomedCode = snomedCode;
    }

    public String getSnomedId() {
        return snomedId;
    }

    public void setSnomedId(String snomedId) {
        this.snomedId = snomedId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Organisms)) {
            return false;
        }
        Organisms other = (Organisms) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Organisms[id=" + id + "]";
    }

}
