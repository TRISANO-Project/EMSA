package gov.utah.health.model.trisano;


import gov.utah.health.model.trisano.Diseases;
import gov.utah.health.model.trisano.Places;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "disease",
    "place",
    "diseaseOnsetStart",
    "diseaseOnsetEnd",
    "dateDiagnosedStart",
    "dateDiagnosedEnd",
    "lhdCaseStatus",
    "stateCaseStatus"
})
@XmlRootElement(name = "case_status_report")
public class CaseStatusReport {

    @XmlElement(name = "disease", required = true)
    private List<Diseases> disease;
    @XmlElement(name = "place", required = true)
    private Places place;
    @XmlElement(name = "disease_onset_start", required = true)
    private Date diseaseOnsetStart;
    @XmlElement(name = "disease_onset_end", required = true)
    private Date diseaseOnsetEnd;
    @XmlElement(name = "date_diagnosed_start", required = true)
    private Date dateDiagnosedStart;
    @XmlElement(name = "date_diagnosed_end", required = true)
    private Date dateDiagnosedEnd;
    @XmlElement(name = "lhd_case_status", required = true)
    private List<CaseStatusCount> lhdCaseStatus;
    @XmlElement(name = "state_case_status", required = true)
    private List<CaseStatusCount> stateCaseStatus;

    public List<CaseStatusCount> getLhdCaseStatus() {
        return lhdCaseStatus;
    }

    public void setLhdCaseStatus(List<CaseStatusCount> lhdCaseStatus) {
        this.lhdCaseStatus = lhdCaseStatus;
    }

    public List<CaseStatusCount> getStateCaseStatus() {
        return stateCaseStatus;
    }

    public void setStateCaseStatus(List<CaseStatusCount> stateCaseStatus) {
        this.stateCaseStatus = stateCaseStatus;
    }

    public List<Diseases> getDisease() {
        return disease;
    }

    public void setDisease(List<Diseases> disease) {
        this.disease = disease;
    }
    public String getDiseaseIds(){
        String list = null;
        StringBuilder sb = new StringBuilder();
        if(this.disease != null){

            for(Diseases d:this.disease){
                if(d.getId()!= null){
                    sb.append(d.getId());
                    sb.append(",");
                }
            }
            if(sb.length() > 0){
                list = sb.toString();
            }

        }

        return list;

    }

    public Date getDateDiagnosedEnd() {
        return dateDiagnosedEnd;
    }

    public void setDateDiagnosedEnd(Date dateDiagnosedEnd) {
        this.dateDiagnosedEnd = dateDiagnosedEnd;
    }

    public Date getDateDiagnosedStart() {
        return dateDiagnosedStart;
    }

    public void setDateDiagnosedStart(Date dateDiagnosedStart) {
        this.dateDiagnosedStart = dateDiagnosedStart;
    }

    public Date getDiseaseOnsetEnd() {
        return diseaseOnsetEnd;
    }

    public void setDiseaseOnsetEnd(Date diseaseOnsetEnd) {
        this.diseaseOnsetEnd = diseaseOnsetEnd;
    }

    public Date getDiseaseOnsetStart() {
        return diseaseOnsetStart;
    }

    public void setDiseaseOnsetStart(Date diseaseOnsetStart) {
        this.diseaseOnsetStart = diseaseOnsetStart;
    }

    public Places getPlace() {
        return place;
    }

    public void setPlace(Places place) {
        this.place = place;
    }

}
