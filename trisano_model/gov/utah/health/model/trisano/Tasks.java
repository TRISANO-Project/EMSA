
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "tasks")
@NamedQueries({
    @NamedQuery(name = "Tasks.findAll", query = "SELECT t FROM Tasks t"),
    @NamedQuery(name = "Tasks.findById", query = "SELECT t FROM Tasks t WHERE t.id = :id"),
    @NamedQuery(name = "Tasks.findByName", query = "SELECT t FROM Tasks t WHERE t.name = :name"),
    @NamedQuery(name = "Tasks.findByNotes", query = "SELECT t FROM Tasks t WHERE t.notes = :notes"),
    @NamedQuery(name = "Tasks.findByPriority", query = "SELECT t FROM Tasks t WHERE t.priority = :priority"),
    @NamedQuery(name = "Tasks.findByDueDate", query = "SELECT t FROM Tasks t WHERE t.dueDate = :dueDate"),
    @NamedQuery(name = "Tasks.findByCreatedAt", query = "SELECT t FROM Tasks t WHERE t.createdAt = :createdAt"),
    @NamedQuery(name = "Tasks.findByUpdatedAt", query = "SELECT t FROM Tasks t WHERE t.updatedAt = :updatedAt"),
    @NamedQuery(name = "Tasks.findByStatus", query = "SELECT t FROM Tasks t WHERE t.status = :status"),
    @NamedQuery(name = "Tasks.findByRepeatingTaskId", query = "SELECT t FROM Tasks t WHERE t.repeatingTaskId = :repeatingTaskId"),
    @NamedQuery(name = "Tasks.findByUntilDate", query = "SELECT t FROM Tasks t WHERE t.untilDate = :untilDate"),
    @NamedQuery(name = "Tasks.findByRepeatingInterval", query = "SELECT t FROM Tasks t WHERE t.repeatingInterval = :repeatingInterval"),
    @NamedQuery(name = "Tasks.findByTaskTrackingKey", query = "SELECT t FROM Tasks t WHERE t.taskTrackingKey = :taskTrackingKey")})

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "name",
    "notes",
    "priority",
    "eventId",
    "userId",
    "dueDate",
    "createdAt",
    "updatedAt",
    "categoryId",
    "status",
    "repeatingTaskId",
    "untilDate",
    "repeatingInterval",
    "taskTrackingKey"
})
@XmlRootElement(name = "tasks")
    public class Tasks implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "name")
    @XmlElement(name = "name", required = true)
    private String name;
    @Column(name = "notes")
    @XmlElement(name = "notes", required = true)
    private String notes;
    @Column(name = "priority")
    @XmlElement(name = "priority", required = true)
    private String priority;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "user_id")
    @XmlElement(name = "user_id", required = true)
    private Integer userId;
    @Column(name = "due_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "due_date", required = true)
    private Date dueDate;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "category_id")
    @XmlElement(name = "category_id", required = true)
    private Integer categoryId;
    @Column(name = "status")
    @XmlElement(name = "status", required = true)
    private String status;
    @Column(name = "repeating_task_id")
    @XmlElement(name = "repeating_task_id", required = true)
    private Integer repeatingTaskId;
    @Column(name = "until_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "until_date", required = true)
    private Date untilDate;
    @Column(name = "repeating_interval")
    @XmlElement(name = "repeating_interval", required = true)
    private String repeatingInterval;
    @Column(name = "task_tracking_key")
    @XmlElement(name = "task_tracking_key", required = true)
    private String taskTrackingKey;

    public Tasks() {
    }

    public Tasks(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRepeatingTaskId() {
        return repeatingTaskId;
    }

    public void setRepeatingTaskId(Integer repeatingTaskId) {
        this.repeatingTaskId = repeatingTaskId;
    }

    public Date getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(Date untilDate) {
        this.untilDate = untilDate;
    }

    public String getRepeatingInterval() {
        return repeatingInterval;
    }

    public void setRepeatingInterval(String repeatingInterval) {
        this.repeatingInterval = repeatingInterval;
    }

    public String getTaskTrackingKey() {
        return taskTrackingKey;
    }

    public void setTaskTrackingKey(String taskTrackingKey) {
        this.taskTrackingKey = taskTrackingKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Tasks)) {
            return false;
        }
        Tasks other = (Tasks) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Tasks[id=" + id + "]";
    }

}
