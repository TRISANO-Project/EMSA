package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "user",
    "emailAddresses"
})
@XmlRootElement(name = "users_attributes")
public class UsersAttributes {

    @XmlElement(name = "users", required = true)
    private Users user;
    @XmlElement(name = "email_addresses", required = true)
    private List<EmailAddresses> emailAddresses;

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<EmailAddresses> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(List<EmailAddresses> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public void addEmailAddress(EmailAddresses emailAddress) {
        if (this.emailAddresses == null) {
            this.emailAddresses = new ArrayList<EmailAddresses>();
        }
        this.emailAddresses.add(emailAddress);
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersAttributes)) {
            return false;
        }
        UsersAttributes other = (UsersAttributes) object;
        if (this.getUser() == null || other.getUser() == null) {
            return false;
        }
        Users oUser = other.getUser();
        Users thisUser = this.getUser();

        if (oUser.getId() == null || thisUser.getId() == null) {

            return false;
        }
        if (!thisUser.getId().equals(oUser.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 59 * hash + (this.emailAddresses != null ? this.emailAddresses.hashCode() : 0);
        return hash;
    }
}
