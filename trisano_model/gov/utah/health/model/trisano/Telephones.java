/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "telephones")
@NamedQueries({
    @NamedQuery(name = "Telephones.findAll", query = "SELECT t FROM Telephones t"),
    @NamedQuery(name = "Telephones.findById", query = "SELECT t FROM Telephones t WHERE t.id = :id"),
    @NamedQuery(name = "Telephones.findByEntityId", query = "SELECT t FROM Telephones t WHERE t.entityId = :entityId"),
    @NamedQuery(name = "Telephones.findByLocationId", query = "SELECT t FROM Telephones t WHERE t.locationId = :locationId"),
    @NamedQuery(name = "Telephones.findByCountryCode", query = "SELECT t FROM Telephones t WHERE t.countryCode = :countryCode"),
    @NamedQuery(name = "Telephones.findByAreaCode", query = "SELECT t FROM Telephones t WHERE t.areaCode = :areaCode"),
    @NamedQuery(name = "Telephones.findByPhoneNumber", query = "SELECT t FROM Telephones t WHERE t.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "Telephones.findByExtension", query = "SELECT t FROM Telephones t WHERE t.extension = :extension"),
    @NamedQuery(name = "Telephones.findByCreatedAt", query = "SELECT t FROM Telephones t WHERE t.createdAt = :createdAt"),
    @NamedQuery(name = "Telephones.findByUpdatedAt", query = "SELECT t FROM Telephones t WHERE t.updatedAt = :updatedAt"),
    @NamedQuery(name = "Telephones.findByEmailAddress", query = "SELECT t FROM Telephones t WHERE t.emailAddress = :emailAddress"),
    @NamedQuery(name = "Telephones.findByEntityLocationTypeId", query = "SELECT t FROM Telephones t WHERE t.entityLocationTypeId = :entityLocationTypeId")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Telephones", propOrder = {
    "id",
    "locationId",
    "countryCode",
    "areaCode",
    "phoneNumber",
    "extension",
    "createdAt",
    "updatedAt",
    "emailAddress",
    "entityLocationTypeId",
    "entityId"
})
@XmlRootElement(name = "telephones")
    public class Telephones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="telephones_id_seq")
    @SequenceGenerator(name="telephones_id_seq", sequenceName="telephones_id_seq", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "location_id")
    @XmlElement(name = "location_id", required = true)
    private Integer locationId;
    @Column(name = "country_code")
    @XmlElement(name = "country_code", required = true)
    private String countryCode;
    @Column(name = "area_code")
    @XmlElement(name = "area_code", required = true)
    private String areaCode;
    @Column(name = "phone_number")
    @XmlElement(name = "phone_number", required = true)
    private String phoneNumber;
    @Column(name = "extension")
    @XmlElement(name = "extension", required = true)
    private String extension;
    @Column(name = "created_at")
    @XmlElement(name = "created_at", required = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @XmlElement(name = "updated_at", required = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "email_address")
    @XmlElement(name = "email_address", required = true)
    private String emailAddress;
    @Column(name = "entity_location_type_id")
    @XmlElement(name = "entity_location_type_id", required = true)
    private Integer entityLocationTypeId;
    @Column(name = "entity_id")
    @XmlElement(name = "entity_id", required = true)
    private Integer entityId;

    public Telephones() {
    }

    public Telephones(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Integer getEntityLocationTypeId() {
        return entityLocationTypeId;
    }

    public void setEntityLocationTypeId(Integer entityLocationTypeId) {
        this.entityLocationTypeId = entityLocationTypeId;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Telephones)) {
            return false;
        }
        Telephones other = (Telephones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.Telephones[id=" + id + "]";
    }

}
