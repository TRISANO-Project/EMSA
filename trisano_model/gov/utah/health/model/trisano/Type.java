package gov.utah.health.model.trisano;

import gov.utah.health.util.AppProperties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides types that are the same as values in the participations.type field in trisano.
 * The name values of these types need to be the same as the various types in the
 * participations.type field in the database.
 *
 * @author UDOH
 */

public class Type {

    private static final AppProperties props = AppProperties.getInstance();
    
    public static Type JURISDICTION             = new Type(props.getIntProperty("jurisdiction_type_id"),"Jurisdiction");
    public static Type REPORTING_AGENCY         = new Type(props.getIntProperty("reporting_agency_type_id"),"ReportingAgency");
    public static Type DIAGNOSTIC_FACILITY      = new Type(props.getIntProperty("diagnostic_facility_type_id"),"DiagnosticFacility");
    public static Type HOSPITALIZATION_FACILITY = new Type(props.getIntProperty("hospitialization_facility_type_id"),"HospitalizationFacility");
    public static Type LAB                      = new Type(props.getIntProperty("lab_facility_type_id"),"Lab");
    public static Type INTERESTED_PLACE         = new Type(-1,"InterestedPlace");
    public static Type INTERESTED_PARTY         = new Type(-1,"InterestedParty");
    public static Type CLINICIAN                = new Type(-1,"Clinician");
    public static Type REPORTER                 = new Type(-1,"Reporter");
    public static Type PLACE_ENTITY             = new Type(-1,"PlaceEntity");
    public static Type PEOPLE_ENTITY            = new Type(-1,"PersonEntity");
    public static Type MORBIDITY_EVENT          = new Type(-1,"MorbidityEvent");
    public static Type CONTACT_EVENT            = new Type(-1,"ContactEvent");
    public static Type ASSESSMENT_EVENT         = new Type(-1,"AssessmentEvent");
    
    Type(Integer number,String name){

        this.number=number;
        this.name=name;
    }

    private Integer number;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public boolean equals(Type Type){

            boolean equals = false;
            if(Type.getNumber().equals(Type.getNumber())){
                equals = true;
            }

            return equals;
    }
    
    @Override
    public String toString(){
        return name.toString();
    }
}
