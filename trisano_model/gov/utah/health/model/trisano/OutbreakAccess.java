package gov.utah.health.model.trisano;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "outbreak_access")
@NamedQueries({
    @NamedQuery(name = "OutbreakAccess.findAll", query = "SELECT o FROM OutbreakAccess o"),
    @NamedQuery(name = "OutbreakAccess.findById", query = "SELECT o FROM OutbreakAccess o WHERE o.id = :id"),
    @NamedQuery(name = "OutbreakAccess.findByOutbreakId", query = "SELECT o FROM OutbreakAccess o WHERE o.outbreakId =:outbreakId"),
    @NamedQuery(name = "OutbreakAccess.findByJurisdictionId", query = "SELECT o FROM OutbreakAccess o WHERE o.jurisdictionId = :jurisdictionId"),
    @NamedQuery(name = "OutbreakAccess.findByJurisdictionIdAndOutbreakId", query = "SELECT o FROM OutbreakAccess o WHERE o.jurisdictionId = :jurisdictionId AND o.outbreakId =:outbreakId")
})
/**
 * @author UDOH
 */
public class OutbreakAccess {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "outbreak_access_id")
    @SequenceGenerator(name = "outbreak_access_id", sequenceName = "outbreak_access_id", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "outbreak_id")
    private Integer outbreakId;
    @Column(name = "jurisdiction_id")
    private Integer jurisdictionId;
    @Column(name = "type")
    private Integer type;
    @Transient
    private Places jurisdiction;

    public OutbreakAccess() {
    }

    public OutbreakAccess(Integer outbreakId, Integer jurisdictionId, Integer type, Places jurisdiction) {
        this.outbreakId = outbreakId;
        this.jurisdictionId = jurisdictionId;
        this.type = type;
        this.jurisdiction = jurisdiction;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeLabel() {
        String label = null;

        if (this.type != null) {
           label = OutbreakType.getAccessType(this.type);
        }
        return label;


    }

    public Places getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Places jurisdiction) {
        this.jurisdiction = jurisdiction;
    }
}
