package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;

public class OutbreakStatus {

    public static final OutbreakStatus ACTIVE = new OutbreakStatus(1, "ACTIVE");
    public static final OutbreakStatus COMPLETED = new OutbreakStatus(2, "COMPLETED");
    public static final List<OutbreakStatus> list = new ArrayList<OutbreakStatus>();

    public static final OutbreakStatus EVENT_STATUS_BELONGING_TO_CURRENT_OUTBREAK = new OutbreakStatus(1, "this outbreak");
    public static final OutbreakStatus EVENT_STATUS_BELONGING_TO_ANY_OUTBREAK = new OutbreakStatus(2, "any outbreak");
    public static final OutbreakStatus EVENT_STATUS_BELONGING_TO_NO_OUTBREAK = new OutbreakStatus(3, "no outbreak");
    public static final List<OutbreakStatus> outbreakEventStatusList = new ArrayList<OutbreakStatus>();
    
    
    static {
        list.add(ACTIVE);
        list.add(COMPLETED);
        
        //outbreakEventStatusList.add(EVENT_STATUS_BELONGING_TO_CURRENT_OUTBREAK);
        outbreakEventStatusList.add(EVENT_STATUS_BELONGING_TO_ANY_OUTBREAK);
        outbreakEventStatusList.add(EVENT_STATUS_BELONGING_TO_NO_OUTBREAK);
    }

    OutbreakStatus(Integer code, String statusMessage) {
        this.code = code;
        this.statusMessage = statusMessage;
    }
    private Integer code;
    private String statusMessage;

    public static String getLabel(Integer code) {

        String c = "Not Found";
        for (OutbreakStatus os : OutbreakStatus.list) {
            if (os.getCode().equals(code)) {
                c = os.getStatusMessage();
            }
        }

        return c;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public boolean equals(OutbreakStatus status) {

        boolean equals = false;
        if (status.getCode().equals(status.getCode())) {
            equals = true;
        }

        return equals;
    }

    @Override
    public String toString() {
        return code.toString();
    }
}
