package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "notes")
@NamedQueries({
    @NamedQuery(name = "Notes.findAll", query = "SELECT n FROM Notes n"),
    @NamedQuery(name = "Notes.findById", query = "SELECT n FROM Notes n WHERE n.id = :id"),
    @NamedQuery(name = "Notes.findByNote", query = "SELECT n FROM Notes n WHERE n.note = :note"),
    @NamedQuery(name = "Notes.findByStruckthrough", query = "SELECT n FROM Notes n WHERE n.struckthrough = :struckthrough"),
    @NamedQuery(name = "Notes.findByUserId", query = "SELECT n FROM Notes n WHERE n.userId = :userId"),
    @NamedQuery(name = "Notes.findByCreatedAt", query = "SELECT n FROM Notes n WHERE n.createdAt = :createdAt"),
    @NamedQuery(name = "Notes.findByUpdatedAt", query = "SELECT n FROM Notes n WHERE n.updatedAt = :updatedAt"),
    @NamedQuery(name = "Notes.findByEventId", query = "SELECT n FROM Notes n WHERE n.eventId = :eventId"),
    @NamedQuery(name = "Notes.findByNoteType", query = "SELECT n FROM Notes n WHERE n.noteType = :noteType")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "note",
    "struckthrough",
    "userId",
    "createdAt",
    "updatedAt",
    "eventId",
    "noteType"
})
@XmlRootElement(name = "notes")
public class Notes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notes_id_seq")
    @SequenceGenerator(name = "notes_id_seq", sequenceName = "notes_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "note")
    @XmlElement(name = "note", required = true)
    private String note;
    @Column(name = "struckthrough")
    @XmlElement(name = "struckthrough", required = true)
    private Boolean struckthrough;
    @Column(name = "user_id")
    @XmlElement(name = "user_id", required = true)
    private Integer userId;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "event_id")
    @XmlElement(name = "event_id", required = true)
    private Integer eventId;
    @Column(name = "note_type")
    @XmlElement(name = "note_type", required = true)
    private String noteType;

    public Notes() {
    }

    public Notes(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getStruckthrough() {
        return struckthrough;
    }

    public void setStruckthrough(Boolean struckthrough) {
        this.struckthrough = struckthrough;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Notes)) {
            return false;
        }
        Notes other = (Notes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Notes[id=" + id + "]";
    }

}
