package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "core_element")
@NamedQueries({
    @NamedQuery(name = "CoreElement.findAll", query = "SELECT ce FROM CoreElement ce ORDER BY ce.sectionId,ce.seq"),
    @NamedQuery(name = "CoreElement.findById", query = "SELECT ce FROM CoreElement ce WHERE ce.id = :id"),
    @NamedQuery(name = "CoreElement.findByTabId", query = "SELECT ce FROM CoreElement ce WHERE ce.tabId = :tabId ORDER BY ce.seq")})
public class CoreElement implements Serializable {

    public CoreElement() {
    }

    public CoreElement(CoreElement ce) {
        if (ce != null) {
            this.inputTypeId = ce.getInputTypeId();
            this.label = ce.getLabel();
            this.newLine = ce.getNewLine();
            this.required = ce.getRequired();
            this.sectionId = ce.getSectionId();
            this.seq = ce.getSeq();
            this.tabId = ce.getTabId();
            this.validator = ce.getValidator();
            this.inputSize = ce.getInputSize();
            this.maxlength = ce.getMaxlength();
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "core_element_id_seq")
    @SequenceGenerator(name = "core_element_id_seq", sequenceName = "core_element_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "core_validator")
    private String validator;
    @Column(name = "core_tab_id")
    private Integer tabId;
    @Column(name = "core_section_id")
    private Integer sectionId;
    @Column(name = "input_type_id")
    private Integer inputTypeId;
    @Column(name = "seq")
    private Integer seq;
    @Column(name = "new_line")
    private Boolean newLine;
    @Column(name = "label")
    private String label;
    @Column(name = "required")
    private Boolean required;
    @Column(name = "xpath")
    private String xpath;
    @Column(name = "input_size")
    private Integer inputSize;
    @Column(name = "maxlength")
    private Integer maxlength;
    @Column(name = "help_text")
    private String helpText;

    @Transient
    private String domId;
    @Transient
    private String value;
    @Transient
    private Boolean modified;

    public Boolean getHasValidator() {
        Boolean has = false;
        if (this.validator != null && this.validator.trim().length() > 0) {
            has = true;
        }
        return has;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDomId() {
        return domId;
    }

    public void setDomId(String domId) {
        this.domId = domId;
    }

    public String getPropertyName() {
        String propName = null;
        if (this.xpath != null && this.xpath.lastIndexOf("/") > 0) {
            propName = this.xpath.substring(this.xpath.lastIndexOf("/")+1);
        }
        return propName;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public Integer getInputSize() {
        return inputSize;
    }

    public void setInputSize(Integer inputSize) {
        this.inputSize = inputSize;
    }

    public Integer getMaxlength() {
        return maxlength;
    }

    public void setMaxlength(Integer maxlength) {
        this.maxlength = maxlength;
    }

    public String getValidator() {
        return validator;
    }

    public void setValidator(String validator) {
        this.validator = validator;
    }

    public Integer getTabId() {
        return tabId;
    }

    public void setTabId(Integer tabId) {
        this.tabId = tabId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Boolean getNewLine() {
        return newLine;
    }

    public void setNewLine(Boolean newLine) {
        this.newLine = newLine;
    }

    public Integer getInputTypeId() {
        return this.inputTypeId;
    }

    public void setInputTypeId(Integer inputTypeId) {
        this.inputTypeId = inputTypeId;
    }

    public String getValue() {
        if (this.inputTypeId != null && this.value != null
                && (this.inputTypeId.equals(2) || this.inputTypeId.equals(3))) {

            try {
                //Fri Dec 27 00:00:00 MST 2013 //"EEE MMM d HH:mm:ss Z yyyy"
                SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
                SimpleDateFormat formatterFromDb = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                formatter.setLenient(false);
                Date dateIn = null;
                try {
                    dateIn = formatterFromDb.parse(this.value);
                } catch (ParseException pe) {
                }
                if (dateIn == null) {
                    try {
                        dateIn = formatter.parse(this.value);
                    } catch (ParseException pe) {
                    }
                }
                if (dateIn != null) {
                    SimpleDateFormat dateFormatterOut = new SimpleDateFormat(this.getDatePattern());
                    String dateOutString = dateFormatterOut.format(dateIn);
                    if (dateOutString != null) {
                        this.value = dateOutString;
                    }
                }

            } catch (Exception e) {
                System.out.print(e);
            }
        }
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDatePattern() {
        String pattern = null;
        if (this.inputTypeId == 2) {
            pattern = "yyyy-MM-dd";
        } else if (this.inputTypeId == 3) {
            pattern = "yyyy-MM-dd HH:mm";
        }
        return pattern;
    }

    public Boolean isModified() {
        return modified;
    }

    public void setModified(Boolean modified) {
        this.modified = modified;
    }

    @Override
    public CoreElement clone() throws CloneNotSupportedException {
        return (CoreElement) super.clone();
    }

}
