package gov.utah.health.model.trisano;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

@Entity
@Table(name = "outbreak_report_sections")
@NamedQueries({
    @NamedQuery(name = "OutbreakReportSection.findAll", query = "SELECT o FROM OutbreakReportSection o"),
    @NamedQuery(name = "OutbreakReportSection.findById", query = "SELECT o FROM OutbreakReportSection o WHERE o.id = :id"),
    @NamedQuery(name = "OutbreakReportSection.findByOutbreakId", query = "SELECT o FROM OutbreakReportSection o WHERE o.outbreakId =:outbreakId ORDER BY o.reportSectionId ASC"),
    @NamedQuery(name = "OutbreakReportSection.findByReportSectionId", query = "SELECT o FROM OutbreakReportSection o WHERE o.reportSectionId = :reportSectionId"),
    @NamedQuery(name = "OutbreakReportSection.findByOutbereakIdAndReportSectionId", query = "SELECT o FROM OutbreakReportSection o WHERE o.outbreakId = :outbreakId AND o.reportSectionId = :reportSectionId"),
    @NamedQuery(name = "OutbreakReportSection.findByUserId", query = "SELECT o FROM OutbreakReportSection o WHERE o.userId =:userId")
})
/**
 * @author UDOH
 */
public class OutbreakReportSection {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "outbreak_report_sections_id")
    @SequenceGenerator(name = "outbreak_report_sections_id", sequenceName = "outbreak_report_sections_id", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "outbreak_id")
    private Integer outbreakId;
    @Column(name = "report_section_id")
    private Integer reportSectionId;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "report_text")
    private String reportText;
    @Transient
    private ReportSection section;

    
    public OutbreakReportSection() {
    }
    public OutbreakReportSection(Integer outbreakId, Integer reportSectionId, Integer userId, Date createdAt, String reportText) {
        this.outbreakId = outbreakId;
        this.reportSectionId = reportSectionId;
        this.userId = userId;
        this.createdAt = createdAt;
        this.reportText = reportText;
    }
    public OutbreakReportSection(Integer outbreakId, ReportSection section) {
        this.outbreakId = outbreakId;
        this.reportSectionId = section.getId();
        this.section = section;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getOutbreakId() {
        return outbreakId;
    }

    public void setOutbreakId(Integer outbreakId) {
        this.outbreakId = outbreakId;
    }

    public Integer getReportSectionId() {
        return reportSectionId;
    }

    public void setReportSectionId(Integer reportSectionId) {
        this.reportSectionId = reportSectionId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public ReportSection getSection() {
        return section;
    }

    public void setSection(ReportSection section) {
        this.section = section;
    }

    public String getReportText() {
        return reportText;
    }

    public void setReportText(String reportText) {
        this.reportText = reportText;
    }
    
    
}
