package gov.utah.health.model.trisano;

import java.util.Date;

/**
 * @author UDOH
 */
public class OutbreakJurisdictionCount {

    private Date begin;
    private Date end;
    private Places jurisdiction;
    private Integer femaleCases;
    private Integer maleCases;
    private Integer unknownGenderCases;
    private Integer nullGenderCases;
    private Integer confirmedCases;
    private Outbreak outbreak;

    public OutbreakJurisdictionCount(Places j, Integer fCases, Integer mCases, Integer uCases, Integer nCases, Integer nConfirmed) {
        this.jurisdiction = j;
        this.femaleCases = fCases;
        this.maleCases = mCases;
        this.unknownGenderCases = uCases;
        this.nullGenderCases = nCases;
        this.confirmedCases = nConfirmed;
    }

    public OutbreakJurisdictionCount() {
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Places getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Places jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public Integer getFemaleCases() {
        return femaleCases;
    }

    public void setFemaleCases(Integer femaleCases) {
        this.femaleCases = femaleCases;
    }

    public Integer getMaleCases() {
        return maleCases;
    }

    public void setMaleCases(Integer maleCases) {
        this.maleCases = maleCases;
    }

    public Integer getUnknownGenderCases() {
        return unknownGenderCases;
    }

    public void setUnknownGenderCases(Integer unknownGenderCases) {
        this.unknownGenderCases = unknownGenderCases;
    }

    public Integer getNullGenderCases() {
        return nullGenderCases;
    }

    public void setNullGenderCases(Integer nullGenderCases) {
        this.nullGenderCases = nullGenderCases;
    }

    public Integer getJursidictionTotal() {

        Integer total = 0;
        if (this.femaleCases != null) {
            total = total + this.femaleCases;
        }
        if (this.maleCases != null) {
            total = total + this.maleCases;
        }
        if (this.unknownGenderCases != null) {
            total = total + this.unknownGenderCases;
        }
        if (this.nullGenderCases != null) {
            total = total + this.nullGenderCases;
        }

        return total;
    }

    public Integer getConfirmedCases() {
        return confirmedCases;
    }

    public void setConfirmedCases(Integer confirmedCases) {
        this.confirmedCases = confirmedCases;
    }

   
    
    public Outbreak getOutbreak() {
        return outbreak;
    }

    public void setOutbreak(Outbreak outbreak) {
        this.outbreak = outbreak;
    }
}
