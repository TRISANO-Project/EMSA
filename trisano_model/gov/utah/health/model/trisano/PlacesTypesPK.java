package gov.utah.health.model.trisano;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Embeddable
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "placeId",
    "typeId"
})
@XmlRootElement(name = "serial_version_uid")
public class PlacesTypesPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "place_id")
    @XmlElement(name = "place_id", required = true)
    private int placeId;
    @Basic(optional = false)
    @Column(name = "type_id")
    @XmlElement(name = "type_id", required = true)
    private int typeId;

    public PlacesTypesPK() {
    }

    public PlacesTypesPK(int placeId, int typeId) {
        this.placeId = placeId;
        this.typeId = typeId;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) placeId;
        hash += (int) typeId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PeopleRacesPK)) {
            return false;
        }
        PlacesTypesPK other = (PlacesTypesPK) object;
        if (this.typeId != other.typeId) {
            return false;
        }
        if (this.placeId != other.placeId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.PlacesTypesPK[placeId=" + placeId + ", typeId=" + typeId + "]";
    }
}
