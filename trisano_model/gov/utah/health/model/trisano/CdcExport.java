package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "startWeek",
    "startYear",
    "endWeek",
    "endYear",
    "lastExport",
    "diseaseCriteria"
})
@XmlRootElement(name = "cdc_export")
public class CdcExport {

    @XmlElement(name = "start_week", required = true)
    private Integer startWeek;
    @XmlElement(name = "start_year", required = true)
    private Integer startYear;
    @XmlElement(name = "end_week", required = true)
    private Integer endWeek;
    @XmlElement(name = "end_year", required = true)
    private Integer endYear;
    @XmlElement(name = "last_export", required = true)
    private Date lastExport;
    @XmlElement(name = "disease_criteria", required = true)
    private List<DiseaseCriteria> diseaseCriteria;

    public Date getLastExport() {
        return lastExport;
    }

    public void setLastExport(Date lastExport) {
        this.lastExport = lastExport;
    }

    public Integer getEndWeek() {
        return endWeek;
    }

    public void setEndWeek(Integer endWeek) {
        this.endWeek = endWeek;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }

    public Integer getStartWeek() {
        return startWeek;
    }

    public void setStartWeek(Integer startWeek) {
        this.startWeek = startWeek;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public List<DiseaseCriteria> getDiseaseCriteria() {
        return diseaseCriteria;
    }

    public void setDiseaseCriteria(List<DiseaseCriteria> diseaseCriteria) {
        this.diseaseCriteria = diseaseCriteria;
    }
    public void addDiseaseCriteria(DiseaseCriteria diseaseCriteria) {
    	if(this.diseaseCriteria == null){
    		this.diseaseCriteria = new ArrayList<DiseaseCriteria>();
    	}
        this.diseaseCriteria.add(diseaseCriteria);
    }



}
