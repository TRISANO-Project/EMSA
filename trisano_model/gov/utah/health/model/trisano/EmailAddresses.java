/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "email_addresses")
@NamedQueries({
    @NamedQuery(name = "EmailAddresses.findAll", query = "SELECT e FROM EmailAddresses e"),
    @NamedQuery(name = "EmailAddresses.findById", query = "SELECT e FROM EmailAddresses e WHERE e.id = :id"),
    @NamedQuery(name = "EmailAddresses.findByOwnerIdAndOwnerType", query = "SELECT e FROM EmailAddresses e WHERE e.ownerId = :ownerId AND e.ownerType = :ownerType"),
    @NamedQuery(name = "EmailAddresses.findByEmailAddress", query = "SELECT e FROM EmailAddresses e WHERE e.emailAddress = :emailAddress"),
    @NamedQuery(name = "EmailAddresses.findByCreatedAt", query = "SELECT e FROM EmailAddresses e WHERE e.createdAt = :createdAt"),
    @NamedQuery(name = "EmailAddresses.findByUpdatedAt", query = "SELECT e FROM EmailAddresses e WHERE e.updatedAt = :updatedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "emailAddress",
    "ownerType",
    "ownerId",
    "createdAt",
    "updatedAt"
})
@XmlRootElement(name = "email_addresses")
public class EmailAddresses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "email_addresses_id_seq")
    @SequenceGenerator(name = "email_addresses_id_seq", sequenceName = "email_addresses_id_seq", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "email_address")
    @XmlElement(name = "email_address", required = true)
    private String emailAddress;
    @Column(name = "owner_type")
    @XmlElement(name = "owner_type", required = true)
    private String ownerType;
    @Column(name = "owner_id")
    @XmlElement(name = "owner_id", required = true)
    private Integer ownerId;
    @Column(name = "created_at")
    @XmlElement(name = "created_at", required = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @XmlElement(name = "updated_at", required = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public EmailAddresses() {
    }

    public EmailAddresses(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }


    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof EmailAddresses)) {
            return false;
        }
        EmailAddresses other = (EmailAddresses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.EmailAddresses[id=" + id + "]";
    }
}
