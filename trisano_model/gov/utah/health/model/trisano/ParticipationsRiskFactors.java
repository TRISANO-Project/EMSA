/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "participations_risk_factors")
@NamedQueries({
    @NamedQuery(name = "ParticipationsRiskFactors.findAll", query = "SELECT p FROM ParticipationsRiskFactors p"),
    @NamedQuery(name = "ParticipationsRiskFactors.findById", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.id = :id"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByParticipationId", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.participationId = :participationId"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByFoodHandlerId", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.foodHandlerId = :foodHandlerId"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByHealthcareWorkerId", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.healthcareWorkerId = :healthcareWorkerId"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByGroupLivingId", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.groupLivingId = :groupLivingId"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByDayCareAssociationId", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.dayCareAssociationId = :dayCareAssociationId"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByPregnantId", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.pregnantId = :pregnantId"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByPregnancyDueDate", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.pregnancyDueDate = :pregnancyDueDate"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByRiskFactors", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.riskFactors = :riskFactors"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByRiskFactorsNotes", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.riskFactorsNotes = :riskFactorsNotes"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByCreatedAt", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByUpdatedAt", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.updatedAt = :updatedAt"),
    @NamedQuery(name = "ParticipationsRiskFactors.findByOccupation", query = "SELECT p FROM ParticipationsRiskFactors p WHERE p.occupation = :occupation")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "participationId",
    "foodHandlerId",
    "healthcareWorkerId",
    "groupLivingId",
    "dayCareAssociationId",
    "pregnantId",
    "pregnancyDueDate",
    "riskFactors",
    "riskFactorsNotes",
    "createdAt",
    "updatedAt",
    "occupation"
})
@XmlRootElement(name = "participations_risk_factors")
public class ParticipationsRiskFactors implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "participation_id")
    @XmlElement(name = "participation_id", required = true)
    private Integer participationId;
    @Column(name = "food_handler_id")
    @XmlElement(name = "food_handler_id", required = true)
    private Integer foodHandlerId;
    @Column(name = "healthcare_worker_id")
    @XmlElement(name = "healthcare_worker_id", required = true)
    private Integer healthcareWorkerId;
    @Column(name = "group_living_id")
    @XmlElement(name = "group_living_id", required = true)
    private Integer groupLivingId;
    @Column(name = "day_care_association_id")
    @XmlElement(name = "day_care_association_id", required = true)
    private Integer dayCareAssociationId;
    @Column(name = "pregnant_id")
    @XmlElement(name = "pregnant_id", required = true)
    private Integer pregnantId;
    @Column(name = "pregnancy_due_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "pregnancy_due_date", required = true)
    private Date pregnancyDueDate;
    @Column(name = "risk_factors")
    @XmlElement(name = "risk_factors", required = true)
    private String riskFactors;
    @Column(name = "risk_factors_notes")
    @XmlElement(name = "risk_factors_notes", required = true)
    private String riskFactorsNotes;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;
    @Column(name = "occupation")
    @XmlElement(name = "occupation", required = true)
    private String occupation;

    public ParticipationsRiskFactors() {
    }

    public ParticipationsRiskFactors(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParticipationId() {
        return participationId;
    }

    public void setParticipationId(Integer participationId) {
        this.participationId = participationId;
    }

    public Integer getFoodHandlerId() {
        return foodHandlerId;
    }

    public void setFoodHandlerId(Integer foodHandlerId) {
        this.foodHandlerId = foodHandlerId;
    }

    public Integer getHealthcareWorkerId() {
        return healthcareWorkerId;
    }

    public void setHealthcareWorkerId(Integer healthcareWorkerId) {
        this.healthcareWorkerId = healthcareWorkerId;
    }

    public Integer getGroupLivingId() {
        return groupLivingId;
    }

    public void setGroupLivingId(Integer groupLivingId) {
        this.groupLivingId = groupLivingId;
    }

    public Integer getDayCareAssociationId() {
        return dayCareAssociationId;
    }

    public void setDayCareAssociationId(Integer dayCareAssociationId) {
        this.dayCareAssociationId = dayCareAssociationId;
    }

    public Integer getPregnantId() {
        return pregnantId;
    }

    public void setPregnantId(Integer pregnantId) {
        this.pregnantId = pregnantId;
    }

    public Date getPregnancyDueDate() {
        return pregnancyDueDate;
    }

    public void setPregnancyDueDate(Date pregnancyDueDate) {
        this.pregnancyDueDate = pregnancyDueDate;
    }

    public String getRiskFactors() {
        return riskFactors;
    }

    public void setRiskFactors(String riskFactors) {
        this.riskFactors = riskFactors;
    }

    public String getRiskFactorsNotes() {
        return riskFactorsNotes;
    }

    public void setRiskFactorsNotes(String riskFactorsNotes) {
        this.riskFactorsNotes = riskFactorsNotes;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ParticipationsRiskFactors)) {
            return false;
        }
        ParticipationsRiskFactors other = (ParticipationsRiskFactors) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.ParticipationsRiskFactors[id=" + id + "]";
    }
}
