package gov.utah.health.model.trisano;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HospitalizationFacilitiesAttributes", propOrder = {
    "hospitalsParticipations"
})
@XmlRootElement(name = "hospitalization_facilities_attributes")

public class HospitalizationFacilitiesAttributes extends PlaceAttributes {


   @XmlElement(name="hospitals_participations",required = true)
    private HospitalsParticipations hospitalsParticipations;


    public HospitalizationFacilitiesAttributes() {
        super.placeType = Type.HOSPITALIZATION_FACILITY.getNumber();
    }


    public HospitalsParticipations getHospitalsParticipations() {
        return hospitalsParticipations;
    }

    public void setHospitalsParticipations(HospitalsParticipations hospitalsParticipations) {
        this.hospitalsParticipations = hospitalsParticipations;
    }


}
