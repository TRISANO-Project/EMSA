/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "participations_encounters")
@NamedQueries({
    @NamedQuery(name = "ParticipationsEncounters.findAll", query = "SELECT p FROM ParticipationsEncounters p"),
    @NamedQuery(name = "ParticipationsEncounters.findById", query = "SELECT p FROM ParticipationsEncounters p WHERE p.id = :id"),
    @NamedQuery(name = "ParticipationsEncounters.findByUserId", query = "SELECT p FROM ParticipationsEncounters p WHERE p.userId = :userId"),
    @NamedQuery(name = "ParticipationsEncounters.findByEncounterDate", query = "SELECT p FROM ParticipationsEncounters p WHERE p.encounterDate = :encounterDate"),
    @NamedQuery(name = "ParticipationsEncounters.findByDescription", query = "SELECT p FROM ParticipationsEncounters p WHERE p.description = :description"),
    @NamedQuery(name = "ParticipationsEncounters.findByEncounterLocationType", query = "SELECT p FROM ParticipationsEncounters p WHERE p.encounterLocationType = :encounterLocationType"),
    @NamedQuery(name = "ParticipationsEncounters.findByCreatedAt", query = "SELECT p FROM ParticipationsEncounters p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "ParticipationsEncounters.findByUpdatedAt", query = "SELECT p FROM ParticipationsEncounters p WHERE p.updatedAt = :updatedAt")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "userId",
    "encounterDate",
    "description",
    "encounterLocationType",
    "createdAt",
    "updatedAt"
})
@XmlRootElement(name = "participations_encounters")
public class ParticipationsEncounters implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="participations_encounters_id_seq")
    @SequenceGenerator(name="participations_encounters_id_seq", sequenceName="participations_encounters_id_seq", allocationSize=1)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlElement(name = "id", required = true)
    private Integer id;
    @Column(name = "user_id")
    @XmlElement(name = "user_id", required = true)
    private Integer userId;
    @Column(name = "encounter_date")
    @Temporal(TemporalType.DATE)
    @XmlElement(name = "encounter_date", required = true)
    private Date encounterDate;
    @Column(name = "description")
    @XmlElement(name = "description", required = true)
    private String description;
    @Column(name = "encounter_location_type")
    @XmlElement(name = "encounter_location_type", required = true)
    private String encounterLocationType;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "created_at", required = true)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = "updated_at", required = true)
    private Date updatedAt;

    public ParticipationsEncounters() {
    }

    public ParticipationsEncounters(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getEncounterDate() {
        return encounterDate;
    }

    public void setEncounterDate(Date encounterDate) {
        this.encounterDate = encounterDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEncounterLocationType() {
        return encounterLocationType;
    }

    public void setEncounterLocationType(String encounterLocationType) {
        this.encounterLocationType = encounterLocationType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof ParticipationsEncounters)) {
            return false;
        }
        ParticipationsEncounters other = (ParticipationsEncounters) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.ParticipationsEncounters[id=" + id + "]";
    }
}
