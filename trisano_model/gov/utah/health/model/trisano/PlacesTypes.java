package gov.utah.health.model.trisano;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "places_types")
@NamedQueries({
    @NamedQuery(name = "PlacesTypes.findAll", query = "SELECT p FROM PlacesTypes p"),
    @NamedQuery(name = "PlacesTypes.findByPlaceId", query = "SELECT p FROM PlacesTypes p WHERE p.placesTypesPK.placeId = :placeId"),
    @NamedQuery(name = "PlacesTypes.findByTypeId", query = "SELECT p FROM PlacesTypes p WHERE p.placesTypesPK.typeId = :typeId")
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "placesTypesPK"
})
@XmlRootElement(name = "places_types")
public class PlacesTypes implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    @XmlElement(name = "serial_version_uid", required = true)
    protected PlacesTypesPK placesTypesPK;
  

    public PlacesTypes() {
    }

    public PlacesTypes(PlacesTypesPK PlacesTypesPK) {
        this.placesTypesPK = PlacesTypesPK;
    }

    public PlacesTypes(int placeId, int typeId) {
        this.placesTypesPK = new PlacesTypesPK(placeId, typeId);
    }

    public PlacesTypesPK getPlacesTypesPK() {
        return placesTypesPK;
    }

    public void setPlacesTypesPK(PlacesTypesPK PlacesTypesPK) {
        this.placesTypesPK = PlacesTypesPK;
    }

   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (placesTypesPK != null ? placesTypesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof PlacesTypes)) {
            return false;
        }
        PlacesTypes other = (PlacesTypes) object;
        if ((this.placesTypesPK == null && other.placesTypesPK != null) || (this.placesTypesPK != null && !this.placesTypesPK.equals(other.placesTypesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.sqlAgent.trisano.PlacesTypes[PlacesTypesPK=" + placesTypesPK + "]";
    }
}
