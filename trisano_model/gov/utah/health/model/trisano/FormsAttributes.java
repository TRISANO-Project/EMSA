
package gov.utah.health.model.trisano;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "form",
    "questions",
    "diseasesForms",
    "formElements",
    "includeQuestions"
})
@XmlRootElement(name = "forms_attributes")

public class FormsAttributes {

    @XmlElement(name="forms", required = true)
    private Forms form;
    @XmlElement(name="questions",required = true)
    private List<Questions> questions;
    @XmlElement(name="diseasesForms",required = true)
    private List<DiseasesForms> diseasesForms;
    @XmlElement(name="formElements",required = true)
    private List<FormElements> formElements;
    @XmlElement(name="include_questions",required = true)
    private Boolean includeQuestions;

    public Forms getForm() {
        return form;
    }

    public void setForm(Forms form) {
        this.form = form;
    }

    public Boolean getIncludeQuestions() {
        return includeQuestions;
    }

    public void setIncludeQuestions(Boolean includeQuestions) {
        this.includeQuestions = includeQuestions;
    }

    public List<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }

    public List<DiseasesForms> getDiseasesForms() {
        return diseasesForms;
    }

    public void setDiseasesForms(List<DiseasesForms> diseasesForms) {
        this.diseasesForms = diseasesForms;
    }

    public List<FormElements> getFormElements() {
        return formElements;
    }

    public void setFormElements(List<FormElements> formElements) {
        this.formElements = formElements;
    }



}
