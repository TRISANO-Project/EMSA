package gov.utah.health.model.trisano;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity composite class.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "event",
    "person",
    "interestedParty",
    "jurisdictionAttributes",
    "labsAttributes",
    "reportingAgencyAttributes",
    "hospitalFacilityAttributes",
    "cliniciansAttributes",
    "reporterAttributes",
    "diseases",
    "disease",
    "diseaseEvents",
    "jurisdiction",
    "eventAddresses",
    "labResults"
})
@XmlRootElement(name = "event_attributes")

public class EventAttributes {

    @XmlElement(name = "event", required = true)
    private Events event;
    @XmlElement(name = "person", required = true)
    private People person;
    @XmlElement(name = "interested_party_attributes", required = true)
    private InterestedPartyAttributes interestedParty;
    @XmlElement(name = "jurisdiction_attributes", required = true)
    private JurisdictionAttributes jurisdictionAttributes;
    @XmlElement(name = "labs_attributes", required = true)
    private List<LabsAttributes> labsAttributes;
    @XmlElement(name = "reporting_agency_attributes", required = true)
    private List<ReportingAgencyAttributes> reportingAgencyAttributes;
    @XmlElement(name = "hospital_facility_attributes", required = true)
    private List<HospitalizationFacilitiesAttributes> hospitalFacilityAttributes;
    @XmlElement(name = "clinicians_attributes", required = true)
    private List<CliniciansAttributes> cliniciansAttributes;
    @XmlElement(name = "reporter_attributes", required = true)
    private List<ReporterAttributes> reporterAttributes;
    @XmlElement(name = "diseases", required = true)
    private List<Diseases> diseases;
    @XmlElement(name = "disease", required = true)
    private Diseases disease;
    @XmlElement(name = "disease_events", required = true)
    private List<DiseaseEvents> diseaseEvents;
    @XmlElement(name = "jurisdiction", required = true)
    private Places jurisdiction;
    @XmlElement(name = "event_addresses", required = true)
    private List<Addresses> eventAddresses;
    @XmlElement(name = "lab_results", required = true)
    private List<LabResults> labResults;
    @XmlTransient
    private UserAccess userAccess;

    public UserAccess getUserAccess() {
        return userAccess;
    }

    public void setUserAccess(UserAccess userAccess) {
        this.userAccess = userAccess;
    }

    public List<DiseaseEvents> getDiseaseEvents() {
        return diseaseEvents;
    }

    public void setDiseaseEvents(List<DiseaseEvents> diseaseEvents) {
        this.diseaseEvents = diseaseEvents;
    }

    public void addDiseaseEvents(DiseaseEvents diseaseEvents) {
        if (this.diseaseEvents == null) {
            this.diseaseEvents = new ArrayList<DiseaseEvents>();
        }
        this.diseaseEvents.add(diseaseEvents);
    }

    public List<LabsAttributes> getLabsAttributes() {
        return labsAttributes;
    }

    public void setLabsAttributes(List<LabsAttributes> labsAttributes) {
        this.labsAttributes = labsAttributes;
    }

    public void addLabsAttributes(LabsAttributes labsAttributes) {
        if (this.labsAttributes == null) {
            this.labsAttributes = new ArrayList<LabsAttributes>();
        }
        this.labsAttributes.add(labsAttributes);
    }

    public List<Diseases> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<Diseases> diseases) {
        this.diseases = diseases;
    }

    public void addDiseases(List<Diseases> diseases) {
        if (this.diseases == null) {
            this.diseases = new ArrayList<Diseases>();
        }
        this.diseases.addAll(diseases);
    }

    public List<CliniciansAttributes> getCliniciansAttributes() {
        return cliniciansAttributes;
    }

    public void setCliniciansAttributes(List<CliniciansAttributes> cliniciansAttributes) {
        this.cliniciansAttributes = cliniciansAttributes;
    }

    public void addCliniciansAttributes(CliniciansAttributes cliniciansAttributes) {
        if (this.cliniciansAttributes == null) {
            this.cliniciansAttributes = new ArrayList<CliniciansAttributes>();
        }
        this.cliniciansAttributes.add(cliniciansAttributes);
    }

    public List<ReporterAttributes> getReporterAttributes() {
        return reporterAttributes;
    }

    public void setReporterAttributes(List<ReporterAttributes> reporterAttributes) {
        this.reporterAttributes = reporterAttributes;
    }

    public void addReporterAttributes(ReporterAttributes reporterAttributes) {
        if (this.reporterAttributes == null) {
            this.reporterAttributes = new ArrayList<ReporterAttributes>();
        }
        this.reporterAttributes.add(reporterAttributes);
    }

    public List<ReportingAgencyAttributes> getReportingAgencyAttributes() {
        return reportingAgencyAttributes;
    }

    public void setReportingAgencyAttributes(List<ReportingAgencyAttributes> reportingAgencyAttributes) {
        this.reportingAgencyAttributes = reportingAgencyAttributes;
    }

    public void addReportingAgencyAttributes(ReportingAgencyAttributes reportingAgencyAttributes) {
        if (this.reportingAgencyAttributes == null) {
            this.reportingAgencyAttributes = new ArrayList<ReportingAgencyAttributes>();
        }
        this.reportingAgencyAttributes.add(reportingAgencyAttributes);
    }

    public List<HospitalizationFacilitiesAttributes> getHospitalFacilityAttributes() {
        return hospitalFacilityAttributes;
    }

    public void setHospitalFacilityAttributes(List<HospitalizationFacilitiesAttributes> hospitalFacilityAttributes) {
        this.hospitalFacilityAttributes = hospitalFacilityAttributes;
    }

    public void addHospitalFacilityAttributes(HospitalizationFacilitiesAttributes hospitalFacilityAttributes) {
        if (this.hospitalFacilityAttributes == null) {
            this.hospitalFacilityAttributes = new ArrayList<HospitalizationFacilitiesAttributes>();
        }
        this.hospitalFacilityAttributes.add(hospitalFacilityAttributes);
    }

    public JurisdictionAttributes getJurisdictionAttributes() {
        return jurisdictionAttributes;
    }

    public void setJurisdictionAttributes(JurisdictionAttributes jurisdictionAttributes) {
        this.jurisdictionAttributes = jurisdictionAttributes;
    }

    public InterestedPartyAttributes getInterestedParty() {
        return interestedParty;
    }

    public void setInterestedParty(InterestedPartyAttributes interestedParty) {
        this.interestedParty = interestedParty;
    }

    public Boolean getCanEdit() {
        return Boolean.TRUE;
    }

    public List<LabResults> getLabResults() {
        return labResults;
    }

    public void setLabResults(List<LabResults> labResults) {
        this.labResults = labResults;
    }

    public void addLabResults(List<LabResults> labResults) {
        if (this.labResults == null) {
            this.labResults = new ArrayList<LabResults>();
        }
        this.labResults.addAll(labResults);
    }

    public List<Addresses> getEventAddresses() {
        return eventAddresses;
    }

    public void setEventAddresses(List<Addresses> eventAddresses) {
        this.eventAddresses = eventAddresses;
    }

    public Places getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(Places jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public Events getEvent() {
        return event;
    }

    public void setEvent(Events event) {
        this.event = event;
    }

    public People getPerson() {
        return person;
    }

    public void setPerson(People person) {
        this.person = person;
    }

    public Diseases getDisease() {
        return disease;
    }

    public void setDisease(Diseases disease) {
        this.disease = disease;
    }

    public Addresses getCurrentAddress() {
        Addresses a = null;
        if (this.getInterestedParty() != null && this.getInterestedParty().getPersonAddresses() != null && 
                !this.getInterestedParty().getPersonAddresses().isEmpty()) {
            a = this.getInterestedParty().getPersonAddresses().get(0);
        }
        return a;
    }
    public Addresses getEventAddress() {
        Addresses a = null;
        if (this.getEventAddresses() != null && 
                !this.getEventAddresses().isEmpty()) {
            a = this.getEventAddresses().get(0);
        }
        return a;
    }


    public Boolean hasVerifiedCurrentAddress() {
        Boolean has = false;
        Addresses addy = this.getCurrentAddress();
        if (addy != null) {
            if (addy.getLatitude() != null && addy.getLatitude() > 0
                    && addy.getLongitude() != null && addy.getLongitude() > 0) {
                has = true;
            }
        }
        return has;
    }

    public Boolean hasVerifiedEventAddress() {
        Boolean has = false;
        if (this.getEventAddresses() != null && !this.getEventAddresses().isEmpty()) {
            Addresses addy = this.getEventAddresses().get(0);
            if (addy.getLatitude() != null && addy.getLatitude() > 0
                    && addy.getLongitude() != null && addy.getLongitude() > 0) {
                has = true;
            }
        }
        return has;
    }
}
