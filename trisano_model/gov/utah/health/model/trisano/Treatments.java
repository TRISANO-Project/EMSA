/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.utah.health.model.trisano;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is a Trisano entity class.
 *
 * @author UDOH
 */
@Entity
@Table(name = "treatments")
@NamedQueries({
    @NamedQuery(name = "Treatments.findAll", query = "SELECT t FROM Treatments t"),
    @NamedQuery(name = "Treatments.findById", query = "SELECT t FROM Treatments t WHERE t.id = :id"),
    @NamedQuery(name = "Treatments.findByTreatmentTypeId", query = "SELECT t FROM Treatments t WHERE t.treatmentTypeId = :treatmentTypeId"),
    @NamedQuery(name = "Treatments.findByTreatmentName", query = "SELECT t FROM Treatments t WHERE t.treatmentName like :treatmentName")})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "treatmentTypeId",
    "treatmentName"
    
})
@XmlRootElement(name = "treatments")
public class Treatments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    @XmlElement(required = true)
    private Integer id;
    @Column(name = "treatment_type_id")
    @XmlElement(name="treatment_type_id",required = true)
    private Integer treatmentTypeId;
    @Column(name = "treatment_name")
    @XmlElement(name="treatment_name",required = true)
    private String treatmentName;
    
    public Treatments() {
    }

    public Treatments(Integer id) {
        this.id = id;
    }

    public Treatments(Integer id, Integer treatmentTypeId, String treatmentName) {
        this.id = id;
        this.treatmentTypeId = treatmentTypeId;
        this.treatmentName = treatmentName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTreatmentTypeId() {
        return treatmentTypeId;
    }

    public void setTreatmentTypeId(Integer treatmentTypeId) {
        this.treatmentTypeId = treatmentTypeId;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Treatments)) {
            return false;
        }
        Treatments other = (Treatments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "gov.utah.health.model.trisano.Treatments[id=" + id + "]";
    }

}
