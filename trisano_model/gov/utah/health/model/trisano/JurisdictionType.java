package gov.utah.health.model.trisano;

import gov.utah.health.util.AppProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
@ManagedBean(name = "jb")
@SessionScoped

public class JurisdictionType implements Serializable  {

    private static final AppProperties props = AppProperties.getInstance();
    public static final JurisdictionType STATE_UTAH_STATE = new JurisdictionType(77, "Utah State");
    public static final JurisdictionType STATE_OUT_OF_STATE = new JurisdictionType(props.getIntProperty("out_of_state_jurisdiction_id"), "Out of State");
    public static final List<JurisdictionType> state_types = new ArrayList<JurisdictionType>();

    static {
        state_types.add(STATE_UTAH_STATE);
        state_types.add(STATE_OUT_OF_STATE);

    }

    JurisdictionType(Integer id, String label) {
        this.id = id;
        this.label = label;
    }
    private Integer id;
    private String label;

    public static Boolean getStateType(Integer id) {
        Boolean is = false;
        for (JurisdictionType t : JurisdictionType.state_types) {
            if (t.getId().equals(id)) {
                is = true;
                break;
            }
        }
        return is;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean equals(JurisdictionType t) {

        boolean equals = false;
        if (t.getId().equals(t.getId())) {
            equals = true;
        }

        return equals;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
