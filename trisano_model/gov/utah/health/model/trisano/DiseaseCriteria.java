package gov.utah.health.model.trisano;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "diseaseId",
    "stateCaseStatusId"
})
@XmlRootElement(name = "disease_criteria")
public class DiseaseCriteria {
	public DiseaseCriteria(int disease_id, int status_id){
		this.diseaseId = disease_id;
		this.stateCaseStatusId = status_id;
	}
	public DiseaseCriteria(){
		
	}

    @XmlElement(name = "disease_id", required = true)
    private Integer diseaseId;
    @XmlElement(name = "state_case_status_id", required = true)
    private Integer stateCaseStatusId;

    public Integer getDiseaseId() {
        return diseaseId;
    }

    public void setDiseaseId(Integer diseaseId) {
        this.diseaseId = diseaseId;
    }

    public Integer getStateCaseStatusId() {
        return stateCaseStatusId;
    }

    public void setStateCaseStatusId(Integer stateCaseStatusId) {
        this.stateCaseStatusId = stateCaseStatusId;
    }

}
