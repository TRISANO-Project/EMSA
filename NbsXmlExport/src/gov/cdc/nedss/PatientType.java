//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.06.20 at 05:51:06 AM PDT 
//


package gov.cdc.nedss;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PatientType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PatientType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendingApplicationPatientIdentifier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.cdc.gov/NEDSS}NameType"/>
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="ReportedAge" type="{http://www.cdc.gov/NEDSS}NumericType" minOccurs="0"/>
 *         &lt;element name="Sex" type="{http://www.cdc.gov/NEDSS}CodedType" minOccurs="0"/>
 *         &lt;element name="Race" type="{http://www.cdc.gov/NEDSS}CodedType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Ethnicity" type="{http://www.cdc.gov/NEDSS}CodedType" minOccurs="0"/>
 *         &lt;element name="PostalAddress" type="{http://www.cdc.gov/NEDSS}PostalAddressType" minOccurs="0"/>
 *         &lt;element name="Telephone" type="{http://www.cdc.gov/NEDSS}TelephoneType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientType", propOrder = {
    "sendingApplicationPatientIdentifier",
    "name",
    "dateOfBirth",
    "reportedAge",
    "sex",
    "race",
    "ethnicity",
    "postalAddress",
    "telephone"
})
public class PatientType {

    @XmlElement(name = "SendingApplicationPatientIdentifier", required = true)
    protected String sendingApplicationPatientIdentifier;
    @XmlElement(name = "Name", required = true)
    protected NameType name;
    @XmlElement(name = "DateOfBirth")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(name = "ReportedAge")
    protected NumericType reportedAge;
    @XmlElement(name = "Sex")
    protected CodedType sex;
    @XmlElement(name = "Race")
    protected List<CodedType> race;
    @XmlElement(name = "Ethnicity")
    protected CodedType ethnicity;
    @XmlElement(name = "PostalAddress")
    protected PostalAddressType postalAddress;
    @XmlElement(name = "Telephone")
    protected TelephoneType telephone;

    /**
     * Gets the value of the sendingApplicationPatientIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendingApplicationPatientIdentifier() {
        return sendingApplicationPatientIdentifier;
    }

    /**
     * Sets the value of the sendingApplicationPatientIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendingApplicationPatientIdentifier(String value) {
        this.sendingApplicationPatientIdentifier = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link NameType }
     *     
     */
    public NameType getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameType }
     *     
     */
    public void setName(NameType value) {
        this.name = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the reportedAge property.
     * 
     * @return
     *     possible object is
     *     {@link NumericType }
     *     
     */
    public NumericType getReportedAge() {
        return reportedAge;
    }

    /**
     * Sets the value of the reportedAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumericType }
     *     
     */
    public void setReportedAge(NumericType value) {
        this.reportedAge = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link CodedType }
     *     
     */
    public CodedType getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedType }
     *     
     */
    public void setSex(CodedType value) {
        this.sex = value;
    }

    /**
     * Gets the value of the race property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the race property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRace().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodedType }
     * 
     * 
     */
    public List<CodedType> getRace() {
        if (race == null) {
            race = new ArrayList<CodedType>();
        }
        return this.race;
    }

    /**
     * Gets the value of the ethnicity property.
     * 
     * @return
     *     possible object is
     *     {@link CodedType }
     *     
     */
    public CodedType getEthnicity() {
        return ethnicity;
    }

    /**
     * Sets the value of the ethnicity property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedType }
     *     
     */
    public void setEthnicity(CodedType value) {
        this.ethnicity = value;
    }

    /**
     * Gets the value of the postalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link PostalAddressType }
     *     
     */
    public PostalAddressType getPostalAddress() {
        return postalAddress;
    }

    /**
     * Sets the value of the postalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostalAddressType }
     *     
     */
    public void setPostalAddress(PostalAddressType value) {
        this.postalAddress = value;
    }

    /**
     * Gets the value of the telephone property.
     * 
     * @return
     *     possible object is
     *     {@link TelephoneType }
     *     
     */
    public TelephoneType getTelephone() {
        return telephone;
    }

    /**
     * Sets the value of the telephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelephoneType }
     *     
     */
    public void setTelephone(TelephoneType value) {
        this.telephone = value;
    }

}
