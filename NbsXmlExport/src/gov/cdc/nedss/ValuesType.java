//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.06.20 at 05:51:06 AM PDT 
//


package gov.cdc.nedss;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValuesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValuesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Numeric" type="{http://www.cdc.gov/NEDSS}NumericType" minOccurs="0"/>
 *         &lt;element name="Coded" type="{http://www.cdc.gov/NEDSS}CodedType" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValuesType", propOrder = {
    "numeric",
    "coded",
    "text"
})
public class ValuesType {

    @XmlElement(name = "Numeric")
    protected NumericType numeric;
    @XmlElement(name = "Coded")
    protected CodedType coded;
    @XmlElement(name = "Text")
    protected String text;

    /**
     * Gets the value of the numeric property.
     * 
     * @return
     *     possible object is
     *     {@link NumericType }
     *     
     */
    public NumericType getNumeric() {
        return numeric;
    }

    /**
     * Sets the value of the numeric property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumericType }
     *     
     */
    public void setNumeric(NumericType value) {
        this.numeric = value;
    }

    /**
     * Gets the value of the coded property.
     * 
     * @return
     *     possible object is
     *     {@link CodedType }
     *     
     */
    public CodedType getCoded() {
        return coded;
    }

    /**
     * Sets the value of the coded property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedType }
     *     
     */
    public void setCoded(CodedType value) {
        this.coded = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

}
