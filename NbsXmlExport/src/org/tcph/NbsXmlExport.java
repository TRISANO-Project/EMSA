/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tcph;

import gov.cdc.nedss.*;
import gov.cdc.nedss.ObjectFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;
import java.sql.*;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Jay Boyer
 */
public class NbsXmlExport {

    private static boolean fExportEventRange = false;
    private static Integer nRecordNumberMin = 0, nRecordNumberMax = 0;
    private static int nError = 0;
    private static Properties properties = new Properties();
    private static Logger logger = Logger.getLogger("");
    private static Connection con;
    // maps for trisano id -> NBS code
    private static final HashMap mapRaceCode = new LinkedHashMap<Integer, String>();
    private static final HashMap mapRaceTxt = new LinkedHashMap<Integer, String>();
    private static final HashMap mapEthnicityCode = new LinkedHashMap<Integer, String>();
    private static final HashMap mapEthnicityTxt = new LinkedHashMap<Integer, String>();
    private static final HashMap mapGenderCode = new LinkedHashMap<Integer, String>();
    private static final HashMap mapGenderTxt = new LinkedHashMap<Integer, String>();
    private static final HashMap mapAgeTypeCode = new LinkedHashMap<Integer, String>();
    private static final HashMap mapAgeTypeTxt = new LinkedHashMap<Integer, String>();
    private static final HashMap mapCountyCode = new LinkedHashMap<Integer, String>();
    private static final HashMap mapCountyTxt = new LinkedHashMap<Integer, String>();
    private static final HashMap mapStateCode = new LinkedHashMap<Integer, String>();
    private static final HashMap mapStateTxt = new LinkedHashMap<Integer, String>();
    private static final HashMap mapYNUCode = new LinkedHashMap<Integer, String>();
    private static final HashMap mapYNUTxt = new LinkedHashMap<Integer, String>();
    // This array maps trisano disease_ids to NBS disease names
    private static final String[][] aStrCondition = {
        {"", "Unused"},
        {"10245", "African Tick Bite Fever"},
        {"11040", "Amebiasis"},
        {"10350", "Anthrax"},
        {"10058", "Cache Valley virus neuroinvasive disease"},
        {"10066", "Cache Valley virus non-neuroinvasive disease"},
        {"10054", "California serogroup virus neuroinvasive disease"},
        {"10061", "California serogroup virus non-neuroinvasive disease"},
        {"10680", "Dengue"},
        {"11705", "Dengue, severe"},
        {"10053", "Eastern equine encephalitis virus neuroinvasive disease"},
        {"10062", "Eastern equine encephalitis virus non-neuroinvasive disease"},
        {"10059", "Japanese encephalitis virus neuroinvasive disease"},
        {"10068", "Japanese encephalitis virus non-neuroinvasive disease"},
        {"10057", "Powassan virus neuroinvasive disease"},
        {"10063", "Powassan virus non-neuroinvasive disease"},
        {"10051", "St. Louis encephalitis virus neuroinvasive disease"},
        {"10064", "St. Louis encephalitis virus non-neuroinvasive disease"},
        {"10055", "Venezuelan equine encephalitis virus neuroinvasive disease"},
        {"10067", "Venezuelan equine encephalitis virus non-neuroinvasive disease"},
        {"10052", "Western equine encephalitis virus neuroinvasive disease"},
        {"10065", "Western equine encephalitis virus non-neuroinvasive disease"},
        {"10530", "Botulism, foodborne"},
        {"10540", "Botulism, Infant"},
        {"10549", "Botulism, wound"},
        {"10548", "Botulism, other unspecified"},
        {"10020", "Brucellosis"},
        {"", "Glanders (Burkholderia mallei)"},
        {"11585", "Melioidosis"},
        {"11020", "Campylobacteriosis"},
        {"10273", "Chancroid"},
        {"10030", "Varicella (Chickenpox)"},
        {"10274", "Chlamydia trachomatis infection"},
        {"10470", "Cholera (toxigenic Vibrio cholerae O1 or O139)"},
        {"11900", "Coccidioidomycosis"},
        {"10093", "Colorado tick fever virus"},
        {"", "Creutzfeldt-Jakob Disease and Prion Diseases"},
        {"11580", "Cryptosporidiosis"},
        {"11575", "Cyclosporiasis"},
        {"10040", "Diphtheria"},
        {"", "Echinococcosis"},
        {"11090", "Anaplasma phagocytophilum"},
        {"11088", "Ehrlichia chaffeensis"},
        {"11089", "Ehrlichia ewingii"},
        {"11091", "Ehrlichiosis/Anaplasmosis,undetermined"},
        {"11570", "Giardiasis"},
        {"10280", "Gonorrhea"},
        {"10276", "Granuloma inguinale (GI)"},
        {"10590", "Haemophilus influenzae,invasive disease"},
        {"10380", "Hansen disease (Leprosy)"},
        {"11610", "Hantavirus infection,non-Hantavirus Pulmonary Syndrome"},
        {"11590", "Hantavirus pulmonary syndrome"},
        {"10110", "Hepatitis A, acute"},
        {"10100", "Hepatitis B, acute"},
        {"10105", "Hepatitis B virus infection,chronic"},
        {"10104", "Hepatitis B, perinatal infection"},
        {"10101", "Hepatitis C, acute"},
        {"10106", "Hepatitis C, chronic"},
        {"10102", "Hepatitis Delta co- or super-infection, acute (Hepatitis D)"},
        {"10103", "Hepatitis E, acute"},
        {"10120", "Hepatitis,viral unspecified"},
        {"10570", "Flu activity code (Influenza)"},
        {"11062", "Novel influenza A virus infections,initial detections of"},
        {"", "Influenza-associated death in a person under 18"},
        {"", "Influenza-associated hospitalization"},
        {"10490", "Legionellosis"},
        {"10640", "Listeriosis"},
        {"11080", "Lyme disease"},
        {"10306", "Lymphogranuloma venereum (LGV)"},
        {"10130", "Malaria"},
        {"10140", "Measles (rubeola),total"},
        {"", "Meningitis, Aseptic"},
        {"", "Meningitis, Viral"},
        {"", "Meningitis, Bacterial Other"},
        {"10150", "Meningococcal disease (Neisseria meningitidis)"},
        {"10180", "Mumps"},
        {"", "Norovirus"},
        {"10190", "Pertussis"},
        {"10440", "Plague"},
        {"10410", "Poliomyelitis, paralytic"},
        {"10405", "Poliovirus infection, nonparalytic"},
        {"10450", "Psittacosis (Ornithosis)"},
        {"10257", "Q fever, acute"},
        {"10258", "Q fever, chronic"},
        {"10340", "Rabies, animal"},
        {"10460", "Rabies, human"},
        {"", "Relapsing Fever"},
        {"", "Rocky Mountain Spotted Fever"},
        {"10200", "Rubella"},
        {"10370", "Rubella, congenital syndrome"},
        {"11000", "Salmonellosis"},
        {"10575", "Severe Acute Respiratory Syndrome (SARS)-associated Coronavirus disease (SARS-CoV)"},
        {"11010", "Shigellosis"},
        {"11800", "Smallpox"},
        {"", "Smallpox Vaccine-Associated Adverse Event"},
        {"11665", "Vancomycin-resistant Staphylococcus aureus (VRSA)"},
        {"11663", "Vancomycin-intermediate Staphylococcus aureus (VISA)"},
        {"11563", "Shiga toxin-producing Escherichia coli (STEC)"},
        {"11710", "Streptococcal disease, invasive, Group A"},
        {"11715", "Streptococcal disease, invasive, Group B"},
        {"11716", "Streptococcal disease, other, invasive, beta-hemolytic (non-group A and non-group B)"},
        {"11723", "Streptococcus pneumoniae, invasive disease (IPD) (all ages)"},
        {"11717", "Streptococcus pneumoniae, invasive disease non-drug resistant (IPD), < 5 years"},
        {"10316", "Syphilis, congenital"},
        {"10313", "Syphilis, early non-primary, non-secondary"},
        {"10314", "Syphilis, late latent"},
        {"10318", "Syphilis, late with clinical manifestations other than neurosyphilis"},
        {"10317", "Neurosyphilis"},
        {"10311", "Syphilis, primary"},
        {"10312", "Syphilis, secondary"},
        {"10315", "Syphilis, unknown latent"},
        {"10210", "Tetanus"},
        {"10270", "Trichinellosis"},
        {"10220", "Tuberculosis"},
        {"10220", "Tuberculosis"},
        {"10220", "Tuberculosis"},
        {"10220", "Tuberculosis"},
        {"", "Tuberculosis, suspect"},
        {"10230", "Tularemia"},
        {"10240", "Typhoid fever (caused by Salmonella typhi)"},
        {"11540", "Vibrio spp., non-toxigenic, other or unspecified"},
        {"10049", "West Nile virus neuroinvasive disease"},
        {"10056", "West Nile virus non-neuroinvasive disease"},
        {"10660", "Yellow Fever"},
        {"11565", "Yersiniosis"},
        {"10070", "Encephalitis, post-chickenpox"},
        {"10080", "Encephalitis, post-mumps"},
        {"10050", "Encephalitis, primary"},
        {"10090", "Encephalitis, post-other"},
        {"10520", "Toxic-shock syndrome (staphylococcal)"},
        {"10568", "Human T-Lymphotropic virus type I infection (HTLV-I)"},
        {"10569", "Human T-Lymphotropic virus type II infection (HTLV-II)"},
        {"10560", "AIDS"},
        {"32010", "Lead poisoning"},
        {"11801", "Monkeypox"},
        {"10308", "Mucopurulent cervicitis (MPC)"},
        {"10562", "HIV Infection, adult"},
        {"10561", "HIV Infection, pediatric"},
        {"10307", "Nongonococcal urethritis (NGU)"},
        {"11550", "Hemolytic uremic syndrome postdiarrheal"},
        {"", "Pelvic Inflammatory Disease (PID), Unknown Etiology"},
        {"11661", "Methicillin- or oxicillin- resistant Staphylococcus aureus coagulase-positive (MRSA a.k.a. ORSA)"},
        {"", " Hepatitis B Pregnancy Event"},
        {"", "Syphilis, reactor"},
        {"", " Animal Bite"},
        {"", " Arsenic poisoning"},
        {"12010", "Babesiosis"},
        {"", " Birth Defects-Zika"},
        {"", "Chagas, acute"},
        {"10073", "Chikungunya, neuroinvasive disease"},
        {"10073", "Chikungunya, non-neuroinvasive disease"},
        {"", "Dermatologic Illness, Unspecified"},
        {"11630", "Ebola hemorrhagic fever"},
        {"", "Exposure, Bloodborne Pathogen"},
        {"", "Exposure, Chemical or Biological"},
        {"", "Gastroenteritis, Unspecified"},
        {"", "Influenza, RIDT"},
        {"", " HIV pediatric seroreverter"},
        {"", " HIV perinatal exposure"},
        {"", " Invasive Pneumococcal Disease"},
        {"10078", "Jamestown Canyon virus, neuroinvasive disease"},
        {"10079", "Jamestown Canyon virus, non-neuroinvasive disease"},
        {"10081", "LaCrosse virus neuroinvasive disease"},
        {"10082", "LaCrosse virus non-neuroinvasive disease"},
        {"", " Leishmaniasis"},
        {"10390", "Leptospirosis"},
        {"", "Listeria, pregnancy related"},
        {"", " Measles Exposure"},
        {"", "Meningitis, Fungal"},
        {"11064", "Middle East Respiratory Syndrome Coronavirus (MERS-CoV) Infection"},
        {"", "Neurologic Illness, Unspecified"},
        {"", " RSV"},
        {"", "Rabies, exposure to a rabies susceptible animal"},
        {"", "Respiratory Illness, Unspecified"},
        {"", " Rotavirus"},
        {"", " STD testing"},
        {"", " STD/HIV Co-Infection"},
        {"", " STD/HIV/TB Co-Infection"},
        {"10250", "Spotted Fever Rickettsiosis"},
        {"11700", "Streptococcal Toxic Shock Syndrome"},
        {"", " TB Testing"},
        {"", "Tuberculosis, Assmt Form"},
        {"10220", "Tuberculosis"},
        {"", "Tuberculosis, Contact"},
        {"", " Unexplained Death"},
        {"", " Unusual Illness"},
        {"11647", "Viral Hemorrhagic Fever"},
        {"50224", "Zika Virus Disease, congenital"},
        {"50223", "Zika Virus Disease, non-congenital"},
        {"50222", "Zika Virus Infection, congenital"},
        {"11726", "Zika"},
        {"", " Carbapenem-resistant Enterobacteriaceae"},
        {"", " MDRA"},
        {"", " VRSA/VISA"},
        {"", "Amebic meningoencephalitis, primary (PAM)"},
        {"", " Ascariasis"},
        {"", "Typhus fever (epidemic louseborne, R. prowazekii)"},
        {"", " Taeniasis"},
        {"", " Cysticercosis"},
        {"", " Fascioliasis"},
        {"", " Hookworm"},
        {"", " Paragonimiasis"},
        {"", " Trichuriasis"},
        {"11120", "Acute flaccid myelitis"},
        {"", "Amebic meningitis, other"},
        {"", "Arbovirus, Neuroinvasive and Non-neuroinvasice"},
        {"", "Chagas, chronic indeterminate"},
        {"", "Chagas, chronic symptomatic"},
        {"11704", "Dengue-like Illness"},
        {"", " Novel coronavirus"},
        {"10260", "Typhus Fever, (endemic fleaborne, Murine)"},
        {"11541", "Vibrio parahaemolyticus"},
        {"11542", "Vibrio vulnificus"},
        {"", "Rickettsia, unspecified"},
        {"", " Hepatitis liver test"},
        {"", " Tuberculosis Gateway"},};

    /**
     * @param args the command line arguments
     */
    public static void main(String[] aStrArg) {

        initLogger();

        // check arguments to see if we are exporting a range of events
        if (aStrArg.length == 1) {
            // parse out the range of event ids
            String aStrRange[] = aStrArg[0].split(":");
            try {
                if (aStrRange.length == 1) {
                    nRecordNumberMin = Integer.parseInt(aStrRange[0]);
                    nRecordNumberMax = nRecordNumberMin + 1;
                } else if (aStrRange.length == 2) {
                    nRecordNumberMin = Integer.parseInt(aStrRange[0]);
                    nRecordNumberMax = Integer.parseInt(aStrRange[1]) + 1;
                }
            } catch (Exception ex) {
                nError = 1;
            }
            fExportEventRange = true;
        } else if (aStrArg.length > 2 || nError > 0) {
            logger.log(Level.SEVERE, "Exiting " + nError + " problem with argument list");
            System.err.println("Usage: NbsXmlExport <event_id>|<event_id_first:event_id_last>\n");
            System.exit(nError);
        }
        // Initialize trisano value -> nbs value maps
        initCodeMaps();

        // read Sql url and credentials from the property file
        loadProperties();

        // Ensure we can connect to the database
        con = getConnection();

        // fetch the events of interest
        ResultSet rs = fetchEvents(con);

        // output the xml for the events
        outputXml(rs);
    }

    /**
     *
     */
    private static void initLogger() {
        FileHandler handler;
        try {
            logger.setUseParentHandlers(false);
            handler = new FileHandler("NbsXmlExport.log");
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(new SimpleFormatter());
            handler.setLevel(Level.ALL);
            logger.addHandler(handler);
            logger.log(Level.INFO, "NbsXmlExport beginning logging");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error 7 setting up logger", ex);
            System.err.println("Error setting up logger");
            System.exit(7);
        }
    }

    /**
     *
     */
    private static void initCodeMaps() {
        mapRaceCode.put(0, "");
        mapRaceTxt.put(0, "");
        mapRaceCode.put(8, "2106-3");
        mapRaceTxt.put(8, "White");
        mapRaceCode.put(9, "2054-5");
        mapRaceTxt.put(9, "Black or African American");
        mapRaceCode.put(10, "1002-5");
        mapRaceTxt.put(10, "American Indian or Alaska Native");
        mapRaceCode.put(11, "2028-9");
        mapRaceTxt.put(11, "Asian");
        mapRaceCode.put(12, "1002-5");
        mapRaceTxt.put(12, "American Indian or Alaska Native");
        mapRaceCode.put(13, "2076-8");
        mapRaceTxt.put(13, "Native Hawaiian or Other Pacific Islander");
        mapRaceCode.put(14, "2131-1");
        mapRaceTxt.put(14, "Other Race");
        mapRaceCode.put(15, "1002-5");
        mapRaceTxt.put(15, "American Indian or Alaska Native");
        mapRaceCode.put(16, "2028-9");
        mapRaceTxt.put(16, "Asian");
        mapRaceCode.put(17, "2028-9");
        mapRaceTxt.put(17, "Asian");
        mapRaceCode.put(18, "2028-9");
        mapRaceTxt.put(18, "Asian");
        mapRaceCode.put(19, "2076-8");
        mapRaceTxt.put(19, "Native Hawaiian or Other Pacific Islander");
        mapRaceCode.put(20, "2028-9");
        mapRaceTxt.put(20, "Asian");
        mapRaceCode.put(21, "2028-9");
        mapRaceTxt.put(21, "Asian");
        mapRaceCode.put(22, "2076-8");
        mapRaceTxt.put(22, "Native Hawaiian or Other Pacific Islander");
        mapRaceCode.put(23, "2076-8");
        mapRaceTxt.put(23, "Native Hawaiian or Other Pacific Islander");
        mapRaceCode.put(24, "2076-8");
        mapRaceTxt.put(24, "Native Hawaiian or Other Pacific Islander");
        mapRaceCode.put(25, "");
        mapRaceTxt.put(25, "");
        mapRaceCode.put(26, "2076-8");
        mapRaceTxt.put(26, "Native Hawaiian or Other Pacific Islander");
        mapRaceCode.put(27, "2131-1");
        mapRaceTxt.put(27, "Other Race");
        mapRaceCode.put(528, "");
        mapRaceTxt.put(528, "");
        mapRaceCode.put(529, "");
        mapRaceTxt.put(529, "");

        mapEthnicityCode.put(0, "");
        mapEthnicityTxt.put(0, "");
        mapEthnicityCode.put(4, "2135-2");
        mapEthnicityTxt.put(4, "Hispanic or Latino");
        mapEthnicityCode.put(5, "2186-5");
        mapEthnicityTxt.put(5, "Not Hispanic or Latino");
        mapEthnicityCode.put(6, "2186-5");
        mapEthnicityTxt.put(6, "Not Hispanic or Latino");
        mapEthnicityCode.put(7, "2186-5");
        mapEthnicityTxt.put(7, "Not Hispanic or Latino");

        mapGenderCode.put(0, "U");
        mapGenderTxt.put(0, "Unknown");
        mapGenderCode.put(1, "M");
        mapGenderTxt.put(1, "Male");
        mapGenderCode.put(2, "F");
        mapGenderTxt.put(2, "Female");
        mapGenderCode.put(3, "U");
        mapGenderTxt.put(3, "Unknown");

        mapAgeTypeCode.put(0, "a");
        mapAgeTypeTxt.put(0, "year");
        mapAgeTypeCode.put(173, "a");
        mapAgeTypeTxt.put(173, "year");
        mapAgeTypeCode.put(174, "wk");    // Months, but the mapped to weeks   
        mapAgeTypeTxt.put(174, "week");
        mapAgeTypeCode.put(175, "wk");
        mapAgeTypeTxt.put(175, "week");
        mapAgeTypeCode.put(176, "d");
        mapAgeTypeTxt.put(176, "day");
        mapAgeTypeCode.put(177, "a");
        mapAgeTypeTxt.put(177, "year");
        mapAgeTypeCode.put(178, "a");
        mapAgeTypeTxt.put(178, "year");

        mapYNUCode.put(0, "");
        mapYNUTxt.put(0, "");
        mapYNUCode.put(119, "Y");
        mapYNUTxt.put(119, "Yes");
        mapYNUCode.put(120, "N");
        mapYNUTxt.put(120, "No");
        mapYNUCode.put(118, "UNK");
        mapYNUTxt.put(118, "Unknown");

        mapCountyCode.put(0, "439"); //Tarrant County
        mapCountyCode.put(272, "003");
        mapCountyCode.put(273, "005");
        mapCountyCode.put(274, "007");
        mapCountyCode.put(275, "009");
        mapCountyCode.put(276, "011");
        mapCountyCode.put(277, "013");
        mapCountyCode.put(278, "015");
        mapCountyCode.put(279, "017");
        mapCountyCode.put(280, "019");
        mapCountyCode.put(281, "021");
        mapCountyCode.put(282, "023");
        mapCountyCode.put(283, "025");
        mapCountyCode.put(284, "027");
        mapCountyCode.put(285, "029");
        mapCountyCode.put(286, "031");
        mapCountyCode.put(287, "033");
        mapCountyCode.put(288, "035");
        mapCountyCode.put(289, "037");
        mapCountyCode.put(290, "039");
        mapCountyCode.put(291, "041");
        mapCountyCode.put(292, "043");
        mapCountyCode.put(293, "045");
        mapCountyCode.put(294, "047");
        mapCountyCode.put(295, "049");
        mapCountyCode.put(296, "051");
        mapCountyCode.put(297, "053");
        mapCountyCode.put(298, "055");
        mapCountyCode.put(299, "057");
        mapCountyCode.put(300, "059");
        mapCountyCode.put(301, "061");
        mapCountyCode.put(302, "063");
        mapCountyCode.put(303, "065");
        mapCountyCode.put(304, "067");
        mapCountyCode.put(305, "069");
        mapCountyCode.put(306, "071");
        mapCountyCode.put(307, "073");
        mapCountyCode.put(308, "075");
        mapCountyCode.put(309, "077");
        mapCountyCode.put(310, "079");
        mapCountyCode.put(311, "081");
        mapCountyCode.put(312, "083");
        mapCountyCode.put(313, "085");
        mapCountyCode.put(314, "087");
        mapCountyCode.put(315, "089");
        mapCountyCode.put(316, "091");
        mapCountyCode.put(317, "093");
        mapCountyCode.put(318, "095");
        mapCountyCode.put(319, "097");
        mapCountyCode.put(320, "099");
        mapCountyCode.put(321, "101");
        mapCountyCode.put(322, "103");
        mapCountyCode.put(323, "105");
        mapCountyCode.put(324, "107");
        mapCountyCode.put(325, "109");
        mapCountyCode.put(326, "111");
        mapCountyCode.put(327, "113");
        mapCountyCode.put(328, "115");
        mapCountyCode.put(329, "117");
        mapCountyCode.put(330, "119");
        mapCountyCode.put(331, "121");
        mapCountyCode.put(332, "123");
        mapCountyCode.put(333, "125");
        mapCountyCode.put(334, "127");
        mapCountyCode.put(335, "129");
        mapCountyCode.put(336, "131");
        mapCountyCode.put(337, "133");
        mapCountyCode.put(338, "135");
        mapCountyCode.put(339, "137");
        mapCountyCode.put(340, "139");
        mapCountyCode.put(341, "141");
        mapCountyCode.put(342, "143");
        mapCountyCode.put(343, "145");
        mapCountyCode.put(344, "147");
        mapCountyCode.put(345, "149");
        mapCountyCode.put(346, "151");
        mapCountyCode.put(347, "153");
        mapCountyCode.put(348, "155");
        mapCountyCode.put(349, "157");
        mapCountyCode.put(350, "159");
        mapCountyCode.put(351, "161");
        mapCountyCode.put(352, "163");
        mapCountyCode.put(353, "165");
        mapCountyCode.put(354, "167");
        mapCountyCode.put(355, "169");
        mapCountyCode.put(356, "171");
        mapCountyCode.put(357, "173");
        mapCountyCode.put(358, "175");
        mapCountyCode.put(359, "177");
        mapCountyCode.put(360, "179");
        mapCountyCode.put(361, "181");
        mapCountyCode.put(362, "183");
        mapCountyCode.put(363, "185");
        mapCountyCode.put(364, "187");
        mapCountyCode.put(365, "189");
        mapCountyCode.put(366, "191");
        mapCountyCode.put(367, "193");
        mapCountyCode.put(368, "195");
        mapCountyCode.put(369, "197");
        mapCountyCode.put(370, "199");
        mapCountyCode.put(371, "201");
        mapCountyCode.put(372, "203");
        mapCountyCode.put(373, "205");
        mapCountyCode.put(374, "207");
        mapCountyCode.put(375, "209");
        mapCountyCode.put(376, "211");
        mapCountyCode.put(377, "213");
        mapCountyCode.put(378, "215");
        mapCountyCode.put(379, "217");
        mapCountyCode.put(380, "219");
        mapCountyCode.put(381, "221");
        mapCountyCode.put(382, "223");
        mapCountyCode.put(383, "225");
        mapCountyCode.put(384, "227");
        mapCountyCode.put(385, "229");
        mapCountyCode.put(386, "231");
        mapCountyCode.put(387, "233");
        mapCountyCode.put(388, "235");
        mapCountyCode.put(389, "237");
        mapCountyCode.put(390, "239");
        mapCountyCode.put(391, "241");
        mapCountyCode.put(392, "243");
        mapCountyCode.put(393, "245");
        mapCountyCode.put(394, "247");
        mapCountyCode.put(395, "249");
        mapCountyCode.put(396, "251");
        mapCountyCode.put(397, "253");
        mapCountyCode.put(398, "255");
        mapCountyCode.put(399, "257");
        mapCountyCode.put(400, "259");
        mapCountyCode.put(401, "261");
        mapCountyCode.put(402, "263");
        mapCountyCode.put(403, "265");
        mapCountyCode.put(404, "267");
        mapCountyCode.put(405, "269");
        mapCountyCode.put(406, "271");
        mapCountyCode.put(407, "273");
        mapCountyCode.put(408, "275");
        mapCountyCode.put(409, "277");
        mapCountyCode.put(410, "279");
        mapCountyCode.put(411, "281");
        mapCountyCode.put(412, "283");
        mapCountyCode.put(413, "285");
        mapCountyCode.put(414, "287");
        mapCountyCode.put(415, "289");
        mapCountyCode.put(416, "291");
        mapCountyCode.put(417, "293");
        mapCountyCode.put(418, "295");
        mapCountyCode.put(419, "297");
        mapCountyCode.put(420, "299");
        mapCountyCode.put(421, "301");
        mapCountyCode.put(422, "303");
        mapCountyCode.put(423, "305");
        mapCountyCode.put(424, "307");
        mapCountyCode.put(425, "309");
        mapCountyCode.put(426, "311");
        mapCountyCode.put(427, "313");
        mapCountyCode.put(428, "315");
        mapCountyCode.put(429, "317");
        mapCountyCode.put(430, "319");
        mapCountyCode.put(431, "321");
        mapCountyCode.put(432, "323");
        mapCountyCode.put(433, "325");
        mapCountyCode.put(434, "327");
        mapCountyCode.put(435, "329");
        mapCountyCode.put(436, "331");
        mapCountyCode.put(437, "333");
        mapCountyCode.put(438, "335");
        mapCountyCode.put(439, "337");
        mapCountyCode.put(440, "339");
        mapCountyCode.put(441, "341");
        mapCountyCode.put(442, "343");
        mapCountyCode.put(443, "345");
        mapCountyCode.put(444, "347");
        mapCountyCode.put(445, "349");
        mapCountyCode.put(446, "351");
        mapCountyCode.put(447, "353");
        mapCountyCode.put(448, "355");
        mapCountyCode.put(449, "357");
        mapCountyCode.put(450, "359");
        mapCountyCode.put(451, "361");
        mapCountyCode.put(452, "363");
        mapCountyCode.put(453, "365");
        mapCountyCode.put(454, "367");
        mapCountyCode.put(455, "369");
        mapCountyCode.put(456, "371");
        mapCountyCode.put(457, "373");
        mapCountyCode.put(458, "375");
        mapCountyCode.put(459, "377");
        mapCountyCode.put(460, "379");
        mapCountyCode.put(461, "381");
        mapCountyCode.put(462, "383");
        mapCountyCode.put(463, "385");
        mapCountyCode.put(464, "387");
        mapCountyCode.put(465, "389");
        mapCountyCode.put(466, "391");
        mapCountyCode.put(467, "393");
        mapCountyCode.put(468, "395");
        mapCountyCode.put(469, "397");
        mapCountyCode.put(470, "399");
        mapCountyCode.put(471, "401");
        mapCountyCode.put(472, "403");
        mapCountyCode.put(473, "405");
        mapCountyCode.put(474, "407");
        mapCountyCode.put(475, "409");
        mapCountyCode.put(476, "411");
        mapCountyCode.put(477, "413");
        mapCountyCode.put(478, "415");
        mapCountyCode.put(479, "417");
        mapCountyCode.put(480, "419");
        mapCountyCode.put(481, "421");
        mapCountyCode.put(482, "423");
        mapCountyCode.put(483, "425");
        mapCountyCode.put(484, "427");
        mapCountyCode.put(485, "429");
        mapCountyCode.put(486, "431");
        mapCountyCode.put(487, "433");
        mapCountyCode.put(488, "435");
        mapCountyCode.put(489, "437");
        mapCountyCode.put(490, "439");
        mapCountyCode.put(491, "441");
        mapCountyCode.put(492, "443");
        mapCountyCode.put(493, "445");
        mapCountyCode.put(494, "447");
        mapCountyCode.put(495, "449");
        mapCountyCode.put(496, "451");
        mapCountyCode.put(497, "453");
        mapCountyCode.put(498, "455");
        mapCountyCode.put(499, "457");
        mapCountyCode.put(500, "459");
        mapCountyCode.put(501, "461");
        mapCountyCode.put(502, "463");
        mapCountyCode.put(503, "465");
        mapCountyCode.put(504, "467");
        mapCountyCode.put(505, "469");
        mapCountyCode.put(506, "471");
        mapCountyCode.put(507, "473");
        mapCountyCode.put(508, "475");
        mapCountyCode.put(509, "477");
        mapCountyCode.put(510, "479");
        mapCountyCode.put(511, "481");
        mapCountyCode.put(512, "483");
        mapCountyCode.put(513, "485");
        mapCountyCode.put(514, "487");
        mapCountyCode.put(515, "489");
        mapCountyCode.put(516, "491");
        mapCountyCode.put(517, "493");
        mapCountyCode.put(518, "495");
        mapCountyCode.put(519, "497");
        mapCountyCode.put(520, "499");
        mapCountyCode.put(521, "501");
        mapCountyCode.put(522, "503");
        mapCountyCode.put(523, "505");
        mapCountyCode.put(524, "507");

        mapCountyTxt.put(0, "Tarrant County");
        mapCountyTxt.put(271, "Anderson County");
        mapCountyTxt.put(272, "Andrews County");
        mapCountyTxt.put(273, "Angelina County");
        mapCountyTxt.put(274, "Aransas County");
        mapCountyTxt.put(275, "Archer County");
        mapCountyTxt.put(276, "Armstrong County");
        mapCountyTxt.put(277, "Atascosa County");
        mapCountyTxt.put(278, "Austin County");
        mapCountyTxt.put(279, "Bailey County");
        mapCountyTxt.put(280, "Bandera County");
        mapCountyTxt.put(281, "Bastrop County");
        mapCountyTxt.put(282, "Baylor County");
        mapCountyTxt.put(283, "Bee County");
        mapCountyTxt.put(284, "Bell County");
        mapCountyTxt.put(285, "Bexar County");
        mapCountyTxt.put(286, "Blanco County");
        mapCountyTxt.put(287, "Borden County");
        mapCountyTxt.put(288, "Bosque County");
        mapCountyTxt.put(289, "Bowie County");
        mapCountyTxt.put(290, "Brazoria County");
        mapCountyTxt.put(291, "Brazos County");
        mapCountyTxt.put(292, "Brewster County");
        mapCountyTxt.put(293, "Briscoe County");
        mapCountyTxt.put(294, "Brooks County");
        mapCountyTxt.put(295, "Brown County");
        mapCountyTxt.put(296, "Burleson County");
        mapCountyTxt.put(297, "Burnet County");
        mapCountyTxt.put(298, "Caldwell County");
        mapCountyTxt.put(299, "Calhoun County");
        mapCountyTxt.put(300, "Callahan County");
        mapCountyTxt.put(301, "Cameron County");
        mapCountyTxt.put(302, "Camp County");
        mapCountyTxt.put(303, "Carson County");
        mapCountyTxt.put(304, "Cass County");
        mapCountyTxt.put(305, "Castro County");
        mapCountyTxt.put(306, "Chambers County");
        mapCountyTxt.put(307, "Cherokee County");
        mapCountyTxt.put(308, "Childress County");
        mapCountyTxt.put(309, "Clay County");
        mapCountyTxt.put(310, "Cochran County");
        mapCountyTxt.put(311, "Coke County");
        mapCountyTxt.put(312, "Coleman County");
        mapCountyTxt.put(313, "Collin County");
        mapCountyTxt.put(314, "Collingsworth County");
        mapCountyTxt.put(315, "Colorado County");
        mapCountyTxt.put(316, "Comal County");
        mapCountyTxt.put(317, "Comanche County");
        mapCountyTxt.put(318, "Concho County");
        mapCountyTxt.put(319, "Cooke County");
        mapCountyTxt.put(320, "Coryell County");
        mapCountyTxt.put(321, "Cottle County");
        mapCountyTxt.put(322, "Crane County");
        mapCountyTxt.put(323, "Crockett County");
        mapCountyTxt.put(324, "Crosby County");
        mapCountyTxt.put(325, "Culberson County");
        mapCountyTxt.put(326, "Dallam County");
        mapCountyTxt.put(327, "Dallas County");
        mapCountyTxt.put(328, "Dawson County");
        mapCountyTxt.put(329, "Deaf Smith County");
        mapCountyTxt.put(330, "Delta County");
        mapCountyTxt.put(331, "Denton County");
        mapCountyTxt.put(332, "DeWitt County");
        mapCountyTxt.put(333, "Dickens County");
        mapCountyTxt.put(334, "Dimmit County");
        mapCountyTxt.put(335, "Donley County");
        mapCountyTxt.put(336, "Duval County");
        mapCountyTxt.put(337, "Eastland County");
        mapCountyTxt.put(338, "Ector County");
        mapCountyTxt.put(339, "Edwards County");
        mapCountyTxt.put(340, "Ellis County");
        mapCountyTxt.put(341, "El Paso County");
        mapCountyTxt.put(342, "Erath County");
        mapCountyTxt.put(343, "Falls County");
        mapCountyTxt.put(344, "Fannin County");
        mapCountyTxt.put(345, "Fayette County");
        mapCountyTxt.put(346, "Fisher County");
        mapCountyTxt.put(347, "Floyd County");
        mapCountyTxt.put(348, "Foard County");
        mapCountyTxt.put(349, "Fort Bend County");
        mapCountyTxt.put(350, "Franklin County");
        mapCountyTxt.put(351, "Freestone County");
        mapCountyTxt.put(352, "Frio County");
        mapCountyTxt.put(353, "Gaines County");
        mapCountyTxt.put(354, "Galveston County");
        mapCountyTxt.put(355, "Garza County");
        mapCountyTxt.put(356, "Gillespie County");
        mapCountyTxt.put(357, "Glasscock County");
        mapCountyTxt.put(358, "Goliad County");
        mapCountyTxt.put(359, "Gonzales County");
        mapCountyTxt.put(360, "Gray County");
        mapCountyTxt.put(361, "Grayson County");
        mapCountyTxt.put(362, "Gregg County");
        mapCountyTxt.put(363, "Grimes County");
        mapCountyTxt.put(364, "Guadalupe County");
        mapCountyTxt.put(365, "Hale County");
        mapCountyTxt.put(366, "Hall County");
        mapCountyTxt.put(367, "Hamilton County");
        mapCountyTxt.put(368, "Hansford County");
        mapCountyTxt.put(369, "Hardeman County");
        mapCountyTxt.put(370, "Hardin County");
        mapCountyTxt.put(371, "Harris County");
        mapCountyTxt.put(372, "Harrison County");
        mapCountyTxt.put(373, "Hartley County");
        mapCountyTxt.put(374, "Haskell County");
        mapCountyTxt.put(375, "Hays County");
        mapCountyTxt.put(376, "Hemphill County");
        mapCountyTxt.put(377, "Henderson County");
        mapCountyTxt.put(378, "Hidalgo County");
        mapCountyTxt.put(379, "Hill County");
        mapCountyTxt.put(380, "Hockley County");
        mapCountyTxt.put(381, "Hood County");
        mapCountyTxt.put(382, "Hopkins County");
        mapCountyTxt.put(383, "Houston County");
        mapCountyTxt.put(384, "Howard County");
        mapCountyTxt.put(385, "Hudspeth County");
        mapCountyTxt.put(386, "Hunt County");
        mapCountyTxt.put(387, "Hutchinson County");
        mapCountyTxt.put(388, "Irion County");
        mapCountyTxt.put(389, "Jack County");
        mapCountyTxt.put(390, "Jackson County");
        mapCountyTxt.put(391, "Jasper County");
        mapCountyTxt.put(392, "Jeff Davis County");
        mapCountyTxt.put(393, "Jefferson County");
        mapCountyTxt.put(394, "Jim Hogg County");
        mapCountyTxt.put(395, "Jim Wells County");
        mapCountyTxt.put(396, "Johnson County");
        mapCountyTxt.put(397, "Jones County");
        mapCountyTxt.put(398, "Karnes County");
        mapCountyTxt.put(399, "Kaufman County");
        mapCountyTxt.put(400, "Kendall County");
        mapCountyTxt.put(401, "Kenedy County");
        mapCountyTxt.put(402, "Kent County");
        mapCountyTxt.put(403, "Kerr County");
        mapCountyTxt.put(404, "Kimble County");
        mapCountyTxt.put(405, "King County");
        mapCountyTxt.put(406, "Kinney County");
        mapCountyTxt.put(407, "Kleberg County");
        mapCountyTxt.put(408, "Knox County");
        mapCountyTxt.put(409, "Lamar County");
        mapCountyTxt.put(410, "Lamb County");
        mapCountyTxt.put(411, "Lampasas County");
        mapCountyTxt.put(412, "La Salle County");
        mapCountyTxt.put(413, "Lavaca County");
        mapCountyTxt.put(414, "Lee County");
        mapCountyTxt.put(415, "Leon County");
        mapCountyTxt.put(416, "Liberty County");
        mapCountyTxt.put(417, "Limestone County");
        mapCountyTxt.put(418, "Lipscomb County");
        mapCountyTxt.put(419, "Live Oak County");
        mapCountyTxt.put(420, "Llano County");
        mapCountyTxt.put(421, "Loving County");
        mapCountyTxt.put(422, "Lubbock County");
        mapCountyTxt.put(423, "Lynn County");
        mapCountyTxt.put(424, "McCulloch County");
        mapCountyTxt.put(425, "McLennan County");
        mapCountyTxt.put(426, "McMullen County");
        mapCountyTxt.put(427, "Madison County");
        mapCountyTxt.put(428, "Marion County");
        mapCountyTxt.put(429, "Martin County");
        mapCountyTxt.put(430, "Mason County");
        mapCountyTxt.put(431, "Matagorda County");
        mapCountyTxt.put(432, "Maverick County");
        mapCountyTxt.put(433, "Medina County");
        mapCountyTxt.put(434, "Menard County");
        mapCountyTxt.put(435, "Midland County");
        mapCountyTxt.put(436, "Milam County");
        mapCountyTxt.put(437, "Mills County");
        mapCountyTxt.put(438, "Mitchell County");
        mapCountyTxt.put(439, "Montague County");
        mapCountyTxt.put(440, "Montgomery County");
        mapCountyTxt.put(441, "Moore County");
        mapCountyTxt.put(442, "Morris County");
        mapCountyTxt.put(443, "Motley County");
        mapCountyTxt.put(444, "Nacogdoches County");
        mapCountyTxt.put(445, "Navarro County");
        mapCountyTxt.put(446, "Newton County");
        mapCountyTxt.put(447, "Nolan County");
        mapCountyTxt.put(448, "Nueces County");
        mapCountyTxt.put(449, "Ochiltree County");
        mapCountyTxt.put(450, "Oldham County");
        mapCountyTxt.put(451, "Orange County");
        mapCountyTxt.put(452, "Palo Pinto County");
        mapCountyTxt.put(453, "Panola County");
        mapCountyTxt.put(454, "Parker County");
        mapCountyTxt.put(455, "Parmer County");
        mapCountyTxt.put(456, "Pecos County");
        mapCountyTxt.put(457, "Polk County");
        mapCountyTxt.put(458, "Potter County");
        mapCountyTxt.put(459, "Presidio County");
        mapCountyTxt.put(460, "Rains County");
        mapCountyTxt.put(461, "Randall County");
        mapCountyTxt.put(462, "Reagan County");
        mapCountyTxt.put(463, "Real County");
        mapCountyTxt.put(464, "Red River County");
        mapCountyTxt.put(465, "Reeves County");
        mapCountyTxt.put(466, "Refugio County");
        mapCountyTxt.put(467, "Roberts County");
        mapCountyTxt.put(468, "Robertson County");
        mapCountyTxt.put(469, "Rockwall County");
        mapCountyTxt.put(470, "Runnels County");
        mapCountyTxt.put(471, "Rusk County");
        mapCountyTxt.put(472, "Sabine County");
        mapCountyTxt.put(473, "San Augustine County");
        mapCountyTxt.put(474, "San Jacinto County");
        mapCountyTxt.put(475, "San Patricio County");
        mapCountyTxt.put(476, "San Saba County");
        mapCountyTxt.put(477, "Schleicher County");
        mapCountyTxt.put(478, "Scurry County");
        mapCountyTxt.put(479, "Shackelford County");
        mapCountyTxt.put(480, "Shelby County");
        mapCountyTxt.put(481, "Sherman County");
        mapCountyTxt.put(482, "Smith County");
        mapCountyTxt.put(483, "Somervell County");
        mapCountyTxt.put(484, "Starr County");
        mapCountyTxt.put(485, "Stephens County");
        mapCountyTxt.put(486, "Sterling County");
        mapCountyTxt.put(487, "Stonewall County");
        mapCountyTxt.put(488, "Sutton County");
        mapCountyTxt.put(489, "Swisher County");
        mapCountyTxt.put(490, "Tarrant County");
        mapCountyTxt.put(491, "Taylor County");
        mapCountyTxt.put(492, "Terrell County");
        mapCountyTxt.put(493, "Terry County");
        mapCountyTxt.put(494, "Throckmorton County");
        mapCountyTxt.put(495, "Titus County");
        mapCountyTxt.put(496, "Tom Green County");
        mapCountyTxt.put(497, "Travis County");
        mapCountyTxt.put(498, "Trinity County");
        mapCountyTxt.put(499, "Tyler County");
        mapCountyTxt.put(500, "Upshur County");
        mapCountyTxt.put(501, "Upton County");
        mapCountyTxt.put(502, "Uvalde County");
        mapCountyTxt.put(503, "Val Verde County");
        mapCountyTxt.put(504, "Van Zandt County");
        mapCountyTxt.put(505, "Victoria County");
        mapCountyTxt.put(506, "Walker County");
        mapCountyTxt.put(507, "Waller County");
        mapCountyTxt.put(508, "Ward County");
        mapCountyTxt.put(509, "Washington County");
        mapCountyTxt.put(510, "Webb County");
        mapCountyTxt.put(511, "Wharton County");
        mapCountyTxt.put(512, "Wheeler County");
        mapCountyTxt.put(513, "Wichita County");
        mapCountyTxt.put(514, "Wilbarger County");
        mapCountyTxt.put(515, "Willacy County");
        mapCountyTxt.put(516, "Williamson County");
        mapCountyTxt.put(517, "Wilson County");
        mapCountyTxt.put(518, "Winkler County");
        mapCountyTxt.put(519, "Wise County");
        mapCountyTxt.put(520, "Wood County");
        mapCountyTxt.put(521, "Yoakum County");
        mapCountyTxt.put(522, "Young County");
        mapCountyTxt.put(523, "Zapata County");
        mapCountyTxt.put(524, "Zavala County");

        mapStateCode.put(0, "48");
        mapStateTxt.put(0, "Texas");
        mapStateCode.put(59, "49");
        mapStateTxt.put(59, "Utah");
        mapStateCode.put(60, "01");
        mapStateTxt.put(60, "Alabama");
        mapStateCode.put(61, "01");
        mapStateTxt.put(61, "Alaska");
        mapStateCode.put(62, "");
        mapStateTxt.put(62, "");
        mapStateCode.put(63, "04");
        mapStateTxt.put(63, "Arizona");
        mapStateCode.put(64, "05");
        mapStateTxt.put(64, "Arkansas");
        mapStateCode.put(65, "06");
        mapStateTxt.put(65, "California");
        mapStateCode.put(66, "08");
        mapStateTxt.put(66, "Colorado");
        mapStateCode.put(67, "09");
        mapStateTxt.put(67, "Connecticut");
        mapStateCode.put(68, "10");
        mapStateTxt.put(68, "Delaware");
        mapStateCode.put(69, "11");
        mapStateTxt.put(69, "District of Columbia");
        mapStateCode.put(70, "");
        mapStateTxt.put(70, "");
        mapStateCode.put(71, "12");
        mapStateTxt.put(71, "Florida");
        mapStateCode.put(72, "13");
        mapStateTxt.put(72, "Georgia");
        mapStateCode.put(73, "");
        mapStateTxt.put(73, "");
        mapStateCode.put(74, "15");
        mapStateTxt.put(74, "Hawaii");
        mapStateCode.put(75, "16");
        mapStateTxt.put(75, "Idaho");
        mapStateCode.put(76, "17");
        mapStateTxt.put(76, "Illinois");
        mapStateCode.put(77, "18");
        mapStateTxt.put(77, "Indiana");
        mapStateCode.put(78, "19");
        mapStateTxt.put(78, "Iowa");
        mapStateCode.put(79, "20");
        mapStateTxt.put(79, "Kansas");
        mapStateCode.put(80, "21");
        mapStateTxt.put(80, "Kentucky");
        mapStateCode.put(81, "22");
        mapStateTxt.put(81, "Louisiana");
        mapStateCode.put(82, "23");
        mapStateTxt.put(82, "Maine");
        mapStateCode.put(83, "");
        mapStateTxt.put(83, "");
        mapStateCode.put(84, "24");
        mapStateTxt.put(84, "Maryland");
        mapStateCode.put(85, "25");
        mapStateTxt.put(85, "Massachusetts");
        mapStateCode.put(86, "26");
        mapStateTxt.put(86, "Michigan");
        mapStateCode.put(87, "27");
        mapStateTxt.put(87, "Minnesota");
        mapStateCode.put(88, "28");
        mapStateTxt.put(88, "Mississippi");
        mapStateCode.put(89, "29");
        mapStateTxt.put(89, "Missouri");
        mapStateCode.put(90, "20");
        mapStateTxt.put(90, "Montana");
        mapStateCode.put(91, "31");
        mapStateTxt.put(91, "Nebraska");
        mapStateCode.put(92, "32");
        mapStateTxt.put(92, "Nevada");
        mapStateCode.put(93, "33");
        mapStateTxt.put(93, "New Hampshire");
        mapStateCode.put(94, "34");
        mapStateTxt.put(94, "New Jersey");
        mapStateCode.put(95, "35");
        mapStateTxt.put(95, "New Mexico");
        mapStateCode.put(96, "36");
        mapStateTxt.put(96, "New York");
        mapStateCode.put(97, "37");
        mapStateTxt.put(97, "North Carolina");
        mapStateCode.put(98, "38");
        mapStateTxt.put(98, "North Dakota");
        mapStateCode.put(99, "");
        mapStateTxt.put(99, "");
        mapStateCode.put(100, "39");
        mapStateTxt.put(100, "Ohio");
        mapStateCode.put(101, "40");
        mapStateTxt.put(101, "Oklahoma");
        mapStateCode.put(102, "41");
        mapStateTxt.put(102, "Oregon");
        mapStateCode.put(103, "");
        mapStateTxt.put(103, "");
        mapStateCode.put(104, "");
        mapStateTxt.put(104, "");
        mapStateCode.put(105, "42");
        mapStateTxt.put(105, "Pennsylvania");
        mapStateCode.put(106, "");
        mapStateTxt.put(106, "");
        mapStateCode.put(107, "44");
        mapStateTxt.put(107, "Rhode Island");
        mapStateCode.put(108, "45");
        mapStateTxt.put(108, "South Carolina");
        mapStateCode.put(109, "46");
        mapStateTxt.put(109, "South Dakota");
        mapStateCode.put(110, "47");
        mapStateTxt.put(110, "Tennessee");
        mapStateCode.put(111, "48");
        mapStateTxt.put(111, "Texas");
        mapStateCode.put(112, "50");
        mapStateTxt.put(112, "Vermont");
        mapStateCode.put(113, "51");
        mapStateTxt.put(113, "Virginia");
        mapStateCode.put(114, "53");
        mapStateTxt.put(114, "Washington");
        mapStateCode.put(115, "54");
        mapStateTxt.put(115, "West Virginia");
        mapStateCode.put(116, "55");
        mapStateTxt.put(116, "Wisconsin");
        mapStateCode.put(117, "56");
        mapStateTxt.put(117, "Wyoming");
    }

    /**
     *
     */
    private static void loadProperties() {
        // TODO Jay fix path
        File file = new File("nbs_xml_export.properties");
 
        try {
            FileInputStream in = new FileInputStream(file.getAbsolutePath());
            properties.load(in);
            in.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 2 could not read properties file" + ex.toString());
            System.err.println("Could not read properties file: " + file.getAbsolutePath());
            System.err.println("Properties Exception: " + ex.toString());
            System.exit(2);
        }
    }

    /**
     * @author Jay Boyer 2013
     */
    private static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(
                    properties.getProperty("db_url"),
                    properties.getProperty("db_user"),
                    properties.getProperty("db_password"));
            if (con == null) {
                logger.log(Level.SEVERE, "Exiting 3 database connection returned null");
                System.err.println("Problem connecting to database.");
                System.exit(3);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 4 could not connect to database " + ex.toString());
            System.err.println("Could not connect to database. Exception: " + ex.toString());
            System.exit(4);
        }
        return con;
    }

    private static ResultSet fetchEvents(Connection con) {

        ResultSet rs = null;
        // SQL for finding completed but not reported events
        // TODO alternatives columns for recording sent to state
        // review_completed_by_state_date
        // sent_to_ibis
        // rs_reported_to_clinician_date
        String strSql = "SELECT e.id, e.record_number, e.created_at, e.event_onset_date, e.\"first_reported_PH_date\","
                + " e.\"MMWR_week\", e.\"MMWR_year\", e.age_at_onset, e.age_type_id, e.investigator_id, de.disease_id,"
                + " p.secondary_entity_id AS ra_entity_id, pl.name AS ra_name, de.hospitalized_id, de.died_id, "
                + " de.disease_onset_date, de.date_diagnosed, e.outbreak_associated_id, e.lhd_case_status_id"
                + " from events e"
                + " INNER JOIN disease_events de ON de.event_id = e.id"
                + " LEFT JOIN participations p ON p.event_id = e.id AND p.type = 'ReportingAgency'"
                + " LEFT JOIN places pl on pl.entity_id = p.secondary_entity_id"
                + " WHERE e.deleted_at IS NULL AND (e.type = 'MorbidityEvent' OR e.type = 'AssessmentEvent') ";
        if (fExportEventRange) {
            strSql += "AND e.record_number >= '" + nRecordNumberMin.toString() + "' AND e.record_number < '" + nRecordNumberMax.toString() + "'";
        } else { // default case
            strSql += "AND workflow_state = 'approved_by_lhd' AND e.review_completed_by_state_date IS NULL";
//            strSql += "AND workflow_state = 'closed' AND e.review_completed_by_state_date IS NULL";
        }
        strSql += " ORDER BY id";
        return executeSql(strSql);
    }

    private static void outputXml(ResultSet rs) {
        int i = 0;
        try {
            ensureOutputDir();
            while (rs.next()) {
                int nEventId = rs.getInt(1);
                String strRecordNum = rs.getString(2);
                System.out.print(" Event ID: " + nEventId);
                logger.log(Level.INFO, "Event ID: " + nEventId);
                ObjectFactory factory = new ObjectFactory();
                Container container = factory.createContainer();
                fillHeader(container, strRecordNum);
                if (fFillCase(container, nEventId, strRecordNum, rs)) {
                    String strFileName = "output/TCPH_" + strRecordNum + ".xml";

                    JAXBContext context = JAXBContext.newInstance("gov.cdc.nedss");
                    Marshaller marshaller = context.createMarshaller();
                    marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
                    marshaller.marshal(container, new FileOutputStream(strFileName));
                } else {
                    logger.log(Level.INFO, "event " + nEventId + " not reportable");
                }
                // record that this event has been exported, or does not need to be exported
                recordExport(nEventId);
                i++;
            }
        } catch (Exception ex) {
            System.err.println("Error getting event id from result: " + i);
            System.err.println("rs Exception: " + ex.toString());
            logger.log(Level.SEVERE, "Exiting 6 " + ex.toString());
            System.exit(6);
        }
    }

    private static void fillHeader(Container container, String strRecordNum)
            throws Exception {
        HeaderType header = new HeaderType();
        header.setMessageType(newCodedType("PDHC", "Public Health Document Container", "2.16.840.1.114222.4.5.1"));
        header.setResultStatus(newCodedType("F", "Final", "2.16.840.1.114222.4.11.815"));
        header.setCreationTime(getXMLGregorianCalendarNow());
        header.setVersionID("1.0");
        header.setProcessingID("TCPH_NBS_" + strRecordNum);
        header.setMessageControlID("TCPH_" + strRecordNum);
        header.setSendingApplication(newHierarchicalDesignationType("TCPH TriSano", "Trisano Suite", "L"));
        header.setSendingFacility(newHierarchicalDesignationType("TCPH TriSano", "TCPH", "L"));
        header.setReceivingApplication(newHierarchicalDesignationType("NBS", "2.16.840.1.114222.4.5.1", "ISO"));
        header.setReceivingFacility(newHierarchicalDesignationType("State of Texas DSHS", "2.16.840.1.114222.4.1.3683", "ISO"));

        container.setHeader(header);
    }

    /**
     *
     * @param container
     * @param nEventId
     * @param strRecordNum
     * @param rs
     * @return true if the case disease is reportable to NBS
     * @throws Exception
     */
    private static boolean fFillCase(Container container, int nEventId, String strRecordNum, ResultSet rs)
            throws Exception {
        HashMap<String, Object> mapEvent = createEventHash(nEventId, rs);
        if (mapEvent == null) {
            return false;
        }

        CaseType phCase = new CaseType();
        SectionHeaderType sh = new SectionHeaderType();
        sh.setDocumentType(newCodedType("PHC236", "CaseReport", "2.16.840.1.114222.4.5.274"));
        sh.setPurpose(newCodedType("SHARE_NOTF", "Share", "2.16.840.1.114222.4.5.1"));
        sh.setDescription((String) mapEvent.get("description"));
        sh.setSendingApplicationEventIdentifier(strRecordNum);
        phCase.setSectionHeader(sh);
        phCase.setCondition(newCodedType((String) mapEvent.get("condition_code"),
                (String) mapEvent.get("condition"), "2.16.840.1.114222.4.5.277"));

        addPatient(phCase, nEventId, mapEvent);
        addCommonQuestions(phCase, nEventId, mapEvent);
        addDiseaseSpecificQuestions(phCase, nEventId, mapEvent);
        addLabReports(phCase, nEventId, mapEvent);

        container.setCase(phCase);

        return true;
    }

    /**
     *
     * @param phCase
     * @param nEventId
     */
    private static void addPatient(CaseType phCase, int nEventId, HashMap<String, Object> mapEvent)
            throws Exception {

        PatientType patient = new PatientType();
        HashMap<String, String> mapPatient = (HashMap<String, String>) mapEvent.get("map_patient");

        // patient identifier is the patient id
        patient.setSendingApplicationPatientIdentifier("TCPH_" + mapPatient.get("entity_id"));
        NameType name = new NameType();
        name.setFirst((String) mapPatient.get("first_name"));
        name.setLast((String) mapPatient.get("last_name"));
        patient.setName(name);
        patient.setDateOfBirth(gregorianDate(mapPatient.get("birth_date")));
        NumericType numeric = new NumericType();
        String strAge = mapPatient.get("reported_age");
        if (strAge.length() > 0) {
            numeric.setValue1(Float.valueOf(strAge));
            numeric.setUnit(newCodedType(mapPatient.get("age_code"), mapPatient.get("age_txt"), "2.16.840.1.113883.6.8"));
            patient.setReportedAge(numeric);
        }
        patient.setSex(newCodedType(mapPatient.get("gender_code"), mapPatient.get("gender_txt"), "2.16.840.1.113883.12.1"));
        String strRaceCode = mapPatient.get("race_code");
        String strEthnicityCode = mapPatient.get("ethnicity_code");
        if (strRaceCode.length() > 0) {
            patient.getRace().add(newCodedType(strRaceCode, mapPatient.get("race_txt"), "2.16.840.1.113883.6.238"));
        }
        if (strEthnicityCode.length() > 0) {
            patient.getRace().add(newCodedType(strEthnicityCode, mapPatient.get("ethnicity_txt"), "2.16.840.1.113883.6.238"));
        }
        PostalAddressType address = new PostalAddressType();
        address.setStreetAddressOne(mapPatient.get("street_address"));
        address.setCity(mapPatient.get("city"));
        address.setZipCode(mapPatient.get("zip"));
        String strState = mapPatient.get("state");
        String strCounty = mapPatient.get("county");
        if (strCounty.length() > 0) {
            address.setState(newCodedType(mapPatient.get("county_code"), strCounty, "2.16.840.1.113883.6.93"));
        }
        if (strState.length() > 0) {
            address.setState(newCodedType(mapPatient.get("state_code"), strState, "2.16.840.1.113883.6.92"));
        }
        patient.setPostalAddress(address);
        phCase.setPatient(patient);
    }

    /**
     *
     * @param phCase
     * @param nEventId
     */
    private static void addCommonQuestions(CaseType phCase, int nEventId, HashMap<String, Object> mapEvent)
            throws Exception {
        CommonQuestionsType cq = new CommonQuestionsType();
        InvestigationInformationType ii = new InvestigationInformationType();
        // 48066 is correct jurisdicion ID?
        ii.setSendingApplicationJurisdiction(newCodedType("48066", "Tarrant County Public Health", "L"));
        ii.setInvestigationStatus(newCodedType("29179001", "Closed", "2.16.840.1.113883.6.96"));
        cq.setInvestigationInformation(ii);
        String strReportingProvider = "";
        String strReportingAgency = (String) mapEvent.get("reporting_agency");
        ReportingInformationType ri = new ReportingInformationType();
        ri.setDateOfReport(getXMLGregorianCalendarNow());
        Date dateReported = (Date) mapEvent.get("date_reported");
        if (dateReported != null) {
            ri.setEarliestDateReportedToCounty(gregorianDate(dateReported));
        }
        ri.setEarliestDateReportedToState(getXMLGregorianCalendarNow());
        if (strReportingAgency.length() > 0) {
            OrganizationParticipantType opt = new OrganizationParticipantType();
            opt.setTypeCd(newCodedType("OrgAsReporterOfPHC", "Organization As Reporter Of PHC", "2.16.840.1.114222.4.5.1"));
            opt.setName(strReportingAgency);
            ri.setReportingOrganization(opt);
            cq.setReportingInformation(ri);
        }
        String strReporterFirstName = (String) mapEvent.get("reporter_first_name");
        String strReporterLastName = (String) mapEvent.get("reporter_last_name");
        if (strReporterFirstName.length() > 0 || strReporterLastName.length() > 0) {
            ProviderParticipantType pp = new ProviderParticipantType();
            ProviderNameType pn = new ProviderNameType();
            pn.setFirst(strReporterFirstName);
            pn.setLast(strReporterLastName);
            pp.setName(pn);
            pp.setTypeCd(newCodedType("PerAsReporterOfPHC", "PHC Reporter", "2.16.840.1.114222.4.5.1"));
            ri.setReportingProvider(pp);
            cq.setReportingInformation(ri);
        }
        ClinicalInformationType ci = new ClinicalInformationType();
        String strClinicianFirstName = (String) mapEvent.get("clinician_first_name");
        String strClinicianLastName = (String) mapEvent.get("clinician_last_name");
        if (strClinicianFirstName.length() > 0 || strClinicianLastName.length() > 0) {
            ProviderParticipantType pp = new ProviderParticipantType();
            ProviderNameType pn = new ProviderNameType();
            pn.setFirst(strClinicianFirstName);
            pn.setLast(strClinicianLastName);
            pp.setName(pn);
            pp.setTypeCd(newCodedType("PhysicianOfPHC", "Physician of PHC", "2.16.840.1.114222.4.5.1"));
            ci.setPhysician(pp);
            cq.setClinicalInformation(ci);
        }
        String strHospital = (String) mapEvent.get("hospital_name");
        if (strHospital.length() > 0) {
            OrganizationParticipantType op = new OrganizationParticipantType();
            op.setName(strHospital);
            op.setTypeCd(newCodedType("HospOfADT", "Hospital of ADT", "2.16.840.1.114222.4.5.1"));
            ci.setHospital(op);
        }
        int nHospitalizedId = intNonNull((Integer) mapEvent.get("hospitalized_id"));
        ci.setWasThePatientHospitalized(newCodedType((String) mapYNUCode.get(nHospitalizedId),
                (String) mapYNUTxt.get(nHospitalizedId), "2.16.840.1.113883.12.136"));
        Date admissionDate = (Date) mapEvent.get("admission_date");
        if (admissionDate != null) {
            ci.setAdmissionDate(gregorianDate(admissionDate));
        }
        Date diagnosedDate = (Date) mapEvent.get("diagnosed_date");
        if (diagnosedDate != null) {
            ci.setDiagnosisDate(gregorianDate(diagnosedDate));
        }
        cq.setClinicalInformation(ci);

        EpidemiologicInformationType ei = new EpidemiologicInformationType();

        Integer nOutbreakAssociatedId = intNonNull((Integer) mapEvent.get("outbreak_associated_id"));
        ei.setIsThisCasePartOfAnOutbreak(newCodedType((String) mapYNUCode.get(nHospitalizedId),
                (String) mapYNUTxt.get(nHospitalizedId), "2.16.840.1.113883.12.136"));
        // TODO jay confirmation method??
        ei.setDetectionMethod(newCodedType("LR", "Laboratory report", "2.16.840.1.113883.6.96"));
        // TODO jay map lhd_case_status_id to case status
        Integer nLhdCaseStatusId = intNonNull((Integer) mapEvent.get("lhd_case_status_id"));
        ei.setCaseStatus(newCodedType("410605003", "Confirmed present", "2.16.840.1.113883.6.96"));
        ei.setMMWRWeek(mapEvent.get("mmwr_week").toString());
        ei.setMMWRYear(mapEvent.get("mmwr_year").toString());
        cq.setEpidemiologicInformation(ei);
        phCase.setCommonQuestions(cq);
    }

    /**
     *
     * @param phCase
     * @param nEventId
     * @param mapEvent
     * @throws Exception
     */
    private static void addDiseaseSpecificQuestions(CaseType phCase, int nEventId, HashMap<String, Object> mapEvent)
            throws Exception {
    }

    /**
     *
     * @param phCase
     * @param nEventId
     * @param mapEvent
     * @throws Exception
     */
    private static void addLabReports(CaseType phCase, int nEventId, HashMap<String, Object> mapEvent)
            throws Exception {
    }

    /**
     * Author Jay Boyer
     *
     * @param nEventId
     * @return null if the condition is not reportable to NBS, otherwise return
     * the hash
     *
     * Fetch the core event data for an event and fill a hash map, mapping the
     * xml output element name to the data
     */
    private static HashMap<String, Object> createEventHash(int nEventId, ResultSet rs)
            throws Exception {
        int idCondition = -1;
        HashMap<String, Object> map = new LinkedHashMap<String, Object>();
        idCondition = rs.getInt(11);
        String strCondition = aStrCondition[idCondition][1];

        // if this condition does not map to a reportable NBS condition
        if (aStrCondition[idCondition][0].equals("")) {
            logger.log(Level.INFO, "event " + nEventId + ",condition: " + strCondition + " (not reportable)");
            return null;
        }

        String strDateCreated = strDefaultDate(rs.getDate(3));
        map.put("date_created", (String) strDateCreated);
        HashMap mapPatient = getPatientMap(nEventId);
        map.put("map_patient", mapPatient);
        String strRecordNumber = rs.getString(2);
        String strFirst = (String) mapPatient.get("first_name");
        String strLast = (String) mapPatient.get("last_name");
        map.put("description", "Closed (Auto) case record #" + strRecordNumber + " for "
                + strCondition + " for " + strFirst + " " + strLast + ", opened on "
                + strDateCreated);
        map.put("condition", strCondition);
        map.put("condition_code", aStrCondition[idCondition][0]);

        String strMmwrWeek = Integer.toString(rs.getInt(6));
        if(strMmwrWeek.length()==1) {
            strMmwrWeek = "0" + strMmwrWeek;
        }
        map.put("mmwr_week", strMmwrWeek);
        map.put("mmwr_year", rs.getInt(7));

        map.put("reporting_agency", strNonNull(rs.getString(13)).trim());
        map.put("ra_entity_id", rs.getInt(12));

        map.put("hospitalized_id", rs.getInt(14));
        map.put("died_id", rs.getInt(15));
        map.put("onset_date", rs.getDate(16));
        map.put("diagnosed_date", rs.getDate(17));
        map.put("outbreak_associated_id", rs.getInt(18));
        map.put("lhd_case_status_id", rs.getInt(19));

        addReporterToHash(nEventId, map);
        addClinicianToHash(nEventId, map);
        addHospitalToHash(nEventId, map);

        map.put("date_reported", rs.getDate(5));

        return map;
    }

    private static CodedType newCodedType(String strCode, String strDesc, String strSystemCode) {
        CodedType ct = new CodedType();
        ct.setCode(strCode);
        ct.setCodeDescTxt(strDesc);
        ct.setCodeSystemCode(strSystemCode);
        return ct;
    }

    private static HierarchicalDesignationType newHierarchicalDesignationType(String strNamespaceID,
            String strUniversalID, String strUniversalIDType) {
        HierarchicalDesignationType hdt = new HierarchicalDesignationType();
        hdt.setNamespaceID(strNamespaceID);
        hdt.setUniversalID(strUniversalID);
        hdt.setUniversalIDType(strUniversalIDType);
        return hdt;
    }

    private static XMLGregorianCalendar getXMLGregorianCalendarNow()
            throws DatatypeConfigurationException {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        XMLGregorianCalendar now =
                datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
        return now;
    }

    /**
     * xml case files are written to a sub-directory to the current dir named
     * output. Create the output sub-directory if it does not exist.
     */
    private static void ensureOutputDir() {

        File file = new File("output");
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private static ResultSet executeSql(String strSql) {
        ResultSet rs = null;
        Statement statement;
        try {
            statement = con.createStatement();
            rs = statement.executeQuery(strSql);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 5 error executing SQL " + ex.toString());
            System.err.println("Error executing SQL: " + strSql);
            System.err.println("SQL Exception: " + ex.toString());
            System.exit(5);
        }
        return rs;
    }

    private static HashMap<String, String> getPatientMap(int nEventId)
            throws SQLException {

        HashMap<String, String> map = new LinkedHashMap<String, String>();

        // default values
        map.put("reported_age", "");
        map.put("age_code", "a");
        map.put("age_txt", "year");
        map.put("street_address", "");
        map.put("city", "");
        map.put("state_code", "48");
        map.put("state", "Texas");
        map.put("county_code", "439");
        map.put("county", "Tarrant County");
        map.put("zip", "");

        String strSql = "SELECT p.last_name, p.first_name, pr.race_id, p.ethnicity_id, p.birth_gender_id, p.birth_date, "
                + " p.entity_id, p.age_type_id, p.approximate_age_no_birthday"
                + " FROM people p"
                + " INNER JOIN participations pa ON pa.primary_entity_id = p.entity_id"
                + " LEFT JOIN people_races pr ON pr.entity_id = p.entity_id"
                + " WHERE pa.type = 'InterestedParty' and pa.event_id = " + nEventId;
        ResultSet rs = executeSql(strSql);
        if (rs.next()) {
            map.put("last_name", strNonNull(rs.getString(1)));
            map.put("first_name", strNonNull(rs.getString(2)));
            map.put("race_code", (String) mapRaceCode.get(intNonNull(rs.getInt(3))));
            map.put("race_txt", (String) mapRaceTxt.get(intNonNull(rs.getInt(3))));
            map.put("ethnicity_code", (String) mapEthnicityCode.get(intNonNull(rs.getInt(4))));
            map.put("ethnicity_txt", (String) mapEthnicityTxt.get(intNonNull(rs.getInt(4))));
            map.put("gender_code", (String) mapGenderCode.get(intNonNull(rs.getInt(5))));
            map.put("gender_txt", (String) mapGenderTxt.get(intNonNull(rs.getInt(5))));
            Date dateBirth = rs.getDate(6);
            map.put("birth_date", strDefaultDate(dateBirth));
            Integer nEntityId = intNonNull(rs.getInt(7));
            map.put("entity_id", nEntityId.toString());
            Integer nAge = intNonNull(rs.getInt(9));

            // If a reported age is given, map it into the NBS acceptable vocabulary
            if (nAge > 0) {
                map.put("reported_age", nAge.toString());
                Integer nAgeUnit = intNonNull(rs.getInt(8));
                String strAgeTxt = (String) mapAgeTypeTxt.get(nAgeUnit);
                if (!"year".equals(strAgeTxt)) {
                    // if the TriSano age is in months, convert it to weeks (the code and txt are already correct)
                    if (nAgeUnit == 174) {
                        nAge = nAge * 13 / 3;
                        map.put("reported_age", nAge.toString());
                    }
                    map.put("age_txt", strAgeTxt);
                    map.put("age_code", (String) mapAgeTypeCode.get(nAgeUnit));
                }
            }

            // now get the address information
            strSql = "SELECT id, county_id, state_id, street_number, street_name,"
                    + " unit_number, postal_code, city, entity_id, event_id from addresses"
                    + " WHERE event_id = " + nEventId + " AND entity_id = " + nEntityId + " ORDER BY id LIMIT 1";
            rs = executeSql(strSql);
            if (rs.next()) {
                map.put("state_code", (String) mapStateCode.get(intNonNull(rs.getInt(3))));
                map.put("state", (String) mapStateTxt.get(intNonNull(rs.getInt(3))));
                map.put("county_code", (String) mapCountyCode.get(intNonNull(rs.getInt(2))));
                map.put("county", (String) mapCountyTxt.get(intNonNull(rs.getInt(2))));
                String strStreet = strNonNull(rs.getString(4)) + " " + strNonNull(rs.getString(5));
                String strUnit = strNonNull(rs.getString(6));
                if (strUnit.length() > 0) {
                    strStreet += " #" + strUnit;
                }
                map.put("street_address", strStreet.trim());
                map.put("city", strNonNull(rs.getString(8)));
                map.put("zip", strNonNull(rs.getString(7)));
            }
        }
        return map;
    }

    private static String strNonNull(String str) {
        if (str == null) {
            return "";
        }
        return str;
    }

    private static Integer intNonNull(Integer n) {
        if (n == null) {
            return 0;
        }
        return n;
    }

    /**
     *
     * @param date
     * @return a string with the date formatted in the default format i.e.
     * 2018-06-30
     */
    private static String strDefaultDate(Date date) {
        if (date == null) {
            return ("");
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return (dateFormat.format(date));
        }
    }

    /**
     *
     * @param strDate
     * @return
     */
    private static XMLGregorianCalendar gregorianDate(String strDate) {
        if (strDate != null && strDate.length() > 0) {
            try {
                return DatatypeFactory.newInstance().newXMLGregorianCalendar(strDate);
            } catch (DatatypeConfigurationException ex) {
            }
        }
        return null;
    }

    private static XMLGregorianCalendar gregorianDate(Date date) {
        String strDate = strDefaultDate(date);
        return gregorianDate(strDate);
    }

    /**
     *
     * @param nEventId
     * @param map
     */
    private static void addReporterToHash(int nEventId, HashMap<String, Object> map) {

        map.put("reporter_first_name", "");
        map.put("reporter_last_name", "");
        String strSql = "SELECT pe.first_name, pe.last_name FROM participations p"
                + " INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id"
                + " WHERE p.type = 'Reporter' AND p.event_id = " + nEventId;
        ResultSet rs = executeSql(strSql);
        try {
            if (rs.next()) {
                map.put("reporter_first_name", strNonNull(rs.getString(1)));
                map.put("reporter_last_name", strNonNull(rs.getString(2)));
            }
        } catch (SQLException ex) {
        }
    }

    /**
     *
     * @param nEventId
     * @param map
     */
    private static void addClinicianToHash(int nEventId, HashMap<String, Object> map) {

        map.put("clinician_first_name", "");
        map.put("clinician_last_name", "");
        String strSql = "SELECT pe.first_name, pe.last_name FROM participations p"
                + " INNER JOIN people pe ON pe.entity_id = p.secondary_entity_id"
                + " WHERE p.type = 'Clinician' AND p.event_id = " + nEventId;
        ResultSet rs = executeSql(strSql);
        try {
            if (rs.next()) {
                map.put("clinician_first_name", strNonNull(rs.getString(1)));
                map.put("clinician_last_name", strNonNull(rs.getString(2)));
            }
        } catch (SQLException ex) {
        }
    }

    /**
     *
     * @param nEventId
     * @param map
     */
    private static void addHospitalToHash(int nEventId, HashMap<String, Object> map) {

        map.put("hospital_name", "");
        map.put("admission_date", null);
        String strSql = "SELECT pl.name, hp.admission_date FROM participations p"
                + " INNER JOIN places pl ON pl.entity_id = p.secondary_entity_id"
                + " INNER JOIN hospitals_participations hp ON p.id = hp.participation_id"
                + " WHERE p.type = 'HospitalizationFacility' AND p.event_id = " + nEventId;
        ResultSet rs = executeSql(strSql);
        try {
            if (rs.next()) {
                map.put("hospital_name", strNonNull(rs.getString(1)));
                map.put("admission_date", rs.getDate(2));
            }
        } catch (SQLException ex) {
        }
    }
    
    private static void recordExport(int nEventId) {
        if (!fExportEventRange) {
            String strSql = "UPDATE events SET review_completed_by_state_date = now() WHERE id = " + nEventId;
            Statement statement;
            try {
                statement = con.createStatement();
                statement.executeUpdate(strSql);
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Error 8 problem updating event to record it has been exported. " + ex.toString());
                System.err.println("Error 8 problem updating event to record it has been exported. " + ex.toString());
                System.exit(8);
            }
        }
    }
}
