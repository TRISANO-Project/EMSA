/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tcph;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 *
 * @author Jay Boyer 2021
 */
public class EpiTraxAuth {
    private static Properties properties = new Properties();
    private static Logger logger = Logger.getLogger("");
    private static Connection con;

    public static void main(String[] asArg) {

        // read the username and password from stdin
        Scanner scanner = new Scanner(System. in);
        String sUser = scanner.nextLine();
        String sPassword = scanner.nextLine();

        //logger.log(Level.SEVERE, "inputs: " + sUser + "|" + sPassword);
        
        loadProperties();
        
        // connect to the database
        con = getConnection();
        
        // check to see if the user is listed in the EpiTrax user table
        String sSql = "select id from public.user WHERE UID='" + sUser + "'";
        
        ResultSet rs = executeSql(sSql, false /*fUpdate*/);
        try {
            if(!rs.next()) {
                // not a valid EpiTrax user.
                logger.log(Level.SEVERE, "Exiting: Not a valid EpiTrax user |" + sUser + "|");
                System.err.println("Exiting: Not a valid EpiTrax user |" + sUser + "|");
                System.exit(-1);
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Exiting: Error checking username |" + sUser + "|");
            System.out.println("Error checking username:|" + sUser + "| " + ex.toString());
            System.exit(-1);
        }

        Hashtable hEnv = new Hashtable();
        
        try {
            hEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            hEnv.put(Context.PROVIDER_URL, properties.getProperty("ldap_url"));
            hEnv.put(Context.SECURITY_PRINCIPAL, properties.getProperty("ldap_user_prefix") + sUser);
            hEnv.put(Context.SECURITY_CREDENTIALS, sPassword);
            DirContext ctx = new InitialDirContext(hEnv);
            System.out.println("IT WORKED");
            logger.log(Level.INFO, "Successful log in:" + sUser);
            System.exit(0);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            System.out.println("Did not Authenticate!!! "  + sUser);
        }
        logger.log(Level.INFO, "Failed to log in: " + sUser);
        System.exit(-1);
    }

        /**
     *
     */
    private static void loadProperties() {
        File file = new File("epitrax_auth.properties");
 
        try {
            FileInputStream in = new FileInputStream(file.getAbsolutePath());
            properties.load(in);
            in.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting: could not read properties file" + ex.toString());
            System.err.println("Could not read properties file: " + file.getAbsolutePath());
            System.err.println("Properties Exception: " + ex.toString());
            System.exit(-1);
        }
    }
    
    private static ResultSet executeSql(String sSql, boolean fUpdate) {
        ResultSet rs = null;
        Statement statement;
        try {
            statement = con.createStatement();
            if(fUpdate) {
                statement.executeUpdate(sSql);
            } else {
                rs = statement.executeQuery(sSql);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting: error executing SQL: " + sSql);
            logger.log(Level.SEVERE, "SQL error: " + ex.toString());
            System.err.println("Error executing SQL: " + sSql);
            System.err.println("SQL Exception: " + ex.toString());
            closeConnection();
            System.exit(-1);
        }
        return rs;
    }

    private static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(
                    properties.getProperty("db_url"),
                    properties.getProperty("db_user"),
                    properties.getProperty("db_password"));
            if (con == null) {
                logger.log(Level.SEVERE, "Exiting: database connection returned null");
                System.err.println("Problem connecting to database.");
                System.exit(-1);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting: could not connect to database " + ex.toString());
            System.err.println("Could not connect to database. Exception: " + ex.toString());
            closeConnection();
            System.exit(-1);
        }
        return con;
    }

    private static void closeConnection() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
               System.err.println("SQL Exception: " + ex.toString());
            }
        }
    }
    
}
