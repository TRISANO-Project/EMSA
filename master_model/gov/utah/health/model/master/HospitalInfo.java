package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hospitalized" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="facility" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="admission" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="discarge" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="medical_record" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="died" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="date_of_death" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pregnant" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="due_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hospitalized",
    "facility",
    "admission",
    "discarge",
    "medicalRecord",
    "died",
    "dateOfDeath",
    "pregnant",
    "dueDate"
})
@XmlRootElement(name = "hospital_info")
public class HospitalInfo {

    @XmlElement(required = true)
    public String hospitalized;
    @XmlElement(required = true)
    public String facility;
    @XmlElement(required = true)
    public String admission;
    @XmlElement(required = true)
    //TODO rename this to discharge 
    public String discarge;
    @XmlElement(name = "medical_record", required = true)
    public String medicalRecord;
    @XmlElement(required = true)
    public String died;
    @XmlElement(name = "date_of_death", required = true)
    public String dateOfDeath;
    @XmlElement(required = true)
    public String pregnant;
    @XmlElement(name = "due_date", required = true)
    public String dueDate;

    /**
     * Gets the value of the hospitalized property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHospitalized() {
        return hospitalized;
    }

    /**
     * Sets the value of the hospitalized property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHospitalized(String value) {
        this.hospitalized = value;
    }

    /**
     * Gets the value of the facility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacility() {
        return facility;
    }

    /**
     * Sets the value of the facility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacility(String value) {
        this.facility = value;
    }

    /**
     * Gets the value of the admission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdmission() {
        return admission;
    }

    /**
     * Sets the value of the admission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdmission(String value) {
        this.admission = value;
    }

    /**
     * Gets the value of the discarge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscarge() {
        return discarge;
    }

    /**
     * Sets the value of the discarge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscarge(String value) {
        this.discarge = value;
    }

    /**
     * Gets the value of the medicalRecord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalRecord() {
        return medicalRecord;
    }

    /**
     * Sets the value of the medicalRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalRecord(String value) {
        this.medicalRecord = value;
    }

    /**
     * Gets the value of the died property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDied() {
        return died;
    }

    /**
     * Sets the value of the died property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDied(String value) {
        this.died = value;
    }

    /**
     * Gets the value of the dateOfDeath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfDeath() {
        return dateOfDeath;
    }

    /**
     * Sets the value of the dateOfDeath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfDeath(String value) {
        this.dateOfDeath = value;
    }

    /**
     * Gets the value of the pregnant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPregnant() {
        return pregnant;
    }

    /**
     * Sets the value of the pregnant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPregnant(String value) {
        this.pregnant = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueDate(String value) {
        this.dueDate = value;
    }
    
  

}
