package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="given" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="date_of_treatment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="treatment_stopped" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "given",
    "name",
    "dateOfTreatment",
    "treatmentStopped"
})
@XmlRootElement(name = "treatments")
public class Treatments {

    @XmlElement(required = true)
    public String given;
    @XmlElement(required = true)
    public String name;
    @XmlElement(name = "date_of_treatment", required = true)
    public String dateOfTreatment;
    @XmlElement(name = "treatment_stopped", required = true)
    public String treatmentStopped;

    /**
     * Gets the value of the given property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGiven() {
        return given;
    }

    /**
     * Sets the value of the given property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGiven(String value) {
        this.given = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the dateOfTreatment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfTreatment() {
        return dateOfTreatment;
    }

    /**
     * Sets the value of the dateOfTreatment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfTreatment(String value) {
        this.dateOfTreatment = value;
    }

    /**
     * Gets the value of the treatmentStopped property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreatmentStopped() {
        return treatmentStopped;
    }

    /**
     * Sets the value of the treatmentStopped property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreatmentStopped(String value) {
        this.treatmentStopped = value;
    }
    
 
}
