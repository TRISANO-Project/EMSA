package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exception_id",
    "message"
})
@XmlRootElement(name = "exception")
public class Exceptions {

    @XmlElement(name = "exception_id", required = true)
    public String exception_id;
    @XmlElement(name = "message", required = true)
    public String message;

    public Exceptions() {
    }

    public Exceptions(String exception_id, String message) {
        this.exception_id = exception_id;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String msg) {
        this.message = msg;
    }

    public String getException() {
        return exception_id;
    }

    public void setException(String msg) {
        this.exception_id = msg;
    }
}
