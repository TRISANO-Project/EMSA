package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "external_id",
    "status",
    "systemid",
    "original_message",
    "original_xml",
    "final_status",
    "exception_status",
    "status_message",
    "channel",
    "event_id",
    "participation_id",
    "add_type",
    "entity_id",
    "schema_name",
    "master_xml_id",
    "event_record_id"
})

@XmlRootElement(name = "sourceid")
public class Sourceid {

    @XmlElement(name = "id",required = true)
    public String id;
    
    @XmlElement(name = "external_id",required = true)
    public String external_id;
    
    @XmlElement(name = "status",required = true)
    public String status;
    
    @XmlElement(name = "system_id",required = true)
    public String systemid;
    
    @XmlElement(name = "original_message",required = true)
    public String original_message;
    
    @XmlElement(name = "original_xml",required = true)
    public String original_xml;
    
    @XmlElement(name = "final_status",required = true)
    public String final_status;
    
    @XmlElement(name = "exception_status",required = true)
    public String exception_status="0";
    
    @XmlElement(name = "status_message",required = true)
    public String status_message;
    
    @XmlElement(name = "channel",required = true)
    public String channel;
    
    @XmlElement(name = "event_id",required = true)
    public String event_id;
    
    @XmlElement(name = "participation_id",required = true)
    public String participation_id;
      
    @XmlElement(name = "add_type",required = true)
    public String add_type;
    
    @XmlElement(name = "entity_id",required = true)
    public String entity_id;
    
    @XmlElement(name = "schema_name",required = true)
    public String schema_name;
    
    @XmlElement(name = "master_xml_id",required = true)
    public String master_xml_id;
    
    @XmlElement(name = "event_record_id",required = true)
    public String event_record_id;
    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }
    
    public String getExternal() {
        return external_id;
    }

    public void setFinalStatus(String value) {
        this.final_status = value;
    }
    
    public String getFinalStatus() {
        return final_status;
    }
    
    public void setMasterXmlId(String value) {
        this.master_xml_id = value;
    }
    
    public String getMasterXmlId() {
        return master_xml_id;
    }
    
    public void setSchema(String value) {
        this.schema_name = value;
    }
    
    public String getSchema() {
        return schema_name;
    }
    
    public void setExceptionStatus(String value) {
        this.exception_status = value;
    }
    
    public String getExceptionStatus() {
        return exception_status;
    }
    
    public void setStatusMessage(String value) {
        this.status_message = value;
    }
    
    public String getStatusMessage() {
        return status_message;
    }
    
    public void setChannel(String value) {
        this.channel = value;
    }
    
    public String getChannel() {
        return channel;
    }
    
    public void setEventId(String value) {
        this.event_id = value;
    }
    
    public String getEventId() {
        return event_id;
    }
    
    public void setParticipationId(String value) {
        this.participation_id = value;
    }
    
    public String getParticipationId() {
        return participation_id;
    }
    
    public void setAddType(String value) {
        this.add_type = value;
    }
    
    public String getAddType() {
        return add_type;
    }
    
    public void setEntityId(String value) {
        this.entity_id = value;
    }
    
    public String getEntityId() {
        return entity_id;
    }
    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternal(String value) {
        this.external_id = value;
    }
    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemid() {
        return systemid;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemid(String value) {
        this.systemid = value;
    }

    public String getOriginalMessage() {
        return original_message;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalMessage(String value) {
        this.original_message = value;
    }

    public String getOriginalXml() {
        return original_xml;
    }

    public void setEventRecordId(String value) {
        this.event_record_id = value;
    }

    public String getEventRecordId() {
        return event_record_id;
    }
    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalXml(String value) {
        this.original_xml = value;
    }

}
