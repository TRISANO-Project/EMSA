package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained
 * within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="types" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "name",
    "types",
    "state",
    "city",
    "streetName",
    "unitNumber",
    "zipcode"
})
@XmlRootElement(name = "diagnostic")
public class Diagnostic {

    @XmlElement(required = true)
    public String name;
    @XmlElement(required = true)
    public String types;
    @XmlElement(required = true)
    public String state;
    @XmlElement(required = true)
    public String city;
    @XmlElement(name = "street_name", required = true)
    public String streetName;
    @XmlElement(name = "unit_number", required = true)
    public String unitNumber;
    @XmlElement(required = true)
    public String zipcode;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String value) {
        this.types = value;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

  
    public String getZip() {
        return this.zipcode;
    }

    public void setZip(String value) {
        this.zipcode = value;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress(){
        StringBuilder addy = new StringBuilder();
        
        // streetName, city, state zip
        
        if(this.streetName != null){
            addy.append(streetName);
        }
        if(this.city != null){
            addy.append(" ");
            addy.append(city);
        }
        if(this.state != null){
            addy.append(", ");
            addy.append(state);
        }
        if(this.zipcode != null){
            addy.append(" ");
            addy.append(zipcode);
        }
        
        return addy.toString();
    }
}
