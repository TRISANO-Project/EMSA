/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.utah.health.model.master;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained
 * within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pregnancyStatus",
    "icd10Diagnoses",
    "treatments",
    "physicianNotes"
})
@XmlRootElement(name = "esp")
public class Esp {

    @XmlElement(name = "pregnancy_status")
    private String pregnancyStatus;
    @XmlElement(name = "icd10_diagnosis")
    private List<String> icd10Diagnoses;
    @XmlElement(name = "treatments")
    private List<Treatments> treatments;
    @XmlElement(name = "physician_note")
    private List<String> physicianNotes;

    public Esp() {
    }
    
    public void setPregancyStatus(String value) {
        this.pregnancyStatus = value;
    }
    
    public String getPregnancyStatus() {
        return this.pregnancyStatus;
    }
      
    public void setIcd10Diagnoses(List<String> values) {
        this.icd10Diagnoses = values;
    }
    
    public List<String> getIcd10Diagnoses() {
        return this.icd10Diagnoses;
    }
    
    public void setTreatments(List<Treatments> values) {
        this.treatments = values;
    }
    
    public List<Treatments> getTreatments() {
        return this.treatments;
    }
    
    public void setPhysicianNotes(List<String> values) {
        this.physicianNotes = values;
    }
    
    public List<String> getPhysicianNotes() {
        return this.physicianNotes;
    }
}