package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="food_handler" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="healthcare_worker" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="group_living" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="day_care_assoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="occupation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="date_of_exposure" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="street_num" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="street_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="unit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="county" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="imported" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="risk_factors" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="risk_factor_notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="other_data" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "foodHandler",
    "healthcareWorker",
    "groupLiving",
    "dayCareAssoc",
    "occupation",
    "type",
    "name",
    "dateOfExposure",
    "streetNum",
    "streetName",
    "unit",
    "city",
    "state",
    "county",
    "zip",
    "imported",
    "riskFactors",
    "riskFactorNotes",
    "otherData",
    "otherData2"
})
@XmlRootElement(name = "epidemiological")
public class Epidemiological {

    @XmlElement(name = "food_handler", required = true)
    public String foodHandler;
    @XmlElement(name = "healthcare_worker", required = true)
    public String healthcareWorker;
    @XmlElement(name = "group_living", required = true)
    public String groupLiving;
    @XmlElement(name = "day_care_assoc", required = true)
    public String dayCareAssoc;
    @XmlElement(required = true)
    public String occupation;
    @XmlElement(required = true)
    public String type;
    @XmlElement(required = true)
    public String name;
    @XmlElement(name = "date_of_exposure", required = true)
    public String dateOfExposure;
    @XmlElement(name = "street_num", required = true)
    public String streetNum;
    @XmlElement(name = "street_name", required = true)
    public String streetName;
    @XmlElement(required = true)
    public String unit;
    @XmlElement(required = true)
    public String city;
    @XmlElement(required = true)
    public String state;
    @XmlElement(required = true)
    public String county;
    @XmlElement(required = true)
    public String zip;
    @XmlElement(required = true)
    public String imported;
    @XmlElement(name = "risk_factors", required = true)
    public String riskFactors;
    @XmlElement(name = "risk_factor_notes", required = true)
    public String riskFactorNotes;
    @XmlElement(name = "other_data", required = true)
    public String otherData;
    @XmlElement(name = "other_data2", required = true)
    public String otherData2;
    /**
     * Gets the value of the foodHandler property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFoodHandler() {
        return foodHandler;
    }

    /**
     * Sets the value of the foodHandler property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFoodHandler(String value) {
        this.foodHandler = value;
    }

    /**
     * Gets the value of the healthcareWorker property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthcareWorker() {
        return healthcareWorker;
    }

    /**
     * Sets the value of the healthcareWorker property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthcareWorker(String value) {
        this.healthcareWorker = value;
    }

    /**
     * Gets the value of the groupLiving property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupLiving() {
        return groupLiving;
    }

    /**
     * Sets the value of the groupLiving property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupLiving(String value) {
        this.groupLiving = value;
    }

    /**
     * Gets the value of the dayCareAssoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDayCareAssoc() {
        return dayCareAssoc;
    }

    /**
     * Sets the value of the dayCareAssoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDayCareAssoc(String value) {
        this.dayCareAssoc = value;
    }

    /**
     * Gets the value of the occupation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * Sets the value of the occupation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupation(String value) {
        this.occupation = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the dateOfExposure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfExposure() {
        return dateOfExposure;
    }

    /**
     * Sets the value of the dateOfExposure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfExposure(String value) {
        this.dateOfExposure = value;
    }

    /**
     * Gets the value of the streetNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetNum() {
        return streetNum;
    }

    /**
     * Sets the value of the streetNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetNum(String value) {
        this.streetNum = value;
    }

    /**
     * Gets the value of the streetName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Sets the value of the streetName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetName(String value) {
        this.streetName = value;
    }

    /**
     * Gets the value of the unit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the value of the unit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the county property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCounty(String value) {
        this.county = value;
    }

    /**
     * Gets the value of the zip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the imported property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImported() {
        return imported;
    }

    /**
     * Sets the value of the imported property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImported(String value) {
        this.imported = value;
    }

    /**
     * Gets the value of the riskFactors property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskFactors() {
        return riskFactors;
    }

    /**
     * Sets the value of the riskFactors property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskFactors(String value) {
        this.riskFactors = value;
    }

    /**
     * Gets the value of the riskFactorNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskFactorNotes() {
        return riskFactorNotes;
    }

    /**
     * Sets the value of the riskFactorNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskFactorNotes(String value) {
        this.riskFactorNotes = value;
    }

    /**
     * Gets the value of the otherData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherData() {
        return otherData;
    }

    /**
     * Sets the value of the otherData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherData(String value) {
        this.otherData = value;
    }
    
    public String getOtherData2() {
        return otherData2;
    }

    /**
     * Sets the value of the otherData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherData2(String value) {
        this.otherData2 = value;
    }

}
