package gov.utah.health.model.master;

public class Ontology {
   public String common_test_name="";
   public String organism="";
   public String reference_range="";
   public String specimen_source="";
   public String scale="";
   public String disease_name="";
   public String state_case_status="";
   public String manual_entry="";
   public String specimen_required="";
   
   public Ontology(){
	   this.common_test_name="";
	   this.organism="";
	   this.reference_range="";
	   this.specimen_source="";
	   this.scale="";
	   this.disease_name="";
	   this.state_case_status="";
	   this.manual_entry="";
	   this.specimen_required="";
   }
   
   public void setDiseaseName(String value){
	   this.disease_name=value;
   }
   
   public String getDiseaseName(){
	   return this.disease_name;
   }
   
   public void setLabScale(String value){
	   this.scale=value;
   }
   
   public String getLabScale(){
	   return this.scale;
   }
   
   public void setSpecimen(String value){
	   this.specimen_source=value;
   }
   
   public String getSpecimen(){
	   return this.specimen_source;
   }
   
   public void setOrganism(String value){
	   this.organism=value;
   }
   
   public String getOrganism(){
	   return this.organism;
   }
   
   public void setReference(String value){
	   this.reference_range=value;
   }
   
   public String getReference(){
	   return this.reference_range;
   }
   
   public void setCommon(String value){
	   this.common_test_name=value;
   }
   
   public String getCommon(){
	   return this.common_test_name;
   }
   
   public void setStateCaseStatus(String value){
	   this.state_case_status=value;
   }
   
   public String getStateCaseStatus(){
	   return this.state_case_status;
   }
   
   public void setManualEntry(String value){
	   this.manual_entry=value;
   }
   
   public String getManualEntry(){
	   return this.manual_entry;
   }
   
   public void setSpecimenRequired(String value){
   	this.specimen_required=value;
   }
   
   public String getSpecimenRequired(){
   	return this.specimen_required;
   }
}
