package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the health package.
 * <p>An ObjectFactory allows you to programatically construct new instances of
 * the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the
 * binding of schema type definitions, element declarations and model groups.
 * Factory methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package: health
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Notes }
     *
     */
    public Notes createNotes() {
        return new Notes();
    }

    /**
     * Create an instance of {@link Encounter }
     *
     */
    public Encounter createEncounter() {
        return new Encounter();
    }

    public PerformingLab createPerforming() {
        return new PerformingLab();
    }

    /**
     * Create an instance of {@link ContactInformation }
     *
     */
    public ContactInformation createContactInformation() {
        return new ContactInformation();
    }

    /**
     * Create an instance of {@link Clinicians }
     *
     */
    public Clinicians createClinicians() {
        return new Clinicians();
    }

    /**
     * Create an instance of {@link Sourceid }
     *
     */
    public Sourceid createSourceid() {
        return new Sourceid();
    }

    /**
     * Create an instance of {@link Health }
     *
     */
    public Health createHealth() {
        return new Health();
    }

    /**
     * Create an instance of {@link Administrative }
     *
     */
    public Administrative createAdministrative() {
        return new Administrative();
    }

    /**
     * Create an instance of {@link Laboratory }
     *
     */
    public Laboratory createLaboratory() {
        return new Laboratory();
    }

    /**
     * Create an instance of {@link Treatments }
     *
     */
    public Treatments createTreatments() {
        return new Treatments();
    }

    /**
     * Create an instance of {@link Disease }
     *
     */
    public Disease createDisease() {
        return new Disease();
    }

    /**
     * Create an instance of {@link Person }
     *
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link Epidemiological }
     *
     */
    public Epidemiological createEpidemiological() {
        return new Epidemiological();
    }

    /**
     * Create an instance of {@link Diagnostic }
     *
     */
    public Diagnostic createDiagnostic() {
        return new Diagnostic();
    }

    /**
     * Create an instance of {@link HospitalInfo }
     *
     */
    public HospitalInfo createHospitalInfo() {
        return new HospitalInfo();
    }

    /**
     * Create an instance of {@link Reporting }
     *
     */
    public Reporting createReporting() {
        return new Reporting();
    }

    public Exceptions createException() {
        return new Exceptions();
    }
}
