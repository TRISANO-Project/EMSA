package gov.utah.health.model.master;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained
 * within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}person"/>
 *         &lt;element ref="{}disease"/>
 *         &lt;element ref="{}hospital_info"/>
 *         &lt;element ref="{}treatments"/>
 *         &lt;element ref="{}clinicians"/>
 *         &lt;element ref="{}diagnostic"/>
 *         &lt;element ref="{}laboratory"/>
 *         &lt;element ref="{}contact_information"/>
 *         &lt;element ref="{}encounter"/>
 *         &lt;element ref="{}epidemiological"/>
 *         &lt;element ref="{}reporting"/>
 *         &lt;element ref="{}notes"/>
 *         &lt;element ref="{}administrative"/>
 *         &lt;element ref="{}gateway"/>
 *         &lt;element ref="{}message_header"/>
 *         &lt;element ref="{}sourceid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "person",
    "disease",
    "hospitalInfo",
    "treatments",
    "clinicians",
    "diagnostic",
    "labs",
    "contactInformation",
    "encounter",
    "epidemiological",
    "reporting",
    "notes",
    "administrative",
    "sourceid",
    "performing",
    "exceptions",
    "esp"
})
@XmlRootElement(name = "health")
public class Health {

    @XmlElement(required = true)
    public Person person;
    @XmlElement(name = "disease", required = true)
    public Disease disease;
    @XmlElement(name = "hospital_info", required = true)
    public HospitalInfo hospitalInfo;
    @XmlElement(required = true)
    public Treatments treatments;
    @XmlElement(required = true)
    public Clinicians clinicians;
    @XmlElement(required = true)
    public Diagnostic diagnostic;
    @XmlElement(name = "labs", required = true)
    public List<Laboratory> labs = new ArrayList<Laboratory>();
    @XmlElement(name = "exceptions", required = true)
    public List<Exceptions> exceptions = new ArrayList<Exceptions>();
    @XmlElement(name = "contact_information", required = true)
    public ContactInformation contactInformation;
    @XmlElement(required = true)
    public Encounter encounter;
    @XmlElement(required = true)
    public Epidemiological epidemiological;
    @XmlElement(required = true)
    public Reporting reporting;
    @XmlElement(required = true)
    public Notes notes;
    @XmlElement(required = true)
    public Administrative administrative;
    @XmlElement(required = true)
    public Sourceid sourceid;
    @XmlElement(name = "performing_lab", required = true)
    public PerformingLab performing;
    @XmlElement(required = false)
    public Esp esp;

    public Health() {
    }


    public Person getPerson() {
        if (person == null) {
            person = new Person();
        }
        return person;
    }


    public void setPerson(Person value) {
        this.person = value;
    }


    public Disease getDisease() {
        if (disease == null) {
            disease = new Disease();
        }
        return disease;
    }

    public void setDisease(Disease value) {
        this.disease = value;
    }


    public HospitalInfo getHospitalInfo() {
        if (hospitalInfo == null) {
            hospitalInfo = new HospitalInfo();
        }
        return hospitalInfo;
    }

    public void setHospitalInfo(HospitalInfo value) {
        this.hospitalInfo = value;
    }


    public Treatments getTreatments() {
        if (treatments == null) {
            treatments = new Treatments();
        }
        return treatments;
    }


    public void setTreatments(Treatments value) {
        this.treatments = value;
    }


    public Clinicians getClinicians() {
        if (clinicians == null) {
            clinicians = new Clinicians();
        }
        return clinicians;
    }


    public void setClinicians(Clinicians value) {
        this.clinicians = value;
    }


    public Diagnostic getDiagnostic() {
        if (diagnostic == null) {
            diagnostic = new Diagnostic();
        }
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic value) {
        this.diagnostic = value;
    }

    public void setPerforming(PerformingLab value) {
        this.performing = value;
    }

    public PerformingLab getPerforming() {
        if (this.performing == null) {
            this.performing = new PerformingLab();
        }
        return this.performing;
    }

    public List<Laboratory> getLabs() {
        if (labs == null) {
            labs = new ArrayList<Laboratory>();
        }

        return labs;
    }
    
    public void setReportDate(String reportDate){
        if(this.reporting == null){
            this.reporting = new Reporting();
        }
        this.reporting.reportDate = reportDate;
    }
  
    public void setLabs(List<Laboratory> labList) {
        this.labs = labList;
    }


    public ContactInformation getContactInformation() {
        if (contactInformation == null) {
            contactInformation = new ContactInformation();
        }
        return contactInformation;
    }

 
    public void setContactInformation(ContactInformation value) {
        this.contactInformation = value;
    }


    public Encounter getEncounter() {
        if (encounter == null) {
            encounter = new Encounter();
        }
        return encounter;
    }


    public void setEncounter(Encounter value) {
        this.encounter = value;
    }

    /**
     * Gets the value of the epidemiological property.
     *
     * @return possible object is {@link Epidemiological }
     *
     */
    public Epidemiological getEpidemiological() {
        if (epidemiological == null) {
            epidemiological = new Epidemiological();
        }
        return epidemiological;
    }

    /**
     * Sets the value of the epidemiological property.
     *
     * @param value allowed object is {@link Epidemiological }
     *
     */
    public void setEpidemiological(Epidemiological value) {
        this.epidemiological = value;
    }

    /**
     * Gets the value of the reporting property.
     *
     * @return possible object is {@link Reporting }
     *
     */
    public Reporting getReporting() {
        if (this.reporting == null) {
            this.reporting = new Reporting();
        }
        return this.reporting;
    }

    /**
     * Sets the value of the reporting property.
     *
     * @param value allowed object is {@link Reporting }
     *
     */
    public void setReporting(Reporting value) {
        this.reporting = value;
    }

    /**
     * Gets the value of the notes property.
     *
     * @return possible object is {@link Notes }
     *
     */
    public Notes getNotes() {
        if (notes == null) {
            notes = new Notes();
        }
        return notes;
    }

    /**
     * Sets the value of the notes property.
     *
     * @param value allowed object is {@link Notes }
     *
     */
    public void setNotes(Notes value) {
        this.notes = value;
    }

    /**
     * Gets the value of the administrative property.
     *
     * @return possible object is {@link Administrative }
     *
     */
    public Administrative getAdministrative() {
        if (administrative == null) {
            administrative = new Administrative();
        }
        return administrative;
    }

    /**
     * Sets the value of the administrative property.
     *
     * @param value allowed object is {@link Administrative }
     *
     */
    public void setAdministrative(Administrative value) {
        this.administrative = value;
    }

   
   
   
   
    /**
     * Gets the value of the sourceid property.
     *
     * @return possible object is {@link Sourceid }
     *
     */
    public Sourceid getSourceid() {
        if (sourceid == null) {
            sourceid = new Sourceid();
        }
        return sourceid;
    }

    public List<Exceptions> getExceptions() {
        return exceptions;
    }
    public void setExceptions(List<Exceptions> eList) {
        this.exceptions = eList;
    }

    public void addException(Exceptions exp) {
        if (this.exceptions == null) {
            this.exceptions = new ArrayList<Exceptions>();
        }
        this.exceptions.add(exp);
    }
    public void addExceptions(List<Exceptions> eList) {
        if (this.exceptions == null) {
            this.exceptions = new ArrayList<Exceptions>();
        }
        this.exceptions.addAll(eList);
    }

    /**
     * Sets the value of the sourceid property.
     *
     * @param value allowed object is {@link Sourceid }
     *
     */
    public void setSourceid(Sourceid value) {
        this.sourceid = value;
    }
    
    public void setLabSegmentIndex(String segmentIndex){
        
        if(this.labs == null){
           this.labs = new ArrayList<Laboratory>();
        }
        if(!this.labs.isEmpty()){
            this.labs.get(0).setSegmentIndex(segmentIndex);
        }
        else{
            Laboratory lab = new Laboratory();
            lab.setSegmentIndex(segmentIndex);
            this.labs.add(lab);
        }
    }
    public String getLabSegmentIndex(){
        String segmentIndex = null;
        if(this.labs != null && !this.labs.isEmpty()){
            segmentIndex = this.labs.get(0).getSegmentIndex();           
        }
        return segmentIndex;
    }
    
    public void setEsp(Esp value) {
        this.esp = value;
    }
    
    public Esp getEsp() {
        return this.esp;
    }
}
