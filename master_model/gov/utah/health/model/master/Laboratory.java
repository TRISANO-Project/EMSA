package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lab" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="test_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="organism" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="test_result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="result_value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="units" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nameerence_range" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="test_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="specimen_source" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="collection_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lab_test_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="state_lab" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accession_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lab",
    "testType",
    "organism",
    "testResult",
    "resultValue",
    "units",
    "referenceRange",
    "testStatus",
    "specimenSource",
    "collectionDate",
    "labTestDate",
    "stateLab",
    "comment",
    "accessionNumber",
    "abnormalFlag",
    "loincCode",
    "scale",
    "status",
    "systemId",
    "labId",
    "resultType",
    "localTestName",
    "localLoincCode",
    "localCode",
    "localCodeTestName",
    "localResultValue",
    "localResultValue2",
    "localSpecimenSource",
    "localReferenceRange",
    "localResultUnit",
    "stateCaseStatus",
    "manualEntry",
    "specimenRequired",
    "segmentIndex"
})
@XmlRootElement(name = "laboratory")
public class Laboratory {

    @XmlElement(required = true)
    public String lab="";
    @XmlElement(name = "test_type", required = true)
    public String testType="";
    @XmlElement(name = "organism",required = true)
    public String organism="";
    @XmlElement(name = "test_result", required = true)
    public String testResult="";
    @XmlElement(name = "result_value", required = true)
    public String resultValue="";
    @XmlElement(name = "units",required = true)
    public String units="";
    @XmlElement(name = "reference_range", required = true)
    public String referenceRange="";
    @XmlElement(name = "test_status", required = true)
    public String testStatus="";
    @XmlElement(name = "specimen_source", required = true)
    public String specimenSource="";
    @XmlElement(name = "collection_date", required = true)
    public String collectionDate="";
    @XmlElement(name = "lab_test_date", required = true)
    public String labTestDate="";
    @XmlElement(name = "state_lab", required = true)
    public String stateLab="";
    @XmlElement(name="comment",required = true)
    public String comment="";
    @XmlElement(name = "accession_number", required = true)
    public String accessionNumber="";
    @XmlElement(name = "abnormal_flag", required = true)
    public String abnormalFlag="";
    @XmlElement(name = "loinc_code", required = true)
    public String loincCode="";
    @XmlElement(name = "scale", required = true)
    public String scale="";
    @XmlElement(name = "status", required = true)
    public String status="";
    @XmlElement(name = "system_id", required = true)
    public String systemId="";
    @XmlElement(name = "lab_id", required = true)
    public String labId="";
    @XmlElement(name = "result_type", required = true)
    public String resultType="";
    @XmlElement(name = "local_test_name", required = false)
    public String localTestName="";
    @XmlElement(name = "local_loinc_code", required = false)
    public String localLoincCode="";
    @XmlElement(name = "local_code", required = false)
    public String localCode="";
    @XmlElement(name = "local_code_test_name", required = false)
    public String localCodeTestName="";
    @XmlElement(name = "local_result_value", required = false)
    public String localResultValue="";
    @XmlElement(name = "local_result_value_2", required = false)
    public String localResultValue2="";
    @XmlElement(name = "local_result_unit", required = false)
    public String localResultUnit="";
    @XmlElement(name = "local_specimen_source", required = false)
    public String localSpecimenSource="";
    @XmlElement(name = "local_reference_range", required = false)
    public String localReferenceRange="";
    @XmlElement(name = "state_case_status", required = false)
    public String stateCaseStatus="";
    @XmlElement(name = "manual_entry", required = false)
    public String manualEntry="";
    @XmlElement(name = "specimen_required", required = false)
    public String specimenRequired="";
    @XmlElement(name = "segment_index", required = false)
    public String segmentIndex="";
    
    /**
     * Gets the value of the lab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLab() {
        return lab;
    }

    /**
     * Sets the value of the lab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLab(String value) {
        this.lab = value;
    }

    /**
     * Gets the value of the lab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabId() {
        return labId;
    }

    /**
     * Sets the value of the lab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabId(String value) {
        this.labId = value;
    }
    /**
     * Gets the value of the lab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the lab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }
    /**
     * Gets the value of the lab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystem() {
        return systemId;
    }

    /**
     * Sets the value of the lab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystem(String value) {
        this.systemId = value;
    }
    /**
     * Gets the value of the testType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestType() {
        return testType;
    }

   
    public void setReferenceRange(String value) {
        this.referenceRange = value;
    }

    public String getResultType() {
        return this.resultType;
    }

    public void setLocalTestName(String value) {
        this.localTestName = value;
    }
  
    public String getLocalTestName() {
        return this.localTestName;
    }
    
    public String getLocalLoincCode() {
        return this.localLoincCode;
    }
    
    public void setLocalLoincCode(String value) {
        this.localLoincCode = value;
    }

    public String getLocalCode() {
        return localCode;
    }

    public void setLocalCode(String localCode) {
        this.localCode = localCode;
    }

    public String getLocalCodeTestName() {
        return localCodeTestName;
    }

    public void setLocalCodeTestName(String localCodeTestName) {
        this.localCodeTestName = localCodeTestName;
    }

    public String getLocalResultValue() {
        return this.localResultValue;
    }
    
    public void setLocalResultValue(String value) {
        this.localResultValue = value;
    }

    public String getLocalResultValue2() {
        return localResultValue2;
    }

    public void setLocalResultValue2(String localResultValue2) {
        this.localResultValue2 = localResultValue2;
    }
    
    public String getLocalResultUnit() {
        return this.localResultUnit;
    }
    
    public void setLocalResultUnit(String value) {
        this.localResultUnit = value;
    }
    
    public String getLocalSpecimenSource() {
        return this.localSpecimenSource;
    }
    
    public void setLocalSpecimenSource(String value) {
        this.localSpecimenSource = value;
    }
    
    public String getLocalReferenceRange() {
        return this.localReferenceRange;
    }
    
    public void setLocalReferenceRange(String value) {
        this.localReferenceRange = value;
    }
    
    public void setResultType(String value) {
        this.resultType = value;
    }

    public void setStateCaseStatus(String value){
    	this.stateCaseStatus=value;
    }
    
    public String getStateCaseStatus(){
    	return this.stateCaseStatus;
    }
    
    public void setManualEntry(String value){
    	this.manualEntry=value;
    }
    
    public String getManualEntry(){
    	return this.manualEntry;
    }
    
    public void setSpecimenRequired(String value){
    	this.specimenRequired=value;
    }
    
    public String getSpecimenRequired(){
    	return this.specimenRequired;
    }
    /**
     * Gets the value of the testType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceRange() {
        return referenceRange;
    }

    /**
     * Sets the value of the testType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestType(String value) {
        this.testType = value;
    }

    /**
     * Gets the value of the organism property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganism() {
        return organism;
    }

    /**
     * Sets the value of the organism property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganism(String value) {
        this.organism = value;
    }

    /**
     * Gets the value of the testResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestResult() {
        return testResult;
    }

    /**
     * Sets the value of the testResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestResult(String value) {
        this.testResult = value;
    }

    /**
     * Gets the value of the resultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultValue() {
        return resultValue;
    }

    /**
     * Sets the value of the resultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultValue(String value) {
        this.resultValue = value;
    }

    /**
     * Gets the value of the units property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnits() {
        return units;
    }

    /**
     * Sets the value of the units property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnits(String value) {
        this.units = value;
    }

    

    /**
     * Gets the value of the testStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestStatus() {
        return testStatus;
    }

    /**
     * Sets the value of the testStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestStatus(String value) {
        this.testStatus = value;
    }

    /**
     * Gets the value of the specimenSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecimenSource() {
        return specimenSource;
    }

    /**
     * Sets the value of the specimenSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecimenSource(String value) {
        this.specimenSource = value;
    }

    /**
     * Gets the value of the collectionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionDate() {
        return collectionDate;
    }

    /**
     * Sets the value of the collectionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionDate(String value) {
        this.collectionDate = value;
    }

    /**
     * Gets the value of the labTestDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabTestDate() {
        return labTestDate;
    }

    /**
     * Sets the value of the labTestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabTestDate(String value) {
        this.labTestDate = value;
    }

    /**
     * Gets the value of the stateLab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateLab() {
        return stateLab;
    }

    /**
     * Sets the value of the stateLab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateLab(String value) {
        this.stateLab = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the accessionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessionNumber() {
        return accessionNumber;
    }

    /**
     * Sets the value of the accessionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessionNumber(String value) {
        this.accessionNumber = value;
    }
    
    /**
     * Gets the value of the accessionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbnormalFlag() {
        return this.abnormalFlag;
    }

    /**
     * Sets the value of the accessionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbnormalFlag(String value) {
        this.abnormalFlag = value;
    }
    
    /**
     * Gets the value of the accessionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoincCode() {
        return this.loincCode;
    }

    /**
     * Sets the value of the accessionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoincCode(String value) {
        this.loincCode = value;
    }
    
    /**
     * Gets the value of the accessionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScale() {
        return this.scale;
    }

    /**
     * Sets the value of the accessionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScale(String value) {
        this.scale = value;
    }

    public String getSegmentIndex() {
        return segmentIndex;
    }

    public void setSegmentIndex(String segmentIndex) {
        this.segmentIndex = segmentIndex;
    }
    
    public void setOntology(Ontology on){
    	
    	if(this!=null){
    		this.setOrganism(on.getOrganism());
    		this.setTestType(on.getCommon());
    		this.setScale(on.getLabScale());
    		this.setReferenceRange(on.getReference());
    		this.setSpecimenSource(on.getSpecimen());
    		this.setStateCaseStatus(on.getStateCaseStatus());
    		this.setManualEntry(on.getManualEntry());
    		this.setSpecimenRequired(on.getSpecimenRequired());
    	}
    	
    }
}
