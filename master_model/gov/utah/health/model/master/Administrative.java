package gov.utah.health.model.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="record_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mmwr_year" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mmwr_week" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="date_record_created" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lhd_case_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="state_case_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="outbreak_assoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="outbreak" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jurisdiction_of_residence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="event_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="acuity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recordNumber",
    "mmwrYear",
    "mmwrWeek",
    "dateRecordCreated",
    "lhdCaseStatus",
    "stateCaseStatus",
    "outbreakAssoc",
    "outbreak",
    "jurisdictionOfResidence",
    "eventName",
    "eventId",
    "participationId",
    "acuity",
    "jurisdictionId",
    "workflowState"
})
@XmlRootElement(name = "administrative")
public class Administrative {

    @XmlElement(name = "record_number", required = true)
    public String recordNumber;
    @XmlElement(name = "mmwr_year", required = true)
    public String mmwrYear;
    @XmlElement(name = "mmwr_week", required = true)
    public String mmwrWeek;
    @XmlElement(name = "date_record_created", required = true)
    public String dateRecordCreated;
    @XmlElement(name = "lhd_case_status", required = true)
    public String lhdCaseStatus;
    @XmlElement(name = "state_case_status", required = true)
    public String stateCaseStatus;
    @XmlElement(name = "outbreak_assoc", required = true)
    public String outbreakAssoc;
    @XmlElement(required = true)
    public String outbreak;
    @XmlElement(name = "jurisdiction_of_residence", required = true)
    public String jurisdictionOfResidence;
    @XmlElement(name = "event_name", required = true)
    public String eventName;
    @XmlElement(name = "event_id", required = true)
    public String eventId;
    @XmlElement(name = "participation_id", required = true)
    public String participationId;
    @XmlElement(required = true)
    public String acuity;
    @XmlElement(name = "jurisdictionId", required = true)
    public String jurisdictionId;
    @XmlElement(name = "workflow_state", required = true)
    public String workflowState;
    
    /**
     * Gets the value of the recordNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordNumber() {
        return recordNumber;
    }

    /**
     * Sets the value of the recordNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordNumber(String value) {
        this.recordNumber = value;
    }

    /**
     * Gets the value of the mmwrYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMmwrYear() {
        return mmwrYear;
    }

    /**
     * Sets the value of the mmwrYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMmwrYear(String value) {
        this.mmwrYear = value;
    }

    /**
     * Gets the value of the mmwrWeek property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMmwrWeek() {
        return mmwrWeek;
    }

    /**
     * Sets the value of the mmwrWeek property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMmwrWeek(String value) {
        this.mmwrWeek = value;
    }

    /**
     * Gets the value of the dateRecordCreated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateRecordCreated() {
        return dateRecordCreated;
    }

    /**
     * Sets the value of the dateRecordCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateRecordCreated(String value) {
        this.dateRecordCreated = value;
    }

    /**
     * Gets the value of the lhdCaseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLhdCaseStatus() {
        return lhdCaseStatus;
    }

    /**
     * Sets the value of the lhdCaseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLhdCaseStatus(String value) {
        this.lhdCaseStatus = value;
    }

    /**
     * Gets the value of the stateCaseStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateCaseStatus() {
        return stateCaseStatus;
    }

    /**
     * Sets the value of the stateCaseStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateCaseStatus(String value) {
        this.stateCaseStatus = value;
    }

    /**
     * Gets the value of the outbreakAssoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutbreakAssoc() {
        return outbreakAssoc;
    }

    /**
     * Sets the value of the outbreakAssoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutbreakAssoc(String value) {
        this.outbreakAssoc = value;
    }

    /**
     * Gets the value of the outbreak property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutbreak() {
        return outbreak;
    }

    /**
     * Sets the value of the outbreak property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutbreak(String value) {
        this.outbreak = value;
    }

    /**
     * Gets the value of the jurisdictionOfResidence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJurisdictionOfResidence() {
        return jurisdictionOfResidence;
    }

    /**
     * Sets the value of the jurisdictionOfResidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJurisdictionOfResidence(String value) {
        this.jurisdictionOfResidence = value;
    }
    
    /**
     * Gets the value of the jurisdictionOfResidence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJurisdictionId() {
        return jurisdictionId;
    }

    /**
     * Sets the value of the jurisdictionOfResidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJurisdictionId(String value) {
        this.jurisdictionId = value;
    }
    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the acuity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcuity() {
        return acuity;
    }

    /**
     * Sets the value of the acuity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcuity(String value) {
        this.acuity = value;
    }
    
    public String getEventId() {
        return eventId;
    }

    /**
     * Sets the value of the acuity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventId(String value) {
        this.eventId = value;
    }
    
    public String getParticipationId() {
        return participationId;
    }

    public void setWorkFlowState(String value) {
        this.workflowState = value;
    }
    
    public String getWorkFlowState() {
        return workflowState;
    }
    
    /**
     * Sets the value of the acuity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipationId(String value) {
        this.participationId = value;
    }
   

}
