package gov.utah.health.model;

import javax.xml.bind.annotation.*;

/**
 * Provides object for communicating the status of actions attempted by the qry agent.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "action",
    "status",
    "errorMessage"
})
@XmlRootElement(name = "status_message")
public final class StatusMessage {

    public StatusMessage() {
    }
    public StatusMessage(String action,String status,String errorMessage) {
        this.action = action;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    @XmlElement(required = true)
    protected String action;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(name = "error_message")
    protected String errorMessage;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


}
