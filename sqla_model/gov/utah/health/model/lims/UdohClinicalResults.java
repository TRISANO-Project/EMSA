package gov.utah.health.model.lims;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderNum",
    "patientExtraId",
    "patientLastName", 
    "patientFirstName", 
    "patientMidName", 
    "patientGender", 
    "patientDob", 
    "patientAge", 
    "customer", 
    "customerId",
    "stateOfOrig", 
    "sampleNumber",
    "specimenSource",
    "sampledDate",
    "sampleRecdDate",
    "analysis",
    "analysisReportedName",
    "testStatus",
    "testComment",
    "testReleasedDate",
    "resultName",
    "result",
    "resultLong",
    "resultReportable"
})
@XmlRootElement(name = "udoh_clinical_results")

    public class UdohClinicalResults implements Cloneable, Serializable {

    @XmlElement(name = "order_num", required = true)
    private String orderNum;
    @XmlElement(name = "patient_extra_id", required = true)
    private String patientExtraId;
    @XmlElement(name = "patient_last_name", required = true)
    private String patientLastName;
    @XmlElement(name = "patient_first_name", required = true)
    private String patientFirstName;
    @XmlElement(name = "patient_mid_name", required = true)
    private String patientMidName;
    @XmlElement(name = "patient_gender", required = true)
    private String patientGender;
    @XmlElement(name = "patient_dob", required = true)
    private Date patientDob;
    @XmlElement(name = "patient_age", required = true)
    private String patientAge;
    @XmlElement(name = "customer", required = true)
    private String customer;
    @XmlElement(name = "customer_id", required = true)
    private String customerId;
    @XmlElement(name = "state_of_orig", required = true)
    private String stateOfOrig;
    @XmlElement(name = "sample_number", required = true)
    private Float sampleNumber;
    @XmlElement(name = "specimen_source", required = true)
    private String specimenSource;
    @XmlElement(name = "sampled_date", required = true)
    private Date sampledDate;
    @XmlElement(name = "sample_recd_date", required = true)
    private Date sampleRecdDate;
    @XmlElement(name = "analysis", required = true)
    private String analysis;
    @XmlElement(name = "analysis_reported_name", required = true)
    private String analysisReportedName;
    @XmlElement(name = "test_status", required = true)
    private String testStatus;
    @XmlElement(name = "test_comment", required = true)
    private String testComment;
    @XmlElement(name = "test_released_date", required = true)
    private Date testReleasedDate;
    @XmlElement(name = "result_name", required = true)
    private String resultName;
    @XmlElement(name = "result", required = true)
    private String result;
    @XmlElement(name = "result_long", required = true)
    private String resultLong;
    @XmlElement(name = "result_reportable", required = true)
    private String resultReportable;

    public UdohClinicalResults() {
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public String getAnalysisReportedName() {
        return analysisReportedName;
    }

    public void setAnalysisReportedName(String analysisReportedName) {
        this.analysisReportedName = analysisReportedName;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public Date getPatientDob() {
        return patientDob;
    }

    public void setPatientDob(Date patientDob) {
        this.patientDob = patientDob;
    }

    public String getPatientExtraId() {
        return patientExtraId;
    }

    public void setPatientExtraId(String patientExtraId) {
        this.patientExtraId = patientExtraId;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMidName() {
        return patientMidName;
    }

    public void setPatientMidName(String patientMidName) {
        this.patientMidName = patientMidName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResultLong() {
        return resultLong;
    }

    public void setResultLong(String resultLong) {
        this.resultLong = resultLong;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public String getResultReportable() {
        return resultReportable;
    }

    public void setResultReportable(String resultReportable) {
        this.resultReportable = resultReportable;
    }

    public Float getSampleNumber() {
        return sampleNumber;
    }

    public void setSampleNumber(Float sampleNumber) {
        this.sampleNumber = sampleNumber;
    }

    public Date getSampleRecdDate() {
        return sampleRecdDate;
    }

    public void setSampleRecdDate(Date sampleRecdDate) {
        this.sampleRecdDate = sampleRecdDate;
    }

    public Date getSampledDate() {
        return sampledDate;
    }

    public void setSampledDate(Date sampledDate) {
        this.sampledDate = sampledDate;
    }

    public String getSpecimenSource() {
        return specimenSource;
    }

    public void setSpecimenSource(String specimenSource) {
        this.specimenSource = specimenSource;
    }

    public String getStateOfOrig() {
        return stateOfOrig;
    }

    public void setStateOfOrig(String stateOfOrig) {
        this.stateOfOrig = stateOfOrig;
    }

    public String getTestComment() {
        return testComment;
    }

    public void setTestComment(String testComment) {
        this.testComment = testComment;
    }

    public Date getTestReleasedDate() {
        return testReleasedDate;
    }

    public void setTestReleasedDate(Date testReleasedDate) {
        this.testReleasedDate = testReleasedDate;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

   

}
