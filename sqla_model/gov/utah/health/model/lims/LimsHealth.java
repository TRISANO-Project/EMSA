package gov.utah.health.model.lims;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Highest level of organization for lims data. Contains all data used in
 * accessing and updating lims.  HealthMessage contains a collection of
 * objects of this type for interchange of information. Supports
 * JAXB marshaling and unmarshaling. Members of this class are comprised of both
 * database-level classes as well as higher level classes used to organize data.
 * All of the members of this class support xml marshaling and unmarshaling.
 *
 * @author UDOH
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "udohClinicalResultsReport"
})
@XmlRootElement(name = "lims_health")
public class LimsHealth {
    

    @XmlElement(name = "udoh_clinical_results_report", required = true)
    protected List<UdohClinicalResultsReport> udohClinicalResultsReport;

    public List<UdohClinicalResultsReport> getUdohClinicalResultsReport() {
        return udohClinicalResultsReport;
    }

    public void setUdohClinicalResultsReport(List<UdohClinicalResultsReport> udohClinicalResultsReport) {
        this.udohClinicalResultsReport = udohClinicalResultsReport;
    }


}
