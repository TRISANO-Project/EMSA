package gov.utah.health.model.lims;

import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author UDOH
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "testReleasedDateStart",
    "testReleasedDateEnd",
    "limit",
    "matches",
    "orderBy",
    "ascDesc",
    "udohClinicalResults"
})
@XmlRootElement(name = "udoh_clinical_results_report")

    public class UdohClinicalResultsReport extends UdohClinicalResults {

    @XmlElement(name = "test_released_date_start", required = true)
    private Date testReleasedDateStart;
    @XmlElement(name = "test_released_date_end", required = true)
    private Date testReleasedDateEnd;
    @XmlElement(name = "limit", required = true)
    private Integer limit;
    @XmlElement(name = "matches", required = true)
    private Integer matches;
    @XmlElement(name = "udoh_clinical_results", required = true)
    private List<UdohClinicalResults> udohClinicalResults;
    @XmlElement(name = "order_by", required = true)
    private String orderBy;
    @XmlElement(name = "asc_desc", required = true)
    private String ascDesc;

    public UdohClinicalResultsReport() {
    }

    public String getAscDesc() {
        return ascDesc;
    }

    public void setAscDesc(String ascDesc) {
        this.ascDesc = ascDesc;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getMatches() {
        return matches;
    }

    public void setMatches(Integer matches) {
        this.matches = matches;
    }

    public Integer getLimit() {
        if(this.limit == null){this.limit = 10;}
        return limit;
    }

    public void setLimit(Integer resultLimit) {
        this.limit = resultLimit;
    }

    public Date getTestReleasedDateEnd() {
        return testReleasedDateEnd;
    }

    public void setTestReleasedDateEnd(Date testReleasedDateEnd) {
        this.testReleasedDateEnd = testReleasedDateEnd;
    }

    public Date getTestReleasedDateStart() {
        return testReleasedDateStart;
    }

    public void setTestReleasedDateStart(Date testReleasedDateStart) {
        this.testReleasedDateStart = testReleasedDateStart;
    }

    public List<UdohClinicalResults> getUdohClinicalResults() {
        return udohClinicalResults;
    }

    public void setUdohClinicalResults(List<UdohClinicalResults> udohClinicalResults) {
        this.udohClinicalResults = udohClinicalResults;
    }



}
