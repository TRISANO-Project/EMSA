package gov.utah.health.model.dashboard;

public class Bucket {

    public static Bucket EXCEPTION = new Bucket(1, "Exception");
    public static Bucket PENDING = new Bucket(2, "Pending");

    Bucket(Integer number, String name) {

        this.number = number;
        this.name = name;
    }
    private Integer number;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public boolean equals(Bucket bucket) {

        boolean equals = false;
        if (this.getNumber().equals(bucket.getNumber())) {
            equals = true;
        }

        return equals;
    }

    @Override
    public String toString() {
        return name.toString();
    }
}
