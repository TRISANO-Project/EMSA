package gov.utah.health.model.dashboard;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;

/**
 * Base class for all dashboard reports / modules
 *
 * @author UDOH
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bucketCounts",
    "buckets",
    "bucketsSelected",
    "diseaseCategories",
    "diseaseCategoriesSelected",
    "diseases",
    "diseasesSelected",
    "places",
    "placesSelected"
})
@XmlRootElement(name = "bucket_report")
public class BucketReport extends BucketCount {


    @XmlElement(name = "buckets", required = true)
    protected List<String> buckets;
    @XmlElement(name = "buckets_selected", required = true)
    protected List<String> bucketsSelected;
    @XmlElement(name = "disease_categories", required = true)
    protected List<String> diseaseCategories;
    @XmlElement(name = "disease_categories_selected", required = true)
    protected List<String> diseaseCategoriesSelected;
    @XmlElement(name = "diseases", required = true)
    protected List<String> diseases;
    @XmlElement(name = "diseases_selected", required = true)
    protected List<String> diseasesSelected;
    @XmlElement(name = "places", required = true)
    protected List<String> places;
    @XmlElement(name = "places_selected", required = true)
    protected List<String> placesSelected;

    @XmlElement(name = "bucket_counts", required = true)
    protected List<BucketCount> bucketCounts;

    public List<BucketCount> getBucketCounts() {
        return bucketCounts;
    }

    public void setBucketCounts(List<BucketCount> bucketCounts) {
        this.bucketCounts = bucketCounts;
    }

    public void addBucketCount(BucketCount bc) {

        if (this.bucketCounts == null) {
            this.bucketCounts = new ArrayList<BucketCount>();
        }
        this.bucketCounts.add(bc);
    }

    public List<String> getBuckets() {
        return buckets;
    }

    public void setBuckets(List<String> buckets) {
        this.buckets = buckets;
    }

    public List<String> getDiseaseCategories() {
        return diseaseCategories;
    }

    public void setDiseaseCategories(List<String> diseaseCategories) {
        this.diseaseCategories = diseaseCategories;
    }

    public List<String> getPlaces() {
        return places;
    }

    public void setPlaces(List<String> places) {
        this.places = places;
    }

    public List<String> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<String> diseases) {
        this.diseases = diseases;
    }

    public void addDisease(String diseaseName){
    
        if(this.diseases == null){
            this.diseases = new ArrayList<String>();
        }
        this.diseases.add(diseaseName);
    }

    public List<String> getBucketsSelected() {
        return bucketsSelected;
    }

    public void setBucketsSelected(List<String> bucketsSelected) {
        this.bucketsSelected = bucketsSelected;
    }

    public List<String> getDiseaseCategoriesSelected() {
        return diseaseCategoriesSelected;
    }

    public void setDiseaseCategoriesSelected(List<String> diseaseCategoriesSelected) {
        this.diseaseCategoriesSelected = diseaseCategoriesSelected;
    }

    public List<String> getDiseasesSelected() {
        return diseasesSelected;
    }

    public void setDiseasesSelected(List<String> diseasesSelected) {
        this.diseasesSelected = diseasesSelected;
    }

    public List<String> getPlacesSelected() {
        return placesSelected;
    }

    public void setPlacesSelected(List<String> placesSelected) {
        this.placesSelected = placesSelected;
    }
    
}
