package gov.utah.health.model.dashboard;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Base class for all dashboard reports / modules
 *
 * @author UDOH
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "start",
    "end",
    "count"
})
@XmlRootElement(name = "report")
public class Report {

    @XmlElement(name = "start", required = true)
    protected Date start;
    @XmlElement(name = "end", required = true)
    protected Date end;
    @XmlElement(name = "count", required = true)
    protected Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public String getStartFormatted() {

        String dateFormatted = "";

        if (this.start != null) {

            SimpleDateFormat sdfOut = new SimpleDateFormat("MM/dd/yyyy");

            if (this.start != null) {
                try {
                    dateFormatted = sdfOut.format(this.start);
                } catch (Exception e) {
                    dateFormatted = "";
                }
            }
        }

        return dateFormatted;

    }
    public String getEndFormatted() {

        String dateFormatted = "";

        if (this.end != null) {

            SimpleDateFormat sdfOut = new SimpleDateFormat("MM/dd/yyyy");

            if (this.end  != null) {
                try {
                    dateFormatted = sdfOut.format(this.end);
                } catch (Exception e) {
                    dateFormatted = "";
                }
            }
        }

        return dateFormatted;

    }

    public void setStart(Date start) {
        this.start = start;
    }
}
