package gov.utah.health.model.dashboard;

import java.util.GregorianCalendar;



public class Metric {

    public static Metric DAY       = new Metric(GregorianCalendar.DAY_OF_YEAR,"Daily");
    public static Metric WEEK      = new Metric(GregorianCalendar.WEEK_OF_YEAR,"Weekly");
    public static Metric MONTH     = new Metric(GregorianCalendar.MONTH,"Monthly");
    public static Metric YEAR      = new Metric(GregorianCalendar.YEAR,"Yearly");

    Metric(Integer number,String name){

        this.number=number;
        this.name=name;
    }

    private Integer number;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public boolean equals(Metric scope){

            boolean equals = false;
            if(this.getNumber().equals(scope.getNumber())){
                equals = true;
            }

            return equals;
    }

    @Override
    public String toString(){
        return name.toString();
    }
}
