package gov.utah.health.model.dashboard;

import java.util.GregorianCalendar;
import javax.xml.bind.annotation.*;

/**
 * Base class for all dashboard reports / modules
 *
 * @author UDOH
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "metric",
    "place",
    "diseaseCategory",
    "bucket"
})
@XmlRootElement(name = "bucket_report")
public class BucketCount extends Report {

    @XmlElement(name = "metric", required = true)
    protected String metric;
    @XmlElement(name = "place", required = true)
    protected String place;
    @XmlElement(name = "disease_category", required = true)
    protected String diseaseCategory;
    @XmlElement(name = "bucket", required = true)
    protected String bucket;

    public BucketCount() {}
    public BucketCount(String place, String metric, String diseaseCategory, String bucket) {

        this.place = place;
        this.diseaseCategory = diseaseCategory;
        this.metric = metric;
        this.bucket = bucket;

    }

    public String getDiseaseCategory() {
        return diseaseCategory;
    }

    public void setDiseaseCategory(String diseaseCategory) {
        this.diseaseCategory = diseaseCategory;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public int getMetricInt() {

        int m = -1;

        if(this.metric != null && this.metric.trim().length() > 0){

        if ("Daily".equals(this.metric)) {
            m = GregorianCalendar.DAY_OF_YEAR;
        } else if ("Weekly".equals(this.metric)) {
            m = GregorianCalendar.WEEK_OF_YEAR;
        } else if ("Monthly".equals(this.metric)) {
            m = GregorianCalendar.MONTH;
        } else if ("Yearly".equals(this.metric)) {
            m = GregorianCalendar.YEAR;
        }

        }

        return m;


    }

}
