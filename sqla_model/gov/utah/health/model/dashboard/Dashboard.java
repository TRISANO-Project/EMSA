package gov.utah.health.model.dashboard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Highest level of organization for dashboard data. HealthMessage contains on
 * object of this type. Supports JAXB marshaling and unmarshaling.
 *
 * @author UDOH
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bucketReport"
})
@XmlRootElement(name = "dashboard")
public class Dashboard {

    @XmlElement(name = "bucket_report", required = true)
    protected BucketReport bucketReport;

    public BucketReport getBucketReport() {
        return bucketReport;
    }

    public void setBucketReport(BucketReport bucketReport) {
        this.bucketReport = bucketReport;
    }


}
