package gov.utah.health.model;

import gov.utah.health.model.dashboard.Dashboard;
import gov.utah.health.model.lims.LimsHealth;
import gov.utah.health.model.master.Health;
import gov.utah.health.model.trisano.CdcExport;
import gov.utah.health.model.trisano.TrisanoHealth;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;

/**
 * Provides object for data interchange between this service and any clients.
 * This is the top-level class for xml marshaling and un-marshaling. General
 * message-level data is contained within this object used to convey general
 * information about the call to the service. System-specific data is also
 * contained in this message and is formatted for the relevant system.
 *
 * @author UDOH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userId",
    "username",
    "password",
    "system",
    "statusMessages",
    "health",
    "systemMessageId",
    "healthXml",
    "trisanoHealth",
    "limsHealth",
    "dashboard"
})
@XmlRootElement(name = "health_message")
public final class HealthMessage {

    public HealthMessage() {
    }

    public HealthMessage(CdcExport cdce) {
        this(new TrisanoHealth());
        this.trisanoHealth.get(0).setCdcExport(cdce);
    }

    public HealthMessage(TrisanoHealth th) {
        List<TrisanoHealth> thl = new ArrayList<TrisanoHealth>();
        thl.add(th);
        this.setTrisanoHealth(thl);
    }
    @XmlElement(required = true)
    protected String system;
    @XmlElement(name = "status_message", required = true)
    protected List<StatusMessage> statusMessages;
    @XmlElement(name = "user_id", required = true)
    protected Integer userId;
    @XmlElement(required = true)
    protected String username;
    @XmlElement(required = true)
    protected String password;
    @XmlElement(name = "trisano_health", required = true)
    protected List<TrisanoHealth> trisanoHealth = new ArrayList<TrisanoHealth>();
    @XmlElement(name = "health", required = true)
    protected List<Health> health = new ArrayList<Health>();
    @XmlElement(name = "system_message_id", required = true)
    protected Integer systemMessageId;
    @XmlElement(name = "health_xml", required = true)
    protected String healthXml;
    @XmlElement(name = "lims_health", required = true)
    protected List<LimsHealth> limsHealth = new ArrayList<LimsHealth>();
    @XmlElement(name = "dashboard", required = true)
    protected Dashboard dashboard;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<StatusMessage> getStatusMessages() {
        return statusMessages;
    }

    public void setStatusMessages(List<StatusMessage> statusMessages) {
        this.statusMessages = statusMessages;
    }

    public void addStatusMessage(StatusMessage statusMessage) {
        if (this.statusMessages == null) {
            this.statusMessages = new ArrayList<StatusMessage>();
        }
        this.statusMessages.add(statusMessage);
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Health> getHealth() {
        return health;
    }

    public void setHealth(List<Health> health) {
        this.health = health;
    }

    public Integer getSystemMessageId() {
        return systemMessageId;
    }

    public void setSystemMessageId(Integer systemMessageId) {
        this.systemMessageId = systemMessageId;
    }

    public String getHealthXml() {
        return healthXml;
    }

    public void setHealthXml(String healthXml) {
        this.healthXml = healthXml;
    }

    public List<TrisanoHealth> getTrisanoHealth() {
        return trisanoHealth;
    }

    public TrisanoHealth getFirstTrisanoHealth() {
        TrisanoHealth firstTh = null;
        if (trisanoHealth != null && !trisanoHealth.isEmpty()) {
            firstTh = trisanoHealth.get(0);
        }
        return firstTh;
    }

    public void setTrisanoHealth(List<TrisanoHealth> trisanoHealth) {
        this.trisanoHealth = trisanoHealth;
    }

    public void addTrisanoHealth(TrisanoHealth trisanoHealth) {
        if (this.trisanoHealth == null) {
            this.trisanoHealth = new ArrayList<TrisanoHealth>();
        }
        this.trisanoHealth.add(trisanoHealth);
    }

    public int getHealthSize() {
        int healthSize = 0;
        if (system.equals("TRISANO")) {
            healthSize = trisanoHealth.size();
        }
        return healthSize;
    }

    public List<LimsHealth> getLimsHealth() {
        return limsHealth;
    }

    public void setLimsHealth(List<LimsHealth> limsHealth) {
        this.limsHealth = limsHealth;
    }

    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
        this.system = "DASHBOARD";
    }
    
    public boolean addUpdateSuccess(){
        boolean status = false;
        
        if(this.statusMessages != null){
            for(StatusMessage sm : this.statusMessages){
                if((sm.getAction().equals("addCmr") || sm.getAction().equals("updateCmr")) && (sm.getStatus().equals(Status.SUCCESS.toString()))){
                    status = true;
                }
            }
        }
        return status;
    
    }
}
