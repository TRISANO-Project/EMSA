package gov.utah.health.model;

/**
 * Provides statuses used to communicate information between the service and clients
 * of this service.
 * Provides specific information regarding successful and unsuccessful operations.
 *
 * @author UDOH
 */
public class Status {

    public static Status SUCCESS                = new Status(100,"SUCCESS");
    public static Status FAILURE_EXCEPTION      = new Status(102,"FAILURE EXCEPTION");
    public static Status FAILURE_MISSING_DATA   = new Status(103,"Data missing in request.");
    public static Status FAILURE_MISSING_SYSTEM = new Status(104,"System missing in request.");
    public static Status FAILURE_JURISDICTION_NOT_FOUND = new Status(105,"Jurisdiction not found.");
    public static Status FAILURE_UNSUPPORTED    = new Status(106,"Function not supported.");
    public static Status SEARCH_QUICK           = new Status(107,"Perform quick search.");
    public static Status MAX_RESULTS_EXCEEDED   = new Status(108,"Maximum results exceeded.");
    public static Status FAILURE_MASTER_EXCEPTIONS      = new Status(109,"FAILURE MASTER EXCEPTIONS");
    public static Status FAILURE_TRISANO_ERROR      = new Status(110,"FAILURE_TRISANO_ERROR");


    Status(Integer code,String statusMessage){

        this.code=code;
        this.statusMessage=statusMessage;
    }

    private Integer code;
    private String statusMessage;

    public Integer getCode() {
        return code;
    }
    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }


    public boolean equals(Status status){

            boolean equals = false;
            if(status.getCode().equals(status.getCode())){
                equals = true;
            }

            return equals;
    }

    @Override
    public String toString(){
        return code.toString();
    }
}
