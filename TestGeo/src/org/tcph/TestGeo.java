/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tcph;

import java.io.InputStream;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author jayb1
 */
public class TestGeo {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        assertAddressResolves("100 Main St", "Ft Worth", "TX", "76102", "32.75680", "-97.33304");
        assertAddressResolves("1520 North Cooper St", "Arlington", "TX", "76012", "32.75835", "-97.11504");
        assertAddressResolves("223 W Kennedale Pkwy", "Kennedale", "TX", "76060", "32.64831", "-97.22429");
        assertAddressResolves("6162 Meadowbrook Dr", "Fort Worth", "TX", "76112", "32.74576", "-97.22333");
        assertAddressResolves("1101 S Main St", "Fort Worth", "TX", "76104", "32.73232", "-97.32462");
        assertAddressResolves("3120 S University Dr", "Fort Worth", "TX", "76109", "32.70480", "-97.36101");
        assertAddressFails("Rural route 7, box 160", "Horton", "IA", "999");
        assertAddressFails("PO Box 1001", "Fort Worth", "TX", "76121");
        assertAddressFails("7513 15th Ave NE", "Seattle", "WA", "98115");
        assertAddressFails("63 Voltaire Ave", "Henderson", "NV", "89002");
        assertAddressFails("Coast Guard Cutter Diligence", "APO 5302", "AP", "96522");
    }
    
    public static void assertAddressResolves(String street, String city, String state, String zip, String lat, String lon) {
        String jsonResult = getGeocode(street, city, state, zip);
        String fullAddress = street + " " + city + " " + state + " " + zip;
        String latRetrieved = "", lonRetrieved = "";
        
        try {
            JSONArray a = new JSONArray(jsonResult);
            JSONObject objRoot = a.getJSONObject(0);
            JSONObject objMeta = objRoot.getJSONObject("metadata");
            latRetrieved = objMeta.getString("latitude");
            lonRetrieved = objMeta.getString("longitude");
        } catch (Exception ex) {
            System.err.println("Error obtaining geocode for: " + fullAddress);
            return;
        }
        
        // check to match the number of digits present in the test data
        if(latRetrieved.substring(0, 8).equals(lat) && lonRetrieved.substring(0,9).equals(lon)) {
            System.out.println("Success: " + fullAddress + " " +latRetrieved + "," + lonRetrieved);
        } else {
            System.err.println("Address lat/long does not match, found: (" + latRetrieved + "," + lonRetrieved  + ") expected: " +
                    lat + "," + lon);
        }
    }
    
    public static void assertAddressFails(String street, String city, String state, String zip) {
        String jsonResult = getGeocode(street, city, state, zip);
        String fullAddress = street + " " + city + " " + state + " " + zip;
        if(jsonResult.equals("[]")) {
            System.out.println("Success identifying OOJ/Bad address: " + fullAddress);
        } else if (jsonResult.length() == 0 || jsonResult.substring(0, 9).equals("Could not")) {
            System.err.println("Geocode web service call failed!!!: " + fullAddress);
        } else {
            System.err.println("Address should NOT have resolved!!!: " + fullAddress);
        }
    }
    
    public static String getGeocode(String street, String city, String state, String zip) {
        
        String strRet = "";
        
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            String qry = "http://hllqryagnt:8080/geo/find?street_name="
                    + URLEncoder.encode(street, "UTF-8")
                    + "&city=" + URLEncoder.encode(city, "UTF-8")
                    + "&state=" + URLEncoder.encode(state, "UTF-8")
                    + "&zip=" + URLEncoder.encode(zip, "UTF-8");
            HttpGet httpGet = new HttpGet(qry);

            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();

            if (response.getStatusLine().toString().indexOf("200 OK") > 0 && entity != null) {
                InputStream is = entity.getContent();
                strRet = IOUtils.toString(is, "UTF-8");
            } else {
                strRet = "Could not resolve address.";
            }


        } catch (Exception e) {
            System.err.println("Error calling geocode: " + e.getMessage());
        } 
        return strRet;
    }
}
