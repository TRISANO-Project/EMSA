package gov.utah.health.data.elr;

import gov.utah.health.model.master.MasterAudit;
import gov.utah.health.model.master.MasterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author UDOH
 */
public class ElrAuditDao {

    public static final Logger logger = Logger.getLogger("ElrDaoImpl");
    public static final ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    public static void addAudit(Connection con, MasterAudit audit) {

        Integer auditId = 0;
        List<MasterException> exList = audit.getExList();
        if (con != null) {
            String sql = "INSERT INTO elr.system_messages_audits "
                    + "(user_id,message_action_id,system_message_id,created_at,lab_id,system_status_id)"
                    + " values (?,?,?,now(),?,?)";

            PreparedStatement ps = null;
            ResultSet keyset = null;

            try {
                ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, audit.getUserId());
                ps.setInt(2, audit.getMessageActionId());
                ps.setInt(3, audit.getSystemMessageId());
                ps.setInt(4, audit.getLabId());
                ps.setInt(5, audit.getSystemStatusId());

                ps.executeUpdate();
                keyset = ps.getGeneratedKeys();
                if (keyset.next()) {
                    // Retrieve the auto generated key(s).
                    auditId = keyset.getInt(1);
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Cannot execute qry", e);
            } finally {
                try {
                    if (keyset != null) {
                        keyset.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException e) {
                }
            }
            if (exList != null && !exList.isEmpty() && auditId > 0) {

                for (MasterException ex : exList) {

                    String exsql = "INSERT INTO elr.system_audit_exceptions "
                            + "(system_messages_audits_id,system_exceptions_id,info)"
                            + " values (?,?,?)";

                    try {
                        ps = con.prepareStatement(exsql);
                        ps.setInt(1, auditId);
                        ps.setInt(2, ex.getId());
                        ps.setString(3, ex.getMessage());
                        ps.executeUpdate();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Cannot execute qry", e);
                    } finally {
                        try {
                            if (ps != null) {
                                ps.close();
                            }
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "Exception in close rs or ps", e);
                        }
                    }
                }

            }


        }
    }
}
