package gov.utah.health.data.elr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 *
 * @author admin
 */
public final class ElrConnect {

    private static DataSource ds = null;
    public static final Logger logger = Logger.getLogger("ElrConnect");

    static {
        try {
            InitialContext ic = new InitialContext();
            ds = (DataSource) ic.lookup("elr");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Static initialization error.",e);
            ds = null;
        }

    }

    public static Connection getConnection() {
        Connection con = null;
        if (ds != null) {
            try {
                con = ds.getConnection();
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error getting a connection from datasource.", e);
                if(con != null) {
                    try {
                        con.close();
                    } catch (SQLException se) {
                        logger.log(Level.SEVERE, "Error closing connection after error in get connection", se);
                    }
                }
            }
        }
        return con;
    }
}