package gov.utah.health.data.elr;

import gov.utah.health.model.master.Health;
import gov.utah.health.model.master.MasterDataType;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterList;
import gov.utah.health.model.master.MasterMirthPath;
import gov.utah.health.model.master.MasterPath;
import gov.utah.health.model.master.MasterPathRuleSet;
import gov.utah.health.model.master.MasterRule;
import gov.utah.health.model.master.MasterStatus;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.model.master.MirthPath;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author UDOH
 */
public class ElrDao {

    public static final Logger logger = Logger.getLogger("ElrDao");
    public static final ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    public static Integer addMessage(Connection con, Health health, String masterXml, Integer originalMessageId, Integer labId, Integer masterListId, List<MasterException> exList) {
        String labSegmentIndex = health.getLabSegmentIndex();
        Integer messageId;
        Integer finalStatus = getFinalStatus(exList, masterListId);
        messageId = ElrDao.getSystemMessageId(con, originalMessageId, labSegmentIndex);

        if (messageId == -1) {

            String sql = "INSERT INTO elr.system_messages (fname,lname,master_xml,original_message_id,lab_id,final_status,segment_index) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = null;
            try {

                ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, health.getPerson().getFirstName());
                ps.setString(2, health.getPerson().getLastName());
                ps.setString(3, masterXml);
                ps.setInt(4, originalMessageId);
                ps.setInt(5, labId);
                ps.setInt(6, finalStatus);
                ps.setString(7, labSegmentIndex);

                int u = ps.executeUpdate();

                ResultSet keyset = ps.getGeneratedKeys();
                if (keyset.next()) {
                    // Retrieve the auto generated key(s).
                    messageId = keyset.getInt(1);
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Cannot execute qry", e);
            } finally {

                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close ps", e);
                    }

                }

            }
        }
        return messageId;
    }

        private static Integer getSystemMessageId(Connection con, Integer originalMessageId, String labSegmentIndex) {
        Integer systemMessageId = -1;
        String sql = "SELECT id FROM elr.system_messages WHERE original_message_id = ?";
        if (labSegmentIndex != null) {
            sql = sql + " AND segment_index = ?";
        }
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, originalMessageId);
            if (labSegmentIndex != null) {
                ps.setString(2, labSegmentIndex);
            }
            rs = ps.executeQuery();
            if (rs.next()) {
                systemMessageId = rs.getInt("id");
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error in qry to get systemMessageId.", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }

            }
        }
        return systemMessageId;
    }

    public static Integer updateMessage(Connection con, Integer messageId, String masterXml, List<MasterException> exceptions, Integer finalStatus, String masterLoinc, String clinician) {

        finalStatus = getFinalStatus(exceptions, finalStatus);
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE elr.system_messages SET master_xml=?,final_status=?,loinc_code=?,clinician=? WHERE id=?";

            ps = con.prepareStatement(sql);
            ps.setString(1, masterXml);
            ps.setInt(2, finalStatus);
            ps.setString(3, masterLoinc);
            ps.setString(4, clinician);
            ps.setInt(5, messageId);
            ps.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Unable to close ps or rs.");
            }
        }

        try {
            String sql = "DELETE FROM elr.system_message_exceptions WHERE system_message_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, messageId);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Unable to close ps or rs.");
            }
        }

        if (!exceptions.isEmpty()) {

            for (MasterException ex : exceptions) {
                try {
                    String sql = "INSERT INTO elr.system_message_exceptions (system_message_id,exception_id,info) VALUES (?,?,?)";
                    ps = con.prepareStatement(sql);
                    ps.setInt(1, messageId);
                    ps.setInt(2, ex.getId());
                    ps.setString(3, ex.getMessage());
                    ps.executeUpdate();

                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Cannot execute qry", e);
                } finally {
                    try {
                        if (ps != null) {
                            ps.close();
                        }
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Unable to close ps or rs.");
                    }
                }
            }
        }
        return finalStatus;
    }

    public static Integer setSystemMessageColumnValue(Connection con, Integer messageId, String column, Object value) {

        PreparedStatement ps = null;
        Integer updates = -1;
        try {
            String sql = "UPDATE elr.system_messages SET "+column+"=? WHERE id=?";

            ps = con.prepareStatement(sql);
            if(value instanceof String){
                ps.setString(1, (String) value);
            }else if(value instanceof Integer){
                ps.setInt(1,(Integer) value);
            }else if(value instanceof Date){
                ps.setDate(1,(Date) value);
            }
            
            ps.setInt(2, messageId);
            updates = ps.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry in ElrDao.setSystemMessageColumnValue", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Unable to close ps or rs.");
            }
        }
        return updates;
    }
    
    
    public static Integer updateMessageStatus(Connection con, Integer messageId, List<MasterException> exceptions, Integer finalStatus) {
        Integer updates = 0;
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE elr.system_messages SET final_status=?,disease=? WHERE id=?";

            ps = con.prepareStatement(sql);
            ps.setInt(1, finalStatus);
            ps.setString(2, "Unknown");
            ps.setInt(3, messageId);
            updates = ps.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Unable to close ps or rs.");
            }
        }

        try {
            String sql = "DELETE FROM elr.system_message_exceptions WHERE system_message_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, messageId);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Unable to close ps or rs.");
            }
        }

        if (!exceptions.isEmpty()) {

            for (MasterException ex : exceptions) {
                try {
                    String sql = "INSERT INTO elr.system_message_exceptions (system_message_id,exception_id,info) VALUES (?,?,?)";
                    ps = con.prepareStatement(sql);
                    ps.setInt(1, messageId);
                    ps.setInt(2, ex.getId());
                    ps.setString(3, ex.getMessage());
                    ps.executeUpdate();

                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Cannot execute qry", e);
                } finally {
                    try {
                        if (ps != null) {
                            ps.close();
                        }
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Unable to close ps or rs.");
                    }
                }
            }
        }
        return updates;
    }

    public static Integer deleteMessage(Connection con, Integer messageId) {

        Integer updates = 0;
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM elr.system_message_exceptions WHERE system_message_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, messageId);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Unable to close ps or rs.");
            }
        }

        try {
            String sql = "DELETE FROM elr.system_messages WHERE id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, messageId);
            updates = ps.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Unable to close ps or rs.");
            }
        }
        return updates;
    }

    public static List<MasterMirthPath> getMasterMirthPaths(Connection con, Integer labId, String version) {

        List<MasterMirthPath> pList = new ArrayList<MasterMirthPath>();

        if (con != null) {
            String sql = "SELECT pm.*,p.xpath as master_path FROM elr.structure_path_mirth pm, elr.structure_path p WHERE pm.master_path_id=p.id AND pm.lab_id = ? AND pm.message_version=? ORDER BY pm.xpath,pm.sequence";
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {

                ps = con.prepareStatement(sql);
                ps.setInt(1, labId);
                ps.setString(2, version);
                rs = ps.executeQuery();

                int lastId = 0;
                MasterMirthPath path = null;

                while (rs.next()) {

                    int pid = rs.getInt("master_path_id");
                    if (lastId != pid) {
                        path = new MasterMirthPath();
                        path.setMasterPath(rs.getString("master_path"));
                        pList.add(path);
                        lastId = pid;
                    }
                    MirthPath mirthPath = new MirthPath();
                    mirthPath.setGlueString(rs.getString("glue_string"));
                    mirthPath.setId(rs.getInt("id"));
                    mirthPath.setLabId(rs.getInt("lab_id"));
                    mirthPath.setMasterPathId(pid);
                    mirthPath.setMasterPath(rs.getString("master_path"));
                    mirthPath.setMessageVersion(rs.getString("message_version"));
                    mirthPath.setSequence(rs.getInt("sequence"));
                    mirthPath.setXpath(rs.getString("xpath"));
                    path.addMirthPath(mirthPath);

                }

            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Cannot execute qry", e);
            } finally {

                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close rs", e);
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close ps", e);
                    }

                }
            }

        }

        return pList;
    }

    public static List<MasterTrisanoPath> getMasterTrisanoPaths(Connection con) {

        List<MasterTrisanoPath> list = new ArrayList<MasterTrisanoPath>();

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT a.id as app_id,a.xpath as app_xpath,p.xpath as master_path,p.id as master_id, a.structure_lookup_operator_id as operator_id, sca.app_category as app_category, sca.app_table as app_table ");
        sql.append("FROM elr.structure_path_application a ");
        sql.append("LEFT JOIN elr.structure_path p ON a.structure_path_id = p.id ");
        sql.append("LEFT JOIN elr.structure_category_application sca  ON sca.id = a.category_application_id");

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            while (rs.next()) {

                MasterTrisanoPath mtp =
                        new MasterTrisanoPath(rs.getString("master_path"), rs.getInt("master_id"),
                        rs.getString("app_xpath"), rs.getInt("app_id"),
                        rs.getInt("operator_id"), rs.getString("app_table"), rs.getString("app_category"));

                list.add(mtp);
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }

            }
        }
        return list;

    }

    public static List<MasterPath> getMasterPathsWithCategory(Connection con) {

        List<MasterPath> pList = new ArrayList<MasterPath>();
        String sql = "SELECT * FROM elr.structure_path p WHERE p.category_id is not null";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            MasterPath masterPath;

            while (rs.next()) {
                masterPath = new MasterPath();
                masterPath.setId(rs.getInt("id"));
                masterPath.setXpath(rs.getString("xpath"));
                masterPath.setDataTypeId(rs.getInt("data_type_id"));
                masterPath.setElement(rs.getString("element"));
                masterPath.setCategoryId(rs.getInt("category_id"));
                pList.add(masterPath);
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }

            }
        }

        return pList;
    }

    public static String getMasterConcept(Connection con, Integer systemMessageId, String value, Integer categoryId) {

        String masterConcept = null;

        if (systemMessageId != null && value != null && categoryId != null) {

            String sql = "SELECT elr.get_master_concept(?,?,?) AS concept";

            PreparedStatement ps = null;
            ResultSet rs = null;
            try {

                ps = con.prepareStatement(sql.toString());

                ps.setInt(1, systemMessageId);
                ps.setString(2, value.trim());
                ps.setInt(3, categoryId);

                rs = ps.executeQuery();

                if (rs.next()) {
                    masterConcept = rs.getString("concept");
                }

            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Cannot execute qry", e);
            } finally {

                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close rs", e);
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close ps", e);
                    }

                }
            }
        }

        return masterConcept;
    }

    public static Integer getTestResultByNominalOrganism(Connection con, String localResultValue, Integer labId) {
        Integer result = null;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT elr.get_nomord_testresult(?,?) as result");

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sql.toString());
            ps.setString(1, localResultValue);
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getInt("result");
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }

            }
        }
        return result;

    }

    public static String getNomordResultValue(Connection con, String resultValue, Integer labId) {
        String noResultValue = null;

        String sql = "SELECT elr.get_nomord_resultvalue(?,?) AS result_value";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sql);
            ps.setString(1, resultValue);
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                noResultValue = rs.getString("result_value");
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }

            }
        }
        return noResultValue;

    }

    public static String getNomordComments(Connection con, String resultValue, Integer labId) {
        String comments = null;

        String sql = "SELECT elr.get_nomord_comments(?,?) AS comments";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sql);
            ps.setString(1, resultValue);
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                comments = rs.getString("comments");
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }

            }
        }
        return comments;

    }

    public static Boolean getInterpretResultsForLoinc(Connection con, String localLoinc, Integer labId) {
        Boolean result = false;

        String sql = "SELECT elr.get_interpret_results_for_child_loinc(?,?) AS interpret_results";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = con.prepareStatement(sql);
            ps.setString(1, localLoinc);
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getBoolean("interpret_results");
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }

            }
        }
        return result;

    }

    public static String getMasterXml(Connection con, Integer masterId) throws SQLException {

        String xml = null;
        String sql = "SELECT master_xml FROM elr.system_messages WHERE id = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, masterId);
            rs = ps.executeQuery();
            if (rs.next()) {
                xml = rs.getString("master_xml");
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error in qry to get master xml.", e);
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        return xml;

    }

    public static String getLocalResultValue(Connection con, String childLoinc, String localResultValue1, String localResultValue2, Integer labId) throws SQLException {

        String localResultValue = null;

        if (con != null) {
            String sql = "SELECT elr.get_local_result_value(?,?,?,?) AS local_result_value";
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, childLoinc);
                ps.setString(2, localResultValue1);
                ps.setString(3, localResultValue2);
                ps.setInt(4, labId);
                rs = ps.executeQuery();
                if (rs.next()) {
                    localResultValue = rs.getString("local_result_value");
                }
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close rs", e);
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close ps", e);
                    }
                }
            }
        }
        return localResultValue;

    }

    public static Integer getLabId(Connection con, String labName) throws SQLException {

        Integer labId = 0;

        if (con != null) {
            String sql = "SELECT elr.get_lab_id_from_lab_name(?) AS lab_id";
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, labName.trim());
                rs = ps.executeQuery();
                if (rs.next()) {
                    labId = rs.getInt("lab_id");
                }
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close rs", e);
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close ps", e);
                    }
                }
            }
        }
        return labId;

    }

    public static String getMasterLoinc(Connection con, String localLoinc, Integer labId) throws SQLException {

        String masterLoinc = null;
        String sql = "SELECT elr.get_master_loinc(?,?) AS loinc";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, localLoinc.trim());
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                masterLoinc = rs.getString("loinc");
                if (masterLoinc != null && masterLoinc.trim().length() > 0) {
                    masterLoinc = masterLoinc.trim();
                }
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }

        }
        return masterLoinc;

    }

    public static String getMasterConditionFromMasterLoinc(Connection con, String masterLoinc) throws SQLException {

        String masterCondition = null;
        String sql = "SELECT elr.get_master_concept_from_master_loinc(?) AS concept";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, masterLoinc);
            rs = ps.executeQuery();
            if (rs.next()) {
                masterCondition = rs.getString("concept");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        return masterCondition;
    }

    public static Integer getLabId(Connection con, Integer systemMessageId) throws SQLException {

        Integer labId = 0;
        String sql = "SELECT elr.get_lab_id_from_system_message_id(?) AS lab_id";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, systemMessageId);
            rs = ps.executeQuery();
            if (rs.next()) {
                labId = rs.getInt("lab_id");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }

        }
        return labId;

    }

    public static Integer getJurisdictionIdFromZip(Connection con, String zip) throws SQLException {

        Integer jurisId = 0;
        String sql = "SELECT elr.get_jurisdiction_id_from_zip(?) AS jid";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, zip);
            rs = ps.executeQuery();
            if (rs.next()) {
                jurisId = rs.getInt("jid");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }

        }
        return jurisId;

    }

    public static Integer getJurisdictionIdFromLabId(Connection con, Integer labId) throws SQLException {

        Integer jurisId = 0;
        String sql = "select elr.get_jurisdiction_id_from_lab_id(?) as jid";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                jurisId = rs.getInt("jid");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        return jurisId;

    }

    public static java.util.Date getReportDate(Connection con, Integer originalMessageId) throws SQLException {

        Date reportDate = null;

        if (con != null) {
            String sql = "SELECT elr.get_report_date(?) as report_date";
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                ps = con.prepareStatement(sql);
                ps.setInt(1, originalMessageId);
                rs = ps.executeQuery();
                if (rs.next()) {
                    Timestamp ts = rs.getTimestamp("report_date");
                    if (ts != null) {
                        reportDate = new Date(ts.getTime());
                    }

                }
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close rs", e);
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close ps", e);
                    }
                }
            }

        }
        return reportDate;

    }

    public static void saveTrisanoXml(Connection con, Integer messageId, String trisanoXml) {

        PreparedStatement ps = null;
        try {
            String sql = "UPDATE elr.system_messages SET transformed_xml=? WHERE id=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, trisanoXml);
            ps.setInt(2, messageId);
            ps.executeUpdate();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {

            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
    }

    public static String getAppCode(Connection con, String childValue, Integer appPathId, Integer labId, Integer appId) throws SQLException {

        String code = null;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT elr.get_app_code(?,?,?,?) AS coded_value");
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql.toString());
            ps.setString(1, childValue);
            ps.setInt(2, appPathId);
            ps.setInt(3, labId);
            ps.setInt(4, appId);
            rs = ps.executeQuery();
            if (rs.next()) {
                code = rs.getString("coded_value");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        return code;
    }

    public static String getResultValueLocation(Connection con, String localLoinc, Integer labId) throws SQLException {

        String concept = null;
        String sql = "SELECT elr.get_result_value_location(?,?) AS concept";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, localLoinc);
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                concept = rs.getString("concept");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        return concept;

    }

    public static List<MasterRule> getTestResultRules(Connection con, String childLoinc, Integer labId) throws SQLException {

        List<MasterRule> rules = new ArrayList<MasterRule>();

        String sql = "SELECT vct.conditions_js, vct.master_id, vct.results_to_comments  "
                + "FROM elr.vocab_c2m_testresult vct, elr.vocab_child_loinc vcl "
                + "WHERE vct.child_loinc_id = vcl.id AND "
                + "vcl.child_loinc = ? AND "
                + "vct.app_id = 1 AND "
                + "vcl.lab_id = ? AND vcl.archived = false";

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, childLoinc);
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            while (rs.next()) {
                MasterRule mr = new MasterRule();
                mr.setMasterVocabId(rs.getInt("master_id"));
                mr.setOperation(rs.getString("conditions_js"));
                mr.setResultsToComments(rs.getString("results_to_comments"));
                rules.add(mr);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        return rules;

    }
    public static String getStateCaseStatus(Connection con, String localLoinc, String localResultValue, Integer labId) throws SQLException {

        String status = null;
        String sql = "SELECT elr.get_state_case_status_nominal(?,?,?,1) AS status";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, localLoinc);
            ps.setString(2, localResultValue);
            ps.setInt(3, labId);

            rs = ps.executeQuery();
            if (rs.next()) {
                status = rs.getString("status");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return status;

    }

    public static List<MasterRule> getMasterLoincRules(Connection con, String masterLoinc) throws SQLException {

        List<MasterRule> rules = new ArrayList<MasterRule>();

        String sql = "SELECT vrm.conditions_js, vrm.state_case_status_master_id  "
                + "FROM elr.vocab_rules_masterloinc vrm, elr.vocab_master_loinc vml "
                + "WHERE vml.l_id = vrm.master_loinc_id AND "
                + "vml.loinc = ? AND "
                + "vrm.app_id = 1";

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, masterLoinc);
            rs = ps.executeQuery();
            while (rs.next()) {
                MasterRule mr = new MasterRule();
                mr.setMasterVocabId(rs.getInt("state_case_status_master_id"));
                mr.setOperation(rs.getString("conditions_js"));
                rules.add(mr);
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        return rules;

    }

    public static String getAppCodedValueFromMasterVocabId(Connection con, Integer masterVocabId, Integer appId) throws SQLException {

        String codedValue = null;
        String sql = "SELECT elr.get_app_coded_value_from_master_vocab_id(?,?) AS coded_value";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, masterVocabId);
            ps.setInt(2, appId);

            rs = ps.executeQuery();
            if (rs.next()) {
                codedValue = rs.getString("coded_value");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return codedValue;

    }

    public static String getTrisnaoOrganismName(Connection con, String childLoinc, Integer labId, String localResultValue) throws SQLException {

        String organismName = null;
        String sql = "SELECT elr.loinc_organism(?,?,?) as trisano_organism";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);

            ps.setString(1, childLoinc);
            ps.setString(2, localResultValue);
            ps.setInt(3, labId);

            rs = ps.executeQuery();
            if (rs.next()) {
                organismName = rs.getString("trisano_organism");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return organismName;

    }

    public static String getTrisanoDiseaseName(Connection con, String childLoinc, Integer labId, String localResultValue) throws SQLException {

        String code = null;
        String sql = "SELECT elr.loinc_condition(?,?,?) as trisano_condition";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, childLoinc);
            ps.setString(2, localResultValue);
            ps.setInt(3, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                code = rs.getString("trisano_condition");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return code;

    }

    public static String getMasterVocabCodedValue(Connection con, Integer stateCaseStatusMasterId) throws SQLException {

        String status = null;

        String sql = "SELECT elr.get_master_vocab_coded_value(?,?) AS coded_value";

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, stateCaseStatusMasterId);
            ps.setInt(2, 1);// appId
            rs = ps.executeQuery();
            if (rs.next()) {
                String s = rs.getString("coded_value");
                if (s != null && s.trim().length() > 0) {
                    status = s;
                }
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return status;

    }

    public static String getTestType(Connection con, String masterLoinc) throws SQLException {

        String testType = null;

        String sql = "SELECT elr.get_test_type(?,?) AS coded_value";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, masterLoinc);
            ps.setInt(2, 1);//appId
            rs = ps.executeQuery();
            if (rs.next()) {
                String s = rs.getString("coded_value");
                if (s != null && s.trim().length() > 0) {
                    testType = s;
                }
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return testType;

    }

    public static String getSpecimenSource(Connection con, String masterLoinc) throws SQLException {

        String ss = null;

        String sql = "SELECT elr.get_specimen_source(?,?) AS coded_value";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, masterLoinc);
            ps.setInt(2, 1);//appId
            rs = ps.executeQuery();
            if (rs.next()) {
                String s = rs.getString("coded_value");
                if (s != null && s.trim().length() > 0) {
                    ss = s;
                }
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return ss;

    }

    public static Boolean getSourceRequired(Connection con, String childLoinc, String resultValue, Integer labId) throws SQLException {

        Boolean req = null;

        String sql = "SELECT elr.get_source_required(?,?,?) AS require_specimen";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, childLoinc);
            ps.setString(2, resultValue);
            ps.setInt(3, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                req = rs.getBoolean("require_specimen");
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return req;

    }

    public static String getReferenceRange(Connection con, String childLoinc, Integer labId) throws SQLException {

        String refRange = null;

        String sql = "SELECT elr.get_reference_range(?,?) AS refrange";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, childLoinc);
            ps.setInt(2, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                String rRange = rs.getString("refrange");
                if (rRange != null && rRange.trim().length() > 0) {
                    refRange = rRange;
                }
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return refRange;

    }

    public static Boolean isValidPostalCode(Connection con, String postalCode) throws SQLException {

        Boolean valid = false;

        String sql = "SELECT zipcode  "
                + "FROM elr.system_zip_codes "
                + "WHERE "
                + "zipcode = ? ";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, postalCode);
            rs = ps.executeQuery();
            if (rs.next()) {
                String z = rs.getString("zipcode");
                if (z != null && z.trim().length() > 0) {
                    valid = true;
                }
            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }

        return valid;

    }

    public static List<MasterPathRuleSet> getMasterPathValidators(Connection con) {

        List<MasterPathRuleSet> pathValidators = new ArrayList<MasterPathRuleSet>();
        String sql = "select * from elr.structure_path";
        Statement st = null;
        ResultSet rs = null;

        try {
            st = con.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                MasterPathRuleSet mpv = new MasterPathRuleSet();
                mpv.setId(rs.getInt("id"));
                mpv.setDataType(new MasterDataType(rs.getInt("data_type_id"), null));
                mpv.setPath(rs.getString("xpath"));
                mpv.setRequired(rs.getBoolean("required"));
                pathValidators.add(mpv);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
        for (MasterPathRuleSet mpv : pathValidators) {
            try {
                ElrDao.getPathRules(con, mpv);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to get path rules", e);
            }
        }
        return pathValidators;

    }

    private static void getPathRules(Connection con, MasterPathRuleSet mpv) throws Exception {

        List<MasterRule> mprList = new ArrayList<MasterRule>();
        String sql = "SELECT * FROM elr.structure_path_rule WHERE path_id = ? ORDER BY sequence ASC";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, mpv.getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                MasterRule mpr = new MasterRule();
                mpr.setId(rs.getInt("id"));
                mpr.setPathId(rs.getInt("path_id"));
                mpr.setOperatorId(rs.getInt("operator_id"));
                mpr.setOperandTypeId(rs.getInt("operand_type_id"));
                mpr.setOperandValue(rs.getString("operand_value"));
                mpr.setSequence(rs.getInt("sequence"));
                mpr.setAndOrOperatorId(rs.getInt("and_or_operator_id"));
                mprList.add(mpr);
            }
            mpv.setRules(mprList);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close rs", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close ps", e);
                }
            }
        }
    }

    public static MasterList getMasterList(Connection con, String localLoincCode, String localResultValue, Integer labId) {

        MasterList masterList = null;
        String qryList = "SELECT elr.get_list(?,?,?) AS id";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(qryList);
            ps.setString(1, localLoincCode);
            ps.setString(2, localResultValue);
            ps.setInt(3, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                Integer listId = rs.getInt("id");
                if (listId > 0) {
                    masterList = new MasterList(listId);
                }
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException re) {
                    logger.log(Level.SEVERE, "Exception in close rs", re);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException pe) {
                    logger.log(Level.SEVERE, "Exception in close ps", pe);
                }
            }
        }

        return masterList;

    }

    private static Integer getFinalStatus(List<MasterException> exList, Integer listId) {
        Integer status;
        Boolean exception = false;
        Boolean pending = false;

        for (MasterException ex : exList) {
            if (ex.getTypeId().equals(MasterStatus.EXCEPTION.getId())) {
                exception = true;
            } else if (ex.getTypeId().equals(MasterStatus.PENDING.getId())) {
                pending = true;
            }
        }

        if (exception) {
            status = MasterStatus.EXCEPTION.getId();
        } else if (pending) {
            status = MasterStatus.PENDING.getId();
        } else {
            if (listId.equals(MasterList.WHITE_LIST.getId())) {
                status = MasterStatus.ENTRY.getId();
            } else {
                status = listId;
            }

        }


        return status;
    }
    
  public static String getLabName(Connection con, Integer labId) {

        String labName = null;
        String qryList = "SELECT elr.get_pretty_lab_name(?) AS lab_name";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(qryList);
            ps.setInt(1, labId);
            rs = ps.executeQuery();
            if (rs.next()) {
                labName = rs.getString("lab_name");
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Cannot execute qry", e);
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException re) {
                    logger.log(Level.SEVERE, "Exception in close rs", re);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException pe) {
                    logger.log(Level.SEVERE, "Exception in close ps", pe);
                }
            }
        }

        return labName;

    }
    
    
}
