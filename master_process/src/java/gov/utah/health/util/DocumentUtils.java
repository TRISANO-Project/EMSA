package gov.utah.health.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author UDOH
 */
public class DocumentUtils {

    public static Document getDocument(String xml) throws IOException, ParserConfigurationException, SAXException {

        InputStream is = new ByteArrayInputStream(xml.getBytes());
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(false); // never forget this!
        DocumentBuilder builder = docFactory.newDocumentBuilder();
        return builder.parse(is);

    }

    public static Document getCopy(Document doc) throws IOException, ParserConfigurationException, SAXException, TransformerException {

        String xml = DocumentUtils.getXml(doc);
        return DocumentUtils.getDocument(xml);

    }

    public static String getXml(Document doc) throws TransformerException, TransformerFactoryConfigurationError {

        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        return writer.toString();

    }

    public static String getPathValue(Document doc, String path) throws DOMException, XPathExpressionException {

        String value = null;
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile(path + "/text()");
        Object result = expr.evaluate(doc, XPathConstants.NODE);
        if (result != null) {
            Node node = (Node) result;
            value = node.getNodeValue();
            if (value != null) {
                value = value.trim();
            }
        }

        return value;
    }

    public static Node getPathNode(Document doc, String path) throws DOMException, XPathExpressionException {

        Node node = null;
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile(path);
        Object result = expr.evaluate(doc, XPathConstants.NODE);
        if (result != null) {
            node = (Node) result;
        }
        return node;
    }

    public static void removePaths(Document doc, List<String> paths) throws DOMException, XPathExpressionException {

        List<Node> delNodes = new ArrayList<Node>();

        for (String path : paths) {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            XPathExpression expr = xpath.compile(path);
            Object result = expr.evaluate(doc, XPathConstants.NODE);
            if (result != null) {
                delNodes.add((Node) result);
            }
        }
        for (Node n : delNodes) {
            n.getParentNode().removeChild(n);
        }
    }

    public static void setPathValue(Document doc, String path, String value) throws DOMException, XPathExpressionException {

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile(path);
        Object result = expr.evaluate(doc, XPathConstants.NODE);
        if (result != null) {
            Node node = (Node) result;
            node.setTextContent(value);
        }
    }

    public static void makeChildrenSiblings(Document doc, String childPath) throws DOMException, XPathExpressionException {

        if (doc != null && childPath != null && childPath.trim().length() > 0) {
            Node child = getPathNode(doc, childPath);
            if (child != null) {
                Node parent = child.getParentNode();
                if (parent != null) {
                    List<Element> grandChildren = DocumentUtils.getChildElements(child);
                    if (grandChildren != null) {
                        for (Element gcEl : grandChildren) {
                            if (gcEl != null) {
                                parent.appendChild(gcEl);
                            }
                        }
                        parent.removeChild(child);
                    }
                }
            }
        }
    }

    public static void addPathValue(Document doc, String path, String value, String repeatingElement, Integer idx, Boolean append) throws DOMException, XPathExpressionException {

        Node node = DocumentUtils.makeXPath(doc, path, repeatingElement, idx);
        if (node != null) {
            String currentValue = null;
            if (append) {
                currentValue = node.getTextContent();
                if (currentValue != null) {
                    value = currentValue + value;
                }
            }
            node.setTextContent(value);
        }

    }

    public static void addCardPathValue(Document doc, String value, String path, Boolean append, String glueString) throws DOMException, XPathExpressionException {

        // go over the path parts add elements that arent there then append or replace value
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length());
        }
        if (path.lastIndexOf("/") == path.length() - 1) {
            path = path.substring(path.length() - 2, path.length() - 1);
        }

        String[] parts = path.split("/");
        Node curNode = null;
        Node parentNode = null;
        StringBuilder sbPath = new StringBuilder();

        for (int i = 0; i < parts.length; i++) {
            String part = parts[i];
            if (part.trim().length() > 0) {

                String nodeName = part;
                int open = part.indexOf("[");
                int close = part.indexOf("]");
                int ct = 0; // the number in [1] starts with 1
                if (open > 0 && close > 0 && open < close) {
                    String ctStr = part.substring(open + 1, close);
                    try {
                        ct = Integer.parseInt(ctStr);
                    } catch (NumberFormatException e) {
                    }
                    nodeName = part.substring(0, open);
                }
                String pathToParent = sbPath.toString();
                if (i > 0) {
                    parentNode = DocumentUtils.getPathNode(doc, pathToParent);
                }
                sbPath.append("/");
                sbPath.append(nodeName);

                if (ct == 0) {

                    curNode = DocumentUtils.getPathNode(doc, sbPath.toString());
                    // current node is not repeating use parent to create the current node if current node is null
                    if (curNode == null && parentNode != null) {
                        Element el = doc.createElement(nodeName);
                        curNode = parentNode.appendChild(el);
                    }
                } else {
                    sbPath.append("[");
                    sbPath.append(ct);
                    sbPath.append("]");
                    // current node is repeating... get parent and add elements of this node name until ct
                    for (int ec = 1; ec <= ct; ec++) {
                        curNode = DocumentUtils.getPathNode(doc, sbPath.toString());
                        if (curNode == null) {
                            Element el = doc.createElement(nodeName);
                            curNode = parentNode.appendChild(el);
                        }
                    }
                }
                // replace or append value in node
                if (i == parts.length - 1 && curNode != null) {
                    if (value != null && value.trim().length() > 0) {
                        if (append) {
                            String curVal = curNode.getTextContent();
                            if (curVal != null && curVal.trim().length() > 0) {
                                StringBuilder curValSb = new StringBuilder(curVal);
                                if (glueString != null) {
                                    curValSb.append(glueString);
                                }
                                curValSb.append(value);
                                curNode.setTextContent(curValSb.toString());
                            } else {
                                curNode.setTextContent(value);
                            }
                        } else {
                            curNode.setTextContent(value);
                        }
                    }
                    break;
                }
            }
        }
    }

    public static void clean(Document doc) {

        List<Element> delEle = new ArrayList<Element>();
        // doc.normalize();
        Element root = doc.getDocumentElement();
        NodeList children = root.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element) {
                cleanElement((Element) child, delEle);
            }
        }
        while (!delEle.isEmpty()) {
            for (Element de : delEle) {
                de.getParentNode().removeChild(de);
            }
            children = root.getChildNodes();
            delEle = new ArrayList<Element>();
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child instanceof Element) {
                    cleanElement((Element) child, delEle);
                }
            }
        }
    }

    public static Integer getElementCount(Document doc, String tagName) {
        NodeList nl = doc.getElementsByTagName(tagName);
        return nl.getLength();
    }

    public static Integer getElementCountInNode(Node node, String tagName) {
        int elementCount = 0;
        if (node != null) {
            elementCount = countElementsInNode(node, tagName);
        }
        return elementCount;
    }

    public static int countElementsInNode(Node node, String match) {
        int c = 0;
        if (match.equals(node.getNodeName())) {
            c++;
        }
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                c = c + countElementsInNode(currentNode, match);
            }
        }
        return c;
    }

    private static void cleanElement(Element parent, List<Element> delEle) {

        NodeList children = parent.getChildNodes();

        if (children.getLength() > 0) {

            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child instanceof Element) {
                    cleanElement((Element) child, delEle);
                } else {
                    String nodeVal = child.getNodeValue();
                    String name = child.getNodeName();
                    if (nodeVal == null
                            || nodeVal.equals("")
                            || nodeVal.equals("-1")
                            || DateUtils.isTemp(nodeVal)) {

                        delEle.add(parent);
                        //parent.getParentNode().removeChild(parent);

                    }
                }
            }

        } else {
            String nodeVal = parent.getNodeValue();
            String name = parent.getNodeName();
            if (nodeVal == null
                    || nodeVal.equals("")
                    || nodeVal.equals("-1")
                    || DateUtils.isTemp(nodeVal)) {
                delEle.add(parent);
                //parent.getParentNode().removeChild(parent);
            }
        }

    }

    private static Node makeCardXPath(Document doc, String xpath, String repeatingElement, int idx) {
        return makeCardXPath(doc, doc, xpath, repeatingElement, idx);
    }

    static private Node makeCardXPath(Document doc, Node parent, String xpath, String repeatingElement, int idx) {
        // grab the next node name in the xpath; or return parent if empty

        if (xpath.startsWith("/")) {
            xpath = xpath.substring(1, xpath.length());
        }
        if (xpath.endsWith("/")) {
            xpath = xpath.substring(0, xpath.length() - 1);
        }

        String[] partsOfXPath = xpath.split("/");
        Node child = null;
        StringBuilder rest = new StringBuilder();
        if (partsOfXPath.length > 0 && partsOfXPath[0].trim().length() > 0) {
            String currentNodeInPath = partsOfXPath[0];
            // get or create the node from the name
            List<Element> repeatingElements = DocumentUtils.getChildElements(parent, repeatingElement);
            Integer numRepeatingEls = repeatingElements.size();

            List<Element> childElements = DocumentUtils.getChildElements(parent);

            int c = 1;
            int nct = 0;
            int openb = currentNodeInPath.indexOf("[");
            int closb = currentNodeInPath.indexOf("]");
            String currentNodeName = currentNodeInPath;

            if (openb > 0 && closb > 0 && closb > openb + 1) {
                String nctStr = currentNodeInPath.substring(openb + 1, closb);
                currentNodeName = currentNodeInPath.substring(0, openb);
                if (nctStr != null && nctStr.length() > 0) {
                    nct = Integer.parseInt(nctStr);
                }
            }
            int z = 1;
            for (Element ch : childElements) {
                if (nct == 0 || nct > 0 && nct == z) {
                    String chName = ch.getNodeName();
                    if (chName.equals(currentNodeName)) {

                        if (!repeatingElement.equals(currentNodeName)) {
                            child = ch;
                        } else if (c == idx) {
                            child = ch;
                            break;
                        } else {
                            c++;
                        }
                    }
                }

                z++;
            }

            if (child == null) {
                Element el = doc.createElement(currentNodeName);
                child = parent.appendChild(el);
            } else if (child.getNodeName().equals(repeatingElement) && numRepeatingEls < idx) {
                Element el = doc.createElement(repeatingElement);
                child = parent.appendChild(el);
                child = makeCardXPath(doc, child, rest.toString(), repeatingElement, idx);
            }

            // rejoin the remainder of the array as an xpath expression and recurse
            if (partsOfXPath.length > 1) {
                rest.append("/");
                for (int i = 1; i < partsOfXPath.length; i++) {
                    rest.append(partsOfXPath[i]);
                    rest.append("/");
                }
            }

        } else {
            return parent;
        }
        return makeCardXPath(doc, child, rest.toString(), repeatingElement, idx);
    }

    private static Node makeXPath(Document doc, String xpath, String repeatingElement, int idx) {
        return makeXPath(doc, doc, xpath, repeatingElement, idx);
    }

    static private Node makeXPath(Document doc, Node parent, String xpath, String repeatingElement, int idx) {
        // grab the next node name in the xpath; or return parent if empty

        if (xpath.startsWith("/")) {
            xpath = xpath.substring(1, xpath.length());
        }
        if (xpath.endsWith("/")) {
            xpath = xpath.substring(0, xpath.length() - 1);
        }

        String[] partsOfXPath = xpath.split("/");
        Node child = null;
        StringBuilder rest = new StringBuilder();
        if (partsOfXPath.length > 0 && partsOfXPath[0].trim().length() > 0) {
            String currentNodeInPath = partsOfXPath[0];
            // get or create the node from the name
            List<Element> repeatingElements = DocumentUtils.getChildElements(parent, repeatingElement);
            Integer numRepeatingEls = repeatingElements.size();

            List<Element> childElements = DocumentUtils.getChildElements(parent);

            int c = 1;
            for (Element ch : childElements) {
                String chName = ch.getNodeName();
                if (chName.equals(currentNodeInPath)) {

                    if (!repeatingElement.equals(currentNodeInPath)) {
                        child = ch;
                    } else if (c == idx) {
                        child = ch;
                        break;
                    } else {
                        c++;
                    }
                }
            }

            if (child == null) {
                Element el = doc.createElement(currentNodeInPath);
                child = parent.appendChild(el);
            } else if (child.getNodeName().equals(repeatingElement) && numRepeatingEls < idx) {
                Element el = doc.createElement(repeatingElement);
                child = parent.appendChild(el);
                child = makeXPath(doc, child, rest.toString(), repeatingElement, idx);
            }

            // rejoin the remainder of the array as an xpath expression and recurse
            if (partsOfXPath.length > 1) {
                rest.append("/");
                for (int i = 1; i < partsOfXPath.length; i++) {
                    rest.append(partsOfXPath[i]);
                    rest.append("/");
                }
            }

        } else {
            return parent;
        }
        return makeXPath(doc, child, rest.toString(), repeatingElement, idx);
    }

    /**
     *
     * Returns all child elements
     */
    private static List<Element> getChildElements(Node node) {

        List<Element> els = new ArrayList<Element>();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node ch = childNodes.item(i);
            if (ch instanceof Element) {
                els.add((Element) ch);
            }
        }
        return els;
    }

    /**
     *
     * Returns all child elements with given name
     */
    public static List<Element> getChildElements(Node node, String nodeName) {

        List<Element> els = new ArrayList<Element>();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node ch = childNodes.item(i);
            if (ch instanceof Element) {
                Element e = (Element) ch;
                if (e.getNodeName().equals(nodeName)) {
                    els.add(e);
                }
            }
        }
        return els;
    }
}
