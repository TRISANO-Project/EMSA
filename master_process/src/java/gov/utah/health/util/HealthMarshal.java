package gov.utah.health.util;

import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.master.Health;
import gov.utah.health.model.trisano.TrisanoHealth;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

/**
 * Utility class for marshaling and un-marshaling objects.
 *
 * @author UDOH
 */
public class HealthMarshal {

    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    public void setMarshaller(Marshaller marshaller) {
        this.marshaller = marshaller;
    }

    public void setUnmarshaller(Unmarshaller unmarshaller) {
        this.unmarshaller = unmarshaller;
    }

    public HealthMessage getHealthMessageObject(String xml) throws Exception {
        HealthMessage healthMsg;
        InputStream is = new ByteArrayInputStream(xml.getBytes());
        healthMsg = (HealthMessage) this.unmarshaller.unmarshal(new StreamSource(is));
        return healthMsg;
    }

    public String getHealthMessageXml(HealthMessage healthMessage) throws Exception {
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        marshaller.marshal(healthMessage, result);
        return writer.toString();
    }

    public String getTrisanoHealthXml(TrisanoHealth trisanoHealth) throws Exception {
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        marshaller.marshal(trisanoHealth, result);
        return writer.toString();
    }

    public TrisanoHealth getTrisanoHealthObject(String xml) throws Exception {
        TrisanoHealth th;
        InputStream is = new ByteArrayInputStream(xml.getBytes());
        th = (TrisanoHealth) this.unmarshaller.unmarshal(new StreamSource(is));
        return th;
    }

    public String getHealthXml(Health he) throws Exception {
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        marshaller.marshal(he, result);
        return writer.toString();
    }

    public Health getHealthObject(String xml) throws Exception {
        Health h;
        InputStream is = new ByteArrayInputStream(xml.getBytes());
        h = (Health) this.unmarshaller.unmarshal(new StreamSource(is));
        return h;
    }
}
