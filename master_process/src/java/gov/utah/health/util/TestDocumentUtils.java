package gov.utah.health.util;

import static gov.utah.health.data.elr.ElrConnect.logger;
import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterMirthPath;
import gov.utah.health.model.master.MirthPath;
import gov.utah.health.service.master.MasterTransform;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.sql.DataSource;
import org.w3c.dom.Document;

/**
 *
 * @author UDOH
 */
public class TestDocumentUtils {

    static final String JDBC_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://localhost:5432/dcp";
    //  Database credentials
    static final String USER = "usernmae";
    static final String PASS = "passwd";

    public static void main(String[] args) {

        DataSource ds = null;
        Connection con = null;

        try {

            //STEP 2: Register JDBC driver
            Class.forName("org.postgresql.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            con = DriverManager.getConnection(DB_URL, USER, PASS);


        } catch (Exception e) {
            System.out.print(e.getMessage());
        }


        if (con != null) {
            try {

                Document masterDoc = null;
                try {
                    masterDoc = DocumentUtils.getDocument("<?xml version=\"1.0\"?>\n<health></health>");
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                }
                StringBuilder sbMirthXml = new StringBuilder();
                sbMirthXml.append("<?xml version=\"1.0\"?>\n"
                        + "<ORU_R01>\n"
                        + "    <MSH>\n"
                        + "        <MSH.1>|</MSH.1>\n"
                        + "        <MSH.2>^~\\&amp;</MSH.2>\n"
                        + "        <MSH.3>\n"
                        + "            <HD.1>LABCORP-CORP</HD.1>\n"
                        + "        </MSH.3>\n"
                        + "        <MSH.4>\n"
                        + "            <HD.1>LABCORP</HD.1>\n"
                        + "            <HD.2>34D0655059</HD.2>\n"
                        + "            <HD.3>CLIA</HD.3>\n"
                        + "        </MSH.4>\n"
                        + "        <MSH.5>\n"
                        + "            <HD.1>UTDOH</HD.1>\n"
                        + "        </MSH.5>\n"
                        + "        <MSH.6>\n"
                        + "            <HD.1>UT</HD.1>\n"
                        + "        </MSH.6>\n"
                        + "        <MSH.7>\n"
                        + "            <TS.1>201311010130</TS.1>\n"
                        + "        </MSH.7>\n"
                        + "        <MSH.9>\n"
                        + "            <MSG.1>ORU</MSG.1>\n"
                        + "            <MSG.2>R01</MSG.2>\n"
                        + "        </MSH.9>\n"
                        + "        <MSH.10>20131101023630861319</MSH.10>\n"
                        + "        <MSH.11>\n"
                        + "            <PT.1>P</PT.1>\n"
                        + "        </MSH.11>\n"
                        + "        <MSH.12>\n"
                        + "            <VID.1>2.3.1</VID.1>\n"
                        + "        </MSH.12>\n"
                        + "    </MSH>\n"
                        + "    <ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI>\n"
                        + "        <ORU_R01.PIDPD1NK1NTEPV1PV2>\n"
                        + "            <PID>\n"
                        + "                <PID.1>1</PID.1>\n"
                        + "                <PID.2>\n"
                        + "                    <CX.1>02241951CH</CX.1>\n"
                        + "                </PID.2>\n"
                        + "                <PID.3>\n"
                        + "                    <CX.1>29819508070</CX.1>\n"
                        + "                    <CX.6>\n"
                        + "                        <HD.1>LabCorp Phoenix</HD.1>\n"
                        + "                        <HD.2>03D0528350</HD.2>\n"
                        + "                        <HD.3>CLIA</HD.3>\n"
                        + "                    </CX.6>\n"
                        + "                </PID.3>\n"
                        + "                <PID.5>\n"
                        + "                    <XPN.1>\n"
                        + "                        <FN.1>HANSEN</FN.1>\n"
                        + "                    </XPN.1>\n"
                        + "                    <XPN.2>CHARLES</XPN.2>\n"
                        + "                </PID.5>\n"
                        + "                <PID.7>\n"
                        + "                    <TS.1>19510224</TS.1>\n"
                        + "                </PID.7>\n"
                        + "                <PID.8>M</PID.8>\n"
                        + "                <PID.10>\n"
                        + "                    <CE.1>U</CE.1>\n"
                        + "                </PID.10>\n"
                        + "                <PID.11>\n"
                        + "                    <XAD.1>PO BOX 745</XAD.1>\n"
                        + "                    <XAD.3>MAGNA</XAD.3>\n"
                        + "                    <XAD.4>UT</XAD.4>\n"
                        + "                    <XAD.5>84044</XAD.5>\n"
                        + "                </PID.11>\n"
                        + "                <PID.13>\n"
                        + "                    <XTN.6>801</XTN.6>\n"
                        + "                    <XTN.7>8658059</XTN.7>\n"
                        + "                </PID.13>\n"
                        + "                <PID.22>\n"
                        + "                    <CE.1>U</CE.1>\n"
                        + "                </PID.22>\n"
                        + "            </PID>\n"
                        + "            <NK1>\n"
                        + "                <NK1.1>1</NK1.1>\n"
                        + "            </NK1>\n"
                        + "        </ORU_R01.PIDPD1NK1NTEPV1PV2>\n"
                        + "        <ORU_R01.ORCOBRNTEOBXNTECTI>\n"
                        + "            <ORC>\n"
                        + "                <ORC.21>\n"
                        + "                    <XON.1>Utah Partners for Health</XON.1>\n"
                        + "                </ORC.21>\n"
                        + "                <ORC.22>\n"
                        + "                    <XAD.1>3665 South 8400 West</XAD.1>\n"
                        + "                    <XAD.3>Magna</XAD.3>\n"
                        + "                    <XAD.4>UT</XAD.4>\n"
                        + "                    <XAD.5>84044</XAD.5>\n"
                        + "                </ORC.22>\n"
                        + "                <ORC.23>\n"
                        + "                    <XTN.6>801</XTN.6>\n"
                        + "                    <XTN.7>2509638</XTN.7>\n"
                        + "                </ORC.23>\n"
                        + "                <ORC.24>\n"
                        + "                    <XAD.1>3665 South 8400 West</XAD.1>\n"
                        + "                    <XAD.3>Magna</XAD.3>\n"
                        + "                    <XAD.4>UT</XAD.4>\n"
                        + "                    <XAD.5>84044</XAD.5>\n"
                        + "                </ORC.24>\n"
                        + "            </ORC>\n"
                        + "            <OBR>\n"
                        + "                <OBR.1>1</OBR.1>\n"
                        + "                <OBR.3>\n"
                        + "                    <EI.1>29819508070</EI.1>\n"
                        + "                </OBR.3>\n"
                        + "                <OBR.4>\n"
                        + "                    <CE.4>550100</CE.4>\n"
                        + "                    <CE.5>HCV RNA by PCR, Qn Rfx Geno</CE.5>\n"
                        + "                    <CE.6>L</CE.6>\n"
                        + "                </OBR.4>\n"
                        + "                <OBR.7>\n"
                        + "                    <TS.1>201310250911</TS.1>\n"
                        + "                </OBR.7>\n"
                        + "                <OBR.14>\n"
                        + "                    <TS.1>201310260403</TS.1>\n"
                        + "                </OBR.14>\n"
                        + "                <OBR.16>\n"
                        + "                    <XCN.1>1861555823</XCN.1>\n"
                        + "                    <XCN.2>\n"
                        + "                        <FN.1>YOUNG</FN.1>\n"
                        + "                    </XCN.2>\n"
                        + "                    <XCN.3>SHANE</XCN.3>\n"
                        + "                    <XCN.4>S</XCN.4>\n"
                        + "                    <XCN.7>MD</XCN.7>\n"
                        + "                    <XCN.13>NPI</XCN.13>\n"
                        + "                </OBR.16>\n"
                        + "                <OBR.17>\n"
                        + "                    <XTN.6>801</XTN.6>\n"
                        + "                    <XTN.7>2509638</XTN.7>\n"
                        + "                </OBR.17>\n"
                        + "                <OBR.25>F</OBR.25>\n"
                        + "            </OBR>\n"
                        + "            <ORU_R01.OBXNTE>\n"
                        + "                <OBX>\n"
                        + "                    <OBX.1>1</OBX.1>\n"
                        + "                    <OBX.2>SN</OBX.2>\n"
                        + "                    <OBX.3>\n"
                        + "                        <CE.1>11011-4</CE.1>\n"
                        + "                        <CE.2>Hepatitis C virus RNA</CE.2>\n"
                        + "                        <CE.3>LN</CE.3>\n"
                        + "                        <CE.4>550028</CE.4>\n"
                        + "                        <CE.5>Hepatitis C Quantitation</CE.5>\n"
                        + "                        <CE.6>L</CE.6>\n"
                        + "                    </OBX.3>\n"
                        + "                    <OBX.5>\n"
                        + "                        <SN.2>6843040.0000</SN.2>\n"
                        + "                    </OBX.5>\n"
                        + "                    <OBX.6>\n"
                        + "                        <CE.1>IU/mL</CE.1>\n"
                        + "                    </OBX.6>\n"
                        + "                    <OBX.11>F</OBX.11>\n"
                        + "                    <OBX.14>\n"
                        + "                        <TS.1>20131029090928</TS.1>\n"
                        + "                    </OBX.14>\n"
                        + "                    <OBX.15>\n"
                        + "                        <CE.1>34D0655059</CE.1>\n"
                        + "                        <CE.2>LabCorp Burlington</CE.2>\n"
                        + "                        <CE.3>CLIA</CE.3>\n"
                        + "                    </OBX.15>\n"
                        + "                    <OBX.17>\n"
                        + "                        <CE.2>Probe.amp.tar</CE.2>\n"
                        + "                    </OBX.17>\n"
                        + "                </OBX>\n"
                        + "            </ORU_R01.OBXNTE>\n"
                        + "            <ORU_R01.OBXNTE>\n"
                        + "                <OBX>\n"
                        + "                    <OBX.1>2</OBX.1>\n"
                        + "                    <OBX.2>CE</OBX.2>\n"
                        + "                    <OBX.3>\n"
                        + "                        <CE.4>551222</CE.4>\n"
                        + "                        <CE.5>HCV Genotype</CE.5>\n"
                        + "                        <CE.6>L</CE.6>\n"
                        + "                    </OBX.3>\n"
                        + "                    <OBX.5>\n"
                        + "                        <CE.4>RTI</CE.4>\n"
                        + "                        <CE.5>To be performed on this specimen.</CE.5>\n"
                        + "                        <CE.6>L</CE.6>\n"
                        + "                    </OBX.5>\n"
                        + "                    <OBX.11>F</OBX.11>\n"
                        + "                    <OBX.14>\n"
                        + "                        <TS.1>20131029090928</TS.1>\n"
                        + "                    </OBX.14>\n"
                        + "                    <OBX.15>\n"
                        + "                        <CE.1>34D0655059</CE.1>\n"
                        + "                        <CE.2>LabCorp Burlington</CE.2>\n"
                        + "                        <CE.3>CLIA</CE.3>\n"
                        + "                    </OBX.15>\n"
                        + "                </OBX>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>1</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>To be performed on this specimen.</NTE.3>\n"
                        + "                </NTE>\n"
                        + "            </ORU_R01.OBXNTE>\n"
                        + "            <ORU_R01.OBXNTE>\n"
                        + "                <OBX>\n"
                        + "                    <OBX.1>3</OBX.1>\n"
                        + "                    <OBX.2>CE</OBX.2>\n"
                        + "                    <OBX.3>\n"
                        + "                        <CE.1>32286-7</CE.1>\n"
                        + "                        <CE.2>Hepatitis C virus genotype</CE.2>\n"
                        + "                        <CE.3>LN</CE.3>\n"
                        + "                        <CE.4>550666</CE.4>\n"
                        + "                        <CE.5>Hepatitis C Genotype</CE.5>\n"
                        + "                        <CE.6>L</CE.6>\n"
                        + "                    </OBX.3>\n"
                        + "                    <OBX.4>1</OBX.4>\n"
                        + "                    <OBX.5>\n"
                        + "                        <CE.4>HC1B</CE.4>\n"
                        + "                        <CE.5>1b</CE.5>\n"
                        + "                        <CE.6>L</CE.6>\n"
                        + "                    </OBX.5>\n"
                        + "                    <OBX.7>See Note</OBX.7>\n"
                        + "                    <OBX.11>F</OBX.11>\n"
                        + "                    <OBX.14>\n"
                        + "                        <TS.1>20131030134103</TS.1>\n"
                        + "                    </OBX.14>\n"
                        + "                    <OBX.15>\n"
                        + "                        <CE.1>34D0655059</CE.1>\n"
                        + "                        <CE.2>LabCorp Burlington</CE.2>\n"
                        + "                        <CE.3>CLIA</CE.3>\n"
                        + "                    </OBX.15>\n"
                        + "                    <OBX.17>\n"
                        + "                        <CE.2>Probe.amp.tar</CE.2>\n"
                        + "                    </OBX.17>\n"
                        + "                </OBX>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>1</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>1b</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>2</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>This assay can detect the six (6) major HCV Genotypes and their most</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>3</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>common subtypes.</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>4</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>.</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>5</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>Several clinical studies have demonstrated that Genotype 1 HCV may be</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>6</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>more refractory to interferon monotherapy as well as to interferon</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>7</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>plus ribavirin combination therapy. Sustained response rates are</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>8</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>increased for Genotype 1 infected patients when therapy is given for</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>9</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>48 weeks instead of 24 weeks.</NTE.3>\n"
                        + "                </NTE>\n"
                        + "            </ORU_R01.OBXNTE>\n"
                        + "        </ORU_R01.ORCOBRNTEOBXNTECTI>\n"
                        + "        <ORU_R01.ORCOBRNTEOBXNTECTI>\n"
                        + "            <OBR>\n"
                        + "                <OBR.1>2</OBR.1>\n"
                        + "                <OBR.3>\n"
                        + "                    <EI.1>29819508070</EI.1>\n"
                        + "                </OBR.3>\n"
                        + "                <OBR.4>\n"
                        + "                    <CE.4>322744</CE.4>\n"
                        + "                    <CE.5>Hepatitis Panel (4)</CE.5>\n"
                        + "                    <CE.6>L</CE.6>\n"
                        + "                </OBR.4>\n"
                        + "                <OBR.7>\n"
                        + "                    <TS.1>201310250911</TS.1>\n"
                        + "                </OBR.7>\n"
                        + "                <OBR.14>\n"
                        + "                    <TS.1>201310260403</TS.1>\n"
                        + "                </OBR.14>\n"
                        + "                <OBR.16>\n"
                        + "                    <XCN.1>1861555823</XCN.1>\n"
                        + "                    <XCN.2>\n"
                        + "                        <FN.1>YOUNG</FN.1>\n"
                        + "                    </XCN.2>\n"
                        + "                    <XCN.3>SHANE</XCN.3>\n"
                        + "                    <XCN.4>S</XCN.4>\n"
                        + "                    <XCN.7>MD</XCN.7>\n"
                        + "                    <XCN.13>NPI</XCN.13>\n"
                        + "                </OBR.16>\n"
                        + "                <OBR.17>\n"
                        + "                    <XTN.6>801</XTN.6>\n"
                        + "                    <XTN.7>2509638</XTN.7>\n"
                        + "                </OBR.17>\n"
                        + "                <OBR.25>F</OBR.25>\n"
                        + "            </OBR>\n"
                        + "            <ORU_R01.OBXNTE>\n"
                        + "                <OBX>\n"
                        + "                    <OBX.1>1</OBX.1>\n"
                        + "                    <OBX.2>SN</OBX.2>\n"
                        + "                    <OBX.3>\n"
                        + "                        <CE.1>48159-8</CE.1>\n"
                        + "                        <CE.2>Hepatitis C virus Ab Signal/Cutoff</CE.2>\n"
                        + "                        <CE.3>LN</CE.3>\n"
                        + "                        <CE.4>140683</CE.4>\n"
                        + "                        <CE.5>Hep C Virus Ab</CE.5>\n"
                        + "                        <CE.6>L</CE.6>\n"
                        + "                    </OBX.3>\n"
                        + "                    <OBX.5>\n"
                        + "                        <SN.1>&gt;</SN.1>\n"
                        + "                        <SN.2>11.0</SN.2>\n"
                        + "                    </OBX.5>\n"
                        + "                    <OBX.6>\n"
                        + "                        <CE.1>s/co ratio</CE.1>\n"
                        + "                    </OBX.6>\n"
                        + "                    <OBX.7>0.0-0.9</OBX.7>\n"
                        + "                    <OBX.8>H</OBX.8>\n"
                        + "                    <OBX.11>F</OBX.11>\n"
                        + "                    <OBX.14>\n"
                        + "                        <TS.1>20131026174321</TS.1>\n"
                        + "                    </OBX.14>\n"
                        + "                    <OBX.15>\n"
                        + "                        <CE.1>03D0528350</CE.1>\n"
                        + "                        <CE.2>LabCorp Phoenix</CE.2>\n"
                        + "                        <CE.3>CLIA</CE.3>\n"
                        + "                    </OBX.15>\n"
                        + "                    <OBX.17>\n"
                        + "                        <CE.2>EIA</CE.2>\n"
                        + "                    </OBX.17>\n"
                        + "                </OBX>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>1</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>Negative: &lt; 0.8</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>2</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>Indeterminate 0.8 - 0.9</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>3</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>Positive: &gt; 0.9</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>4</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>.</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>5</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>In order to reduce the incidence of a false positive</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>6</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>result, the CDC recommends that all s/co ratios</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>7</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>between 1.0 and 10.9 be confirmed by a more specific</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>8</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>supplemental or PCR testing. LabCorp offers HCV Ab</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>9</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>w/Reflex to Verification test #144065.</NTE.3>\n"
                        + "                </NTE>\n"
                        + "                <NTE>\n"
                        + "                    <NTE.1>10</NTE.1>\n"
                        + "                    <NTE.2>L</NTE.2>\n"
                        + "                    <NTE.3>.</NTE.3>\n"
                        + "                </NTE>\n"
                        + "            </ORU_R01.OBXNTE>\n"
                        + "        </ORU_R01.ORCOBRNTEOBXNTECTI>\n"
                        + "    </ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI>\n"
                        + "</ORU_R01>");

                String mirthXml = sbMirthXml.toString();

//                String pathMsh4 = "/ORU_R01/MSH/MSH.4/HD.1";
//
//                String pathHd1 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.PIDPD1NK1NTEPV1PV2/PID/PID.3/CX.6/HD.1";
//                String pathHd2 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.PIDPD1NK1NTEPV1PV2/PID/PID.3/CX.6/HD.2";
//                String pathHd3 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.PIDPD1NK1NTEPV1PV2/PID/PID.3/CX.6/HD.3";
//
//
//                String pathObr4Ce5 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/OBR/OBR.4/CE.5";
//
//                String pathObx2 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/ORU_R01.OBXNTE/OBX/OBX.2";
//        String pathNte1 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/ORU_R01.OBXNTE/NTE/NTE.1";
//        String pathNte2 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/ORU_R01.OBXNTE/NTE/NTE.2";
//                String pathNte3 = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/ORU_R01.OBXNTE/NTE/NTE.3";
//
//                String pathLabResultNote = "/health/labs/lab_result/note";
//                String pathLabResultObx2 = "/health/labs/lab_result/obx2";
//                String pathLabLabName = "/health/labs/lab";
//                String pathLabLocalTestName = "/health/labs/local_code_test_name";


                List<MasterMirthPath> mmpList = new ArrayList<MasterMirthPath>();


                mmpList = ElrDao.getMasterMirthPaths(con, 16, "2.3.1");//1

//                //This path will need to be added to the database as usual for mapping 
//                MasterMirthPath obrOne = new MasterMirthPath();
//// /ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/OBR/OBR.1
//                obrOne.setMasterPath("/health/labs/obr_one");
//                List<MirthPath> mirthpList = new ArrayList<MirthPath>();
//                MirthPath mirthp = new MirthPath();
//                mirthp.setGlueString(null);
//                mirthp.setXpath("/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/OBR/OBR.1");
//                mirthpList.add(mirthp);
//                obrOne.setMirthPaths(mirthpList);
//                mmpList.add(obrOne);
//                
                MasterMirthPath mmp = new MasterMirthPath();




//        MirthPath mp1 = new MirthPath();
//        mp1.setXpath(pathNte1);
//        mp1.setMasterPath(pathMasterLabResultNote);
//        mpList.add(mp1);
//
//        MirthPath mp2 = new MirthPath();
//        mp2.setXpath(pathNte2);
//        mp2.setMasterPath(pathMasterLabResultNote);
//        mpList.add(mp2);

//        MirthPath mp3 = new MirthPath();
//        mp3.setXpath(pathNte3);
//        mp3.setMasterPath(pathLabResultNote);
//        mp3.setGlueString(", ");
//        mpList.add(mp3);
//
//        MirthPath mp4 = new MirthPath();
//        mp4.setXpath(pathObx2);
//        mp4.setMasterPath(pathLabResultObx2);
//        mpList.add(mp4);
//
//        MirthPath mp5 = new MirthPath();
//        mp5.setXpath(pathMsh4);
//        mp5.setMasterPath(pathLabLabName);        
//        mpList.add(mp5);
//
//        MirthPath mp6 = new MirthPath();
//        mp6.setXpath(pathHd1);
//        mpList.add(mp6);
//
//        MirthPath mp7 = new MirthPath();
//        mp7.setXpath(pathHd2);
//        mpList.add(mp7);
//
//        MirthPath mp8 = new MirthPath();
//        mp8.setXpath(pathHd3);
//        mpList.add(mp8);
//
//        MirthPath mp9 = new MirthPath();
//        mp9.setXpath(pathObr4Ce5);
//        mp9.setMasterPath(pathLabLocalTestName);
//        mpList.add(mp9);
//
//        mmp.setMirthPaths(mpList);
//        mmpList.add(mmp);

                List<Document> masterDocs = new ArrayList<Document>();

                Document mirthDoc = null;
                try {
                    mirthDoc = DocumentUtils.getDocument(mirthXml);
                } catch (Exception e) {
                    //TODO Handel
                }

                for (MasterMirthPath mmpProc : mmpList) {
                    for (MirthPath mpProc : mmpProc.getMirthPaths()) {
                        MasterTransform.mirthPathToMasterDoc(mpProc, mirthDoc, masterDoc);
                    }
                }

                try {
                    System.out.print("CARD MASTER=\n");
                    System.out.print(DocumentUtils.getXml(masterDoc));
                } catch (Exception e) {
                }

                MasterTransform.divideMaster(masterDoc, masterDocs);

                int c = 1;
                for (Document md : masterDocs) {

                    try {
                        System.out.print("\nLAB SEGMENT "+c+++" \n");
                        System.out.print(DocumentUtils.getXml(md));
                    } catch (Exception e) {
                    }



                }


            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to process message", e);
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Cannot close connection", e);
                }

            }

        }


    }
}
