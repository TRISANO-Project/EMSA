package gov.utah.health.util;

import gov.utah.health.model.master.Exceptions;
import java.util.List;

/**
 *
 * @author UDOH
 */
public class TextUtils {

    public static String getList(List<Exceptions> exList, String del) {



        StringBuilder exIds = new StringBuilder("");
        int comma = 0;
        for (Exceptions e : exList) {
            if (comma > 0) {
                exIds.append(",");
                comma = 1;
            }
            exIds.append(e.exception_id);
        }

        return exIds.toString();
    }
}
