package gov.utah.health.util;

import gov.utah.health.model.master.Administrative;
import gov.utah.health.model.master.Clinicians;
import gov.utah.health.model.master.ContactInformation;
import gov.utah.health.model.master.Diagnostic;
import gov.utah.health.model.master.Disease;
import gov.utah.health.model.master.Encounter;
import gov.utah.health.model.master.Epidemiological;
import gov.utah.health.model.master.Exceptions;
import gov.utah.health.model.master.Health;
import gov.utah.health.model.master.HospitalInfo;
import gov.utah.health.model.master.Laboratory;
import gov.utah.health.model.master.Notes;
import gov.utah.health.model.master.Ontology;
import gov.utah.health.model.master.PerformingLab;
import gov.utah.health.model.master.Person;
import gov.utah.health.model.master.Reporting;
import gov.utah.health.model.master.Sourceid;
import gov.utah.health.model.master.Treatments;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class HealthObjectFactory {
    
    public static Health getInstance() {
        
        Health h = new Health();
        
        Administrative a = new Administrative();
        a.setAcuity("");
        a.setDateRecordCreated("");
        a.setEventId("");
        a.setEventName("");
        a.setJurisdictionId("");
        a.setJurisdictionOfResidence("");
        a.setLhdCaseStatus("");
        a.setMmwrWeek("");
        a.setMmwrYear("");
        a.setOutbreak("");
        a.setOutbreakAssoc("");
        a.setParticipationId("");
        a.setRecordNumber("");
        a.setStateCaseStatus("");
        a.setWorkFlowState("");
        h.setAdministrative(a);
        //

        Clinicians c = new Clinicians();
        c.setAreaCode("");
        c.setExtension("");
        c.setFirstName("");
        c.setLastName("");
        c.setMiddleName("");
        c.setPhone("");
        c.setPhoneType("");
        h.setClinicians(c);
        
        ContactInformation ci = new ContactInformation();
        ci.setAreaCode("");
        ci.setContactType("");
        ci.setDisposition("");
        ci.setExtension("");
        ci.setFirstName("");
        ci.setLastName("");
        ci.setPhone("");
        ci.setPhoneType("");
        h.setContactInformation(ci);
        
        Diagnostic d = new Diagnostic();
        d.setCity("");
        d.setState("");
        d.setStreetName("");
        d.setUnitNumber("");
        d.setName("");
        d.setTypes("");
        d.setZip("");
        h.setDiagnostic(d);
        
        Disease dis = new Disease();
        dis.setDiagnosed("");
        dis.setName("");
        dis.setOnsetDate("");
        h.setDisease(dis);
        
        Encounter e = new Encounter();
        e.setDescription("");
        e.setEncounterDate("");
        e.setInvestigator("");
        e.setLocation("");
        h.setEncounter(e);
        
        
        Epidemiological epi = new Epidemiological();
        epi.setCity("");
        epi.setCounty("");
        epi.setDateOfExposure("");
        epi.setDayCareAssoc("");
        epi.setFoodHandler("");
        epi.setGroupLiving("");
        epi.setHealthcareWorker("");
        epi.setImported("");
        epi.setName("");
        epi.setOccupation("");
        epi.setOtherData("");
        epi.setOtherData2("");
        epi.setRiskFactorNotes("");
        epi.setRiskFactors("");
        epi.setState("");
        epi.setStreetName("");
        epi.setStreetNum("");
        epi.setType("");
        epi.setUnit("");
        epi.setZip("");
        h.setEpidemiological(epi);
        
        Exceptions exc = new Exceptions();
        exc.setException("");
        exc.setMessage("");
        List<Exceptions> elist = new ArrayList<Exceptions>();
        elist.add(exc);
        h.setExceptions(elist);


   

        
        HospitalInfo hi = new HospitalInfo();
        hi.setAdmission("");
        hi.setDateOfDeath("");
        hi.setDied("");
        hi.setDiscarge("");
        hi.setDueDate("");
        hi.setFacility("");
        hi.setHospitalized("");
        hi.setMedicalRecord("");
        hi.setPregnant("");
        h.setHospitalInfo(hi);
        
        
        Laboratory lab = new Laboratory();
        lab.setAbnormalFlag("");
        lab.setAccessionNumber("");
        lab.setCollectionDate("");
        lab.setComment("");
        lab.setLab("");
        lab.setLabId("");
        lab.setLabTestDate("");
        lab.setLocalLoincCode("");
        lab.setLocalCodeTestName("");
        lab.setLocalReferenceRange("");
        lab.setLocalResultUnit("");
        lab.setLocalResultValue("");
        lab.setLocalSpecimenSource("");
        lab.setLocalTestName("");
        lab.setLoincCode("");
        lab.setManualEntry("");
        Ontology o = new Ontology();
        o.setCommon("");
        o.setDiseaseName("");
        o.setLabScale("");
        o.setManualEntry("");
        o.setOrganism("");
        o.setReference("");
        o.setSpecimen("");
        o.setSpecimenRequired("");
        o.setStateCaseStatus("");
        lab.setOntology(o);
        lab.setOrganism("");
        lab.setReferenceRange("");
        lab.setResultType("");
        lab.setResultValue("");
        lab.setScale("");
        lab.setSpecimenRequired("");
        lab.setSpecimenSource("");
        lab.setStateCaseStatus("");
        lab.setStateLab("");
        lab.setStatus("");
        lab.setSystem("");
        lab.setTestResult("");
        lab.setTestStatus("");
        lab.setTestType("");
        lab.setUnits("");
    
    

        List<Laboratory> ll = new ArrayList<Laboratory>();
        ll.add(lab);
        h.setLabs(ll);



        Notes n = new Notes();
        n.setNote("");
        h.setNotes(n);
        
        PerformingLab pl = new PerformingLab();
        pl.setAddress("");
        pl.setCity("");
        pl.setFirstName("");
        pl.setLastName("");
        pl.setMiddleName("");
        pl.setName("");
        pl.setPhone("");
        pl.setState("");
        pl.setZipCode("");
        h.setPerforming(pl);
        
        
        Person person = new Person();
        person.setAge("");
        person.setAreaCode("");
        person.setCity("");
        person.setCounty("");
        person.setCountry("");
        person.setDateOfBirth("");
        person.setEmail("");
        person.setEthnicity("");
        person.setExtension("");
        person.setFirstName("");
        person.setGender("");
        person.setLanguage("");
        person.setLastName("");
        person.setLatitude("");
        person.setLongitude("");
        person.setMiddleName("");
        person.setParentName("");
        person.setParentRelationship("");
        person.setPersonId("");
        person.setPhone("");
        person.setPhoneType("");
        person.setRace("");
        person.setState("");
        person.setStreet("");
        person.setStreetName("");
        person.setUnit("");
        person.setZip("");
        h.setPerson(person);
        
        Reporting r = new Reporting();
        r.setAgency("");
        r.setAreaCode("");
        r.setExtension("");
        r.setFirstName("");
        r.setLastName("");
        r.setPhone("");
        r.setReportDate("");
        r.setReportDohDate("");
        r.setShortName("");
        r.setTypes("");
        h.setReporting(r);
        
        Sourceid sid = new Sourceid();
        sid.setAddType("");
        sid.setChannel("");
        sid.setEntityId("");
        sid.setEventId("");
        sid.setEventRecordId("");
        sid.setExceptionStatus("");
        sid.setExternal("");
        sid.setFinalStatus("");
        sid.setId("");
        sid.setMasterXmlId("");
        sid.setOriginalMessage("");
        sid.setOriginalXml("");
        sid.setParticipationId("");
        sid.setSchema("");
        sid.setStatus("");
        sid.setStatusMessage("");
        sid.setSystemid("");
        h.setSourceid(sid);
        
        
        Treatments t = new Treatments();
        t.setDateOfTreatment("");
        t.setGiven("");
        t.setName("");
        t.setTreatmentStopped("");
        h.setTreatments(t);
        
        
        return h;
        
        
    }
}
