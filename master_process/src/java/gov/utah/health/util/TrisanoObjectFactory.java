package gov.utah.health.util;

import gov.utah.health.model.trisano.Addresses;
import gov.utah.health.model.trisano.Attachments;
import gov.utah.health.model.trisano.CliniciansAttributes;
import gov.utah.health.model.trisano.ContactChildEventsAttributes;
import gov.utah.health.model.trisano.DiagnosticFacilitiesAttributes;
import gov.utah.health.model.trisano.DiseaseEvents;
import gov.utah.health.model.trisano.Events;
import gov.utah.health.model.trisano.HospitalizationFacilitiesAttributes;
import gov.utah.health.model.trisano.HospitalsParticipations;
import gov.utah.health.model.trisano.InterestedPartyAttributes;
import gov.utah.health.model.trisano.JurisdictionAttributes;
import gov.utah.health.model.trisano.LabResults;
import gov.utah.health.model.trisano.LabsAttributes;
import gov.utah.health.model.trisano.Notes;
import gov.utah.health.model.trisano.ParticipationsContacts;
import gov.utah.health.model.trisano.ParticipationsRiskFactors;
import gov.utah.health.model.trisano.ParticipationsTreatments;
import gov.utah.health.model.trisano.People;
import gov.utah.health.model.trisano.PeopleRaces;
import gov.utah.health.model.trisano.PeopleRacesPK;
import gov.utah.health.model.trisano.Places;
import gov.utah.health.model.trisano.ReporterAttributes;
import gov.utah.health.model.trisano.ReportingAgencyAttributes;
import gov.utah.health.model.trisano.Telephones;
import gov.utah.health.model.trisano.TrisanoHealth;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public class TrisanoObjectFactory {

    public static final Date TEMP = DateUtils.getTemp();

    public static TrisanoHealth getInstance() {

        TrisanoHealth h = new TrisanoHealth();
        h.setAddresses(getAddresses());

        List<CliniciansAttributes> caList = new ArrayList<CliniciansAttributes>();
        CliniciansAttributes ca = new CliniciansAttributes();
        ca.setClinician(getPeople());
        ca.setTelephones(getTelephones());
        caList.add(ca);
        h.setCliniciansAttributes(caList);

        List<ContactChildEventsAttributes> cceaList = new ArrayList<ContactChildEventsAttributes>();
        ContactChildEventsAttributes ccea = new ContactChildEventsAttributes();
        ccea.setInterestedPartyAttributes(getInterestedPartyAttributes());
        ParticipationsContacts pc = new ParticipationsContacts();
        pc.setContactTypeId(-1);
        pc.setDispositionDate(TEMP);
        pc.setDispositionId(-1);
        ccea.setParticipationsContacts(pc);
        cceaList.add(ccea);
        h.setContactChildEventsAttributes(cceaList);


        List<DiagnosticFacilitiesAttributes> dfaList = new ArrayList<DiagnosticFacilitiesAttributes>();
        DiagnosticFacilitiesAttributes dfa = new DiagnosticFacilitiesAttributes();
        dfa.setAddresses(getAddresses());
        dfa.setPlace(getPlace());
        dfa.setTelephones(getTelephones());
        dfaList.add(dfa);
        h.setDiagnosticFacilitiesAttributes(dfaList);


        List<DiseaseEvents> deList = new ArrayList<DiseaseEvents>();
        DiseaseEvents de = new DiseaseEvents();
        de.setDateDiagnosed(TEMP);
        de.setDiedId(-1);
        de.setDiseaseId(-1);
        de.setDiseaseOnsetDate(TEMP);
        de.setEventId(-1);
        de.setHospitalizedId(-1);
        deList.add(de);
        h.setDiseaseEvents(deList);



        List<Events> eList = new ArrayList<Events>();
        Events event = new Events();
        event.setAcuity(-1);
        event.setAgeAtOnset(-1);
        event.setAgeTypeId(-1);
        event.setCdcUpdatedAt(TEMP);
        event.setEventName("");
        event.setEventOnsetDate(TEMP);
        event.setEventQueueId(-1);
        event.setFirstReportedPHdate(TEMP);
        event.setIbisUpdatedAt(TEMP);
        event.setImportedFromId(-1);
        event.setInvestigationCompletedLHDdate(TEMP);
        event.setInvestigationLHDstatusid(-1);
        event.setInvestigationStartedDate(TEMP);
        event.setInvestigatorId(-1);
        event.setLhdCaseStatusId(-1);
        event.setMMWRweek(-1);
        event.setMMWRyear(-1);
        event.setOtherData1("");
        event.setOtherData2("");
        event.setParentGuardian("");
        event.setRecordNumber("");
        event.setResultsReportedToClinicianDate(TEMP);
        event.setReviewCompletedByStateDate(TEMP);
        event.setSentToCdc(null);
        event.setSentToIbis(null);
        event.setStateCaseStatusId(-1);
        event.setStateManagerId(-1);
        event.setType("");
        event.setUndergoneFormAssignment(null);
        event.setWorkflowState("");
        eList.add(event);
        h.setEvents(eList);

        List<HospitalizationFacilitiesAttributes> hfaList = new ArrayList<HospitalizationFacilitiesAttributes>();
        HospitalizationFacilitiesAttributes hfa = new HospitalizationFacilitiesAttributes();
        hfa.setAddresses(getAddresses());
        HospitalsParticipations hp = new HospitalsParticipations();
        hp.setAdmissionDate(TEMP);
        hp.setDischargeDate(TEMP);
        hp.setHospitalRecordNumber("");
        hp.setMedicalRecordNumber("");
        hfa.setHospitalsParticipations(hp);
        hfa.setPlace(getPlace());
        hfa.setTelephones(getTelephones());
        hfaList.add(hfa);
        h.setHospitalizationFacilitiesAttributes(hfaList);

        List<InterestedPartyAttributes> ipaList = new ArrayList<InterestedPartyAttributes>();
        ipaList.add(getInterestedPartyAttributes());
        h.setInterestedPartyAttributes(ipaList);

        List<JurisdictionAttributes> jaList = new ArrayList<JurisdictionAttributes>();
        JurisdictionAttributes ja = new JurisdictionAttributes();
        ja.setAddresses(getAddresses());
        ja.setPlace(getPlace());
        ja.setTelephones(getTelephones());
        jaList.add(ja);
        h.setJurisdictionAttributes(jaList);

        List<LabsAttributes> laList = new ArrayList<LabsAttributes>();
        LabsAttributes la = new LabsAttributes();
        la.setAddresses(getAddresses());
        la.setPlace(getPlace());
        la.setTelephones(getTelephones());

        
        List<LabResults> lrList = new ArrayList<LabResults>();
        LabResults lr = new LabResults();
        lr.setCollectionDate(TEMP);
        lr.setComment("");
        lr.setLabTestDate(TEMP);
        lr.setLoincCode("");
        lr.setOrganismId(-1);
        lr.setReferenceRange("");
        lr.setResultValue("");
        lr.setSpecimenSentToStateId(-1);
        lr.setSpecimenSourceId(-1);
        lr.setStagedMessageId(-1);
        lr.setTestResultId(-1);
        lr.setTestStatusId(-1);
        lr.setTestTypeId(-1);
        lr.setUnits("");
        lr.setAccessionNo("");
        lrList.add(lr);
        la.setLabResults(lrList);
        laList.add(la);
        h.setLabsAttributes(laList);

        Notes note = new Notes();
        note.setNote("");
        note.setNoteType("");
        note.setUserId(-1);
        h.setNotes(note);



        List<ReporterAttributes> raList = new ArrayList<ReporterAttributes>();
        ReporterAttributes ra = new ReporterAttributes();
        ra.setReporter(getPeople());
        ra.setTelephones(getTelephones());
        raList.add(ra);
        h.setReporterAttributes(raList);


        List<ReportingAgencyAttributes> raaList = new ArrayList<ReportingAgencyAttributes>();
        ReportingAgencyAttributes raa = new ReportingAgencyAttributes();
        raa.setAddresses(getAddresses());
        raa.setPlace(getPlace());
        raa.setTelephones(getTelephones());
        raaList.add(raa);
        h.setReportingAgencyAttributes(raaList);

        List<Attachments> attachList = new ArrayList<Attachments>();
        Attachments attachment = new Attachments();
        attachment.setCategory("");
        attachment.setContentType("");
        attachment.setEventId(-1);
        attachment.setFilename("");
        attachment.setMasterId(-1);
        attachList.add(attachment);
        h.setAttachments(attachList);
        
        return h;


    }

    private static InterestedPartyAttributes getInterestedPartyAttributes() {

        InterestedPartyAttributes ipa = new InterestedPartyAttributes();
        ipa.setPerson(getPeople());
        ipa.setPeopleRaces(getPeopleRaces());
        ipa.setTelephones(getTelephones());
        List<ParticipationsRiskFactors> prfList = new ArrayList<ParticipationsRiskFactors>();
        ParticipationsRiskFactors prf = new ParticipationsRiskFactors();
        prf.setOccupation("");
        prf.setPregnantId(-1);
        prf.setPregnancyDueDate(TEMP);
        prf.setRiskFactors("");
        prf.setRiskFactorsNotes("");
        prfList.add(prf);
        ipa.setRiskFactorAttributes(prfList);


        List<ParticipationsTreatments> ptList = new ArrayList<ParticipationsTreatments>();
        ParticipationsTreatments pt = new ParticipationsTreatments();
        pt.setStopTreatmentDate(TEMP);
        pt.getTreatmentComment();
        pt.setTreatmentDate(TEMP);
        pt.setTreatmentGivenYnId(-1);
        pt.setTreatmentId(-1);
        ptList.add(pt);
        ipa.setTreatmentsAttributes(ptList);

        return ipa;

    }

    private static List<Addresses> getAddresses() {
        List<Addresses> addys = new ArrayList<Addresses>();
        addys.add(getAddress());
        return addys;
    }

    private static Addresses getAddress() {

        Addresses addy = new Addresses();
        addy.setCity("");
        addy.setCountyId(-1);
        addy.setPostalCode("");
        addy.setStateId(-1);
        addy.setStreetName("");
        addy.setUnitNumber("");
        return addy;

    }

    private static People getPeople() {

        People p = new People();
        p.setBirthDate(TEMP);
        p.setFirstName("");
        p.setLastName("");
        p.setMiddleName("");
        p.setApproximateAgeNoBirthday(-1);
        p.setBirthGenderId(-1);
        p.setDateOfDeath(TEMP);
        p.setEthnicityId(-1);
        p.setLive("");
        p.setPersonType("");
        p.setPrimaryLanguageId(-1);

        return p;
    }

    private static Places getPlace() {

        Places p = new Places();
        p.setId(-1);
        p.setName("");
        p.setShortName("");
        return p;
    }

    private static List<Telephones> getTelephones() {

        List<Telephones> tList = new ArrayList<Telephones>();
        tList.add(getTelephone());
        return tList;

    }

    private static List<PeopleRaces> getPeopleRaces() {

        List<PeopleRaces> prList = new ArrayList<PeopleRaces>();
        prList.add(getPeopleRace());
        return prList;

    }

    private static Telephones getTelephone() {


        Telephones t = new Telephones();
        t.setAreaCode("");
        t.setCountryCode("");
        t.setExtension("");
        t.setPhoneNumber("");
        return t;
    }

    private static PeopleRaces getPeopleRace() {
        PeopleRaces pr = new PeopleRaces();
        pr.setPeopleRacesPK(new PeopleRacesPK(-1, -1));
        return pr;
    }
    
    
   public static void addAttachment(TrisanoHealth th, Integer masterId, String labName, String diagnosticName,String fName,String lName) {

        Attachments att = new Attachments();
        att.setMasterId(masterId);
        att.setCategory("lab");
        att.setCreatedAt(new Date());
        att.setFilename(getPDFFileName(labName,diagnosticName,fName,lName));       
        th.addAttachment(att);
    }
    private static String getPDFFileName(String labName, String diagnosticName, String fName, String lName) {
        StringBuilder sb = new StringBuilder();
        sb.append(labName.toUpperCase());
        sb.append("ELR");
        sb.append(diagnosticName);
        sb.append(formatDateTimeFileName(new Date()));
        if (fName != null && lName != null) {
            sb.append(getPersonInitial(fName, lName));
        }
        String fileNamePdf = sb.toString().toUpperCase() + ".pdf";
        return fileNamePdf;
    }

    private static String getPersonInitial(String fName, String lName) {
        StringBuilder sb = new StringBuilder();
        sb.append(fName.trim().substring(0, 1));
        sb.append(lName.trim().substring(0, 1));
        return sb.toString();
    }

    private static String formatDateTimeFileName(Date date) {
        SimpleDateFormat sdfOut = new SimpleDateFormat("MMddyyHHmmss");
        String dateFormatted = "";

        if (date != null) {
            try {
                dateFormatted = sdfOut.format(date);
            } catch (Exception e) {
                dateFormatted = date.toString();
            }
        }
        return dateFormatted;
    }    
}
