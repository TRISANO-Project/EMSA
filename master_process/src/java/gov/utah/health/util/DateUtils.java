package gov.utah.health.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author UDOH
 */
public class DateUtils {

    private final static List<String> patsNum = new ArrayList();
    private final static List<String> patsOth = new ArrayList();
    private final static GregorianCalendar tempGc = new GregorianCalendar();
    private final static String masterDatePattern = "yyyy-MM-dd'T'HH:mm:ss";
    public final static SimpleDateFormat MASTER_FORMAT =
            new SimpleDateFormat(masterDatePattern);

    static {

        patsNum.add("yyyyMMdd");
        patsNum.add("yyyyMMddHH");
        patsNum.add("yyyyMMddHHmm");
        patsNum.add("yyyyMMddHHmmss");
        // 
        patsOth.add("yyyyMMddHHmmss.SSSZ");
        patsOth.add(masterDatePattern);
        patsOth.add("yyyy-MM-dd");
        patsOth.add("yyyy/MM/dd");


        tempGc.set(GregorianCalendar.YEAR, 1);
        tempGc.set(GregorianCalendar.MONTH, 0);
        tempGc.set(GregorianCalendar.DAY_OF_MONTH, 1);
        tempGc.set(GregorianCalendar.HOUR, 0);
        tempGc.set(GregorianCalendar.MINUTE, 0);
        tempGc.set(GregorianCalendar.SECOND, 0);
        tempGc.set(GregorianCalendar.MILLISECOND, 0);

    }

    public static Date getTemp() {
        return tempGc.getTime();
    }

    public static Boolean isTemp(String ds) {
        Boolean isTemp = false;
        try {

            Date d = MASTER_FORMAT.parse(ds);
            Date t = getTemp();
            if (t.compareTo(d) == 0) {
                isTemp = true;
            }
        } catch (Exception e) {
            String tmp = "";
        }
        return isTemp;
    }

    public static Date parseDate(String inputDate) {

        Date outDate = null;
        if (inputDate != null && inputDate.trim().length() > 0) {
            // strip off the trailing +digits from dates like:  20150126194900+0000 or 20150126194900-0000
            String ss = "+";
            Pattern pattern;
            pattern = Pattern.compile("^(\\d+)(\\+|-)(\\d*)$");
            Matcher matcher = pattern.matcher(inputDate);
            if (matcher.matches()) {
                inputDate = matcher.group(1);
            }

            Integer inputDateLength = inputDate.length();
            Boolean inputDateIsNumbers = Pattern.matches("^\\d{" + inputDateLength + "}$", inputDate);

            if (inputDateIsNumbers) {
                for (String pat : patsNum) {
                    SimpleDateFormat sdf = new SimpleDateFormat(pat);
                    sdf.setLenient(false);
                    if (inputDateLength == pat.length()) {
                        try {
                            outDate = sdf.parse(inputDate);
                            break;
                        } catch (Exception e) {
                            // do nothing
                        }
                    }
                }
            } else {
                for (String pat : patsOth) {
                    SimpleDateFormat sdf = new SimpleDateFormat(pat);
                    sdf.setLenient(false);
                    try {
                        outDate = sdf.parse(inputDate);
                        break;
                    } catch (Exception e) {
                        // do nothing
                    }
                }
            }
        }
        return outDate;

    }

    public static String formatDateTime(String format, Date date) {

        SimpleDateFormat sdfOut = new SimpleDateFormat(format);//"MMddyyHHmmss"
        String dateFormatted = "";

        if (date != null) {
            try {
                dateFormatted = sdfOut.format(date);
            } catch (Exception e) {
                dateFormatted = date.toString();
            }
        }
        return dateFormatted;
    }

    public static String getMasterDateFormat(String dateIn) {
        Date d = parseDate(dateIn);
        String formattedDate = null;
        if (d != null) {
            formattedDate = MASTER_FORMAT.format(d);
        }
        return formattedDate;
    }

    public static String getMasterDateFormat(Date dateIn) {
        String formattedDate = MASTER_FORMAT.format(dateIn);
        return formattedDate;
    }

    public static Date getDateYearsFromNow(Integer years) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(GregorianCalendar.YEAR, years);
        return gc.getTime();
    }
}
