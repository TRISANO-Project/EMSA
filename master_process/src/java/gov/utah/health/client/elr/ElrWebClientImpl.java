package gov.utah.health.client.elr;

import gov.utah.health.util.AppProperties;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.params.AuthPNames;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.AuthPolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Repository;

/**
 * This class provides the web client for interfacing the elrapp web
 * application. 
 *
 * @author UDOH
 */
@Repository
public class ElrWebClientImpl implements ElrWebClient {

    private static final Log log = LogFactory.getLog(ElrWebClientImpl.class);

    /**
     * Makes a single get request to the elr app for automatic processing of
     * given system_messages.id
     * @param masterId
     */
    @Override
    public void processElr(Integer masterId) {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        
        AppProperties props = null;

        props = AppProperties.getInstance();
        boolean useNtlsCredentialing = props.getBooleanProperty("use_ntls_credentialing");
        String elrHost = props.getProperty("elr_host");
        int elrPort = props.getIntProperty("elr_port");
        String elrAutoKey = props.getProperty("elr_auto_key");
        String elrContext = props.getProperty("elr_context");
        String elrUser = props.getProperty("elr_user");
        String ntlsHost = props.getProperty("ntls_host");
        String ntlsDomain = props.getProperty("ntls_domain");
        String ntlsUser = props.getProperty("ntls_user");
        String ntlsPassword = props.getProperty("ntls_password");
        
//log.error("UseNtls:"+(useNtlsCredentialing?"true":"false")+"elrHost:"+elrHost+"elrPort"+elrPort+
//         "elrContext:"+elrContext+"elrUser:"+elrUser+"ntlsHost:"+ntlsHost+"ntlsDomain:"+ntlsDomain+"ntlsUser:"+ntlsUser+
//        "ntlsPassword:"+ntlsPassword);
        
        try {
            URI uri = new URI("http", null, elrHost, elrPort, elrContext+"/", "selected_page=3&type=17&emsa_action=elrauto&id=" + masterId +
                    "&elr_auto_key=" + elrAutoKey, null);
            HttpGet httpGet = new HttpGet(uri);
            
            if(useNtlsCredentialing) {
log.error("\nTODO using NTLS");
                List<String> authpref = new ArrayList<String>();
                authpref.add(AuthPolicy.NTLM);
                httpclient.getParams().setParameter(AuthPNames.TARGET_AUTH_PREF, authpref);
                NTCredentials creds = new NTCredentials(ntlsUser, ntlsPassword, ntlsHost, ntlsDomain);
                httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, creds);
            } else {
log.error("\nTODO NOT using NTLS");
                Header header = new BasicHeader("UID", elrUser);
                httpGet.setHeader(header);
            }

            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String statusLine = response.getStatusLine().toString();

            if (entity != null) {
                if (statusLine.indexOf("200 OK") > 0) {
                    log.info("\nAUTO ELR SUCCESS... system_messages.id=" + masterId + "\n"
                            + "URI=" + uri + "\n"
                            + "StatusCode=" + statusLine);
                } else {
                    log.error("\nAUTO ELR FAILURE... system_messages.id=" + masterId + "\n"
                            + "URI=" + uri + "\n"
                            + "StatusCode=" + statusLine);
                }
            }
            EntityUtils.consume(entity);
        } catch (Exception e) {
            log.error("ElrWebClientImpl.processElr failure. masterId=" + masterId, e);
        } finally {
            httpclient.getConnectionManager().shutdown();
        }

    }
}
