package gov.utah.health.client.elr;

/**
 * Interface for general web-based data access to elrApp.
 * @author UDOH
 */
public interface ElrWebClient {

    public void processElr(Integer masterId);
   
}

