package gov.utah.health.client.sqla;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceContext;

/**
 *
 * @author UDOH
 */
@WebServiceClient(name = "SqlAgentService", targetNamespace = "http://hllqryagnt/SqlAgent/SqlAgentService/", wsdlLocation = "http://hllqryagnt:8080/SqlAgent/SqlAgentService?wsdl")
public class SqlAgentClient {

   // @WebServiceRef(wsdlLocation = "http://hllqryagnt:8080/SqlAgent/SqlAgentService?wsdl")
    public SqlAgentService service;
    @Resource
    protected WebServiceContext context;

    public static String searchPerson(java.lang.String criteria) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.searchPerson(criteria);
    }

    public static String findPerson(java.lang.String criteria) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.findPerson(criteria);
    }

    public static String findEvent(java.lang.String criteria) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.findEvent(criteria);
    }

    public static String addCmr(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.addCmr(healthMessage);
    }

    public static String updateCmr(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.updateCmr(healthMessage);
    }

    public static String findId(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.findId(healthMessage);
    }

    public static String findQuestions(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.findQuestions(healthMessage);
    }

    public static String findAnswers(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.findAnswers(healthMessage);
    }

    public static String getTableData(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getTableData(healthMessage);
    }

    public static String getExample(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getExample(healthMessage);
    }

    public static String getCaseStatusReport(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getCaseStatusReport(healthMessage);
    }

    public static String getDiseaseReport(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getDiseaseReport(healthMessage);
    }

    public static String getCareTimeReport(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getCareTimeReport(healthMessage);
    }

    public static String getUserRoles(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getUserRoles(healthMessage);
    }

    public static String getRoles(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getRoles(healthMessage);
    }

    public static String attachPdf(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.attachPdf(healthMessage);
    }

    public static String addNote(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.addNote(healthMessage);
    }

    public static String addAnswers(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.addAnswers(healthMessage);
    }

    public static String addAttachments(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.addAttachments(healthMessage);
    }

    public static String exportTreatmentNotes(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.exportTreatmentNotes(healthMessage);
    }

//    public static String exportForms(java.lang.String healthMessage) {
//        SqlAgentServiceService service = new SqlAgentServiceService();
//        SqlAgentService port = service.getSqlAgentServicePort();
//        return port.exportForms(healthMessage);
//    }
    public static String addTask(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.addTask(healthMessage);
    }

    public static String getUdohClinicalResults(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getUdohClinicalResults(healthMessage);
    }

    public static String getCdcExport(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getCdcExport(healthMessage);
    }

    public static String getGeocode(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.getGeocode(healthMessage);
    }

    public static String testAccess(java.lang.String healthMessage) {
        SqlAgentServiceService service = new SqlAgentServiceService();
        SqlAgentService port = service.getSqlAgentServicePort();
        return port.testAccess(healthMessage);
    }
}
