package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterPath {

    private Integer id;
    private String xpath;
    private Integer dataTypeId;
    private String element;
    private Integer categoryId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public Integer getDataTypeId() {
        return dataTypeId;
    }

    public void setDataTypeId(Integer dataTypeId) {
        this.dataTypeId = dataTypeId;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }


    
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.xpath != null ? this.xpath.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MasterPath other = (MasterPath) obj;
        if ((this.xpath == null) ? (other.xpath != null) : !this.xpath.equals(other.xpath)) {
            return false;
        }
        return true;
    }
}
