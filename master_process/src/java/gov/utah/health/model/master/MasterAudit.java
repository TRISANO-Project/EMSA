package gov.utah.health.model.master;

import java.util.List;

/**
 *
 * @author  UDOH
 */
public class MasterAudit {

    private Integer userId;
    private Integer messageActionId;
    private Integer systemMessageId;
    private Integer originalMessageId;
    private Integer labId;
    private List<MasterException> exList;

    public Integer getOriginalMessageId() {
        return originalMessageId;
    }

    public void setOriginalMessageId(Integer originalMessageId) {
        this.originalMessageId = originalMessageId;
    }
    
    private Integer systemStatusId;

    public MasterAudit(Integer userId, Integer messageActionId, Integer systemMessageId, Integer labId, Integer systemStatusId, List<MasterException> exList) {
        this.userId = userId;
        this.messageActionId = messageActionId;
        this.systemMessageId = systemMessageId;
        this.labId = labId;
        this.systemStatusId = systemStatusId;
        this.exList = exList;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMessageActionId() {
        return messageActionId;
    }

    public void setMessageActionId(Integer messageActionId) {
        this.messageActionId = messageActionId;
    }

    public Integer getSystemMessageId() {
        return systemMessageId;
    }

    public void setSystemMessageId(Integer systemMessageId) {
        this.systemMessageId = systemMessageId;
    }

    public Integer getLabId() {
        return labId;
    }

    public void setLabId(Integer labId) {
        this.labId = labId;
    }

    public List<MasterException> getExList() {
        return exList;
    }

    public void setExList(List<MasterException> exList) {
        this.exList = exList;
    }


    public Integer getSystemStatusId() {
        return systemStatusId;
    }

    public void setSystemStatusId(Integer systemStatusId) {
        this.systemStatusId = systemStatusId;
    }
    
    
    
    
}
