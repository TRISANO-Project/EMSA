package gov.utah.health.model.master;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author UDOH
 */
public class MasterMirthPath {

    private String masterPath;
    private String repeatingElement;
    private List<MirthPath> mirthPaths;

    public String getRepeatingElement() {
        if(masterPath != null && masterPath.contains("labs")){
            repeatingElement = "labs";
        }
        return repeatingElement;
    }

    public void setRepeatingElement(String repeatingElement) {
        this.repeatingElement = repeatingElement;
    }


    public String getMasterPath() {
        return masterPath;
    }

    public void setMasterPath(String masterPath) {
        this.masterPath = masterPath;
    }

    public List<MirthPath> getMirthPaths() {
        return mirthPaths;
    }

    public void setMirthPaths(List<MirthPath> mirthPaths) {
        this.mirthPaths = mirthPaths;
    }

    public void addMirthPath(MirthPath mirthPath) {
        if (this.mirthPaths == null) {
            this.mirthPaths = new ArrayList<MirthPath>();
        }
        this.mirthPaths.add(mirthPath);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.masterPath != null ? this.masterPath.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MasterMirthPath other = (MasterMirthPath) obj;
        if ((this.masterPath == null) ? (other.masterPath != null) : !this.masterPath.equals(other.masterPath)) {
            return false;
        }
        return true;
    }
}
