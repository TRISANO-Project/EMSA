package gov.utah.health.model.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author UDOH
 */
public class MasterCard extends Cardinality {
    // These lists define the cardinality higherarchy of the master model

    public static final Cardinality CARD_LAB = new Cardinality();
    public static final Cardinality CARD_LAB_RESULT = new Cardinality();
    public static final String NODE_LAB = "labs";
    public static final String NODE_LAB_RESULT = "lab_result";
    public static final Map<String, String> CARD_MAP_MIRTH = new HashMap<String, String>();

    static {
        // Order of each list is critical first in list is closer to root of tree
        List<String> labCards = new ArrayList<String>();
        labCards.add(NODE_LAB);
        CARD_LAB.setId(1);
        CARD_LAB.setCard(labCards);

        List<String> labResultCards = new ArrayList<String>();
        labResultCards.add(NODE_LAB);
        labResultCards.add(NODE_LAB_RESULT);
        CARD_LAB_RESULT.setId(2);
        CARD_LAB_RESULT.setCard(labResultCards);

        CARD_MAP_MIRTH.put(MirthCard.NODE_ORCOBRNTEOBXNTECTI.toUpperCase(), NODE_LAB);
        CARD_MAP_MIRTH.put(MirthCard.NODE_OBXNTE.toUpperCase(), NODE_LAB_RESULT);

    }

    public static Cardinality getMasterCard(String path) {
        Cardinality card = null;
        if (path != null) {
            if (path.indexOf("/" + MasterCard.NODE_LAB_RESULT + "/") > 0) {
                card = MasterCard.CARD_LAB_RESULT;
            } else if (path.indexOf("/" + MasterCard.NODE_LAB_RESULT + "/") == -1
                    && path.indexOf("/" + MasterCard.NODE_LAB + "/") > 0) {
                card = MasterCard.CARD_LAB;
            }
        }
        return card;
    }

    public static String translateMirthPath(String mirthPath, String masterPath) {

        //TODO this if should be temp
        if(mirthPath.indexOf(MirthCard.NODE_OBXNTE)>0){
        masterPath = masterPath.replace(MasterCard.NODE_LAB, MasterCard.NODE_LAB+"/"+MasterCard.NODE_LAB_RESULT);
        }
        
        if (mirthPath.indexOf("[") > 0) {

            String[] mirthParts = mirthPath.split("/");
            for (int i = 0; i < mirthParts.length; i++) {
                String mirthPart = mirthParts[i].trim();
                String masterPart = null;
                if (mirthPart.length() > 0) {
                    int openb = mirthPart.indexOf("[");
                    int closb = mirthPart.indexOf("]");
                    String mp = mirthPart;
                    String mc = null;
                    if (openb > 0 && closb > 0 && openb < closb) {
                        mp = mirthPart.substring(0, openb);
                        mc = mirthPart.substring(openb, closb + 1);
                        masterPart = CARD_MAP_MIRTH.get(mp.toUpperCase());
                        if (masterPart != null && mc != null) {
                            String replace = masterPart + mc;
                            try{
                            masterPath = masterPath.replace(masterPart, replace);
                            }catch(Exception e){
                                String m = e.getMessage();
                            }
                        }
                    }
                }
            }
        }
        return masterPath;
    }
}
