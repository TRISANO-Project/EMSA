package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterDataType extends BaseIdLabel {

    public static final MasterDataType STRING = new MasterDataType(1, "STRING");
    public static MasterDataType DATE = new MasterDataType(2, "DATE");
    public static MasterDataType NUMBER = new MasterDataType(3, "NUMBER");

    public MasterDataType(Integer id, String label) {
       super(id,label);
    }
}
