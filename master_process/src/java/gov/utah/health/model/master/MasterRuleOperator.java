package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterRuleOperator extends BaseIdLabel {

    public static MasterRuleOperator DIRECT_COPY = new MasterRuleOperator(1, "Direct Copy");
    public static MasterRuleOperator CODE_LOOKUP = new MasterRuleOperator(2, "Code Lookup");
    public static MasterRuleOperator COMPLEX_RULE = new MasterRuleOperator(3, "Complex Rule");
    public static MasterRuleOperator LAB_RESULT_VALUE = new MasterRuleOperator(99, "Lab Result Value");
    public static MasterRuleOperator LAB_TEST_RESULT_ID = new MasterRuleOperator(4, "Lab Test Result Id");
    public static MasterRuleOperator CLINICIAN_PERSON_TYPE = new MasterRuleOperator(5, "Clinician Person Type");
    public static MasterRuleOperator EVENT_TYPE = new MasterRuleOperator(6, "Event Type");
    public static MasterRuleOperator AGE_AT_ONSET = new MasterRuleOperator(7, "Age At Onset");
    public static MasterRuleOperator PARENT_GUARDIAN = new MasterRuleOperator(8, "Parent Guardian");
    public static MasterRuleOperator NOTE_TYPE = new MasterRuleOperator(9, "Note Type");
    public static MasterRuleOperator DISEASE_ID = new MasterRuleOperator(10, "Disease Id");
    public static MasterRuleOperator ORGANISM_ID = new MasterRuleOperator(11, "Organism Id");
    public static MasterRuleOperator STATE_CASE_STATUS_ID = new MasterRuleOperator(12, "State Case Status Id");
    public static MasterRuleOperator REFERENCE_RANGE = new MasterRuleOperator(13, "Reference Range");
    public static MasterRuleOperator TEST_TYPE_ID = new MasterRuleOperator(14, "Test Type");
    public static MasterRuleOperator SPECIMEN_SOURCE_ID = new MasterRuleOperator(15, "Specimen Source");
    public static MasterRuleOperator POSTAL_CODE = new MasterRuleOperator(16, "Postal Code");
    public static MasterRuleOperator JURISDICTION_ID = new MasterRuleOperator(17, "Jurisdiction Id");
    
    

    public MasterRuleOperator(Integer id, String label) {
        super(id, label);
    }
}
