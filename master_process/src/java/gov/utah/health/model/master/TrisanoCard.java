package gov.utah.health.model.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author UDOH
 */
public class TrisanoCard extends Cardinality {
    // These lists define the cardinality higherarchy of the trisano model
    public static final Cardinality CARD_LAB = new Cardinality();
    public static final Cardinality CARD_LAB_RESULT = new Cardinality();
    public static final String NODE_LAB = "labs";
    public static final String NODE_LAB_RESULT = "lab_result";
    public static final Map<String, String> CARD_MAP_MASTER = new HashMap<String, String>();

    static {
        // Order of each list is critical first in list is closer to root of tree
        List<String> labCards = new ArrayList<String>();
        labCards.add(NODE_LAB);
        CARD_LAB.setId(1);
        CARD_LAB.setCard(labCards);

        List<String> labResultCards = new ArrayList<String>();
        labResultCards.add(NODE_LAB);
        labResultCards.add(NODE_LAB_RESULT);
        CARD_LAB_RESULT.setId(2);
        CARD_LAB_RESULT.setCard(labResultCards);

        CARD_MAP_MASTER.put(MasterCard.NODE_LAB.toUpperCase(), NODE_LAB);
        CARD_MAP_MASTER.put(MasterCard.NODE_LAB_RESULT.toUpperCase(), NODE_LAB_RESULT);

    }

    public static Cardinality getTrisanoCard(String path) {
        Cardinality card = null;
        if (path != null) {
            if (path.indexOf("/" + MasterCard.NODE_LAB_RESULT + "/") > 0) {
                card = MasterCard.CARD_LAB_RESULT;
            } else if (path.indexOf("/" + MasterCard.NODE_LAB_RESULT + "/") == -1
                    && path.indexOf("/" + MasterCard.NODE_LAB + "/") > 0) {
                card = MasterCard.CARD_LAB;
            }
        }
        return card;
    }

    public static String translateMasterPath(String mp, String tp) {

        //TODO this if should be temp
        if(mp.indexOf(MasterCard.NODE_LAB_RESULT)>0){
        mp = mp.replace(TrisanoCard.NODE_LAB, TrisanoCard.NODE_LAB+"/"+TrisanoCard.NODE_LAB_RESULT);
        }
        
        if (mp.indexOf("[") > 0) {

            String[] mpParts = mp.split("/");
            for (int i = 0; i < mpParts.length; i++) {
                String mPart = mpParts[i].trim();
                String trisanoPart = null;
                if (mPart.length() > 0) {
                    int openb = mPart.indexOf("[");
                    int closb = mPart.indexOf("]");
                    String mpv = mPart;
                    String mc = null;
                    if (openb > 0 && closb > 0 && openb < closb) {
                        mpv = mPart.substring(0, openb);
                        mc = mPart.substring(openb, closb + 1);
                        trisanoPart = CARD_MAP_MASTER.get(mpv.toUpperCase());
                        if (trisanoPart != null && mc != null) {
                            String replace = trisanoPart + mc;
                            try{
                            tp = tp.replace(trisanoPart, replace);
                            }catch(Exception e){
                                String m = e.getMessage();
                            }
                        }
                    }
                }
            }
        }
        return tp;
    }
}
