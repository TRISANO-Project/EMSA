package gov.utah.health.model.master;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author UDOH
 */
public class Cardinality {

    private Integer id;
    private List<String> card;
    private Map<String, Integer> cardCounts;

    public Cardinality() {
    }

    public Cardinality(Integer id, List<String> card) {
        this.id = id;
        this.card = card;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getCard() {
        return card;
    }

    public void setCard(List<String> card) {
        this.card = card;
    }

    public Map<String, Integer> getCardCounts() {
        if (this.cardCounts == null) {
            cardCounts = new HashMap<String, Integer>();
            if (this.card != null) {
                for (String c : card) {
                    cardCounts.put(c, new Integer(0));
                }
            }
        }
        return cardCounts;
    }
}
