package gov.utah.health.model.master;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author UDOH
 */
public class MirthCard extends Cardinality {
    // These lists define the cardinality higherarchy of the mirth hl7 model

    public static final Cardinality CARD_ORCOBRNTEOBXNTECTI = new Cardinality();
    public static final Cardinality CARD_OBXNTE = new MirthCard();
    public static final Cardinality CARD_NTE = new MirthCard();
    public static final String NODE_NTE = "NTE";
    public static final String NODE_OBXNTE = "ORU_R01.OBXNTE";
    public static final String NODE_ORCOBRNTEOBXNTECTI = "ORU_R01.ORCOBRNTEOBXNTECTI";

    static {
        // Order of each list is critical first in list is closer to root of tree
        List<String> orcObxNteCardParts = new ArrayList<String>();
        orcObxNteCardParts.add(NODE_ORCOBRNTEOBXNTECTI);
        CARD_ORCOBRNTEOBXNTECTI.setId(1);
        CARD_ORCOBRNTEOBXNTECTI.setCard(orcObxNteCardParts);


        List<String> obxNteCardParts = new ArrayList<String>();
        obxNteCardParts.add(NODE_ORCOBRNTEOBXNTECTI);
        obxNteCardParts.add(NODE_OBXNTE);
        CARD_OBXNTE.setId(2);
        CARD_OBXNTE.setCard(obxNteCardParts);

        List<String> nteCardParts = new ArrayList<String>();
        nteCardParts.add(NODE_ORCOBRNTEOBXNTECTI);
        nteCardParts.add(NODE_OBXNTE);
        nteCardParts.add(NODE_NTE);
        CARD_NTE.setId(3);
        CARD_NTE.setCard(nteCardParts);
    }

    public static Cardinality getMirthCard(String path) {
        Cardinality card = null;
        if (path != null) {
            if (path.indexOf("/" + MirthCard.NODE_NTE + "/") > 0) {
                card = MirthCard.CARD_NTE;
            } else if (path.indexOf("/" + MirthCard.NODE_NTE + "/") == -1
                    && path.indexOf("/" + MirthCard.NODE_OBXNTE + "/") > 0) {
                card = MirthCard.CARD_OBXNTE;
            } else if (path.indexOf("/" + MirthCard.NODE_NTE + "/") == -1
                    && path.indexOf("/" + MirthCard.NODE_OBXNTE + "/") == -1
                    && path.indexOf("/" + MirthCard.NODE_ORCOBRNTEOBXNTECTI + "/") > 0) {
                card = MirthCard.CARD_ORCOBRNTEOBXNTECTI;
            }
        }
        return card;
    }
}
