package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterOperator extends BaseIdLabel {

    public static MasterOperator EQUAL = new MasterOperator(1, "Equal");
    public static MasterOperator NOT_EQUAL = new MasterOperator(2, "Not Equal");
    public static MasterOperator GREATER_THAN = new MasterOperator(3, "Greater Than");
    public static MasterOperator LESS_THAN = new MasterOperator(4, "Less Than");
    public static MasterOperator GREATER_THAN_OR_EQUAL_TO = new MasterOperator(5, "Greater than or equal to");
    public static MasterOperator LESS_THAN_OR_EQUAL_TO = new MasterOperator(6, "Less than or equal to");
    public static MasterOperator AND = new MasterOperator(7, "And");
    public static MasterOperator OR = new MasterOperator(8, "Or");
    public static MasterOperator IN = new MasterOperator(9, "In");

    public MasterOperator(Integer id, String label) {
        super(id, label);
    }
}
