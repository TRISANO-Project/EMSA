package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterList extends BaseIdLabel {

    public static final MasterList WHITE_LIST = new MasterList(1, "White");
    public static MasterList GREY_LIST = new MasterList(2, "Grey");
    public static MasterList BLACK_LIST = new MasterList(4, "Black");
    public static final MasterList EXCEPTION = new MasterList(3, "Exception");
    public static final MasterList ENTRY = new MasterList(17, "Entry");

    public MasterList(Integer id, String label) {
        super(id, label);
    }
    public MasterList(Integer id) {
        super(id, null);        
        if(MasterList.BLACK_LIST.getId().equals(id)){
            super.setLabel(MasterList.BLACK_LIST.getLabel());
        }
        else if(MasterList.WHITE_LIST.getId().equals(id)){
            super.setLabel(MasterList.WHITE_LIST.getLabel());
        }
        else if(MasterList.GREY_LIST.getId().equals(id)){
            super.setLabel(MasterList.GREY_LIST.getLabel());
        }
        else if(MasterList.ENTRY.getId().equals(id)){
            super.setLabel(MasterList.ENTRY.getLabel());
        }
        else if(MasterList.EXCEPTION.getId().equals(id)){
            super.setLabel(MasterList.EXCEPTION.getLabel());
        }
        else{
            super.setLabel("UNKNOWN");
        }
        
    }
}
