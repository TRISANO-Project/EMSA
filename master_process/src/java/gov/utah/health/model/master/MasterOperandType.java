package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterOperandType extends BaseIdLabel {

    public static MasterOperandType FROM_VALUE = new MasterOperandType(1, "From value");
    public static MasterOperandType FROM_PATH = new MasterOperandType(2, "From path");
    public static MasterOperandType FROM_NOW = new MasterOperandType(3, "From now");
    public static MasterOperandType FROM_PATH_DATE = new MasterOperandType(4, "From path date");
    public static MasterOperandType FROM_LOOKUP = new MasterOperandType(5, "From lookup");
    public static MasterOperandType FROM_IN = new MasterOperandType(6, "From in");

    public MasterOperandType(Integer id, String label) {
        super(id, label);
    }
}
