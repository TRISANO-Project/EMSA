package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterPaths {

    public static final String masterSegmentIdxPath = "/health/labs/segment_index";
    public static final String masterLocalLoincPath1 = "/health/labs/local_loinc_code";
    public static final String masterLocalLoincPath2 = "/health/labs/local_code";
    public static final String masterLoincPath = "/health/labs/loinc_code";
    public static final String reportDatePath = "/health/reporting/report_date";
    public static final String diseaseNamePath = "/health/disease/name";
    public static final String firstNamePath = "/health/person/first_name";
    public static final String lastNamePath = "/health/person/last_name";
    public static final String clinFirstNamePath = "/health/clinicians/first_name";
    public static final String clinLastNamePath = "/health/clinicians/last_name";
    public static final String localResultValue1 = "/health/labs/local_result_value";
    public static final String localResultValue2 = "/health/labs/local_result_value_2";
    public static final String personZip = "/health/person/zip";
    public static final String diagnosticZipcode = "/health/diagnostic/zipcode";
    


}
