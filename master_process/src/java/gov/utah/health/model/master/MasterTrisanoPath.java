package gov.utah.health.model.master;

import gov.utah.health.rules.trisano.PathRule;
import gov.utah.health.rules.trisano.PathRuleList;

/**
 *
 * @author UDOH
 */
public class MasterTrisanoPath {

    
    private String masterPath;
    private Integer masterPathId;
    private String trisanoPath;
    private Integer trisanoPathId;
    private Integer lookupOperatorId;
    private TrisanoCodeTable codeTable;
    private String appCategory;
    private Integer trisanoCode;
    private String derivedValue;
    private PathRule rule;
    
    public MasterTrisanoPath(String masterPath, Integer masterPathId, 
            String trisanoPath, Integer trisanoPathId,
            Integer lookupOperatorId, String codeTable, String appCategory) {
        this.masterPath = masterPath;
        this.masterPathId = masterPathId;
        this.trisanoPath = trisanoPath;
        this.trisanoPathId = trisanoPathId;
        this.lookupOperatorId = lookupOperatorId;
        if(codeTable != null && codeTable.trim().length() > 0){
            this.codeTable = new TrisanoCodeTable(codeTable);
        }
        this.appCategory = appCategory;
        this.rule = PathRuleList.getRule(trisanoPathId);
    }

    public PathRule getRule() {
        return rule;
    }

    public void setRule(PathRule rule) {
        this.rule = rule;
    }

    public String getDerivedValue() {
        return derivedValue;
    }

    public void setDerivedValue(String derivedValue) {
        this.derivedValue = derivedValue;
    }

    public Integer getMasterPathId() {
        return masterPathId;
    }

    public void setMasterPathId(Integer masterPathId) {
        this.masterPathId = masterPathId;
    }

    public Integer getTrisanoPathId() {
        return trisanoPathId;
    }

    public void setTrisanoPathId(Integer trisanoPathId) {
        this.trisanoPathId = trisanoPathId;
    }

    public String getMasterPath() {
        return masterPath;
    }

    public void setMasterPath(String masterPath) {
        this.masterPath = masterPath;
    }

    public String getTrisanoPath() {
        return trisanoPath;
    }

    public void setTrisanoPath(String trisanoPath) {
        this.trisanoPath = trisanoPath;
    }

    public Integer getLookupOperatorId() {
        return lookupOperatorId;
    }

    public void setLookupOperatorId(Integer lookupOperatorId) {
        this.lookupOperatorId = lookupOperatorId;
    }

    public Integer getTrisanoCode() {
        return trisanoCode;
    }

    public void setTrisanoCode(Integer trisanoCode) {
        this.trisanoCode = trisanoCode;
    }

    public TrisanoCodeTable getCodeTable() {
        return codeTable;
    }

    public void setCodeTable(TrisanoCodeTable codeTable) {
        this.codeTable = codeTable;
    }

    public String getAppCategory() {
        return appCategory;
    }

    public void setAppCategory(String appCategory) {
        this.appCategory = appCategory;
    }
   
    
    public boolean needsId(){
        boolean needsId = false;
        if(this.codeTable != null){
            needsId = true;
        }
        return needsId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.trisanoPathId != null ? this.trisanoPathId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MasterTrisanoPath other = (MasterTrisanoPath) obj;
        if (this.trisanoPathId != other.trisanoPathId && (this.trisanoPathId == null || !this.trisanoPathId.equals(other.trisanoPathId))) {
            return false;
        }
        return true;
    }


    
}
