package gov.utah.health.model.master;

import java.util.List;

/**
 *
 * @author UDOH
 */
public class MasterPathRuleSet {

    private Integer id;
    private String path;
    private Boolean required;
    private MasterDataType dataType;
    private List<MasterRule> rules;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public MasterDataType getDataType() {
        return dataType;
    }

    public void setDataType(MasterDataType dataType) {
        this.dataType = dataType;
    }

    public List<MasterRule> getRules() {
        return rules;
    }

    public void setRules(List<MasterRule> rules) {
        this.rules = rules;
    }
    
    
}
