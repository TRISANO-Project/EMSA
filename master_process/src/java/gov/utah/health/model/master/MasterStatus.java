package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterStatus extends BaseIdLabel {

    public static MasterStatus EXCEPTION = new MasterStatus(3, "Exception");
    public static MasterStatus PENDING = new MasterStatus(12, "Pending");
    public static MasterStatus ENTRY = new MasterStatus(17, "Entry");

    public MasterStatus(Integer id, String label) {
        super(id,label);
    }
}
