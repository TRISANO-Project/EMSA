package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MirthPath {

    private Integer id;
    private Integer labId;
    private String messageVersion;
    private Integer masterPathId;
    private String masterPath;
    private String glueString;
    private String repeatingElement;
    private String xpath;
    private Integer sequence;
    private Cardinality mirthCard;
    private Cardinality masterCard;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLabId() {
        return labId;
    }

    public void setLabId(Integer labId) {
        this.labId = labId;
    }

    public Cardinality getMirthCard() {

        if (this.mirthCard == null) {
            mirthCard = MirthCard.getMirthCard(this.xpath);
        }
        return mirthCard;
    }

    public Cardinality getMasterCard() {

        if (this.masterCard == null) {
            masterCard = MasterCard.getMasterCard(this.masterPath);
        }
        return masterCard;
    }

    public String getRepeatingElement() {
        //TODO get this value from the database
        if (this.xpath != null && this.xpath.indexOf("ORU_R01.OBXNTE") > 0) {
            return "ORU_R01.OBXNTE";
        } else {
            return repeatingElement;
        }

    }

    public void setRepeatingElement(String repeatingElement) {
        this.repeatingElement = repeatingElement;
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public String getMasterPath() {
        return masterPath;
    }

    public void setMasterPath(String masterPath) {
        this.masterPath = masterPath;
    }

    public Integer getMasterPathId() {
        return masterPathId;
    }

    public void setMasterPathId(Integer masterPathId) {
        this.masterPathId = masterPathId;
    }

    public String getGlueString() {
        return glueString;
    }

    public void setGlueString(String glueString) {
        this.glueString = glueString;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Boolean append() {
        Boolean append = Boolean.FALSE;
        if (this.sequence != null && this.sequence > 1) {
            append = Boolean.TRUE;
        }
        return append;
    }
}
