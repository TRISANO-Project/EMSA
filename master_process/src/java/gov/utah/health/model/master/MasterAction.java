package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterAction extends BaseIdLabel {

    public static MasterAction DUPLICATE_MESSAGE = new MasterAction(7, "Duplicate Message");
    public static final MasterAction MESSAGE_DELETED = new MasterAction(8, "Message Deleted");
    public static MasterAction WHITE = new MasterAction(9, "White");
    public static MasterAction BLACK = new MasterAction(10, "Black");
    public static MasterAction GREY = new MasterAction(11, "Gray");
    public static MasterAction PENDING = new MasterAction(12, "Pending");
    public static MasterAction EXCEPTION = new MasterAction(13, "Exception");
    public static MasterAction HOLDING = new MasterAction(14, "Holding");
    public static MasterAction INVALID_PARAMS_FROM_MIRTH = new MasterAction(30, "Invalid Params From Mirth");
    public static MasterAction CREATED_TRISANO_XML = new MasterAction(31, "Trisano XML Created");

    public MasterAction(Integer id, String label) {
        super(id,label);
    }
}
