package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class MasterException extends BaseIdLabel {

    // original
    public static final MasterException CHILD_LOINC_MISSING_ELR = new MasterException(1, "1" ,3);
    public static final MasterException CHILD_LOINC_MISSING_MESSAGE = new MasterException(2, "2" ,3);
    public static final MasterException INVALID_ZIP_CODE = new MasterException(3, "3" ,3);
    public static final MasterException SQL_AGENT_ERROR = new MasterException(4, "4" ,3);
    public static final MasterException MESSAGE_CRITERIA_NOT_MET = new MasterException(5, "5" ,3);
//    public static final MasterException ZIP_CODE_MISSING = new MasterException(6, "6" ,12);
    public static final MasterException LAB_EVENT_SAME = new MasterException(7, "7" ,3);
    public static final MasterException LAB_MISSING_SPECIMEN = new MasterException(8, "8" ,3);
    public static final MasterException LAB_MISSING_DISEASE = new MasterException(9, "9" ,3);
    public static final MasterException LAB_MISSING_ORGANISM = new MasterException(10, "10" ,3);
 //   public static final MasterException LAB_MISSING = new MasterException(11, "11" ,12);
    public static final MasterException LAB_MISSING_TEST_TYPE = new MasterException(12, "12" ,3);
    public static final MasterException LOINC_VALUES_MISSING = new MasterException(13, "13" ,3);
    public static final MasterException TEST_RESULT_VALUE_MISSING = new MasterException(14, "14" ,3);
    public static final MasterException NO_LAB_NOT_IMPORTED = new MasterException(15, "15" ,3);
    public static final MasterException NO_HEALTH_OBJ_CREATED = new MasterException(16, "16" ,3);
    public static final MasterException COULD_NOT_MAP_SYSTEM = new MasterException(17, "17" ,3);
    public static final MasterException IMPORT_SQL_BAD_RESPONSE = new MasterException(18, "18" ,3);
    public static final MasterException DO_RULES_SQL_BAD_RESPONSE = new MasterException(19, "19" ,3);
    public static final MasterException COULD_NOT_CREATE_SYSTEM_OBJ = new MasterException(20, "20" ,3);
    public static final MasterException NO_LOINC_RESULT_RULES_EVALUATED_TRUE = new MasterException(21, "21" ,3);
    public static final MasterException FAILIED_TO_ADD_OR_UPDATE = new MasterException(22, "22" ,3);
    public static final MasterException RESULT_VALUE_NOT_MAPPED = new MasterException(23, "23" ,3);
 //   public static final MasterException BAD_BIRTH_DATE = new MasterException(24, "24" ,12);
 //   public static final MasterException LAST_NAME_MISSING_INVALID = new MasterException(25, "25" ,12);
 //   public static final MasterException LAB_NAME_MISSING_INVALID = new MasterException(26, "26" ,12);
//    public static final MasterException LOINC_CODE_NOT_MAPPED_TO_MASTER = new MasterException(27, "27" ,3);
//    public static final MasterException ADDRESS_NOT_FOUND = new MasterException(28, "28" ,12);
//    public static final MasterException SPECIMEN_NOT_FOUND = new MasterException(29, "29" ,12);
//    public static final MasterException REFERENCE_RANGE_NOT_FOUND_QUANTITATIVE = new MasterException(30, "30" ,12);

    //added
    public static final MasterException MISSING_REQUIRED_FIELD = new MasterException(31, "31" ,3);
    public static final MasterException INVALID_VALUE_FOR_DATATYPE = new MasterException(32, "32" ,3);
    public static final MasterException VALIDATION_RULE_FAILURE = new MasterException(33, "33" ,3);
    public static final MasterException MIRTH_TO_MASTER_MAPPINGS_NOT_FOUND = new MasterException(34, "34" ,3);
    public static final MasterException MASTER_TO_TRISANO_MAPPINGS_NOT_FOUND = new MasterException(35, "35" ,3);
    public static final MasterException MISSING_MIRTH_PARAM_ORIGINAL_MESSAGE_ID = new MasterException(36, "36" ,3);
    public static final MasterException MISSING_MIRTH_PARAM_HL7XML = new MasterException(37, "37" ,3);
    public static final MasterException MISSING_MIRTH_PARAM_LAB_NAME = new MasterException(38, "38" ,3);
    public static final MasterException MISSING_MIRTH_PARAM_VERSION = new MasterException(39, "39" ,3);
    public static final MasterException LAB_ID_NOT_FOUND_FOR_LAB_NAME = new MasterException(40, "40" ,3);
    public static final MasterException ORIGINAL_MESSAGE_ID_NAN = new MasterException(41, "41" ,3);
    public static final MasterException UNABLE_TO_TRANSFORM_MIRTHXML_TO_MASTERDOC = new MasterException(42, "42" ,3);
    public static final MasterException UNABLE_TO_CONVERT_MASTERDOC_TO_MASTERXML = new MasterException(43, "43" ,3);
    public static final MasterException UNABLE_TO_OBTAIN_EMPTY_TRISANODOC = new MasterException(44, "44" ,3);
    public static final MasterException UNABLE_TO_ADD_SYSTEM_MESSAGE = new MasterException(45, "45" ,3);
    public static final MasterException ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC = new MasterException(46, "46" ,3);
    public static final MasterException ERROR_ACCESSING_TRISANOPATH_IN_TRISANODOC = new MasterException(47, "47" ,3);
    public static final MasterException ERROR_SETTING_VALUE_IN_TRISANODOC = new MasterException(48, "48" ,3);
    public static final MasterException NO_APP_CODED_VALUE_FOR_CHILD_CONCEPT = new MasterException(49, "49" ,3);
    public static final MasterException UNABLE_TO_CONVERT_TRISANODOC_TO_TRISANOXML = new MasterException(50, "50" ,3);
    public static final MasterException ERROR_FINDING_APP_CODE_WITH_LOCALLOINC = new MasterException(51, "51" ,3);
    public static final MasterException NO_APP_CODE_FOUND_WITH_LOCALLOINC = new MasterException(52, "52" ,3);
    public static final MasterException TRISANO_ID_LOOKUP_FAILED = new MasterException(53, "53" ,3);
    public static final MasterException UNABLE_TO_OBTAIN_DATE_MESSAGE_RECEIVED = new MasterException(54, "54" ,3);
    public static final MasterException UNABLE_TO_SET_DATE_MESSAGE_RECEIVED_IN_DOCUMENT = new MasterException(55, "55" ,3);
    public static final MasterException UNABLE_TO_EVALUATE_RULE = new MasterException(56, "56" ,3);
    public static final MasterException UNABLE_TO_LOOKUP_DISEASE_NAME = new MasterException(57, "57" ,3);
    public static final MasterException UNABLE_TO_LOOKUP_ORGANISM = new MasterException(58, "58" ,3);
    public static final MasterException UNABLE_TO_SET_DATE_IN_MASTERDOC = new MasterException(59, "59" ,3);
    public static final MasterException UNABLE_TO_SET_MASTERLOINC_IN_MASTERDOC = new MasterException(60, "60" ,3);
    public static final MasterException NO_REFERENCE_RANGE_FOUND_IN_MASTERDOC = new MasterException(61, "61" ,3);
    public static final MasterException TRISANO_ID_NEEDED_NONE_FOUND = new MasterException(62, "62" ,3);
    public static final MasterException NO_TEST_TYPE_FOUND_FOR_MASTER_LOINC = new MasterException(63, "63" ,3);
    public static final MasterException NO_DISEASE_NAME_FOUND_FOR_LOCAL_LOINC = new MasterException(64, "64" ,3);
    public static final MasterException NO_TEST_RESULT_RULES_FOUND_FOR_LOCAL_LOINC = new MasterException(65, "65" ,3);
    public static final MasterException SPECIMEN_NOT_MAPPED = new MasterException(67, "67" ,3);
    public static final MasterException PATIENT_NAME_CHANGED_BY_MARRIAGE = new MasterException(68, "68" ,3);
    public static final MasterException ENTRY_QUEUE = new MasterException(69, "69" ,3);
    public static final MasterException WHITELIST_RULE = new MasterException(70, "70" ,3);
    public static final MasterException UNEXPECTED_VALUE_LOCAL_RESULT_VALUE = new MasterException(71, "71" ,3);    
    public static final MasterException NO_CASE_MANAGE_RULES_FOUND_FOR_MASTER_LOINC = new MasterException(72, "72" ,3);
    public static final MasterException NO_CASE_MANAGE_RULES_TRUE = new MasterException(73, "73" ,3);
    public static final MasterException NO_MASTER_VOCAB_ID_FOUND_FOR_RESULT_VALUE = new MasterException(74, "74" ,3);

    public MasterException(Integer id, String label, Integer typeId) {
        super(id, label);
        this.typeId = typeId;
    }
    public MasterException(MasterException me, String message) {
        super(me.getId(), me.getLabel());
        this.typeId = me.getTypeId();
        this.message = message;
    }
    
    private Integer typeId;
    private String message;

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
