package gov.utah.health.model.master;

/**
 *
 * @author UDOH
 */
public class TrisanoCodeTable extends BaseIdLabel {

    public static final TrisanoCodeTable EXTERNAL_CODES = new TrisanoCodeTable(1, "external_codes");
    public static TrisanoCodeTable COMMON_TEST_TYPES = new TrisanoCodeTable(2, "common_test_types");
    public static TrisanoCodeTable DISEASES = new TrisanoCodeTable(3, "diseases");
    public static TrisanoCodeTable ORGANISMS = new TrisanoCodeTable(4, "organisms");
    public static TrisanoCodeTable CODES = new TrisanoCodeTable(5, "codes");
    public static TrisanoCodeTable TREATMENTS = new TrisanoCodeTable(6, "treatments");

    public TrisanoCodeTable(Integer id, String label) {
       super(id,label);
    }
    public TrisanoCodeTable(String label){
        super(-1,label);
        if(EXTERNAL_CODES.getLabel().equals(label)){super.setId(EXTERNAL_CODES.getId());}
        if(COMMON_TEST_TYPES.getLabel().equals(label)){super.setId(COMMON_TEST_TYPES.getId());}
        if(DISEASES.getLabel().equals(label)){super.setId(DISEASES.getId());}
        if(ORGANISMS.getLabel().equals(label)){super.setId(ORGANISMS.getId());}
        if(CODES.getLabel().equals(label)){super.setId(CODES.getId());}
        if(TREATMENTS.getLabel().equals(label)){super.setId(TREATMENTS.getId());}
    }
}
