package gov.utah.health.model.master;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 *
 * @author UDOH
 */
public class MasterRule {

    private Integer id;
    private Integer pathId;
    private Integer operatorId;
    private Integer operandTypeId;
    private String operandValue;
    private Integer sequence;
    private Integer andOrOperatorId;
    private Integer masterVocabId;
    private String operation;
    private String resultsToComments;

    public MasterRule(Integer operatorId, String operandValue, Integer sequence) {
        this.operatorId = operatorId;
        this.operandValue = operandValue;
        this.sequence = sequence;

    }

    public MasterRule() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResultsToComments() {
        return resultsToComments;
    }

    public void setResultsToComments(String resultsToComments) {
        this.resultsToComments = resultsToComments;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getMasterVocabId() {
        return masterVocabId;
    }

    public void setMasterVocabId(Integer masterVocabId) {
        this.masterVocabId = masterVocabId;
    }

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getOperandTypeId() {
        return operandTypeId;
    }

    public void setOperandTypeId(Integer operandTypeId) {
        this.operandTypeId = operandTypeId;
    }

    public String getOperandValue() {
        return operandValue;
    }

    public void setOperandValue(String operandValue) {
        this.operandValue = operandValue;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getAndOrOperatorId() {
        return andOrOperatorId;
    }

    public void setAndOrOperatorId(Integer andOrOperatorId) {
        this.andOrOperatorId = andOrOperatorId;
    }

    public Boolean evaluate(String input) throws Exception {
        Boolean result = false;
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");

        if (input != null) {
            engine.put("input", input);
            engine.eval("if" + this.operation + "{result = true;}else{result = false;}");
            result = (Boolean) engine.get("result");
        }

        return result;

    }
}
