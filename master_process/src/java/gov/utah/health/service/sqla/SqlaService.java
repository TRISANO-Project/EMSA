package gov.utah.health.service.sqla;

import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.model.trisano.TrisanoHealth;
import java.util.List;

/**
 *  Service level interface for operations specific to the query agent and Trisano
 *
 * @author UDOH
 */
public interface SqlaService {

    public TrisanoHealth findIds(List<MasterTrisanoPath> mtpList);
    
}
