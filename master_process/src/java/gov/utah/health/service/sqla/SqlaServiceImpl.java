package gov.utah.health.service.sqla;

import gov.utah.health.client.sqla.SqlAgentClient;
import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.model.master.TrisanoCodeTable;
import gov.utah.health.model.trisano.Codes;
import gov.utah.health.model.trisano.CommonTestTypes;
import gov.utah.health.model.trisano.Diseases;
import gov.utah.health.model.trisano.ExternalCodes;
import gov.utah.health.model.trisano.Organisms;
import gov.utah.health.model.trisano.Treatments;
import gov.utah.health.model.trisano.TrisanoHealth;
import gov.utah.health.util.HealthMarshal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Service level implementation class. Calls to this service are configured in
 * the applicationContext.xml file.
 *
 * @author UDOH
 */
@Service
public class SqlaServiceImpl implements SqlaService {

    public static final Logger logger = Logger.getLogger("SqlaServiceImpl");
    public static final ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    @Override
    public TrisanoHealth findIds(List<MasterTrisanoPath> mtpList) {

        TrisanoHealth th = new TrisanoHealth();
        String hmString = null;
        HealthMarshal marshal = (HealthMarshal) appContext.getBean("msgMarshal");
        Boolean lookup = false;
        for (MasterTrisanoPath mtp : mtpList) {
            if (mtp.getCodeTable() != null) {
                if (TrisanoCodeTable.CODES.equals(mtp.getCodeTable())) {
                    Codes code = new Codes();
                    code.setTheCode(mtp.getDerivedValue());
                    code.setCodeDescription(mtp.getDerivedValue());
                    th.addCode(code);
                    lookup = true;
                } else if (TrisanoCodeTable.EXTERNAL_CODES.equals(mtp.getCodeTable())) {
                    ExternalCodes ec = new ExternalCodes();
                    ec.setTheCode(mtp.getDerivedValue());
                    ec.setCodeDescription(mtp.getDerivedValue());
                    ec.setCodeName(mtp.getAppCategory());
                    th.addExternalCode(ec);
                    lookup = true;
                } else if (TrisanoCodeTable.DISEASES.equals(mtp.getCodeTable())) {
                    Diseases disease = new Diseases();
                    disease.setDiseaseName(mtp.getDerivedValue());
                    th.addDisease(disease);
                    lookup = true;
                } else if (TrisanoCodeTable.ORGANISMS.equals(mtp.getCodeTable())) {
                    Organisms org = new Organisms();
                    org.setOrganismName(mtp.getDerivedValue());
                    th.addOrganism(org);
                    lookup = true;
                } else if (TrisanoCodeTable.COMMON_TEST_TYPES.equals(mtp.getCodeTable())) {
                    CommonTestTypes ctt = new CommonTestTypes();
                    ctt.setCommonName(mtp.getDerivedValue());
                    th.addCommonTestType(ctt);
                    lookup = true;
                } else if (TrisanoCodeTable.TREATMENTS.equals(mtp.getCodeTable())) {
                    Treatments t = new Treatments();
                    t.setTreatmentName(mtp.getDerivedValue());
                    th.addTreatment(t);
                    lookup = true;
                }
            }
        }
        if (lookup) {



            HealthMessage hm = new HealthMessage();
            hm.addTrisanoHealth(th);
            hm.setSystem("TRISANO");

            try {
                hmString = marshal.getHealthMessageXml(hm);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to get health xml from healthMessage", e.getMessage());
            }
            if (hmString != null && hmString.trim().length() > 0) {

                hmString = SqlAgentClient.findId(hmString);

                try {
                    hm = marshal.getHealthMessageObject(hmString);
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Unable to get health xml from healthMessage", e.getMessage());
                }
            }

            th = hm.getTrisanoHealth().get(0);
        } else {
            th = null;
        }
        return th;
    }
}
