package gov.utah.health.service.master;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterDataType;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterOperator;
import gov.utah.health.model.master.MasterPathRuleSet;
import gov.utah.health.model.master.MasterRule;
import gov.utah.health.util.DateUtils;
import gov.utah.health.util.DocumentUtils;
import java.sql.Connection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

/**
 *
 * @author UDOH
 */
public class MasterValidator {

    public static final Logger logger = Logger.getLogger("MasterValidator");

    /**
     * Validate that the given masterDoc has valid values based on each path
     * definition. Checks are made for data type and basic value validation like
     * birth date < 100 years old
     */
    public static void validateMaster(Connection con, Document doc, List<MasterException> exList) {


        // get validation paths
        List<MasterPathRuleSet> pathRuleSets = MasterValidator.getPathValidators(con);
        for (MasterPathRuleSet prs : pathRuleSets) {

            String docValue = null;

            try {
                docValue = DocumentUtils.getPathValue(doc, prs.getPath());
            } catch (DOMException de) {
                exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, prs.getPath()));
            } catch (XPathExpressionException xe) {
                exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, prs.getPath()));
            }
            // check required fields
            if (prs.getRequired() && (docValue == null || docValue.trim().length() == 0)) {
                exList.add(new MasterException(MasterException.MISSING_REQUIRED_FIELD, prs.getPath()));
            }
            // if the document has a value apply validation
            if (docValue != null && docValue.trim().length() > 0) {

                //check document value for valid dataType format
                switch (prs.getDataType().getId()) {
                    case 2: // MasterDataType.DATE
                        if (!MasterValidator.isValidDateString(docValue)) {
                            exList.add(new MasterException(MasterException.INVALID_VALUE_FOR_DATATYPE, prs.getPath()));
                        } else {
                            // set date to master date format string
                            String masterDateString = DateUtils.getMasterDateFormat(docValue);
                            try {
                                //DocumentUtils.setPathValue(doc, prs.getPath(), masterDateString);
                                DocumentUtils.addPathValue(doc, prs.getPath(), masterDateString, "labs", 1, false);
                            } catch (DOMException e) {
                                exList.add(new MasterException(MasterException.UNABLE_TO_SET_DATE_IN_MASTERDOC, e.getMessage()));
                            } catch (XPathExpressionException e) {
                                exList.add(new MasterException(MasterException.UNABLE_TO_SET_DATE_IN_MASTERDOC, e.getMessage()));
                            }
                        }
                        break;
                    case 3: // MasterDataType.INTEGER
                        if (!MasterValidator.isValidInteger(docValue)) {
                            exList.add(new MasterException(MasterException.INVALID_VALUE_FOR_DATATYPE, prs.getPath()));
                        }
                        break;
                }


                // check document value against MasterPathRules
                // add to exList for each rule that evaluates to false.
                List<MasterRule> pathRules = prs.getRules();
                // r1 and r2 or r3 or r4 and r5 ((((r1 and/or r2) and/or (r3)) and/or (r4)) and/or (r5))
                Boolean result = false;
                for (MasterRule rule : pathRules) {
                    if (rule.getSequence() == 1) {
                        result = MasterValidator.evaluateRule(doc, prs, rule, docValue, exList);
                    } else {
                        if (rule.getAndOrOperatorId() == MasterOperator.AND.getId()) {
                            if (result && MasterValidator.evaluateRule(doc, prs, rule, docValue, exList)) {
                                result = true;
                            }
                        } else if (rule.getAndOrOperatorId() == MasterOperator.OR.getId()) {
                            if (result || MasterValidator.evaluateRule(doc, prs, rule, docValue, exList)) {
                                result = true;
                            }
                        }
                    }
                    if (!result) {
                        exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, prs.getPath()));
                    }
                }
            }
        }
    }

    private static Boolean evaluateRule(Document doc, MasterPathRuleSet pv, MasterRule rule, String docValue, List<MasterException> exList) {

        Boolean result = false;
//String msg = "MasterValidator-evaluateRule id="+rule.getId()+", docValue="+docValue+", MasterPathRuleSet[id="+pv.getId()+", path"+pv.getPath()+"]"; 
//logger.info(msg);
        Object operandValue = MasterValidator.getOperandValue(doc, rule);

        switch (rule.getOperatorId()) {
            case 1: // MasterOperator.EQUAL
                if (MasterValidator.isEqual(pv.getDataType(), docValue, operandValue)) {
                    result = true;
                } else {
                    exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, pv.getPath() + "/" + docValue + " " + MasterOperator.EQUAL.getLabel() + " " + operandValue));
                }
                ;
                break;
            case 2: // MasterOperator.NOT_EQUAL
                if (!MasterValidator.isEqual(pv.getDataType(), docValue, operandValue)) {
                    result = true;
                } else {
                    exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, pv.getPath() + "/" + docValue + " " + MasterOperator.NOT_EQUAL.getLabel() + " " + operandValue));
                }
                ;
                break;
            case 3: // MasterOperator.GREATER_THAN
                if (MasterValidator.isGreaterThan(pv.getDataType(), docValue, operandValue)) {
                    result = true;
                } else {
                    exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, pv.getPath() + "/" + docValue + " " + MasterOperator.GREATER_THAN.getLabel() + " " + operandValue));
                }
                ;
                break;
            case 4: // MasterOperator.LESS_THAN
                if (MasterValidator.isLessThan(pv.getDataType(), docValue, operandValue)) {
                    result = true;
                } else {
                    exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, pv.getPath() + "/" + docValue + " " + MasterOperator.LESS_THAN.getLabel() + " " + operandValue));
                }
                ;
                break;
            case 5: // MasterOperator.GREATER_THAN_OR_EQUAL_TO
                if (MasterValidator.isGreaterThan(pv.getDataType(), docValue, operandValue)
                        || MasterValidator.isEqual(pv.getDataType(), docValue, operandValue)) {
                    result = true;
                } else {
                    exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, pv.getPath() + "/" + docValue + " " + MasterOperator.GREATER_THAN_OR_EQUAL_TO.getLabel() + " " + operandValue));
                }
                ;
                break;
            case 6: // MasterOperator.LESS_THAN_OR_EQUAL_TO
                if (MasterValidator.isLessThan(pv.getDataType(), docValue, operandValue)
                        || MasterValidator.isEqual(pv.getDataType(), docValue, operandValue)) {
                    result = true;
                } else {
                    exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, pv.getPath() + "/" + docValue + " " + MasterOperator.LESS_THAN_OR_EQUAL_TO.getLabel() + " " + operandValue));
                }
                ;
                break;
            case 9: // MasterOperator.IN
                if (MasterValidator.isIn(docValue, operandValue)) {
                    result = true;
                } else {
                    exList.add(new MasterException(MasterException.VALIDATION_RULE_FAILURE, pv.getPath() + "/" + docValue + " " + MasterOperator.IN.getLabel() + " " + operandValue));
                }
                ;
                break;
        }
        return result;
    }

    private static List<MasterPathRuleSet> getPathValidators(Connection con) {

        List<MasterPathRuleSet> pathValidators = ElrDao.getMasterPathValidators(con);
        return pathValidators;

    }

    private static Object getOperandValue(Document doc, MasterRule rule) {

        Object value = null;

        Integer operandTypeId = rule.getOperandTypeId();
        String operandValue = rule.getOperandValue();
        GregorianCalendar gc;

        switch (operandTypeId) {
            case 1: // MasterOperandType.FROM_VALUE

                value = operandValue;
                break;

            case 2: // MasterOperandType.FROM_PATH

                try {
                    value = DocumentUtils.getPathValue(doc, operandValue);
                } catch (Exception e) {

                    logger.log(Level.SEVERE, "Unable to find value for operandValue", e);
                }
                break;

            case 3: // MasterOperandType.FROM_NOW

                gc = new GregorianCalendar();

                if (operandValue.trim().length() > 0) {
                    // oVal in the form [amount,field]
                    String[] oValArray = operandValue.split(",");
                    if (oValArray.length == 2) {
                        //0=+-int
                        //1=int
                        // 6=Day
                        // 2=Month
                        // 1=Year
                        int amount = Integer.parseInt(oValArray[0]);
                        int field = Integer.parseInt(oValArray[1]);
                        gc.add(field, amount);
                        value = gc.getTime();

                    } else {
                        logger.log(Level.SEVERE, "Invalid operand format from now. operandValue=" + operandValue);
                    }
                } else {
                    value = gc.getTime();
                }

                break;
            case 4: // MasterOperandType.FROM_PATH_DATE
                //['path']
                //or
                //['path','number','duration']
                //gc = new GregorianCalendar();

                if (operandValue.trim().length() > 0) {

                    String[] oValArray = operandValue.split(",");
                    if (oValArray.length == 1) {
                        // assume if one item a path
                        String pathToDate = oValArray[0];
                        String dateString = null;
                        try {
                            dateString = DocumentUtils.getPathValue(doc, pathToDate);
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "unable to get referenced date in doc.", e);

                        }

                        value = DateUtils.parseDate(dateString);

                    } else if (oValArray.length == 3) {
                        String path = oValArray[0];
                        String dateString = null;
                        try {
                            dateString = DocumentUtils.getPathValue(doc, path);
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "unable to get referenced date in doc.", e);
                        }
                        if (dateString != null) {
                            Date pathDate = DateUtils.parseDate(dateString);
                            if (pathDate != null) {
                                gc = new GregorianCalendar();
                                gc.setTime(pathDate);

                                int field = Integer.parseInt(oValArray[1]);
                                int duration = Integer.parseInt(oValArray[2]);

                                gc.add(field, duration);
                                value = gc.getTime();
                            }
                        }

                    } else {
                        logger.log(Level.SEVERE, "Invalid operand format from path date.");
                    }
                }

                break;
            case 5: // MasterOperandType.FROM_LOOKUP
                break;
            case 6: // MasterOperandType.FROM_IN
                value = operandValue;
                break;
        }
//String msg = "MasterValidator-operandValue="+operandValue+", value="+value+", rule="+rule.getId(); 
//logger.info(msg);

        return value;
    }

    private static Boolean isValidDateString(String dateString) {

        Boolean valid = false;
        if (DateUtils.parseDate(dateString) != null) {
            valid = true;
        }
        return valid;
    }

    private static Boolean isValidInteger(String number) {

        Boolean valid = true;
        try {
            Integer i = Integer.parseInt(number);
        } catch (NumberFormatException e) {
            valid = false;
        }
        return valid;
    }

    private static Boolean isEqual(MasterDataType dataType, String docValue, Object operandValue) {

        Boolean equal = Boolean.TRUE;

        //check document value for valid dataType format
        switch (dataType.getId()) {
            case 2: // MasterDataType.DATE

                Date docDate = DateUtils.parseDate(docValue);
                if (operandValue != null) {
                    Date operandDate = (Date) operandValue;
                    if (docDate == null || !docDate.equals(operandDate)) {
                        equal = Boolean.FALSE;
                    }
                } else {
                    equal = Boolean.FALSE;
                }
//String msg = "MasterValidator-isEqual docDate="+docDate+" isEqual operandDate="+operandDate+" isEqual="+equal; 
//logger.info(msg);                

                break;

            case 3: // MasterDataType.INTEGER

                try {
                    Integer docInteger = Integer.parseInt(docValue);
                    Integer validInteger = (Integer) operandValue;
                    if (!docInteger.equals(validInteger)) {
                        equal = Boolean.FALSE;
                    }

                } catch (Exception e) {
                    // this will only happen if the value in the validValue from the database is not an integer
                    logger.log(Level.SEVERE, "Operand value is not valid for Integer.", e);
                }

                break;
            case 1: // MasterDataType.STRING
                String operandString = (String) operandValue;
                if (!docValue.equals(operandString)) {
                    equal = Boolean.FALSE;
                }

                break;
        }


        return equal;

    }

    private static Boolean isGreaterThan(MasterDataType dataType, String docValue, Object operandValue) {

        Boolean gt = Boolean.TRUE;

        switch (dataType.getId()) {
            case 2: // MasterDataType.DATE

                Date docDate = DateUtils.parseDate(docValue);
                if (operandValue != null) {
                    Date validDate = (Date) operandValue;
                    if (docDate == null || !docDate.after(validDate)) {
                        gt = Boolean.FALSE;
                    }
                } else {
                    gt = Boolean.FALSE;
                }
//String msg = "MasterValidator-isGreaterThan docDate="+docDate+" isGreaterThan operandValue="+validDate+" isGreaterThan="+gt; 
//logger.info(msg);                

                break;

            case 3: // MasterDataType.INTEGER

                try {
                    Integer docInteger = Integer.parseInt(docValue);
                    Integer validInteger = (Integer) operandValue;
                    if (docInteger.compareTo(validInteger) < 0) {
                        gt = Boolean.FALSE;
                    }

                } catch (ClassCastException e) {
                    // this will only happen if the value in the validValue from the database is not an integer
                    logger.log(Level.SEVERE, "Operand value is not valid for Integer.", e);
                }

                break;
        }


        return gt;

    }

    private static Boolean isLessThan(MasterDataType dataType, String docValue, Object operandValue) {

        Boolean gt = Boolean.TRUE;

        switch (dataType.getId()) {
            case 2: // MasterDataType.DATE

                Date docDate = DateUtils.parseDate(docValue);
                if (operandValue != null) {
                    Date validDate = (Date) operandValue;
                    if (docDate == null || !docDate.before(validDate)) {
                        gt = Boolean.FALSE;
                    }
                } else {
                    gt = Boolean.FALSE;
                }
//String msg = "MasterValidator-isLessThan docDate="+docDate+" isLessThan operandValue="+validDate+" isLessThan="+gt; 
//logger.info(msg);                
                break;

            case 3: // MasterDataType.INTEGER

                try {
                    Integer docInteger = Integer.parseInt(docValue);
                    Integer validInteger = (Integer) operandValue;
                    if (docInteger.compareTo(validInteger) > 0) {
                        gt = Boolean.FALSE;
                    }

                } catch (ClassCastException e) {
                    // this will only happen if the value in the validValue from the database is not an integer
                    logger.log(Level.SEVERE, "Operand value is not valid for Integer.", e);
                }

                break;
        }

        return gt;

    }

    private static Boolean isIn(String docValue, Object operandValue) {

        Boolean in = Boolean.FALSE;
        if (operandValue != null) {
            String oStr = (String) operandValue;
            String[] oArray = oStr.split(",");
            for (String o : oArray) {
                if (o.equals(docValue)) {
                    in = true;
                }
            }
        }
        return in;

    }
}
