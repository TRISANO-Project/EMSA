package gov.utah.health.service.master;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.Health;
import gov.utah.health.model.master.MasterCard;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterMirthPath;
import gov.utah.health.model.master.MasterPaths;
import gov.utah.health.model.master.MirthPath;
import static gov.utah.health.service.master.MasterTransform.logger;
import gov.utah.health.util.DocumentUtils;
import gov.utah.health.util.HealthMarshal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author UDOH
 */
public class MasterTransform {

    public static final Logger logger = Logger.getLogger("MasterTransform");
    public static final ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    public static List<Document> mirthXmlToMasterDocNew(Connection con, Health health, String mirthXml, Integer labId, String version, List<MasterException> exList) {

        List<Document> masterDocs = new ArrayList<Document>();
        List<MasterMirthPath> mmpList = ElrDao.getMasterMirthPaths(con, labId, version);

        if (mmpList.isEmpty()) {
            exList.add(new MasterException(MasterException.MIRTH_TO_MASTER_MAPPINGS_NOT_FOUND, "labId=" + Integer.toString(labId)));
        } else {

            try {

                // read mirth xml into Document
                Document mirthDoc = DocumentUtils.getDocument(mirthXml);
                HealthMarshal marshal = (HealthMarshal) appContext.getBean("healthMarshal");
                String masterXml = marshal.getHealthXml(health);
                Document masterDoc = DocumentUtils.getDocument(masterXml);

                for (MasterMirthPath mmp : mmpList) {

                    for (MirthPath mp : mmp.getMirthPaths()) {

                        mirthPathToMasterDoc(mp, mirthDoc, masterDoc);

                    }
                }
// TODO Jay
String strXmlTODO = DocumentUtils.getXml(masterDoc);
                divideMaster(masterDoc, masterDocs);
strXmlTODO = DocumentUtils.getXml(masterDoc);
                masterDoc = masterDocs.get(0);
                // copy ESP unique data from NTE segments into first master doc
                NodeList espList = mirthDoc.getElementsByTagName("NTE");
                ArrayList<String> keys = new ArrayList<String>();
                ArrayList<String> values = new ArrayList<String>();
                ArrayList<String> startDates = new ArrayList<String>();
                ArrayList<String> endDates = new ArrayList<String>();
                int iMax = espList.getLength();
                for (int i = 0; i < iMax; i++ ) {
                    NodeList nteList = espList.item(i).getChildNodes();
                    int jMax = nteList.getLength();
                    String key = "";
                    String value = "";
                    String start = "";
                    String end = "";
                    for (int j = 0; j < jMax; j++ ) {
                        Node  nteContent = nteList.item(j);
                        if("NTE.2".equals(nteContent.getNodeName())) {
                            key = nteContent.getTextContent().trim().toLowerCase();
                        } else if("NTE.3".equals(nteContent.getNodeName())) {
                            value = nteContent.getTextContent().trim();
                        } else if("NTE.4".equals(nteContent.getNodeName())) {
                            start = nteContent.getTextContent().trim();                            
                        } else if("NTE.5".equals(nteContent.getNodeName())) {
                            end = nteContent.getTextContent().trim();                            
                        }
                        
                    }
                    if (value != null && value.length() > 0 && key != null) {
                        if (key.equals("esp_pregnancy_status")
                            || key.equals("esp_icd10_diagnosis")
                            || key.equals("esp_treatment")
                            || key.equals("esp_physician_note")) {
                            keys.add(key.substring(4));
                            values.add(value);
                            startDates.add(start);
                            endDates.add(end);
                        }
                    }
                }
                if(keys.size() > 0) {
                    Element elRoot = masterDoc.getDocumentElement();
                    Element elEsp = masterDoc.createElement("esp");
                    iMax = keys.size();
                    for(int i = 0; i < iMax; i++) {
                        String key = keys.get(i);
                        if(!key.equals("treatment")) {
                            Element el = masterDoc.createElement(key);
                            el.setTextContent(values.get(i));
                            elEsp.appendChild(el);
                        } else {
                            Element el = masterDoc.createElement("treatments");
                            Element elInner = masterDoc.createElement("name");
                            elInner.setTextContent(values.get(i));
                            el.appendChild(elInner);
                            elInner = masterDoc.createElement("given");
                            elInner.setTextContent("119"); // id for Yes
                            el.appendChild(elInner);
                            String start = startDates.get(i);
                            String end = endDates.get(i);
                            elInner = masterDoc.createElement("date_of_treatment");
                            elInner.setTextContent(start);
                            el.appendChild(elInner);
                            elInner = masterDoc.createElement("treatment_stopped");
                            elInner.setTextContent(end);
                            el.appendChild(elInner);
                            elEsp.appendChild(el);
                        }
                    }
                    elRoot.appendChild(elEsp);
                }
strXmlTODO = DocumentUtils.getXml(masterDoc);
strXmlTODO += " ";
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to transform mirth to master.", e);
                exList.add(new MasterException(MasterException.UNABLE_TO_TRANSFORM_MIRTHXML_TO_MASTERDOC, e.getMessage()));
            }
        }
        return masterDocs;
    }

    public static List<Document> mirthXmlToMasterDoc(Connection con, Health health, String mirthXml, Integer labId, String version, List<MasterException> exList) {

        List<Document> masterDocs = new ArrayList<Document>();
        List<MasterMirthPath> mmpList = ElrDao.getMasterMirthPaths(con, labId, version);

        if (mmpList.isEmpty()) {
            exList.add(new MasterException(MasterException.MIRTH_TO_MASTER_MAPPINGS_NOT_FOUND, "labId=" + Integer.toString(labId)));
        } else {

            try {

                // read mirth xml into Document
                Document mirthDoc = DocumentUtils.getDocument(mirthXml);
                HealthMarshal marshal = (HealthMarshal) appContext.getBean("healthMarshal");
                String masterXml = marshal.getHealthXml(health);
                Document masterDoc = DocumentUtils.getDocument(masterXml);
                List<MasterMirthPath> delMirthPaths = new ArrayList<MasterMirthPath>();
                String masterRepeatEle = null;


                //Processes multiple in elements to many result... e.g "ORU_R01.OBXNTE" to labs
                for (MasterMirthPath mmp : mmpList) {
                    masterRepeatEle = mmp.getRepeatingElement();
                    for (MirthPath mp : mmp.getMirthPaths()) {
                        String mirthXpath = mp.getXpath();
                        String mirthRepeatEle = mp.getRepeatingElement();
                        if (mirthRepeatEle != null && masterRepeatEle != null) {
                            Integer repeatingElementCount = DocumentUtils.getElementCount(mirthDoc, mirthRepeatEle);
                            for (int i = 1; i <= repeatingElementCount; i++) {
                                String masterXpath = mp.getMasterPath();
                                String mirthValue =
                                        DocumentUtils.getPathValue(mirthDoc, mirthXpath.replace(mirthRepeatEle, mirthRepeatEle + "[" + i + "]"));
                                if (mirthValue != null) {
                                    Boolean append = false;
                                    if (mp.getSequence() != null && mp.getSequence() > 1) {
                                        append = true;
                                        if (mp.getGlueString() != null) {
                                            mirthValue = mp.getGlueString() + mirthValue;
                                        }
                                    }
                                    DocumentUtils.addPathValue(masterDoc, masterXpath, mirthValue, masterRepeatEle, i, append);
                                } else {
                                    delMirthPaths.add(mmp);
                                }
                            }
                        }
                    }
                }
                //Processes one in element to many result... e.g one "/ORU_R01/MSH/MSH.3/HD" to many health/labs/lab
                for (MasterMirthPath mmp : mmpList) {
                    masterRepeatEle = mmp.getRepeatingElement();
                    for (MirthPath mp : mmp.getMirthPaths()) {
                        String mirthXpath = mp.getXpath();
                        String mirthRepeatEle = mp.getRepeatingElement();
                        if (mirthRepeatEle == null && masterRepeatEle != null) {
                            Integer masterNodeCount = getMasterNodeCount(masterRepeatEle, mirthDoc, mmpList);
                            for (int i = 1; i <= masterNodeCount; i++) {
                                String masterXpath = mp.getMasterPath();
                                String mirthValue =
                                        DocumentUtils.getPathValue(mirthDoc, mirthXpath);
                                if (mirthValue != null) {
                                    Boolean append = false;
                                    if (mp.getSequence() != null && mp.getSequence() > 1) {
                                        append = true;
                                        if (mp.getGlueString() != null) {
                                            mirthValue = mp.getGlueString() + mirthValue;
                                        }
                                    }
                                    DocumentUtils.addPathValue(masterDoc, masterXpath, mirthValue, masterRepeatEle, i, append);
                                } else {
                                    delMirthPaths.add(mmp);
                                }
                            }
                        }
                    }
                }

                for (MasterMirthPath mmp : mmpList) {
                    masterRepeatEle = mmp.getRepeatingElement();
                    for (MirthPath mp : mmp.getMirthPaths()) {
                        String mirthXpath = mp.getXpath();
                        String mirthRepeatEle = mp.getRepeatingElement();
                        if (mirthRepeatEle == null && masterRepeatEle == null) {
                            String mirthValue = DocumentUtils.getPathValue(mirthDoc, mirthXpath);
                            if (mirthValue != null && mirthValue.trim().length() > 0) {
                                Boolean append = false;
                                if (mp.getSequence() != null && mp.getSequence() > 1) {
                                    append = true;
                                    if (mp.getGlueString() != null) {
                                        mirthValue = mp.getGlueString() + mirthValue;
                                    }
                                }
                                DocumentUtils.addPathValue(masterDoc, mmp.getMasterPath(), mirthValue, "labs", 1, append);
                            } else {
                                delMirthPaths.add(mmp);
                            }
                        }
                    }
                }
                for (MasterMirthPath dp : delMirthPaths) {
                    mmpList.remove(dp);
                }

                // copy masterDoc into as many labs
                String labPath = "/health/labs";
                Integer labsCount = DocumentUtils.getElementCount(masterDoc, "labs");


                if (labsCount > 1) {
                    for (int i = 1; i <= labsCount; i++) {

                        Document tempCopy = DocumentUtils.getCopy(masterDoc);
                        List<String> delPaths = new ArrayList<String>();
                        for (int c = 1; c <= labsCount; c++) {
                            if (c != i) {
                                delPaths.add(labPath.replace("labs", "labs[" + c + "]"));
                            }
                        }

                        DocumentUtils.removePaths(tempCopy, delPaths);
                        masterDocs.add(tempCopy);
                    }

                } else {
                    masterDocs.add(masterDoc);
                }


            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to transform mirth to master.", e);
                exList.add(new MasterException(MasterException.UNABLE_TO_TRANSFORM_MIRTHXML_TO_MASTERDOC, e.getMessage()));
            }
        }


        return masterDocs;

    }

    private static Integer getMasterNodeCount(String repeatMasterNode, Document mirthDoc, List<MasterMirthPath> mmpList) {
        Integer count = 0;

        //for each mmp find one with the same repeating master node and use its repeating mirth node to count the number of 
        // repeating mirth nodes in the mirthDoc
        // the count is the same in the master.
        String masterRepeatEle = null;
        String mirthRepeatEle = null;

        for (MasterMirthPath mmp : mmpList) {
            masterRepeatEle = mmp.getRepeatingElement();
            if (masterRepeatEle != null) {
                for (MirthPath mp : mmp.getMirthPaths()) {
                    mirthRepeatEle = mp.getRepeatingElement();
                    if (mirthRepeatEle != null) {
                        break;
                    }
                }
            }
            if (masterRepeatEle != null && mirthRepeatEle != null) {
                break;
            }
        }
        count = DocumentUtils.getElementCount(mirthDoc, mirthRepeatEle);
        return count;
    }

    public static void mirthPathToMasterDoc(MirthPath mpProc, Document mirthDoc, Document masterDoc) {


        if (mpProc.getMirthCard() == null && mpProc.getXpath() != null) {

            try {
                String mirthData = DocumentUtils.getPathValue(mirthDoc, mpProc.getXpath());
String strTODOMirthPath = mpProc.getXpath();
//System.out.print(mpProc.getXpath() + "\n=" + mirthData + "\n");
                String masterPath = mpProc.getMasterPath();
                String newMasterPath = MasterCard.translateMirthPath(mpProc.getXpath(), masterPath);
                DocumentUtils.addCardPathValue(masterDoc, mirthData, newMasterPath, mpProc.append(), mpProc.getGlueString());
            
            } catch (Exception e) {
                  logger.log(Level.SEVERE, "Unable to process master path id="+mpProc.getMasterPathId(), e);
            }

        } else {

            List<String> mirthCards = mpProc.getMirthCard().getCard();
            String mirthPath = mpProc.getXpath();

            try {
                String pathToData;
                String repeater1name = mirthCards.get(0);
                String repeater1path = mirthPath.substring(0, mirthPath.indexOf(repeater1name));
                Integer repeater1count = DocumentUtils.getElementCount(mirthDoc, repeater1name);
                for (int i = 1; i <= repeater1count; i++) {

                    String repeater1nameNumber = repeater1name + "[" + Integer.toString(i) + "]";
                    pathToData = mirthPath.replace("/" + repeater1name + "/", "/" + repeater1nameNumber + "/");

                    if (mirthCards.size() == 1) {
                        String mirthData = DocumentUtils.getPathValue(mirthDoc, pathToData);
//System.out.print(pathToData + "\n=" + mirthData + "\n");
                        String masterPath = mpProc.getMasterPath();
                        String newMasterPath = MasterCard.translateMirthPath(pathToData, masterPath);

                        DocumentUtils.addCardPathValue(masterDoc, mirthData, newMasterPath, mpProc.append(), mpProc.getGlueString());

                    } else {
                        Node repeat1Node = DocumentUtils.getPathNode(mirthDoc, repeater1path + repeater1nameNumber);
                        String repeater2name = mirthCards.get(1);
                        Integer repeater2count = DocumentUtils.getElementCountInNode(repeat1Node, repeater2name);

                        for (int i2 = 1; i2 <= repeater2count; i2++) {

                            String repeater2nameNumber = repeater2name + "[" + Integer.toString(i2) + "]";
                            String r2PathToData = pathToData.replace("/" + repeater2name + "/", "/" + repeater2nameNumber + "/");
                            if (mirthCards.size() == 2) {

                                String mirthData = DocumentUtils.getPathValue(mirthDoc, r2PathToData);
//System.out.print(r2PathToData + "\n=" + mirthData + "\n");
                                String masterPath = mpProc.getMasterPath();
                                String newMasterPath = MasterCard.translateMirthPath(r2PathToData, masterPath);

                                DocumentUtils.addCardPathValue(masterDoc, mirthData, newMasterPath, mpProc.append(), mpProc.getGlueString());

                            } else {

                                String repeater2path = r2PathToData.substring(0, r2PathToData.indexOf(repeater2nameNumber) + repeater2nameNumber.length());
                                String repeater3name = mirthCards.get(2);
                                Node repeat2Node = DocumentUtils.getPathNode(mirthDoc, repeater2path);
                                Integer repeater3count = DocumentUtils.getElementCountInNode(repeat2Node, repeater3name);

                                for (int i3 = 1; i3 <= repeater3count; i3++) {

                                    String repeater3nameNumber = repeater3name + "[" + Integer.toString(i3) + "]";
                                    String r3PathToData = r2PathToData.replace("/" + repeater3name + "/", "/" + repeater3nameNumber + "/");
                                    String mirthData = DocumentUtils.getPathValue(mirthDoc, r3PathToData);
//System.out.print(r3PathToData + "\n=" + mirthData + "\n");

                                    String masterPath = mpProc.getMasterPath();
                                    String newMasterPath = MasterCard.translateMirthPath(r3PathToData, masterPath);

                                    DocumentUtils.addCardPathValue(masterDoc, mirthData, newMasterPath, mpProc.append(), mpProc.getGlueString());

                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                  logger.log(Level.SEVERE, "Unable to process master path id="+mpProc.getMasterPathId(), e);
            }
        }
    }

    public static void divideMaster(Document masterDoc, List<Document> masterDocs) throws Exception {

        String labPath = "/health/labs";
        Integer labsCount = DocumentUtils.getElementCount(masterDoc, "labs");
        Integer segmentIdxCount = 1;

        if (labsCount > 0) {
            for (int i = 1; i <= labsCount; i++) {

                Document labCopy = DocumentUtils.getCopy(masterDoc);
                List<String> delLabPaths = new ArrayList<String>();
                for (int c = 1; c <= labsCount; c++) {
                    if (c != i) {
                        delLabPaths.add(labPath.replace("labs", "labs[" + c + "]"));
                    }
                }

                DocumentUtils.removePaths(labCopy, delLabPaths);

                String labResultPath = "/health/labs/lab_result";
                Integer labResultCount = DocumentUtils.getElementCount(labCopy, "lab_result");
                if (labResultCount > 0) {
                    for (int i2 = 1; i2 <= labResultCount; i2++) {

                        Document labResultCopy = DocumentUtils.getCopy(labCopy);
                        List<String> delLabResultPaths = new ArrayList<String>();
                        for (int c2 = 1; c2 <= labResultCount; c2++) {
                            if (c2 != i2) {
                                delLabResultPaths.add(labResultPath.replace("lab_result", "lab_result[" + c2 + "]"));
                            }

                        }

                        DocumentUtils.removePaths(labResultCopy, delLabResultPaths);
                        DocumentUtils.addCardPathValue(labResultCopy, Integer.toString(segmentIdxCount++), MasterPaths.masterSegmentIdxPath, false, null);
                        DocumentUtils.makeChildrenSiblings(labResultCopy, labResultPath);
                        masterDocs.add(labResultCopy);

                    }

                }
            }
        }
    }
    /*
     * Add additional clinician information for labs from ESP-TRISANO-INTERFACE
     */
    public static void addEspClinicians(Document trisanoDoc, String mirthXml) {
        if (mirthXml != null) {
            try {
                Document mirthDoc = DocumentUtils.getDocument(mirthXml);
                XPathExpression xpath = XPathFactory.newInstance().newXPath().compile("/ORU_R01/MSH/MSH.3/HD.1");
                String lab = xpath.evaluate(mirthDoc);
                if(lab.toUpperCase().equals("ESP-TRISANO-INTERFACE")) {
                    xpath = XPathFactory.newInstance().newXPath().compile("/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.PIDPD1NK1NTEPV1PV2/NK1/NK1.2");
                    NodeList nodes = (NodeList) xpath.evaluate(mirthDoc, XPathConstants.NODESET);
                    xpath = XPathFactory.newInstance().newXPath().compile("/trisano_health");
                    NodeList nodesT = (NodeList)xpath.evaluate(trisanoDoc, XPathConstants.NODESET);
                    Node nodeTrisanoHealth = nodesT.item(0);
                    
                    // we are skipping the first clinician because that one came across 
                    // in the master xml
                    for (int i=1; i < nodes.getLength(); i++) {
                        Node node = nodes.item(i);
                        xpath = XPathFactory.newInstance().newXPath().compile("XPN.1/FN.1");
                        String lastName = xpath.evaluate(node).trim();
                        xpath = XPathFactory.newInstance().newXPath().compile("XPN.2");
                        String firstName = xpath.evaluate(node).trim();
                        Element elClinicianAttributes = trisanoDoc.createElement("clinician_attributes");
                        Element elClinician = trisanoDoc.createElement("clinician");
                        Element elFirst = trisanoDoc.createElement("first_name");
                        elFirst.setTextContent(firstName);
                        elClinician.appendChild(elFirst);
                        Element elLast = trisanoDoc.createElement("last_name");
                        elLast.setTextContent(lastName);
                        elClinician.appendChild(elLast);
                        elClinicianAttributes.appendChild(elClinician);
                        nodeTrisanoHealth.appendChild(elClinicianAttributes);
                    }
                }
            } catch (Exception e) {
                logger.log(Level.WARNING, "Problem with ESP clinician NK1 field: ", e);
            }
        }
    }
}
