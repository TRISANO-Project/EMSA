package gov.utah.health.service.master;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterList;
import gov.utah.health.model.master.MasterPaths;
import gov.utah.health.util.DocumentUtils;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;

/**
 * Controls access to the MasterList
 *
 * @author UDOH
 */
public class MasterListProcessor {

    public static final Logger logger = Logger.getLogger("MasterListProcessor");

    public static MasterList getMasterList(Connection con, Document doc, Integer labId, String localLoincCode) {

        // use /health/labs/local_loinc_code to query vocab_child_loinc.child_loinc use 
        // child_loinc.master_loinc to look up table vacab_master_loinc.list        
        MasterList ml = null;

        String localResultValue = null;
        try {
            String lrv1 = DocumentUtils.getPathValue(doc, MasterPaths.localResultValue1);
            String lrv2 = DocumentUtils.getPathValue(doc, MasterPaths.localResultValue2);
            localResultValue = ElrDao.getLocalResultValue(con, localLoincCode, lrv1, lrv2, labId);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Unable to get localResultValue", e);
        }

        if (localLoincCode != null
                && localLoincCode.trim().length() > 0 ) {
            ml = ElrDao.getMasterList(con, localLoincCode, localResultValue, labId);
        }

        return ml;
    }
}
