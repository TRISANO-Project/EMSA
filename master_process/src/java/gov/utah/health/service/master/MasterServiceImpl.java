package gov.utah.health.service.master;

import gov.utah.health.client.elr.ElrWebClientImpl;
import gov.utah.health.data.elr.ElrConnect;
import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.model.master.Health;
import gov.utah.health.model.master.MasterAction;
import gov.utah.health.model.master.MasterAudit;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterList;
import gov.utah.health.model.master.MasterPath;
import gov.utah.health.model.master.MasterPaths;
import gov.utah.health.model.trisano.TrisanoHealth;
import static gov.utah.health.service.master.MasterTrisanoProcessor.appContext;
import gov.utah.health.util.DateUtils;
import gov.utah.health.util.DocumentUtils;
import gov.utah.health.util.HealthObjectFactory;
import gov.utah.health.util.TrisanoObjectFactory;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

/**
 * Service level implementation class. Calls to this service are configured in
 * the applicationContext.xml file.
 *
 * @author UDOH
 */
@Service
public class MasterServiceImpl implements MasterService {

    public static final Logger logger = Logger.getLogger("MasterServiceImpl");
    //public static final String mirthSegmentIdxPath = "/ORU_R01/ORU_R01.PIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI/ORU_R01.ORCOBRNTEOBXNTECTI/ORU_R01.OBXNTE/OBX/OBX.1";

    /**
     * Processes incoming messages from mirth.
     */
    @Override
    public void saveMasterFromMirth(String originalMessageId, String mirthXml, String labName, String version) {

        List<MasterException> exList = new ArrayList<MasterException>();
        Integer userId = 9999;
        Integer lId = 0;
        Integer origId = 0;
        Integer messageId;
        Date dateReceived;

        StringBuilder logMsg = new StringBuilder("\nMESSAGE RECEIVED >>> ");
        logMsg.append("originalMessageId=");
        logMsg.append(originalMessageId);
        logMsg.append(",labName=");
        logMsg.append(labName);
        logMsg.append(",version=");
        logMsg.append(version);
        logger.log(Level.INFO, logMsg.toString());

        if (originalMessageId == null || originalMessageId.trim().length() == 0) {
            exList.add(new MasterException(MasterException.MISSING_MIRTH_PARAM_ORIGINAL_MESSAGE_ID, labName));
        } else {
            try {
                origId = Integer.parseInt(originalMessageId);
            } catch (NumberFormatException e) {
                logger.log(Level.SEVERE, "Unable to convert originalMessageId to Integer.", e);
                exList.add(new MasterException(MasterException.ORIGINAL_MESSAGE_ID_NAN, e.getMessage()));
            }
        }
        if (mirthXml == null || mirthXml.trim().length() == 0) {
            exList.add(new MasterException(MasterException.MISSING_MIRTH_PARAM_HL7XML, originalMessageId));
        }
        if (labName == null || labName.trim().length() == 0) {
            exList.add(new MasterException(MasterException.MISSING_MIRTH_PARAM_LAB_NAME, originalMessageId));
        }
        if (version == null || version.trim().length() == 0) {
            exList.add(new MasterException(MasterException.MISSING_MIRTH_PARAM_VERSION, originalMessageId));
        }

        Connection con = ElrConnect.getConnection();
        if (con != null) {
            try {

                if (exList.isEmpty()) {

                    try {
                        lId = ElrDao.getLabId(con, labName);
                    } catch (SQLException e) {
                        exList.add(new MasterException(MasterException.LAB_ID_NOT_FOUND_FOR_LAB_NAME, labName+" error="+e.getMessage()));
                    }

                    //Health health = HealthObjectFactory.getInstance();
                    Health health = new Health();
                    //TODO just use a new Health() as the method for adding values to the xml will create missing nodes

                    try {
                        dateReceived = ElrDao.getReportDate(con, origId);
                        if (dateReceived != null) {
                            health.setReportDate(DateUtils.getMasterDateFormat(dateReceived));
                        }
                    } catch (SQLException e) {
                        exList.add(new MasterException(MasterException.UNABLE_TO_OBTAIN_DATE_MESSAGE_RECEIVED, "originalMessageId=" + originalMessageId + " " + e.getMessage()));
                    }
String masterTODO = null;
                    if (lId > 0) {
                        String masterXml = null;
                        List<Document> masterDocList = MasterTransform.mirthXmlToMasterDocNew(con, health, mirthXml, lId, version, exList);
                        //List<Document> masterDocList = MasterTransform.mirthXmlToMasterDoc(con, health, mirthXml, lId, version, exList);
                        for (Document masterDoc : masterDocList) {
                            if (masterDoc == null) {
                                exList.add(new MasterException(MasterException.UNABLE_TO_TRANSFORM_MIRTHXML_TO_MASTERDOC, "labId=" + lId));
                            } else {
                                try {
                                    health.getPerson().setFirstName(DocumentUtils.getPathValue(masterDoc, MasterPaths.firstNamePath));
                                    health.getPerson().setLastName(DocumentUtils.getPathValue(masterDoc, MasterPaths.lastNamePath));
                                    String masterSegValue = DocumentUtils.getPathValue(masterDoc, MasterPaths.masterSegmentIdxPath);
                                    health.setLabSegmentIndex(masterSegValue);
                                    masterXml = DocumentUtils.getXml(masterDoc);
                                } catch (DOMException e) {
                                    logger.log(Level.SEVERE, "Error DocumentUtils.getXml(masterDoc)", e.getMessage());
                                } catch (XPathExpressionException e) {
                                    logger.log(Level.SEVERE, "Error DocumentUtils.getXml(masterDoc)", e.getMessage());
                                } catch (Exception e) {
                                    logger.log(Level.SEVERE, "Unable to create masterXml from document", e);
                                }
                            }

                            if (masterXml != null && masterXml.trim().length() > 0) {
                                messageId = ElrDao.addMessage(con, health, masterXml, origId, lId, 0, exList);
                                if (messageId > 0) {
masterTODO = DocumentUtils.getXml(masterDoc);                  
                                    this.saveMaster(con, masterDoc, messageId, lId, userId, mirthXml);
masterTODO = DocumentUtils.getXml(masterDoc);                  
                                } else {
                                    exList.add(new MasterException(MasterException.UNABLE_TO_ADD_SYSTEM_MESSAGE, "labId=" + lId));
                                }
                            } else {
                                exList.add(new MasterException(MasterException.UNABLE_TO_CONVERT_MASTERDOC_TO_MASTERXML, "labId=" + lId));
                            }

                        }
                        for (MasterException me : exList) {
                            logger.log(Level.SEVERE, "Processing error exception id=" + me.getId() + " " + me.getMessage());
                        }
                    } else {
                        logger.log(Level.SEVERE, "Unable to save master. valid lab id missing");
                    }
                } else {
                    for (MasterException ex : exList) {
                        logger.log(Level.SEVERE, "Missing params in post from Mirth.", ex.getId());
                    }
                    messageId = ElrDao.addMessage(con, null, null, origId, lId, MasterList.EXCEPTION.getId(), exList);
                    Audit.addAudit(con, new MasterAudit(userId, MasterAction.INVALID_PARAMS_FROM_MIRTH.getId(), messageId, lId, MasterList.EXCEPTION.getId(), exList));
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to process message", e);
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Cannot close connection", e);
                }

            }
        }
    }

    /**
     * Processes incoming messages to this web service formatted as a
     * HealthMessage.
     */
    @Override
    public HealthMessage saveMaster(HealthMessage hm) {
        String decodedMasterXml = null;
        String healthXml = hm.getHealthXml();
        Integer systemMessageId = hm.getSystemMessageId();
        Integer userId = hm.getUserId();
        Integer labId = null;
        Connection con = ElrConnect.getConnection();
        if (con != null) {
            try {
                if (systemMessageId != null && systemMessageId > 0) {
                    try {
                        labId = ElrDao.getLabId(con, systemMessageId);
                    } catch (SQLException e) {
                        hm.addStatusMessage(new StatusMessage("saveMaster", Status.FAILURE_EXCEPTION.toString(), "systemMessageId=" + systemMessageId + "\n" + e.getMessage()));
                        logger.log(Level.SEVERE, "Exception in call to find lab based on systemMessageId=" + systemMessageId, e);
                    }
                    if (labId != null && labId > 0) {
                        if (healthXml != null && healthXml.trim().length() > 0) {
                            try {
                                decodedMasterXml = URLDecoder.decode(healthXml, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                hm.addStatusMessage(new StatusMessage("saveMaster", Status.FAILURE_EXCEPTION.toString(), "systemMessageId=" + systemMessageId + "\n" + e.getMessage()));
                                logger.log(Level.SEVERE, "Decoding healthXml failed.", e);
                            }
                        } else {
                            hm.addStatusMessage(new StatusMessage("saveMaster", Status.FAILURE_MISSING_DATA.toString(), "health_message.health_xml is missing, systemMessageId=" + systemMessageId));
                            logger.log(Level.SEVERE, "health_message.health_xml is required.");
                        }


                        if (decodedMasterXml != null && decodedMasterXml.trim().length() > 0) {

                            Document masterDoc = null;

                            try {
                                masterDoc = DocumentUtils.getDocument(decodedMasterXml);
                            } catch (Exception e) {
                                hm.addStatusMessage(new StatusMessage("saveMaster", Status.FAILURE_EXCEPTION.toString(), "systemMessageId=" + systemMessageId + "\n" + e.getMessage()));
                                logger.log(Level.SEVERE, healthXml, e);
                            }

                            if (masterDoc != null) {
                                this.saveMaster(con, masterDoc, systemMessageId, labId, userId, null);
                                hm.addStatusMessage(new StatusMessage("saveMaster", Status.SUCCESS.toString(), "systemMessageId=" + systemMessageId));
                            }

                        } else {
                            hm.addStatusMessage(new StatusMessage("saveMaster", Status.FAILURE_EXCEPTION.toString(), "systemMessageId=" + systemMessageId + ", decodedMasterXml is null"));

                        }

                    } else {
                        hm.addStatusMessage(new StatusMessage("saveMaster", Status.FAILURE_EXCEPTION.toString(), "systemMessageId=" + systemMessageId + ", labId is null or 0"));
                        // labId is null or 0
                    }

                } else {
                    hm.addStatusMessage(new StatusMessage("saveMaster", Status.FAILURE_MISSING_DATA.toString(), "systemMessageId is missing"));
                }


            } catch (Exception e) {
                logger.log(Level.SEVERE, "Unable to save master... "
                        +"\nsystemMessageId="+systemMessageId +
                        "\nuserId="+userId, e);
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception in close connection", e);
                }
            }
        }
        return hm;
    }

    /**
     * Internal method for processing master objects as Document objects into
     * the elr system.
     */
    private void saveMaster(Connection con, Document masterDoc, Integer messageId, Integer labId, Integer userId, String mirthXml) {
// TODO
try {
        List<MasterException> exList = new ArrayList<MasterException>();
        String localLoinc = null;
        Integer finalStatus = 0;
        try {
            localLoinc = DocumentUtils.getPathValue(masterDoc, MasterPaths.masterLocalLoincPath1);
            if (localLoinc == null || localLoinc.trim().length() == 0) {
                localLoinc = DocumentUtils.getPathValue(masterDoc, MasterPaths.masterLocalLoincPath2);
            }
        } catch (Exception e) {
            exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
        }
        if (localLoinc != null && localLoinc.trim().length() > 0) {

            MasterList masterList = MasterListProcessor.getMasterList(con, masterDoc, labId, localLoinc);

            if (masterList == null) {
                exList.add(new MasterException(MasterException.CHILD_LOINC_MISSING_ELR, localLoinc));
            } else {
                if (masterList.equals(MasterList.BLACK_LIST)) {

                    Audit.addAudit(con, new MasterAudit(userId, MasterAction.BLACK.getId(), messageId, labId, MasterList.BLACK_LIST.getId(), exList));
                    ElrDao.deleteMessage(con, messageId);
                    finalStatus = 1;// this is set to > 0 to prevent trying to set final status below
                    // TODO Later remove from original message table.
                } else if (masterList.equals(MasterList.GREY_LIST) || masterList.equals(MasterList.WHITE_LIST)) {
String masterTODO;

masterTODO = "A" + DocumentUtils.getXml(masterDoc);


                    MasterValidator.validateMaster(con, masterDoc, exList);
masterTODO = "B" + DocumentUtils.getXml(masterDoc);

                    String masterLoinc = null;
                    try {
                        masterLoinc = ElrDao.getMasterLoinc(con, localLoinc, labId);
                        if (masterLoinc != null) {
                            try {
                                DocumentUtils.addPathValue(masterDoc, MasterPaths.masterLoincPath, masterLoinc, "labs", 1, false);
                            } catch (DOMException e) {
                                exList.add(new MasterException(MasterException.UNABLE_TO_SET_MASTERLOINC_IN_MASTERDOC, "masterLoinc=" + masterLoinc));
                            } catch (XPathExpressionException e) {
                                exList.add(new MasterException(MasterException.UNABLE_TO_SET_MASTERLOINC_IN_MASTERDOC, "masterLoinc=" + masterLoinc));
                            }
                        }
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Error accessing masterLoinc", e.getMessage());
                    }

                    String clinician = null;
                    String clinLn = null;
                    String clinFn = null;
                    try {
                        clinLn = DocumentUtils.getPathValue(masterDoc, MasterPaths.clinLastNamePath);
                        clinFn = DocumentUtils.getPathValue(masterDoc, MasterPaths.clinFirstNamePath);
                    } catch (DOMException e) {
                        logger.log(Level.SEVERE, "Error DocumentUtils.setPathValue()", e.getMessage());
                    } catch (XPathExpressionException e) {
                        logger.log(Level.SEVERE, "Error DocumentUtils.setPathValue()", e.getMessage());
                    }
                    if (clinLn != null) {
                        clinician = clinLn;
                    }
                    if (clinFn != null) {
                        clinician = clinician + ", " + clinFn;
                    }

                    TrisanoHealth th = TrisanoObjectFactory.getInstance();
                    th.getAttachments().get(0).setMasterId(messageId);

masterTODO = "C" + DocumentUtils.getXml(masterDoc);

                    Document trisanoDoc =
                            MasterTrisanoProcessor.getTrisanoDocFromMasterDoc(con, th, masterDoc, localLoinc, masterLoinc, exList, labId, messageId);

                    MasterTransform.addEspClinicians(trisanoDoc, mirthXml);
                    if (trisanoDoc != null) {

                        String trisanoXml = null;
                        try {
masterTODO = "D" + DocumentUtils.getXml(masterDoc);
                            DocumentUtils.clean(trisanoDoc);
                            trisanoXml = DocumentUtils.getXml(trisanoDoc);
                        } catch (TransformerException e) {
                            exList.add(new MasterException(MasterException.UNABLE_TO_CONVERT_TRISANODOC_TO_TRISANOXML, e.getMessage()));
                        }
                        if (trisanoXml != null && trisanoXml.trim().length() > 0) {
                            ElrDao.saveTrisanoXml(con, messageId, trisanoXml);
                            Audit.addAudit(con, new MasterAudit(userId, MasterAction.CREATED_TRISANO_XML.getId(), messageId, labId, masterList.getId(), null));
                        }
                    }

                    String masterXml = null;
                    try {
                        masterXml = DocumentUtils.getXml(masterDoc);
                    } catch (TransformerException e) {
                        logger.log(Level.SEVERE, "Error getting master xml from doc", e.getMessage());
                    } catch (TransformerFactoryConfigurationError e) {
                        logger.log(Level.SEVERE, "Error getting master xml from doc", e.getMessage());
                    }

                    finalStatus = ElrDao.updateMessage(con, messageId, masterXml, exList, masterList.getId(), masterLoinc, clinician);
                    if (masterList.equals(MasterList.GREY_LIST)) {
                        Audit.addAudit(con, new MasterAudit(userId, MasterAction.GREY.getId(), messageId, labId, MasterList.GREY_LIST.getId(), exList));
                    } else if (masterList.equals(MasterList.WHITE_LIST)) {
                        // If there are no exceptions send notification to elr app that message is ready for auto processing.   
                        if (exList.isEmpty()) {
                            ElrWebClientImpl elrApp = (ElrWebClientImpl) appContext.getBean("elr");
                            elrApp.processElr(messageId);
                        } else { // TODO Jay
                            for(MasterException ex: exList) {
                                logger.log(Level.SEVERE, "\n>>>>>>>Exception in saveMaster(): " + ex.getMessage());
                            }
                        }
                        Audit.addAudit(con, new MasterAudit(userId, MasterAction.WHITE.getId(), messageId, labId, MasterList.WHITE_LIST.getId(), exList));
                    }
                }
            }
        } else {
            logger.log(Level.INFO, "Child loinc code not found.");
            exList.add(new MasterException(MasterException.CHILD_LOINC_MISSING_MESSAGE, MasterPaths.masterLocalLoincPath1));
            Audit.addAudit(con, new MasterAudit(userId, MasterAction.EXCEPTION.getId(), messageId, labId, MasterList.EXCEPTION.getId(), exList));
        }
        if (finalStatus.equals(0)) {
            // check to see if the final status in > 0 if not change final status to exception and audit exceptons
           ElrDao.updateMessageStatus(con, messageId, exList, MasterList.EXCEPTION.getId());
            Audit.addAudit(con, new MasterAudit(userId, MasterAction.EXCEPTION.getId(), messageId, labId, MasterList.EXCEPTION.getId(), exList));
        }
} catch (Exception ej) {
    String foo = ej.toString();
    String bar = foo + "2";
}
    }

    /**
     * Returns a full Health xml without values.
     */
    @Override
    public HealthMessage getMasterExample(HealthMessage healthMessage) {

        try {
            Health h = HealthObjectFactory.getInstance();
            List<Health> hList = new ArrayList<Health>();
            hList.add(h);
            healthMessage.setHealth(hList);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Unable to get master example", e);
        }

        return healthMessage;

    }

    /**
     * Returns a full TrisanoHealth xml without values.
     */
    @Override
    public HealthMessage getTrisnoHealthExample(HealthMessage healthMessage) {

        try {
            TrisanoHealth h = TrisanoObjectFactory.getInstance();
            List<TrisanoHealth> hList = new ArrayList<TrisanoHealth>();
            hList.add(h);
            healthMessage.setTrisanoHealth(hList);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Unable to get trisano example", e);
        }

        return healthMessage;

    }

    /**
     * Returns the master xml for the given masterId. This method is known to be
     * accessed by the process for creating pdf reports in trisano. Code values
     * in the master are translated to human readable values
     */
    @Override
    public String getMasterXml(Integer systemMessageId) {
        String masterXml = null;
        try {
            Connection con = ElrConnect.getConnection();
            if (con != null) {
                Document masterDoc = DocumentUtils.getDocument(ElrDao.getMasterXml(con, systemMessageId));
                try {
                    List<MasterPath> masterPaths = ElrDao.getMasterPathsWithCategory(con);
                    for (MasterPath path : masterPaths) {
                        String xpath = path.getXpath();
                        String categoryValue = DocumentUtils.getPathValue(masterDoc, xpath);
                        String translatedValue = ElrDao.getMasterConcept(con, systemMessageId, categoryValue, path.getCategoryId());
                        if (translatedValue != null && translatedValue.trim().length() > 0) {
                            DocumentUtils.setPathValue(masterDoc, xpath, translatedValue);
                        }
                        masterXml = DocumentUtils.getXml(masterDoc);
                    }

                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Unable to look up master concept", e);
                } finally {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Exception in close connection", e);
                    }
                }
            }


        } catch (Exception e) {
            logger.log(Level.SEVERE, "Unable to get master xml with masterId=" + systemMessageId, e);
        }

        return masterXml;

    }
}
