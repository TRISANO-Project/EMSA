package gov.utah.health.service.master;

import gov.utah.health.model.HealthMessage;

/**
 *  Service level interface for general operations against data sources.
 *
 * @author  UDOH
 */
public interface MasterService {

    public HealthMessage saveMaster(HealthMessage healthMessage);
    public void saveMasterFromMirth(String originalMessageId, String mirthXml, String labId, String version);
    public HealthMessage getMasterExample(HealthMessage healthMessage);
    public HealthMessage getTrisnoHealthExample(HealthMessage healthMessage);
    public String getMasterXml(Integer masterId);

}
