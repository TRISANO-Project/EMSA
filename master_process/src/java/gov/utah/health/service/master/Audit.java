package gov.utah.health.service.master;

import gov.utah.health.data.elr.ElrAuditDao;
import gov.utah.health.model.master.MasterAudit;
import java.sql.Connection;
/**
 *
 * @author  UDOH
 */
public class Audit {

    public static void addAudit(Connection con, MasterAudit audit) {

        ElrAuditDao.addAudit(con,audit);

    }
}
