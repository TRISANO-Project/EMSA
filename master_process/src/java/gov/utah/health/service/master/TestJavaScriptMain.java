package gov.utah.health.service.master;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 *
 * @author  UDOH
 */
public class TestJavaScriptMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        try {
            String operation = "((input >= 20) && (input <= 170000000))";
            String input = "2,900,000";
            Boolean result;
            if (input.indexOf("1:") >= 0) {
                input = input.split(":")[1];
                operation = operation.replace("1:", "");
            }
            if(input.indexOf(",") >= 0){
                String tmp = input.replaceAll(",", "");
                if(tmp.matches("\\d*[.]?\\d+")){
                    input = tmp;
                }
            }
            engine.put("input", input);
            engine.eval("if" + operation + "    {result = true;}else{result = false;}");
            result = (Boolean) engine.get("result");
            System.out.print(result);
        } catch (Exception e) {
            String m = e.getMessage();
            String m2 = e.getLocalizedMessage();
        }
    }
}
