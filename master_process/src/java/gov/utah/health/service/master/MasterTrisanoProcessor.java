package gov.utah.health.service.master;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.model.master.TrisanoCodeTable;
import gov.utah.health.model.trisano.Codes;
import gov.utah.health.model.trisano.CommonTestTypes;
import gov.utah.health.model.trisano.Diseases;
import gov.utah.health.model.trisano.ExternalCodes;
import gov.utah.health.model.trisano.Organisms;
import gov.utah.health.model.trisano.Treatments;
import gov.utah.health.model.trisano.TrisanoHealth;
import gov.utah.health.service.sqla.SqlaService;
import gov.utah.health.util.DocumentUtils;
import gov.utah.health.util.HealthMarshal;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class is responsible for producing a trisano document.
 *
 * @author UDOH
 */
public class MasterTrisanoProcessor {

    public static final Logger logger = Logger.getLogger("MasterTrisanoProcessor");
    public static final ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    public static Document getTrisanoDocFromMasterDoc(Connection con, TrisanoHealth th, Document masterDoc, String localLoinc, String masterLoinc, List<MasterException> exList, Integer labId, Integer systemMessageId) {

        // Trisano 'blank' object
        Document trisanoDoc = null;
        try {
            HealthMarshal marshal = (HealthMarshal) appContext.getBean("thMarshal");
            String trisanoXml = marshal.getTrisanoHealthXml(th);
            trisanoDoc = DocumentUtils.getDocument(trisanoXml);
            if (trisanoDoc == null) {
                throw new Exception("trisanoDoc is null.");
            }
        } catch (Exception e) {
            exList.add(new MasterException(MasterException.UNABLE_TO_OBTAIN_EMPTY_TRISANODOC, e.getMessage()));
        }
        if (trisanoDoc != null) {
            SqlaService sqlaService = (SqlaService) appContext.getBean("sqla");
            // The list of all masterPath-trisanoPath mappings with lookup operators
            List<MasterTrisanoPath> mtpList = ElrDao.getMasterTrisanoPaths(con);
            if (mtpList.isEmpty()) {
                exList.add(new MasterException(MasterException.MASTER_TO_TRISANO_MAPPINGS_NOT_FOUND, "No master to trisano mappings found. Check structure_path_application table?"));
            } else {

                //Calculate all derived values in the mtpList.
                // derived values are used for:
                //   direct copy into trisano path locations
                //   id lookup to sqla service and trisano model id insertion
                DerivedValueProcessor dvp = new DerivedValueProcessor();
                dvp.setSystemMessageId(systemMessageId);
                dvp.deriveValues(con, masterDoc, mtpList, localLoinc, masterLoinc, exList, labId);

                // all derivedValues for looking up tridano table ids have been set in mtpList.
                // call sqla service to find ids packaged in the TrisanoHealth obj
                TrisanoHealth thIds = sqlaService.findIds(mtpList);

                // Set all the values of the trisanoDoc
                //use the th to gain the ids needed for trisnao id values
                for (MasterTrisanoPath mtp : mtpList) {

                    String derivedValue = mtp.getDerivedValue();
                    String trisanoPath = mtp.getTrisanoPath();

                    if (mtp.needsId()) {
                        if (thIds == null) {
                            exList.add(new MasterException(MasterException.TRISANO_ID_NEEDED_NONE_FOUND, "sqla package for ids is null."));
                            break;
                        }
                        Integer codeId = getTrisanoCodeId(mtp, thIds, derivedValue);
                        if (codeId != null && codeId > 0) {
                            derivedValue = Integer.toString(codeId);
                        } else {
                            exList.add(new MasterException(MasterException.TRISANO_ID_NEEDED_NONE_FOUND, mtp.getTrisanoPath() + " value=" + derivedValue));
                        }
                    }
                    if (derivedValue != null) {
                        try {
                            DocumentUtils.setPathValue(trisanoDoc, trisanoPath, derivedValue);
                        } catch (DOMException de) {
                            exList.add(new MasterException(MasterException.ERROR_ACCESSING_TRISANOPATH_IN_TRISANODOC, de.getMessage()));
                        } catch (XPathExpressionException xe) {
                            exList.add(new MasterException(MasterException.ERROR_ACCESSING_TRISANOPATH_IN_TRISANODOC, xe.getMessage()));
                        }
                    }
                }
               copyEsp(masterDoc, trisanoDoc);
            }
        }

        return trisanoDoc;

    }

    private static Integer getTrisanoCodeId(MasterTrisanoPath mtp, TrisanoHealth th, String value) {
        Integer id = null;

        if (value != null) {
            TrisanoCodeTable tct = mtp.getCodeTable();

            if (TrisanoCodeTable.CODES.equals(tct)) {
                Codes c = th.getCodeByTheCode(mtp.getAppCategory(), value);
                if (c != null && c.getId() != null && c.getId() > 0) {
                    id = c.getId();
                }
            } else if (TrisanoCodeTable.COMMON_TEST_TYPES.equals(tct)) {
                CommonTestTypes ctt = th.getCommonTestTypeByCommonName(value);
                if (ctt != null && ctt.getId() != null && ctt.getId() > 0) {
                    id = ctt.getId();
                }
            } else if (TrisanoCodeTable.DISEASES.equals(tct)) {
                Diseases d = th.getDiseaseByDiseaseName(value);
                if (d != null && d.getId() != null && d.getId() > 0) {
                    id = d.getId();
                }
            } else if (TrisanoCodeTable.EXTERNAL_CODES.equals(tct)) {
                ExternalCodes ec = th.getExternalCodeByTheCode(mtp.getAppCategory(), value);
                if (ec != null && ec.getId() != null && ec.getId() > 0) {
                    id = ec.getId();
                }
            } else if (TrisanoCodeTable.ORGANISMS.equals(tct)) {
                Organisms org = th.getOrganismByOrganismName(value);
                if (org != null && org.getId() != null && org.getId() > 0) {
                    id = org.getId();
                }
            } else if (TrisanoCodeTable.TREATMENTS.equals(tct)) {
                Treatments treat = th.getTreatmentByTreatmentName(value);
                if (treat != null && treat.getId() != null && treat.getId() > 0) {
                    id = treat.getId();
                }
            }
        }
        return id;
    }
    
    private static void copyEsp(Document docFrom, Document docTo) {
        NodeList nodes = docFrom.getElementsByTagName("esp");
        if(nodes != null && nodes.getLength() > 0) {
            Node newNode = docTo.importNode(nodes.item(0), true);
            docTo.getDocumentElement().appendChild(newNode);
        }
    }
}
