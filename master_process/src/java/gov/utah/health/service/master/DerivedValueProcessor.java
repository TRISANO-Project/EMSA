package gov.utah.health.service.master;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterPaths;
import gov.utah.health.model.master.MasterRuleOperator;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.rules.trisano.PathRule;
import gov.utah.health.util.DocumentUtils;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;

/**
 *
 * @author UDOH
 */
public class DerivedValueProcessor {

    public static final Logger logger = Logger.getLogger("DerivedValueProcessor");
    public Connection con;
    public Integer systemMessageId;
    public Document masterDoc;
    public List<MasterTrisanoPath> mtpList;
    public String localLoinc;
    public String masterLoinc;
    public List<MasterException> exList;
    public Integer labId;
    private String testResultDerived;
    private Integer masterVocabId;
    private String localResultValue;
    private String resultValueLocation;
    private Boolean interpretResultsForLoinc;
    private String labResultComments;

    public Integer getSystemMessageId() {
        return systemMessageId;
    }

    public void setSystemMessageId(Integer systemMessageId) {
        this.systemMessageId = systemMessageId;
    }

    public String getLabResultComments() {
        return labResultComments;
    }

    public void setLabResultComments(String labResultComments) {
        this.labResultComments = labResultComments;
    }

    
    
    public Boolean getInterpretResultsForLoinc() {
        if (interpretResultsForLoinc == null) {
            this.interpretResultsForLoinc = ElrDao.getInterpretResultsForLoinc(this.con, this.localLoinc, this.labId);
        }
        return this.interpretResultsForLoinc;
    }

    public String getLocalResultValue() {
        if (this.localResultValue == null) {

            String localResultValue1 = null;
            String localResultValue2 = null;

            try {
                localResultValue1 = DocumentUtils.getPathValue(this.masterDoc, MasterPaths.localResultValue1);
                localResultValue2 = DocumentUtils.getPathValue(this.masterDoc, MasterPaths.localResultValue2);
            } catch (DOMException e) {
                this.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
            } catch (XPathExpressionException e) {
                this.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
            }
            try {
                this.localResultValue = ElrDao.getLocalResultValue(con, localLoinc, localResultValue1, localResultValue2, labId);
                if (this.localResultValue == null || this.localResultValue.trim().length() == 0) {
                    this.exList.add(new MasterException(MasterException.RESULT_VALUE_NOT_MAPPED, "localLoinc=" + localLoinc + ",labId=" + labId + ",localResultValue1=" + localResultValue1 + ",localResultValue2=" + localResultValue2));
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error accessing result value", e);
            }
        }
        return this.localResultValue;
    }

    public void setLocalResultValue(String localResultValue) {
        this.localResultValue = localResultValue;
    }

    public String getResultValueLocation() {
        if (this.resultValueLocation == null) {
            try {
                this.resultValueLocation = ElrDao.getResultValueLocation(con, localLoinc, labId);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error accessing result value location", e);
            }
        }
        return this.resultValueLocation;
    }

    public Integer getMasterVocabId() {
        return masterVocabId;
    }

    public void setMasterVocabId(Integer masterVocabId) {
        this.masterVocabId = masterVocabId;
    }

    public void setResultValueLocation(String resultValueLocation) {
        this.resultValueLocation = resultValueLocation;
    }

    public String getTestResultDerived() {
        return testResultDerived;
    }

    public void setTestResultDerived(String testResultDerived) {
        this.testResultDerived = testResultDerived;
    }

    public void deriveValues(Connection con, Document masterDoc, List<MasterTrisanoPath> mtpList, String localLoinc, String masterLoinc, List<MasterException> exList, Integer labId) {

// TODO
try {
        this.con = con;
        this.masterDoc = masterDoc;
        this.mtpList = mtpList;
        this.localLoinc = localLoinc;
        this.masterLoinc = masterLoinc;
        this.exList = exList;
        this.labId = labId;
        this.testResultDerived = null;

        List<MasterTrisanoPath> deletePaths = new ArrayList<MasterTrisanoPath>();

        for (MasterTrisanoPath mtp : mtpList) {
            String derivedValue = null;
            if (MasterRuleOperator.DIRECT_COPY.getId().equals(mtp.getLookupOperatorId())) {
                derivedValue = null;
                try {
                   derivedValue = DocumentUtils.getPathValue(masterDoc, mtp.getMasterPath());
                } catch (DOMException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                } catch (XPathExpressionException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                }
            } else if (MasterRuleOperator.CODE_LOOKUP.getId().equals(mtp.getLookupOperatorId())) {
                derivedValue = null;
                String masterValue = null;
                try {
                    masterValue = DocumentUtils.getPathValue(masterDoc, mtp.getMasterPath());
                    if (masterValue != null && masterValue.trim().length() > 0) {
                        derivedValue = ElrDao.getAppCode(con, masterValue, mtp.getTrisanoPathId(), labId, 1);
                        if (derivedValue == null || derivedValue.trim().length() == 0) {
                            exList.add(new MasterException(MasterException.NO_APP_CODED_VALUE_FOR_CHILD_CONCEPT, "child_vocab.concept=" + 
                                    masterValue + " application_path_id=" + mtp.getTrisanoPathId() + " master path: " + mtp.getMasterPath()));
                        }
                    }
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Error accessing trisano code=" + masterValue, e.getMessage());
                } catch (DOMException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                } catch (XPathExpressionException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                }
            } else if (MasterRuleOperator.COMPLEX_RULE.getId().equals(mtp.getLookupOperatorId())) {
                derivedValue = null;
                PathRule rule = mtp.getRule();
                if (rule != null) {
                    derivedValue = rule.eval(this, mtp);
                } else {
                    logger.log(Level.SEVERE, "no rule defined for interpreted value master path id=" + mtp.getMasterPathId(), mtp.getMasterPathId());
                }
            }
            if (derivedValue != null && derivedValue.trim().length() > 0) {
                mtp.setDerivedValue(derivedValue);
            } else {
                deletePaths.add(mtp);
            }
        }
        for (MasterTrisanoPath dpath : deletePaths) {
            mtpList.remove(dpath);
        }
} catch (Exception ej) {int foo = 0;}
    }
    public void deriveValuesNew(Connection con, Document masterDoc, List<MasterTrisanoPath> mtpList, String localLoinc, String masterLoinc, List<MasterException> exList, Integer labId) {

        this.con = con;
        this.masterDoc = masterDoc;
        this.mtpList = mtpList;
        this.localLoinc = localLoinc;
        this.masterLoinc = masterLoinc;
        this.exList = exList;
        this.labId = labId;
        this.testResultDerived = null;

        List<MasterTrisanoPath> deletePaths = new ArrayList<MasterTrisanoPath>();

        for (MasterTrisanoPath mtp : mtpList) {
            String derivedValue = null;
            if (MasterRuleOperator.DIRECT_COPY.getId().equals(mtp.getLookupOperatorId())) {
                derivedValue = null;
                try {
                    derivedValue = DocumentUtils.getPathValue(masterDoc, mtp.getMasterPath());
                } catch (DOMException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                } catch (XPathExpressionException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                }
            } else if (MasterRuleOperator.CODE_LOOKUP.getId().equals(mtp.getLookupOperatorId())) {
                derivedValue = null;
                String masterValue = null;
                try {
                    masterValue = DocumentUtils.getPathValue(masterDoc, mtp.getMasterPath());
                    if (masterValue != null && masterValue.trim().length() > 0) {
                        derivedValue = ElrDao.getAppCode(con, masterValue, mtp.getTrisanoPathId(), labId, 1);
                        if (derivedValue == null || derivedValue.trim().length() == 0) {
                            exList.add(new MasterException(MasterException.NO_APP_CODED_VALUE_FOR_CHILD_CONCEPT, "child_vocab.concept=" + masterValue + " application_path_id=" + mtp.getTrisanoPathId()));
                        }
                    }
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Error accessing trisano code=" + masterValue, e.getMessage());
                } catch (DOMException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                } catch (XPathExpressionException e) {
                    logger.log(Level.SEVERE, "Error accessing dom value=" + mtp.getMasterPath(), e.getMessage());
                }
            } else if (MasterRuleOperator.COMPLEX_RULE.getId().equals(mtp.getLookupOperatorId())) {
                derivedValue = null;
                PathRule rule = mtp.getRule();
                if (rule != null) {
                    derivedValue = rule.eval(this, mtp);
                } else {
                    logger.log(Level.SEVERE, "no rule defined for interpreted value master path id=" + mtp.getMasterPathId(), mtp.getMasterPathId());
                }
            }
            if (derivedValue != null && derivedValue.trim().length() > 0) {
                mtp.setDerivedValue(derivedValue);
            } else {
                deletePaths.add(mtp);
            }
        }
        for (MasterTrisanoPath dpath : deletePaths) {
            mtpList.remove(dpath);
        }
    }

}
