package gov.utah.health.server.mirth;

import gov.utah.health.service.master.MasterService;
import javax.jws.WebService;
import localhost.mirth.schemas.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author UDOH
 */
@WebService(serviceName = "MirthService", portName = "MirthSoap11", endpointInterface = "localhost.mirth.schemas.Mirth", targetNamespace = "http://localhost/mirth/schemas", wsdlLocation = "WEB-INF/wsdl/MirthWebService/localhost_9090/MirthService/service/hl7message.wsdl")
public class MirthWebService {

    ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    public localhost.mirth.schemas.MirthResponse mirth(localhost.mirth.schemas.MirthRequest mirthRequest) {

        MirthResponse mr = new MirthResponse();
        Message m = mirthRequest.getMessage();
        if (m != null) {
            String channel = m.getChannel();
            String mirthXml = m.getContent();
            String labId = m.getLabId();
            String version = m.getMessageId();
            String method = m.getMethod();
            String originalMessageId = m.getOriginalMessage();
            String schema = m.getSchemaName();

            MasterService ms = (MasterService) appContext.getBean("master");
            ms.saveMasterFromMirth(originalMessageId, mirthXml, labId, version);

        }
        Information i = new Information();
        i.setSystemMessage("PROCESSED");
        mr.setInformation(i);
        return mr;

    }

    public localhost.mirth.schemas.PassThruResponse passThru(localhost.mirth.schemas.PassThruRequest passThruRequest) {

        throw new UnsupportedOperationException("Not implemented yet.");
    }
}
