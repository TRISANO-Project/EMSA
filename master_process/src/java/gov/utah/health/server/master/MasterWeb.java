package gov.utah.health.server.master;

import gov.utah.health.model.HealthMessage;
import gov.utah.health.model.Status;
import gov.utah.health.model.StatusMessage;
import gov.utah.health.service.master.MasterService;
import gov.utah.health.util.HealthMarshal;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Web service source file for this project. Handles marshaling and unmarshaling
 * of the HealthMessage object.
 *
 * @author UDOH
 */
@WebService()
public class MasterWeb {

    private static final Log log = LogFactory.getLog(MasterWeb.class);
    ApplicationContext appContext =
            new ClassPathXmlApplicationContext("applicationContext.xml");

    @WebMethod(operationName = "getMasterXml")
    public java.lang.String getMasterXml(@WebParam(name = "arg0") int arg0, @WebParam(name = "arg1") int arg1) {

        MasterService ms = (MasterService) appContext.getBean("master");
        String encodedMaster = null;
        try {
            encodedMaster = URLEncoder.encode(ms.getMasterXml(arg0), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("Unable to encode master xml.", e);
        }
        return encodedMaster;

    }

    @WebMethod(operationName = "recieveELRMessages")
    public int recieveELRMessages(@WebParam(name = "arg0") java.lang.String arg0, @WebParam(name = "arg1") java.lang.String arg1, @WebParam(name = "arg2") java.lang.String arg2, @WebParam(name = "arg3") java.lang.String arg3) {

        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @WebMethod(operationName = "saveMaster")
    public String saveMaster(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        MasterService ms = (MasterService) appContext.getBean("master");
        ms.saveMaster(hm);
        return getHealthMessageXml(hm);
    }

    @WebMethod(operationName = "getMasterExample")
    public String getMasterExample(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        MasterService ms = (MasterService) appContext.getBean("master");
        ms.getMasterExample(hm);
        return getHealthMessageXml(hm);
    }

    @WebMethod(operationName = "getTrisanoExample")
    public String getTrisanoExample(@WebParam(name = "healthMessage") String healthMessage) {
        HealthMessage hm = getHealthMessage(healthMessage);
        MasterService ms = (MasterService) appContext.getBean("master");
        ms.getTrisnoHealthExample(hm);
        return getHealthMessageXml(hm);
    }

    private HealthMessage getHealthMessage(String hmXml) {

        HealthMarshal marshal = (HealthMarshal) appContext.getBean("msgMarshal");
        HealthMessage h;
        try {
            h = marshal.getHealthMessageObject(hmXml);
        } catch (Exception e) {
            h = new HealthMessage();
            h.addStatusMessage(new StatusMessage("getHealthMessage", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            log.error("unable to unmarshal healthMessage xml=" + hmXml, e);
        }

        return h;
    }

    private String getHealthMessageXml(HealthMessage hm) {

        HealthMarshal marshal = (HealthMarshal) appContext.getBean("msgMarshal");
        String h = null;
        try {
            h = marshal.getHealthMessageXml(hm);
        } catch (Exception e) {

            hm = new HealthMessage();
            hm.addStatusMessage(new StatusMessage("getHealthMessageXml", Status.FAILURE_EXCEPTION.toString(), e.getMessage()));
            try {
                h = marshal.getHealthMessageXml(hm);
            } catch (Exception e2) {
                log.error("Unable to marshal healthMessage", e2);
            }
            log.error("Unable to marshal healthMessage" + hm, e);

        }

        return h;
    }
}
