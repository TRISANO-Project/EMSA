package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class DiseaseIdRule extends PathRule {

    public static final Logger logger = Logger.getLogger("DiseaseIdRule");

    public DiseaseIdRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;

        try {
            derivedValue = ElrDao.getTrisanoDiseaseName(dvp.con, dvp.localLoinc, dvp.labId, dvp.getLocalResultValue());
        } catch (SQLException e) {
            dvp.exList.add(new MasterException(MasterException.UNABLE_TO_LOOKUP_DISEASE_NAME, e.getMessage()));
        }
        if (derivedValue != null && derivedValue.trim().length() > 0) {
            try {
                ElrDao.setSystemMessageColumnValue(dvp.con, dvp.systemMessageId, "disease", derivedValue);
                DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
            } catch (DOMException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            } catch (XPathExpressionException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }
        return derivedValue;

    }
}
