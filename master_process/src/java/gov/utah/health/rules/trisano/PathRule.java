package gov.utah.health.rules.trisano;

import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;

/**
 *
 * @author UDOH
 */
public class PathRule {
    /**
     *
     */

    public Integer appPathId;

    public PathRule(Integer pathId) {
        appPathId = pathId;
    } 

    public String eval(DerivedValueProcessor dvp,MasterTrisanoPath mtp) {
        return null;
    }

   public boolean isRuleForPath(Integer pathId){
       boolean isRule = false;
       if(this.appPathId != null && 
               pathId != null && 
               this.appPathId.equals(pathId)){
          return true;
       }
       
       return isRule;
   }
    
}
