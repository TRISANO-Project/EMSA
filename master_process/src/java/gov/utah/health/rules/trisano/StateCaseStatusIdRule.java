package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterRule;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class StateCaseStatusIdRule extends PathRule {

    public static final Logger logger = Logger.getLogger("StateCaseStatusIdRule");

    public StateCaseStatusIdRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        try {

            if (dvp.getTestResultDerived() == null) {
                TestResultIdRule testResultIdRule = new TestResultIdRule();
                dvp.setTestResultDerived(testResultIdRule.getTestResultDerivedValue(dvp, mtp));
            }
            Integer resultMasterVocabId = dvp.getMasterVocabId();

            derivedValue = ElrDao.getStateCaseStatus(dvp.con, dvp.localLoinc, dvp.getLocalResultValue(), dvp.labId);

            if (derivedValue == null && resultMasterVocabId != null) {

                List<MasterRule> rules = ElrDao.getMasterLoincRules(dvp.con, dvp.masterLoinc);
                if (rules.isEmpty()) {
                    dvp.exList.add(new MasterException(MasterException.NO_CASE_MANAGE_RULES_FOUND_FOR_MASTER_LOINC, dvp.masterLoinc));
                } else {
                    Boolean result = false;
                    String input = Integer.toString(resultMasterVocabId);

                    for (MasterRule rule : rules) {
                        try {
                            result = rule.evaluate(input);
                        } catch (Exception e) {
                            dvp.exList.add(new MasterException(MasterException.UNABLE_TO_EVALUATE_RULE, e.getMessage()));
                        }
                        if (result) {
                            Integer masterVocabId = rule.getMasterVocabId();
                            if (masterVocabId != null && masterVocabId > 0) {
                                derivedValue = ElrDao.getMasterVocabCodedValue(dvp.con, masterVocabId);
                            }
                            break;
                        }
                    }
                    if (!result) {
                        dvp.exList.add(new MasterException(MasterException.NO_CASE_MANAGE_RULES_TRUE, "No State Case Status Rule: " + dvp.masterLoinc + "" + dvp.getTestResultDerived()));
                    }
                }
            }
            if (derivedValue != null) {
                try {
                    //DocumentUtils.setPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue);
                    DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
                } catch (DOMException e) {
                    logger.log(Level.SEVERE, "error setting value in master doc.", e);
                } catch (XPathExpressionException e) {
                    logger.log(Level.SEVERE, "error setting value in master doc.", e);
                }
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing state case status=", e.getMessage());
        }


        return derivedValue;

    }
}
