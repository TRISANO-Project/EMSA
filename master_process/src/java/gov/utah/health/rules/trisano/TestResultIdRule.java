package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterRule;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class TestResultIdRule extends PathRule {

    public static final Logger logger = Logger.getLogger("TestResultIdRule");
    public static final String POSITIVE = "positive";
    public static final String NEGITIVE = "negative";

    public TestResultIdRule() {
        super(-1);
    }

    public TestResultIdRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = dvp.getTestResultDerived();

        if (derivedValue == null) {
            derivedValue = this.getTestResultDerivedValue(dvp, mtp);
        }

        if (derivedValue != null && derivedValue.trim().length() > 0) {
            dvp.setTestResultDerived(derivedValue);
            try {
                //DocumentUtils.setPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue);
                DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
            } catch (DOMException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            } catch (XPathExpressionException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }

        return derivedValue;

    }

    public String getTestResultDerivedValue(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {
        String value = null;
        String localResultValue = null;

        localResultValue = dvp.getLocalResultValue();
        if (localResultValue != null && localResultValue.trim().length() > 0) {
            if (dvp.getInterpretResultsForLoinc()) {

                try {
                    List<MasterRule> rules = ElrDao.getTestResultRules(dvp.con, dvp.localLoinc, dvp.labId);
                    if (rules.isEmpty()) {
                        dvp.exList.add(new MasterException(MasterException.NO_TEST_RESULT_RULES_FOUND_FOR_LOCAL_LOINC, dvp.localLoinc));
                    } else {
                        Boolean result = false;
                        Boolean isTiter = false;
                        Boolean modifiedByOperator = false;

                        // prepare the input value to be valid for javascript evaluation.            
                        String input = localResultValue;
                        if (input.indexOf("1:") > -1) {
                            isTiter = true;
                            input = input.replace("1:", "");
                        }
                        input = input.replaceAll(",", "");
                        
                        // Look for strings like "<50", "greater than 500" and strip out 
                        // the comparison text or operators, leaving just a number 
                        // (with a small delta added i.e. <50 becomes 49.99)
                        String[] tokens = { ">", "greater than", "<", "less than", "="};
                        String[] increments = { "0.01", "0.01", "-0.01", "-0.01", "0.0"};
                        if(isTiter) {
                            increments[0] = "1";
                            increments[1] = "1";
                            increments[2] = "-1";
                            increments[3] = "-1";
                            increments[4] = "0";
                        }
                        String inputLower = input.toLowerCase();
                        
                        for (int i = 0; i < 5; i++) {
                            String tok = tokens[i];
                            if (inputLower.indexOf(tok) >= 0) {
                                try {
                                    String t = inputLower.replaceAll(tok, "").trim();
                                    Float tf = Float.parseFloat(t);
                                    tf = tf + new Float(increments[i]);
                                    input = Float.toString(tf);
                                    modifiedByOperator = true;
                                    break;
                                } catch (Exception ex) {
                                    // if we got here we have a strange result string that can't be parsed
                                    // into a float after stripping out the > or <
                                    break;
                                }
                            }
                        }

                        for (MasterRule rule : rules) {
                            try {
                                result = rule.evaluate(input);
                            } catch (Exception e) {
                                dvp.exList.add(new MasterException(MasterException.UNABLE_TO_EVALUATE_RULE, e.getMessage()));
                            }
                            if (result) {
                                Integer masterVocabId = rule.getMasterVocabId();
                                dvp.setMasterVocabId(masterVocabId);
                                value = ElrDao.getAppCodedValueFromMasterVocabId(dvp.con, masterVocabId, 1);

                                //check comments
                                if (rule.getResultsToComments() != null && rule.getResultsToComments().trim().length() > 0) {
                                    dvp.setLabResultComments(rule.getResultsToComments());
                                }

                                break;
                            }
                        }

                        if (value != null && !modifiedByOperator 
                                && ((result && TestResultIdRule.POSITIVE.equals(value.toLowerCase()) && localResultValue.contains("<"))
                                || (result && TestResultIdRule.NEGITIVE.equals(value.toLowerCase()) && localResultValue.contains(">")))) {
                            dvp.exList.add(new MasterException(MasterException.UNEXPECTED_VALUE_LOCAL_RESULT_VALUE, localResultValue));
                        }

                        if (!result) {
                            dvp.exList.add(new MasterException(MasterException.NO_LOINC_RESULT_RULES_EVALUATED_TRUE, localResultValue));
                        }
                    }
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Error accessing test result rules labId=" + dvp.labId + " localLoinc=" + dvp.localLoinc, e.getMessage());
                }

            } else {
                Integer masterVocabId = null;
                try {
                    masterVocabId = ElrDao.getTestResultByNominalOrganism(dvp.con, localResultValue, dvp.labId);
                    if(masterVocabId != null){
                        dvp.setMasterVocabId(masterVocabId);
                        value = ElrDao.getAppCodedValueFromMasterVocabId(dvp.con, masterVocabId, 1);
                    }
                    else{
                        dvp.exList.add(new MasterException(MasterException.NO_MASTER_VOCAB_ID_FOUND_FOR_RESULT_VALUE, localResultValue));
                    }
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Error getAppCodedValueFromMasterVocabId masterVocabId=" + masterVocabId + " localLoinc=" + dvp.localLoinc, e.getMessage());
                }


            }
        }
        return value;
    }
}
