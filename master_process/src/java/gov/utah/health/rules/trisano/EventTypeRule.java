package gov.utah.health.rules.trisano;

import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;

/**
 *
 * @author UDOH
 */
public class EventTypeRule extends PathRule {

    public EventTypeRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp,MasterTrisanoPath mtp) {
        
        return "MorbidityEvent";

    }
}
