package gov.utah.health.rules.trisano;

import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class PostalCodeRule extends PathRule {

    public static final Logger logger = Logger.getLogger("PostalCodeRule");
    public static final String VALID_ZIP_REGEX = "^\\d{5}(-\\d{4})?$";
    public static final String ZIP_9_MISSING_DASH = "^\\d{9}$";
    List ids;

    public PostalCodeRule(List ids) {
        super(-1);
        this.ids = ids;
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        String masterValue = null;

        try {
            masterValue = DocumentUtils.getPathValue(dvp.masterDoc, mtp.getMasterPath());
        } catch (DOMException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
        } catch (XPathExpressionException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
        }
        if (masterValue != null) {
            derivedValue = masterValue;
            
            Boolean validZip = false;
            
            // alter zip like 123456789 to 12345-6789
            if(Pattern.matches(ZIP_9_MISSING_DASH, derivedValue)) {
                derivedValue = derivedValue.substring(0,5) + "-" + derivedValue.substring(5);
                validZip = true;
            } else {
                validZip = Pattern.matches(VALID_ZIP_REGEX, derivedValue);  //ElrDao.isValidPostalCode(dvp.con, derivedValue);
            }
            if (!validZip) {
                dvp.exList.add(new MasterException(MasterException.INVALID_ZIP_CODE, derivedValue));
            }
        }

        return derivedValue;

    }

    @Override
    public boolean isRuleForPath(Integer pathId) {
        if (this.ids != null
                && pathId != null
                && this.ids.contains(pathId)) {
            return true;
        }
        return false;
    }
}
