package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class SpecimenSourceIdRule extends PathRule {

    public static final Logger logger = Logger.getLogger("SpecimenSourceIdRule");

    public SpecimenSourceIdRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        String masterValue = null;

        try {
            //local_specimen_source_id
            masterValue = DocumentUtils.getPathValue(dvp.masterDoc, mtp.getMasterPath());
        } catch (DOMException e) {
            logger.log(Level.SEVERE, "Error accessing master path in doc", e.getMessage());
        } catch (XPathExpressionException e) {
            logger.log(Level.SEVERE, "Error accessing master path in doc", e.getMessage());
        }

        if (masterValue != null && masterValue.trim().length() > 0) {

            try {
                derivedValue = ElrDao.getAppCode(dvp.con, masterValue, mtp.getTrisanoPathId(),dvp.labId,1);
                if (derivedValue == null) {
                    dvp.exList.add(new MasterException(MasterException.SPECIMEN_NOT_MAPPED, masterValue));
                } else if (derivedValue.toLowerCase().equals("x")) {
                    try {
                        derivedValue = ElrDao.getSpecimenSource(dvp.con, dvp.masterLoinc);
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Error accessing SpecimenSource", e.getMessage());
                    }
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error accessing trisano code", e.getMessage());
            }

        } else {
            try {
                derivedValue = ElrDao.getSpecimenSource(dvp.con, dvp.masterLoinc);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error accessing SpecimenSource", e.getMessage());
            }
        }
        if (derivedValue == null || derivedValue.trim().length() == 0) {
            try {
                if (ElrDao.getSourceRequired(dvp.con, dvp.localLoinc,dvp.getLocalResultValue(),dvp.labId)) {
                    dvp.exList.add(new MasterException(MasterException.LAB_MISSING_SPECIMEN, masterValue));
                }

            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error accessing SpecimenSource is required", e.getMessage());
            }

        } else {
            try {
                //DocumentUtils.setPathValue(dvp.masterDoc, "/health/labs/specimen_source", derivedValue);
                DocumentUtils.addPathValue(dvp.masterDoc, "/health/labs/specimen_source", derivedValue, "labs", 1, false);
            } catch (DOMException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            } catch (XPathExpressionException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }
        return derivedValue;

    }
}
