package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterPaths;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.model.trisano.JurisdictionType;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class JurisdictionIdRule extends PathRule {

    public static final Logger logger = Logger.getLogger("JurisdictionIdRule");

    public JurisdictionIdRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        String personZip = null;
        String personState = null;
        String diagnosticZip = null;
        Integer jurisId = null;

        try {
            personZip = DocumentUtils.getPathValue(dvp.masterDoc, MasterPaths.personZip);
        } catch (DOMException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
        } catch (XPathExpressionException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
        }

        if (personZip != null && personZip.trim().length() > 0) {

            Boolean validPersonZip = Pattern.matches(PostalCodeRule.VALID_ZIP_REGEX, personZip);
            if (validPersonZip) {

                try {
                    jurisId = ElrDao.getJurisdictionIdFromZip(dvp.con,personZip);
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "error accessing getJurisdictionIdFromZip", e);
                }
                if (jurisId == null || jurisId <= 0) {

                    try {
                        personState = DocumentUtils.getPathValue(dvp.masterDoc, "/health/person/state");
                    } catch (DOMException e) {
                        dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
                    } catch (XPathExpressionException e) {
                        dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
                    }
                    //TODO look "ut" up
                    if (personState != null && "ut".equals(personState.trim().toLowerCase())) {
                        dvp.exList.add(new MasterException(MasterException.INVALID_ZIP_CODE, personZip));
                    } else {
                        jurisId = JurisdictionType.STATE_OUT_OF_STATE.getId();
                    }
                }
            }

        } else {
            // no patient zip... use diagnostic facility zip

            try {
                diagnosticZip = DocumentUtils.getPathValue(dvp.masterDoc, MasterPaths.diagnosticZipcode);
            } catch (DOMException e) {
                dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
            } catch (XPathExpressionException e) {
                dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
            }

            if (diagnosticZip != null && diagnosticZip.trim().length() > 0) {
                try {
                    jurisId = ElrDao.getJurisdictionIdFromZip(dvp.con,diagnosticZip);
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "error accessing getJurisdictionIdFromZip", e);
                }
            }
        }
        // none of the above use labid to find juris id
        if (jurisId == null || jurisId <= 0) {
            try {
                jurisId = ElrDao.getJurisdictionIdFromLabId(dvp.con, dvp.labId);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "error accessing getJurisdictionIdFromLabId", e);
            }
        }

        if (jurisId != null) {
            derivedValue = Integer.toString(jurisId);
            try {
                //DocumentUtils.setPathValue(dvp.masterDoc, "/health/administrative/jurisdictionId", derivedValue);
                DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
                
            } catch (DOMException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            } catch (XPathExpressionException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }

        return derivedValue;
    }
}
