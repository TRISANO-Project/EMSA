package gov.utah.health.rules.trisano;

import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class LabResultUnitRule extends PathRule {

    public static final Logger logger = Logger.getLogger("LabResultUnitRule");

    public LabResultUnitRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;

        if (dvp.getResultValueLocation() != null && dvp.getResultValueLocation().toLowerCase().equals("result value")) {

            String unit = null;
            try {
                unit = DocumentUtils.getPathValue(dvp.masterDoc, "//health/labs/local_result_unit");
            } catch (DOMException e) {
                dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
            } catch (XPathExpressionException e) {
                dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
            }
            if (unit != null && unit.trim().length() > 0) {
                derivedValue = unit;
            }

        }
        if (derivedValue != null) {
            try {
                //DocumentUtils.setPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue);
                DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
            } catch (DOMException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            } catch (XPathExpressionException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }

        return derivedValue;

    }
}
