package gov.utah.health.rules.trisano;

import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class NoteTypeRule extends PathRule {

    public NoteTypeRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        String note = null;
        try {
            note = DocumentUtils.getPathValue(dvp.masterDoc, "//health/notes/note");
        } catch (DOMException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
        } catch (XPathExpressionException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
        }
        if (note != null && note.trim().length() > 0) {
            derivedValue = "administrative";
        }
        return derivedValue;

    }
}
