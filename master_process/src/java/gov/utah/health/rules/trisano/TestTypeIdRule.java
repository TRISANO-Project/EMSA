package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class TestTypeIdRule extends PathRule {

    public static final Logger logger = Logger.getLogger("TestTypeIdRule");

    public TestTypeIdRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        try {
            derivedValue = ElrDao.getTestType(dvp.con, dvp.masterLoinc);
            if (derivedValue != null) {
                try {
                    //DocumentUtils.setPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue);
                    DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
                } catch (DOMException e) {
                    logger.log(Level.SEVERE, "error setting value in master doc.", e);
                } catch (XPathExpressionException e) {
                    logger.log(Level.SEVERE, "error setting value in master doc.", e);
                }
            }
            else{
                //TODO The labs_attributes/lab_results/test_type_id is a required field in trisano
                // so if any lab_results data is to be added the test_type_id must be set.
                dvp.exList.add(new MasterException(MasterException.NO_TEST_TYPE_FOUND_FOR_MASTER_LOINC, dvp.masterLoinc));
            
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing test type=", e.getMessage());
        }

        return derivedValue;

    }
}
