package gov.utah.health.rules.trisano;

import java.util.ArrayList;
import java.util.List;

public class PathRuleList {

    public static final List<PathRule> ruleList = new ArrayList();

    static {
        // elr.structure_path_application ids
        ruleList.add(new DiseaseIdRule(591));
        ruleList.add(new EventTypeRule(599));
        ruleList.add(new JurisdictionIdRule(642));
        ruleList.add(new LabResultsCommentRule(589));
        ruleList.add(new NoteTypeRule(609));
        ruleList.add(new OrganismIdRule(590));
        ruleList.add(new ParentGuardianRule(600));
        List<Integer> zipCodePaths = new ArrayList();
        zipCodePaths.add(606);
        zipCodePaths.add(633);
        ruleList.add(new PostalCodeRule(zipCodePaths));//606,633
        ruleList.add(new ReferenceRangeRule(583));
        ruleList.add(new ResultValueRule(586));
        ruleList.add(new SpecimenSourceIdRule(580));
        ruleList.add(new StateCaseStatusIdRule(596));
        ruleList.add(new TestResultIdRule(585));
        ruleList.add(new TestTypeIdRule(584));
        ruleList.add(new LabResultUnitRule(587));
        ruleList.add(new LabNameRule(579));
        ruleList.add(new ReportingAgencyNameRule(635));
        ruleList.add(new CountyIdRule(602));
        
    }

    public static PathRule getRule(Integer appPathId) {
        PathRule pr = null;
        
        for (PathRule p : ruleList) {
            if (p.isRuleForPath(appPathId)) {
                pr = p;
                break;
            }
        }
        return pr;
    }
}
