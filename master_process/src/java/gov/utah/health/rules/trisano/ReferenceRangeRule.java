package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class ReferenceRangeRule extends PathRule {

    public static final Logger logger = Logger.getLogger("ReferenceRangeRule");

    public ReferenceRangeRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        try {
            String refRange = ElrDao.getReferenceRange(dvp.con, dvp.localLoinc, dvp.labId);
            if (refRange != null) {
                derivedValue = refRange;
            } else {
                // get the referenceRange out of the master
                String mRefRange = null;
                try {
                    mRefRange = DocumentUtils.getPathValue(dvp.masterDoc, "//health/labs/local_reference_range"); ///health/labs/local_reference_range or /health/labs/reference_range
                } catch (DOMException e) {
                    dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));

                } catch (XPathExpressionException e) {
                    dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
                }
                if (mRefRange == null) {
                    dvp.exList.add(new MasterException(MasterException.NO_REFERENCE_RANGE_FOUND_IN_MASTERDOC, dvp.localLoinc));
                } else {
                    derivedValue = mRefRange;
                }
            }
            if (derivedValue != null && derivedValue.trim().length() > 0) {
                try {
                    //DocumentUtils.setPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue);
                    DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
                } catch (DOMException e) {
                    logger.log(Level.SEVERE, "error setting value in master doc.", e);
                } catch (XPathExpressionException e) {
                    logger.log(Level.SEVERE, "error setting value in master doc.", e);
                }
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error accessing database for reference_range.", e.getMessage());
        }

        return derivedValue;

    }
}
