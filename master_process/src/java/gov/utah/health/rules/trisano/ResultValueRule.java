package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class ResultValueRule extends PathRule {

    public static final Logger logger = Logger.getLogger("ResultValueRule");

    public ResultValueRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        if (!dvp.getInterpretResultsForLoinc()) {
            //elr.get_nomord_resultvalue
            derivedValue = ElrDao.getNomordResultValue(dvp.con, dvp.getLocalResultValue(), dvp.labId);
        } else {

            if (dvp.getResultValueLocation() != null && dvp.getResultValueLocation().toLowerCase().equals("result value")) {

                String localResultValue = dvp.getLocalResultValue();
                if (localResultValue != null && localResultValue.trim().length() > 0) {
                    derivedValue = localResultValue;
                }
            }

        }

        if (derivedValue != null) {
            try {
                DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue, "labs", 1, false);
            } catch (DOMException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            } catch (XPathExpressionException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }
        return derivedValue;

    }
}
