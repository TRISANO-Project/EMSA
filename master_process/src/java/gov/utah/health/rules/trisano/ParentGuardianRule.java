package gov.utah.health.rules.trisano;

import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class ParentGuardianRule extends PathRule {

    public ParentGuardianRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        try {
            String parentName = DocumentUtils.getPathValue(dvp.masterDoc, "//health/person/parent_name");
            String parentRelationship = DocumentUtils.getPathValue(dvp.masterDoc, "//health/person/parent_relationship");

            StringBuilder sb = new StringBuilder();

            if (parentName != null && parentName.trim().length() > 0) {
                sb.append(parentName);
                if (parentRelationship != null && parentRelationship.trim().length() > 0) {
                    sb.append("(");
                    sb.append(parentRelationship);
                    sb.append(")");
                }
                derivedValue = sb.toString();
            }

        } catch (DOMException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
        } catch (XPathExpressionException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
        }
        return derivedValue;

    }
}
