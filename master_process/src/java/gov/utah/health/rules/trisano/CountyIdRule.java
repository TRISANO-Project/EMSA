package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class CountyIdRule extends PathRule {

    public static final Logger logger = Logger.getLogger("JurisdictionIdRule");

    public CountyIdRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        String derivedValue = null;
        String county = null;

        try {
            county = DocumentUtils.getPathValue(dvp.masterDoc, mtp.getMasterPath());
        } catch (DOMException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
        } catch (XPathExpressionException e) {
            dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, mtp.getMasterPath() + " " + e.getMessage()));
        }

        if (county != null && county.trim().length() > 0) {
            try {
                derivedValue = ElrDao.getAppCode(dvp.con, county, mtp.getTrisanoPathId(),dvp.labId,1);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }
        return derivedValue;
    }
}
