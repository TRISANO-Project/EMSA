package gov.utah.health.rules.trisano;

import gov.utah.health.data.elr.ElrDao;
import gov.utah.health.model.master.MasterException;
import gov.utah.health.model.master.MasterTrisanoPath;
import gov.utah.health.service.master.DerivedValueProcessor;
import gov.utah.health.util.DocumentUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.DOMException;

/**
 *
 * @author UDOH
 */
public class LabResultsCommentRule extends PathRule {

    public static final Logger logger = Logger.getLogger("LabResultsCommentRule");

    public LabResultsCommentRule(Integer id) {
        super(id);
    }

    @Override
    public String eval(DerivedValueProcessor dvp, MasterTrisanoPath mtp) {

        StringBuilder derivedValue = new StringBuilder();

        if (dvp.getTestResultDerived() == null) {
            TestResultIdRule testResultIdRule = new TestResultIdRule();
            dvp.setTestResultDerived(testResultIdRule.getTestResultDerivedValue(dvp, mtp));
        }

        if(dvp.getLabResultComments() != null){
            derivedValue.append(dvp.getLabResultComments());
        }

        if (!dvp.getInterpretResultsForLoinc()) {
            //elr.get_nomord_comments
            String nomordComments = ElrDao.getNomordComments(dvp.con, dvp.getLocalResultValue(), dvp.labId);
            if(nomordComments != null){
                derivedValue.append(nomordComments);
            }
        } else {

            if (dvp.getResultValueLocation() != null && dvp.getResultValueLocation().toLowerCase().equals("comment")) {
                String localResultValue = null;
                String unit = null;
                try {
                    localResultValue = DocumentUtils.getPathValue(dvp.masterDoc, "//health/labs/local_result_value");
                    unit = DocumentUtils.getPathValue(dvp.masterDoc, "//health/labs/local_result_unit");
                } catch (DOMException e) {
                    dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
                } catch (XPathExpressionException e) {
                    dvp.exList.add(new MasterException(MasterException.ERROR_ACCESSING_MASTERPATH_IN_MASTERDOC, e.getMessage()));
                }
                if (localResultValue != null && localResultValue.trim().length() > 0) {
                    derivedValue.append(localResultValue);
                    if (unit != null && unit.trim().length() > 0) {
                        derivedValue.append(" ");
                        derivedValue.append(unit);
                    }
                }

            }
        }
        if (derivedValue.length() > 0) {
            try {
                String localComment = DocumentUtils.getPathValue(dvp.masterDoc, mtp.getMasterPath());
                if(localComment != null){
                derivedValue.append(localComment);
                }
                DocumentUtils.addPathValue(dvp.masterDoc, mtp.getMasterPath(), derivedValue.toString(), "labs", 1, false);
            } catch (DOMException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            } catch (XPathExpressionException e) {
                logger.log(Level.SEVERE, "error setting value in master doc.", e);
            }
        }

        return derivedValue.toString();

    }
}
