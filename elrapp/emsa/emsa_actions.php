<?php
	
	/**
	 * emsa_actions.php
	 * consolidates edit/move/delete/retry actions
	 * called from emsa/index.php between forms & lists
	 */
	
	unset($clean_actions);
	unset($bulk_actions);
	unset($emsa_action_sql);
	
	include_once WEBROOT_URL.'/includes/message_processing_functions.php';
	include_once WEBROOT_URL.'/includes/classes/notification.php';
	
	/*
	 * BULK ACTION PROCESSING
	 */
	if (isset($_REQUEST['bulk_action']) && isset($_REQUEST['bulk_ids']) && is_array($_REQUEST['bulk_ids']) && (count($_REQUEST['bulk_ids']) > 0)) {
		switch (trim($_REQUEST['bulk_action'])) {
			case "bulk_retry":
			case "bulk_move":
				$bulk_actions['action'] = trim($_REQUEST['bulk_action']);
				break;
		}
		
		foreach ($_REQUEST['bulk_ids'] as $bulk_ids_raw) {
			$bulk_actions['ids'][] = intval($bulk_ids_raw);
		}
	}
	
	if (isset($bulk_actions['action']) && isset($bulk_actions['ids'])) {
		if ($bulk_actions['action'] == "bulk_retry") {
			bulkRetryWrapper($bulk_actions['ids']);  // bulk retry messages
		}
	}
	
	/*
	 * EMSA STANDARD ACTION PRE-PROCESSING
	 */
	if (isset($_REQUEST['emsa_action'])) {
//error_log('TODO >>>>>>> emsa action set: ' . trim($_REQUEST['emsa_action']));
        switch (trim($_REQUEST['emsa_action'])) {
			case "elrauto":
			case "addnew":
			case "update":
			case "edit":
			case "save":
			case "move":
			case "delete":
			case "retry":
			case "set_flag":
			case "unset_flag":
			case "add_qa_comment":
				$clean_actions['action'] = trim($_REQUEST['emsa_action']);
				break;
		}
//error_log('TODO >>>>>>> clean_actions[action]: ' . $clean_actions['action']);
	}
	
	if (isset($_REQUEST['emsa_override'])) {
		if (filter_var(trim($_REQUEST['emsa_override']), FILTER_SANITIZE_NUMBER_INT) == 1) {
			$clean_actions['override'] = true;
			$clean_actions['override_id'] = ((isset($_REQUEST['override_event']) && is_numeric($_REQUEST['override_event'])) ? filter_var(trim($_REQUEST['override_event']), FILTER_SANITIZE_NUMBER_INT) : null);
			//exit;
		}
	}
	
	if (isset($clean_actions)) {
		// valid action specified
		$clean_actions['id'] = ((isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) ? intval(trim($_REQUEST['id'])) : false);
	}
	
	/*
	 * AUTOMATED PROCESSING
	 */
	if (isset($clean_actions['action']) && ($clean_actions['action'] == 'elrauto')) {
error_log('TODO ***>>>emsa action, starting automated processing');
		
    #debug error_log(microtime(true).'    ---');
		#debug error_log(microtime(true).' ...Starting automation');
		// ensure automation is enabled on this environment
		if (AUTOMATION_ENABLED !== true) {
error_log('TODO >>>>>>>Automation DISABLED!!!!!!!!!!!!!!!!!!!!!!');
error_log('TODO >>>>>>>Automation DISABLED!!!!!!!!!!!!!!!!!!!!!!');
			ob_clean();
			header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
			exit;
		}
//error_log('TODO >>>>>>>Automation enabled');
		
		$clean_actions['is_automation'] = true;
		
		// check for a valid ID passed
		if ($clean_actions['id']) {
			#debug error_log(microtime(true).' ...Getting event details');
			$auto_event_details = getEmsaDetail($clean_actions['id']);  // get fname, lname & dob for search
			#debug error_log(microtime(true).' ...Event details complete');
			
			$fSemiAuto = isLoincSemiAutoEntry($auto_event_details['local_loinc_code'], $auto_event_details['local_code'], $auto_event_details['lab_id']);
			if ($props['emsa_environment'] == 'SNHD') {
				unset($master_sxe);
				$sql = "SELECT master_xml FROM ".$my_db_schema."system_messages WHERE id = ".$clean_actions['id'];
//error_log("**** sql master: ".$sql);
				$rs = @pg_query($host_pa, $sql);
//error_log("**** rs: ".$rs. " count: ".@pg_num_rows($rs));
				if ($rs !== false && @pg_num_rows($rs) > 0) {
					$master_xml_raw = @pg_fetch_result($rs, 0, "master_xml");
//error_log("****  master raw: ".$master_xml_raw);
					$master_sxe = simplexml_load_string($master_xml_raw);
//error_log("****  master: ".$master_sxe->asXML());
					if(isDuplicateLab($clean_actions['id'], $master_sxe, $fSemiAuto)) {
error_log("TODO JAY Discarding duplicate lab in emsa_actions");
						$sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.BLACK_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 1, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
						@pg_query($host_pa, $sql);
						ob_clean();
						exit;
					}
				}
			}
			
			// if Master LOINC still in QA, defer to Entry queue
			if (isMasterLoincInQA($auto_event_details['loinc_code'], $auto_event_details['lab_id'])) {
error_log('TODO >>>>>>>>>>>>>>>>>  MASTER LOINC IN QA: '.$auto_event_details['loinc_code'].' '.$auto_event_details['lab_id'].' **********************************************');
error_log('TODO >>>>>>>>>>>>>>>>>  MASTER LOINC IN QA: '.$auto_event_details['loinc_code'].' '.$auto_event_details['lab_id'].' **********************************************');
				ob_clean();
				header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
				auditMessage($clean_actions['id'], 34, $type, 'Master LOINC needs QA; deferred to Entry queue.');  // ELR auto-processing
				exit;
			}
			
			// if Child LOINC specifies SNOMED QA and Child SNOMED is still in QA, skip
			if (isChildSnomedInQA($auto_event_details['local_result_value_2'], $auto_event_details['local_loinc_code'], $auto_event_details['local_code'], $auto_event_details['lab_id'])) {
				ob_clean();
				header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
				auditMessage($clean_actions['id'], 34, $type, 'Child SNOMED needs QA; deferred to Entry queue.');  // ELR auto-processing
				exit;
			}
			
			// if LOINC uses semi-automated entry, defer to Entry queue
			if ($fSemiAuto) {
				ob_clean();
				header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
				auditMessage($clean_actions['id'], 34, $type, 'Child LOINC uses Semi-Automated Entry; deferred to Entry queue.');  // ELR auto-processing
				exit;
			}
			
//error_log('TODO >>>>>>>Automation past entry queue filters');
			
			/* 
			 * Random QA disabled for now per Susan 2013-08-20, will QA from Entry-deferred messages instead
			if (isLoincInRandomQA($auto_event_details['loinc_code'], $auto_event_details['lab_id'])) {
				#debug error_log('Msg selected for random QA: '.trim($clean_actions['id']));  // we heard you like to QA, so we put some QA in your QA so you could QA while you QA
				$clean_actions['action'] = 'move';
				$_REQUEST['info'] = 'Message selected for random QA';
				$_REQUEST['target'] = QA_STATUS;
			} else */ 
			if (is_array($auto_event_details) && (count($auto_event_details) > 0)) {
				// determine master condition and gateway crossrefs from the ELR disease
//error_log('TODO >>> Automation disease: '.$auto_event_details['disease']);
				$trisano_disease = $auto_event_details['disease'];
				$master_condition = $trisano_disease;
				
				$current_sql = "SELECT  mv.concept FROM  ".$my_db_schema."vocab_master2app ma 
					INNER JOIN ".$my_db_schema."vocab_master_vocab mv ON (ma.master_id = mv.id) 
					INNER JOIN ".$my_db_schema."vocab_master_condition mc ON (mc.condition = mv.id) 
					WHERE ma.coded_value = '".$trisano_disease."';";
				$current_rs = @pg_query($host_pa, $current_sql);

				if ($current_rs && pg_num_rows($current_rs) > 0 && ($current_row = @pg_fetch_object($current_rs))) {
					$master_condition = trim($current_row->concept);
				} else {
					error_log('Could not fetch master condition for disease: '.$auto_event_details['disease']);
				}
//error_log('TODO >>> Master condition: '.$master_condition);

				try {
					#debug error_log(microtime(true).' ...Getting People Search results');
					$ps_results_array = getPeopleSearchResults($auto_event_details['fname'], $auto_event_details['lname'], $auto_event_details['bday'], $master_condition);  // conduct person search, get back result arrays
					#debug error_log(microtime(true).' ...People Search done');
				} catch (Exception $e) {
					ob_clean();
					header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
					auditMessage($clean_actions['id'], 34, $type, 'Unable to retrieve Person Search results (reason:  '.$e->getMessage().'); deferred to Data Entry');  // ELR auto-processing
					exit;
				}
error_log("TODO >>>>>>>Automation past people search results: ".$auto_event_details['lname'].", ".$auto_event_details['fname']." ".$auto_event_details['bday']);
				
				$new_event_type = 'cmr';
				if ((count($ps_results_array['five_star']) > 0) && 
					(isPropertyTrue($props['always_assign_5_star']) || (count($ps_results_array['four_star']) === 0)))  {
					// if all 5-star results (or only 5-star results and <4-star results), transmute to 'update' w/ all people IDs from 5-star matches
error_log('TODO >>>>>>>Automation 5 Star');
					$clean_actions['action'] = 'update';
					$_REQUEST['match_persons'] = implode('|', $ps_results_array['five_star']);
					auditMessage($clean_actions['id'], 34, $type, 'Found exact Person Search matches!  Checking for whitelisted events in Person ID(s) '.implode(', ', $ps_results_array['five_star']).'.');  // ELR auto-processing
                } elseif (count($ps_results_array['four_star']) == 1) {
                    //TODO Jay assigning to 4 star matches to deal with Covid BOLUS
                    // if there is only 1 four star match and it is exact on name, dob, and gender
                    $person_id = strtolower($ps_results_array['four_star'][0]);
                    $full_name = strtolower($ps_results_array['results'][$person_id]['full_name']);
                    $birth_date = $ps_results_array['results'][$person_id]['birth_date'];
                    $sex = strtolower($ps_results_array['results'][$person_id]['sex']);
                    $auto_full_name = strtolower($auto_event_details['full_name']);
                    $auto_birth_date = $auto_event_details['birth_date'];
                    $auto_sex = strtolower($auto_event_details['sex']);
                    $len = strlen($full_name);
                    $auto_len = strlen($auto_full_name);
                    if($len > $auto_len) {
                        $full_name = substr($full_name, 0, $auto_len);
                    } else {
                        $auto_full_name = substr($auto_full_name, 0, $len);
                    }
error_log('TODO Covid19: '.$full_name.'|'.$auto_full_name.'\n|'.$birth_date.'|'.$auto_birth_date.'\n|'
    .$sex.'|'.$auto_sex);
                    if($full_name == $auto_full_name 
                        && (empty($sex) || empty($auto_sex) || $sex == $auto_sex) 
                        && (empty($birth_date) || empty($auto_birth_date) || $birth_date == $auto_birth_date)) {
                        // assign the lab to the matched entry
                        $clean_actions['action'] = 'update';
                        $_REQUEST['match_persons'] = implode('|', $ps_results_array['four_star']);
                    } else {
                        ob_clean();
                        header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
                    }
				} elseif ((count($ps_results_array['sub_four_star']) > 0) && (count($ps_results_array['five_star']) === 0) && (count($ps_results_array['four_star']) === 0)) {
					// if all <4-star results, add new person & new CMR
error_log('TODO >>>>>>>Automation 4 Star');
					if(isCD4orNegHivViralTest($auto_event_details['loinc_code'], $auto_event_details['test_result'])) {
error_log("TODO JAY Discarding  Neg HIV or Viral Test in emsa_actions 1");
						$sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.BLACK_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 1, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
						@pg_query($host_pa, $sql);
						ob_clean();
						exit;
					} else {
						$clean_actions['action'] = 'addnew';
						auditMessage($clean_actions['id'], 34, $type, 'No Person Search matches with 4+ match quality; creating new CMR');  // ELR auto-processing
					}
				} elseif ((count($ps_results_array['sub_four_star']) === 0) && (count($ps_results_array['five_star']) === 0) && (count($ps_results_array['four_star']) === 0)) {
error_log('TODO >>>>>>>Automation no person found');
error_log('TODO Jay event_details: '.$auto_event_details['local_test_name'][0].':'.$auto_event_details['test_result']);
					if(isCD4orNegHivViralTest($auto_event_details['loinc_code'], $auto_event_details['test_result'])) {
error_log("TODO JAY Discarding Neg HIV or Viral Test in emsa_actions 2");
						$sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.BLACK_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).
                            '\', status = 0, external_system_id = NULL, deleted = 1, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
						@pg_query($host_pa, $sql);
						ob_clean();
						exit;
					} else {
						// no results, add new person & new CMR
						$clean_actions['action'] = 'addnew';
						auditMessage($clean_actions['id'], 34, $type, 'No Person Search matches found; creating new CMR');  // ELR auto-processing
					}
				} else {
					// if any results >=4 & <5, do nothing & leave in Entry queue
                    // For SNHD Negative Hepatitis C and B labs, move to gray list			
                    if ($props['emsa_environment'] == 'SNHD' && 
                        (preg_match("/hepatitis c/i", $master_condition) || preg_match("/hepatitis b/i", $master_condition)) && 
                        strtoupper(substr($auto_event_details['test_result'],0,8)) == 'NEGATIVE') {
error_log('TODO >>>>>>>Moving neg Hep b/c Lab to Gray list.');
						$sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).
                            '\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
						@pg_query($host_pa, $sql);
						ob_clean();

                    } else {
error_log('TODO >>>>>>>Automation leave in entry queue');
                        ob_clean();
                        header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
                        auditMessage($clean_actions['id'], 34, $type, 'Ambiguous Person Search result; deferred to Data Entry');  // ELR auto-processing
                    }
					exit;
				}
			} else {
				ob_clean();
				header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error", TRUE, 500);  // no record found with that id
				exit;
			}
		} else {
			ob_clean();
			header($_SERVER['SERVER_PROTOCOL'] . " 400 Bad Request", TRUE, 400);  // id passed to automated processor not a valid integer
			exit;
		}
		#debug error_log(microtime(true).' ...Automation determination complete');
	} else {
		$clean_actions['is_automation'] = false;
	}
	
	if ($clean_actions['id']) {
		// valid ID & action passed
		
		if (messageUnprocessed($clean_actions['id'], $type)) {
			// message has not been previously processed, continue with specified action
			
			$clean_actions['target'] = ((isset($_REQUEST['target']) & (strlen(trim($_REQUEST['target'])) > 0)) ? trim($_REQUEST['target']) : "NULL");
			$clean_actions['info'] = ((isset($_REQUEST['info']) & (strlen(trim($_REQUEST['info'])) > 0)) ? trim($_REQUEST['info']) : null);
			$clean_actions['match_persons'] = ((isset($_REQUEST['match_persons']) & (strlen(trim($_REQUEST['match_persons'])) > 0)) ? trim($_REQUEST['match_persons']) : false);
			
			/**
			 * draw 'Edit' form
			 */
			if ($clean_actions['action'] == 'edit') {
				include 'edit.php';
				
			/**
			 * save 'Edit' changes
			 */
			} elseif ($clean_actions['action'] == 'save') {
				// get master xml from database to make changes
				$edit_sql = 'SELECT master_xml FROM '.$my_db_schema.'system_messages WHERE id = '.$clean_actions['id'].';';
				$edit_row = get_db_row_array(get_db_result_set($host_pa, $edit_sql));
				$edit_master_xml = $edit_row['master_xml'];
				
				$edit_xmlstring = simplexml_load_string($edit_master_xml);
				$edit_sxe = new SimpleXMLElement($edit_xmlstring->asXML());
				unset($edit_sxe->exceptions);  // no longer used, clean up to avoid confusion -- exceptions stored in db
				
				// set edited values
				if (isset($_REQUEST['edit_last_name']) && (strlen(trim($_REQUEST['edit_last_name'])) > 0)) {
					$edit_sxe->person->last_name = filter_var($_REQUEST['edit_last_name'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->last_name = '';
				}
				
				if (isset($_REQUEST['edit_first_name']) && (strlen(trim($_REQUEST['edit_first_name'])) > 0)) {
					$edit_sxe->person->first_name = filter_var($_REQUEST['edit_first_name'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->first_name = '';
				}
				
				if (isset($_REQUEST['edit_middle_name']) && (strlen(trim($_REQUEST['edit_middle_name'])) > 0)) {
					$edit_sxe->person->middle_name = filter_var($_REQUEST['edit_middle_name'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->middle_name = '';
				}
				
				if (isset($_REQUEST['edit_gender']) && (strlen(trim($_REQUEST['edit_gender'])) > 0)) {
					$edit_sxe->person->gender = filter_var($_REQUEST['edit_gender'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->gender = '';
				}
				
				if (isset($_REQUEST['edit_street_name']) && (strlen(trim($_REQUEST['edit_street_name'])) > 0)) {
					$edit_sxe->person->street_name = filter_var($_REQUEST['edit_street_name'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->street_name = '';
				}
				
				if (isset($_REQUEST['edit_unit']) && (strlen(trim($_REQUEST['edit_unit'])) > 0)) {
					$edit_sxe->person->unit = filter_var($_REQUEST['edit_unit'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->unit = '';
				}
				
				if (isset($_REQUEST['edit_city']) && (strlen(trim($_REQUEST['edit_city'])) > 0)) {
					$edit_sxe->person->city = filter_var($_REQUEST['edit_city'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->city = '';
				}
				
				if (isset($_REQUEST['edit_state']) && (strlen(trim($_REQUEST['edit_state'])) > 0)) {
					$edit_sxe->person->state = filter_var($_REQUEST['edit_state'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->state = '';
				}
				
				if (isset($_REQUEST['edit_county']) && (strlen(trim($_REQUEST['edit_county'])) > 0)) {
					$edit_sxe->person->county = filter_var($_REQUEST['edit_county'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->county = '';
				}
				
				if (isset($_REQUEST['edit_zip']) && (strlen(trim($_REQUEST['edit_zip'])) > 0)) {
					$edit_sxe->person->zip = filter_var($_REQUEST['edit_zip'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->zip = '';
				}
				
				if (isset($_REQUEST['edit_country']) && (strlen(trim($_REQUEST['edit_country'])) > 0)) {
					$edit_sxe->person->country = filter_var($_REQUEST['edit_country'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->person->country = '';
				}
				
				if (isset($_REQUEST['edit_area_code']) && (strlen(trim($_REQUEST['edit_area_code'])) > 0)) {
					$edit_sxe->person->area_code = strtr(filter_var($_REQUEST['edit_area_code'], FILTER_SANITIZE_STRING), array("-" => "", "(" => "", ")" => "", " " => ""));
				} else {
					$edit_sxe->person->area_code = '';
				}
				
				if (isset($_REQUEST['edit_telephone']) && (strlen(trim($_REQUEST['edit_telephone'])) > 0)) {
					$edit_sxe->person->phone = strtr(filter_var($_REQUEST['edit_telephone'], FILTER_SANITIZE_STRING), array("-" => "", "(" => "", ")" => "", " " => ""));
				} else {
					$edit_sxe->person->phone = '';
				}
				
				if (isset($_REQUEST['edit_test_type']) && (strlen(trim($_REQUEST['edit_test_type'])) > 0)) {
					$edit_sxe->labs->test_type = filter_var($_REQUEST['edit_test_type'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->labs->test_type = '';
				}
				
				if (isset($_REQUEST['edit_test_name']) && (strlen(trim($_REQUEST['edit_test_name'])) > 0)) {
					$edit_sxe->labs->local_test_name = filter_var($_REQUEST['edit_test_name'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->labs->local_test_name = '';
				}
				
				if (isset($_REQUEST['edit_result_value']) && (strlen(trim($_REQUEST['edit_result_value'])) > 0)) {
					$edit_sxe->labs->local_result_value = filter_var($_REQUEST['edit_result_value'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->labs->local_result_value = '';
				}
				
				if (isset($_REQUEST['edit_specimen_source']) && (strlen(trim($_REQUEST['edit_specimen_source'])) > 0)) {
					$edit_sxe->labs->local_specimen_source = filter_var($_REQUEST['edit_specimen_source'], FILTER_SANITIZE_STRING);
				} else {
					$edit_sxe->labs->local_specimen_source = '';
				}
				
				
				if (isset($_REQUEST['edit_dob']) && (strlen(trim($_REQUEST['edit_dob'])) > 0)) {
					if ($temp_edit_dob = strtotime(trim($_REQUEST['edit_dob']))) {
						$temp_edit_dob_arr = getdate($temp_edit_dob);
						$now_parsed = getdate(time());
						$now_year = intval($now_parsed['year']);
						if (intval($temp_edit_dob_arr['year']) <= $now_year) {
							// make sure valid date generated and birthdate isn't in the future
							$edit_sxe->person->date_of_birth = date(DATE_W3C, $temp_edit_dob);
						}
					}
				} else {
					$edit_sxe->person->date_of_birth = '';
				}
				
				
				if (isset($_REQUEST['edit_date_reported']) && (strlen(trim($_REQUEST['edit_date_reported'])) > 0)) {
					if ($temp_edit_date_reported = strtotime(trim($_REQUEST['edit_date_reported']))) {
						$temp_edit_date_reported_arr = getdate($temp_edit_date_reported);
						$now_parsed = getdate(time());
						$now_year = intval($now_parsed['year']);
						if (intval($temp_edit_date_reported_arr['year']) <= $now_year) {
							// make sure valid date generated and date reported isn't in the future
							$edit_sxe->reporting->report_date = date(DATE_W3C, $temp_edit_date_reported);
						}
					}
				} else {
					$edit_sxe->reporting->report_date = '';
				}
				
				
				if (isset($_REQUEST['edit_date_collected']) && (strlen(trim($_REQUEST['edit_date_collected'])) > 0)) {
					if ($temp_edit_date_collected = strtotime(trim($_REQUEST['edit_date_collected']))) {
						$temp_edit_date_collected_arr = getdate($temp_edit_date_collected);
						$now_parsed = getdate(time());
						$now_year = intval($now_parsed['year']);
						if (intval($temp_edit_date_collected_arr['year']) <= $now_year) {
							// make sure valid date generated and date collected isn't in the future
							$edit_sxe->labs->collection_date = date(DATE_W3C, $temp_edit_date_collected);
						}
					}
				} else {
					$edit_sxe->labs->collection_date = '';
				}
				
				
				$edit_xmlstring = $edit_sxe->asXML();
				
				// send updated master xml through master save process
				$encoded_masterxml = urlencode(str_replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "", $edit_xmlstring));
				
				$xml_for_masterservice = new SimpleXMLElement("<health_message></health_message>");
				$xml_for_masterservice->addChild("user_id", "9999");
				$xml_for_masterservice->addChild("system_message_id", intval($clean_actions['id']));
				$xml_for_masterservice->addChild("health_xml", $encoded_masterxml);
				
				#debug echo "<hr><strong>Debug -- Sent XML:<br>".htmlentities($xml_for_masterservice->asXML())."</strong><hr>";
				
				try {
					$master_client = @new SoapClient($props['master_wsdl_url']);
				} catch (Exception $e) {
					suicide($e, -1, 1);
				} catch (SoapFault $f) {
					suicide($f, -1, 1);
				}
				
				highlight("Revalidating message...", "ui-icon-elrretry");
//error_log("TODO >>>> attempting sending message to TriSano");
				
				if ($master_client) {
					try {
error_log("TODO >>>> calling saveMaster");
						$master_result = $master_client->saveMaster(array("healthMessage" => $xml_for_masterservice->asXML()));
					} catch (Exception $e) {
error_log("TODO >>>> EXCEPTION");
						suicide("Unable to process message.  The following errors occurred:<pre>".$e."</pre><br><br>XML Sent:<br><pre>".htmlentities($final_xml)."</pre>", -1, 1);
					} catch (SoapFault $f) {
error_log("TODO >>>> SOAP FAULT");
						suicide("Unable to process message.  The following errors occurred:<pre>".$f."</pre><br><br>XML Sent:<br><pre>".htmlentities($final_xml)."</pre>", -1, 1);
					}
//error_log("TODO >>>> getting return status");
					$master_return = simplexml_load_string($master_result->return);
					if (getTrisanoReturnStatus($master_return->status_message)->status) {
						#debug echo "<hr><strong style=\"color: red;\">Debug -- Returned XML:<br>".htmlentities($master_return->asXML())."</strong><hr>";
						auditMessage($clean_actions['id'], 21, $type);
						
						$current_status = "Unknown";
						$current_status_sql = "SELECT ss.name AS status_name FROM ".$my_db_schema."system_statuses ss INNER JOIN ".$my_db_schema."system_messages sm ON (sm.final_status = ss.id AND sm.id = ".intval($clean_actions['id']).");";
						$current_status = @pg_fetch_result(@pg_query($host_pa, $current_status_sql), 0, "status_name");
						
						$current_sql = "SELECT se.description AS description, sme.info AS info, ss.name AS type FROM ".$my_db_schema."system_message_exceptions sme 
							INNER JOIN ".$my_db_schema."system_exceptions se ON (sme.exception_id = se.exception_id) 
							INNER JOIN ".$my_db_schema."system_statuses ss ON (se.exception_type_id = ss.id) 
							WHERE sme.system_message_id = ".intval($clean_actions['id'])." 
							ORDER BY sme.id;";
						$current_rs = @pg_query($host_pa, $current_sql);
						if ($current_rs) {
							if (pg_num_rows($current_rs) > 0) {
								$post_save_exception_html = <<<EOH
<table class="audit_log">
		<thead>
			<tr>
				<th>Error Type</th>
				<th>Error Description</th>
				<th>Error Details</th>
			</tr>
		</thead>
		<tbody>
EOH;
								while ($current_row = @pg_fetch_object($current_rs)) {
									$post_save_exception_html .= "<tr><td>".htmlentities($current_row->type)."</td><td>".htmlentities($current_row->description)."</td><td>".htmlentities($current_row->info)."</td></tr>";
								}
								$post_save_exception_html .= "</tbody></table>";
								highlight("Changes to message were saved, but the following errors were still found:<hr>".$post_save_exception_html."<hr><br>Message moved to '".$current_status."' queue.", "ui-icon-elrerror");
							} else {
								highlight("Changes saved successfully & message validated with no errors!<br><br>Message moved to '".$current_status."' queue.", "ui-icon-elrsuccess");
							}
							@pg_free_result($current_rs);
						} else {
							suicide("Changes to message were saved, but could not verify successful message validation due to database errors.<br><br>Message moved to '".$current_status."' queue.", 1);
						}
						
					} else {
						suicide("Unable to save changes to message.<br><br>".htmlentities(getTrisanoReturnStatus($master_return->status_message)->errors));
					}
				}
				
			/**
			 * add a message QA comment
			 */
			} elseif ($clean_actions['action'] == 'add_qa_comment') {
				$emsa_action_sql = "INSERT INTO ".$my_db_schema."system_message_comments (system_message_id, user_id, comment) VALUES (".$clean_actions['id'].", '".pg_escape_string(trim($_SESSION['umdid']))."', '".pg_escape_string($clean_actions['info'])."');";
				if (@get_db_result_set($host_pa,$emsa_action_sql)) {
					highlight("QA comment added successfully!", 'ui-icon-elrsuccess');
				} else {
					suicide("Error:  Could not set add comment.", 1);
				}
				
			/**
			 * set a message flag
			 */
			} elseif ($clean_actions['action'] == 'set_flag') {
				$emsa_action_sql = "UPDATE ".$my_db_schema."system_messages SET message_flags = message_flags | ".intval($clean_actions['target'])." WHERE id=".$clean_actions['id'];
				if (@get_db_result_set($host_pa,$emsa_action_sql)) {
					highlight("Message flag successfully set!", 'ui-icon-elrsuccess');
					auditMessage($clean_actions['id'], 32, $type, decodeMessageFlagName(intval($clean_actions['target'])).((!is_null($clean_actions['info'])) ? ' ('.$clean_actions['info'].')' : '')); // message flag set
				} else {
					suicide("Error:  Could not set message flag.", 1);
				}
				if (!is_null($clean_actions['info'])) {
					addOrUpdateFlagComment($clean_actions['id'], $clean_actions['target'], $clean_actions['info']);  // comment was passed along with the flag; store in system_message_flag_comments
				}
				
			/**
			 * clear a message flag
			 */
			} elseif ($clean_actions['action'] == 'unset_flag') {
				$emsa_action_sql = "UPDATE ".$my_db_schema."system_messages SET message_flags = message_flags & ~".intval($clean_actions['target'])." WHERE id=".$clean_actions['id'];
				if (@get_db_result_set($host_pa,$emsa_action_sql)) {
					highlight("Message flag successfully cleared!", 'ui-icon-elrsuccess');
					auditMessage($clean_actions['id'], 33, $type, decodeMessageFlagName(intval($clean_actions['target']))); // message flag clear
				} else {
					suicide("Error:  Could not clear message flag.", 1);
				}
				clearFlagComment($clean_actions['id'], $clean_actions['target']);  // un-set comment for this flag in system_message_flag_comments, if one is set
				
			/**
			 * move selected ELR message to new list
			 */
			} elseif ($clean_actions['action'] == 'move') {
				auditMessage($clean_actions['id'], 25, $type, $clean_actions['info']);
				
				$emsa_action_sql = "UPDATE ".$my_db_schema."system_messages SET deleted=0,final_status=".$clean_actions['target']." WHERE id=".$clean_actions['id'];
				if (@get_db_result_set($host_pa,$emsa_action_sql)) {
					highlight("Message successfully moved!");
				} else {
					suicide("Error:  Could not move message to new list.", 1);
				}
				
			/**
			 * delete specified ELR message
			 */
			} elseif ($clean_actions['action'] == 'delete') {
				auditMessage($clean_actions['id'], 8, $type);
				
				if (deleteMessageById($clean_actions['id'])) {
					highlight('Message successfully deleted!');
				} else {
					suicide('Error:  Could not delete message.', 1);
				}
				
			/**
			 * retry sending new ELR message to TriSano
			 */
			} elseif ($clean_actions['action'] == 'retry') {
error_log("TODO >>>> retry sending message to TriSano");
				// get master xml from database
				$edit_sql = "SELECT master_xml FROM ".$my_db_schema."system_messages WHERE id=".$clean_actions['id'];
				$edit_row = get_db_row_array(get_db_result_set($host_pa,$edit_sql));
				$edit_master_xml = $edit_row['master_xml'];
				
				$edit_sxe = simplexml_load_string($edit_master_xml);
				unset($edit_sxe->exceptions);  // no longer used, clean up to avoid confusion -- exceptions stored in db
				
				$edit_xmlstring = $edit_sxe->asXML();
				
				// send updated master xml through master save process
				$encoded_masterxml = urlencode(str_replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "", $edit_xmlstring));
				
				$xml_for_masterservice = new SimpleXMLElement("<health_message></health_message>");
				$xml_for_masterservice->addChild("user_id", "9999");
				$xml_for_masterservice->addChild("system_message_id", intval($clean_actions['id']));
				$xml_for_masterservice->addChild("health_xml", $encoded_masterxml);
				
				#debug echo "<hr><strong>Debug -- Sent XML:<br>".htmlentities($xml_for_masterservice->asXML())."</strong><hr>";
				
				try {
					$master_client = @new SoapClient($props['master_wsdl_url']);
				} catch (Exception $e) {
					suicide($e, -1, 1);
				} catch (SoapFault $f) {
					suicide($f, -1, 1);
				}
				
				highlight("Revalidating message...", "ui-icon-elrretry");
				
				if ($master_client) {
					try {
						$master_result = $master_client->saveMaster(array("healthMessage" => $xml_for_masterservice->asXML()));
					} catch (Exception $e) {
						suicide("Unable to process message.  The following errors occurred:<pre>".$e."</pre><br><br>XML Sent:<br><pre>".htmlentities($final_xml)."</pre>", -1, 1);
					} catch (SoapFault $f) {
						suicide("Unable to process message.  The following errors occurred:<pre>".$f."</pre><br><br>XML Sent:<br><pre>".htmlentities($final_xml)."</pre>", -1, 1);
					}
					$master_return = simplexml_load_string($master_result->return);
					if (getTrisanoReturnStatus($master_return->status_message)->status) {
						#debug echo "<hr><strong style=\"color: red;\">Debug -- Returned XML:<br>".htmlentities($master_return->asXML())."</strong><hr>";
						auditMessage($clean_actions['id'], 21, $type);
						
						$current_status = "Unknown";
						$current_status_sql = "SELECT ss.name AS status_name FROM ".$my_db_schema."system_statuses ss INNER JOIN ".$my_db_schema."system_messages sm ON (sm.final_status = ss.id AND sm.id = ".intval($clean_actions['id']).");";
						$current_status = @pg_fetch_result(@pg_query($host_pa, $current_status_sql), 0, "status_name");
						
						$current_sql = "SELECT se.description AS description, sme.info AS info, ss.name AS type FROM ".$my_db_schema."system_message_exceptions sme 
							INNER JOIN ".$my_db_schema."system_exceptions se ON (sme.exception_id = se.exception_id) 
							INNER JOIN ".$my_db_schema."system_statuses ss ON (se.exception_type_id = ss.id) 
							WHERE sme.system_message_id = ".intval($clean_actions['id'])." 
							ORDER BY sme.id;";
						$current_rs = @pg_query($host_pa, $current_sql);
						if ($current_rs) {
							if (pg_num_rows($current_rs) > 0) {
								$post_save_exception_html = <<<EOH
<table class="audit_log">
		<thead>
			<tr>
				<th>Error Type</th>
				<th>Error Description</th>
				<th>Error Details</th>
			</tr>
		</thead>
		<tbody>
EOH;
								while ($current_row = @pg_fetch_object($current_rs)) {
									$post_save_exception_html .= "<tr><td>".htmlentities($current_row->type)."</td><td>".htmlentities($current_row->description)."</td><td>".htmlentities($current_row->info)."</td></tr>";
								}
								$post_save_exception_html .= "</tbody></table>";
								highlight("The following errors were found while revalidating this message:<hr>".$post_save_exception_html."<hr><br>Message moved to '".$current_status."' queue.", "ui-icon-elrerror");
							} else {
								highlight("Message validated with no errors!<br><br>Message moved to '".$current_status."' queue.", "ui-icon-elrsuccess");
							}
							@pg_free_result($current_rs);
						} else {
							suicide("Could not verify successful message validation due to database errors.<br><br>Message moved to '".$current_status."' queue.", 1);
						}
						
					} else {
						suicide("Unable to revalidate message.<br><br>".htmlentities(getTrisanoReturnStatus($master_return->status_message)->errors));
					}
				}
			
			/**
			 * add ELR as new CMR + new person
			 */
			} elseif ($clean_actions['action'] == 'addnew') {
				#debug error_log(microtime(true).' ...Starting AddNew');
error_log('TODO >>>>>>>Automation Adding a new cmr');
				$this_audit_id = auditMessage($clean_actions['id'], 35, $type, '[Add New Person & CMR] option selected');  // attempting to create new CMR
				// get TriSano XML & notification flags, along with Master Condition name...
				$xml_qry = 'SELECT 
						sm.loinc_code as loinc_code, sm.master_xml as master_xml, sm.transformed_xml as transformed_xml, sm.lab_id as lab_id, 
						mc.white_rule as white_rule, mc.notify_state as notify_state, mc.immediate_notify as immediate_notify, mc.district_override AS district_override, 
						mv.concept as condition 
					FROM '.$my_db_schema.'system_messages sm 
					INNER JOIN '.$my_db_schema.'vocab_master_loinc ml ON (sm.id = '.$clean_actions['id'].' AND sm.loinc_code = ml.loinc) 
					INNER JOIN '.$my_db_schema.'vocab_master_vocab mv ON ((sm.disease = mv.concept) AND (mv.category = '.$my_db_schema.'vocab_category_id(\'condition\'))) 
					INNER JOIN '.$my_db_schema.'vocab_master_condition mc ON (mv.id = mc.condition);';
				$xml_rs = @pg_query($host_pa, $xml_qry);
//error_log('TODO >>>> xml qry: '.$xml_qry);
				if ($xml_rs === false || (@pg_num_rows($xml_rs) < 1)) {
error_log('TODO >>>>>>>Automation MESSAGE NOT FOUND');
					// message not found, throw exception
					messageException($clean_actions['id'], $this_audit_id, 69, 'Could not assign message; message not found.');  // Entry queue exception
					suicide('Could not assign message:  message could not be found.<br><br>Message moved to Exception list.');
				} else {
					$trisano_xml_raw = @pg_fetch_result($xml_rs, 0, "transformed_xml");
					$master_xml_raw = @pg_fetch_result($xml_rs, 0, "master_xml");
					$surveillance_disease = @pg_fetch_result($xml_rs, 0, "condition");
					$laboratory_id = @pg_fetch_result($xml_rs, 0, 'lab_id');
					$notify_state = @pg_fetch_result($xml_rs, 0, "notify_state");
					$is_immediate = @pg_fetch_result($xml_rs, 0, "immediate_notify");
					$master_loinc_code = @pg_fetch_result($xml_rs, 0, "loinc_code");
					$district_override_id = intval(@pg_fetch_result($xml_rs, 0, "district_override"));
					$whitelist_rule_tmp = @pg_fetch_result($xml_rs, 0, 'white_rule');
					@pg_free_result($xml_rs);
				
					$add_xml = $trisano_xml_raw;
					$master_xml = $master_xml_raw;
					$master_sxe = simplexml_load_string($master_xml);
					
					$master_test_result = trim($master_sxe->labs->test_result);
//error_log('TODO >>>>>>>> ************** master test result ' . $master_test_result);
					$master_organism = trim($master_sxe->labs->organism);
					$child_local_loinc_code = trim($master_sxe->labs->local_loinc_code);
					$child_local_code = trim($master_sxe->labs->local_code);
					$child_loinc = ((isset($child_local_loinc_code) && !is_null($child_local_loinc_code) && !empty($child_local_loinc_code)) ? $child_local_loinc_code : $child_local_code);
					$is_pregnancy = isPregnancyIndicated($child_loinc, $laboratory_id);
					$allow_new_cmr = allowNewCMR($master_loinc_code, $master_test_result, $master_sxe, 1);
					$valid_specimen = isValidSpecimenSource($child_loinc, getLocalResultValue($master_sxe, $laboratory_id), trim($master_sxe->labs->specimen_source), intval($laboratory_id));
					$is_surveillance = isSurveillance($master_loinc_code, $master_organism, $master_test_result, 1);

					if ($is_surveillance === IS_SURVEILLANCE_ERR_NO_RULES_DEFINED) {
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 72, '[isSurveillance] '.$master_loinc_code);  // No Case Management rules defined for Master LOINC
						highlight('Could not add new event:  No Case Management rules defined for Master LOINC.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_NO_RULES_MATCHED) {
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 73, '[isSurveillance] '.$master_loinc_code.'/'.$master_test_result);  // No Case Management rules evaluated true
						highlight('Could not add new event:  No Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_MULT_RULES_MATCHED) {
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Multiple rules evaluated true; '.$master_loinc_code.'/'.$master_test_result);  // Multiple Case Management rules evaluated true
						highlight('Could not add new event:  Multiple Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_MULT_ORGS_MATCHED) {
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Multiple organisms found with the same name; '.$master_loinc_code.'/'.$master_organism);  // Multiple organisms are defined with the same name
						highlight('Could not add new event:  Multiple Organisms were found with the same name.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_REQFIELD_LOINC) {
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Missing required field (Master LOINC)');  // Unable to evaluate rule
						highlight('Could not add new event:  Unable to determine surveillance event status due to missing master LOINC code.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_REQFIELD_ORGNAME) {
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Missing required field (Organism)');  // Unable to evaluate rule
						highlight('Could not add new event:  Unable to determine surveillance event status due to missing organism name.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_REQFIELD_TESTRESULT) {
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Missing required field (Test Result)');  // Unable to evaluate rule
						highlight('Could not add new event:  Unable to determine surveillance event status due to missing test result.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					}
					
					// check to see if incoming lab result is positive; if not, discard
					$add_sxe = simplexml_load_string($add_xml);
					$test_result_arr = $add_sxe->xpath("//test_result_id");
					foreach ($test_result_arr as $test_result_key => $test_result_val) {
						$test_result = intval($test_result_val);
					}
					
//error_log(" emsa actions xml: ".$add_xml);
					$person_identifier = array (
						'first_name' => (string)$add_sxe->interested_party_attributes->person->first_name,
						'last_name' => (string)$add_sxe->interested_party_attributes->person->last_name,
						'birth_date' => (string)$add_sxe->interested_party_attributes->person->birth_date);
error_log(" emsa actions person: ".print_r($person_identifier, true));

                    // for TCPH set event type to be created to Assessment Event
                    $add_sxe->create_assessment_event = true;

					// modify xml for SNHD whitelist rule behaviors
					addSNHDNewWhitelistSettings($clean_actions['id'], $whitelist_rule_tmp, $master_test_result, $add_sxe, $person_identifier, $master_condition);
					
					$close_surveillance = closeSurveillanceBoolByJurisdiction(intval(trim($add_sxe->jurisdiction_attributes->place->id)));  // check whether this jurisdiction's surveillance events should be open or closed at the LHD level
					
					removeParentGuardianEmptyGlue($add_sxe); // check for & remove ',' value in empty parent_guardian field
					addCmrAbnormalFlagToComments($master_sxe, $add_sxe); // add decoded Abnormal Flag to comments, if present
					addCmrHL7ToNotes(getHL7($clean_actions['id']), $add_sxe); // add original HL7 as a 'Note'
					geocodeNedssXml($add_sxe);  // geocode lat/long coordinates for address, if possible
					
					// set pregnancy flag if pregnancy status interpreted from LOINC
					if ($is_pregnancy) {
						setPatientPregnant($add_sxe, true);
					}
					
					// close if surveillance event
					if (($is_surveillance === IS_SURVEILLANCE_YES) && $close_surveillance) {
						setWorkflow($add_sxe, 'closed');
					}
					
					// add 'SURV' to other_data_1 field for surveillance event search/filtering
					if ($is_surveillance === IS_SURVEILLANCE_YES) {
						if (isset($add_sxe->events->other_data_1)) {
							$add_sxe->events->other_data_1 = 'SURV';
						} else {
							$add_sxe->events->addChild('other_data_1', 'SURV');
						}
					}
					
					// check for override to default jurisdiction, based on condition
					if ($district_override_id > 0) {
						$add_sxe->jurisdiction_attributes->place->id = getJurisdictionByDistrictID($district_override_id);
					}
					
					if ($allow_new_cmr == ALLOW_CMR_YES) {
						if (($is_surveillance === IS_SURVEILLANCE_YES) || ($is_surveillance === IS_SURVEILLANCE_NO)) {
							if ($valid_specimen === SPECIMEN_VALID) {
error_log('TODO >>>>>>>Automation about to add new CMR');
								/*
								 * Add incoming ELR message as new CMR event
								 */
								 
								// check for married name change; exclude if in 'override' mode
								unset($family_name_check);
								$family_name_check = ((isset($clean_actions['override']) && $clean_actions['override']) ? false : hasFamilyNameChanged($clean_actions['id'], intval(trim($add_sxe->disease_events->disease_id))));
								if (is_array($family_name_check)) {
									$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
									messageException($clean_actions['id'], $this_audit_id, 68, 'Matched Record Numbers:<br>'.nedssLinkByEventArray($family_name_check));  // last name change due to marriage
									highlight('Could not add new event:  Female or minor patient family name may have changed due to marriage<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
								} else {
									// wrap trisano xml with health_message node & required params...
									$add_xml_final = str_ireplace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", str_ireplace("</trisano_health>", "</trisano_health></health_message>", str_ireplace("<trisano_health>", "<health_message><username>9999</username><system>TRISANO</system><trisano_health>", str_ireplace("\xC2\xA0", ' ', $add_sxe->asXML()))));
									
									// init SOAP client
									try {
										$addcmr_client = @new SoapClient($props['sqla_wsdl_url']);
									} catch (Exception $e) {
										suicide($e, -1, 1);
									} catch (SoapFault $f) {
										suicide($f, -1, 1);
									}
									
									if ($addcmr_client) {
//error_log('TODO >>>>>>>Automation adding CMR');
error_log('TODO >>>>>***Automation add_xml_final: '.$add_xml_final);
										try {
											$addcmr_result = $addcmr_client->addCmr(array("healthMessage" => $add_xml_final));
										} catch (Exception $e) {
											suicide("Unable to process message.  The following errors occurred:<pre>".$e."</pre><br><br>XML Sent:<br><pre>".htmlentities(formatXml($add_xml_final))."</pre>", -1, 1);
										} catch (SoapFault $f) {
											suicide("Unable to process message.  The following errors occurred:<pre>".$f."</pre><br><br>XML Sent:<br><pre>".htmlentities(formatXml($add_xml_final))."</pre>", -1, 1);
										}
error_log('TODO >>>>>>>Automation SUCCESS adding CMR');
										#debug echo "<pre><strong>XML Sent:</strong><br>".htmlentities(formatXml($add_xml_final))."</pre>";
										$addcmr_return = simplexml_load_string($addcmr_result->return);
										#debug echo "<hr><pre><strong>XML Returned:</strong><br>";
										#debug print_r(htmlentities(formatXml($addcmr_result->return)));
										#debug echo "</pre>";
										
										if (getTrisanoReturnStatus($addcmr_return->status_message)->status === false) {
											// a trisano error of some sort occurred
											$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
											
											// log the NEDSS XML sent to add this event
											auditXML(null, $add_xml_final, $this_audit_id, false);
										
											messageException($clean_actions['id'], $this_audit_id, 66, getTrisanoReturnStatus($addcmr_return->status_message)->errors);  // unable to xmit trisano XML
											suicide("A TriSano error occurred while attempting to add/update this message.<br><br>Message moved to Exception list.");
										} else {
											$this_audit_id = auditMessage($clean_actions['id'], 23, $type);  // new person & new CMR event
											
											// log the NEDSS XML sent to add this event
											auditXML(null, $add_xml_final, $this_audit_id, false);
										
											// to obtain lab ID of lab_results just affected (either inserted or updated), 
											// get back list of lab_results elements and sort by date updated
											$addcmr_labid_arr = $addcmr_return->xpath("//lab_results");
											foreach ($addcmr_labid_arr as $addcmr_lab_obj => $addcmr_lab_val) {
												$addcmr_lab_ids[intval($addcmr_lab_val->id)] = strtotime(trim($addcmr_lab_val->updated_at));
											}
											arsort($addcmr_lab_ids, SORT_NUMERIC);
											reset($addcmr_lab_ids);
											
											$addcmr_lab_id = intval(key($addcmr_lab_ids));
											$addcmr_test_type = trim($master_sxe->labs->test_type);
											$addcmr_test_result = trim($master_sxe->labs->test_result);
											$addcmr_result_value = trim($master_sxe->labs->result_value);
											$addcmr_specimen_source = trim($master_sxe->labs->specimen_source);
                                            $event_id_arr = $addcmr_return->xpath("trisano_health/events/id");
											$addcmr_event_id = intval($event_id_arr[0]);
											$addcmr_record_number = trisanoRecordNumberByEventId($addcmr_event_id);
											$jurisdiction_id_arr = $addcmr_return->xpath("trisano_health/jurisdiction_attributes/place/id");
											$addcmr_jurisdiction_id = intval($jurisdiction_id_arr[0]);
											
											// prepare notification rules
											$nc = new NotificationContainer();
			
											$nc->system_message_id		= $clean_actions['id'];
											$nc->nedss_event_id			= $addcmr_event_id;
											$nc->nedss_record_number	= $addcmr_record_number;
											$nc->is_surveillance		= (($is_surveillance === IS_SURVEILLANCE_YES) ? true : false);
											$nc->is_immediate			= (($is_immediate == "t") ? true : false);
											$nc->is_state				= (($notify_state == "t") ? true : false);
											$nc->is_pregnancy			= $is_pregnancy;
											$nc->is_automated			= $clean_actions['is_automation'];
											$nc->is_new_cmr				= true;
											$nc->is_event_closed		= false;
											$nc->condition				= $surveillance_disease;
											$nc->jurisdiction			= $addcmr_jurisdiction_id;
											$nc->test_type				= $addcmr_test_type;
											$nc->test_result			= $addcmr_test_result;
											$nc->result_value			= $addcmr_result_value;
											$nc->investigator			= null;
											$nc->master_loinc			= $master_loinc_code;
											$nc->specimen				= $addcmr_specimen_source;
											if($new_event_type == 'ae') {
												$nc->event_type				= 'AssessmentEvent';
											} else {
												$nc->event_type				= 'MorbidityEvent';
											}
											
											try {
												$nc->logNotification();  // run rules, generate any appropriate notifications
											} catch (NotificationDatabaseException $e) {
												suicide($e->getMessage(), 1);
											} catch (NotificationValidationException $e) {
												suicide($e->getMessage());
											} catch (Exception $e) {
												suicide($e->getMessage());
											}
											
											$addnewcmr_sql = sprintf("UPDATE %ssystem_messages SET status = 0, final_status = %d, assigned_date = '%s', event_id = %d, lab_result_id = %d WHERE id = %d;",
												$my_db_schema, ASSIGNED_STATUS, date("Y-m-d H:i:s", time()), $addcmr_event_id, $addcmr_lab_id, $clean_actions['id']);
											
											if (@pg_query($host_pa, $addnewcmr_sql)) {
												highlight("New event created for ".$addcmr_return->trisano_health->interested_party_attributes->person->last_name.", ".$addcmr_return->trisano_health->interested_party_attributes->person->first_name."!<br><br><button type=\"button\" class=\"emsa_btn_viewnedss\" id=\"emsa_btn_viewnedss_".$addcmr_event_id."\" value=\"".$addcmr_event_id."\">View in TriSano</button><br>", "ui-icon-elrsuccess");
											} else {
												suicide("Warning:  New event created for ".$addcmr_return->trisano_health->interested_party_attributes->person->last_name.", ".$addcmr_return->trisano_health->interested_party_attributes->person->first_name." in TriSano, but there were database errors (see error details below)!<br><br><button type=\"button\" class=\"emsa_btn_viewnedss\" id=\"emsa_btn_viewnedss_".$addcmr_event_id."\" value=\"".$addcmr_event_id."\">View in TriSano</button><br>", 1);
											}
										}
									}
								}
							} elseif ($valid_specimen === SPECIMEN_INVALID) {
								/*
								 * Specimen source in invalid specimen sources list; graylilst message
								 */
error_log('TODO >>>>>>>Automation specimen invalid graylist');
								
								auditMessage($clean_actions['id'], 27, $type, 'Invalid specimen source');  // list change by whitelist rule
								
								$discard_sql = sprintf("UPDATE %ssystem_messages SET final_status = %d, assigned_date = '%s', status = 0, external_system_id = '', deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = %d;", $my_db_schema, GRAY_STATUS, date("Y-m-d H:i:s", time()), $clean_actions['id']);
								if (@pg_query($host_pa, $discard_sql)) {
									highlight("Valid specimen source not provided.<br><br>Message moved to Gray list...", "ui-icon-elrsuccess");
								} else {
									suicide("A database error occurred while attempting to Gray List incoming message.  Please contact a system administrator.", 1);
								}
							} else {
								/*
								 * Specimen source not in valid or invalid specimen sources list; send to exception
								 */
error_log('TODO >>>>>>>Automation specimen invalid exception');
								
								$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
								messageException($clean_actions['id'], $this_audit_id, 69, 'Unexpected specimen source detected.');  // Entry queue exception
								highlight('Could not add new event:  Condition requires specimen source validation, but an invalid specimen source was provided.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
							}
						}
                    } elseif ($allow_new_cmr == ALLOW_CMR_NO_QUEST_ID_AS_NAME) {
						/*
                         * QUEST ordered message with case id in name fields, leave in entry queue
						 */
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					} elseif ($allow_new_cmr == ALLOW_CMR_NO_INFORM) {
						/*
						 * Informative test only, do not use to create new CMR; graylist incoming ELR message
						 */
						
						auditMessage($clean_actions['id'], 27, $type, 'Informative test only -- not usable for independent new CMR');  // list change by whitelist rule
						
						$discard_sql = sprintf("UPDATE %ssystem_messages SET final_status = %d, assigned_date = '%s', status = 0, external_system_id = '', deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = %d;", $my_db_schema, GRAY_STATUS, date("Y-m-d H:i:s", time()), $clean_actions['id']);
						if (@pg_query($host_pa, $discard_sql)) {
							highlight("Test type not allowed to create new CMRs without existing event.<br><br>Message moved to Gray list...", "ui-icon-elrsuccess");
						} else {
							suicide("A database error occurred while attempting to Gray List incoming message.  Please contact a system administrator.", 1);
						}
					} elseif ($allow_new_cmr == ALLOW_CMR_NO_NEG) {
						/*
						 * Non-Positive result, Graylist incoming ELR message
						 */
						
						auditMessage($clean_actions['id'], 27, $type, 'Negative or Inconclusive test results');  // list change by whitelist rule
						
						$discard_sql = sprintf("UPDATE %ssystem_messages SET final_status = %d, assigned_date = '%s', status = 0, external_system_id = '', deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = %d;", $my_db_schema, GRAY_STATUS, date("Y-m-d H:i:s", time()), $clean_actions['id']);
						if (@pg_query($host_pa, $discard_sql)) {
							highlight("Incoming message contains Negative or Inconclusive results.<br><br>Message moved to Gray list...", "ui-icon-elrsuccess");
						} else {
							suicide("A database error occurred while attempting to Gray List incoming message.  Please contact a system administrator.", 1);
						}
					} elseif ($allow_new_cmr == ALLOW_CMR_ERR_XML_REQ_FIELDS) {
						/*
						 * Missing required fields for allowNewCMR()
						 */
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 56, '[allowNewCMR] Unable to run Case Management rules due to missing Master LOINC Code or Test Result.');  // Unable to evaluate rule
						highlight('Could not add new event:  Unable to run Case Management rules due to missing data.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($allow_new_cmr == ALLOW_CMR_ERR_NO_RULES_DEFINED) {
						/*
						 * No Case Management rules defined for Master LOINC
						 */
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 72, '[allowNewCMR] '.$master_loinc_code);  // No Case Management rules defined for Master LOINC
						highlight('Could not add new event:  No Case Management rules defined for Master LOINC.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($allow_new_cmr == ALLOW_CMR_ERR_NO_RULES_MATCHED) {
						/*
						 * No Case Management rules evaluated true
						 */
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 73, '[allowNewCMR] '.$master_loinc_code.'/'.$master_test_result);  // No Case Management rules evaluated true
						highlight('Could not add new event:  No Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					} elseif ($allow_new_cmr == ALLOW_CMR_ERR_MULT_RULES_MATCHED) {
						/*
						 * Multiple Case Management rules evaluated true
						 */
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 56, '[allowNewCMR] Multiple rules evaluated true; '.$master_loinc_code.'/'.$master_test_result);  // Multiple Case Management rules evaluated true
						highlight('Could not add new event:  Multiple Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					}
				
				}
				#debug error_log(microtime(true).' ...AddNew complete');
				if ($clean_actions['is_automation'] === true) {
					// if elrauto and message is assigned, exit (don't display EMSA list)
					ob_clean();
					header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
					exit;
				}
				
			/**
			 * for exception resolution, manually append an ELR message to an existing CMR
			 */
			} elseif ($clean_actions['action'] == 'update' && isset($clean_actions['override']) && $clean_actions['override']) {
error_log('TODO >>Update cmr or override');
			
				include 'lib/override_update_cmr.php';
				
			/**
			 * use whitelist rules to add/append new ELR to existing person's CMR
			 */
			} elseif ($clean_actions['action'] == 'update') {
				#debug error_log(microtime(true).' ...Starting Update');
				include 'lib/update_cmr.php';
				#debug error_log(microtime(true).' ...Update complete');
				if ($clean_actions['is_automation'] === true) {
					// if elrauto and message is assigned, exit (don't display EMSA list)
					ob_clean();
					header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
					exit;
				}
				
			}
		} else {
			highlight('Could not perform selected action on this message:  the selected ELR message has already been processed.', 'ui-icon-elrstop');
		}
	}
	
?>