<?php

	include_once WEBROOT_URL.'/includes/emsa_functions.php';

	#debug $start_time = microtime(true);
	/**
	 * Session stuff for filters
	 */
	$_SESSION["emsa_params"]["type"] = 1;
	if (isset($_GET['type']) && filter_var($_GET['type'], FILTER_VALIDATE_INT)) {
		$_SESSION["emsa_params"]["type"] = intval(filter_var($_GET['type'], FILTER_SANITIZE_NUMBER_INT));
	}

	/**
	 * Sort order
	 */
	if (isset($_GET['sort']) && filter_var($_GET['sort'], FILTER_VALIDATE_INT)) {
		$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"] = intval(filter_var($_GET['sort'], FILTER_SANITIZE_NUMBER_INT));
	}
	if (!isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"])) {
		if ($_SESSION["emsa_params"]["type"] == ASSIGNED_STATUS) {
			$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"] = 6;
		} else {
			$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"] = 1;
		}
	}

	/**
	 * Get/store current page for selected queue...
	 */
	if (isset($_GET['currentpage']) && filter_var($_GET['currentpage'], FILTER_VALIDATE_INT)) {
		$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["current_page"] = intval(filter_var($_GET['currentpage'], FILTER_SANITIZE_NUMBER_INT));
	}
	// default to pg. 1 if no 'current_page' is saved & no page# specified in querystring
	if (!isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["current_page"])) {
		$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["current_page"] = 1;
	}

	/**
	 * Search/Filter Prep
	 */
	// pre-build our vocab-specific search data...
	if (isset($_GET["q"])) {
		if ((trim($_GET["q"]) != "Enter search terms...") && (strlen(trim($_GET["q"])) > 0)) {
			$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_display"] = htmlentities(trim($_GET["q"]), ENT_QUOTES, "UTF-8");
			$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_sql"] = pg_escape_string(trim($_GET["q"]));
			if (!isset($_GET['f'])) {
				// search query found, but no filters selected
				// if any filters were previously SESSIONized, they've been deselected via the UI, so we'll clear them...
				unset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]);
			}
		} else {
			// search field was empty/defaulted, so we'll destroy the saved search params...
			unset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_display"]);
			unset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_sql"]);
			// not only was search blank, but no filters selected, so clear them as well...
			unset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]);
		}
	}

	// update SESSIONized filters or destroy them if no filters are selected...
	if (isset($_GET['f'])) {
		if (isset($_GET['f']['evalue']) && empty($_GET['f']['evalue'])) {
			unset($_GET['f']['evalue']);
		}
		if (is_array($_GET['f']) && count($_GET['f']) > 0) {
			$_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"] = $_GET['f'];
		}
		if (empty($_SESSION['emsa_params'][$_SESSION['emsa_params']['type']]['filters']['evalue'])) {
			unset($_SESSION['emsa_params'][$_SESSION['emsa_params']['type']]['filters']['evalue']);
		}
	}

	// preserve clicked message on page refresh
	if (isset($_GET['focus']) && filter_var($_GET['focus'], FILTER_VALIDATE_INT)){
		$focus_id = intval(filter_var($_GET['focus'], FILTER_SANITIZE_NUMBER_INT));
	}

	// set queue type
	if(isset($_GET['type']) && filter_var($_GET['type'], FILTER_VALIDATE_INT)){
		$type=intval(filter_var($_GET['type'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		$type=ENTRY_STATUS;
	}

	$view=0;
	$_SESSION['cview']=0;

	if(isset($_REQUEST['view'])){
		$view=$_REQUEST['view'];
		$_SESSION['cview']=$_REQUEST['view'];
	}

	$jd=0;

	if(isset($_REQUEST['jd'])){
		$jd=$_REQUEST['jd'];
	}

	// define whether to show 'immediately reportable' list
	// this can be overwritten by lib/emsa_lists.php for cases when there is only a single list (such as 'Assigned')
	// and there shouldn't be either an 'immediate' or 'non-immediate' list
	if($type == ENTRY_STATUS || $type == PENDING_STATUS || $type == EXCEPTIONS_STATUS || $type == QA_STATUS){
		$immediate=1;
	}else{
		$immediate=0;
	}

	session_write_close(); // done writing to session; prevent blocking

	include_once './emsa/lib/emsa_jquery_functions.php';

	// Note: Adding ability to render all exception right by name
	$_IS_EXCEPTIONS_STATUS = ($type == EXCEPTIONS_STATUS);
?>

			<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elremsa"></span><?php echo getEmsaQueueName($type);?></h1>

			<div id="confirm_addnew_dialog" title="Really Add New Person?" style="display: none;">
				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><strong>Warning:</strong>  You have selected one or more matching people from the search results.<br><br>Are you sure you want to create a new person instead of adding these results to the people selected below?</p>
			</div>

			<form name="search_form" id="search_form" method="GET" action="<?php echo $main_url; ?>">

				<div class="vocab_search ui-tabs ui-widget">
					<label for="q" class="vocab_search_form" style="margin-right: 7px;">Search:</label><input type="text" name="q" id="q" class="vocab_query ui-corner-all">
					<button name="q_go" id="q_go" title="Search">Search</button>
					<button type="button" name="clear_filters" id="clear_filters" title="Clear all filters/search terms">Clear Search/Filters</button>
					<button type="button" name="toggle_filters" id="toggle_filters" title="Show/hide filters">Hide Advanced Filters</button>
					<?php if (checkPermission(URIGHTS_TAB_QA)) { ?>
					<button type="button" name="toggle_bulk" id="toggle_bulk" title="Show/hide bulk message actions">Hide Bulk Message Actions</button>
					<?php } ?>
				</div>

				<?php
					############### If filters applied, display which ones ###############
					if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"])) {
				?>
				<div class="vocab_search ui-widget ui-widget-content ui-state-highlight ui-corner-all" style="padding: 5px;">
					<span class="ui-icon ui-icon-elroptions" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;">Filtering by
				<?php
						$active_filters = 0;
						foreach ($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"] as $sqlfiltercol => $sqlfiltervals) {
							$sqlfiltercol_label = "Unknown";
							switch ($sqlfiltercol) {
								case "disease":
									$sqlfiltercol_label = "Disease";
									break;
								case "lab":
									$sqlfiltercol_label = "Lab";
									break;
								case "eflag":
									$sqlfiltercol_label = "Error Flag";
									break;
								case "evalue":
									$sqlfiltercol_label = "Error Details";
									break;
								case "showdeleted";
									$sqlfiltercol_label = "Show Deleted";
									break;
								case "showauto";
									$sqlfiltercol_label = "Manual vs. Automated";
									break;
								case "mflag":
									$sqlfiltercol_label = "Message Flag";
									break;
								case "clinician":
									$sqlfiltercol_label = "Clinician";
									break;
								case "testtype":
									$sqlfiltercol_label = "Test Type";
									break;
							}
							if ($active_filters == 0) {
								echo "<strong>" . $sqlfiltercol_label . "</strong>";
							} else {
								echo ", <strong>" . $sqlfiltercol_label . "</strong>";			}
							$active_filters++;
						}
				?>
					</p>
				</div>
				<?php
					}
				?>

				<div class="vocab_filter ui-widget ui-widget-content ui-corner-all">
					<div style="clear: both;"><label class="vocab_search_form">Filters:</label></div>
				<?php
					// disease filter
					echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Disease</label>";
					echo " <span rel=\"f_disease\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_disease\" class=\"vocab_filter_selectnone\">None</span><br>";
					echo "<div class=\"vocab_filter_checklist\" rel=\"f_disease\">";

					$role_sql = '';

					if (isset($_SESSION['override_user_role']) && !empty($_SESSION['override_user_role'])) {
						$role_sql = intval(trim($_SESSION['override_user_role']));
					} else {
						$role_sql = implode(',', $_SESSION['user_system_roles']);
					}

					$filter_qry = 'SELECT DISTINCT mv.concept FROM '.$my_db_schema.'vocab_master_vocab mv
						INNER JOIN '.$my_db_schema.'vocab_master_condition mc ON (mv.id = mc.condition)
						WHERE mv.category = elr.vocab_category_id(\'condition\') ORDER BY mv.concept ASC;';
					$filter_rs = @pg_query($host_pa, $filter_qry);
					$value_iterator = 0;
					while ($filter_row = @pg_fetch_object($filter_rs)) {
						$value_iterator++;
						$this_filter_selected = false;

						if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["disease"]) && in_array(base64_encode($filter_row->concept), $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["disease"])) {
							$this_filter_selected = true;
						}

						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_disease_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[disease][]\" id=\"f_disease_".$value_iterator."\" checked value=\"".base64_encode($filter_row->concept)."\">&nbsp;" . ((is_null(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_disease_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[disease][]\" id=\"f_disease_".$value_iterator."\" value=\"".base64_encode($filter_row->concept)."\">&nbsp;" . ((is_null(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
					}
					@pg_free_result($filter_rs);
					echo "</div></div>";

					// lab filter
					echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Lab</label><br>";
					echo "<div class=\"vocab_filter_checklist\">";

					$filter_qry = 'SELECT DISTINCT sl.id, sl.ui_name
						FROM '.$my_db_schema.'structure_labs sl
						INNER JOIN '.$my_db_schema.'system_role_loincs_by_lab rd ON (rd.lab_id = sl.id)
						WHERE rd.system_role_id IN ('.$role_sql.')
						ORDER BY ui_name ASC;';
					$filter_rs = @pg_query($host_pa, $filter_qry);
					$value_iterator = 0;
					while ($filter_row = @pg_fetch_object($filter_rs)) {
						$value_iterator++;
						$this_filter_selected = false;

						if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["lab"]) && in_array($filter_row->id, $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["lab"])) {
							$this_filter_selected = true;
						}

						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_lab_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[lab][]\" id=\"f_lab_".$value_iterator."\" checked value=\"".intval($filter_row->id)."\">&nbsp;" . ((is_null(htmlentities($filter_row->ui_name, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->ui_name, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->ui_name, ENT_QUOTES, "UTF-8")) . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_lab_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[lab][]\" id=\"f_lab_".$value_iterator."\" value=\"".intval($filter_row->id)."\">&nbsp;" . ((is_null(htmlentities($filter_row->ui_name, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->ui_name, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->ui_name, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
					}
					@pg_free_result($filter_rs);
					echo "</div></div>";

					// show deleted filter
					echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Show Deleted?</label><br>";
					echo "<div class=\"vocab_filter_checklist\">";

					$this_filter_selected = false;

					if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showdeleted"]) && in_array(1, $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showdeleted"])) {
						$this_filter_selected = true;
					}

					if ($this_filter_selected) {
						echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_showdeleted_1\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[showdeleted][]\" id=\"f_showdeleted_1\" checked value=\"1\">&nbsp;Yes</label><br>";
					} else {
						echo "<label class=\"pseudo_select_label\" for=\"f_showdeleted_1\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[showdeleted][]\" id=\"f_showdeleted_1\" value=\"1\">&nbsp;Yes</label><br>";
					}

					echo "</div></div>";

					if ($type == ASSIGNED_STATUS) {
						// assigned 'elr automation' filter
						echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Manual vs. Automated</label><br>";
						echo "<div class=\"vocab_filter_checklist\">";

						$showauto_vals = array(
							0 => 'Only Automated Messages',
							1 => 'Only Non-Automated Messages'
						);

						foreach ($showauto_vals as $showauto_id => $showauto_label) {
							$this_filter_selected = false;

							if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"]) && in_array(intval($showauto_id), $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"])) {
								$this_filter_selected = true;
							}

							if ($this_filter_selected) {
								echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_showauto_".intval($showauto_id)."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[showauto][]\" id=\"f_showauto_".intval($showauto_id)."\" checked value=\"".intval($showauto_id)."\">&nbsp;".trim($showauto_label)."</label><br>";
							} else {
								echo "<label class=\"pseudo_select_label\" for=\"f_showauto_".intval($showauto_id)."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[showauto][]\" id=\"f_showauto_".intval($showauto_id)."\" value=\"".intval($showauto_id)."\">&nbsp;".trim($showauto_label)."</label><br>";
							}
						}

						echo "</div></div>";
					}

					// message flags filter
					echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Message Flags</label>";
					echo " <span rel=\"f_mflag\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_mflag\" class=\"vocab_filter_selectnone\">None</span><br>";
					echo "<div class=\"vocab_filter_checklist\" rel=\"f_mflag\">";

					$flag_qry = sprintf("SELECT id, label AS concept FROM %ssystem_message_flags ORDER BY label ASC;", $my_db_schema);
					$flag_rs = @pg_query($host_pa, $flag_qry);
					$value_iterator = 0;
					while ($flag_row = @pg_fetch_object($flag_rs)) {
						$value_iterator++;
						$this_filter_selected = false;

						if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["mflag"]) && in_array(intval(pow(2, $flag_row->id)), $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["mflag"])) {
							$this_filter_selected = true;
						}

						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_mflag_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[mflag][]\" id=\"f_mflag_".$value_iterator."\" checked value=\"".intval(pow(2, $flag_row->id))."\">&nbsp;" . ((is_null(htmlentities($flag_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($flag_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($flag_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_mflag_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[mflag][]\" id=\"f_mflag_".$value_iterator."\" value=\"".intval(pow(2, $flag_row->id))."\">&nbsp;" . ((is_null(htmlentities($flag_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($flag_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($flag_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
					}
					@pg_free_result($filter_rs);
					echo "</div></div>";

					if ($type==PENDING_STATUS || $type==EXCEPTIONS_STATUS) {
						// exception type filter
						echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Error Flag</label>";
						echo " <span rel=\"f_eflag\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_eflag\" class=\"vocab_filter_selectnone\">None</span><br>";
						echo "<div rel=\"f_eflag\" class=\"vocab_filter_checklist\">";

						$filter_qry = sprintf("SELECT exception_id, description FROM %ssystem_exceptions ORDER BY description ASC;", $my_db_schema);
						$filter_rs = @pg_query($host_pa, $filter_qry);
						$value_iterator = 0;
						while ($filter_row = @pg_fetch_object($filter_rs)) {
							$value_iterator++;
							$this_filter_selected = false;

							if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["eflag"]) && in_array($filter_row->exception_id, $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["eflag"])) {
								$this_filter_selected = true;
							}

							if ($this_filter_selected) {
								echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_eflag_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[eflag][]\" id=\"f_eflag_".$value_iterator."\" checked value=\"".intval($filter_row->exception_id)."\">&nbsp;" . ((is_null(htmlentities($filter_row->description, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->description, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->description, ENT_QUOTES, "UTF-8")) . "</label><br>";
							} else {
								echo "<label class=\"pseudo_select_label\" for=\"f_eflag_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[eflag][]\" id=\"f_eflag_".$value_iterator."\" value=\"".intval($filter_row->exception_id)."\">&nbsp;" . ((is_null(htmlentities($filter_row->description, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->description, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->description, ENT_QUOTES, "UTF-8")) . "</label><br>";
							}
						}
						@pg_free_result($filter_rs);
						echo "</div></div>";

						// exception value filter
						echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Error Details</label><br>";
						//echo "<div class=\"vocab_filter_checklist\">";
						echo '<textarea class="ui-corner-all" style="background-color: lightcyan; font-family: Consolas, \'Courier New\', sans-serif; border-color: #999; width: 300px; height: 75px;" name="f[evalue]" id="f_evalue">'.htmlentities(decodeIfBase64Encoded($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["evalue"]), ENT_QUOTES, "UTF-8").'</textarea>';
						echo "</div>";
					}

					if ($type == GRAY_STATUS) {
						// graylist clinician
						echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Clinician</label>";
						echo " <span rel=\"f_clinician\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_clinician\" class=\"vocab_filter_selectnone\">None</span><br>";
						echo "<div rel=\"f_clinician\" class=\"vocab_filter_checklist\">";

						$filter_qry = sprintf("SELECT DISTINCT clinician FROM %ssystem_messages WHERE final_status = 2;", $my_db_schema);
						$filter_rs = @pg_query($host_pa, $filter_qry);
						$value_iterator = 0;
						while ($filter_row = @pg_fetch_object($filter_rs)) {
							$value_iterator++;
							$this_filter_selected = false;

							if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["clinician"]) && in_array($filter_row->clinician, $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["clinician"])) {
								$this_filter_selected = true;
							}

							if ($this_filter_selected) {
								echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_clinician_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[clinician][]\" id=\"f_clinician_".$value_iterator."\" checked value=\"".htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8")) . "</label><br>";
							} else {
								echo "<label class=\"pseudo_select_label\" for=\"f_clinician_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[clinician][]\" id=\"f_clinician_".$value_iterator."\" value=\"".htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->clinician, ENT_QUOTES, "UTF-8")) . "</label><br>";
							}
						}
						@pg_free_result($filter_rs);
						echo "</div></div>";
					}

					// test type filter
					echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Test Type</label>";
					echo " <span rel=\"f_testtype\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_testtype\" class=\"vocab_filter_selectnone\">None</span><br>";
					echo "<div rel=\"f_testtype\" class=\"vocab_filter_checklist\">";

					$filter_qry = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('test_type') ORDER BY concept ASC;", $my_db_schema);
					$filter_rs = @pg_query($host_pa, $filter_qry);
					$value_iterator = 0;
					while ($filter_row = @pg_fetch_object($filter_rs)) {
						$value_iterator++;
						$this_filter_selected = false;

						if (is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["testtype"]) && in_array($filter_row->id, $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["testtype"])) {
							$this_filter_selected = true;
						}

						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_testtype_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[testtype][]\" id=\"f_testtype_".$value_iterator."\" checked value=\"".intval($filter_row->id)."\">&nbsp;" . ((is_null(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_testtype_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[testtype][]\" id=\"f_testtype_".$value_iterator."\" value=\"".intval($filter_row->id)."\">&nbsp;" . ((is_null(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
					}
					@pg_free_result($filter_rs);
					echo "</div></div>";

					if (($type == ASSIGNED_STATUS) && (checkPermission(URIGHTS_TAB_QA))) {
						// assigned QA sorting
						echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Sort By</label><br>";
						echo '<select class="pseudo_select_label" name="sort" id="sort">';
						echo '<option value="6"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 6) ? ' selected' : '').'>Date Assigned (Newest First)</option>';
						echo '<option value="5"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 5) ? ' selected' : '').'>Date Assigned (Oldest First)</option>';
						echo '<option value="1"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 1) ? ' selected' : '').'>Date Reported (Oldest First)</option>';
						echo '<option value="2"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 2) ? ' selected' : '').'>Date Reported (Newest First)</option>';
						echo '<option value="3"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 3) ? ' selected' : '').'>Patient Name (A-Z)</option>';
						echo '<option value="4"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 4) ? ' selected' : '').'>Patient Name (Z-A)</option>';
						echo '</select>';
						echo "</div>";
					}

					if ($type == GRAY_STATUS) {
						// graylist sorting
						echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">Sort By</label><br>";
						echo '<select class="pseudo_select_label" name="sort" id="sort">';
						echo '<option value="1"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 1) ? ' selected' : '').'>Date Reported (Oldest First)</option>';
						echo '<option value="2"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 2) ? ' selected' : '').'>Date Reported (Newest First)</option>';
						echo '<option value="3"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 3) ? ' selected' : '').'>Patient Name (A-Z)</option>';
						echo '<option value="4"'.((isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) && intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"]) == 4) ? ' selected' : '').'>Patient Name (Z-A)</option>';
						echo '</select>';
						echo "</div>";
					}

				?>
					<br><br><button name="apply_filters" id="apply_filters" title="Apply Filters" style="clear: both; float: left; margin: 5px;">Apply Filters</button>
				</div>

				<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
				<input type="hidden" name="submenu" value="<?php echo ((isset($submenu) && filter_var($submenu, FILTER_VALIDATE_INT) && (intval($submenu) > 0)) ? $submenu : ""); ?>">
				<input type="hidden" name="type" value="<?php echo (($type < 2) ? 1 : $type); ?>">

			</form>

			<?php if ($_IS_EXCEPTIONS_STATUS) :
			// Ability to show and hide the exception for faster look at the exception values
			?>
			<div style="text-align:right;margin:10px auto;padding:6px 10px;" class="ui-widget">
				<button id="show_exception_detail" title="Show All Exceptions">Show All Exceptions</button>
			</div>
			<script>
				jQuery(function($) {
					var isShown = false;
					$("#show_exception_detail").button({
					}).click(function(){
						var $btn = $(this);
						var $btnText = $btn.find('.ui-button-text');
						var text = (isShown) ? 'Show All Exceptions': 'Hide All Exceptions';
						$btnText.text(text);
						isShown = !isShown;
						// console.log(isShown);
						$('.exception_details').each(function() {
						    var $this = $(this);
						    $this.each(function() {
						    	var $exception = $(this);
						    	if (!$exception.find('h3').text().match(/Current Errors/ig)) {
						    		return;
						    	}
						    	// Process the text
						    	var $exceptionRows = $exception.find('.audit_log tbody tr');
						    	if (!$exceptionRows.length) {
						    		return;
						    	}
						    	var $currentException = $exceptionRows.first();
						    	// console.log($currentException.text());
						    	var $parentContainer = $currentException.closest('.emsa_dupsearch');
						    	var $renderRowContainer = $parentContainer.prev();
						    	// console.log($renderRowContainer);
						    	var $renderRow = $renderRowContainer.find('td').first();
						    	if (isShown) {
						    		var $exceptionMarkUp = $('</p>').addClass('easy-render-exception').css({
						    			margin: '5px 10px'
						    		});
						    		$exceptionMarkUp.html('<small>' + $currentException.text() + '</small>');
						    		$renderRow.append($exceptionMarkUp);
						    	} else {
						    		$renderRow.find('.easy-render-exception').remove();
						    	}

						    });
						});

					});
				})
			</script>
			<?php endif; ?>

			<form name="bulk_form" id="bulk_form" method="POST" action="<?php echo $main_url; ?>index.php?selected_page=<?php echo $selected_page; ?>&type=<?php echo $type; ?>">
				<div id="bulk_form_container" class="vocab_search ui-tabs ui-widget">
					<input type="hidden" name="bulk_action" id="bulk_action" value="">
					<input type="hidden" name="bulk_target" id="bulk_target" value="">

					<label class="vocab_search_form" style="margin-right: 7px;">Bulk Actions:</label>
					<button type="button" name="bulk_selectall" id="bulk_selectall" title="Select all messages on this page">Select All</button>
					<button type="button" name="bulk_selectnone" id="bulk_selectnone" title="De-select all messages on this page">Select None</button>

					<?php if ($type != ASSIGNED_STATUS) { ?>
					<div class="emsa_toolbar_separator"></div>
					<button type="button" name="bulk_retry" id="bulk_retry" title="Re-process all selected messages" disabled>Retry Selected Messages</button>
					<?php } ?>

					<?php if (checkPermission(URIGHTS_ACTION_MOVE) && (1 == 0)) { ?>
					<div class="emsa_toolbar_separator"></div>
					<button type="button" name="bulk_move" id="bulk_move" title="Move all selected messages to another queue" disabled>Move Selected Messages</button>
					<?php } ?>

				</div>
			</form>

<?php

//error_log(">>>> In emsa/index.php handling emsa actions");

	// handle any edit/move/retry/delete actions...
	include_once 'emsa_actions.php';

	#debug echo 'starting list... '.round(abs(microtime(true)-$start_time), 5).'<br>';

	// for pending, iterate through laboratories & show all messages for that lab in the 'pending' queue
	if ($type == PENDING_STATUS) {
		$pend_labs_qry = "SELECT DISTINCT s.lab_id AS id, l.ui_name AS ui_name FROM ".$my_db_schema."system_messages s
			INNER JOIN ".$my_db_schema."structure_labs l ON ((s.lab_id = l.id) AND (s.final_status = 12) AND ((s.deleted IS NULL) OR (s.deleted != 1))) ORDER BY ui_name;";
		$pend_labs_rs = @pg_query($host_pa, $pend_labs_qry);
		if ($pend_labs_rs) {
			if (pg_num_rows($pend_labs_rs) > 0) {
				while ($pend_labs_row = @pg_fetch_object($pend_labs_rs)) {
					unset($pending_group_labid);
					unset($pending_group_labname);
					$pending_group_labid = $pend_labs_row->id;
					$pending_group_labname = $pend_labs_row->ui_name;
					include 'lib/emsa_pending_lists.php';
				}
			} else {
				highlight("No Pending messages found!");
			}
		} else {
			suicide("Could not retrieve laboratories from database.", 1);
		}
		@pg_free_result($pend_labs_rs);
	} else {
		if(($type == ENTRY_STATUS) || ($type == EXCEPTIONS_STATUS) || ($type == QA_STATUS)){
			if ($view==2 || $view==0) {
				include 'lib/emsa_immediate_lists.php';
			}
		}

		if (($view == 1) || ($view == 0)) {
			include 'lib/emsa_lists.php';
		}
	}

	if(($type == ENTRY_STATUS) || ($type == EXCEPTIONS_STATUS) || ($type == QA_STATUS)){
?>

<div id="no_messages" class="import_widget ui-widget import_error ui-state-highlight ui-corner-all" style="display: none; padding: 5px;"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;">No messages found!</p></div>

<script type="text/javascript">
	$(function() {
		if ($(".emsa-list-nonimmediate").is(":hidden") && $(".emsa-list-immediate").is(":hidden")) {
			$("#no_messages").show();
		}
	});
</script>
<?php
	}
	#debug echo 'done with list... '.round(abs(microtime(true)-$start_time), 5).'<br>';
?>
