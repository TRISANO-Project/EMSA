<?php

	include '../includes/app_config.php';
	include WEBROOT_URL.'/includes/address_functions.php';
	
	session_write_close(); // done writing to session; prevent blocking
	
	$this_id = intval(trim($_POST['id']));
	$xml = getValidAddress($this_id);

?>
<fieldset class="emsa-list ui-widget ui-widget-content ui-corner-all">
	<legend class="emsa-list-legend ui-widget-content ui-corner-all">Validated Address</legend>
		<div class="emsa_results_container">
			<p style="font-family: Consolas, 'Courier New', Courier; font-size: 1.2em; line-height: 1.1em; font-weight: 700; margin-left: 20px; color: darkgreen;"><?php echo $xml['valid_address']; ?></p>
			<input type="hidden" name="validated_address" id="validated_address" value="<?php echo $xml['valid_address']; ?>" />
		</div>
</fieldset>

<fieldset class="emsa-list ui-widget ui-widget-content ui-corner-all">
	<legend class="emsa-list-legend ui-widget-content ui-corner-all">Original Message Address</legend>
		<div class="emsa_results_container">
			<p style="font-family: Consolas, 'Courier New', Courier; font-size: 1.2em; line-height: 1.1em; font-weight: 700; margin-left: 20px; color: navy;">
				<?php echo $xml['street_name']; ?><br>
				<?php echo $xml['city']; ?>, <?php echo $xml['state']; ?> <?php echo $xml['postal_code']; ?><br>
			</p>
		</div>
</fieldset>