<?php

	// prevent caching...
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');
	
	include '../includes/app_config.php';
    include_once WEBROOT_URL.'/includes/emsa_functions.php';
       
	session_write_close(); // done writing to session; prevent blocking
	
	unset($clean);
	$nedss_url = $props['trisano_url'];;
	if (isset($_GET['event_id']) && filter_var($_GET['event_id'], FILTER_VALIDATE_INT)) {
		$clean['event_id'] = filter_var($_GET['event_id'], FILTER_SANITIZE_NUMBER_INT);
	}
	
	if (isset($clean['event_id']) && $clean['event_id'] > 0) {
		$clean['event_type'] = getTrisanoEventTypeById($clean['event_id']);
	}

	$type_tag = 'cmrs/';
	if ($clean['event_type'] == 'AssessmentEvent') {
		$type_tag = 'aes/';
	} elseif ($clean['event_type'] == 'ContactEvent') {
		$type_tag = 'contact_events/';
	}
	if(isPropertyTrue($props['show_not_edit'])) {
		$nedss_url .= $type_tag.$clean['event_id'];
	} else {
		$nedss_url .= $type_tag.$clean['event_id'].'/edit/';
	}
	header('Location: '.$nedss_url);

?>