	<div class="emsa_results_container">
	<table class="emsa_results">
		<thead>
			<tr>
			<?php
				$show_clia = false;
				if ($props['emsa_environment'] == "SNHD" && ($type == ENTRY_STATUS || $type == GRAY_STATUS)) {
				$show_clia = true;
			?>
				<th style="width: 22%;">Name</th>
				<th style="width: 6%;">Gender</th>
				<th style="width: 8%;">D.O.B.</th>
				<th style="width: 35%;">Address</th>
			<?php } else { ?>
				<th style="width: 24%;">Name</th>
				<th style="width: 6%;">Gender</th>
				<th style="width: 8%;">D.O.B.</th>
				<th style="width: 41%;">Address</th>
			<?php } ?>
				<th style="width: 13%;">Telephone #</th>
			<?php if ($type == ASSIGNED_STATUS) { ?>
				<th style="width: 9%;">Date Assigned</th>
			<?php } else { ?>
				<th style="width: 9%;">Date Reported</th>
			<?php } ?>
			<?php if ($show_clia) { ?>
				<th style="width: 7%;">CLIA</th>
			<?php } ?>
			</tr>
		</thead>
		<tbody>
		<?php
			if ($numrows == 0) {
				echo "<tr><td colspan=\"6\"><em>No events found</em></td></tr>";
			} else {
				while($start < count($results)){
					#debug echo 'starting getEmsaDetail... '.round(abs(microtime(true)-$start_time), 5);
					$result = $results[$start];
					$event_details = getEmsaDetail($result['id']);

					$_ux_disease = htmlentities($event_details['disease'], ENT_QUOTES, "UTF-8");
					if (!empty($_ux_disease)) {
						$_ux_disease = '<small style="text-decoration:underline;margin-left:12px;">' . $_ux_disease. '</small>';
					}

					#debug echo ' -- '.round(abs(microtime(true)-$start_time), 5).' done<br>';
					$this_searchclass = '';
					$this_searchclass .= (!($type == QA_STATUS) && !($type == ENTRY_STATUS) && !(($type == ASSIGNED_STATUS) && (checkPermission(URIGHTS_TAB_QA)))) ? ' emsa_nopeoplesearch' : '';
					$this_searchclass .= ($type != PENDING_STATUS) ? ' emsa_noeventsearch' : '';
					printf("\n<tr class=\"emsa_dup%s%s\" id=\"%s\" title=\"%s\"><td><button type=\"button\" class=\"emsa_close\" id=\"emsa_close_%s\" style=\"display: none; margin-right: 5px;\">Close</button><input type=\"hidden\" id=\"fname_%s\" value=\"%s\" /><input type=\"hidden\" id=\"lname_%s\" value=\"%s\" /><input type=\"hidden\" id=\"bday_%s\" value=\"%s\" />".
						"<input type=\"hidden\" id=\"condition_%s\" value=\"%s\" />%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td>".
						($show_clia ? "<td>%s</td>" : "").
						"<td nowrap>%s</td></tr>",
						$this_searchclass,
						(((($type == ENTRY_STATUS) || ($type == QA_STATUS) || ($type == EXCEPTIONS_STATUS)) && isElrMessageOverdue($result['created_at_long'])) ? " emsa_overdue" : ""),
						$result['id'],
						htmlentities($result['disease'], ENT_QUOTES, "UTF-8"),
						$result['id'],
						$result['id'], htmlentities($result['fname'], ENT_QUOTES, "UTF-8"),
						$result['id'], htmlentities($result['lname'], ENT_QUOTES, "UTF-8"),
						$result['id'], htmlentities($result['birth_date'], ENT_QUOTES, "UTF-8"),
						$result['id'], htmlentities($event_details['disease'], ENT_QUOTES, "UTF-8"),
						htmlentities($result['full_name'], ENT_QUOTES, "UTF-8") . ' ' . $_ux_disease,
						htmlentities($result['sex'], ENT_QUOTES, "UTF-8"),
						htmlentities($result['birth_date'], ENT_QUOTES, "UTF-8"),
						htmlentities(sprintf("%s%s %s %s %s %s", $result['street'], ((strlen(trim($result['street'])) > 0)? "," : ""), ((strlen(trim($result['unit'])) > 0)? 'Unit '.$result['unit'].',' : ''), $result['city'], $result['zip'], ((strlen(trim($result['county'])) > 0) ? '['.$result['county'].' County]' : '')), ENT_QUOTES, "UTF-8"),
						htmlentities($result['phone'], ENT_QUOTES, "UTF-8"),
						(($type == ASSIGNED_STATUS) ? htmlentities($result['assigned_at'], ENT_QUOTES, "UTF-8") : htmlentities($result['created_at'], ENT_QUOTES, "UTF-8")),
						htmlentities($event_details['clia'], ENT_QUOTES, "UTF-8")
					);
					printf("\n<tr class=\"emsa_dupsearch\" id=\"dupsearch_%s\"><td colspan=\"6\">", $result['id']);
					printf("<div class=\"emsa_dupsearch_tabset\" id=\"emsa_dupsearch_%s_tabset\">", $result['id']);
					echo "<ul>";
					printf("<li%s><a href=\"#emsa_dupsearch_%s_tab1\">People Search Results</a></li>", ((!($type == ENTRY_STATUS) && !($type == QA_STATUS) && !(($type == ASSIGNED_STATUS) && (checkPermission(URIGHTS_TAB_QA)))) ? " class=\"emsa_nosearch\"" : ""), $result['id']);
					printf("<li%s><a href=\"#emsa_dupsearch_%s_tab8\">TriSano Event Search</a></li>", (($type != PENDING_STATUS) ? " class=\"emsa_nosearch\"" : ""), $result['id']);
					if (checkPermission(URIGHTS_TAB_FULL)) {
						printf("<li><a href=\"#emsa_dupsearch_%s_tab2\">Full Lab</a></li>", $result['id']);
					}
					if ($type == EXCEPTIONS_STATUS || ($props['emsa_environment'] == "SNHD" && $type == ENTRY_STATUS)) {
						printf("<li><a href=\"#emsa_dupsearch_%s_tab_override\">Manual Override</a></li>", $result['id']);
					}
					if (checkPermission(URIGHTS_TAB_AUDIT) || ($type == PENDING_STATUS)) {
						printf("<li><a href=\"#emsa_dupsearch_%s_tab7\">Audit Log</a></li>", $result['id']);
					}
					if (checkPermission(URIGHTS_TAB_ERROR)) {
						printf("<li><a href=\"#emsa_dupsearch_%s_tab3\">Error Flags</a></li>", $result['id']);
					}
					if (checkPermission(URIGHTS_TAB_HL7)) {
						printf("<li><a href=\"#emsa_dupsearch_%s_tab4\">HL7</a></li>", $result['id']);
					}
					if (checkPermission(URIGHTS_TAB_XML)) {
						printf("<li><a href=\"#emsa_dupsearch_%s_tab5\">Original Master XML</a></li>", $result['id']);
						printf("<li><a href=\"#emsa_dupsearch_%s_tab6\">Original NEDSS XML</a></li>", $result['id']);
					}
					//if (($type == PENDING_STATUS || $type == QA_STATUS || $type == EXCEPTIONS_STATUS) && checkPermission(URIGHTS_TAB_QA)) {
					if (checkPermission(URIGHTS_TAB_QA)) {
						printf("<li><a href=\"#emsa_dupsearch_%s_tab9\">QA Tracking</a></li>", $result['id']);
					}
					echo "</ul>";

					printf("<div id=\"emsa_dupsearch_%s_tab1\" class=\"emsa_tabset_content emsa_tabset_content_ps%s\"><img style=\"vertical-align: bottom;\" src=\"%s/img/ajax-loader.gif\" height=\"16\" width=\"16\" border=\"0\" /> Searching...</div>", $result['id'], ((!($type == ENTRY_STATUS) && !($type == QA_STATUS) && !(($type == ASSIGNED_STATUS) && (checkPermission(URIGHTS_TAB_QA)))) ? " emsa_nosearch" : ""), MAIN_URL);
					printf("<div id=\"emsa_dupsearch_%s_tab8\" class=\"emsa_tabset_content emsa_tabset_content_ps%s\"><img style=\"vertical-align: bottom;\" src=\"%s/img/ajax-loader.gif\" height=\"16\" width=\"16\" border=\"0\" /> Searching...</div>", $result['id'], (($type != PENDING_STATUS) ? " emsa_nosearch" : ""), MAIN_URL);
					if (checkPermission(URIGHTS_TAB_FULL)) {
						printf("<div id=\"emsa_dupsearch_%s_tab2\" class=\"emsa_tabset_labdetail\">", $result['id']);
						include 'emsa/detail_view2.php';
						echo "</div>";
					}
					if ($type == EXCEPTIONS_STATUS || ($props['emsa_environment'] == "SNHD" && $type == ENTRY_STATUS)) {
						printf("<div id=\"emsa_dupsearch_%s_tab_override\">", $result['id']);
						include 'emsa/manual_override_form.php';
                        if ($type == EXCEPTION_STATUS) {
                            include 'emsa/exception_view_current.php';
                        }
						echo "</div>";
					}
					if (checkPermission(URIGHTS_TAB_AUDIT) || ($type == PENDING_STATUS)) {
						printf("<div id=\"emsa_dupsearch_%s_tab7\">%s</div>", $result['id'], displayAuditLogById($result['id']));
					}
					if (checkPermission(URIGHTS_TAB_ERROR)) {
						printf("<div id=\"emsa_dupsearch_%s_tab3\">", $result['id']);
						include 'emsa/exception_view_current.php';
						include 'emsa/exception_view_history.php';
						echo "</div>";
					}
					if (checkPermission(URIGHTS_TAB_HL7)) {
						printf("<div id=\"emsa_dupsearch_%s_tab4\" class=\"emsa_tabset_content\"><textarea disabled=\"disabled\" class=\"ui-corner-all\" style=\"background-color: aliceblue; font-family: Consolas, 'Courier New', serif; width: 95%%; height: 20em;\" id=\"%s_hl7\">%s</textarea></div>", $result['id'], $result['id'], htmlentities(getMessageFieldFormatted($result['id'], 5), ENT_QUOTES, "UTF-8"));
					}
					if (checkPermission(URIGHTS_TAB_XML)) {
						printf("<div id=\"emsa_dupsearch_%s_tab5\" class=\"emsa_tabset_content\"><pre id=\"%s_mxml\">%s</pre></div>", $result['id'], $result['id'], htmlentities(getMessageFieldFormatted($result['id'], 4), ENT_QUOTES, "UTF-8"));
						printf("<div id=\"emsa_dupsearch_%s_tab6\" class=\"emsa_tabset_content\"><pre id=\"%s_nxml\">%s</pre></div>", $result['id'], $result['id'], htmlentities(getMessageFieldFormatted($result['id'], 1), ENT_QUOTES, "UTF-8"));
					}
					//if (($type == PENDING_STATUS || $type == QA_STATUS || $type == EXCEPTIONS_STATUS) && checkPermission(URIGHTS_TAB_QA)) {
					if (checkPermission(URIGHTS_TAB_QA)) {
						printf("<div id=\"emsa_dupsearch_%s_tab9\">", $result['id']);
						include 'emsa/qa_tracking_tab.php';
						echo "</div>";
					}
					echo "</div></td></tr>";
					$start++;
				}
			}

		?>
		</tbody>
	</table>
<?php
	/******  build the pagination links ******/
	// range of num links to show
	$range = 5;

	echo "<div class=\"emsa_paging\">";

	if (count($results) > 0) {
		printf("<p><strong>Page %d of %d</strong></p>", $currentpage, $totalpages);
	} else {
		echo "<p><strong><em>No labs found</em></strong></p>";
	}

	// if not on page 1, don't show back links
	if ($totalpages > 1) {
		if ($currentpage != 1) {
			// show << link to go back to page 1
			echo "<a class='paging_link_first' title='First Page' href='{$main_url}?selected_page=".$selected_page."&type=".$type."&currentpage=1'>&lt;&lt;</a>";

			// get previous page num
			$prevpage = $currentpage - 1;

			// show < link to go back to 1 page
			echo "<a class='paging_link_previous' title='Previous Page' href='{$main_url}?selected_page=".$selected_page."&type=".$type."&currentpage=$prevpage'>&lt;</a>";
		}

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
			// if it's a valid page number...
			if (($x > 0) && ($x <= $totalpages)) {
				// if we're on current page...
				if ($x == $currentpage) {
					// 'highlight' it but don't make a link
					echo '<form style="display: inline; margin: 0px;" method="GET" action="'.$main_url.'">';
					echo '<input type="hidden" name="selected_page" value="'.$selected_page.'">';
					echo '<input type="hidden" name="type" value="'.$type.'">';
					echo '<input type="text" title="Go to page..." name="currentpage" style="font-family: \'Open Sans\' !important; font-weight: 600; background-color: lightcyan; text-align: center; width: 2em; padding: .4em;" class="ui-corner-all" value="'.$x.'">';
					echo '</form>';
					//echo "<span class='paging_link_current ui-state-highlight'>$x</span>";
				} else {
					// if not current page... make it a link
					echo "<a class='paging_link' title='Page ".$x."' href='{$main_url}?selected_page=".$selected_page."&type=".$type."&currentpage=$x'>$x</a>";
				}
			}
		}

		// if not on last page, show forward and last page links
		if ($currentpage != $totalpages) {
			// get next page
			$nextpage = $currentpage + 1;

			// echo forward link for next page
			echo "<a class='paging_link_next' title='Next Page' href='{$main_url}?selected_page=".$selected_page."&type=".$type."&currentpage=$nextpage'>&gt;</a>";

			// echo forward link for lastpage
			echo "<a class='paging_link_last' title='Last Page' href='{$main_url}?selected_page=".$selected_page."&type=".$type."&currentpage=$totalpages'>&gt;&gt;</a>";
		}
	}
	echo "</div>";
	/****** end build pagination links ******/
?>
