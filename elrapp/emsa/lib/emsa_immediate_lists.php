<?php

	/**
	 * Calculate pagination
	 */
	$numrows = getEmsaLists($type, 0, 0, $immediate, true);

	// number of rows to show per page
	if($_REQUEST['per_page'])
		$rowsperpage = $_REQUEST['per_page'];
	else
		$rowsperpage = 25;
	
	// find out total pages
	$totalpages = ceil($numrows / $rowsperpage);

	// get the current page from session
	$currentpage = $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["current_page"];
	
	// if current page is greater than total pages...
	if ($currentpage > $totalpages) {
		// set current page to last page
		$currentpage = $totalpages;
	}

	// if current page is less than first page...
	if ($currentpage < 1) {
		// set current page to first page
		$currentpage = 1;
	}

	// the offset of the list, based on current page 
	$offset = ($currentpage - 1) * $rowsperpage;
    $start=0;

	$results = getEmsaLists($type, $offset, $rowsperpage, $immediate, false);
	
?>

<fieldset class="emsa-list emsa-list-immediate ui-widget ui-widget-content ui-corner-all">
	<legend class="emsa-list-legend ui-widget-content ui-corner-all">Immediate&nbsp;&nbsp;&nbsp;[ <?php echo intval($numrows); ?> ]</legend>
<?php
	
	include 'emsa_list_fieldset.php';
	
?>
</fieldset>
<?php
	if ($numrows == 0) {
		// if no rows in immediate list, hide the entire fieldset for UI real estate usability
?>

<script type="text/javascript">
	$(function() {
		$(".emsa-list-immediate").hide();
	});
</script>
<?php
	}
?>