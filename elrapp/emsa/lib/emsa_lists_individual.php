<?php

	$numrows = 1;
	$rowsperpage = DEFAULT_ROWS_PER_PAGE;
	
	// find out total pages
	$totalpages = 1;

	// get the current page from session
	$currentpage = 1;
	
	// the offset of the list, based on current page 
	$offset = 0;
    $start = 0;
	
	$results = getEmsaLists($type, $offset ,$rowsperpage, 2, false, $exceptions, $task_id, null, $focus_id);
	
?>

<fieldset class="emsa-list ui-widget ui-widget-content ui-corner-all">
	<legend class="emsa-list-legend ui-widget-content ui-corner-all">Message ID# <?php echo trim($focus_id); ?> [<?php echo getEmsaQueueName($type);?>]</legend>
<?php
	
	include 'emsa_list_fieldset.php';
	
?>
</fieldset>