<?php

	// prevent caching...
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");

	include '../../includes/app_config.php';
	include WEBROOT_URL.'/includes/address_functions.php';
	
	session_write_close(); // done writing to session; prevent blocking
	
	unset($clean_vahandler);

	$clean_vahandler['original'] = ((isset($_POST['original']) && (intval(trim($_POST['original'])) == 1)) ? TRUE : FALSE);
	$clean_vahandler['address'] = ((isset($_POST['address']) && (strlen(trim($_POST['address'])) > 0)) ? trim($_POST['address']) : FALSE);
	$clean_vahandler['id'] = ((isset($_POST['id']) && (intval(trim($_POST['id'])) > 0)) ? intval(trim($_POST['id'])) : FALSE);
	$clean_vahandler['type'] = ((isset($_POST['type']) && (intval(trim($_POST['type'])) > 0)) ? intval(trim($_POST['type'])) : FALSE);
	
	if (( ($clean_vahandler['address'] && !$clean_vahandler['original']) || $clean_vahandler['original']) && $clean_vahandler['id']) {
		if ($clean_vahandler['original']) {
			// use original message's address
			$sql="UPDATE ".$my_db_schema."system_messages SET address_is_valid=1 WHERE id=".$clean_vahandler['id'];
			if (@get_db_result_set($host_pa,$sql)) {
				header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
				exit;
			} else {
				header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error", TRUE, 500);
				exit;
			}
		} else {
			// update master xml with validated address
			if (@updateValidatedAddress($clean_vahandler['id'], $clean_vahandler['address'])) {
				// log entry in audit log for successful address update
				$audit_status_id = $clean_vahandler['type'];
				$audit_record_id = $clean_vahandler['id'];
				$audit_action_id = 16;
				include_once '../../includes/audit_log.php';
				header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
				exit;
			} else {
				header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error", TRUE, 500);
				exit;
			}
		}
	} else {
		// missing address/id, return HTTP 400 Bad Request to client
		header($_SERVER['SERVER_PROTOCOL'] . " 400 Bad Request", TRUE, 400);
		exit;
	}
	
?>