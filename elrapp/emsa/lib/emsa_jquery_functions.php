			<script type="text/javascript">
				function semiAutomatedEntry(thisEvent, cmrMode) {
					try {
						var semiAutoOriginal = jQuery.parseJSON($("#semi_auto_original_"+thisEvent).val());
					} catch (err) {
						var semiAutoOriginal = {};
					}
					
					if (semiAutoOriginal) {
						var changedFields = 0;
						var semiAutoEdits = {};
							semiAutoEdits.message			= thisEvent;
							semiAutoEdits.disease			= $("#cultureentry_"+thisEvent+"__disease").val();
							semiAutoEdits.organism			= $("#cultureentry_"+thisEvent+"__organism").val();
							semiAutoEdits.jurisdiction		= $("#cultureentry_"+thisEvent+"__jurisdiction").val();
							semiAutoEdits.state_case_status	= $("#cultureentry_"+thisEvent+"__state_case_status").val();
							semiAutoEdits.test_type			= $("#cultureentry_"+thisEvent+"__test_type").val();
							semiAutoEdits.specimen			= $("#cultureentry_"+thisEvent+"__specimen").val();
							semiAutoEdits.testresult		= $("#cultureentry_"+thisEvent+"__testresult").val();
							semiAutoEdits.resultvalue		= $("#cultureentry_"+thisEvent+"__resultvalue").val();
							semiAutoEdits.units				= $("#cultureentry_"+thisEvent+"__units").val();
							semiAutoEdits.comment			= $("#cultureentry_"+thisEvent+"__comment").val();
							semiAutoEdits.teststatus		= $("#cultureentry_"+thisEvent+"__teststatus").val();
							semiAutoEdits.hospitalized		= $("#cultureentry_"+thisEvent+"__hospitalized").val();
							
						for (var prop in semiAutoEdits) {
							if (semiAutoEdits.hasOwnProperty(prop)) {
								if (semiAutoEdits[prop] !== semiAutoOriginal[prop]) {
									changedFields++;
								}
							}
						}
						
						if (!(parseInt(semiAutoEdits.disease) > 0)) {
							alert('No Condition specified!\n\nPlease select a Condition on the "Full Lab" tab & try again.');
							return false;
						}
						
						if (changedFields > 0) {
							// make ajax request to update master/nedss xml
							$.post("emsa/lib/ajax_semiauto_entry.php", semiAutoEdits)
							.done(function(semiAutoSaveData) {
								// changes saved, continue assigning message
								if (cmrMode == 'add_new_cmr') {
									assignMessageAddNewCMR(thisEvent);
								} else {
									assignMessageUpdateCMR(thisEvent);
								}
							})
							.fail(function(xhr, err) {
								alert('An error occurred while attempting to save the semi-automated lab entry changes.\n\nMessage cannot be processed at this time.');
								return false;
							});
						} else {
							// no changes made, continue assigning message
							if (cmrMode == 'add_new_cmr') {
								assignMessageAddNewCMR(thisEvent);
							} else {
								assignMessageUpdateCMR(thisEvent);
							}
						}
					} else {
						// this message not using semi-automated entry, continue assigning message
						if (cmrMode == 'add_new_cmr') {
							assignMessageAddNewCMR(thisEvent);
						} else {
							assignMessageUpdateCMR(thisEvent);
						}
					}
				}
				
				function assignMessageAddNewCMR(thisEvent) {
					var thisPersonsArray = new Array();
					$.each($("input[name='use_person["+thisEvent+"][]']:checked"), function() {
						thisPersonsArray.push($(this).val());
					});
					if (thisPersonsArray.length > 0) {
						$("#confirm_addnew_dialog").dialog('option', 'buttons', {
								"Yes, add results to a new person" : function() {
									$("#emsa_cmraction_"+thisEvent).val("addnew");
									$("#cmr_"+thisEvent).submit();
								},
								"No, add results to the selected persons" : function() {
									$("#emsa_cmraction_"+thisEvent).val("update");
									$("#match_persons_"+thisEvent).val(thisPersonsArray.join("|"));
									$("#cmr_"+thisEvent).submit();
								},
								"Do nothing, I'll pick new options" : function() {
									$(this).dialog("close");
								}
						});

						$("#confirm_addnew_dialog").dialog("open");
					} else {
						$("#emsa_cmraction_"+thisEvent).val("addnew");
						$("#cmr_"+thisEvent).submit();
					}
				}
				
				function assignMessageUpdateCMR(thisEvent) {
					var thisPersonsArray = new Array();
					$.each($("input[name='use_person["+thisEvent+"][]']:checked"), function() {
						thisPersonsArray.push($(this).val());
					});
					if (thisPersonsArray.length < 1) {
						alert("No person(s) selected to update!\n\nPlease choose at least one person to add this event to.");
						return false;
					} else {
						$("#emsa_cmraction_"+thisEvent).val("update");
						$("#match_persons_"+thisEvent).val(thisPersonsArray.join("|"));
						$("#cmr_"+thisEvent).submit();
					}
				}
				
				$(function() {
					$(".vocab_filter_selectall").click(function() {
						var thisFilter = $(this).attr("rel");
						$("div.vocab_filter_checklist[rel='"+thisFilter+"']").find($(":input")).each(function() {
							if (!$(this).is(':checked')) {
								$(this).trigger('click');
							}
						});
					});
					
					$(".vocab_filter_selectnone").click(function() {
						var thisFilter = $(this).attr("rel");
						$("div.vocab_filter_checklist[rel='"+thisFilter+"']").find($(":input")).each(function() {
							if ($(this).is(':checked')) {
								$(this).trigger('click');
							}
						});
					});
					
					$("#confirm_addnew_dialog").dialog({
						autoOpen: false,
						modal: true,
						draggable: false,
						resizable: false
					});
					
					$(".paging_link").button();
					$(".paging_link_current").button({
						disabled: true
					});
					$(".paging_link_first").button({
						icons: {
							primary: "ui-icon-arrowthickstop-1-w"
						},
						text: false
					});
					$(".paging_link_previous").button({
						icons: {
							primary: "ui-icon-arrowthick-1-w"
						},
						text: false
					});
					$(".paging_link_next").button({
						icons: {
							primary: "ui-icon-arrowthick-1-e"
						},
						text: false
					});
					$(".paging_link_last").button({
						icons: {
							primary: "ui-icon-arrowthickstop-1-e"
						},
						text: false
					});
					
					$(".override_new_cmr").button({
						icons: {
							primary: "ui-icon-elraddcmr"
						}
					}).click(function(e) {
						e.preventDefault();
						var thisEvent = $(this).val();
						$("#override_emsa_cmraction_"+thisEvent).val("addnew");
						$("#override_cmr_"+thisEvent).submit();
					});
					
					$(".override_update_cmr").button({
						icons: {
							primary: "ui-icon-elrupdatecmr"
						}
					}).click(function(e) {
						e.preventDefault();
						var thisEvent = $(this).val();
						var overrideId = $("#override_event_"+thisEvent).val();
						if (overrideId == "") {
							alert('Please enter an Event ID and try again.');
							return false;
						} else {
							$("#override_emsa_cmraction_"+thisEvent).val("update");
							$("#override_cmr_"+thisEvent).submit();
						}
					});
				<?php
					if (isset($focus_id)) {
				?>
					if ($("#<?php echo $focus_id; ?>").length) {
						setTimeout(function() {
							$("#<?php echo $focus_id; ?>").trigger('click');
							$("#emsa_dupsearch_<?php echo $focus_id; ?>_tabset").tabs("option", "selected", 2);
							//deprecated... var container = (($.browser.msie || $.browser.mozilla) ? $("body,html") : $("body"));
							var container = $("body,html");
							var scrollTo = $("#<?php echo $focus_id; ?>");
							container.scrollTop(
								scrollTo.offset().top - container.offset().top + container.scrollTop()
							);
						}, 10);
					}
				<?php
					}
				?>
					$("#toggle_filters").button({
						icons: {
							secondary: "ui-icon-triangle-1-n"
						}
					}).click(function() {
						$(".vocab_filter").toggle("blind");
						var objIcons = $(this).button("option", "icons");
						if (objIcons['secondary'] == "ui-icon-triangle-1-s") {
							$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-n"});
							$(this).button("option", "label", "Hide Advanced Filters");
							$("#addnew_form").hide();
							$("#addnew_button").show();
						} else {
							$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
							$(this).button("option", "label", "Show Advanced Filters");
						}
					});
					
					$(".vocab_filter").hide();
					$("#toggle_filters").button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
					$("#toggle_filters").button("option", "label", "Show Advanced Filters");
					
					$("#toggle_bulk").button({
						icons: {
							secondary: "ui-icon-triangle-1-n"
						}
					}).click(function() {
						$("#bulk_form").toggle("blind");
						var objIcons = $(this).button("option", "icons");
						if (objIcons['secondary'] == "ui-icon-triangle-1-s") {
							$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-n"});
							$(this).button("option", "label", "Hide Bulk Message Actions");
							$("#addnew_form").hide();
							$("#addnew_button").show();
						} else {
							$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
							$(this).button("option", "label", "Show Bulk Message Actions");
						}
					});
					
					$("#bulk_form").hide();
					$("#toggle_bulk").button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
					$("#toggle_bulk").button("option", "label", "Show Bulk Message Actions");
					
					$("#clear_filters").button({
						icons: {
							primary: "ui-icon-elrcancel"
						}
					}).click(function() {
						$(".pseudo_select").removeAttr("checked");
						$(".pseudo_select_label").removeClass("pseudo_select_on");
						$("#search_form")[0].reset();
						$("#q").val("").blur();
						$("#f_evalue").val("").blur();
						$("#sort").val("").blur();
						$("#search_form").submit();
					});
					
					$("#q_go").button({
						icons: {
							primary: "ui-icon-elrsearch"
						}
					}).click(function(){
						$("#search_form").submit();
					});
					
					$("#apply_filters").button({
						icons: {
							primary: "ui-icon-elroptions"
						}
					}).click(function(){
						$("#search_form").submit();
					});
					
					$(".pseudo_select").change(function() {
						$(this).closest('label').toggleClass('pseudo_select_on');
					});
					
					$("#q").addClass("search_empty").val("Enter search terms...").click(function() {
						var search_val = $.trim($("#q").val());
						if (search_val == "Enter search terms...") {
							$(this).removeClass("search_empty").val("");
						}
					}).blur(function() {
						var search_val_ln = $.trim($("#q").val()).length;
						if (search_val_ln == 0) {
							$("#q").addClass("search_empty").val("Enter search terms...");
						}
					});
					
					<?php
						if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_display"])) {
							if ($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_display"] != "Enter search terms...") {
					?>
					$("#q").removeClass("search_empty").val("<?php echo $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_display"]; ?>");
					<?php
							}
						}
					?>
					
					$(".emsa_btn_cultureentry").button({
						icons: {
							primary: "ui-icon-elrbacteria"
						}
					}).click(function() {
						var editId = $(this).val();
						var originalObj = {};
							originalObj.message				= editId;
							originalObj.disease				= $("#cultureentry_"+editId+"__disease").val();
							originalObj.organism			= $("#cultureentry_"+editId+"__organism").val();
							originalObj.jurisdiction		= $("#cultureentry_"+editId+"__jurisdiction").val();
							originalObj.state_case_status	= $("#cultureentry_"+editId+"__state_case_status").val();
							originalObj.test_type			= $("#cultureentry_"+editId+"__test_type").val();
							originalObj.specimen			= $("#cultureentry_"+editId+"__specimen").val();
							originalObj.testresult			= $("#cultureentry_"+editId+"__testresult").val();
							originalObj.resultvalue			= $("#cultureentry_"+editId+"__resultvalue").val();
							originalObj.units				= $("#cultureentry_"+editId+"__units").val();
							originalObj.comment				= $("#cultureentry_"+editId+"__comment").val();
							originalObj.teststatus			= $("#cultureentry_"+editId+"__teststatus").val();
							originalObj.hospitalized		= $("#cultureentry_"+editId+"__hospitalized").val();
						var originalJson = JSON.stringify(originalObj);
						
						$("#semi_auto_original_"+editId).val(originalJson);
						$("#emsa_btn_edit_"+editId).button({ disabled: true });
						$("#emsa_btn_retry_"+editId).button({ disabled: true });
						$(".emsa_cultureentry_"+editId).prop("disabled", false);
						$(".cultureentry_container_"+editId+" input, .cultureentry_container_"+editId+" select, .cultureentry_container_"+editId+" textarea").addClass("cultureentry_active").addClass("ui-corner-all");
					});
					
					$(".emsa_btn_edit").button({
						icons: {
							primary: "ui-icon-elredit"
						}
					}).click(function() {
						var editId = $(this).val();
						if ( ($("#emsa_btn_flagdeother_"+editId).length || $("#emsa_btn_flagdeerror_"+editId).length || $("#emsa_btn_flagmandatory_"+editId).length || $("#emsa_btn_flagvocabcoding_"+editId).length || $("#emsa_btn_flagmqf_"+editId).length || $("#emsa_btn_flagfixduplicate_"+editId).length) && !($("#emsa_btn_flagdeother_"+editId).hasClass("emsa_btn_flagdeother_off") || $("#emsa_btn_flagdeerror_"+editId).hasClass("emsa_btn_flagdeerror_off") || $("#emsa_btn_flagmandatory_"+editId).hasClass("emsa_btn_flagmandatory_off") || $("#emsa_btn_flagvocabcoding_"+editId).hasClass("emsa_btn_flagvocabcoding_off") || $("#emsa_btn_flagmqf_"+editId).hasClass("emsa_btn_flagmqf_off") || $("#emsa_btn_flagfixduplicate_"+editId).hasClass("emsa_btn_flagfixduplicate_off") || $("#emsa_btn_flagneedfix_"+editId).hasClass("emsa_btn_flagneedfix_off"))) {
							alert("Cannot retry message.\n\nNo 'Quality Check' reason selected.");
							return false;
						} else {
							$("#emsa_action_"+editId).val("edit");
							$("#emsa_actions_"+editId).submit();
						}
					});
					
					$(".emsa_btn_retry").button({
						icons: {
							primary: "ui-icon-elrretry"
						}
					}).click(function() {
						var retryId = $(this).val();
						if ( ($("#emsa_btn_flagdeother_"+retryId).length || $("#emsa_btn_flagdeerror_"+retryId).length || $("#emsa_btn_flagmandatory_"+retryId).length || $("#emsa_btn_flagvocabcoding_"+retryId).length || $("#emsa_btn_flagmqf_"+retryId).length || $("#emsa_btn_flagfixduplicate_"+retryId).length) && !($("#emsa_btn_flagdeother_"+retryId).hasClass("emsa_btn_flagdeother_off") || $("#emsa_btn_flagdeerror_"+retryId).hasClass("emsa_btn_flagdeerror_off") || $("#emsa_btn_flagmandatory_"+retryId).hasClass("emsa_btn_flagmandatory_off") || $("#emsa_btn_flagvocabcoding_"+retryId).hasClass("emsa_btn_flagvocabcoding_off") || $("#emsa_btn_flagmqf_"+retryId).hasClass("emsa_btn_flagmqf_off") || $("#emsa_btn_flagfixduplicate_"+retryId).hasClass("emsa_btn_flagfixduplicate_off") || $("#emsa_btn_flagneedfix_"+retryId).hasClass("emsa_btn_flagneedfix_off"))) {
							alert("Cannot retry message.\n\nNo 'Quality Check' reason selected.");
							return false;
						} else {
							if (confirm("Are you sure you want to retry sending this event?")) {
								$("#emsa_action_"+retryId).val("retry");
								$("#emsa_actions_"+retryId).submit();
							} else {
								return false;
							}
						}
					});
					
					$(".emsa_btn_move").button({
						icons: {
							primary: "ui-icon-elrmove"
						}
					}).click(function() {
						var moveId = $(this).val();
						if ( ($("#emsa_btn_flagdeother_"+moveId).length || $("#emsa_btn_flagdeerror_"+moveId).length || $("#emsa_btn_flagmandatory_"+moveId).length || $("#emsa_btn_flagvocabcoding_"+moveId).length || $("#emsa_btn_flagmqf_"+moveId).length || $("#emsa_btn_flagfixduplicate_"+moveId).length) && !($("#emsa_btn_flagdeother_"+moveId).hasClass("emsa_btn_flagdeother_off") || $("#emsa_btn_flagdeerror_"+moveId).hasClass("emsa_btn_flagdeerror_off") || $("#emsa_btn_flagmandatory_"+moveId).hasClass("emsa_btn_flagmandatory_off") || $("#emsa_btn_flagvocabcoding_"+moveId).hasClass("emsa_btn_flagvocabcoding_off") || $("#emsa_btn_flagmqf_"+moveId).hasClass("emsa_btn_flagmqf_off") || $("#emsa_btn_flagfixduplicate_"+moveId).hasClass("emsa_btn_flagfixduplicate_off") || $("#emsa_btn_flagneedfix_"+moveId).hasClass("emsa_btn_flagneedfix_off"))) {
							alert("Cannot move message from queue.\n\nNo 'Quality Check' reason selected.");
							return false;
						} else {
							var destList = $("#system_status_id_"+moveId);
							var moveComments = $("#move_info_"+moveId);
							var destListText = $("#system_status_id_"+moveId+" :selected").text();
							if (destList.val() < 1) {
								alert("No list selected!\n\nPlease choose a list to move this event to and try again.");
								return false;
							} else {
								if (confirm("Are you sure you want to move this event to the '"+destListText+"' list?")) {
									$("#target_"+moveId).val(destList.val());
									$("#info_"+moveId).val(moveComments.val());
									$("#emsa_action_"+moveId).val("move");
									$("#emsa_actions_"+moveId).submit();
								} else {
									return false;
								}
							}
						}
					});
					
					$(".emsa_btn_delete").button({
						icons: {
							primary: "ui-icon-elrdelete"
						}
					}).click(function() {
						var delId = $(this).val();
						if (confirm("Are you sure you want to delete this event?")) {
							$("#emsa_action_"+delId).val("delete");
							$("#emsa_actions_"+delId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagcomplete").button()
					.click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to flag this event as 'Investigation Completed'?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_INVESTIGATION_COMPLETE; ?>);
							$("#emsa_action_"+moveId).val("set_flag");
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagcomplete_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to clear the 'Investigation Completed' flag for this event?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_INVESTIGATION_COMPLETE; ?>);
							$("#emsa_action_"+moveId).val("unset_flag");
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagdeerror").button().click(function() {
						var moveId = $(this).val();
						var deReason = $("#de_error_info_"+moveId).val().trim();
						if (deReason.length == 0) {
							alert("Please select a type of 'Data Entry Error' and try again.");
							return false;
						} else {
							if (confirm("Are you sure you want to set the Quality Check reason for this message as 'Data Entry Error'?")) {
								$("#target_"+moveId).val(<?php echo EMSA_FLAG_DE_ERROR; ?>);
								$("#emsa_action_"+moveId).val("set_flag");
								$("#info_"+moveId).val(deReason);
								$("#emsa_actions_"+moveId).submit();
							} else {
								return false;
							}
						}
					});
					
					$(".emsa_btn_flagdeerror_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						var deReason = $("#de_error_info_"+moveId).val().trim();
						if (confirm("Are you sure you want to un-set 'Data Entry Error' as the Quality Check reason for this message?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_DE_ERROR; ?>);
							$("#emsa_action_"+moveId).val("unset_flag");
							$("#info_"+moveId).val(deReason);
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagdeother").button().click(function() {
						var moveId = $(this).val();
						var otherReason = $("#de_other_info_"+moveId).val().trim();
						if (otherReason.length == 0) {
							alert("Please enter a reason for choosing 'Other' and try again.");
							return false;
						} else {
							if (confirm("Are you sure you want to set the Quality Check reason for this message as 'Other'?")) {
								$("#target_"+moveId).val(<?php echo EMSA_FLAG_DE_OTHER; ?>);
								$("#emsa_action_"+moveId).val("set_flag");
								$("#info_"+moveId).val(otherReason);
								$("#emsa_actions_"+moveId).submit();
							} else {
								return false;
							}
						}
					});
					
					$(".emsa_btn_flagdeother_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						var otherReason = $("#de_other_info_"+moveId).val().trim();
						if (confirm("Are you sure you want to un-set 'Other' as the Quality Check reason for this message?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_DE_OTHER; ?>);
							$("#emsa_action_"+moveId).val("unset_flag");
							$("#info_"+moveId).val(otherReason);
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagfixduplicate").button().click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to flag this event as 'Fix Duplicate'?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_FIX_DUPLICATE; ?>);
							$("#emsa_action_"+moveId).val("set_flag");
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagfixduplicate_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to clear the 'Fix Duplicate' flag for this event?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_FIX_DUPLICATE; ?>);
							$("#emsa_action_"+moveId).val("unset_flag");
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagneedfix").button().click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to flag this event as 'Needs Fixing'?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_DE_NEEDFIX; ?>);
							$("#emsa_action_"+moveId).val("set_flag");
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagneedfix_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to clear the 'Needs Fixing' flag for this event?")) {
							$("#target_"+moveId).val(<?php echo EMSA_FLAG_DE_NEEDFIX; ?>);
							$("#emsa_action_"+moveId).val("unset_flag");
							$("#emsa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagmandatory").button().click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to flag this event as 'Missing Mandatory Fields'?")) {
							$("#qa_target_"+moveId).val(<?php echo EMSA_FLAG_QA_MANDATORY; ?>);
							$("#qa_emsa_action_"+moveId).val("set_flag");
							$("#emsa_qa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagmandatory_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to clear the 'Missing Mandatory Fields' flag for this event?")) {
							$("#qa_target_"+moveId).val(<?php echo EMSA_FLAG_QA_MANDATORY; ?>);
							$("#qa_emsa_action_"+moveId).val("unset_flag");
							$("#emsa_qa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagvocabcoding").button().click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to flag this event as 'Vocab/Coding Errors'?")) {
							$("#qa_target_"+moveId).val(<?php echo EMSA_FLAG_QA_CODING; ?>);
							$("#qa_emsa_action_"+moveId).val("set_flag");
							$("#emsa_qa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagvocabcoding_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to clear the 'Vocab/Coding Errors' flag for this event?")) {
							$("#qa_target_"+moveId).val(<?php echo EMSA_FLAG_QA_CODING; ?>);
							$("#qa_emsa_action_"+moveId).val("unset_flag");
							$("#emsa_qa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagmqf").button().click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to flag this event as 'MQF Structural Errors'?")) {
							$("#qa_target_"+moveId).val(<?php echo EMSA_FLAG_QA_MQF; ?>);
							$("#qa_emsa_action_"+moveId).val("set_flag");
							$("#emsa_qa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_flagmqf_off").button({
						icons: {
							primary: "ui-icon-elrsuccess"
						}
					}).click(function() {
						var moveId = $(this).val();
						if (confirm("Are you sure you want to clear the 'MQF Structural Errors' flag for this event?")) {
							$("#qa_target_"+moveId).val(<?php echo EMSA_FLAG_QA_MQF; ?>);
							$("#qa_emsa_action_"+moveId).val("unset_flag");
							$("#emsa_qa_actions_"+moveId).submit();
						} else {
							return false;
						}
					});
					
					$(".emsa_btn_addcomment").button({
						icons: {
							primary: "ui-icon-elraddcomment"
						}
					}).click(function() {
						var moveId = $(this).val();
						var moveComments = $("#add_comment_"+moveId);
						$("#qa_target_"+moveId).val('');
						$("#qa_emsa_action_"+moveId).val("add_qa_comment");
						$("#qa_info_"+moveId).val(moveComments.val());
						$("#emsa_qa_actions_"+moveId).submit();
					});
					
					$(".emsa_btn_viewnedss").button({
						icons: {
							primary: "ui-icon-elrview"
						}
					}).click(function() {
						window.open("emsa/nedss_link.php?event_id="+$(this).val(), '_blank');
						return false;
					});
					
					<?php 
						/* Address Validation not used at this time
						 *
					// address validation ajax
					$(".validate_address_btn").button({
							icons: {
								primary: "ui-icon-elrsearchglobe"
							}
						}).click(function() {
							// loads address validation dialog
							// buttons on dialog make subsequent ajax call to execute address updates
							var validateId = $(this).val();
							$("#validate_address_dlg").remove();
							var validateAddressModal = $("<div id='validate_address_dlg' title='Validate Address'></div>");
							$.post("emsa/validate_address.php", { id: validateId }, function(validateData) {
								validateAddressModal.html(validateData).dialog({
									autoOpen: false,
									height: 400,
									width: 500,
									modal: true,
									buttons: {
										"Use Validated Address": function() {
											var validatedAddress = $("#validated_address").val();
											$.post("emsa/lib/validate_address_handler.php", { id: validateId, original: "0", address: validatedAddress, type: "<?php echo $type; ?>" }, function() {
												alert("Address updated successfully!");
												window.location.replace(window.location.href.split("&focus")[0]+"&focus="+validateId);
											}).error(function(vaHandlerErrXhr, vaHandlerErrText, vaHandlrErrThrown) {
												console.log(vaHandlerErrXhr);
												alert("Address update failed:\n\n"+vaHandlerErrText+" ("+vaHandlerErrXhr.status+": "+vaHandlrErrThrown+")");
											});
											$(this).dialog("close");
										},
										"Use Original Address": function() {
											$.post("emsa/lib/validate_address_handler.php", { id: validateId, original: "1", type: "<?php echo $type; ?>" }, function() {
												alert("Address validated!");
												window.location.replace(window.location.href.split("&focus")[0]+"&focus="+validateId);
											}).error(function(vaHandlerErrXhr, vaHandlerErrText, vaHandlrErrThrown) {
												console.log(vaHandlerErrXhr);
												alert("Address validation failed:\n\n"+vaHandlerErrText+" ("+vaHandlerErrXhr.status+": "+vaHandlrErrThrown+")");
											});
											$(this).dialog("close");
										}
									}
								}).dialog("open");
							}).error(function(vaErrXhr, vaErrText, vaErrThrown) {
								alert("Could not validate address.\n\nError Details:\n"+vaErrText+" ("+vaErrThrown+")");
							})
					});
						*/
					?>
					
					// audit log xml viewer
					$(".audit_view_xml").button({
						icons: {
							primary: "ui-icon-elrxml-small"
						}
					}).click(function() {
						// loads xml viewer dialog
						var jsonObj = jQuery.parseJSON($(this).val());
						
						$("#view_xml_dlg").remove();
						var viewXmlModal = $("<div id='view_xml_dlg' title='View XML'></div>");
						$.post("emsa/xml_audit_log.php", { id: jsonObj.id, type: jsonObj.type }, function(xmlData) {
							viewXmlModal.html(xmlData).dialog({
								autoOpen: false,
								height: 400,
								width: 500,
								modal: true,
								buttons: {
									"OK": function() {
										$(this).dialog("close");
									}
								}
							}).dialog("open");
						}).error(function(vxErrXhr, vxErrText, vxErrThrown) {
							alert("Could not view XML.\n\nError Details:\n"+vxErrText+" ("+vxErrThrown+")");
						})
					});
					
					$(".emsa_close").button({
						icons: {
							secondary: "ui-icon-arrowreturnthick-1-n"
						}
					}).click(function(e) {
						e.preventDefault();
					});
					
					$("#bulk_retry").button({
						icons: {
							primary: 'ui-icon-elrretry'
						}
					}).click(function() {
						$("#bulk_action").val("bulk_retry");
						$("#bulk_target").val("");
						$("#bulk_form").submit();
					});
					
					$("#bulk_move").button({
						icons: {
							primary: 'ui-icon-elrmove'
						}
					}).click(function() {
						$("#bulk_action").val("bulk_move");
						$("#bulk_target").val(""); //todo
						$("#bulk_form").submit();
					});
					
					$("#bulk_selectall").button({
						icons: {
							primary: 'ui-icon-elrsuccess'
						}
					}).click(function() {
						var e = jQuery.Event("click");
						e.ctrlKey = true;
						$(".emsa_dup").each(function() {
							if (!$(this).hasClass("ctrlclicked")) {
								$(this).trigger(e);
							}
						});
					});
					
					$("#bulk_selectnone").button({
						icons: {
							primary: 'ui-icon-radio-on'
						}
					}).click(function() {
						var e = jQuery.Event("click");
						e.ctrlKey = true;
						$(".emsa_dup").each(function() {
							if ($(this).hasClass("ctrlclicked")) {
								$(this).trigger(e);
							}
						});
					});
					
					$("#view_detailed_results").button({
						icons: { secondary: "ui-icon-circle-triangle-s" }
					}).click(function() {
						$("#detailed_results").toggle();
					});
					
					
					
					$(".emsa_dup").click(function(e) {
						if (e.ctrlKey) {
							// Bulk Message selection functionality
							var container = $("#bulk_form_container");
							$("#"+this.id).toggleClass("ctrlclicked");
							$("#"+this.id+" > td").toggleClass("ctrlclicked");
							if ($("#"+this.id).hasClass("ctrlclicked")) {
								$("<input>", {type: "hidden", name: "bulk_ids[]", id: "bulk_ids_"+this.id, value: this.id}).appendTo(container);
							} else {
								$("#bulk_ids_"+this.id).remove();
							}
							if ($("input[name='bulk_ids[]']").length > 0) {
								$("#bulk_retry").button("enable");
								$("#bulk_move").button("enable");
							} else {
								$("#bulk_retry").button("disable");
								$("#bulk_move").button("disable");
							}
						} else {
							// Message assignment functionality
							var thisID = "dupsearch_"+this.id;
							var theId = this.id;
							var thisTab = "emsa_dupsearch_"+this.id+"_tab1";
							var thisEventSearchTab = "emsa_dupsearch_"+this.id+"_tab8";
							var thisFname = $("#fname_"+this.id).val();
							var thisLname = $("#lname_"+this.id).val();
							var thisBday = $("#bday_"+this.id).val();
							var thisCondition = $("#condition_"+this.id).val();
							$(".emsa_dupsearch_tabset").tabs();
							$(".emsa_dupsearch:not(#"+thisID+")").hide();
							$(".emsa_dup:not(#"+this.id+") > td").removeClass("emsa_results_search_parent");
							if ($("#"+thisID).is(':hidden')) {
								if (!$("#"+this.id).hasClass("emsa_nopeoplesearch")) {
									// people search
									$.post("emsa/lib/ajax_merged_search.php", { fname: thisFname, lname: thisLname, bday: thisBday, condition: thisCondition, id: this.id, type: "<?php echo $type; ?>", selected_page: "<?php echo $selected_page; ?>", submenu: "<?php echo $submenu; ?>" })
									.done(function(thisData) {
										if (thisData.indexOf("AUTOSUBMIT") > 0) {
											// intercept siteminder login prompt within People Search Results container
											// to prevent a badly broken UI
											window.location.href = "<?php echo $main_url; ?>";
										} else {
											$("#"+thisTab).empty();
											$("#"+thisTab).html(thisData);
											$(".add_new_cmr").button({
												icons: {
													primary: "ui-icon-elraddcmr"
												}
											}).click(function(e) {
												e.preventDefault();
												var thisEvent = $(this).val();
												
												// check for semi-automated lab entry, save changes if necessary
												semiAutomatedEntry(thisEvent, 'add_new_cmr');
											});
											$(".update_cmr").button({
												icons: {
													primary: "ui-icon-elrupdatecmr"
												}
											}).click(function(e) {
												e.preventDefault();
												var thisEvent = $(this).val();
												
												// check for semi-automated lab entry, save changes if necessary
												semiAutomatedEntry(thisEvent, 'update_cmr');
											});
											$(".emsa_cmrbtn_move").button({
												icons: {
													primary: "ui-icon-elrmove"
												}
											}).click(function() {
												var moveId = $(this).val();
												var destList = $("#cmr_status_id_"+moveId);
												var moveComments = $("#cmr_move_info_"+moveId);
												var destListText = $("#cmr_status_id_"+moveId+" :selected").text();
												if (destList.val() < 1) {
													alert("No list selected!\n\nPlease choose a list to move this event to and try again.");
													return false;
												} else {
													if (confirm("Are you sure you want to move this event to the '"+destListText+"' list?")) {
														$("#cmrtarget_"+moveId).val(destList.val());
														$("#cmrinfo_"+moveId).val(moveComments.val());
														$("#emsa_cmraction_"+moveId).val("move");
														$("#cmr_"+moveId).submit();
													} else {
														return false;
													}
												}
											});
											$(".emsa_search_results tr").click(function(event) {
												if (event.target.nodeName != "INPUT") {
													var thisSearchId = this.id.substring((this.id.search("__")+2));
													var thisPersonId = thisID.substring(10);
													var thisCheckbox = $("#use_person_"+thisPersonId+"_"+thisSearchId);
													thisCheckbox.prop("checked", !thisCheckbox.prop("checked"));
												}
											});
											$(".person_match_found td input:checkbox").each(function() { this.checked = true; });  // auto-check all exact matches
										}
									});
								} else if (!$("#"+this.id).hasClass("emsa_noeventsearch")) {
									// event search
									$.post("emsa/lib/ajax_event_search.php", { fname: thisFname, lname: thisLname, bday: thisBday, condition: thisCondition, id: this.id, type: "<?php echo $type; ?>", selected_page: "<?php echo $selected_page; ?>", submenu: "<?php echo $submenu; ?>" })
									.done(function(thisData) {
										if (thisData.indexOf("AUTOSUBMIT") > 0) {
											// intercept siteminder login prompt within People Search Results container
											// to prevent a badly broken UI
											window.location.href = "<?php echo $main_url; ?>";
										} else {
											$("#"+thisEventSearchTab).empty();
											$("#"+thisEventSearchTab).html(thisData);
											$(".add_new_cmr").button({
												icons: {
													primary: "ui-icon-elraddcmr"
												}
											}).click(function(e) {
												e.preventDefault();
												var thisEvent = $(this).val();
												
												// check for semi-automated lab entry, save changes if necessary
												semiAutomatedEntry(thisEvent, 'add_new_cmr');
											});
											$(".update_cmr").button({
												icons: {
													primary: "ui-icon-elrupdatecmr"
												}
											}).click(function(e) {
												e.preventDefault();
												var thisEvent = $(this).val();
												
												// check for semi-automated lab entry, save changes if necessary
												semiAutomatedEntry(thisEvent, 'update_cmr');
											});
											$(".emsa_cmrbtn_move").button({
												icons: {
													primary: "ui-icon-elrmove"
												}
											}).click(function() {
												var moveId = $(this).val();
												var destList = $("#cmr_status_id_"+moveId);
												var moveComments = $("#cmr_move_info_"+moveId);
												var destListText = $("#cmr_status_id_"+moveId+" :selected").text();
												if (destList.val() < 1) {
													alert("No list selected!\n\nPlease choose a list to move this event to and try again.");
													return false;
												} else {
													if (confirm("Are you sure you want to move this event to the '"+destListText+"' list?")) {
														$("#cmrtarget_"+moveId).val(destList.val());
														$("#cmrinfo_"+moveId).val(moveComments.val());
														$("#emsa_cmraction_"+moveId).val("move");
														$("#cmr_"+moveId).submit();
													} else {
														return false;
													}
												}
											});
											$(".emsa_search_results tr").click(function(event) {
												if (event.target.nodeName != "INPUT") {
													var thisSearchId = this.id.substring((this.id.search("__")+2));
													var thisPersonId = thisID.substring(10);
													var thisCheckbox = $("#use_person_"+thisPersonId+"_"+thisSearchId);
													thisCheckbox.prop("checked", !thisCheckbox.prop("checked"));
												}
											});
										}
									});
									$(".emsa_dupsearch_tabset").tabs({selected: 2});
								} else {
									$(".emsa_dupsearch_tabset").tabs({selected: 2});
								}
								$("#"+this.id).removeClass("emsa_dup_dimmed");
								$(".emsa_dup:not(#"+this.id+")").addClass("emsa_dup_dimmed");
								$("#"+this.id+" > td").addClass("emsa_results_search_parent");
								$("#emsa_close_"+this.id).show();
								$(".emsa_close:not(#emsa_close_"+this.id+")").hide();
								$("#"+thisID).show();
								
								setTimeout(function() {
									//deprecated... var container = (($.browser.msie || $.browser.mozilla) ? $("body,html") : $("body"));
									var container = $("body,html");
									var scrollTo = $("#"+theId);
									container.scrollTop(container.offset().top); // reset scroll to top of page
									container.scrollTop(
										scrollTo.offset().top - container.offset().top + container.scrollTop()
									); // scroll to position of clicked row
								}, 10);
							} else {
								$(".emsa_dup:not(#"+this.id+")").removeClass("emsa_dup_dimmed");
								$("#"+this.id+" > td").removeClass("emsa_results_search_parent");
								$("#emsa_close_"+this.id).hide();
								$("#"+thisID).hide();
							}
						}
					});
				});
			</script>