<?php

	/**
	 * update_cmr.php
	 * 
	 * Accepts incoming ELR message from 'Entry' queue along with existing person ID
	 * and attempts to either update an existing event or add a new event for that
	 * person.
	 * 
	 * included by emsa/emsa_actions.php
	 */

	function setCommonCmrProps(&$orig_sxe, $white_rule, &$master_xml, &$clean_actions, $is_pregnancy, $is_surveillance, 
		$close_surveillance, $success_message, $failure_message) {

error_log("TODO Jay setCommonCmrProps orig_sxe:".$orig_sxe->asXML());		
//error_log("TODO Jay setCommonCmrProps master_xml:".$master_xml->asXML());		
		global $updatecmr_action_id, $final_xml, $new_cmr, $final_function, $this_audit_id, $district_override_id;

		removeParentGuardianEmptyGlue($orig_sxe); // check for & remove ',' value in empty parent_guardian field
		addCmrAbnormalFlagToComments($master_xml, $orig_sxe); // add decoded Abnormal Flag to comments, if present
		addCmrHL7ToNotes(getHL7($clean_actions['id']), $orig_sxe); // add original HL7 as a 'Note'
		geocodeNedssXml($orig_sxe);  // geocode lat/long coordinates for address, if possible
		
		// set pregnancy flag if pregnancy status interpreted from LOINC
		if ($is_pregnancy) {
			setPatientPregnant($orig_sxe, true);
		}
		// close if surveillance event
		if (($is_surveillance === IS_SURVEILLANCE_YES) && $close_surveillance) {
			setWorkflow($orig_sxe, 'closed');
		}

		// add 'SURV' to other_data_1 field for surveillance event search/filtering
		if ($is_surveillance === IS_SURVEILLANCE_YES) {
			if (isset($orig_sxe->events->other_data_1)) {
				$orig_sxe->events->other_data_1 = 'SURV';
			} else {
				$orig_sxe->events->addChild('other_data_1', 'SURV');
			}
		}
		
		// check to see if person selected for new CMR has the same name, address, and DOB; exception if not
		/*
		 * person-centric code for future use...
		if (updatedContactInfoToNotes($use_findperson_xml, true, $clean_actions['id'])) {
			// same name/addr/dob
			$updatecmr_action_id = 24;
			$final_xml = $orig_sxe->asXML();
			$new_cmr = true;
			$final_function = 'updateCmr';
			
			highlight($success_message);
		} else {
			// different name/addr/dob
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 69, 'Lab patient did not match selected patient for new CMR');  // entry queue exception
			suicide($failure_message);
		}
		 */
		 
		// check for possible event with same disease and different marriage-related name change
		$family_name_check= hasFamilyNameChanged($clean_actions['id'], $source['disease']);
		if (is_array($family_name_check)) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 68, 'Matched Record Numbers:<br>'.nedssLinkByEventArray($family_name_check));  // last name change due to marriage
			highlight($failure_message, 'ui-icon-elrerror');
		} else {
			$updatecmr_action_id = 23;  // new person & new CMR
								
			$person_identifier = array (
				'first_name' => (string)$master_xml->person->first_name,
				'last_name' => (string)$master_xml->person->last_name,
				'birth_date' => (string)$master_xml->person->birth_date);
//error_log("TODO Jay updateCmr person: ".print_r($person_identifier, true));
            // for TCPH set event type to be created to Assessment Event
            $add_sxe->create_assessment_event = true;


			// modify xml for SNHD whitelist rule behaviors
			addSNHDNewWhitelistSettings($clean_actions['id'], $white_rule, trim($master_xml->labs->test_result), $orig_sxe, $person_identifier, $master_condition);

			// check for override to default jurisdiction, based on condition
			if ($district_override_id > 0) {
				$orig_sxe->jurisdiction_attributes->place->id = getJurisdictionByDistrictID($district_override_id);
			}
					
			$final_xml = $orig_sxe->asXML();
			$new_cmr = true;
			$final_function = 'addCmr';
			highlight($success_message);
		}
	}
    
    function setPersonDemographics(&$orig_sxe, $person_entity_id) {
        global $person_search_demographics;
        $orig_sxe->interested_party_attributes->person->addChild('id', intval($use_person));
        if(isset($person_search_demographics)) {
            $orig_sxe->interested_party_attributes->person->birth_gender_id = $person_search_demographics['birth_gender_id'];
            $orig_sxe->interested_party_attributes->person->ethnicity_id = $person_search_demographics['ethnicity_id'];
            $orig_sxe->interested_party_attributes->person->first_name = $person_search_demographics['first_name'];
            $orig_sxe->interested_party_attributes->person->last_name = $person_search_demographics['last_name'];
            $orig_sxe->interested_party_attributes->person->birth_date = $person_search_demographics['birth_date'];
        }
    }

	unset($person_search);
	unset($entity_ids);
	unset($whitelist_rule);
	unset($whitelist_rule_tmp);
	unset($district_override_id);
	unset($contact_whitelist_rule);
	unset($notify_state);
	unset($is_immediate);
	unset($master_xml_raw);
	unset($trisano_xml_raw);
	unset($source);
	unset($result_events);
	unset($primary_result_events);
	unset($secondary_result_events);
	unset($whitelist_events);
	unset($whitelist_event_dates);
	unset($contact_whitelist_events);
	unset($contact_whitelist_event_dates);
	unset($exception_events);
	unset($contact_exception_events);
	unset($final_function);
	unset($final_xml);
	
	// SNHD specific variables
	unset($create_assessment_event);
    unset($person_search_demographics);
	
	$new_cmr = false;
	
	$is_stitchable = false;
	$stitchable_loincs = array(
		'11011-4', 
		'42595-9', 
		'42617-1', 
		'48398-2', 
		'48576-3', 
		'20447-9', 
		'24013-5', 
		'25835-0', 
		'25836-8', 
		'29539-4', 
		'29541-0', 
		'5017-9', 
		'5018-7', 
		'9837-6'
	);  // master LOINCs that should be stitched together for ARUP
//error_log('>>>>>update_cmr, checking whitelist');
	
	// get Master XML for whitelist check...
/*	$xml_qry = 'SELECT 
			sm.loinc_code AS loinc_code, sm.master_xml AS master_xml, sm.transformed_xml AS transformed_xml, sm.lab_id AS lab_id, 
            mc.white_rule AS white_rule, mc.contact_white_rule AS contact_white_rule, mc.notify_state AS notify_state, mc.immediate_notify AS immediate_notify, 
            mc.district_override AS district_override, mc.check_xref_first AS check_xref_first, mc.new_event AS species_new_event, 
			mv.concept AS condition 
		FROM '.$my_db_schema.'system_messages sm 
		INNER JOIN '.$my_db_schema.'vocab_master_loinc ml ON (sm.id = '.$clean_actions['id'].' AND sm.loinc_code = ml.loinc) 
		INNER JOIN '.$my_db_schema.'vocab_master_vocab mv ON ((sm.disease = mv.concept) AND (mv.category = elr.vocab_category_id(\'condition\'))) 
		INNER JOIN '.$my_db_schema.'vocab_master_condition mc ON (mv.id = mc.condition);';
		*/
	// We are getting System Message information and master Condition information for the System Message
	// NOTE that the Master condition of the event we assign the ELR too may be different (crossref condition)
	$xml_qry = 'SELECT 
			sm.loinc_code AS loinc_code, sm.master_xml AS master_xml, sm.transformed_xml AS transformed_xml, sm.lab_id AS lab_id, 
			mc.white_rule AS white_rule, mc.contact_white_rule AS contact_white_rule, mc.notify_state AS notify_state, mc.immediate_notify AS immediate_notify, 
			mc.district_override AS district_override, mc.check_xref_first AS check_xref_first, mc.new_event AS species_new_event, 
			mv.concept AS condition 
		FROM '.$my_db_schema.'system_messages sm 
		INNER JOIN '.$my_db_schema.'vocab_master2app ma ON (ma.coded_value = sm.disease)
		INNER JOIN '.$my_db_schema.'vocab_master_vocab mv ON ((ma.master_id = mv.id) AND (mv.category = elr.vocab_category_id(\'condition\')))
		INNER JOIN '.$my_db_schema.'vocab_master_condition mc ON (mv.id = mc.condition)
		WHERE sm.id = '.$clean_actions['id'].';';
		
//error_log('>>>>>update_cmr, xml_qry: '. $xml_qry);

	$xml_rs = @pg_query($host_pa, $xml_qry);
	if ($xml_rs === false || (@pg_num_rows($xml_rs) < 1)) {
		// message not found, throw exception
		$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
		messageException($clean_actions['id'], $this_audit_id, 69, 'Could not assign message; message not found.');  // Entry queue exception
		suicide('Could not assign message:  message could not be found.<br><br>Message moved to Exception list.');
	} else {
		$master_xml_raw = @pg_fetch_result($xml_rs, 0, 'master_xml');
		$trisano_xml_raw = @pg_fetch_result($xml_rs, 0, 'transformed_xml');
		$master_loinc_code = @pg_fetch_result($xml_rs, 0, 'loinc_code');
		$laboratory_id = @pg_fetch_result($xml_rs, 0, 'lab_id');
		$master_condition = @pg_fetch_result($xml_rs, 0, 'condition');
		$district_override_id = intval(@pg_fetch_result($xml_rs, 0, "district_override"));
		$whitelist_rule_tmp = @pg_fetch_result($xml_rs, 0, 'white_rule');
		$contact_whitelist_rule_tmp = @pg_fetch_result($xml_rs, 0, 'contact_white_rule');
        $check_xref_first = (trim(@pg_fetch_result($xml_rs, 0, 'check_xref_first')) == 't') ? true : false;
		$species_new_event = (trim(@pg_fetch_result($xml_rs, 0, 'species_new_event')) == 't') ? true : false;
		
		$is_stitchable = in_array($master_loinc_code, $stitchable_loincs) ? true : false;  // check if LOINC is a stitchable one (for ARUP use only)
		
		// record whether disease is state-notifiable & immediately notifiable or not
		$is_surveillance = IS_SURVEILLANCE_NO;
		$close_surveillance = true;
		$notify_state = false;
		$is_immediate = false;
		if (trim(@pg_fetch_result($xml_rs, 0, 'notify_state')) == 't') {
			$notify_state = true;
		}
		if (trim(@pg_fetch_result($xml_rs, 0, 'immediate_notify')) == 't') {
			$is_immediate = true;
		}
		
		@pg_free_result($xml_rs);
	}
	
	if (isset($trisano_xml_raw) && isset($master_xml_raw)) {
		$master_xml = simplexml_load_string($master_xml_raw);
		$master_test_type = trim($master_xml->labs->test_type);
		$master_test_result = trim($master_xml->labs->test_result);
		$master_result_value = trim($master_xml->labs->result_value);
		$master_test_name = trim($master_xml->labs->local_test_name);
// TODO Jay 
//error_log('TODO Jay >>>>>>>> master_test_result: '.$master_test_result);
//error_log('TODO Jay >>>>>>>> master_xml: '.$master_xml_raw);
		$master_organism = trim($master_xml->labs->organism);
		$child_local_loinc_code = trim($master_xml->labs->local_loinc_code);
		$child_local_code = trim($master_xml->labs->local_code);
		
		$child_loinc = ((isset($child_local_loinc_code) && !is_null($child_local_loinc_code) && !empty($child_local_loinc_code)) ? $child_local_loinc_code : $child_local_code);
		$is_pregnancy = isPregnancyIndicated($child_loinc, $laboratory_id);
		$allow_new_cmr = allowNewCMR($master_loinc_code, $master_test_result, $master_xml, 1);
		$valid_specimen = isValidSpecimenSource($child_loinc, getLocalResultValue($master_xml, $laboratory_id), trim($master_xml->labs->specimen_source), $laboratory_id);
		$is_surveillance = isSurveillance($master_loinc_code, $master_organism, $master_test_result, 1);
//error_log('TODO >>>>>update_cmr, flags have been set');
		
		if ($is_surveillance === IS_SURVEILLANCE_ERR_NO_RULES_DEFINED) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 72, '[isSurveillance] '.$master_loinc_code);  // No Case Management rules defined for Master LOINC
			highlight('Could not add new event:  No Case Management rules defined for Master LOINC.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
		} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_NO_RULES_MATCHED) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 73, '[isSurveillance] '.$master_loinc_code.'/'.$master_test_result);  // No Case Management rules evaluated true
			highlight('Could not add new event:  No Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
		} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_MULT_RULES_MATCHED) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Multiple rules evaluated true; '.$master_loinc_code.'/'.$master_test_result);  // Multiple Case Management rules evaluated true
			highlight('Could not add new event:  Multiple Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
		} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_MULT_ORGS_MATCHED) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Multiple organisms found with the same name; '.$master_loinc_code.'/'.$master_organism);  // Multiple organisms are defined with the same name
			highlight('Could not add new event:  Multiple Organisms were found with the same name.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
		} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_REQFIELD_LOINC) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Missing required field (Master LOINC)');  // Unable to evaluate rule
			highlight('Could not add new event:  Unable to determine surveillance event status due to missing master LOINC code.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
		} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_REQFIELD_ORGNAME) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Missing required field (Organism)');  // Unable to evaluate rule
			highlight('Could not add new event:  Unable to determine surveillance event status due to missing organism name.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
		} elseif ($is_surveillance === IS_SURVEILLANCE_ERR_REQFIELD_TESTRESULT) {
			$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			messageException($clean_actions['id'], $this_audit_id, 56, '[isSurveillance] Missing required field (Test Result)');  // Unable to evaluate rule
			highlight('Could not add new event:  Unable to determine surveillance event status due to missing test result.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
		} else {
		
			/*
			 * decode morbidity whitelist rule...
			 * valid whitelist rule types:
			 *   + time_onset (inc. specimen collection date vs. existing event onset date)
			 *   + time_treatment (inc. specimen collection date vs. existing event last treatment date)
			 *   + time_last_positive (inc. specimen collection date vs. existing event last positive lab received date)
			 *   + never_new (always append inc. lab to existing event if exactly one matching event is found)
			 *   + exception (require epidemiologist intervention to decide add/update)
			 */
			
			if (strlen($whitelist_rule_tmp) < 1) {
				$whitelist_rule = array('type' => 'exception', 'value' => null);
				
			} elseif (stripos($whitelist_rule_tmp, 'exception') !== false) {
				$whitelist_rule = array('type' => 'exception', 'value' => null);
				
			} elseif (stripos($whitelist_rule_tmp, 'newest') !== false) {
				$whitelist_rule = array('type' => 'newest', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'if case') !== false) {
				// temporarily treating 'logic' options as exception-type rules
				$whitelist_rule = array('type' => 'exception', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'never') !== false) {
				$whitelist_rule = array('type' => 'never_new', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'std multi') !== false) {
				$whitelist_rule = array('type' => 'std_multi', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'treatment') !== false) {
				$whitelist_tmp_value = trim(str_ireplace('after date of treatment', '', str_ireplace('after last treatment date', '', $whitelist_rule_tmp)));
				$whitelist_rule = array('type' => 'time_treatment', 'value' => $whitelist_tmp_value);
			} elseif (stripos($whitelist_rule_tmp, 'positive') !== false) {
				$whitelist_tmp_value = trim(str_ireplace('after date of last positive lab', '', $whitelist_rule_tmp));
				$whitelist_rule = array('type' => 'time_last_positive', 'value' => $whitelist_tmp_value);
			} elseif (stripos($whitelist_rule_tmp, 'snhd std syph') !== false) {
				$whitelist_rule = array('type' => 'snhd_std', 'value' => 'syph');
			} elseif (stripos($whitelist_rule_tmp, 'snhd std hiv') !== false) {
				$whitelist_rule = array('type' => 'snhd_std', 'value' => 'hiv');
			} elseif (stripos($whitelist_rule_tmp, 'snhd std ngct') !== false) {
				$whitelist_rule = array('type' => 'snhd_std', 'value' => 'ngct');
			} elseif (stripos($whitelist_rule_tmp, 'snhd tb') !== false) {
				$whitelist_rule = array('type' => 'snhd_tb', 'value' => NULL);
			} elseif (stripos($whitelist_rule_tmp, 'snhd flu') !== false) {
				$whitelist_rule = array('type' => 'snhd_flu', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'snhd hepa') !== false) {
				$whitelist_rule = array('type' => 'snhd_hepa', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'snhd hepb igm') !== false) {
				$whitelist_rule = array('type' => 'snhd_hepb_igm', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'snhd hepb') !== false) {
				$whitelist_rule = array('type' => 'snhd_hepb', 'value' => null);
			} elseif (stripos($whitelist_rule_tmp, 'snhd hepc') !== false) {
				$whitelist_rule = array('type' => 'never_new', 'value' => null);
			} else {
				$whitelist_tmp_value = trim($whitelist_rule_tmp);
				$whitelist_rule = array('type' => 'time_onset', 'value' => $whitelist_tmp_value);
			}
			/*
			 * decode contact whitelist rule...
			 * valid contact whitelist rule types:
			 *   + ...
			 */
			if (strlen($contact_whitelist_rule_tmp) < 1) {
				$contact_whitelist_rule = array('type' => 'exception', 'value' => null);
			} elseif (stripos($contact_whitelist_rule_tmp, 'diagnostic') !== false) {
				$contact_whitelist_rule = array('type' => 'always_new_if_diagnostic', 'value' => null);
			} elseif (stripos($contact_whitelist_rule_tmp, 'always') !== false) {
				$contact_whitelist_rule = array('type' => 'always_new', 'value' => null);
			} elseif (stripos($contact_whitelist_rule_tmp, 'never') !== false) {
				$contact_whitelist_rule = array('type' => 'never_new', 'value' => null);
			} else {
				$contact_whitelist_tmp_value = trim($contact_whitelist_rule_tmp);
				$contact_whitelist_rule = array('type' => 'time_onset', 'value' => $contact_whitelist_tmp_value);
			}
			
			// get TriSano XML...
			$orig_xml = simplexml_load_string($trisano_xml_raw);

			// if lab_test_date or collection_date is not set, copy it from the other
			if (!isset($orig_xml->labs_attributes->lab_results->collection_date) || empty($orig_xml->labs_attributes->lab_results->collection_date)) {
				$orig_xml->labs_attributes->lab_results->collection_date = $orig_xml->labs_attributes->lab_results->lab_test_date;
			} else if (!isset($orig_xml->labs_attributes->lab_results->lab_test_date) || empty($orig_xml->labs_attributes->lab_results->lab_test_date)) {
				$orig_xml->labs_attributes->lab_results->lab_test_date = $orig_xml->labs_attributes->lab_results->collection_date;
			} 

			$orig_sxe = new SimpleXMLElement($orig_xml->asXML());
			
			$source['disease'] = intval(trim($orig_xml->disease_events->disease_id));
			$source['organism'] = intval(trim($orig_xml->labs_attributes->lab_results->organism_id));
			$gateway_crossrefs = gatewayCrossrefsByName($master_condition, $source['disease']);
			
			#debug var_dump($gateway_crossrefs);
			
			// collect comparison data for use later in whitelist/existing lab rules
			$source['date_collected'] = date("Y-m-d", strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date)));
			$source['accession_no'] = trim($orig_xml->labs_attributes->lab_results->accession_no);
			$source['lab_name'] = trim($orig_xml->labs_attributes->place->name);
			$source['specimen_source'] = intval(trim($orig_xml->labs_attributes->lab_results->specimen_source_id));
			$source['test_type'] = intval(trim($orig_xml->labs_attributes->lab_results->test_type_id));
			$source['test_result'] = intval(trim($orig_xml->labs_attributes->lab_results->test_result_id));
			$source['result_value'] = ((isset($orig_xml->labs_attributes->lab_results->result_value) && (strlen(trim($orig_xml->labs_attributes->lab_results->result_value)) > 0)) ? trim($orig_xml->labs_attributes->lab_results->result_value) : null);
			$source['test_status'] = intval(trim($orig_xml->labs_attributes->lab_results->test_status_id));
			$source['comment'] = ((isset($orig_xml->labs_attributes->lab_results->comment) && (strlen(trim($orig_xml->labs_attributes->lab_results->comment)) > 0)) ? trim($orig_xml->labs_attributes->lab_results->comment) : null);
			$source['master_loinc'] = trim($orig_xml->labs_attributes->lab_results->loinc_code);
			$source['jurisdiction'] = intval(trim($orig_xml->jurisdiction_attributes->place->id));
			$is_arup = ((stripos($source['lab_name'], 'arup') === 0) ? true : false );
			$close_surveillance = closeSurveillanceBoolByJurisdiction($source['jurisdiction']);  // check whether this jurisdiction's surveillance events should be open or closed at the LHD level
		}
	} else {
		suicide('Error:  Unable to retrieve XML for selected event.');
	}
	
	#debug echo '<br>Source Disease, Collection Date, Master Condition, Whitelist Rule:<br><pre>';
	#debug print_r(array(&$source['disease'], &$source['date_collected'], &$master_condition, &$whitelist_rule));
	#debug echo '</pre><br>';
//error_log('>>>>>update_cmr, about to do person search');
	
	// make sure at least one person ID was passed
	if ($clean_actions['match_persons']) {
		$person_search = explode('|', $clean_actions['match_persons']);
//error_log('TODO Jay >>>>>update_cmr, person search: '.print_r($person_search, true));
	} else {
		suicide('Could not add to existing event:  No person selected.<br><br>Please select at least one matching person & try again.');
	}
	
	if (isset($person_search) && (isset($whitelist_rule) || isset($contact_whitelist_rule))) {
		/*
		 * Get past events for selected persons & check
		 * for same-disease events, then apply whitelist rules.
		 */
		
		foreach($person_search as $needle) {
			// call 'findPerson' from web service to get events back
			$findperson_qry = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health>
		<interested_party_attributes>
			<person>
				<id>$needle</id>
			</person>
		</interested_party_attributes>
	</trisano_health>
</health_message>
EOX;
			
//error_log('TODO Jay update_cmr find person query: '.$findperson_qry);
			// init SOAP client
			try {
				$soap_client = @new SoapClient($props['sqla_wsdl_url']);
			} catch (Exception $e) {
				suicide($e, -1, 1);
			} catch (SoapFault $f) {
				suicide($f, -1, 1);
			}
//error_log('TODO Jay >>>>>update_cmr, getting soap client: ');
			
			if ($soap_client) {
				try {
					$findperson_result = $soap_client->findPerson(array('healthMessage' => $findperson_qry));
				} catch (Exception $e) {
					suicide($e->getMessage(), -1, 1);
				} catch (SoapFault $f) {
					suicide($f->getMessage(), -1, 1);
				}
				$findperson_sxe = simplexml_load_string($findperson_result->return);
                
                // save the demographics for the first person found
                if(!isset($person_search_demographics)) {
                    $person_search_demographics['birth_gender_id'] = trim($findperson_sxe->trisano_health->interested_party_attributes->person->birth_gender_id);
                    $person_search_demographics['ethnicity_id'] = trim($findperson_sxe->trisano_health->interested_party_attributes->person->ethnicity_id);
                    $person_search_demographics['first_name'] = trim($findperson_sxe->trisano_health->interested_party_attributes->person->first_name);
                    $person_search_demographics['last_name'] = trim($findperson_sxe->trisano_health->interested_party_attributes->person->last_name);
                    $person_search_demographics['birth_date'] = trim($findperson_sxe->trisano_health->interested_party_attributes->person->birth_date);
                }
//error_log('TODO Jay update_cmr find person events result: '.$findperson_result->return);
//error_log('TODO Jay update_cmr find person events found: '.$findperson_sxe->asXML());
			
				// save entity IDs for later in event of no matching events
				$entity_ids[$needle] = array(
					'entity_id' => intval(trim($findperson_sxe->trisano_health->interested_party_attributes->person->entity_id)), 
					'participation_id' => intval(trim($findperson_sxe->trisano_health->interested_party_attributes->participations->id)), 
					'findperson_return' => $findperson_sxe
				);
				
				// set up arrays for prioritizing lab assignment to either disease or gateway xref
				$xref_events = array();
				$disease_events = array();

				if ($$check_xref_first) {
					$primary_result_events = &$xref_events;
					$secondary_result_events = &$disease_events;
				} else {
					$primary_result_events = &$disease_events;
					$secondary_result_events = &$xref_events;
				}

/*//error_log('TODO Jay >>>>>>>> master_test_result: '.$master_test_result);
foreach($findperson_sxe->trisano_health as $thT) {
    error_log("TODO JAY >>>>>> DISEASE EVENT: ".$thT->asXML());
    logger_error("TODO JAY >>>>>> DISEASE EVENT: ".$deT->asXML());
}*/
                

				foreach ($findperson_sxe->trisano_health->disease_events as $disease_event) {
					unset($this_ipa_participation_id);
					unset($last_treatment_date_arr);
					unset($treatment_dates);
					unset($last_treatment_date);
					unset($this_event_id);
					unset($this_event_disease_event_id);
					unset($this_event_disease_id);
					unset($this_event_disease_name);
					unset($this_event_jurisdiction_id);
					unset($this_event_created_at_arr);
					unset($this_event_created_at);
					unset($this_event_reported_date_arr);
					unset($this_event_reported_date);
					unset($this_event_deleted_at_arr);
					unset($this_event_deleted_at);
					unset($this_event_state_case_status_arr);
					unset($this_event_state_case_status);
					unset($this_event_investigator_id);
                    unset($this_event_queue);
                    unset($this_first_lab_or_report);
//error_log('TODO >>>>>update_cmr, disease_event: '.$disease_event->asXML());
//error_log("TODO Jay checking disease id: ".$disease_event->disease_id);
					
					if ((intval($disease_event->disease_id) == $source['disease']) || 
						(in_array(intval($disease_event->disease_id), $gateway_crossrefs)) ) {
						/*
						 * calculate last treatment date for any matching event IDs
						 * sort array & get back newest treatment date, then pass to $result_events array for this event
						 */
//error_log('TODO >>>>>update_cmr, disease matches disease event or cross-ref, event: '.$disease_event->asXML());
//error_log('TODO >>>>> found trisano health: '.$findperson_sxe->trisano_health->asXML());
						$this_event_id = intval(trim($disease_event->event_id));
						$this_event_disease_event_id = intval(trim($disease_event->id));
						$this_event_disease_id = intval(trim($disease_event->disease_id));
						foreach ($findperson_sxe->trisano_health->diseases as $disease) {
							if($disease->id == $this_event_disease_id) {
								$this_event_disease_name = $disease->disease_name;
								break;
							}
						}
//error_log("TODO Jay this event disease name: ".$this_event_disease_name);
						$this_event_jurisdiction_id = (isset($findperson_sxe->trisano_health->jurisdiction_attributes->place->id)) ? intval(trim($findperson_sxe->trisano_health->jurisdiction_attributes->place->id)) : null;

						$this_event_deleted_at_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/deleted_at');
						$this_event_deleted_at = null;
						if (($this_event_deleted_at_arr !== false) && is_array($this_event_deleted_at_arr) && (count($this_event_deleted_at_arr) > 0)) {
							foreach ($this_event_deleted_at_arr as $teda_obj => $teda_val) {
								if (strlen(trim($teda_val)) > 0) {
									$this_event_deleted_at = strtotime(trim($teda_val));
								}
							}
						}
						$this_event_state_case_status_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/state_case_status_id');
						$this_event_state_case_status = null;
						if (($this_event_state_case_status_arr !== false) && is_array($this_event_state_case_status_arr) && (count($this_event_state_case_status_arr) > 0)) {
							foreach ($this_event_state_case_status_arr as $tescs_obj => $tescs_val) {
								if (strlen(trim($tescs_val)) > 0) {
									$this_event_state_case_status = strtolower($_SESSION['trisano_codes']['external_codes']['case'][intval(trim($tescs_val))]['code_description']);
								}
							}
						}

						// only add events that are not deleted/discarded
						if (is_null($this_event_deleted_at) && (stripos($this_event_state_case_status, 'discarded') === false)) {
//error_log('TODO >>>>>update_cmr, state case status: '.$this_event_state_case_status);


                            if (in_array(intval($disease_event->disease_id), $gateway_crossrefs)) {
                                $xref_events[trim($findperson_sxe->trisano_health->interested_party_attributes->person->id)][] = $this_event_id;
                            }
                            if (intval($disease_event->disease_id) == $source['disease']) {
                                $disease_events[trim($findperson_sxe->trisano_health->interested_party_attributes->person->id)][] = $this_event_id;
                            }
							
							$this_ipa_participation_id_arr = $findperson_sxe->xpath('trisano_health/interested_party_attributes/participations[event_id=\''.$this_event_id.'\']/id');
							if (is_array($this_ipa_participation_id_arr)) {
								foreach ($this_ipa_participation_id_arr as $ipa_tpi_obj => $ipa_tpi_val) {
									$this_ipa_participation_id = intval($ipa_tpi_val);
								}
							}
							if (isset($this_ipa_participation_id)) {
								$last_treatment_date_arr = $findperson_sxe->xpath('trisano_health/interested_party_attributes/treatments_attributes[participation_id=\''.$this_ipa_participation_id.'\']/treatment_date');
								if (is_array($last_treatment_date_arr) && (count($last_treatment_date_arr) > 0)) {
									foreach ($last_treatment_date_arr as $ltd_obj => $ltd_val) {
										$treatment_dates[] = strtotime(trim($ltd_val));
									}
									// get back the most-recent date from the $treatment_dates array...
									rsort($treatment_dates, SORT_NUMERIC);
									$last_treatment_date = reset($treatment_dates);
								}
							}
							$this_event_date=0;
							$this_event_date_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/event_onset_date');
							foreach ($this_event_date_arr as $ted_obj => $ted_val) {
								$this_event_date = strtotime(trim($ted_val));
							}
//error_log("TODO Jay this event date:".$this_event_date);
							$this_event_created_at_arr = $findperson_sxe->xpath("trisano_health/events[id='".$this_event_id."']/created_at");
							foreach ($this_event_created_at_arr as $teca_obj => $teca_val) {
								$this_event_created_at = strtotime(trim($teca_val));
							}
							$this_event_reported_date_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/first_reported_PH_date');
							if (is_array($this_event_reported_date_arr) && (count($this_event_reported_date_arr) > 0)) {
								foreach ($this_event_reported_date_arr as $terd_obj => $terd_val) {
									$this_event_reported_date = strtotime(trim($terd_val));
								}
							} else {
								$this_event_reported_date = null;
							}
							$this_event_type_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/type');
							foreach ($this_event_type_arr as $tet_obj => $tet_val) {
								$this_event_type = trim($tet_val);
							}
//error_log("TODO Jay this event type:".$this_event_type);
							$this_event_investigator_id_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/investigator_id');
							foreach ($this_event_investigator_id_arr as $tet_obj => $tet_val) {
								$this_event_investigator_id = trim($tet_val);
							}
//error_log("TODO Jay this event investigator_id:".$this_event_investigator_id);
							$this_event_workflow_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/workflow_state');
							foreach ($this_event_workflow_arr as $tew_obj => $tew_val) {
								$this_event_workflow = trim($tew_val);
							}
//error_log("TODO Jay this event workflow:".$this_event_workflow);
							$this_event_recordnumber_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/record_number');
							foreach ($this_event_recordnumber_arr as $ter_obj => $ter_val) {
								$this_event_recordnumber = trim($ter_val);
							}
                            // TODO Jay:  fill in the queue name in SQLA.war in the findPerson() method
                            $this_event_queue = "";
                            if ($props['emsa_environment'] == 'SNHD') {
                                $this_event_queue_id_arr = $findperson_sxe->xpath('trisano_health/events[id=\''.$this_event_id.'\']/event_queue_id');
                                foreach ($this_event_queue_id_arr as $ter_obj => $ter_val) {
                                    $queue_id = trim($ter_val);
//error_log("TODO Jay queue id: '".$queue_id."'");
                                    switch($queue_id) {
                                        case "31":
                                            $this_event_queue = "DDCS High Priority";
                                            break;
                                        case "33":
                                            $this_event_queue = "DDCS CT/NG";
                                            break;
                                        case "34":
                                            $this_event_queue = "DDCS Syphilis";
                                            break;
                                        case "35":
                                            $this_event_queue = "DDCS TB";
                                            break;
                                        case "36":
                                            $this_event_queue = "DDCS TB Priority";
                                            break;
                                        default:
                                            break;
                                    }
                                }
//error_log("TODO Jay this event queue:".$this_event_queue);
                            }
							
							unset($labresult_participation_ids);
							unset($this_event_labresults_predicates);
							unset($this_event_labresults_arr);
							$labsattribs_participations_arr = $findperson_sxe->xpath('trisano_health/labs_attributes/participations[event_id=\''.$this_event_id.'\'][type=\'Lab\']');
							if (isset($labsattribs_participations_arr) && is_array($labsattribs_participations_arr)) {
								foreach ($labsattribs_participations_arr as $lapa_index => $lapa_obj) {
									$labresult_participation_ids[] = intval($lapa_obj->id);
								}
							}
							if (isset($labresult_participation_ids) && is_array($labresult_participation_ids)) {
								foreach ($labresult_participation_ids as $lr_pid_index => $lr_pid_obj) {
									$this_event_labresults_predicates[] = 'participation_id=\''.intval($lr_pid_obj).'\'';
								}
							}
							if (isset($this_event_labresults_predicates) && is_array($this_event_labresults_predicates)) {
								$this_event_labresults_arr = $findperson_sxe->xpath('trisano_health/labs_attributes/lab_results['.implode(' or ', $this_event_labresults_predicates).']');
							}

                            $first_lab_date=null;
                            $time_first_lab_date=null;
							$last_lab_date=null;
							$time_last_lab_date=null;
							if (isset($this_event_labresults_arr) && is_array($this_event_labresults_arr)) {
								foreach ($this_event_labresults_arr as $telr_index => $telr_obj) {
									if(isset($telr_obj->collection_date)) {
//error_log("TODO Jay >>> event id:".$this_event_id." lab result date:".print_r($telr_obj->collection_date, true));								   
										// get last and first lab collection dates
										$time_collection_date = strtotime(trim($telr_obj->collection_date));
										if($last_lab_date==null || $time_collection_date > $time_last_lab_date) {
											$last_lab_date=$telr_obj->collection_date;
											$time_last_lab_date=$time_collection_date;
										}
										if($first_lab_date==null || $time_collection_date < $time_first_lab_date) {
											$first_lab_date=$telr_obj->collection_date;
											$time_first_lab_date=$time_collection_date;
										}
									}
								
									if (intval($telr_obj->test_result_id) === intval($props['positive_result_id'])) {
										// positive lab
										if (($species_new_event && (intval($telr_obj->organism_id) === intval($source['organism']))) || (!$species_new_event)) {
											// if 'different species/new event' is set to "New", constrain to labs w/ same organism
											// otherwise, look for any positive lab
											$positive_lab_times[] = strtotime(trim($telr_obj->collection_date));
										}
									}
								}
							}
                            if($first_lab_date == null) {
                                $this_first_lab_or_report = $this_event_reported_date;
                            } else {
                                $this_first_lab_or_report = $time_first_lab_date;
                            }
							if (isset($positive_lab_times) && is_array($positive_lab_times) && (count($positive_lab_times) > 0)) {
								rsort($positive_lab_times, SORT_NUMERIC);
								$last_positive_lab_timestamp = reset($positive_lab_times);  // get back the most-recent date from the $positive_lab_times array...
								
								sort($positive_lab_times, SORT_NUMERIC);
								$first_positive_lab_timestamp = reset($positive_lab_times);  // get back the oldest date from the $positive_lab_times array...
							}
							
							
							$result_events[trim($findperson_sxe->trisano_health->interested_party_attributes->person->id)][$this_event_id] = array(
								'event_date'				=> $this_event_date,
								'treat_date'				=> ((isset($last_treatment_date)) ? $last_treatment_date : null),
								'first_positive_lab_date'	=> ((isset($first_positive_lab_timestamp)) ? $first_positive_lab_timestamp : null),
								'last_positive_lab_date'	=> ((isset($last_positive_lab_timestamp)) ? $last_positive_lab_timestamp : null),
								'last_lab_date'			 => $last_lab_date,
								'created_at'				=> $this_event_created_at,
								'reported_date'				=> $this_event_reported_date,
								'event_type'				=> $this_event_type,
								'record_number'				=> $this_event_recordnumber,
								'workflow'					=> $this_event_workflow,
								'jurisdiction_id'			=> $this_event_jurisdiction_id,
								'disease_event_id'			=> $this_event_disease_event_id,
								'disease_id'				=> $this_event_disease_id, 
								'disease_name'			  => $this_event_disease_name,
								'deleted_at'			    => $this_event_deleted_at,
								'state_case_status'			=> $this_event_state_case_status,
								'investigator_id'		   => $this_event_investigator_id,
								'event_queue'		       => $this_event_queue,
                                'first_lab_or_report'      => $this_first_lab_or_report);
						}
					}
				}
			}
		}

error_log("TODO Jay >>>>> count of matching people:".count($result_events));
//error_log("TODO >>>>>  result events: ".print_r($result_events, true));		
//error_log("TODO >>>>> count primary: ".count($primary_result_events)." ".print_r($primary_result_events, true));
//error_log("TODO >>>>> count secondary: ".count($secondary_result_events)." ".print_r($secondary_result_events, true));
		if(useSNHDStdWhiteList($orig_sxe, $orig_xml, $result_events, $whitelist_rule['type'], $whitelist_rule['value'], 
				$master_test_name, $master_test_result) || 
				useSNHDTBWhiteList($orig_sxe, $orig_xml, $result_events, $whitelist_rule['type'], $whitelist_rule['value'], 
				$master_test_name, $master_test_result)
				) {
			unset($primary_result_events);
			unset($secondary_result_events);
		}
        
		if (isset($primary_result_events) && isset($secondary_result_events) && is_array($primary_result_events) && 
				is_array($secondary_result_events) && (count($primary_result_events) > 0) && (count($secondary_result_events) > 0)) {
			foreach ($secondary_result_events as $prune_person_id => $prune_events) {
				foreach ($prune_events as $prune_event_id) {
					#debug echo 'unsetting $result_events['.$prune_person_id.']['.$prune_event_id.']';
					unset($result_events[$prune_person_id][$prune_event_id]);
//error_log("TODO >>>>>>>>>>> unset person id: ".$prune_person_id." event id: ".$prune_event_id);	
				}
			}
		}
		
		#debug echo '<br>primary_result_events:<br><pre>';
		#debug print_r($primary_result_events);
		#debug echo '</pre><br>secondary_result_events:<br><pre>';
		#debug print_r($secondary_result_events);
		#debug echo '</pre><br>result_events:<br><pre>';
		#debug print_r($result_events);
		#debug echo '</pre><br>Entity IDs:<br><pre>';
		#debug print_r($entity_ids);
		#debug echo "</pre><br>";
		
		if (count($result_events) < 1) {
			/*
			 * none of the people searched have a matching event,
			 * check if incoming ELR message allows new CMRs.
			 * 
			 * if yes, use first person_id returned & create a new event...
			 */
			 
			auditMessage($clean_actions['id'], 35, $type, 'No matching events found in selected people');  // attempting to create new CMR
error_log('TODO Jay >>>>> No matching events found, update_cmr, checking allow cmr');
			
			if ($orig_sxe->discard_lab) {
				$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.BLACK_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 1, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
				if (@pg_query($host_pa, $exception_sql)) {
					highlight('Message discarded...', 'ui-icon-elrsuccess');
				} else {
					suicide('A database error occurred while attempting to discard incoming message.  Please contact a system administrator.', 1);
				}
			} else if ($orig_sxe->move_to_gray_list) {
				$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
				if (@pg_query($host_pa, $exception_sql)) {
					highlight('Message moved to Gray list...', 'ui-icon-elrsuccess');
				} else {
					suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
				}
			} else if ($allow_new_cmr == ALLOW_CMR_YES) {
				if (($is_surveillance === IS_SURVEILLANCE_YES) || ($is_surveillance === IS_SURVEILLANCE_NO)) {
					if ($valid_specimen === SPECIMEN_VALID) {
						// new CMR event, existing person
						/*
						 * For now, not using person-centric matching, so any 'new CMR' case within the 'update_cmr.php' script 
						 * will create a new CMR & new person anyway, not a new CMR for an existing person.
						 * 
						 * May switch later, so leaving the code here for possible future use...
						 *
						$use_person = reset($person_search);
						$use_entity = $entity_ids[$use_person]['entity_id'];
						$use_findperson_xml = $entity_ids[$use_person]['findperson_return'];
						
						$orig_sxe->interested_party_attributes->person->addChild('id', intval($use_person));
						$orig_sxe->interested_party_attributes->person->addChild('entity_id', intval($use_entity));
						 */

						$use_person = reset($person_search);
//						$orig_sxe->interested_party_attributes->person->addChild('id', intval($use_person));
                        setPersonDemographics($orig_sxe, intval($use_person));
error_log("TODO Jay >> ID of matched person for new event: ".$use_person);
						setCommonCmrProps($orig_sxe, $whitelist_rule_tmp, $master_xml, $clean_actions, $is_pregnancy, $is_surveillance, $close_surveillance, 
							'No matching events found, adding new event...',	
							'No matching events found, but could not add new event:  Female or minor patient family name may have changed due to marriage<br><br>Message moved to Exception list.');
						 
					} elseif ($valid_specimen === SPECIMEN_INVALID) {
						// specimen source in invalid specimen sources list; graylilst message
						auditMessage($clean_actions['id'], 27, $type, 'Invalid specimen source');  // list change by whitelist rule
						$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
						if (@pg_query($host_pa, $exception_sql)) {
							highlight('Valid specimen source not provided.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
						} else {
							suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
						}
					} else {
						// Specimen source not in valid or invalid specimen sources list; send to exception
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 69, 'Unexpected specimen source detected.');  // Entry queue exception
						highlight('Could not add new event:  Condition requires specimen source validation, but an invalid specimen source was provided.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
					}
				}
			} elseif ($allow_new_cmr == ALLOW_CMR_NO_QUEST_ID_AS_NAME) {
				/*
				 * QUEST ordered message with case id in name fields, leave in entry queue 
				 */
				$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
			} elseif ($allow_new_cmr == ALLOW_CMR_NO_INFORM) {
				// informative test only, do not use to create new CMR; graylist incoming ELR message
				auditMessage($clean_actions['id'], 27, $type, 'Informative test only -- not usable for independent new CMR');  // list change by whitelist rule
				$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
				if (@pg_query($host_pa, $exception_sql)) {
					highlight('Test type not allowed to create new CMRs without existing event.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
				} else {
					suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
				}
			} elseif ($allow_new_cmr == ALLOW_CMR_NO_NEG) {
				// result != positive; graylist
				auditMessage($clean_actions['id'], 27, $type, 'Negative or Inconclusive test results');  // list change by whitelist rule
				$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
				if (@pg_query($host_pa, $exception_sql)) {
					highlight('Lab result is Negative or Indeterminate, moving to Gray List.', 'ui-icon-elrsuccess');
				} else {
					suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
				}
			} elseif ($allow_new_cmr == ALLOW_CMR_ERR_XML_REQ_FIELDS) {
				// Missing required fields for allowNewCMR()
				$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
				messageException($clean_actions['id'], $this_audit_id, 56, '[allowNewCMR] Unable to run Case Management rules due to missing Master LOINC Code or Test Result.');  // Unable to evaluate rule
				highlight('Could not add new event:  Unable to run Case Management rules due to missing data.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
			} elseif ($allow_new_cmr == ALLOW_CMR_ERR_NO_RULES_DEFINED) {
				// No Case Management rules defined for Master LOINC
				$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
				messageException($clean_actions['id'], $this_audit_id, 72, $master_loinc_code);  // No Case Management rules defined for Master LOINC
				highlight('Could not add new event:  No Case Management rules defined for Master LOINC.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
			} elseif ($allow_new_cmr == ALLOW_CMR_ERR_NO_RULES_MATCHED) {
				// No Case Management rules evaluated true
				$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
				messageException($clean_actions['id'], $this_audit_id, 73, $master_loinc_code.'/'.$master_test_result);  // No Case Management rules evaluated true
				highlight('Could not add new event:  No Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
			} elseif ($allow_new_cmr == ALLOW_CMR_ERR_MULT_RULES_MATCHED) {
				// Multiple Case Management rules evaluated true
				$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
				messageException($clean_actions['id'], $this_audit_id, 56, '[allowNewCMR] Multiple rules evaluated true; '.$master_loinc_code.'/'.$master_test_result);  // Multiple Case Management rules evaluated true
				highlight('Could not add new event:  Multiple Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
			}
		} else {
			/*
			 * one or more persons found with a matching event, run whitelist rules against
			 * events listed in $result_events
			 */
error_log("TODO Jay one or more people with matching events found: ".count($result_events));
			highlight('Matching events found, checking events against whitelist rules...');
			foreach ($result_events as $result_person_id => $result_person_events) {
				foreach ($result_person_events as $person_event_id => $person_event_data) {
					// check each historical event against incoming lab condition's whitelist rules...
					$is_whitelisted = false;
					if (is_null($result_events[$result_person_id][$person_event_id]['deleted_at']) && (stripos($result_events[$result_person_id][$person_event_id]['state_case_status'], 'discarded') === false)) {
						// skip deleted & discarded events
						if (in_array($result_events[$result_person_id][$person_event_id]['event_type'], array('MorbidityEvent', 'AssessmentEvent', 'ContactEvent'))) {
						// morbidity event or assessment (and contact event for SNHD), use $whitelist_rule
							if (!isset($orig_xml->labs_attributes->lab_results->collection_date) || empty($orig_xml->labs_attributes->lab_results->collection_date)) {
error_log("TODO Jay X * X * X * X * X * X * X * X * X * X * X * X * X * X * no lab collection date!!!! ");
error_log("TODO Jay X * X * X * X * X * X * X * X * X * X * X * X * X * X * no lab collection date!!!! ");
								$is_whitelisted = false;  // no lab collection date; create new
							} else {
								if ($whitelist_rule['type'] == 'never_new') {
									$is_whitelisted = true;
								} elseif ($whitelist_rule['type'] == 'newest' || $whitelist_rule['type'] == 'snhd_hepa' || 
									$whitelist_rule['type'] == 'snhd_hepb' || $whitelist_rule['type'] == 'snhd_hepb_igm') {
									$is_whitelisted = true;
									$whitelist_reported_dates[$person_event_id] = $result_events[$result_person_id][$person_event_id]['reported_date'];
								} elseif ($whitelist_rule['type'] == 'exception') {
									$is_whitelisted = true;
									$exception_events[] = $person_event_id;
								} elseif ($whitelist_rule['type'] == 'std_multi') {
									// multi-step whitelist rule for STDs
									$whitelist_start_str = '+30 days';
									$whitelist_onset_start_date = strtotime($whitelist_start_str, $result_events[$result_person_id][$person_event_id]['event_date']);
									if ($whitelist_onset_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
										// collected within 30 days of onset, update CMR
										$is_whitelisted = true;
									} else {
										// collected 30 or more days since onset
										if (is_null($result_events[$result_person_id][$person_event_id]['treat_date'])) {
											$is_whitelisted = true;
											$exception_events[$person_event_id] = $result_person_id;  // no treatment date, flag as an exception
										} else {
											$whitelist_treat_start_date = strtotime($whitelist_start_str, $result_events[$result_person_id][$person_event_id]['treat_date']);
											if ($whitelist_treat_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
												// within 30 days of treatment
												$is_whitelisted = true;
											}
										}
									}
								} elseif ($whitelist_rule['type'] == 'time_onset') {
									$whitelist_start_str = '+'.$whitelist_rule['value'];
									// first, check whitelist rules against event date...
									if (is_null($result_events[$result_person_id][$person_event_id]['event_date'])) {
										$is_whitelisted = false;
									} else {
										$whitelist_start_date = strtotime($whitelist_start_str, $result_events[$result_person_id][$person_event_id]['event_date']);
										if ($whitelist_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
											$is_whitelisted = true;
										}
									}
									if ($is_whitelisted !== true) {
										// if still not whitelisted by event date, check for first positive lab date
										if (is_null($result_events[$result_person_id][$person_event_id]['first_positive_lab_date'])) {
											// if no positive labs, flag as an exception
											$is_whitelisted = true;
											$exception_events[$person_event_id] = $result_person_id;
										} else {
											$whitelist_start_date = strtotime($whitelist_start_str, $result_events[$result_person_id][$person_event_id]['first_positive_lab_date']);
											if ($whitelist_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
												$is_whitelisted = true;
											}
										}
									}
								} elseif ($whitelist_rule['type'] == 'time_treatment') {
									$whitelist_start_str = '+'.$whitelist_rule['value'];
									if (is_null($result_events[$result_person_id][$person_event_id]['treat_date'])) {
										// if no treatment date, flag as an exception
										$is_whitelisted = true;
										$exception_events[$person_event_id] = $result_person_id;
									} else {
										$whitelist_start_date = strtotime($whitelist_start_str, $result_events[$result_person_id][$person_event_id]['treat_date']);
										if ($whitelist_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
											$is_whitelisted = true;
										}
									}
								} elseif ($whitelist_rule['type'] == 'time_last_positive') {
									$whitelist_start_str = '+'.$whitelist_rule['value'];
									if (is_null($result_events[$result_person_id][$person_event_id]['last_positive_lab_date'])) {
										// if no positive labs, flag as an exception
										$is_whitelisted = true;
										$exception_events[$person_event_id] = $result_person_id;
									} else {
										$whitelist_start_date = strtotime($whitelist_start_str, $result_events[$result_person_id][$person_event_id]['last_positive_lab_date']);
										if ($whitelist_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
											$is_whitelisted = true;
										}
									}
								} elseif ($whitelist_rule['type'] == 'snhd_flu') {
									// SNHD whitelist rule for  Flu
									$whitelist_start_str = '+30 days';
									$whitelist_onset_start_date = strtotime($whitelist_start_str, $result_events[$result_person_id][$person_event_id]['event_date']);
									$whitelist_event_dates[$person_event_id] = $result_events[$result_person_id][$person_event_id]['event_date'];
									if ($whitelist_onset_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
										$is_whitelisted = true;
									}
								} elseif ($whitelist_rule['type'] == 'snhd_std' || $whitelist_rule['type'] == 'snhd_tb') {
									// by this time we already have a filtered list of relevant cases
									// we white list it to the most recent case.
									$is_whitelisted = true;
									$best_date = $result_events[$result_person_id][$person_event_id]['reported_date'];
									if(isset($result_events[$result_person_id][$person_event_id]['event_date'])) {
										$best_date = $result_events[$result_person_id][$person_event_id]['event_date'];
									}
									$whitelist_event_dates[$person_event_id] = $best_date;
								} 
							}
							if ($is_whitelisted) {
								// morbidity whitelist rule matches
								$whitelist_events[$person_event_id] = $result_person_id;
							}
						} else {
							// contact event, use $contact_whitelist_rule
							if (!isset($orig_xml->labs_attributes->lab_results->collection_date) || empty($orig_xml->labs_attributes->lab_results->collection_date)) {
								$is_whitelisted = false;  // no lab collection date; create new
							} else {
								if ($contact_whitelist_rule['type'] == 'exception') {
									$is_whitelisted = true;
									$contact_exception_events[] = $person_event_id;
								} elseif ($contact_whitelist_rule['type'] == 'always_new_if_diagnostic') {
									$is_whitelisted = false; // $allow_new_cmr should check for 'if diagnostic' later, assume new CMR here though
								} elseif ($contact_whitelist_rule['type'] == 'always_new') {
									$is_whitelisted = false;
								} elseif ($contact_whitelist_rule['type'] == 'never_new') {
									$is_whitelisted = true;
								} elseif ($contact_whitelist_rule['type'] == 'time_onset') {
									$contact_whitelist_start_str = '+'.$contact_whitelist_rule['value'];
									$contact_whitelist_start_date = strtotime($contact_whitelist_start_str, $result_events[$result_person_id][$person_event_id]['event_date']);
									if ($contact_whitelist_start_date > strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date))) {
										$is_whitelisted = true;
									}
								}
							}
							if ($is_whitelisted) {
								// contact whitelist rule matches
								$contact_whitelist_event_dates[$person_event_id] = $result_events[$result_person_id][$person_event_id]['event_date'];
								$contact_whitelist_events[$person_event_id] = $result_person_id;
							}
						}
					}
				}
			}

			if ((count($whitelist_events) > 1) && 
				($whitelist_rule['type'] == 'newest' || $whitelist_rule['type'] == 'snhd_std' || $whitelist_rule['type'] == 'snhd_tb' || 
					$whitelist_rule['type'] == 'snhd_hepa' || $whitelist_rule['type'] == 'snhd_hepb' || $whitelist_rule['type'] == 'snhd_hepb_igm')) {
				// for 'Newest', 'SNHD gonchla','SNHD HepA', 'SNHD HepB', 'SNHD STD', 'SNHD_TB' whitelist rules, if more than one case, trim results to newest event
				unset($max_event);
				
				arsort($whitelist_event_dates, SORT_NUMERIC);  // sort matching events by date (unix epoch, get largest value)
				$max_event = array(reset(array_flip($whitelist_event_dates)));  // get the first (newest) one from the list
				
				$whitelist_events = array_intersect_key($whitelist_events, array_flip($max_event));  // remove all of the items from $contact_whitelist_events except the one we want
			}
			
			if (count($contact_whitelist_events) > 1) {
				// more than one Contact matched whitelist rules, get newest Contact
				unset($max_contact_event);
				
				arsort($contact_whitelist_event_dates, SORT_NUMERIC);  // sort matching events by date (unix epoch, get largest value)
				$max_contact_event = array(reset(array_flip($contact_whitelist_event_dates)));  // get the first (newest) one from the list
				
				$contact_whitelist_events = array_intersect_key($contact_whitelist_events, array_flip($max_contact_event));  // remove all of the items from $contact_whitelist_events except the one we want
			}
			
			if (count($whitelist_events) > 1) {
				// if more than one whitelist event, loop through list & prune out any "not a case" events, unless all of the events are "not a case"
				$notacase_prune = array(
					'case' => array(),
					'notacase' => array()
				);
				
				foreach ($whitelist_events as $whiteprune_event_id => $whiteprune_person_id) {
					if (stripos($result_events[$whiteprune_person_id][$whiteprune_event_id]['state_case_status'], 'not a case') !== false) {
						$notacase_prune['notacase'][] = $whiteprune_event_id;
					} else {
						$notacase_prune['case'][] = $whiteprune_event_id;
					}
				}
				
				if ((count($notacase_prune['notacase']) > 0) && (count($notacase_prune['notacase']) < count($whitelist_events))) {
					foreach ($notacase_prune['notacase'] as $notacase_prune_event_id) {
						unset($whitelist_events[$notacase_prune_event_id]);
					}
				}
			}
//error_log("TODO Jay count of white list events: ".count($whitelist_events));

			if (count($whitelist_events) > 1) {
				/*
				 * more than one CMR matched whitelist rules,
				 * move to exception
				 */
error_log("TODO Jay Moving to EXCEPTION More than one matching event found");
				$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
				if (isset($exception_events)) {
					// one or more exceptions happened, be sure to log these
					if (($whitelist_rule['type'] == 'time_treatment') || ($whitelist_rule['type'] == 'std_multi')) {
						messageException($clean_actions['id'], $this_audit_id, 70, 'No existing treatments found for treatment date-based rule.  Matched Events:<br>'.nedssLinkByEventArray($whitelist_events).'<br><br>Event(s) with exceptions:<br>'.nedssLinkByEventArray($exception_events));  // whitelist exception
						suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
					} elseif ($whitelist_rule['type'] == 'time_last_positive') {
						messageException($clean_actions['id'], $this_audit_id, 70, 'No existing positive labs found, Whitelist Rule requires most-recent positive lab collection date.  Matched Events:<br>'.nedssLinkByEventArray($whitelist_events).'<br><br>Event(s) with exceptions:<br>'.nedssLinkByEventArray($exception_events));  // whitelist exception
						suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
					} elseif ($whitelist_rule['type'] == 'exception') {
						messageException($clean_actions['id'], $this_audit_id, 70, 'Morbidity Whitelist Rules require manual review ['.$master_condition.']');  // whitelist exception
						suicide('This lab could not be automatically added to existing events for the selected person because it requires manual review by an Epidemiologist.<br><br>Message moved to Exception list.');
					} else {
						messageException($clean_actions['id'], $this_audit_id, 70, 'No existing positive labs found, Whitelist Rule requires first positive lab collection date.  Matched Events:<br>'.nedssLinkByEventArray($whitelist_events).'<br><br>Event(s) with exceptions:<br>'.nedssLinkByEventArray($exception_events));  // whitelist exception
						suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
					}
				} else {
					messageException($clean_actions['id'], $this_audit_id, 70, 'More than one TriSano CMR matched whitelist rules.  Matched Record Numbers:<br>'.nedssLinkByEventArray($whitelist_events));  // whitelist exception
					suicide('Could not add the lab to the selected person(s) because multiple TriSano CMRs matched whitelist rules.<br><br>Message moved to Exception list.');
				}
			} elseif ((count($whitelist_events) < 1) && (count($contact_whitelist_events) > 1)) {
				/*
				 * no CMRs matched and more than one Contact matched whitelist rules,
				 * move to exception
				 */
				$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
				if (isset($contact_exception_events)) {
					messageException($clean_actions['id'], $this_audit_id, 70, 'No Contact Whitelist Rules defined for Master Condition ['.$master_condition.']  Matched Record Numbers:<br>'.nedssLinkByEventArray($contact_whitelist_events));  // whitelist exception
					suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
				} else {
					messageException($clean_actions['id'], $this_audit_id, 70, 'More than one TriSano Contact matched whitelist rules.  Matched Record Numbers:<br>'.nedssLinkByEventArray($contact_whitelist_events));  // whitelist exception
					suicide('Could not add the lab to the selected person(s) because multiple TriSano Contacts matched whitelist rules.<br><br>Message moved to Exception list.');
				}
			} elseif ((count($whitelist_events) < 1) && (count($contact_whitelist_events) < 1)) {
				/*
				 * none of the CMRs or Contacts found match whitelist rules, 
				 * check if incoming ELR message allows new CMRs
				 * 
				 * if yes, use first person_id returned & create a new event...
				 */
				 
				auditMessage($clean_actions['id'], 35, $type, 'None of the matched events met whitelist conditions');  // attempting to create new CMR
				if ($allow_new_cmr == ALLOW_CMR_YES) {
					if (($is_surveillance === IS_SURVEILLANCE_YES) || ($is_surveillance === IS_SURVEILLANCE_NO)) {
						if ($valid_specimen === SPECIMEN_VALID) {
							// new CMR event, existing person
							/*
							 * For now, not using person-centric matching, so any 'new CMR' case within the 'update_cmr.php' script 
							 * will create a new CMR & new person anyway, not a new CMR for an existing person.
							 * 
							 * May switch later, so leaving the code here for possible future use...
							 *
							$use_person = reset($person_search);
							$use_entity = $entity_ids[$use_person]['entity_id'];
							$use_findperson_xml = $entity_ids[$use_person]['findperson_return'];
							
							$orig_sxe->interested_party_attributes->person->addChild('id', intval($use_person));
							$orig_sxe->interested_party_attributes->person->addChild('entity_id', intval($use_entity));
							 */
                            $use_person = reset($person_search);
//                            $orig_sxe->interested_party_attributes->person->addChild('id', intval($use_person));
                            setPersonDemographics($orig_sxe, intval($use_person));
error_log("TODO Jay >>>>>> ID of matched person for new event: ".$use_person);
							setCommonCmrProps($orig_sxe, $whitelist_rule_tmp, $master_xml, $clean_actions, $is_pregnancy, $is_surveillance, $close_surveillance, 
								'No events matched whitelist rules, adding new event...', 
								'No matching events found, but could not add new event:  Female or minor patient family name may have changed due to marriage<br><br>Message moved to Exception list.');
								
						} elseif ($valid_specimen === SPECIMEN_INVALID) {
							// specimen source in invalid specimen sources list; graylilst message
							auditMessage($clean_actions['id'], 27, $type, 'Invalid specimen source');  // list change by whitelist rule
							$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
							if (@pg_query($host_pa, $exception_sql)) {
								highlight('Valid specimen source not provided.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
							} else {
								suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
							}
						} else {
							// Specimen source not in valid or invalid specimen sources list; send to exception
							$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
							messageException($clean_actions['id'], $this_audit_id, 69, 'Unexpected specimen source detected.');  // Entry queue exception
							highlight('Could not add new event:  Condition requires specimen source validation, but an invalid specimen source was provided.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
						}
					}
				} elseif ($allow_new_cmr == ALLOW_CMR_NO_QUEST_ID_AS_NAME) {
					/*
					 * QUEST ordered message with case id in name fields, leave in entry queue 
					 */
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
				} elseif ($allow_new_cmr == ALLOW_CMR_NO_INFORM) {
					// informative test only, do not use to create new CMR; graylist incoming ELR message
					auditMessage($clean_actions['id'], 27, $type, 'Informative test only -- not usable for independent new CMR');  // list change by whitelist rule
					$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
					if (@pg_query($host_pa, $exception_sql)) {
						highlight('Test type not allowed to create new CMRs without existing event.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
					} else {
						suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
					}
				} elseif ($allow_new_cmr == ALLOW_CMR_NO_NEG) {
					// result != positive; graylist
					auditMessage($clean_actions['id'], 27, $type, 'Negative or Inconclusive test results');  // list change by whitelist rule
					$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
					if (@pg_query($host_pa, $exception_sql)) {
						highlight('Incoming message contains Negative or Inconclusive results.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
					} else {
						suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
					}
				} elseif ($allow_new_cmr == ALLOW_CMR_ERR_XML_REQ_FIELDS) {
					// Missing required fields for allowNewCMR()
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					messageException($clean_actions['id'], $this_audit_id, 56, '[allowNewCMR] Unable to run Case Management rules due to missing Master LOINC Code or Test Result');  // Unable to evaluate rule
					highlight('Could not add new event:  Unable to run Case Management rules due to missing data.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
				} elseif ($allow_new_cmr == ALLOW_CMR_ERR_NO_RULES_DEFINED) {
					// No Case Management rules defined for Master LOINC
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					messageException($clean_actions['id'], $this_audit_id, 72, $master_loinc_code);  // No Case Management rules defined for Master LOINC
					highlight('Could not add new event:  No Case Management rules defined for Master LOINC.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
				} elseif ($allow_new_cmr == ALLOW_CMR_ERR_NO_RULES_MATCHED) {
					// No Case Management rules evaluated true
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					messageException($clean_actions['id'], $this_audit_id, 73, $master_loinc_code.'/'.$master_test_result);  // No Case Management rules evaluated true
					highlight('Could not add new event:  No Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
				} elseif ($allow_new_cmr == ALLOW_CMR_ERR_MULT_RULES_MATCHED) {
					// Multiple Case Management rules evaluated true
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					messageException($clean_actions['id'], $this_audit_id, 56, '[allowNewCMR] Multiple rules evaluated true; '.$master_loinc_code.'/'.$master_test_result);  // Multiple Case Management rules evaluated true
					highlight('Could not add new event:  Multiple Case Management rules evaluated true.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
				}
			} else {
				/*
				 * found exactly one event that matched the whitelist
				 * make sure it's not an exception first
				 */
error_log("TODO >>>>>>>>>>>>>>>>>> found exactly one event matching whitelist");
				unset($is_contact);
				$is_contact = (count($whitelist_events) === 1) ? false : true; // if exactly one CMR matched whitelist rules, update is for a CMR, otherwise updating a Contact
				
				if ($is_contact && isset($contact_exception_events)) {
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					messageException($clean_actions['id'], $this_audit_id, 70, 'No Contact Whitelist Rules defined for Master Condition ['.$master_condition.']');  // whitelist exception
					suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
				} elseif (isset($exception_events)) {
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					if (($whitelist_rule['type'] == 'time_treatment') || ($whitelist_rule['type'] == 'std_multi')) {
						messageException($clean_actions['id'], $this_audit_id, 70, 'No existing treatments found for treatment date-based rule.  Event(s) matched:<br>'.nedssLinkByEventArray($exception_events));  // whitelist exception
						suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
					} elseif ($whitelist_rule['type'] == 'time_last_positive') {
						messageException($clean_actions['id'], $this_audit_id, 70, 'No existing positive labs found, Whitelist Rule requires most-recent positive lab collection date.  Event(s) matched:<br>'.nedssLinkByEventArray($exception_events));  // whitelist exception
						suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
					} elseif ($whitelist_rule['type'] == 'exception') {
						messageException($clean_actions['id'], $this_audit_id, 70, 'Morbidity Whitelist Rules require manual review ['.$master_condition.']');  // whitelist exception
						suicide('This lab could not be automatically added to existing events for the selected person because it requires manual review by an Epidemiologist.<br><br>Message moved to Exception list.');
					} else {
//error_log("TODO >>>>>>>>>>>>>>>>>> Exception with 1 match: ".print_r($whitelist_rule['type'], true)." ".print_r($exception_events, true));
						messageException($clean_actions['id'], $this_audit_id, 70, 'No existing positive labs found, Whitelist Rule requires first positive lab collection date.  Event(s) matched:<br>'.nedssLinkByEventArray($exception_events));  // whitelist exception
						suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
					}
				} else {
					// not an exception, proceed with updating existing CMR...
//error_log('TODO >>>>>>>>>>>>>>>>> Found one event matching whitelist rules, checking existing labs...');
					highlight('Found one event matching whitelist rules, checking existing labs...');
					unset($discard_labs);
					unset($update_lab);
					unset($exception_labs);
					unset($participation_matches);
					
					// get values to update into TriSano XML...
					$use_person = ($is_contact) ? reset($contact_whitelist_events) : reset($whitelist_events);
					$use_entity = $entity_ids[$use_person]['entity_id'];
					$use_ipa_part_id = $entity_ids[$use_person]['participation_id'];
					$use_event = ($is_contact) ? key($contact_whitelist_events) : key($whitelist_events);
					$use_event_type = $result_events[$use_person][$use_event]['event_type'];
					$use_event_disease_event_id = $result_events[$use_person][$use_event]['disease_event_id'];
					$use_event_disease_id = $result_events[$use_person][$use_event]['disease_id'];
					$use_event_disease_name = $result_events[$use_person][$use_event]['disease_name'];
					$use_event_workflow = $result_events[$use_person][$use_event]['workflow'];
					$use_event_record_number = $result_events[$use_person][$use_event]['record_number'];
					$use_event_date = $result_events[$use_person][$use_event]['event_date'];
					$use_event_investigator_id = $result_events[$use_person][$use_event]['investigator_id'];
					$use_event_queue = $result_events[$use_person][$use_event]['event_queue'];
//error_log('TODO Jay >>>>>>>>>>>>>> person/event: |'.$use_person.'|'.$use_event.'|'.print_r($result_events[$use_person][$use_event], true));					
					// check to see if event we're updating has the same jurisdiction as the incoming lab
					/* or not... no longer using, but keeping code just in case
					unset($same_jurisdiction);
					$same_jurisdiction = ($result_events[$use_person][$use_event]['jurisdiction_id'] == $source['jurisdiction']) ? true : false;
					
					if ($same_jurisdiction) {
					*/
					if (is_null($result_events[$use_person][$use_event]['jurisdiction_id'])) {
						// no jurisdiction set for event we're updating, throw an exception
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 69, 'Existing event to update has no jurisdiction assigned.  Matched Event:<br>'.nedssLinkByEventId($use_event));  // entry queue exception
						suicide('Could not update this event:  Existing event is not assigned to a jurisdiction.<br><br>Message moved to Exception list.');
					} elseif ((stripos($result_events[$use_person][$use_event]['state_case_status'], 'not a case') !== false) && ($allow_new_cmr == ALLOW_CMR_YES)) {
						/*
						 * Deprecated... no longer treating as Exception
						 */
						//$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						//messageException($clean_actions['id'], $this_audit_id, 69, 'Existing event is marked as "Not a Case" in TriSano.  Matched Event:<br>'.nedssLinkByEventId($use_event));  // entry queue exception
						//suicide('Could not update this event:  Existing event is not a case.<br><br>Message moved to Exception list.');
						
//error_log("TODO updateCmr >>>>>>>> checking Surveillance ");
						// event we're updating is flagged as 'not a case', result of incoming lab is positive... create new CMR instead
						auditMessage($clean_actions['id'], 35, $type, 'Existing event ('.nedssLinkByEventId($use_event).') was marked as \'Not a Case\' in TriSano.');  // attempting to create new CMR
						
						if (($is_surveillance === IS_SURVEILLANCE_YES) || ($is_surveillance === IS_SURVEILLANCE_NO)) {
							if ($valid_specimen === SPECIMEN_VALID) {
								// new CMR event
								/*
								 * For now, not using person-centric matching, so any 'new CMR' case within the 'update_cmr.php' script 
								 * will create a new CMR & new person anyway, not a new CMR for an existing person.
								 * 
								 * May switch later, so leaving the code here for possible future use...
								 *
								$use_person = reset($person_search);
								$use_entity = $entity_ids[$use_person]['entity_id'];
								$use_findperson_xml = $entity_ids[$use_person]['findperson_return'];
								
								$orig_sxe->interested_party_attributes->person->addChild('id', intval($use_person));
								$orig_sxe->interested_party_attributes->person->addChild('entity_id', intval($use_entity));
								 */

								setCommonCmrProps($orig_sxe, $whitelist_rule_tmp, $master_xml, $clean_actions, $is_pregnancy, $is_surveillance, $close_surveillance, 
									'Matching event marked "Not a Case", adding new event...',	
									'No matching events found, but could not add new event:  selected person for new event may not match incoming lab; moving to Exception list.');

								} elseif ($valid_specimen === SPECIMEN_INVALID) {
								// specimen source in invalid specimen sources list; graylilst message
								auditMessage($clean_actions['id'], 27, $type, 'Invalid specimen source');  // list change by whitelist rule
								$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
								if (@pg_query($host_pa, $exception_sql)) {
									highlight('Valid specimen source not provided.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
								} else {
									suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
								}
							} else {
								// Specimen source not in valid or invalid specimen sources list; send to exception
								$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
								messageException($clean_actions['id'], $this_audit_id, 69, 'Unexpected specimen source detected.');  // Entry queue exception
								highlight('Could not add new event:  Condition requires specimen source validation, but an invalid specimen source was provided.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
							}
						}
					} else {
//error_log("TODO updateCmr >>>>>>>> setting demographic data");
						$orig_sxe->interested_party_attributes->person->addChild('id', intval($use_person));
						$orig_sxe->interested_party_attributes->person->addChild('entity_id', intval($use_entity));
						
						// add participations node to interested_party_attributes...
						$orig_sxe->interested_party_attributes->addChild('participations');
						$orig_sxe->interested_party_attributes->participations->addChild('type', 'InterestedParty');
						$orig_sxe->interested_party_attributes->participations->addChild('event_id', $use_event); // event_id of matched CMR event
						$orig_sxe->interested_party_attributes->participations->addChild('primary_entity_id', $use_entity); // entity_id of matched person
						$orig_sxe->interested_party_attributes->participations->addChild('id', $use_ipa_part_id); // participation id of matched person's interested_party_attributes
						
						$orig_sxe->events->addChild('id', intval($use_event));
						unset($orig_sxe->events->type);
						$orig_sxe->events->addChild('type', trim($use_event_type));
						
						unset($orig_sxe->disease_events->id);
						unset($orig_sxe->disease_events->event_id);
						unset($orig_sxe->disease_events->disease_id);
						$orig_sxe->disease_events->addChild('id', intval($use_event_disease_event_id));
						$orig_sxe->disease_events->addChild('event_id', intval($use_event));
						$orig_sxe->disease_events->addChild('disease_id', intval($use_event_disease_id));
						
						if ($is_contact) {
							unset($orig_sxe->reporting_agency_attributes);  // if contact event, remove 'reporting_agency_attributes' node...
						}
						
						// if 'updateCmr' & event does not already have a 'first reported to public health' date set (i.e. legacy event), 
						// set it to be the event creation date; otherwise, remove the <first_reported_PH_date/> node to avoid validation errors
						if (is_null($result_events[$use_person][$use_event]['reported_date'])) {
							$orig_sxe->events->first_reported_PH_date = date(DATE_W3C, $result_events[$use_person][$use_event]['created_at']);
						} else {
							unset($orig_sxe->events->first_reported_PH_date);
						}
						
						// deprecated, see below // unless state case status of existing event is blank, don't send a new state case status on update
						// per Susan 7/9/2013, don't ever send state_case_status on update... for now.  rules may change this later
						//if (!is_null($result_events[$use_person][$use_event]['state_case_status'])) {
						unset($orig_sxe->events->state_case_status_id);
						//}
						
						// existing CMR event, existing person
						// check for duplicate labs against 'existing lab rules'...
						$event_labs_qry = <<<EOEX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health>
		<events>
			<id>$use_event</id>
		</events>
	</trisano_health>
</health_message>
EOEX;
						
						if ($soap_client) {
							try {
								$event_labs_result = $soap_client->findEvent(array('healthMessage' => $event_labs_qry));
							} catch (Exception $e) {
								suicide($e->getMessage(), -1, 1);
							} catch (SoapFault $f) {
								suicide($f->getMessage(), -1, 1);
							}
							$event_labs_sxe = simplexml_load_string($event_labs_result->return);
							
							$use_event_disease_name = trim($event_labs_sxe->trisano_health->diseases->disease_name);  // get disease name of event being updated for notification purposes
							
							foreach($event_labs_sxe->trisano_health->labs_attributes as $labs_attribute) {
								unset($this_lab);
								// set labs_attributes-specific variables...
								$this_lab['lab_name'] = trim($labs_attribute->place->name);
								$this_lab['place_entity_id'] = intval(trim($labs_attribute->place->entity_id));
								$this_lab['place_id'] = intval(trim($labs_attribute->place->id));
								$match_lab_name = (strtolower($this_lab['lab_name']) == strtolower($source['lab_name'])) ? true : false;
								
								foreach ($labs_attribute->lab_results as $lab_result) {
									// set lab_results-specific variables...
									$this_lab['id'] = intval(trim($lab_result->id));
									$this_lab['date_collected'] = ((isset($lab_result->collection_date) && (strlen(trim($lab_result->collection_date)) > 0)) ? date("Y-m-d", strtotime(trim($lab_result->collection_date))) : null);
									$this_lab['specimen_source'] = ((isset($lab_result->specimen_source_id) && (strlen(trim($lab_result->specimen_source_id)) > 0)) ? intval(trim($lab_result->specimen_source_id)) : null);
									$this_lab['test_result'] = ((isset($lab_result->test_result_id) && (strlen(trim($lab_result->test_result_id)) > 0)) ? intval(trim($lab_result->test_result_id)) : null);
									$this_lab['organism'] = ((isset($lab_result->organism_id) && (strlen(trim($lab_result->organism_id)) > 0)) ? intval(trim($lab_result->organism_id)) : null);
									$this_lab['result_value'] = ((isset($lab_result->result_value) && (strlen(trim($lab_result->result_value)) > 0)) ? trim($lab_result->result_value) : null);
									$this_lab['units'] = ((isset($lab_result->units) && (strlen(trim($lab_result->units)) > 0)) ? trim($lab_result->units) : null);
									$this_lab['comment'] = ((isset($lab_result->comment) && (strlen(trim($lab_result->comment)) > 0)) ? trim($lab_result->comment) : null);
									$this_lab['ref_range'] = ((isset($lab_result->reference_range) && (strlen(trim($lab_result->reference_range)) > 0)) ? trim($lab_result->reference_range) : null);
									$this_lab['test_status'] = ((isset($lab_result->test_status_id) && (strlen(trim($lab_result->test_status_id)) > 0)) ? intval(trim($lab_result->test_status_id)) : null);
									$this_lab['master_loinc'] = ((isset($lab_result->loinc_code) && (strlen(trim($lab_result->loinc_code)) > 0)) ? trim($lab_result->loinc_code) : null);
									$this_lab['accession_no'] = ((isset($lab_result->accession_no) && (strlen(trim($lab_result->accession_no)) > 0)) ? trim($lab_result->accession_no) : null);
//error_log("TODO updateCmr >>>>>>>> lab result id: ".$this_lab['id']);
									
									$match_date_collected = ($this_lab['date_collected'] == $source['date_collected']) ? true : false;
									$match_specimen_source = ($this_lab['specimen_source'] == $source['specimen_source']) ? true : false;
									$match_test_status = ($this_lab['test_status'] == $source['test_status']) ? true : false;
									$match_master_loinc = ($this_lab['master_loinc'] == $source['master_loinc']) ? true : false;
									$match_accession_no = ($this_lab['accession_no'] == $source['accession_no']) ? true : false;
									$upgrade_test_status = (intval($this_lab['test_status']) <= intval($source['test_status'])) ? true : false;
									
									$match_test_results = true;
									$match_organism = ($this_lab['organism'] == $source['organism']) ? true : false;
									$throw_arup_exception = false;
									if ($is_arup && $is_stitchable) {
										if (is_null($source['result_value']) && is_null($source['comment'])) {
											// setting test result id only; no way to tell if duplicate, so assume it is not
											$match_test_results = false;
										} elseif (is_null($source['result_value'])) {
											// setting comment
											if (is_null($this_lab['comment']) || (stripos($this_lab['comment'], $source['comment']) === false)) {
												$match_test_results = false;
											}
										} else {
											// setting result value
											if (is_null($this_lab['result_value'])) {
												$match_test_results = false;
											} else {
												if ($this_lab['result_value'] != $source['result_value']) {
													$match_test_results = false;
													$throw_arup_exception = true;
												}
											}
										}
									} else {
										if (is_null($source['result_value']) && is_null($this_lab['result_value'])) {
											// result value blank in both incoming lab & existing lab, defer to test result id
											if ($this_lab['test_result'] != $source['test_result']) {
												$match_test_results = false;
											}
										} elseif (is_null($source['result_value']) || is_null($this_lab['result_value'])) {
											// result value in either ELR message or current lab being evaluated is blank, results are different
											$match_test_results = false;
										} else {
											// compare result value
											if ($this_lab['result_value'] != $source['result_value']) {
												$match_test_results = false;
												$throw_arup_exception = true;
											}
										}
									}
//error_log("TODO updateCmr >>>>>>>> checking if lab should be discarded");
									
									if ($match_lab_name && $match_date_collected && $match_accession_no && $match_master_loinc && $match_test_results && $match_organism) {
										if ($match_test_status) {
											// discard
											$discard_labs[] = $this_lab['id'];
										} else {
											// update test status
											// check test status priority, discard if same/lower
											if ($upgrade_test_status) {
												$update_lab = array('lab_id' => $this_lab['id'], 'result_value' => $this_lab['result_value'], 'units' => $this_lab['units'], 'comment' => $this_lab['comment'], 'ref_range' => $this_lab['ref_range']);
												$participation_matches[intval(trim($lab_result->participation_id))] = array('place_entity_id' => $this_lab['place_entity_id'], 'place_id' => $this_lab['place_id']);
											} else {
												$discard_labs[] = $this_lab['id'];
											}
										}
									} elseif (!$match_lab_name && $match_date_collected && $match_master_loinc && $match_test_results && $match_organism) {
										// same test & results, but from different lab... discard
										$discard_labs[] = $this_lab['id'];
									} elseif ($is_arup && $is_stitchable && $match_lab_name && $match_date_collected && $match_accession_no && $match_master_loinc) {
										// stitch ARUP results
										$update_lab = array('lab_id' => $this_lab['id'], 'result_value' => $this_lab['result_value'], 'units' => $this_lab['units'], 'comment' => $this_lab['comment'], 'ref_range' => $this_lab['ref_range']);
										$participation_matches[intval(trim($lab_result->participation_id))] = array('place_entity_id' => $this_lab['place_entity_id'], 'place_id' => $this_lab['place_id']);
									} else {
										// add new lab
									}
									
									if ($is_arup && $match_master_loinc && $match_date_collected && $match_accession_no && $match_organism && !$match_test_results && $throw_arup_exception) {
										// if ARUP & same collection date but different results for same Master LOINC, move to exception
										$exception_labs[] = $this_lab['id'];
									}
																	   
									#debug echo '<hr>';
									#debug echo '<br><strong>$source:<pre>';
									#debug print_r($source);
									#debug echo '</pre></strong>';
									
									#debug echo '<br><strong>$this_lab:<pre>';
									#debug print_r($this_lab);
									#debug echo '</pre></strong>';
									
									#debug echo '<br><strong>$match_*:<pre>';
									#debug print_r(array("lab_name" => $match_lab_name, "date_collected" => $match_date_collected, "specimen_source" => $match_specimen_source, "test_type" => $match_test_type, "test_results" => $match_test_results, "test_status" => $match_test_status));
									#debug echo '</pre></strong>';
									
								}
							}
									
							if (isset($exception_labs) && is_array($exception_labs) && (count($exception_labs) > 0)) {
//error_log("TODO updateCmr >>>>>>>> exception labs");
								// lab can't be updated due to an exception
								$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
								messageException($clean_actions['id'], $this_audit_id, 69, 'Test results changed for same sample collection time.  Matched Event:<br>'.nedssLinkByEventId($use_event));  // entry queue exception
								suicide('An exception occurred while attempting to update this lab.<br><br>Message moved to Exception list.');
							} elseif (isset($discard_labs) && is_array($discard_labs) && (count($discard_labs) > 0)) {
error_log("TODO updateCmr >>>>>>>> discarded labs");
								// at least one event was an exact match, discard as duplicate
								auditMessage($clean_actions['id'], 7, $type); // duplicate message
								$discard_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.ASSIGNED_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = '.$clean_actions['id'].', deleted = 1, event_id = '.$use_event.', lab_result_id = '.intval(reset($discard_labs)).' WHERE id = '.$clean_actions['id'].';';
								if (@pg_query($host_pa, $discard_sql)) {
									highlight('Incoming message detected as duplicate.  Discarding message...', 'ui-icon-elrsuccess');
								} else {
									suicide('A database error occurred while validating whitelist rules.  Please contact a system administrator.', 1);
								}
							} elseif (isset($update_lab) && !is_null($update_lab['lab_id'])) {
error_log("TODO updateCmr >>>>>>>> updating labs");
								// updating lab results with additional data, add a lab_result ID...
								
								// set up participation data
								unset($this_paticipation_id);
								$this_paticipation_id = key($participation_matches);
								$orig_sxe->labs_attributes->place->addChild('id', intval($participation_matches[$this_paticipation_id]['place_id']));
								$orig_sxe->labs_attributes->place->addChild('entity_id', intval($participation_matches[$this_paticipation_id]['place_entity_id']));
								$orig_sxe->labs_attributes->addChild('participations');
								$orig_sxe->labs_attributes->participations->addChild('type', 'Lab');
								$orig_sxe->labs_attributes->participations->addChild('event_id', $use_event); // event_id of matched CMR event
								$orig_sxe->labs_attributes->participations->addChild('primary_entity_id', $use_entity); // entity_id of matched person
								$orig_sxe->labs_attributes->participations->addChild('secondary_entity_id', intval($participation_matches[$this_paticipation_id]['place_entity_id'])); // entity_id of lab associated with this participation
								$orig_sxe->labs_attributes->participations->addChild('id', $this_paticipation_id);
								$orig_sxe->labs_attributes->lab_results->addChild('participation_id', $this_paticipation_id);
								
								$orig_sxe->labs_attributes->lab_results->addChild('id', intval($update_lab['lab_id']));
								
								if ($is_arup && $is_stitchable) {
									// stitch together ARUP results
									highlight('Updating ARUP lab results...');
									
									// only overwrite Reference Range if this is an ARUP LOINC that sets the result value, otherwise, preserve Reference Range from NEDSS
									// can tell by checking if master XML has a labs/result_value set
									if (!isset($master_xml->labs->result_value) || empty($master_xml->labs->result_value) || (strlen(trim($master_xml->labs->result_value)) == 0)) {
										$orig_sxe->labs_attributes->lab_results->reference_range = $update_lab['ref_range'];
									}
									
									// if result value or units aren't set in the incoming lab results, duplicate them from the existing lab in TriSano,
									// otherwise, the lack of their elements in the XML causes TriSano to unset them in the stored event
									if (!isset($orig_sxe->labs_attributes->lab_results->result_value)) {
										$orig_sxe->labs_attributes->lab_results->addChild('result_value', $update_lab['result_value']);
									}
									if (!isset($orig_sxe->labs_attributes->lab_results->units)) {
										$orig_sxe->labs_attributes->lab_results->addChild('units', $update_lab['units']);
									}
									
									// check for & remove ',' value in empty parent_guardian field
									removeParentGuardianEmptyGlue($orig_sxe);
									
									// set pregnancy flag if pregnancy status interpreted from LOINC
									if ($is_pregnancy) {
										setPatientPregnant($orig_sxe, true);
									}
									
									// process lab comments
									clinicalDataToComments($update_lab['comment'], false);
								} else {
									// check for & remove ',' value in empty parent_guardian field
									removeParentGuardianEmptyGlue($orig_sxe);
									
									// set pregnancy flag if pregnancy status interpreted from LOINC
									if ($is_pregnancy) {
										setPatientPregnant($orig_sxe, true);
									}
									
									// process lab comments
									clinicalDataToComments($update_lab['comment'], false);
									
									// retain current values in NEDSS... only update test status
									highlight('Updating test status for existing lab results...');
									$orig_sxe->labs_attributes->lab_results->result_value = $update_lab['result_value'];
									$orig_sxe->labs_attributes->lab_results->units = $update_lab['units'];
									$orig_sxe->labs_attributes->lab_results->reference_range = $update_lab['ref_range'];
								}
								
								// check for valid specimen source
								if ($valid_specimen === SPECIMEN_VALID) {
//error_log("TODO updateCmr >>>>>>>> valid specimen source");
									// move updated contact info (name, address, telephone, etc.) to notes
									// check to see if person associated with CMR to update has the same name, address, and DOB; exception if not
									if (updatedContactInfoToNotes($event_labs_sxe, false, $clean_actions['id'])) {
										// same name/addr/dob
										// remove from xml so data isn't overwritten
										removeUnusedNodesForUpdate();
										
										$updatecmr_action_id = 28;
										addSNHDUpdateWhitelistSettings($whitelist_rule_tmp, $master_test_result, 
											$master_result_value, $orig_sxe, $orig_xml, $use_event_date, $use_event_disease_name,  
											$use_event_workflow, $use_event_investigator_id, $use_event_queue);
										$final_xml = $orig_sxe->asXML();
										$final_function = 'updateCmr';
									} else {
										// different name/addr/dob
										$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
										messageException($clean_actions['id'], $this_audit_id, 69, 'Lab patient did not match selected patient for CMR update.  Matched CMR:<br>'.nedssLinkByEventId($use_event));  // entry queue exception
										suicide('Could not update lab results:  selected person may not match incoming lab.<br><br>Message moved to Exception list.');
									}
								} elseif ($valid_specimen === SPECIMEN_INVALID) {
//error_log("TODO updateCmr >>>>>>>> INVALID specimen source");
									// specimen source in invalid specimen sources list; graylilst message
									auditMessage($clean_actions['id'], 27, $type, 'Invalid specimen source');  // list change by whitelist rule
									$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
									if (@pg_query($host_pa, $exception_sql)) {
										highlight('Valid specimen source not provided.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
									} else {
										suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
									}
								} else {
									// Specimen source not in valid or invalid specimen sources list; send to exception
									$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
									messageException($clean_actions['id'], $this_audit_id, 69, 'Unexpected specimen source detected.');  // Entry queue exception
									highlight('Could not add new event:  Condition requires specimen source validation, but an invalid specimen source was provided.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
								}
							} else {
								// no exact matches, insert new lab into this event
								
								// check for & remove ',' value in empty parent_guardian field
								removeParentGuardianEmptyGlue($orig_sxe);
								
								// set pregnancy flag if pregnancy status interpreted from LOINC
								if ($is_pregnancy) {
									setPatientPregnant($orig_sxe, true);
								}
								
								// process lab comments
								clinicalDataToComments($update_lab['comment'], true);
								
								// check for valid specimen source
								if ($valid_specimen === SPECIMEN_VALID) {
									// move updated contact info (name, address, telephone, etc.) to notes
									// check to see if person associated with CMR to update has the same name, address, and DOB; exception if not
									if (updatedContactInfoToNotes($event_labs_sxe, false, $clean_actions['id'])) {
										// same name/addr/dob
										// remove from xml so data isn't overwritten
										removeUnusedNodesForUpdate();
										
										$updatecmr_action_id = 22;
										addSNHDUpdateWhitelistSettings($whitelist_rule_tmp, $master_test_result, 
											$master_result_value, $orig_sxe, $orig_xml, $use_event_date, $use_event_disease_name, 
											$use_event_workflow, $use_event_investigator_id, $use_event_queue);
										$final_xml = $orig_sxe->asXML();
										$final_function = 'updateCmr';
										
										highlight('No matching labs, adding new lab & lab results to event...');
									} else {
										// different name/addr/dob
										$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
										messageException($clean_actions['id'], $this_audit_id, 69, 'Lab patient did not match selected patient for CMR update.  Matched CMR:<br>'.nedssLinkByEventId($use_event));  // entry queue exception
										suicide('Could not add new lab results to event:  selected person may not match incoming lab.<br><br>Message moved to Exception list.');
									}
								} elseif ($valid_specimen === SPECIMEN_INVALID) {
									// specimen source in invalid specimen sources list; graylilst message
									auditMessage($clean_actions['id'], 27, $type, 'Invalid specimen source');  // list change by whitelist rule
									$exception_sql = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.GRAY_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = NULL, deleted = 0, event_id = 0, lab_result_id = 0 WHERE id = '.$clean_actions['id'].';';
									if (@pg_query($host_pa, $exception_sql)) {
										highlight('Valid specimen source not provided.<br><br>Message moved to Gray list...', 'ui-icon-elrsuccess');
									} else {
										suicide('A database error occurred while attempting to graylist incoming message.  Please contact a system administrator.', 1);
									}
								} else {
									// Specimen source not in valid or invalid specimen sources list; send to exception
									$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
									messageException($clean_actions['id'], $this_audit_id, 69, 'Unexpected specimen source detected.');  // Entry queue exception
									highlight('Could not add new event:  Condition requires specimen source validation, but an invalid specimen source was provided.<br><br>Message moved to Exception list.', 'ui-icon-elrerror');
								}
							}
						}
					}
					/* } else {
						// jurisdiction of incoming lab does not match existing event
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 69, 'Lab jurisdiction did not match selected event.  Matched Event:<br>'.nedssLinkByEventId($use_event));  // entry queue exception
						suicide('Could not update this event:  Jurisdiction for lab results does not match.<br><br>Message moved to Exception list.');
					} */
				}
			}
			
			#debug echo '<br>Whitelist Events:<br><pre>';
			#debug print_r($whitelist_events);
			#debug echo '</pre><br>Never New Events:<br><pre>';
			#debug echo '</pre>';
		}
		
		if (isset($final_xml) && isset($final_function)) {
			/**
			 * whitelist rules checked, xml is set, and we know if we're doing an 'updateCmr' or an 'addCmr'
			 * call the web service with the prepared XML & function name
			 */
			 
			// wrap trisano xml with health_message node & required params...
			$final_xml = str_ireplace('<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', str_ireplace('</trisano_health>', '</trisano_health></health_message>', str_ireplace('<trisano_health>', '<health_message><username>9999</username><system>TRISANO</system><trisano_health>', str_ireplace("\xC2\xA0", ' ', $final_xml))));
			
			#debug echo '<br><strong>Final XML:<pre>';
			#debug print_r(htmlentities(formatXml($final_xml), ENT_QUOTES, 'UTF-8'));
			#debug echo '</pre></strong>';
			
			try {
				$final_client = @new SoapClient($props['sqla_wsdl_url']);
			} catch (Exception $e) {
				suicide($e, -1, 1);
			} catch (SoapFault $f) {
				suicide($f, -1, 1);
			}
			
			if ($final_client) {
				try {
error_log('TODO >>>>>update_cmr, calling final_function: '.$final_function);
//error_log('TODO >>>>> final_xml: '.formatXml($final_xml));
					$final_result = $final_client->$final_function(array('healthMessage' => $final_xml));
				} catch (Exception $e) {
					suicide('Unable to process message.  The following errors occurred:<pre>'.$e.'</pre><br><br>XML Sent:<br><pre>'.htmlentities(formatXml($final_xml)).'</pre>', -1, 1);
				} catch (SoapFault $f) {
					suicide('Unable to process message.  The following errors occurred:<pre>'.$f.'</pre><br><br>XML Sent:<br><pre>'.htmlentities(formatXml($final_xml)).'</pre>', -1, 1);
				}
				$final_return = simplexml_load_string($final_result->return);
				
				if (getTrisanoReturnStatus($final_return->status_message)->status === false) {
					// a trisano error of some sort occurred
					$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
					messageException($clean_actions['id'], $this_audit_id, 66, getTrisanoReturnStatus($final_return->status_message)->errors);  // unable to xmit trisano XML
					
					// log the NEDSS XML sent to NEDSS
					if ($new_cmr) {
						auditXML(null, $final_xml, $this_audit_id, false);
					} else {
						auditXML($event_labs_sxe->asXML(), $final_xml, $this_audit_id, true);
					}
					
					suicide('A TriSano error occured while attempting to add/update this message.<br><br>Message moved to Exception list.');
				} else {
					// to obtain lab ID of lab_results just affected (either inserted or updated), 
					// get back list of lab_results elements and sort by date updated
					$final_labid_arr = $final_return->xpath('//lab_results');
					foreach ($final_labid_arr as $final_lab_obj => $final_lab_val) {
						$final_lab_ids[intval($final_lab_val->id)] = strtotime(trim($final_lab_val->updated_at));
					}
					arsort($final_lab_ids, SORT_NUMERIC);
					reset($final_lab_ids);
					
					$final_lab_id = intval(key($final_lab_ids));
					$final_event_id = intval(reset($final_return->xpath('trisano_health/events/id')));
					$final_jurisdiction_id = intval(reset($final_return->xpath('trisano_health/jurisdiction_attributes/place/id')));
					$final_investigator = ($new_cmr && isset($event_labs_sxe->trisano_health->events->investigator_id)) ? '' : getInvestigatorNameById(intval(trim($event_labs_sxe->trisano_health->events->investigator_id)));
					
					$final_qry = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.ASSIGNED_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = '.$clean_actions['id'].', event_id = '.$final_event_id.', lab_result_id = '.$final_lab_id.' WHERE id = '.$clean_actions['id'].';';
					if (@pg_query($host_pa, $final_qry)) {
						highlight('Successfully processed message!<br><br><button type=\'button\' class=\'emsa_btn_viewnedss\' id=\'emsa_btn_viewnedss_'.$final_event_id.'\' value=\''.$final_event_id.'\'>View in TriSano</button><br>', 'ui-icon-elrsuccess');
					} else {
						suicide('Message successfully processed, but a database error occurred while updating ELR.  Please notify a system administrator.<br><br><em>Event ID '.$final_event_id.', Lab ID '.$final_lab_id.'</em>', 1);
					}
					
					// record an audit for the event, based on the derived updatecmr_action_id above
					$this_audit_id = auditMessage($clean_actions['id'], $updatecmr_action_id, $type);
					
					// log the NEDSS XML sent to NEDSS
					if ($new_cmr) {
						auditXML(null, $final_xml, $this_audit_id, false);
					} else {
						auditXML($event_labs_sxe->asXML(), $final_xml, $this_audit_id, true);
					}
					
					/**
					 * Post-add/update notification handling
					 *
					 */
					// set up notify params now so we don't have to have two sets of notification logic later...
					if($new_event_type == 'ae') {
						$target_event_type = 'AssessmentEvent';
					} else {
						$target_event_type = 'MorbidityEvent';
					}
					
					$notify_params = array(
						'event_id' => $final_event_id,
						'jurisdiction_id' => $final_jurisdiction_id,
						'condition' => (($new_cmr) ? $master_condition : $use_event_disease_name),
						'test_type' => $master_test_type,
						'test_result' => $master_test_result,
						'event_type' => (($new_cmr) ? $target_event_type : $use_event_type),
						'investigator' => $final_investigator);
					
					if ($new_cmr) {
						$notify_params['record_number'] = trisanoRecordNumberByEventId($final_event_id);
					} else {
						$notify_params['record_number'] = $use_event_record_number;
					}
					
					// prepare notification rules
					$nc = new NotificationContainer();

					$nc->system_message_id		= $clean_actions['id'];
					$nc->nedss_event_id			= $notify_params['event_id'];
					$nc->nedss_record_number	= $notify_params['record_number'];
					$nc->is_surveillance		= (($is_surveillance === IS_SURVEILLANCE_YES) ? true : false);
					$nc->is_immediate			= $is_immediate;
					$nc->is_state				= $notify_state;
					$nc->is_pregnancy			= $is_pregnancy;
					$nc->is_automated			= $clean_actions['is_automation'];
					$nc->is_new_cmr				= $new_cmr;
					$nc->is_event_closed		= ((!$new_cmr && (($use_event_workflow == 'closed') || ($use_event_workflow == 'approved_by_lhd'))) ? true : false);
					$nc->condition				= $notify_params['condition'];
					$nc->jurisdiction			= $notify_params['jurisdiction_id'];
					$nc->test_type				= $notify_params['test_type'];
					$nc->test_result			= $notify_params['test_result'];
					$nc->result_value			= $source['result_value'];
					$nc->investigator			= $notify_params['investigator'];
					$nc->master_loinc			= $master_loinc_code;
					$nc->specimen				= trim($master_xml->labs->specimen_source);;
					$nc->event_type				= $notify_params['event_type'];
					
					try {
						$nc->logNotification();  // run rules, generate any appropriate notifications
					} catch (NotificationDatabaseException $e) {
						suicide($e->getMessage(), 1);
					} catch (NotificationValidationException $e) {
						suicide($e->getMessage());
					} catch (Exception $e) {
						suicide($e->getMessage());
					}
				}
				
				#debug echo '<br><strong style=\'color: purple;\'>Notify Params:<pre>';
				#debug print_r($notify_params);
				#debug echo '</pre></strong>';
				
				#debug echo '<br><strong style=\'color: crimson;\'>Return XML:<pre>';
				#debug print_r(htmlentities(formatXml($final_return->asXML()), ENT_QUOTES, 'UTF-8'));
				#debug echo '</pre></strong>';
			}
		}
	}
	
	unset($soap_client);
	unset($final_client);
	
?>