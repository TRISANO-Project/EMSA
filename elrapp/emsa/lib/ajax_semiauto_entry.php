<?php

	// prevent caching...
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');

	include '../../includes/app_config.php';
	
	session_write_close(); // done writing to session; prevent blocking
	
	$clean = array();
	$params = array(
		'disease' => 'integer',
		'organism' => 'integer',
		'jurisdiction' => 'integer',
		'state_case_status' => 'integer',
		'test_type' => 'integer',
		'specimen' => 'integer',
		'testresult' => 'integer',
		'resultvalue' => 'text',
		'units' => 'text',
		'comment' => 'text',
		'teststatus' => 'integer', 
		'hospitalized' => 'integer'
	);
	
	if (isset($_REQUEST['message']) && !is_null($_REQUEST['message']) && !empty($_REQUEST['message'])) {
		$clean['message'] = intval(trim($_REQUEST['message']));
	}
	
	if (!isset($clean['message'])) {
		header($_SERVER['SERVER_PROTOCOL'] . " 400 Bad Request", TRUE, 400);  // no message ID specified
		exit;
	}
	
	// sanitize expected params
	foreach ($params as $param_name => $param_type) {
		if (isset($_REQUEST[$param_name])) {
			if ($param_type === 'integer') {
				$clean[$param_name] = ((strlen(trim($_REQUEST[$param_name])) > 0) ? intval(trim($_REQUEST[$param_name])) : null);
			} else {
				$clean[$param_name] = ((strlen(trim($_REQUEST[$param_name])) > 0) ? trim($_REQUEST[$param_name]) : null);
			}
		} else {
			$clean[$param_name] = null;
		}
	}
	
	// get existing master & NEDSS XML from database for this message
	$sql_pre = 'SELECT master_xml, transformed_xml FROM '.$my_db_schema.'system_messages WHERE id = '.$clean['message'].';';
	$rs_pre = @pg_query($host_pa, $sql_pre);
	if (($rs_pre === false) || (@pg_num_rows($rs_pre) !== 1)) {
		header($_SERVER['SERVER_PROTOCOL'] . " 400 Bad Request", TRUE, 400);  // message does not exist
		exit;
	} else {
		$clean['master_xml'] = @pg_fetch_result($rs_pre, 0, 'master_xml');
		$clean['nedss_xml'] = @pg_fetch_result($rs_pre, 0, 'transformed_xml');
		@pg_free_result($rs_pre);
	}
	
	// load xml into memory for manipulation
	$master_sxe = @simplexml_load_string($clean['master_xml']);
	$nedss_sxe = @simplexml_load_string($clean['nedss_xml']);
	
	
	
	// translate & store values if set
	if (is_null($clean['disease'])) {
		$master_sxe->disease->name = '';
		unset($nedss_sxe->disease_events->disease_id);
	} else {
		$master_sxe->disease->name = trim($_SESSION['trisano_codes']['diseases'][$clean['disease']]);
		$nedss_sxe->disease_events->disease_id = $clean['disease'];
	}
	
	if (is_null($clean['organism'])) {
		$master_sxe->labs->organism = '';
		unset($nedss_sxe->labs_attributes->lab_results->organism_id);
	} else {
		$master_sxe->labs->organism = trim($_SESSION['trisano_codes']['organisms'][$clean['organism']]);
		$nedss_sxe->labs_attributes->lab_results->organism_id = $clean['organism'];
	}
	
	if (is_null($clean['jurisdiction'])) {
		$master_sxe->administrative->jurisdictionId = '';
		unset($nedss_sxe->jurisdiction_attributes->place->id);
	} else {
		$master_sxe->administrative->jurisdictionId = $clean['jurisdiction'];
		$nedss_sxe->jurisdiction_attributes->place->id = $clean['jurisdiction'];
	}
	
	if (is_null($clean['state_case_status'])) {
		$master_sxe->labs->state_case_status = '';
		unset($nedss_sxe->events->state_case_status_id);
	} else {
		$master_sxe->labs->state_case_status = trim($_SESSION['trisano_codes']['external_codes']['case'][$clean['state_case_status']]['the_code']);
		$nedss_sxe->events->state_case_status_id = $clean['state_case_status'];
	}
	
	if (is_null($clean['test_type'])) {
		$master_sxe->labs->test_type = '';
		unset($nedss_sxe->labs_attributes->lab_results->test_type_id);
	} else {
		$master_sxe->labs->test_type = trim($_SESSION['trisano_codes']['common_test_types'][$clean['test_type']]);
		$nedss_sxe->labs_attributes->lab_results->test_type_id = $clean['test_type'];
	}
	
	if (is_null($clean['specimen'])) {
		$master_sxe->labs->specimen_source = '';
		unset($nedss_sxe->labs_attributes->lab_results->specimen_source_id);
	} else {
		$master_sxe->labs->specimen_source = trim($_SESSION['trisano_codes']['external_codes']['specimen'][$clean['specimen']]['the_code']);
		$nedss_sxe->labs_attributes->lab_results->specimen_source_id = $clean['specimen'];
	}
	
	if (is_null($clean['testresult'])) {
		$master_sxe->labs->test_result = '';
		unset($nedss_sxe->labs_attributes->lab_results->test_result_id);
	} else {
		$master_sxe->labs->test_result = trim($_SESSION['trisano_codes']['external_codes']['test_result'][$clean['testresult']]['the_code']);
		$nedss_sxe->labs_attributes->lab_results->test_result_id = $clean['testresult'];
	}
	
	if (is_null($clean['resultvalue'])) {
		$master_sxe->labs->result_value = '';
		unset($nedss_sxe->labs_attributes->lab_results->result_value);
	} else {
		$master_sxe->labs->result_value = $clean['resultvalue'];
		$nedss_sxe->labs_attributes->lab_results->result_value = $clean['resultvalue'];
	}
	
	if (is_null($clean['units'])) {
		$master_sxe->labs->units = '';
		unset($nedss_sxe->labs_attributes->lab_results->units);
	} else {
		$master_sxe->labs->units = $clean['units'];
		$nedss_sxe->labs_attributes->lab_results->units = $clean['units'];
	}
	
	if (is_null($clean['comment'])) {
		$master_sxe->labs->comment = '';
		unset($nedss_sxe->labs_attributes->lab_results->comment);
	} else {
		$master_sxe->labs->comment = $clean['comment'];
		$nedss_sxe->labs_attributes->lab_results->comment = $clean['comment'];
	}
	
	if (is_null($clean['teststatus'])) {
		$master_sxe->labs->test_status = '';
		unset($nedss_sxe->labs_attributes->lab_results->test_status_id);
	} else {
		$master_sxe->labs->test_status = trim($_SESSION['trisano_codes']['external_codes']['test_status'][$clean['teststatus']]['the_code']);
		$nedss_sxe->labs_attributes->lab_results->test_status_id = $clean['teststatus'];
	}
	
	if (is_null($clean['hospitalized'])) {
		$master_sxe->hospital_info->hospitalized = '';
		unset($nedss_sxe->disease_events->hospitalized_id);
	} else {
		$master_sxe->hospital_info->hospitalized = trim($_SESSION['trisano_codes']['external_codes']['yesno'][$clean['hospitalized']]['the_code']);
		$nedss_sxe->disease_events->hospitalized_id = $clean['hospitalized'];
	}
	
	$save_sql = 'UPDATE '.$my_db_schema.'system_messages 
		SET master_xml = \''.@pg_escape_string($master_sxe->asXML()).'\', 
		transformed_xml = \''.@pg_escape_string($nedss_sxe->asXML()).'\', 
		disease = \''.@pg_escape_string(trim($_SESSION['trisano_codes']['diseases'][$clean['disease']])).'\' 
		WHERE id = '.$clean['message'].';';
	$save_rs = @pg_query($host_pa, $save_sql);
	
	if ($save_rs === false) {
		header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error", TRUE, 500);
		exit;
	} else {
		header($_SERVER['SERVER_PROTOCOL'] . " 200 OK", TRUE, 200);
		exit;
	}

?>