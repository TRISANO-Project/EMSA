<?php

	// prevent caching...
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');

	include '../../includes/app_config.php';
	include WEBROOT_URL.'/includes/emsa_functions.php';
	
	session_write_close(); // done writing to session; prevent blocking
	
	unset($results);
	unset($result_addresses);

	$clean['fname'] = (strlen(trim($_POST['fname'])) > 0) ? trim($_POST['fname']) : FALSE;
	$clean['lname'] = (strlen(trim($_POST['lname'])) > 0) ? trim($_POST['lname']) : FALSE;
	$clean['bday'] = (strlen(trim($_POST['bday'])) > 0) ? date("Y-m-d", strtotime(trim($_POST['bday']))) : FALSE;
	$clean['condition'] = (strlen(trim($_POST['condition'])) > 0) ? trim($_POST['condition']) : null;
	$clean['id'] = (strlen(trim($_POST['id'])) > 0) ? intval(trim($_POST['id'])) : FALSE;
	$clean['type'] = intval(trim($_POST['type']));
	$clean['selected_page'] = intval(trim($_POST['selected_page']));
	
	if ($clean['fname'] || $clean['lname'] || $clean['bday'] || $clean['id']) {
	
		try {
			//$results_array = getPeopleSearchResults($clean['fname'], $clean['lname'], $clean['bday'], $clean['condition']);
			$results_array = getPeopleSearchResults($clean['fname'], $clean['lname'], $clean['bday']);
			$results = $results_array['results'];
		} catch (Exception $e) {
			suicide('Unable to retrieve list of matches:<br><br>'.$e->getMessage().'.');
			$results = false;
		}
		
		$events_by_person_id = $results_array['events_by_person_id'];
		
		if (is_array($results)) {
			
			?>
				<table class="emsa_search_results_header">
					<thead>
						<tr>
							<th style="width: 24%;">Name</th>
							<th style="width: 6%;">Gender</th>
							<th style="width: 9%;">D.O.B.</th>
							<th style="width: 37%;">Addresses</th>
							<th style="width: 13%;">Telephone #s</th>
							<th style="width: 11%;">Match Quality</th>
						</tr>
					</thead>
				</table>
				<div class="emsa_sr_scroller">
				<table class="emsa_eventsearch_results">
					<tbody>
					<?php
						if (is_array($results) && (count($results) > 0)) {
							foreach ($results as $result_key => $result_item) {
						?>
							<tr class="event_match_person" id="event_match_person_<?php echo $clean['id']; ?>__<?php echo intval($result_key); ?>">
								<td style="width: 24%;">
									<?php echo htmlentities($result_item['full_name'], ENT_QUOTES, "UTF-8"); ?>
								</td>
								<td style="width: 6%;"><?php echo htmlentities($result_item['sex'], ENT_QUOTES, "UTF-8"); ?></td>
								<td style="width: 9%;"><?php echo htmlentities(trim($result_item['birth_date']), ENT_QUOTES, "UTF-8"); ?></td>
								<td style="width: 37%;">
							<?php
								if (is_array($result_item['addresses'])) {
									foreach ($result_item['addresses'] as $address_item) {
							?>
									<?php printf("%s", ((strlen(trim($address_item->street_name)) > 0) ? htmlentities($address_item->street_name, ENT_QUOTES, "UTF-8").', ' : ' ')); ?>
									<?php printf("%s", ((strlen(trim($address_item->unit_number)) > 0) ? 'Unit '.htmlentities($address_item->unit_number, ENT_QUOTES, "UTF-8").', ' : ' ')); ?>
									<?php echo htmlentities($address_item->city, ENT_QUOTES, "UTF-8"); ?>
									<?php echo htmlentities($address_item->postal_code, ENT_QUOTES, "UTF-8"); ?>
									<?php echo ((intval(trim($address_item->county_id)) > 0) ? '['.$_SESSION['trisano_codes']['external_codes']['county'][intval($address_item->county_id)]['code_description'].' County]' : ' '); ?><br>
							<?php
									}
								}
							?>
								</td>
								<td style="width: 13%;">
							<?php
								if (is_array($result_item['telephones'])) {
									foreach ($result_item['telephones'] as $telephone_item) {
							?>
									<?php echo htmlentities($telephone_item, ENT_QUOTES, "UTF-8"); ?><br>
							<?php
									}
								}
							?>
								</td>
								<td style="width: 11%;">
								<?php
									unset($this_starcount);
									unset($star_img);
									
									if ($result_item['real_score'] == 100.0) {
										$this_starcount = array('full' => 5, 'half' => FALSE);
									} elseif ($result_item['real_score'] >= 90.0) {
										$this_starcount = array('full' => 4, 'half' => TRUE);
									} elseif ($result_item['real_score'] >= 80.0) {
										$this_starcount = array('full' => 4, 'half' => FALSE);
									} elseif ($result_item['real_score'] >= 70.0) {
										$this_starcount = array('full' => 3, 'half' => TRUE);
									} elseif ($result_item['real_score'] >= 60.0) {
										$this_starcount = array('full' => 3, 'half' => FALSE);
									} elseif ($result_item['real_score'] >= 50.0) {
										$this_starcount = array('full' => 2, 'half' => TRUE);
									} elseif ($result_item['real_score'] >= 40.0) {
										$this_starcount = array('full' => 2, 'half' => FALSE);
									} elseif ($result_item['real_score'] >= 30.0) {
										$this_starcount = array('full' => 1, 'half' => TRUE);
									} elseif ($result_item['real_score'] >= 20.0) {
										$this_starcount = array('full' => 1, 'half' => FALSE);
									} elseif ($result_item['real_score'] >= 10.0) {
										$this_starcount = array('full' => 0, 'half' => TRUE);
									} else {
										$this_starcount = array('full' => 0, 'half' => FALSE);
									}
									
									if ($this_starcount['full'] === 5) {
										$star_img = '<div title="Exact Match!" style="display: inline;">';
									} else {
										$star_img = '<div title="Match Score: '.intval($result_item['real_score']).'%" style="display: inline;">';
									}
									
									for ($full_star = 0; $full_star < $this_starcount['full']; $full_star++) {
										$star_img .= '<img src="'.MAIN_URL.'/img/star_1_16.png" width="16" height="16" />';
									}
									if ($this_starcount['half']) {
										$star_img .= '<img src="'.MAIN_URL.'/img/star_2_16.png" width="16" height="16" />';
									}
									for ($empty_star = 0; $empty_star < (5-($this_starcount['full']+(($this_starcount['half']) ? 1 : 0))); $empty_star++) {
										$star_img .= '<img src="'.MAIN_URL.'/img/star_3_16.png" width="16" height="16" />';
									}
									
									$star_img .= '</div>';
									
									if ($this_starcount['full'] === 5) {
										$star_img .= '<script type="text/javascript">';
										$star_img .= '$("#event_match_person_'.$clean['id'].'__'.intval($result_key).'").addClass("person_match_found");';
										$star_img .= '$(".event_match_events_'.$clean['id'].'__'.intval($result_key).'").addClass("person_match_found");';
										$star_img .= '</script>';
									}
									
									echo $star_img;
								?>
								</td>
							</tr>
						<?php
								foreach ($events_by_person_id[intval($result_key)] as $event_by_person_key => $event_by_person_data) {
						?>
							<tr class="event_match_events event_match_events_<?php echo $clean['id']; ?>__<?php echo intval($result_key); ?>" id="event_match_events_<?php echo $clean['id']; ?>__<?php echo intval($result_key); ?>__<?php echo intval($event_by_person_key); ?>">
								<td colspan="6" style="width: 100%;">
									&bull; <a style="font-weight: bold; color: navy;" href="<?php echo $props['trisano_url'].(($event_by_person_data['event_type'] == 'Morbidity Event') ? 'cmrs/' : 'contact_events/').$event_by_person_data['event_id']; ?>" target="_blank">Record# <?php echo $event_by_person_data['record_number']; ?></a>  <?php echo $event_by_person_data['event_type'].' ('.$event_by_person_data['event_date'].') ['.$event_by_person_data['disease_name'].']'; ?>
								</td>
							</tr>
						<?php
								}
							}
						} else {
							// no results found
							echo '<tr id="person_match_'.$clean['id'].'__notfound"><td style="width: 100%;">No matches found!</td></tr>';
						}
					?>
					</tbody>
				</table>
				</div>
			<?php
		}
		unset($client);
	} else {
		echo 'Unable to perform search.  Missing name/birthdate fields.';
	}

?>