<?php

	/**
	 * override_update_cmr.php
	 * 
	 * Used to resolve ELR messages in Exception by allowing ELR staff to specify 
	 * a TriSano event to manually append the ELR lab to, rather than using normal
	 * People Search/Whitelist Rule processing.
	 *
	 * Target event must be specified by an Event ID.
     *
     *  -- OR -- if Report Number is passed in for the Event ID, the Event ID is derived from the report number
	 * 
	 * included by emsa/emsa_actions.php
	 */
	 
	// get NEDSS XML...
	$xml_qry = 'SELECT loinc_code, disease, master_xml, transformed_xml, lab_id FROM '.$my_db_schema.'system_messages WHERE id = '.$clean_actions['id'].';';
	if ($xml_rs = @pg_query($host_pa, $xml_qry)) {
		$master_xml_raw = @pg_fetch_result($xml_rs, 0, 'master_xml');
		$trisano_xml_raw = @pg_fetch_result($xml_rs, 0, 'transformed_xml');
		$master_loinc_code = @pg_fetch_result($xml_rs, 0, 'loinc_code');
		$laboratory_id = @pg_fetch_result($xml_rs, 0, 'lab_id');
		$master_condition = @pg_fetch_result($xml_rs, 0, 'disease');
		@pg_free_result($xml_rs);
		
		
		$master_xml = simplexml_load_string($master_xml_raw);
		$orig_sxe = simplexml_load_string($trisano_xml_raw);
		$master_test_type = trim($master_xml->labs->test_type);
		$master_test_result = trim($master_xml->labs->test_result);
		
		$child_local_loinc_code = trim($master_xml->labs->local_loinc_code);
		$child_local_code = trim($master_xml->labs->local_code);
		$child_loinc = ((isset($child_local_loinc_code) && !is_null($child_local_loinc_code) && !empty($child_local_loinc_code)) ? $child_local_loinc_code : $child_local_code);
		$is_pregnancy = isPregnancyIndicated($child_loinc, $laboratory_id);
		
		$source_result_value = ((isset($orig_sxe->labs_attributes->lab_results->result_value) && (strlen(trim($orig_sxe->labs_attributes->lab_results->result_value)) > 0)) ? trim($orig_sxe->labs_attributes->lab_results->result_value) : null);
	} else {
		suicide('Error:  Unable to retrieve XML for selected event.');
	}
	
	if (is_null($clean_actions['override_id']) || empty($clean_actions['override_id'])) {
		suicide('Unable to manually add this message to a TriSano event:  no Event ID specified.');
	} else {
		try {
			$soap_client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e, -1, 1);
		} catch (SoapFault $f) {
			suicide($f, -1, 1);
		}
		
		$findevent_xml = <<<EOEX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <system>TRISANO</system>
    <trisano_health>
        <events>
            <id>{$clean_actions['override_id']}</id>
        </events>
    </trisano_health>
</health_message>
EOEX;
					
		if ($soap_client) {
			try {
				$findevent_result = $soap_client->findEvent(array('healthMessage' => $findevent_xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$findevent_sxe = simplexml_load_string($findevent_result->return);
			
			if (getTrisanoReturnStatus($findevent_sxe->status_message)->status === false) {
				suicide('A TriSano error occurred while attempting to locate the specified event.');
			} else {
				if (isset($findevent_sxe->trisano_health)) {
					$use_reported_date = ((isset($findevent_sxe->trisano_health->events->first_reported_PH_date) && (strlen(trim($findevent_sxe->trisano_health->events->first_reported_PH_date)) > 0)) ? strtotime(trim($findevent_sxe->trisano_health->events->first_reported_PH_date)) : null);
					$has_state_case_status = ((isset($findevent_sxe->trisano_health->events->state_case_status_id) && (strlen(trim($findevent_sxe->trisano_health->events->state_case_status_id)) > 0)) ? true : false);
					$use_created_at = strtotime(trim($findevent_sxe->trisano_health->events->created_at));
					$use_record_number = trim($findevent_sxe->trisano_health->events->record_number);
					$use_event_workflow = trim($findevent_sxe->trisano_health->events->workflow_state);
					$use_event_disease_name = trim($findevent_sxe->trisano_health->diseases->disease_name);
					
					$orig_sxe->events->addChild('id', $clean_actions['override_id']);
					unset($orig_sxe->events->type);
					$orig_sxe->events->addChild('type', trim($findevent_sxe->trisano_health->events->type));
					
					unset($orig_sxe->disease_events->id);
					unset($orig_sxe->disease_events->event_id);
					unset($orig_sxe->disease_events->disease_id);
					$orig_sxe->disease_events->addChild('id', intval($findevent_sxe->trisano_health->disease_events->id));
					$orig_sxe->disease_events->addChild('event_id', $clean_actions['override_id']);
					$orig_sxe->disease_events->addChild('disease_id', intval($findevent_sxe->trisano_health->disease_events->disease_id));
					
					// if contact event, remove 'reporting_agency_attributes' node...
					if (stripos($findevent_sxe->trisano_health->events->type, 'contact') !== false) {
						unset($orig_sxe->reporting_agency_attributes);
					}
					
					// if event does not already have a 'first reported to public health' date set (i.e. legacy event),
					// set it to be the event creation date; otherwise, remove the <first_reported_PH_date/> node to avoid validation errors
					if (is_null($use_reported_date)) {
						$orig_sxe->events->first_reported_PH_date = date(DATE_W3C, $use_created_at);
					} else {
						unset($orig_sxe->events->first_reported_PH_date);
					}
					
					// unless state case status of existing event is blank, don't send a new state case status on update
					if ($has_state_case_status) {
						unset($orig_sxe->events->state_case_status_id);
					}
					
					removeParentGuardianEmptyGlue($orig_sxe);  // check for & remove ',' value in empty parent_guardian field
					clinicalDataToComments(null, true);  // process lab comments
					updatedContactInfoToNotes($findevent_sxe, false, $clean_actions['id']);  // move updated contact info (name, address, telephone, etc.) to notes
					removeUnusedNodesForUpdate();
					
					// set pregnancy flag if pregnancy status interpreted from LOINC
					if ($is_pregnancy) {
						setPatientPregnant($orig_sxe, true);
					}
					
					// wrap trisano xml with health_message node & required params...
					$final_xml = $orig_sxe->asXML();
					$final_xml = str_ireplace('<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', str_ireplace('</trisano_health>', '</trisano_health></health_message>', str_ireplace('<trisano_health>', '<health_message><username>9999</username><system>TRISANO</system><trisano_health>', str_ireplace("\xC2\xA0", ' ', $final_xml))));
					
					try {
						$final_result = $soap_client->updateCmr(array('healthMessage' => $final_xml));
					} catch (Exception $e) {
						suicide('Unable to process message.  The following errors occurred:<pre>'.$e.'</pre><br><br>XML Sent:<br><pre>'.htmlentities(formatXml($final_xml)).'</pre>', -1, 1);
					} catch (SoapFault $f) {
						suicide('Unable to process message.  The following errors occurred:<pre>'.$f.'</pre><br><br>XML Sent:<br><pre>'.htmlentities(formatXml($final_xml)).'</pre>', -1, 1);
					}
					$final_return = simplexml_load_string($final_result->return);
					
					if (getTrisanoReturnStatus($final_return->status_message)->status === false) {
						// a trisano error of some sort occurred
						$this_audit_id = auditMessage($clean_actions['id'], 13, $type);  // exception
						messageException($clean_actions['id'], $this_audit_id, 66, getTrisanoReturnStatus($final_return->status_message)->errors);  // unable to xmit trisano XML
						
						auditXML($findevent_sxe->asXML(), $final_xml, $this_audit_id, true);  // log the NEDSS XML sent to NEDSS
						
						suicide('A TriSano error occured while attempting to add/update this message.<br><br>Message moved to Exception list.');
					} else {
						// to obtain lab ID of lab_results just affected (either inserted or updated), 
						// get back list of lab_results elements and sort by date updated
						$final_labid_arr = $final_return->xpath('//lab_results');
						foreach ($final_labid_arr as $final_lab_obj => $final_lab_val) {
							$final_lab_ids[intval($final_lab_val->id)] = strtotime(trim($final_lab_val->updated_at));
						}
						arsort($final_lab_ids, SORT_NUMERIC);
						reset($final_lab_ids);
						
						$final_lab_id = intval(key($final_lab_ids));
						$final_event_id = intval(reset($final_return->xpath('trisano_health/events/id')));
						$final_jurisdiction_id = intval(reset($final_return->xpath('trisano_health/jurisdiction_attributes/place/id')));
						
						$final_qry = 'UPDATE '.$my_db_schema.'system_messages SET final_status = '.ASSIGNED_STATUS.', assigned_date = \''.date("Y-m-d H:i:s", time()).'\', status = 0, external_system_id = '.$clean_actions['id'].', event_id = '.$final_event_id.', lab_result_id = '.$final_lab_id.' WHERE id = '.$clean_actions['id'].';';
						if (@pg_query($host_pa, $final_qry)) {
							highlight('Successfully processed message!<br><br><button type=\'button\' class=\'emsa_btn_viewnedss\' id=\'emsa_btn_viewnedss_'.$final_event_id.'\' value=\''.$final_event_id.'\'>View in TriSano</button><br>', 'ui-icon-elrsuccess');
						} else {
							suicide('Message successfully processed, but a database error occurred while updating ELR.  Please notify a system administrator.<br><br><em>Event ID '.$final_event_id.', Lab ID '.$final_lab_id.'</em>', 1);
						}
						
						$this_audit_id = auditMessage($clean_actions['id'], 22, $type);  // record an audit for the lab append
						
						auditXML($findevent_sxe->asXML(), $final_xml, $this_audit_id, true);  // log the NEDSS XML sent to NEDSS
						
						/**
						 * Post-add/update notification handling
						 *
						 */
						// set up notify params now so we don't have to have two sets of notification logic later...
						$notify_params = array(
							'event_id' => $final_event_id,
							'jurisdiction_id' => $final_jurisdiction_id,
							'condition' => $use_event_disease_name,
							'test_type' => $master_test_type,
							'test_result' => $master_test_result,
							'event_type' => trim($findevent_sxe->trisano_health->events->type));
						
						$notify_params['record_number'] = $use_record_number;
						
						// prepare notification rules
						$nc = new NotificationContainer();

						$nc->system_message_id		= $clean_actions['id'];
						$nc->nedss_event_id			= $notify_params['event_id'];
						$nc->nedss_record_number	= $notify_params['record_number'];
						$nc->is_surveillance		= false;  // needs to be fixed, was apparently never built into original code for manual override
						$nc->is_immediate			= false;  // needs to be fixed, was apparently never built into original code for manual override
						$nc->is_state				= false;  // needs to be fixed, was apparently never built into original code for manual override
						$nc->is_pregnancy			= $is_pregnancy;
						$nc->is_automated			= false;  // manual override, never automated
						$nc->is_new_cmr				= false;  // always an update
						$nc->is_event_closed		= ((($use_event_workflow == 'closed') || ($use_event_workflow == 'approved_by_lhd')) ? true : false);
						$nc->condition				= $notify_params['condition'];
						$nc->jurisdiction			= $notify_params['jurisdiction_id'];
						$nc->test_type				= $notify_params['test_type'];
						$nc->test_result			= $notify_params['test_result'];
						$nc->result_value			= $source_result_value;
						$nc->investigator			= $notify_params['investigator'];  // needs to be fixed, was apparently never built into original code for manual override
						$nc->master_loinc			= $master_loinc_code;
						$nc->specimen				= trim($master_xml->labs->specimen_source);
						$nc->event_type				= $notify_params['event_type'];
						
						try {
							$nc->logNotification();  // run rules, generate any appropriate notifications
						} catch (NotificationDatabaseException $e) {
							suicide($e->getMessage(), 1);
						} catch (NotificationValidationException $e) {
							suicide($e->getMessage());
						} catch (Exception $e) {
							suicide($e->getMessage());
						}
					}
					
				} else {
					suicide('Unable to manually add message to a TriSano event:  specified TriSano event not found.');
				}
			}
		}
	}
	
?>