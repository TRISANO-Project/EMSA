<?php
	
	$index_url = $main_url."index.php?selected_page=".$selected_page."&type=".$type."&focus=".$clean_actions['id'];
	$xml_qry = "SELECT lab_id, master_xml FROM ".$my_db_schema."system_messages WHERE id = ".intval($clean_actions['id']).";";
	$lab_id = @pg_fetch_result(@pg_query($host_pa, $xml_qry), 0, "lab_id");
	$master_xml = @pg_fetch_result(@pg_query($host_pa, $xml_qry), 0, "master_xml");
	$sxe_master_xml = simplexml_load_string($master_xml);
	
	$result['id'] = $clean_actions['id'];
	
?>

<script type="text/javascript">
	$(function() {
		$("#search_form").hide();
		
		$("#edit_dob").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			closeText: "Done",
			yearRange: "1900:+0"
		});
	
		$("#edit_date_reported, #edit_date_collected").datetimepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			closeText: "Done",
			yearRange: "1900:+0",
			timeFormat: "HH:mm:ss z", 
			timezoneList: [
				{ value: -420, label: 'Mountain' }
			]
		});
	
		$("#edit_save").button({
			icons: {
				primary: "ui-icon-elrsave"
			}
		});
		
		$("#edit_cancel").button({
				icons: {
					primary: "ui-icon-elrcancel"
				}
			}).click(function(e) {
				e.preventDefault();
				var cancelAction = "<?php echo $index_url; ?>";
				window.location.href = cancelAction;
		});
	});
</script>
<script type="text/javascript" src="/elrapp/js/jquery-ui-timepicker-addon.js"></script>

<style type="text/css">
	#edit_lab_form label { display: inline-block; font-weight: bold; width: 150px !important; text-align: right; }
	#edit_lab_form input, #edit_lab_form select { margin: 4px; min-width: 250px; }
	#ui-datepicker-div { box-shadow: 2px 2px 15px 2px #666666; }
	.audit_log th, .audit_log td { text-align: left; }
	.audit_log th { color: dimgray; border-bottom: 2px darkgray solid; }
	.audit_log td { color: black; border-bottom: 1px lightgray solid; }
	
	/* css for timepicker */
	.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
	.ui-timepicker-div dl { text-align: left; }
	.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
	.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
	.ui-timepicker-div td { font-size: 90%; }
	.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

	.ui-timepicker-rtl{ direction: rtl; }
	.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
	.ui-timepicker-rtl dl dt{ float: right; clear: right; }
	.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }
</style>

<fieldset class="emsa-list ui-widget ui-widget-content ui-corner-all">
	<legend class="emsa-list-legend ui-widget-content ui-corner-all">Current Errors/Pending Flags</legend>
	<div class="emsa_results_container">
		<div class="exception_details">
			<table class="audit_log">
				<thead>
					<tr>
						<th>Error Type</th>
						<th>Error Description</th>
						<th>Error Details</th>
					</tr>
				</thead>
				<tbody>

		<?php
			
			$current_sql = "SELECT se.description AS description, sme.info AS info, ss.name AS type FROM ".$my_db_schema."system_message_exceptions sme 
				INNER JOIN ".$my_db_schema."system_exceptions se ON (sme.exception_id = se.exception_id) 
				INNER JOIN ".$my_db_schema."system_statuses ss ON (se.exception_type_id = ss.id) 
				WHERE sme.system_message_id = ".intval($result['id'])." 
				ORDER BY sme.id;";
			$current_rs = @pg_query($host_pa, $current_sql);
			if ($current_rs) {
				if (pg_num_rows($current_rs) > 0) {
					while ($current_row = @pg_fetch_object($current_rs)) {
						echo "<tr><td>".htmlentities($current_row->type)."</td><td>".htmlentities($current_row->description)."</td><td>".htmlentities($current_row->info)."</td></tr>";
					}
				} else {
					echo "<tr><td colspan=\"3\"><em>Message has no current errors/pending flags</em></td></tr>";
				}
				@pg_free_result($current_rs);
			} else {
				echo "<tr><td colspan=\"3\"><em>Unable to retrieve list of errors</em></td></tr>";
			}
			
			
		?>

				</tbody>
			</table>
		</div>
	</div>
</fieldset>

<fieldset class="emsa-list ui-widget ui-widget-content ui-corner-all">
	<legend class="emsa-list-legend ui-widget-content ui-corner-all">Edit Message</legend>
	<div class="emsa_results_container">
		<form id="edit_lab_form" method="POST" action="<?php echo $index_url; ?>">
			<h3>Patient Information</h3>
			<label for="edit_last_name">Last Name:</label> <input class="ui-corner-all" type="text" name="edit_last_name" id="edit_last_name" value="<?php echo $sxe_master_xml->person->last_name; ?>" /><br>
			<label for="edit_first_name">First Name:</label> <input class="ui-corner-all" type="text" name="edit_first_name" id="edit_first_name" value="<?php echo $sxe_master_xml->person->first_name; ?>" /><br>
			<label for="edit_middle_name">Middle Name:</label> <input class="ui-corner-all" type="text" name="edit_middle_name" id="edit_middle_name" value="<?php echo $sxe_master_xml->person->middle_name; ?>" /><br>
			<label for="edit_gender">Gender:</label>
				<select name="edit_gender" id="edit_gender" class="ui-corner-all">
					<option value="" <?php echo ((strlen(trim($sxe_master_xml->person->gender)) < 1) ? " selected" : ""); ?>>--</option>
				<?php
					$gender_qry = 'SELECT vm.concept AS master_label, vc.concept AS child_label 
						FROM '.$my_db_schema.'vocab_master_vocab vm 
						INNER JOIN '.$my_db_schema.'vocab_child_vocab vc ON (vm.id = vc.master_id AND vc.lab_id = '.intval($lab_id).') 
						WHERE (vm.category = elr.vocab_category_id(\'gender\')) ORDER BY vm.concept;';
					$gender_rs = @pg_query($host_pa, $gender_qry);
					if ($gender_rs) {
						while ($gender_row = @pg_fetch_object($gender_rs)) {
							if ($gender_row->child_label == $sxe_master_xml->person->gender) {
								echo '<option value="'.$gender_row->child_label.'" selected>'.$gender_row->master_label.' ['.$sxe_master_xml->labs->lab.': '.$gender_row->child_label.']</option>';
							} else {
								echo '<option value="'.$gender_row->child_label.'">'.$gender_row->master_label.' ['.$sxe_master_xml->labs->lab.': '.$gender_row->child_label.']</option>';
							}
						}
					} else {
						suicide('Unable to retrieve list of genders', 1, 1);
					}
					@pg_free_result($gender_rs);
				?>
				</select><br>
			<label for="edit_street_name">Street Address:</label> <input class="ui-corner-all" type="text" name="edit_street_name" id="edit_street_name" value="<?php echo $sxe_master_xml->person->street_name; ?>" /><br>
			<label for="edit_unit">Unit:</label> <input class="ui-corner-all" type="text" name="edit_unit" id="edit_unit" value="<?php echo $sxe_master_xml->person->unit; ?>" /><br>
			<label for="edit_city">City:</label> <input class="ui-corner-all" type="text" name="edit_city" id="edit_city" value="<?php echo $sxe_master_xml->person->city; ?>" /><br>
			<label for="edit_state">State:</label> <input class="ui-corner-all" type="text" name="edit_state" id="edit_state" value="<?php echo $sxe_master_xml->person->state; ?>" /><br>
			<label for="edit_county">County:</label> <input class="ui-corner-all" type="text" name="edit_county" id="edit_county" value="<?php echo $sxe_master_xml->person->county; ?>" /><br>
			<label for="edit_zip">ZIP Code:</label> <input class="ui-corner-all" type="text" name="edit_zip" id="edit_zip" value="<?php echo $sxe_master_xml->person->zip; ?>" /><br>
			<label for="edit_country">Country:</label> <input class="ui-corner-all" type="text" name="edit_country" id="edit_country" value="<?php echo $sxe_master_xml->person->country; ?>" /><br><br>
			<label for="edit_area_code">Area Code:</label> <input class="ui-corner-all" type="text" name="edit_area_code" id="edit_area_code" value="<?php echo $sxe_master_xml->person->area_code; ?>" /><br>
			<label for="edit_telephone">Telephone:</label> <input class="ui-corner-all" type="text" name="edit_telephone" id="edit_telephone" value="<?php echo $sxe_master_xml->person->phone; ?>" /><br><br>
			<label for="edit_dob">Date of Birth:</label> <input class="ui-corner-all" type="text" name="edit_dob" id="edit_dob" value="<?php echo date("m/d/Y", strtotime($sxe_master_xml->person->date_of_birth)); ?>" placeholder="MM/DD/YYYY" /><br><br>
			<h3>Lab Information</h3>
		<?php if ($type == EXCEPTIONS_STATUS) { ?>
			<label for="edit_date_reported">Date/Time Reported:</label> <input class="ui-corner-all" type="text" name="edit_date_reported" id="edit_date_reported" value="<?php echo date("m/d/Y H:i:s O", strtotime($sxe_master_xml->reporting->report_date)); ?>" placeholder="MM/DD/YYYY HH:MM:SS -0700" /><br>
			<label for="edit_test_type">Test Type:</label>
				<select name="edit_test_type" id="edit_test_type" class="ui-corner-all">
					<option value="" <?php echo ((strlen(trim($sxe_master_xml->labs->test_type)) < 1) ? " selected" : ""); ?>>--</option>
				<?php
					$testtype_qry = 'SELECT vm.concept AS master_label, m2a.coded_value AS app_concept
						FROM '.$my_db_schema.'vocab_master_vocab vm 
						INNER JOIN '.$my_db_schema.'vocab_master2app m2a ON (vm.id = m2a.master_id)
						WHERE (vm.category = elr.vocab_category_id(\'test_type\')) AND (m2a.coded_value IS NOT NULL) ORDER BY vm.concept;';
					$testtype_rs = @pg_query($host_pa, $testtype_qry);
					if ($testtype_rs) {
						while ($testtype_row = @pg_fetch_object($testtype_rs)) {
							if ($testtype_row->master_label == $sxe_master_xml->labs->test_type) {
								echo '<option value="'.$testtype_row->app_concept.'" selected>'.$testtype_row->app_concept.'</option>';
							} else {
								echo '<option value="'.$testtype_row->app_concept.'">'.$testtype_row->app_concept.'</option>';
							}
						}
					} else {
						suicide('Unable to retrieve list of test types', 1, 1);
					}
					@pg_free_result($testtype_rs);
				?>
				</select><br>
			<label for="edit_test_name">Local Test Name:</label> <input class="ui-corner-all" type="text" name="edit_test_name" id="edit_test_name" value="<?php echo $sxe_master_xml->labs->local_test_name; ?>" /><br>
			<label for="edit_result_value">Local Result Value:</label> <input class="ui-corner-all" type="text" name="edit_result_value" id="edit_result_value" value="<?php echo $sxe_master_xml->labs->local_result_value; ?>" /><br>
		<?php } else { ?>
			<input type="hidden" name="edit_date_reported" value="<?php echo date("m/d/Y H:i:s O", strtotime($sxe_master_xml->reporting->report_date)); ?>">
			<input type="hidden" name="edit_test_type" value="<?php echo trim($sxe_master_xml->labs->test_type); ?>">
			<input type="hidden" name="edit_test_name" value="<?php echo $sxe_master_xml->labs->local_test_name; ?>">
			<input type="hidden" name="edit_result_value" value="<?php echo $sxe_master_xml->labs->local_result_value; ?>">
		<?php } ?>
			<label for="edit_specimen_source">Specimen Source:</label>
				<select name="edit_specimen_source" id="edit_specimen_source" class="ui-corner-all">
					<option value="" <?php echo ((strlen(trim($sxe_master_xml->labs->local_specimen_source)) < 1) ? " selected" : ""); ?>>--</option>
				<?php
					$specimen_qry = 'SELECT vm.concept AS master_label, vc.concept AS child_label 
						FROM '.$my_db_schema.'vocab_master_vocab vm 
						INNER JOIN '.$my_db_schema.'vocab_child_vocab vc ON (vm.id = vc.master_id AND vc.lab_id = '.intval($lab_id).') 
						WHERE (vm.category = elr.vocab_category_id(\'specimen\')) ORDER BY vm.concept;';
					$specimen_rs = @pg_query($host_pa, $specimen_qry);
					if ($specimen_rs) {
						while ($specimen_row = @pg_fetch_object($specimen_rs)) {
							if ($specimen_row->child_label == $sxe_master_xml->labs->local_specimen_source) {
								echo '<option value="'.$specimen_row->child_label.'" selected>'.$specimen_row->master_label.' ['.$sxe_master_xml->labs->lab.': '.$specimen_row->child_label.']</option>';
							} else {
								echo '<option value="'.$specimen_row->child_label.'">'.$specimen_row->master_label.' ['.$sxe_master_xml->labs->lab.': '.$specimen_row->child_label.']</option>';
							}
						}
					} else {
						suicide('Unable to retrieve list of specimen sources', 1, 1);
					}
					@pg_free_result($specimen_rs);
				?>
				</select><br>
				<label for="edit_date_collected">Date/Time Collected:</label> <input class="ui-corner-all" type="text" name="edit_date_collected" id="edit_date_collected" value="<?php echo date("m/d/Y H:i:s O", strtotime(trim($sxe_master_xml->labs->collection_date))); ?>" placeholder="MM/DD/YYYY HH:MM:SS -0700" /><br><br>
			<input type="hidden" name="emsa_action" id="emsa_action" value="save" />
			<input type="hidden" name="id" id="id" value="<?php echo intval($clean_actions['id']); ?>" />
			<button type="submit" name="edit_save" id="edit_save">Save & Retry</button>
			<button type="button" name="edit_cancel" id="edit_cancel">Cancel</button>
		</form>
	</div>
	
</fieldset>

<?php
	exit;
?>