<div class="exception_details">
	<h3>Current Errors</h3>
	<table class="audit_log">
		<thead>
			<tr>
				<th>Error Type</th>
				<th>Error Description</th>
				<th>Error Details</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$current_sql = "SELECT se.description AS description, sme.info AS info, ss.name AS type FROM ".$my_db_schema."system_message_exceptions sme 
		INNER JOIN ".$my_db_schema."system_exceptions se ON (sme.exception_id = se.exception_id) 
		INNER JOIN ".$my_db_schema."system_statuses ss ON (se.exception_type_id = ss.id) 
		WHERE sme.system_message_id = ".intval($result['id'])." 
		ORDER BY sme.id;";
	$current_rs = @pg_query($host_pa, $current_sql);
	if ($current_rs) {
		if (pg_num_rows($current_rs) > 0) {
			while ($current_row = @pg_fetch_object($current_rs)) {
				echo "<tr><td>".htmlentities($current_row->type)."</td><td>".htmlentities($current_row->description)."</td><td>".$current_row->info."</td></tr>";
			}
		} else {
			echo "<tr><td colspan=\"3\"><em>Message has no current errors</em></td></tr>";
		}
		@pg_free_result($current_rs);
	} else {
		echo "<tr><td colspan=\"3\"><em>Unable to retrieve list of errors</em></td></tr>";
	}
	
	
?>

		</tbody>
	</table>
</div>