<div class="exception_details">
	<br><br>
	<h3>Error History</h3>
	<table class="audit_log">
		<thead>
			<tr>
				<th>Error Type</th>
				<th>Date/Time</th>
				<th>Error Description</th>
				<th>Error Details</th>
			</tr>
		</thead>
		<tbody>
		
<?php
	
	$history_sql = "SELECT sma.created_at AS created_at, se.description AS description, sae.info AS info, ss.name AS type FROM ".$my_db_schema."system_audit_exceptions sae 
		INNER JOIN ".$my_db_schema."system_exceptions se ON (sae.system_exceptions_id = se.exception_id) 
		INNER JOIN ".$my_db_schema."system_statuses ss ON (se.exception_type_id = ss.id) 
		INNER JOIN ".$my_db_schema."system_messages_audits sma ON (sae.system_messages_audits_id = sma.id) 
		WHERE sma.system_message_id = ".intval($result['id'])." 
		ORDER BY sma.created_at, ss.name, se.description;";
	$history_rs = @pg_query($host_pa, $history_sql);
	if ($history_rs) {
		if (pg_num_rows($history_rs) > 0) {
			while ($history_row = @pg_fetch_object($history_rs)) {
				echo "<tr><td>".htmlentities($history_row->type)."</td><td>".date("m/d/Y h:i:s A", strtotime($history_row->created_at))."</td><td>".htmlentities($history_row->description)."</td><td>".$history_row->info."</td></tr>";
			}
		} else {
			echo "<tr><td colspan=\"3\"><em>Message has no previous errors</em></td></tr>";
		}
		@pg_free_result($history_rs);
	} else {
		echo "<tr><td colspan=\"3\"><em>Unable to retrieve error history: " . pg_last_error() . "</em></td></tr>";
	}
	
	
?>

		</tbody>
	</table>
</div>