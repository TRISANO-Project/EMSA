<?php

	include_once WEBROOT_URL.'/includes/emsa_functions.php';

	// preserve clicked message on page refresh
	if (isset($_GET['focus']) && filter_var($_GET['focus'], FILTER_VALIDATE_INT)){
		$focus_id = intval(filter_var($_GET['focus'], FILTER_SANITIZE_NUMBER_INT));
	}

	$type = getMessageQueueById($focus_id);
	
	$view=0;
	$_SESSION['cview']=0;
	
	if(isset($_REQUEST['view'])){
		$view=$_REQUEST['view'];
		$_SESSION['cview']=$_REQUEST['view'];
	}
	
	$jd=0;
	
	if(isset($_REQUEST['jd'])){
		$jd=$_REQUEST['jd'];
	}
	
	include_once './emsa/lib/emsa_jquery_functions.php';
	
?>

			<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elremsa"></span>View Individual ELR Message</h1>
			
<?php

	// handle any edit/move/retry/delete actions...
	include_once 'emsa_actions.php';
	
	$type = getMessageQueueById($focus_id);  // requery to pick up any changed queue due to assignment or move
	
	include_once 'lib/emsa_lists_individual.php';
	
 ?>
