<div class="ui-corner-all" id="emsa_toolbar">
	<?php
		$form_action = $main_url.'index.php?selected_page='.$selected_page;
		if (($selected_page == 6) && ($submenu == 6)) {
			$form_action .= '&submenu='.$submenu;
		} else {
			$form_action .= '&type='.$type;
		}
		$form_action .= '&focus='.$result['id'];
	?>
	<form method="POST" id="override_cmr_<?php echo $result['id']; ?>" action="<?php echo $form_action; ?>">
		<input type="hidden" name="id" id="cmr_<?php echo $result['id']; ?>_id" value="<?php echo $result['id']; ?>" />
		<input type="hidden" name="emsa_action" id="override_emsa_cmraction_<?php echo $result['id']; ?>" value="" />
		<input type="hidden" name="emsa_override" id="emsa_override_<?php echo $result['id']; ?>" value="1" />
		<label>Override Actions:</label>
		<button type="button" title="Add these lab results to a new person" class="override_new_cmr" value="<?php echo $result['id']; ?>">Create New Person & CMR</button>
		<div class="emsa_toolbar_separator"></div>
		<button type="button" title="Add these lab results to the specified Event ID" class="override_update_cmr" value="<?php echo $result['id']; ?>">Add Lab Results to Event ID:</button>
		<input type="text" class="ui-corner-all" style="background-color: lightcyan; font-family: Consolas, 'Courier New', Courier, serif;" name="override_event" id="override_event_<?php echo $result['id']; ?>" placeholder="Event ID#" />
	</form>
</div>