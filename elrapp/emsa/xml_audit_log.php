<?php

	// prevent caching...
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');

	include '../includes/app_config.php';
	
	session_write_close(); // done writing to session; prevent blocking
	
	unset($clean['xml_audit']);
	
	switch (filter_var($_POST['type'], FILTER_SANITIZE_NUMBER_INT)) {
		case 2:
			$clean['xml_audit']['field'] = 'sent_xml';
			break;
		default:
			$clean['xml_audit']['field'] = 'previous_xml';
			break;
	}
	
	if (isset($_POST['id']) && !empty($_POST['id']) && (filter_var($_POST['id'], FILTER_VALIDATE_INT) !== false)) {
		$qry = 'SELECT '.$clean['xml_audit']['field'].' AS result FROM '.$my_db_schema.'system_nedss_xml_audits WHERE id = '.filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT).';';
	} else {
		echo '<h2>No ID specified.  Please see a system administrator.</h2>';
		exit;
	}
	
	$rs = @pg_query($host_pa, $qry);
	if ($rs !== false) {
		echo '<textarea style="width: 100%; height: 100%; font-family: Consolas, \'Courier New\', Courier, sans-serif;">'.formatXml(@pg_fetch_result($rs, 0, 'result')).'</textarea>';
	} else {
		echo '<h2>Unable to retrieve XML.</h2>';
	}
	@pg_free_result($rs);
	
	exit;
	
?>