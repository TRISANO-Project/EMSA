	<div class="ui-corner-all" id="emsa_qa_toolbar">
		<?php
			$form_action = $main_url.'index.php?selected_page='.$selected_page;
			if (($selected_page == 6) && ($submenu == 6)) {
				$form_action .= '&submenu='.$submenu;
			} else {
				$form_action .= '&type='.$type;
			}
			$form_action .= '&focus='.$result['id'];
		?>
		<form style="display: inline-block;" id="emsa_qa_actions_<?php echo $result['id']; ?>" method="POST" action="<?php echo $form_action; ?>">
			<input type="hidden" name="id" value="<?php echo $result['id']; ?>" />
			<input type="hidden" name="target" id="qa_target_<?php echo $result['id']; ?>" value="" />
			<input type="hidden" name="emsa_action" id="qa_emsa_action_<?php echo $result['id']; ?>" value="" />
			<input type="hidden" name="info" id="qa_info_<?php echo $result['id']; ?>" value="" />
			<label>QA Flags:</label>

<?php

	if ($event_details['flags']['qa_mandatory_fields']) {
		echo '<button type="button" class="emsa_btn_flagmandatory_off" id="emsa_btn_flagmandatory_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Missing Mandatory Fields</button>';
	} else {
		echo '<button type="button" class="emsa_btn_flagmandatory" id="emsa_btn_flagmandatory_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Missing Mandatory Fields</button>';
	}
	
	echo '<div class="emsa_toolbar_separator"></div>';
	if ($event_details['flags']['qa_vocab_coding']) {
		echo '<button type="button" class="emsa_btn_flagvocabcoding_off" id="emsa_btn_flagvocabcoding_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Coding/Vocabulary Errors</button>';
	} else {
		echo '<button type="button" class="emsa_btn_flagvocabcoding" id="emsa_btn_flagvocabcoding_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Coding/Vocabulary Errors</button>';
	}
	
	echo '<div class="emsa_toolbar_separator"></div>';
	if ($event_details['flags']['qa_mqf']) {
		echo '<button type="button" class="emsa_btn_flagmqf_off" id="emsa_btn_flagmqf_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">MQF Structural Errors</button>';
	} else {
		echo '<button type="button" class="emsa_btn_flagmqf" id="emsa_btn_flagmqf_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">MQF Structural Errors</button>';
	}
	
	echo '<div class="emsa_toolbar_separator"></div>';
	if ($event_details['flags']['fix_duplicate']) {
		echo '<button type="button" class="emsa_btn_flagfixduplicate_off" id="emsa_btn_flagfixduplicate_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Fix Duplicate</button>';
	} else {
		echo '<button type="button" class="emsa_btn_flagfixduplicate" id="emsa_btn_flagfixduplicate_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Fix Duplicate</button>';
	}
	
	echo '<div class="emsa_toolbar_separator"></div>';
	if ($event_details['flags']['need_fix']) {
		echo '<button type="button" class="emsa_btn_flagneedfix_off" id="emsa_btn_flagneedfix_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Needs Fixing</button>';
	} else {
		echo '<button type="button" class="emsa_btn_flagneedfix" id="emsa_btn_flagneedfix_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Needs Fixing</button>';
	}
	
	//if ($type == PENDING_STATUS || $type == EXCEPTIONS_STATUS) {
		?>
			<div style="display: block; margin-top: 5px; padding-top: 5px; border-top: 1px #aaaaaa dotted;">
				<label>Quality Check:</label>
				
<?php

		if ($event_details['flags']['de_error']) {
			echo '<button type="button" class="emsa_btn_flagdeerror_off" id="emsa_btn_flagdeerror_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Data Entry Error</button>';
		} else {
			echo '<button type="button" class="emsa_btn_flagdeerror" id="emsa_btn_flagdeerror_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Data Entry Error</button>';
		}
		echo '<select class="ui-corner-all" id="de_error_info_'.$result['id'].'" name="de_error_info_'.$result['id'].'">';
		echo '<option selected value="">Select type of Data Entry error...</option>';
		$this_message_flag_comment = getMessageFlagComment($result['id'], EMSA_FLAG_DE_ERROR);
		echo '<option '.(($this_message_flag_comment == 'Laboratory Error') ? 'selected ' : '').'value="Laboratory Error">Laboratory Error</option>';
		echo '<option '.(($this_message_flag_comment == 'TriSano Error') ? 'selected ' : '').'value="TriSano Error">TriSano Error</option>';
		echo '<option '.(($this_message_flag_comment == 'Undetermined Error') ? 'selected ' : '').'value="Undetermined Error">Undetermined Error</option>';
		echo '<option '.(($this_message_flag_comment == 'Alias') ? 'selected ' : '').'value="Alias">Alias</option>';
		echo '</select>';
		echo '<div class="emsa_toolbar_separator"></div>';
		if ($event_details['flags']['de_other']) {
			echo '<button type="button" class="emsa_btn_flagdeother_off" id="emsa_btn_flagdeother_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Other</button>';
		} else {
			echo '<button type="button" class="emsa_btn_flagdeother" id="emsa_btn_flagdeother_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Other</button>';
		}
		echo '<input type="text" class="ui-corner-all" id="de_other_info_'.$result['id'].'" name="de_other_info_'.$result['id'].'" placeholder="Explain \'Other\' reason..." value="'.htmlentities(getMessageFlagComment($result['id'], EMSA_FLAG_DE_OTHER)).'">';

?>
			</div>
<?php
	//}

?>

		</form>
	</div>
	
	<div class="ui-corner-all" id="emsa_qa_comments">
		<br><h3>QA Comments</h3>
		<?php echo displayQACommentsById($result['id']); ?>
		<textarea class="ui-corner-all" style="font-family: 'Open Sans', Arial, Helvetica, sans-serif; font-weight: 400; width: 50%; height: 50px; margin: 5px;" id="add_comment_<?php echo $result['id']; ?>" name="add_comment_<?php echo $result['id']; ?>" placeholder="Enter new comment here"></textarea><br>
		<button type="button" class="emsa_btn_addcomment" style="margin: 5px;" id="emsa_btn_addcomment_<?php echo $result['id']; ?>" value="<?php echo $result['id']; ?>" title="Add Comment">Add New Comment</button>
	</div>