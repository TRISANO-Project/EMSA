<?php
	
	// pre-set address validation stuff...
	/* Address validation disabled for the time being
	if($event_details['address_is_valid']==1){
		$address_valid_class="emsa_address_valid";
		$validate_address="";
	}else{
		$address_valid_class="emsa_address_invalid";
		$validate_address="<br><button type=\"button\" name=\"id\" class=\"validate_address_btn\" value=\"".$result['id']."\" title=\"Validate Address\">Validate</button>";
	}
	*/
	
?>

	<div class="ui-corner-all" id="emsa_toolbar">
		<?php
			$form_action = $main_url.'index.php?selected_page='.$selected_page;
			if (($selected_page == 6) && ($submenu == 6)) {
				$form_action .= '&submenu='.$submenu;
			} else {
				$form_action .= '&type='.$type;
			}
			$form_action .= '&focus='.$result['id'];
		?>
		<form style="display: inline-block;" id="emsa_actions_<?php echo $result['id']; ?>" method="POST" action="<?php echo $form_action; ?>">
			<input type="hidden" name="id" value="<?php echo $result['id']; ?>" />
			<input type="hidden" name="target" id="target_<?php echo $result['id']; ?>" value="" />
			<input type="hidden" name="emsa_action" id="emsa_action_<?php echo $result['id']; ?>" value="" />
			<input type="hidden" name="info" id="info_<?php echo $result['id']; ?>" value="" />
			<input type="hidden" name="semi_auto_original" id="semi_auto_original_<?php echo $result['id']; ?>" value="" />
			<?php echo showMessageMoveReason($result['id'], $type); ?>
			<div style="display: block;">
				<label>Actions:</label>
		
<?php

	$is_semi_auto_entry = false;
	if ($type == ENTRY_STATUS) {
		$is_semi_auto_entry = true;  // for now, all msgs in 'Entry' allow this
		echo '<button type="button" class="emsa_btn_cultureentry" id="emsa_btn_cultureentry_'.$result['id'].'" value="'.$result['id'].'" title="Manually set complex lab results">Semi-Auto Lab Entry</button>';
		echo '<div class="emsa_toolbar_separator"></div>';
		
	}
	
	//if ($type == EXCEPTIONS_STATUS || $type == PENDING_STATUS) {
	if ($type == EXCEPTIONS_STATUS || $type == ENTRY_STATUS) {
		// buttons for edit & retry
		echo '<button type="button" class="emsa_btn_edit" id="emsa_btn_edit_'.$result['id'].'" value="'.$result['id'].'" title="Edit & revalidate this message">Edit & Retry</button>';
		echo '<div class="emsa_toolbar_separator"></div>';
		echo '<button type="button" class="emsa_btn_retry" id="emsa_btn_retry_'.$result['id'].'" value="'.$result['id'].'" title="Revalidate this message as-is">Retry Without Changes</button>';
		if (checkPermission(URIGHTS_ACTION_MOVE) || (checkPermission(URIGHTS_ACTION_DELETE) && ($type != ASSIGNED_STATUS))) {
			echo '<div class="emsa_toolbar_separator"></div>';
		}
	}
	
	// action bar buttons
	if (checkPermission(URIGHTS_ACTION_MOVE)) {
		echo '<button type="button" class="emsa_btn_move" id="emsa_btn_move_'.$result['id'].'" value="'.$result['id'].'" title="Move to selected queue">Move To:</button>' . getQueueNameMenuByTypeAndMsgId($type, $result['id'], false);
		echo '<input type="text" class="ui-corner-all" id="move_info_'.$result['id'].'" name="move_info_'.$result['id'].'" placeholder="Reason for moving?">';
	}
	if (checkPermission(URIGHTS_ACTION_MOVE) && (checkPermission(URIGHTS_ACTION_DELETE) && ($type != ASSIGNED_STATUS))) {
		echo '<div class="emsa_toolbar_separator"></div>';
	}
	if (checkPermission(URIGHTS_ACTION_DELETE) && ($type != ASSIGNED_STATUS)) {
		echo '<button type="button" class="emsa_btn_delete" id="emsa_btn_delete_'.$result['id'].'" value="'.$result['id'].'" title="Delete this message">Delete</button>';
	}
	if ($type == ASSIGNED_STATUS) {
		if (checkPermission(URIGHTS_ACTION_MOVE) || checkPermission(URIGHTS_ACTION_DELETE)) {
			echo '<div class="emsa_toolbar_separator"></div>';
		}
		echo '<button type="button" class="emsa_btn_viewnedss" id="emsa_btn_viewnedss_'.$result['id'].'" value="'.$event_details['event_id'].'">View in TriSano</button>';
	}
	
?>
			</div>
<?php

	//if (($type == PENDING_STATUS || $type == EXCEPTIONS_STATUS) && !checkPermission(URIGHTS_TAB_QA)) {
	if (($type == ENTRY_STATUS || $type == EXCEPTIONS_STATUS) && !checkPermission(URIGHTS_TAB_QA)) {
?>
			<div style="display: block; margin-top: 5px; padding-top: 5px; border-top: 1px #aaaaaa dotted;">
				<label>Quality Check:</label>
				
<?php

		if ($event_details['flags']['de_error']) {
			echo '<button type="button" class="emsa_btn_flagdeerror_off" id="emsa_btn_flagdeerror_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Data Entry Error</button>';
		} else {
			echo '<button type="button" class="emsa_btn_flagdeerror" id="emsa_btn_flagdeerror_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Data Entry Error</button>';
		}
		echo '<select class="ui-corner-all" id="de_error_info_'.$result['id'].'" name="de_error_info_'.$result['id'].'">';
		echo '<option selected value="">Select type of Data Entry error...</option>';
		$this_message_flag_comment = getMessageFlagComment($result['id'], EMSA_FLAG_DE_ERROR);
		echo '<option '.(($this_message_flag_comment == 'Laboratory Error') ? 'selected ' : '').'value="Laboratory Error">Laboratory Error</option>';
		echo '<option '.(($this_message_flag_comment == 'TriSano Error') ? 'selected ' : '').'value="TriSano Error">TriSano Error</option>';
		echo '<option '.(($this_message_flag_comment == 'Undetermined Error') ? 'selected ' : '').'value="Undetermined Error">Undetermined Error</option>';
		echo '<option '.(($this_message_flag_comment == 'Alias') ? 'selected ' : '').'value="Alias">Alias</option>';
		echo '</select>';
		if (1 === 0) {
			echo '<div class="emsa_toolbar_separator"></div>';
			if ($event_details['flags']['fix_duplicate']) {
				echo '<button type="button" class="emsa_btn_flagfixduplicate_off" id="emsa_btn_flagfixduplicate_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Fix Duplicate</button>';
			} else {
				echo '<button type="button" class="emsa_btn_flagfixduplicate" id="emsa_btn_flagfixduplicate_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Fix Duplicate</button>';
			}
		}
		echo '<div class="emsa_toolbar_separator"></div>';
		if ($event_details['flags']['de_other']) {
			echo '<button type="button" class="emsa_btn_flagdeother_off" id="emsa_btn_flagdeother_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Other</button>';
		} else {
			echo '<button type="button" class="emsa_btn_flagdeother" id="emsa_btn_flagdeother_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Other</button>';
		}
		echo '<input type="text" class="ui-corner-all" id="de_other_info_'.$result['id'].'" name="de_other_info_'.$result['id'].'" placeholder="Explain \'Other\' reason..." value="'.htmlentities(getMessageFlagComment($result['id'], EMSA_FLAG_DE_OTHER)).'">';

?>
			</div>
<?php
	}
?>
<?php

	if ($type == GRAY_STATUS) {
?>
			<div style="display: block; margin-top: 5px; padding-top: 5px; border-top: 1px #aaaaaa dotted;">
				<label>Message Flags:</label>
				
<?php

		if ($event_details['flags']['investigation_complete']) {
			echo '<button type="button" class="emsa_btn_flagcomplete_off" id="emsa_btn_flagcomplete_'.$result['id'].'" value="'.$result['id'].'" title="Clear Flag">Investigation Complete</button>';
		} else {
			echo '<button type="button" class="emsa_btn_flagcomplete" id="emsa_btn_flagcomplete_'.$result['id'].'" value="'.$result['id'].'" title="Set Flag">Investigation Complete</button>';
		}

?>
			</div>
<?php
	}
?>
	
		</form>
	</div>
	
	<table class="list">
		<tr>
			<th width="200px">Patient Info</th>
			<th width="200px">Disease & Organism</th>
			<th width="345px">Event Data</th>
		</tr>
		<tr>
			<td width="200px" valign="top">
				<table border="0" width="100%">
					<tr><td><b>Name:</b></td><td><?php echo $event_details['full_name']; ?></td></tr>
					<tr><td><b>DOB:</b></td><td><?php echo $event_details['birth_date']; ?></td></tr>
					<tr><td><b>Medical Record #</b></td><td><?php echo $event_details['medical_record']; ?></td></tr>
					<tr><td><b>Accession #</b></td><td><?php echo $event_details['accession_number']; ?></td></tr>
				<?php if ($is_semi_auto_entry) { ?>
					<tr><td><b>Hospitalized?</b></td><td class="cultureentry_container_<?php echo $result['id']; ?>">
						<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 25em;" id="cultureentry_<?php echo $result['id']; ?>__hospitalized" name="cultureentry_<?php echo $result['id']; ?>__hospitalized">
							<option value="">--</option>
							<?php
								foreach ($_SESSION['trisano_codes']['external_codes']['yesno'] as $trisano_id__hospitalized => $trisano_label__hospitalized) {
									if (strtolower($event_details['hospitalized']) == strtolower($trisano_label__hospitalized['code_description'])) {
										echo '<option selected value="'.intval($trisano_id__hospitalized).'">'.htmlspecialchars($trisano_label__hospitalized['code_description']).'</option>'.PHP_EOL;
									} else {
										echo '<option value="'.intval($trisano_id__hospitalized).'">'.htmlspecialchars($trisano_label__hospitalized['code_description']).'</option>'.PHP_EOL;
									}
								}
							?>
						</select>
					</td></tr>
				<?php } else { ?>
					<tr><td><b>Hospitalized?</b></td><td><?php echo $event_details['hospitalized']; ?></td></tr>
				<?php } ?>
				</table>
			</td>
			<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="top" width="200px" style="font-size: 1.2em; font-weight: 900; color: purple;">
				<?php if ($is_semi_auto_entry) { ?>
					<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 25em;" id="cultureentry_<?php echo $result['id']; ?>__disease" name="cultureentry_<?php echo $result['id']; ?>__disease">
						<option value="">--</option>
						<?php
							foreach ($_SESSION['trisano_codes']['diseases'] as $trisano_id__disease => $trisano_label__disease) {
								if (strtolower($event_details['disease']) == strtolower($trisano_label__disease)) {
									echo '<option selected value="'.intval($trisano_id__disease).'">'.htmlspecialchars($trisano_label__disease).'</option>'.PHP_EOL;
								} else {
									echo '<option value="'.intval($trisano_id__disease).'">'.htmlspecialchars($trisano_label__disease).'</option>'.PHP_EOL;
								}
							}
						?>
					</select><br><span style="font-size: 0.9em; font-weight: 400; color: purple;">
					<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 25em;" id="cultureentry_<?php echo $result['id']; ?>__organism" name="cultureentry_<?php echo $result['id']; ?>__organism">
						<option value="">--</option>
						<?php
							foreach ($_SESSION['trisano_codes']['organisms'] as $trisano_id__organsim => $trisano_label__organism) {
								if (strtolower($event_details['organism']) == strtolower($trisano_label__organism)) {
									echo '<option selected value="'.intval($trisano_id__organsim).'">'.htmlspecialchars($trisano_label__organism).'</option>'.PHP_EOL;
								} else {
									echo '<option value="'.intval($trisano_id__organsim).'">'.htmlspecialchars($trisano_label__organism).'</option>'.PHP_EOL;
								}
							}
						?>
					</select></span><br>
				<?php } else { ?>
					<?php echo $event_details['disease']; ?><br><em style="font-weight: 600; color: mediumorchid;"><?php echo ((strlen($event_details['organism']) > 0) ? $event_details['organism'] : '--Organism N/A--'); ?></em>
				<?php } ?>
			</td>
			<td valign="top" width="245px">
				<table border="0" width="100%">
					<tr>
						<td class="cultureentry_container_<?php echo $result['id']; ?>"><b>Jurisdiction:</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>">
						<?php if ($is_semi_auto_entry) { ?>
							<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 25em;" id="cultureentry_<?php echo $result['id']; ?>__jurisdiction" name="cultureentry_<?php echo $result['id']; ?>__jurisdiction">
								<option value="">--</option>
								<?php
									foreach ($_SESSION['jurisdictions'] as $trisano_id__jurisdiction => $trisano_label__jurisdiction) {
										if (strtolower($event_details['jurisdiction']) == strtolower($trisano_label__jurisdiction)) {
											echo '<option selected value="'.intval($trisano_id__jurisdiction).'">'.htmlspecialchars($trisano_label__jurisdiction).'</option>'.PHP_EOL;
										} else {
											echo '<option value="'.intval($trisano_id__jurisdiction).'">'.htmlspecialchars($trisano_label__jurisdiction).'</option>'.PHP_EOL;
										}
									}
								?>
							</select>
						<?php } else { ?>
							<?php echo $event_details['jurisdiction']; ?>
						<?php } ?>
						</td>
					</tr>
					<tr>
						<td class="cultureentry_container_<?php echo $result['id']; ?>"><b>State Case Status:</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>">
						<?php if ($is_semi_auto_entry) { ?>
							<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 25em;" id="cultureentry_<?php echo $result['id']; ?>__state_case_status" name="cultureentry_<?php echo $result['id']; ?>__state_case_status">
								<option value="">--</option>
								<?php
									foreach ($_SESSION['trisano_codes']['external_codes']['case'] as $trisano_id__case => $trisano_label__case) {
										if (strtolower($event_details['state_case_status']) == strtolower($trisano_label__case['code_description'])) {
											echo '<option selected value="'.intval($trisano_id__case).'">'.htmlspecialchars($trisano_label__case['code_description']).'</option>'.PHP_EOL;
										} else {
											echo '<option value="'.intval($trisano_id__case).'">'.htmlspecialchars($trisano_label__case['code_description']).'</option>'.PHP_EOL;
										}
									}
								?>
							</select>
						<?php } else { ?>
							<?php echo $event_details['state_case_status']; ?>
						<?php } ?>
						</td>
					</tr>
					<tr><td><b>Received:</b></td><td><?php echo $event_details['report_date']; ?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<th width="200px">Demographics</th>
			<th width="200px">Facilities</th>
			<th width="345px">Local Lab</th>
		</tr>
		<tr>
			<td valign="top" width="200px">
				<table border="0" width="100%">
					<tr><td><b>Age:</b></td><td><?php echo $event_details['age']; ?></td></tr>
					<tr><td><b>Gender:</b></td><td><?php echo $event_details['sex']; ?></td></tr>
					<tr><td><b>Race:</b></td><td><?php echo $event_details['race']; ?></td></tr>
					<tr><td><b>Ethnicity:</b></td><td><?php echo $event_details['ethnicity']; ?></td></tr>
					<tr>
						<td valign="top"><b>Address:</b></td>
						<td><?php echo $event_details['street'].'<br>'.$event_details['city'].'<br>'.$event_details['state'].' '.$event_details['zip']; ?></td>
					</tr>
					<tr><td><b>Telephone:</b></td><td><?php echo $event_details['patient_phone']; ?></td></tr>
				</table>
			</td>
			<td valign="top" width="200px">
				<table border="0" width="100%">
					<tr><td><b>Report Date:</b></td><td><?php echo $event_details['report_date']; ?></td></tr>
					<tr><td><b>Reporting Lab:</b></td><td><?php echo $event_details['lab']; ?></td></tr>
					<tr><td><b>Reporting Agency:</b></td><td><?php echo $event_details['reporting']; ?></td></tr>
					<tr><td valign="top"><b>Clinician:</b></td><td><?php echo $event_details['clinician']; ?></td></tr>
					<tr><td valign="top"><b>Clinician Telephone:</b></td><td><?php echo $event_details['clinician_phone']; ?></td></tr>
					<tr><td valign="top"><b>Diagnostic Facility:</b></td><td><?php echo $event_details['diagnostic']; ?><br><?php echo $event_details['diagnostic_street'].'<br>'.$event_details['diagnostic_city'].'<br>'.$event_details['diagnostic_state'].' '.$event_details['diagnostic_zip']; ?></td></tr>
				</table>
			</td>
			<td valign="top" width="345px">
				<table border="0" width="100%">
				
				<?php
				
					// build local result string based on whether l_r_v & l_r_v2 are populated, as well as local_units
					$local_result_str = '';
					$local_result_str .= ((strlen(trim($event_details['local_result_value'])) > 0) ? trim($event_details['local_result_value']) : '');
					$local_result_str .= (((strlen(trim($event_details['local_result_value'])) > 0) && (strlen(trim($event_details['local_result_value_2'])) > 0)) ? ' [' : '');
					$local_result_str .= ((strlen(trim($event_details['local_result_value_2'])) > 0) ? trim($event_details['local_result_value_2']) : '');
					$local_result_str .= (((strlen(trim($event_details['local_result_value'])) > 0) && (strlen(trim($event_details['local_result_value_2'])) > 0)) ? ']' : '');
					$local_result_str .= ((strlen(trim($event_details['local_units'])) > 0) ? ' '.trim($event_details['local_units']) : '');
					
				?>
					
					<tr><td><b>Child LOINC:</b></td><td style="white-space: nowrap;"><?php echo $event_details['local_loinc_code']; ?></td></tr>
					<tr><td><b>Child LOINC Name:</b></td><td><?php echo $event_details['local_test_name']; ?></td></tr>
					<tr><td><b>Local Test Code:</b></td><td style="white-space: nowrap;"><?php echo $event_details['local_code']; ?></td></tr>
					<tr><td><b>Local Test Name:</b></td><td><?php echo $event_details['local_code_test_name']; ?></td></tr>
					<tr><td><b>Specimen:</b></td><td><?php echo $event_details['local_specimen_source']; ?></td></tr>
					<tr><td><b>Lab Result Value:</b></td><td><?php echo $local_result_str; ?></td></tr>
					<tr><td><b>Reference Range:</b></td><td><?php echo $event_details['local_reference_range']; ?></td></tr>
					<tr><td><b>Abnormal Flag:</b></td><td><?php echo ((strlen(trim($event_details['abnormal_flag'])) > 0) ? '<b style="color: red;">'.$event_details['abnormal_flag'].'</b>' : ''); ?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<th colspan="3" title="Translated Lab">Translated Lab</div></th>
		</tr>
		<tr>
			<td colspan="20">
				<table border="0" width="100%">
					<tr>
						<td valign="bottom"><b>Master LOINC</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="bottom"><b>Test Type</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="bottom"><b>Specimen</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="bottom"><b>Test Result</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="bottom"><b>Result Value+Units</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="bottom"><b>Comment</b></td>
						<td valign="bottom"><b>Reference Range</b></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="bottom"><b>Test Status</b></td>
						<td valign="bottom"><b>Collection Date</b></td>
						<td valign="bottom"><b>Lab Test Date</b></td>
					</tr>
					<tr>
						<td valign="top" style="white-space: nowrap;"><?php echo $event_details['loinc_code']; ?></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="top">
							<?php if ($is_semi_auto_entry) { ?>
								<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 12em;" id="cultureentry_<?php echo $result['id']; ?>__test_type" name="cultureentry_<?php echo $result['id']; ?>__test_type">
									<option value="">--</option>
									<?php
										foreach ($_SESSION['trisano_codes']['common_test_types'] as $trisano_id__testtype => $trisano_label__testtype) {
											if (strtolower($event_details['test_type']) == strtolower($trisano_label__testtype)) {
												echo '<option selected value="'.intval($trisano_id__testtype).'">'.htmlspecialchars($trisano_label__testtype).'</option>'.PHP_EOL;
											} else {
												echo '<option value="'.intval($trisano_id__testtype).'">'.htmlspecialchars($trisano_label__testtype).'</option>'.PHP_EOL;
											}
										}
									?>
								</select>
							<?php } else { ?>
								<?php echo $event_details['test_type']; ?>
							<?php } ?>
						</td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="top">
							<?php if ($is_semi_auto_entry) { ?>
								<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 12em;" id="cultureentry_<?php echo $result['id']; ?>__specimen" name="cultureentry_<?php echo $result['id']; ?>__specimen">
									<option value="">--</option>
									<?php
										foreach ($_SESSION['trisano_codes']['external_codes']['specimen'] as $trisano_id__specimen => $trisano_label__specimen) {
											if (strtolower($event_details['specimen_source']) == strtolower($trisano_label__specimen['code_description'])) {
												echo '<option selected value="'.intval($trisano_id__specimen).'">'.htmlspecialchars($trisano_label__specimen['code_description']).'</option>'.PHP_EOL;
											} else {
												echo '<option value="'.intval($trisano_id__specimen).'">'.htmlspecialchars($trisano_label__specimen['code_description']).'</option>'.PHP_EOL;
											}
										}
									?>
								</select>
							<?php } else { ?>
								<?php echo $event_details['specimen_source']; ?>
							<?php } ?>
						</td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="top">
							<?php if ($is_semi_auto_entry) { ?>
								<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 12em;" id="cultureentry_<?php echo $result['id']; ?>__testresult" name="cultureentry_<?php echo $result['id']; ?>__testresult">
									<option value="">--</option>
									<?php
										foreach ($_SESSION['trisano_codes']['external_codes']['test_result'] as $trisano_id__testresult => $trisano_label__testresult) {
											if (strtolower($event_details['test_result']) == strtolower($trisano_label__testresult['code_description'])) {
												echo '<option selected value="'.intval($trisano_id__testresult).'">'.htmlspecialchars($trisano_label__testresult['code_description']).'</option>'.PHP_EOL;
											} else {
												echo '<option value="'.intval($trisano_id__testresult).'">'.htmlspecialchars($trisano_label__testresult['code_description']).'</option>'.PHP_EOL;
											}
										}
									?>
								</select>
							<?php } else { ?>
								<?php echo $event_details['test_result']; ?>
							<?php } ?>
						</td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="top">
							<?php if ($is_semi_auto_entry) { ?>
								<input class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 7em;" id="cultureentry_<?php echo $result['id']; ?>__resultvalue" name="cultureentry_<?php echo $result['id']; ?>__resultvalue" value="<?php echo htmlspecialchars($event_details['result_value']); ?>">
								+<input class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 3em;" id="cultureentry_<?php echo $result['id']; ?>__units" name="cultureentry_<?php echo $result['id']; ?>__units" value="<?php echo htmlspecialchars($event_details['units']); ?>">
							<?php } else { ?>
								<?php echo $event_details['result_value'].' '.$event_details['units']; ?>
							<?php } ?>
						</td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="top">
							<?php if ($is_semi_auto_entry) { ?>
								<textarea class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="width: 12em; height: 3em;" id="cultureentry_<?php echo $result['id']; ?>__comment" name="cultureentry_<?php echo $result['id']; ?>__comment"><?php echo htmlspecialchars($event_details['comment']); ?></textarea>
							<?php } else { ?>
								<?php echo $event_details['comment']; ?>
							<?php } ?>
						</td>
						<td valign="top"><?php echo $event_details['reference_range']; ?></td>
						<td class="cultureentry_container_<?php echo $result['id']; ?>" valign="top">
							<?php if ($is_semi_auto_entry) { ?>
								<select class="emsa_cultureentry_<?php echo $result['id']; ?>" disabled style="max-width: 12em;" id="cultureentry_<?php echo $result['id']; ?>__teststatus" name="cultureentry_<?php echo $result['id']; ?>__teststatus">
									<option value="">--</option>
									<?php
										foreach ($_SESSION['trisano_codes']['external_codes']['test_status'] as $trisano_id__teststatus => $trisano_label__teststatus) {
											if (strtolower($event_details['test_status']) == strtolower($trisano_label__teststatus['code_description'])) {
												echo '<option selected value="'.intval($trisano_id__teststatus).'">'.htmlspecialchars($trisano_label__teststatus['code_description']).'</option>'.PHP_EOL;
											} else {
												echo '<option value="'.intval($trisano_id__teststatus).'">'.htmlspecialchars($trisano_label__teststatus['code_description']).'</option>'.PHP_EOL;
											}
										}
									?>
								</select>
							<?php } else { ?>
								<?php echo $event_details['test_status']; ?>
							<?php } ?>
						</td>
						<td valign="top"><?php echo $event_details['collection_date']; ?></td>
						<td valign="top"><?php echo $event_details['lab_test_date']; ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>