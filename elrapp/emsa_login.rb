require 'rubygems'
require 'net/ldap'

# username and password expected as args
if ARGV.count == 2
  ldap = Net::LDAP.new
  ldap.host = "adtcdc03.tarrantcounty.com"
  ldap.port = 389
  ldap.auth "tarrantcounty\\" + ARGV[0], ARGV[1]
  if ldap.bind
    # authentication succeeded
    puts "Success"
  else
    # authentication failed
#    p ldap.get_operation_result
    puts "Failure"
  end
else
  puts "Usage: ruby emsa_login.rb username password"
end
