<?php

	// sanitization & filtering
	if (isset($_GET['e_id']) && filter_var(trim($_GET['e_id']), FILTER_VALIDATE_INT)) {
		$clean['retry_exception_id'] = filter_var(trim($_GET['e_id']), FILTER_SANITIZE_NUMBER_INT);
	}
	
	if (isset($_GET['e_value']) && (strlen(trim($_GET['e_value'])) > 0)) {
		$clean['retry_exception_value'] = decodeIfBase64Encoded(filter_var(trim($_GET['e_value']), FILTER_SANITIZE_STRING));
	}
	
	if (isset($_GET['l_id']) && filter_var(trim($_GET['l_id']), FILTER_VALIDATE_INT)) {
		$clean['lab_id'] = filter_var(trim($_GET['l_id']), FILTER_SANITIZE_NUMBER_INT);
	}
	
?>

<script>
	$(function() {
		$(".retry_btn").button({
			icons: { primary: "ui-icon-elrretry" },
			text: false
		}).click(function() {
			$(this).prop('disabled', true);
			var jsonObj = jQuery.parseJSON($(this).val());
			var retryAction = "";
			if (jsonObj.e_id) {
				if (jsonObj.e_value) {
					retryAction = "<?php echo $main_url; ?>?selected_page=6&submenu=2&l_id="+jsonObj.l_id+"&e_id="+jsonObj.e_id+"&e_value="+encodeURIComponent(jsonObj.e_value);
				} else {
					retryAction = "<?php echo $main_url; ?>?selected_page=6&submenu=2&l_id="+jsonObj.l_id+"&e_id="+jsonObj.e_id;
				}
				window.location.href = retryAction;
			} else {
				alert("An error occurred.\n\nUnable to retry messages.  Please contact a system administrator.");
			}
		});
		
		$("#view_detailed_results").button({
			icons: { secondary: "ui-icon-circle-triangle-s" }
		}).click(function() {
			$("#detailed_results").toggle();
		});
		
		$(".emsa_btn_viewnedss").button({
				icons: {
					primary: "ui-icon-elrview"
				}
			}).click(function() {
				window.open("emsa/nedss_link.php?event_id="+$(this).val(), '_blank');
				return false;
		});
		
		$("#tabs").tabs();
	});
</script>

<style type="text/css">
	input[type="radio"] { min-width: 0 !important; }
	#new_lab_form div { display: inline-block; margin: 7px; padding: 10px; }
	#labResults td { border-bottom-width: 2px; border-bottom-color: darkorange; }
	.audit_log td { border-bottom-width: 1px !important; border-bottom-color: lightgray !important; }
	.audit_log tr:hover > td { background-color: lightcyan !important; }
	#labResults tr:hover > td { background-color: lightblue; }
	#labResults a:link, #labResults a:visited, #labResults a:active { color: #005D9C; font-weight: 600; }
	#labResults a:hover { color: black; }
	.retry_btn { vertical-align: middle; height: 24px; }
</style>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrerrorbig"></span>E-Tasks</h1>

<?php
	if ((isset($clean['retry_exception_id'])) && (isset($clean['lab_id']))) {
		// attempt a retry
		include_once 'retry.php';
	}
?>

<div class="lab_results_container ui-widget ui-corner-all">
	<?php
		unset($etask_arr);
		unset($etask_lookup);
		unset($etask_lab_arr);
		unset($lab_id_arr);
		unset($etask_lab_etype_arr);
		
		$etask_lookup = array();
		
		$qry = "SELECT l.ui_name AS lab, l.id AS lab_id, se.description AS description, se.id AS e_id, se.allow_retry AS allow_retry, sme.info AS info, count(sme.exception_id) AS exceptions 
			FROM elr.system_message_exceptions sme
			LEFT JOIN elr.system_messages sm ON (sme.system_message_id = sm.id)
			LEFT JOIN elr.structure_labs l ON (sm.lab_id = l.id)
			INNER JOIN elr.system_exceptions se ON (sme.exception_id = se.exception_id AND se.exception_type_id = 3)
			WHERE (sm.deleted != 1 OR sm.deleted IS NULL) AND sm.final_status = 3
			GROUP BY l.ui_name, l.id, se.id, se.description, se.allow_retry, sme.info
			ORDER BY lab, description, exceptions DESC, info;";
		$rs = @pg_query($host_pa, $qry);
		
		while ($row = pg_fetch_object($rs)) {
			if (!isset($etask_lookup[intval($row->e_id)])) {
				$etask_lookup[intval($row->e_id)] = array(
					'description' => trim($row->description),
					'allow_retry' => ((trim($row->allow_retry == 't')) ? true : false)
				);
			}
			if (!isset($lab_id_arr[intval($row->lab_id)])) {
				$lab_id_arr[intval($row->lab_id)] = trim($row->lab);
			}
			$etask_arr[trim($row->lab_id)][intval($row->e_id)][trim($row->info)] = intval($row->exceptions);
		}
		@pg_free_result($rs);
		
		if (isset($etask_arr) && is_array($etask_arr)) {
		
			echo '<div id="tabs"><ul>';
		
			foreach ($etask_arr as $etask_labid => $etask_lab_etypes) {
				foreach ($etask_lab_etypes as $etask_lab_etype => $etask_lab_etype_values) {
					foreach ($etask_lab_etype_values as $etask_lab_etype_value => $etask_lab_etype_value_count) {
						$etask_lab_arr[$etask_labid] += $etask_lab_etype_value_count;
						$etask_lab_etype_arr[$etask_labid][$etask_lab_etype] += $etask_lab_etype_value_count;
					}
				}
				echo '<li><a href="#tabs-'.md5($lab_id_arr[$etask_labid]).'">'.$lab_id_arr[$etask_labid].' ('.$etask_lab_arr[$etask_labid].' errors)</a></li>';
			}
			
			echo '</ul>';
			
			foreach ($etask_lab_arr as $etask_lab_arr_key => $etask_lab_arr_value) {
				echo '<div id="tabs-'.md5($lab_id_arr[$etask_lab_arr_key]).'">';
				//echo "<h3>".htmlentities($etask_lab_arr_key, ENT_QUOTES, "UTF-8")." [".intval($etask_lab_arr_value)." Exceptions]</h3>";
			?>
					<table id="labResults">
						<thead>
							<tr>
								<th style="width: 45%;">Exception Type</th>
								<th style="width: 40%;">Exception Values</th>
								<th style="width: 15%;"># of Exceptions</th>
							</tr>
						</thead>
						<tbody>
			<?php
				
				foreach($etask_lab_etype_arr[$etask_lab_arr_key] as $etask_lab_etype_arr_key => $etask_lab_etype_arr_value) {
					echo '<tr><td style="vertical-align: top;"><span><strong style="font-size: 1.05em;">'.$etask_lookup[$etask_lab_etype_arr_key]['description'].'</strong><br>'.(($etask_lookup[$etask_lab_etype_arr_key]['allow_retry']) ? ' <button class="retry_btn" type="button" title="Retry" value=\''.json_encode(array('l_id' => $etask_lab_arr_key, 'e_id' => $etask_lab_etype_arr_key, 'e_value' => null)).'\'></button><div class="emsa_toolbar_separator"></div>' : '').'<span style="display: inline-block; vertical-align: middle; padding-bottom: 3px;" class="ui-icon ui-icon-newwin"></span><a href="?selected_page=6&submenu=1&type=3&f[lab][]='.$etask_lab_arr_key.'&f[eflag][]='.$etask_lab_etype_arr_key.'" target="_blank" title="View all messages with \''.$etask_lookup[$etask_lab_etype_arr_key]['description'].'\' errors...">'.intval($etask_lab_etype_arr_value).' error'.((intval($etask_lab_etype_arr_value) > 1) ? 's' : '').'</a></span></td><td colspan="2"><table class="audit_log" style="width: 100%">';
					foreach ($etask_arr[$etask_lab_arr_key][$etask_lab_etype_arr_key] as $etask_info => $etask_count) {
						echo '<tr><td style="width: 40%;">'.$etask_info.'</td><td style="width: 15%;"><span>'.(($etask_lookup[$etask_lab_etype_arr_key]['allow_retry']) ? ' <button class="retry_btn" type="button" title="Retry" value=\''.json_encode(array('l_id' => $etask_lab_arr_key, 'e_id' => $etask_lab_etype_arr_key, 'e_value' => base64_encode($etask_info))).'\'></button><div class="emsa_toolbar_separator"></div>' : '').'<span style="display: inline-block; vertical-align: middle; padding-bottom: 3px;" class="ui-icon ui-icon-newwin"></span><a href="?selected_page=6&submenu=1&type=3&f[lab][]='.$etask_lab_arr_key.'&f[eflag][]='.$etask_lab_etype_arr_key.'&f[evalue]='.urlencode(base64_encode($etask_info)).'" target="_blank" title="View all messages with \''.$etask_lookup[$etask_lab_etype_arr_key]['description'].'\' errors containing a value of \''.htmlentities($etask_info).'\'...">'.intval($etask_count).' error'.((intval($etask_count) > 1) ? 's' : '').'</a></span></td></tr>';
					}
					echo "</table></td></tr>";
				}
				
				echo "</tbody></table>";
				echo '</div>';
			}
			
			echo '</div>';
			
			/*

			<table id="labResults">
				<thead>
					<tr>
						<th style="width: 25%;">Message Details</th>
						<th style="width: 14%;">Event Date/Time</th>
						<th style="width: 12%;">User</th>
						<th style="width: 8%;">Category</th>
						<th style="width: 35%;">Action</th>
						<th style="width: 6%;">Status</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
			
			*/
		} else {
			highlight("No Exceptions Found!");
		}
	?>
</div>