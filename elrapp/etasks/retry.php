<?php

	include WEBROOT_URL.'/includes/message_processing_functions.php';

	$retry_msgs = array();
	
	$retry_sql = 'SELECT DISTINCT(sm.id) AS id FROM '.$my_db_schema.'system_messages sm 
					INNER JOIN '.$my_db_schema.'system_message_exceptions sme ON (sme.system_message_id = sm.id) 
					WHERE (sm.final_status = '.EXCEPTIONS_STATUS.') 
					AND (sm.lab_id = '.$clean['lab_id'].')
					AND (sme.exception_id = '.$clean['retry_exception_id'].') 
					AND ((sm.deleted IS NULL) OR (sm.deleted NOT IN (1, 2)))
					'.((isset($clean['retry_exception_value']) && !empty($clean['retry_exception_value'])) ? 'AND (sme.info = \''.@pg_escape_string($clean['retry_exception_value']).'\')' : '').';';
	$retry_rs = @pg_query($host_pa, $retry_sql);
	if ($retry_rs !== false && @pg_num_rows($retry_rs) > 0) {
		while ($retry_row = @pg_fetch_object($retry_rs)) {
			$retry_msgs[] = intval($retry_row->id);
		}
		@pg_free_result($retry_rs);
	} else {
		suicide('Unable to retrieve list of messages to retry.', 1);
	}
	
	if (count($retry_msgs) > 0) {
		bulkRetryWrapper($retry_msgs);
	}
	
?>