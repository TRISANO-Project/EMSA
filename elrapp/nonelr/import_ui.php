<script>
	$(function() {
		$( "#upload_button" ).button({
			icons: {
				primary: "ui-icon-elrsave"
			}
		});
		
		$("#vocab_type_mastervocab").click(function() {
			$("#vocab_child_lab").hide('fast');
		});
		
		$("#vocab_type_trisano").click(function() {
			$("#vocab_child_lab").hide('fast');
		});
		
		$("#vocab_type_master").click(function() {
			$("#vocab_child_lab").hide('fast');
		});
		
		$("#vocab_type_child").click(function() {
			$("#vocab_child_lab").show('fast');
		});
		
		$("#vocab_type_childvocab").click(function() {
			$("#vocab_child_lab").show('fast');
		});
	});
</script>

<style type="text/css">
	.ui-widget { font-family: 'Open Sans' !important; font-weight: 400 !important; }
	.ui-widget strong { font-family: 'Open Sans' !important; font-weight: 600 !important; }
	label { font-family: 'Open Sans' !important; font-weight: 600 !important; }
	input { background-color: lightcyan; width: 50%; margin: 10px; font-family: Consolas, 'Courier New', Courier, serif !important; }
</style>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrimport"></span>Import Non-ELR CSV</h1>

<?php
	if ($process_import) {
		include 'import_process.php';  // process non-ELR CSV import
		echo '<hr>';
	}
?>

<div class="import_widget ui-widget">
	<div>
		<p>To import new non-ELR records into UTNEDSS, select the CSV file below and click <strong>Import Records</strong>.</p>
		<?php //highlight('Warning:  Size of file uploaded must not exceed '.ini_get('upload_max_filesize').'.'); ?>
		<?php highlight('<strong>Important:</strong> System is currently configured to import data to TEST environment only.', 'ui-icon-elrcancel'); ?>
	</div>
</div>

<div class="import_widget ui-widget ui-widget-content ui-corner-all" style="padding: 15px;">
	<form name="import_uploader" id="import_uploader" method="POST" enctype="multipart/form-data">
		<p><label for="csv_type">Type of CSV data:</label><br><input type="text" class="ui-corner-all" id="csv_type" name="csv_type" value="<?php echo $clean['import_csv_type']; ?>"></p>
		<p><label for="source_csv">Select the CSV file containing the non-ELR data to import:</label><br>
		<input type="file" name="source_csv" id="source_csv" class="ui-corner-all"></p>
		<p><button id="upload_button" type="submit">Import Records</button></p>
		<input type="hidden" name="import_flag" id="import_flag" value="1">
		<input type="hidden" name="selected_page" id="selected_page" value="8">
		<input type="hidden" name="submenu" id="submenu" value="1">
	</form>
</div>