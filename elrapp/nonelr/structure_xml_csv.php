<?php

	// handle setting of the data type to manage
	if (isset($_GET['q'])) {
		if (empty($_GET['q'])) {
			// user reset/searched with no data type, destroy saved param
			unset($_SESSION['nonelr_csv_mapping']['type']);
		} else {
			// set saved data type
			$_SESSION['nonelr_csv_mapping']['type'] = filter_var(trim($_GET['q']), FILTER_SANITIZE_STRING);
		}
	}
	
?>

<script>
	$(function() {
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elradd"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_element").focus();
			$(this).hide();
		});
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_savelab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$(".edit_lab").button({
            icons: { primary: "ui-icon-elredit" }
        }).next().button({
            icons: { primary: "ui-icon-elrdelete" }
        }).parent().buttonset();
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false
		});
		
		$(".delete_lab").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=8&submenu=3&delete_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$("#edit_lab_dialog").dialog({
			autoOpen: false,
			width: 600,
			modal: true
		});
		
		$(".edit_lab").click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id) {
				$("#edit_id").val(jsonObj.id);
				$("#edit_column").val(jsonObj.colname);
				$("#edit_type").val(jsonObj.datatype);
				$("#edit_apppath").val(jsonObj.app_path_id);
				if (jsonObj.required == "t") {
					$("#edit_required_yes").click();
				} else {
					$("#edit_required_no").click();
				}
				if (jsonObj.rowid == "t") {
					$("#edit_rowid_yes").click();
				} else {
					$("#edit_rowid_no").click();
				}
				if (jsonObj.district_source == "t") {
					$("#edit_districtsource_yes").click();
				} else {
					$("#edit_districtsource_no").click();
				}
				
				$("#edit_lab_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#edit_modal_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#edit_lab_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
		$("#clear_filters").button({
            icons: {
                primary: "ui-icon-elrcancel"
            }
        }).click(function() {
			$("#search_form")[0].reset();
			$("#q").val("").blur();
			$("#search_form").submit();
		});
		
		$("#q_go").button({
			icons: {
                primary: "ui-icon-elrsearch"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
	});
</script>

<?php

	if (isset($_GET['edit_id'])) {
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path_csv WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['edit_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to save changes to CSV XML element.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide('Unable to save changes to mapping -- CSV XML element does not exist.');
		} else {
			$edit_sql = sprintf("UPDATE %sstructure_path_csv SET csv_column = %s, application_path_id = %s, required = %s, id_field = %s, district_source = %s WHERE id = %d;",
				$my_db_schema,
				((strlen(trim($_GET['edit_column'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_column']))."'" : 'NULL'),
				((intval(trim($_GET['edit_apppath'])) > 0) ? intval(trim($_GET['edit_apppath'])) : 'NULL'),
				((trim($_GET['edit_required']) == 'true') ? 'true' : 'false'), 
				((trim($_GET['edit_rowid']) == 'true') ? 'true' : 'false'), 
				((trim($_GET['edit_districtsource']) == 'true') ? 'true' : 'false'), 
				intval(trim($_GET['edit_id']))
			);
			if (@pg_query($host_pa, $edit_sql)) {
				highlight('CSV XML element successfully updated!', 'ui-icon-elrsuccess');
			} else {
				suicide('Unable to save changes to CSV XML element.', 1);
			}
		}
	} elseif (isset($_GET['delete_id'])) {
		########## delete lab ##########
		
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path_csv WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to delete CSV XML element.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to delete CSV XML element -- record not found.");
		} else {
			// everything checks out, commit the delete...
			$delete_sql = sprintf("DELETE FROM ONLY %sstructure_path_csv WHERE id = %d;", $my_db_schema, intval($_GET['delete_id']));
			if (@pg_query($host_pa, $delete_sql)) {
				highlight('CSV XML element successfully deleted!', 'ui-icon-elrsuccess');
			} else {
				suicide('Unable to delete CSV XML element.', 1);
			}
		}
	} elseif (isset($_GET['add_flag'])) {
		// add new lab
		if ((strlen(trim($_GET['new_type'])) > 0) && (strlen(trim($_GET['new_column'])) > 0)) {
			$addlab_sql = sprintf("INSERT INTO %sstructure_path_csv (csv_type, csv_column, application_path_id, required, id_field, district_source) VALUES (%s, %s, %s, %s, %s, %s)",
				$my_db_schema,
				"'".getNonELRDataType(trim($_GET['new_type']), 1)."'", 
				((strlen(trim($_GET['new_column'])) > 0) ? "'".pg_escape_string(trim($_GET['new_column']))."'" : "NULL"), 
				((intval(trim($_GET['new_apppath'])) > 0) ? intval(trim($_GET['new_apppath'])) : "NULL"), 
				((trim($_GET['new_required']) == 'true') ? 'true' : 'false'), 
				((trim($_GET['new_rowid']) == 'true') ? 'true' : 'false'), 
				((trim($_GET['new_districtsource']) == 'true') ? 'true' : 'false')
			);
			@pg_query($host_pa, $addlab_sql) or suicide('Could not add new CSV XML element.', 1);
			highlight('New CSV XML element "'.htmlentities(trim($_GET['new_element'])).'" added successfully!', 'ui-icon-elrsuccess');
		} else {
			suicide('Missing CSV Data Type and/or column name!  Please specify a data type & column name and try again.');
		}
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrxml"></span>CSV XML Structure</h1>

<form name="search_form" id="search_form" method="GET" action="<?php echo $main_url; ?>">

<div class="vocab_search ui-tabs ui-widget">
	<label for="q" class="vocab_search_form">Mapping for Data Type:</label><br><input type="text" name="q" id="q" class="vocab_query ui-corner-all" placeholder="Enter a CSV data type..." value="<?php echo htmlentities($_SESSION['nonelr_csv_mapping']['type'], ENT_QUOTES, "UTF-8"); ?>">
	<button name="q_go" id="q_go" title="Set/update the CSV Data Type to manage XML mapping for">Set Data Type</button>
	<button type="button" name="clear_filters" id="clear_filters" title="Clear Data Type">Reset</button>
	<button type="button" id="addnew_button" title="Add a new CSV XML element">Add New CSV XML Element</button>
</div>

<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
<input type="hidden" name="submenu" value="<?php echo $submenu; ?>">

</form>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New CSV XML Element:</label><br><br></div>
	<form id="new_lab_form" method="GET" action="<?php echo $main_page; ?>">
		<label class="vocab_search_form2" for="new_type">Data Type:</label><input class="ui-corner-all" type="text" name="new_type" id="new_type" value="<?php echo htmlentities($_SESSION['nonelr_csv_mapping']['type'], ENT_QUOTES, "UTF-8"); ?>" />
		<label class="vocab_search_form2" for="new_column">CSV Column Name:</label><input class="ui-corner-all" type="text" name="new_column" id="new_column" />
		<br><br><label class="vocab_search_form2" for="new_apppath">Application XPath:</label>
			<select class="ui-corner-all" name="new_apppath" id="new_apppath">
				<option value="0" selected>--</option>
			<?php
				// get list of XML paths for menu
				$path_sql = sprintf("SELECT DISTINCT id, element, xpath FROM %sstructure_path_application ORDER BY element;", $my_db_schema);
				$path_rs = @pg_query($host_pa, $path_sql) or suicide("Unable to retrieve list of XML paths.", 1, 1);
				while ($path_row = pg_fetch_object($path_rs)) {
					printf("<option value=\"%d\">%s (%s)</option>", intval($path_row->id), htmlentities($path_row->element, ENT_QUOTES, "UTF-8"), htmlentities($path_row->xpath, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($path_rs);
			?>
			</select>
		<br><br><label class="vocab_search_form2">Required?:</label>
			<label class="vocab_search_form2" for="new_required_yes"><input class="edit_radio ui-corner-all" type="radio" name="new_required" id="new_required_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label class="vocab_search_form2" for="new_required_no"><input class="edit_radio ui-corner-all" type="radio" name="new_required" id="new_required_no" value="false" /> No</label>
		<br><br><label class="vocab_search_form2">Is Record ID?:</label>
			<label class="vocab_search_form2" for="new_rowid_yes"><input class="edit_radio ui-corner-all" type="radio" name="new_rowid" id="new_rowid_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label class="vocab_search_form2" for="new_rowid_no"><input class="edit_radio ui-corner-all" type="radio" name="new_rowid" id="new_rowid_no" value="false" /> No</label>
		<br><br><label class="vocab_search_form2">Sets Jurisdiction?:</label>
			<label class="vocab_search_form2" for="new_districtsource_yes"><input class="edit_radio ui-corner-all" type="radio" name="new_districtsource" id="new_districtsource_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label class="vocab_search_form2" for="new_districtsource_no"><input class="edit_radio ui-corner-all" type="radio" name="new_districtsource" id="new_districtsource_no" value="false" /> No</label>
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_savelab" id="new_savelab">Save New CSV XML Element</button>
		<button type="button" id="addnew_cancel">Cancel</button>
	</form>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th>CSV Column Name</th>
				<th>Application XPath</th>
				<th>Required?</th>
				<th>Is Record ID?</th>
				<th>Sets Jurisdiction?</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$xml_qry = 'SELECT c.id AS id, c.csv_column AS colname, c.required AS required, c.id_field AS rowid, c.district_source as district_source, a.id AS app_path_id, a.xpath AS app_path FROM '.$my_db_schema.'structure_path_csv c LEFT JOIN '.$my_db_schema.'structure_path_application a ON (c.application_path_id = a.id) WHERE c.csv_type = \''.getNonELRDataType($_SESSION['nonelr_csv_mapping']['type'], 1).'\' ORDER BY a.xpath, c.id;';
	$xml_rs = @pg_query($host_pa, $xml_qry) or die("Could not connect to database: ".pg_last_error());
	
	while ($xml_row = @pg_fetch_object($xml_rs)) {
		echo "<tr>";
		printf("<td class=\"action_col\">");
		unset($edit_lab_params);
		$edit_lab_params = array(
			"id" => intval($xml_row->id), 
			"colname" => htmlentities($xml_row->colname, ENT_QUOTES, "UTF-8"), 
			"required" => trim($xml_row->required), 
			"rowid" => trim($xml_row->rowid), 
			"district_source" => trim($xml_row->district_source), 
			"app_path_id" => intval($xml_row->app_path_id)
		);
		printf("<button class=\"edit_lab\" type=\"button\" value='%s' title=\"Edit this record\">Edit</button>", json_encode($edit_lab_params));
		printf("<button class=\"delete_lab\" type=\"button\" value=\"%s\" title=\"Delete this record\">Delete</button>", $xml_row->id);
		echo "</td>";
		echo "<td>".htmlentities($xml_row->colname)."</td>";
		echo "<td>".htmlentities($xml_row->app_path)."</td>";
		echo "<td>".((trim($xml_row->required) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Required\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"Not Required\"></span>")."</td>";
		echo "<td>".((trim($xml_row->rowid) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Yes\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"No\"></span>")."</td>";
		echo "<td>".((trim($xml_row->district_source) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Yes\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"No\"></span>")."</td>";
		echo "</tr>";
	}
	
	pg_free_result($xml_rs);

?>

		</tbody>
	</table>
	
</div>

<div id="confirm_delete_dialog" title="Delete this CSV XML Element?">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This CSV XML element will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="edit_lab_dialog" title="Edit CSV XML Element">
	<form id="edit_modal_form" method="GET" action="<?php echo $main_page; ?>">
		<label for="edit_column">Column Name:</label><br><input class="ui-corner-all" type="text" name="edit_column" id="edit_column" /><br><br>
		<label for="edit_apppath">Application XPath:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_apppath" id="edit_apppath">
			<option value="0" selected>--</option>
		<?php
			// get list of XML paths for menu
			$path_sql = sprintf("SELECT DISTINCT id, element, xpath FROM %sstructure_path_application ORDER BY element;", $my_db_schema);
			$path_rs = @pg_query($host_pa, $path_sql) or suicide("Unable to retrieve list of XML paths.", 1, 1);
			while ($path_row = pg_fetch_object($path_rs)) {
				printf("<option value=\"%d\">%s (%s)</option>", intval($path_row->id), htmlentities($path_row->element, ENT_QUOTES, "UTF-8"), htmlentities($path_row->xpath, ENT_QUOTES, "UTF-8"));
			}
			pg_free_result($path_rs);
		?>
		</select><br><br>
		<label>Required?:</label><br>
			<label for="edit_required_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_required" id="edit_required_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label for="edit_required_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_required" id="edit_required_no" value="false" /> No</label>
		<br><br><label>Is Record ID?:</label><br>
			<label for="edit_rowid_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_rowid" id="edit_rowid_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label for="edit_rowid_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_rowid" id="edit_rowid_no" value="false" /> No</label>
		<br><br><label>Sets Jurisdiction?:</label><br>
			<label for="edit_districtsource_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_districtsource" id="edit_districtsource_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label for="edit_districtsource_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_districtsource" id="edit_districtsource_no" value="false" /> No</label>
		<input type="hidden" name="edit_id" id="edit_id" />
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
	</form>
</div>