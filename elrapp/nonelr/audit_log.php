<?php

	// handle setting of the data type to manage
	if (isset($_GET['q'])) {
		if (empty($_GET['q'])) {
			// user reset/searched with no data type, destroy saved param
			unset($_SESSION['nonelr_audit_log']['type']);
		} else {
			// set saved data type
			$_SESSION['nonelr_audit_log']['type'] = filter_var(trim($_GET['q']), FILTER_SANITIZE_STRING);
		}
	}
	
?>

<script>
	$(function() {
		$("#addnew_form").show();
		
		$("#new_savelab").button();
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
		$(".date-range").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
		
		$("#clear_filters").button({
            icons: {
                primary: "ui-icon-elrcancel"
            }
        }).click(function() {
			$("#search_form")[0].reset();
			$("#q").val("").blur();
			$("#search_form").submit();
		});
		
		$("#q_go").button({
			icons: {
                primary: "ui-icon-elrsearch"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
	});
</script>

<style type="text/css">
	input[type="radio"] { min-width: 0 !important; }
	#new_lab_form div { display: inline-block; margin: 7px; padding: 10px; }
	#labResults td { border-bottom-width: 2px; border-bottom-color: darkorange; }
	.audit_log td { border-bottom-width: 1px !important; border-bottom-color: lightgray !important; }
</style>

<?php

	if (isset($_GET['message_id']) && filter_var(trim($_GET['message_id']), FILTER_VALIDATE_INT)) {
		$clean['message_id'] = filter_var(trim($_GET['message_id']), FILTER_SANITIZE_NUMBER_INT);
	}
	
	if (isset($_GET['view_type']) && filter_var($_GET['view_type'], FILTER_VALIDATE_INT) && in_array(intval(trim($_GET['view_type'])), array(1, 2, 3))) {
		$clean['view_type'] = filter_var(trim($_GET['view_type']), FILTER_SANITIZE_NUMBER_INT);
	}
	
	if (isset($_GET['date_from']) && (strlen(trim($_GET['date_from'])) > 0)) {
		$clean['date_from'] = filter_var(trim($_GET['date_from']), FILTER_SANITIZE_STRING);
	}

	if (isset($_GET['date_to']) && (strlen(trim($_GET['date_to'])) > 0)) {
		$clean['date_to'] = filter_var(trim($_GET['date_to']), FILTER_SANITIZE_STRING);
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elraudit"></span>Non-ELR Data Import Audit Log</h1>

<form name="search_form" id="search_form" method="GET" action="<?php echo $main_url; ?>">

<div class="vocab_search ui-tabs ui-widget">
	<label for="q" class="vocab_search_form">View Audit Logs for Data Type:</label><br><input type="text" name="q" id="q" class="vocab_query ui-corner-all" placeholder="Enter a CSV data type..." value="<?php echo htmlentities($_SESSION['nonelr_audit_log']['type'], ENT_QUOTES, "UTF-8"); ?>">
	<button name="q_go" id="q_go" title="Set/update the CSV Data Type to manage XML mapping for">Set Data Type</button>
	<button type="button" name="clear_filters" id="clear_filters" title="Clear Data Type">Reset</button>
</div>

<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
<input type="hidden" name="submenu" value="<?php echo $submenu; ?>">

</form>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">View Audit Logs By:</label></div>
	<form id="new_lab_form" method="GET" action="<?php echo $main_page; ?>">
		<div class="ui-widget-content ui-corner-all"><input type="radio" name="view_type" id="view_type_recent" value="3"<?php echo ((isset($clean['view_type']) && ($clean['view_type'] == 3)) ? " checked" : ""); ?>/><label class="vocab_search_form2" for="view_type_recent">View Most-Recent Imports</label></div>
		<div class="ui-widget-content ui-corner-all"><input type="radio" name="view_type" id="view_type_date" value="2"<?php echo ((isset($clean['view_type']) && ($clean['view_type'] == 2)) ? " checked" : ""); ?>/><label class="vocab_search_form2" for="view_type_date">View by "Date Imported" Range: <input class="date-range ui-corner-all" type="text" name="date_from" id="date_from" value="<?php echo ((isset($clean['date_from'])) ? $clean['date_from'] : ""); ?>" placeholder="Any Time"> to <input class="date-range ui-corner-all" type="text" name="date_to" id="date_to" value="<?php echo ((isset($clean['date_to'])) ? $clean['date_to'] : ""); ?>" placeholder="Present"></label></div>
		<input type="hidden" name="selected_page" id="selected_page" value="<?php echo intval($selected_page); ?>">
		<input type="hidden" name="submenu" id="submenu" value="<?php echo intval($submenu); ?>">
		<br><button type="submit" name="new_savelab" id="new_savelab">View Selected Audit Logs</button>
	</form>
</div>

<?php

	// check for view type
	if (isset($clean['view_type'])) {
		// valid 'view_type' flag
		
?>
<div class="lab_results_container ui-widget ui-corner-all">
	<h3>Message Audits...</h3>
	<table id="labResults">
		<thead>
			<tr>
				<th style="width: 25%;">Message Details</th>
				<th style="width: 14%;">Import Date/Time</th>
				<th style="width: 10%;">ID1</th>
				<th style="width: 10%;">ID2</th>
				<th style="width: 41%;">Status</th>
			</tr>
		</thead>
		<tbody>
<?php

		if ($clean['view_type'] == 1) {
			// view by message id
			/* not used... yet
			$outer_qry = "SELECT DISTINCT a.system_message_id, m.original_message_id, s.name, o.created_at 
				FROM ".$my_db_schema."system_messages_audits a 
				INNER JOIN ".$my_db_schema."system_messages m ON (a.system_message_id = m.id) 
				INNER JOIN ".$my_db_schema."system_original_messages o ON (m.original_message_id = o.id)
				LEFT JOIN ".$my_db_schema."system_statuses s ON (m.final_status = s.id) 
				WHERE (a.system_message_id = ".$clean['message_id'].") OR (m.original_message_id = ".$clean['message_id'].") 
				GROUP BY a.system_message_id, m.original_message_id, s.name, o.created_at
				ORDER BY o.created_at DESC;";
			*/
		} elseif ($clean['view_type'] == 2) {
			// view by date range
			unset($date_range_clause);
			if (isset($clean['date_from']) && isset($clean['date_to'])) {
				// between
				$date_range_clause = "AND (a.created_at BETWEEN '" . date(DATE_W3C, strtotime($clean['date_from'])) . "' AND '" . date(DATE_W3C, strtotime($clean['date_to'])) . "') ";
			} elseif (isset($clean['date_from'])) {
				// start to infinity
				$date_range_clause = "AND (a.created_at > '" . date(DATE_W3C, strtotime($clean['date_from'])) . "') ";
			} elseif (isset($clean['date_to'])) {
				// infinity to end
				$date_range_clause = "AND (a.created_at < '" . date(DATE_W3C, strtotime($clean['date_to'])) . "') ";
			} else {
				$date_range_clause = "";
			}
			$outer_qry = 'SELECT a.* FROM '.$my_db_schema.'csv_audits_aggregate a 
				WHERE a.csv_type = \''.getNonELRDataType($_SESSION['nonelr_audit_log']['type'], 2).'\' '.$date_range_clause.'ORDER BY a.created_at DESC LIMIT 50;';
		} else {
			// view by most-recent
			$outer_qry = 'SELECT a.* FROM '.$my_db_schema.'csv_audits_aggregate a 
				WHERE a.csv_type = \''.getNonELRDataType($_SESSION['nonelr_audit_log']['type'], 2).'\' ORDER BY a.created_at DESC LIMIT 50;';
		}
		
		$outer_rs = @pg_query($host_pa, $outer_qry);
		if ($outer_rs) {
			if (pg_num_rows($outer_rs) > 0) {
				// draw results
				while ($outer_row = pg_fetch_object($outer_rs)) {
					echo '<tr>';
					echo '<td style="text-align: left; vertical-align: top;">Import ID:<br><strong>'.trim($outer_row->import_id).'</strong><br><br>Imported:<br><strong>'.date('Y-m-d H:i:s', strtotime($outer_row->created_at)).'</strong>
						<br><br><strong>Import Stats:</strong><br>
						<strong>'.intval($outer_row->rows_found).'</strong> records found<br>
						<strong>'.intval($outer_row->rows_success).'</strong> records imported<br>
						<strong>'.intval($outer_row->rows_skipped).'</strong> records excluded<br>
						<strong>'.intval($outer_row->rows_error).'</strong> errors
						</td>';
					echo '<td colspan="5" style="text-align: left; vertical-align: top;"><table class="audit_log">';
					echo '<tbody>';
					
					// get individual audits for this message
					unset($inner_qry);
					unset($inner_rs);
					unset($inner_row);
					$inner_qry = 'SELECT d.created_at, d.id1, d.id2, d.status_description, case when d.status = 1 then \'Success\' when d.status = -1 then \'Error (%s)\' else \'Skipped (%s)\' end as status FROM '.$my_db_schema.'csv_audits_detail d WHERE d.csv_type = \''.getNonELRDataType($_SESSION['nonelr_audit_log']['type'], 2).'\' AND d.import_id = \''.trim($outer_row->import_id).'\' ORDER BY d.created_at DESC;';
					$inner_rs = @pg_query($host_pa, $inner_qry);
					while($inner_row = pg_fetch_object($inner_rs)) {
						echo '<tr>';
						echo '<td style="width: 14%;">'.date("m/d/Y H:i:s", strtotime($inner_row->created_at)).'</td>';
						echo '<td style="width: 10%;">'.htmlentities($inner_row->id1, ENT_QUOTES, 'UTF-8').'</td>';
						echo '<td style="width: 10%;">'.htmlentities($inner_row->id2, ENT_QUOTES, 'UTF-8').'</td>';
						echo '<td style="width: 41%;">'.htmlentities(sprintf($inner_row->status, $inner_row->status_description), ENT_QUOTES, 'UTF-8').'</td>';
						echo '</tr>';
					}
					@pg_free_result($inner_rs);
					
					echo '</tbody></table></td>';
					echo '</tr>';
				}
			} else {
				// draw 'no results'
				echo '<tr><td colspan="6"><em>No messages found that match your criteria</em></td></tr>';
				
			}
		} else {
			suicide('Could not connect to Audit Log database.', 1);
		
		}
		@pg_free_result($outer_rs);

?>

		</tbody>
	</table>
	
<?php

	}
	
?>
	
</div>