<?php

	/**
	 * import_process.php
	 * Handles importing of non-ELR data from external CSV files into UTNEDSS system.
	 *
	 * includes 'functions.php'
	 */

	$old_max_time = ini_get('max_execution_time');
	ini_set('max_execution_time', 10800);
	
	include_once 'functions.php';
	
	$time_start = microtime(true); // elapsed time debugging
	
	unset($nedss_mapping);
	unset($csv_cols);
	unset($csv_data);
	unset($csv_row);
	$row = 0;
	$audit_event_id = generateEventId();
	
	$stats = array(
		'found' => 0, 
		'success' => 0, 
		'invalid' => 0, 
		'error' => 0
	);
	
	if (isset($_POST['csv_type']) && !empty($_POST['csv_type'])) {
		$clean['import_csv_type_hash'] = getNonELRDataType(filter_var(trim($_POST['csv_type']), FILTER_SANITIZE_STRING), 1);
		$clean['import_csv_type'] = filter_var(trim($_POST['csv_type']), FILTER_SANITIZE_STRING);
	
		$mapping_qry = 'SELECT c.csv_column AS colname, c.required AS required, c.id_field AS id_field, c.district_source AS district_source, a.xpath AS xpath, a.structure_lookup_operator_id AS lookup_id, sca.category_id AS vocab_category, sca.app_table AS app_table, sca.app_category AS app_category FROM '.$my_db_schema.'structure_path_csv c LEFT JOIN '.$my_db_schema.'structure_path_application a ON (c.application_path_id = a.id) LEFT JOIN '.$my_db_schema.'structure_category_application sca ON ((a.category_application_id = sca.id) AND (sca.app_id = 1)) WHERE (c.application_path_id IS NOT NULL) AND (c.csv_type = \''.$clean['import_csv_type_hash'].'\');';
		$mapping_rs = @pg_query($host_pa, $mapping_qry);
		if ($mapping_rs !== false) {
			if (pg_num_rows($mapping_rs) > 0) {
				while ($mapping_row = @pg_fetch_object($mapping_rs)) {
					$nedss_mapping[$mapping_row->colname] = array(
						'xpath' => $mapping_row->xpath, 
						'lookup' => $mapping_row->lookup_id, 
						'vocab_category' => $mapping_row->vocab_category, 
						'table' => $mapping_row->app_table, 
						'category' => $mapping_row->app_category, 
						'required' => (($mapping_row->required == 't') ? true : false), 
						'id_field' => (($mapping_row->id_field == 't') ? true : false), 
						'district_source' => (($mapping_row->district_source == 't') ? true : false)
					);
				}
			} else {
				suicide('Invalid data type entered.  Please check the data type and try again.', -1);
			}
		} else {
			suicide('A database error occurred.  Unable to complete import.', 1);
		}
		@pg_free_result($mapping_rs);
	
	} else {
		suicide('No data type entered.  Unable to continue with import.', -1);
	}
	
	if (isset($nedss_mapping) && is_array($nedss_mapping)) {
		$example_xml = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
</health_message>
EOX;
	
		try {
			$soap_client = @new SoapClient($props['master_wsdl_url']);
		} catch (Exception $e) {
			suicide($e, -1, 1);
		} catch (SoapFault $f) {
			suicide($f, -1, 1);
		}
		
		if ($soap_client) {
			try {
				$soap_result = $soap_client->getTrisanoExample(array("healthMessage" => $final_xml));
			} catch (Exception $e) {
				suicide('Unable to get TriSano XML template.  The following errors occurred:<pre>'.$e.'</pre>', -1, 1);
			} catch (SoapFault $f) {
				suicide('Unable to get TriSano XML template.  The following errors occurred:<pre>'.$f.'</pre>', -1, 1);
			}
			$trisano_template = simplexml_load_string($soap_result->return);
			unset($trisano_template->status);  // clear the <status/> code
			unset($trisano_template->status_message);  // clear any <status_message/> codes
			$trisano_template->addChild('system', 'TRISANO');  // set <system>TRISANO</system> to ensure proper processing
			$trisano_template->addChild('username', '9999');  // set username
			$trisano_template->trisano_health->events->type = 'MorbidityEvent';  // set to a 'Morbidity' Event
			$trisano_template->trisano_health->events->workflow_state = 'closed';  // set to a 'Closed' workflow state
		}
		unset($soap_client);
		
		if (isset($_FILES['source_csv']) && ($_FILES['source_csv']['error'] === 0)) {
			// successful upload
			$csv = fopen($_FILES['source_csv']['tmp_name'], 'rb');
			while (($csv_row = fgetcsv($csv, 0)) !== FALSE) {
				if ($row == 0) {
					foreach ($csv_row as $csv_row_key => $csv_row_data) {
						$csv_cols[$csv_row_key] = $csv_row_data;
					}
				} else {
					$stats['found']++;
					foreach ($csv_cols as $csv_col_key => $csv_col_name) {
						if (isset($nedss_mapping[$csv_col_name])) {
							if ($nedss_mapping[$csv_col_name]['lookup'] == 1) {
								// direct copy
								if (($csv_col_name == 'dob') || (stripos($csv_col_name, '_dt') !== false)) {
									// handle date field adjustments
									$csv_data[$row][$csv_col_name] = dateCleanup1($csv_row[$csv_col_key]);
								} elseif (stripos($csv_col_name, '_phone') !== false) {
									// split phone to phone & area code if CSV doesn't have a native 'area code' field
									$csv_data[$row][$csv_col_name] = phoneSplit1($csv_row[$csv_col_key], 'phone_number'); // phone number part
									$csv_data[$row][str_replace('_phone', '_areacode', $csv_col_name)] = phoneSplit1($csv_row[$csv_col_key], 'area_code'); // area code part
								} else {
									$csv_data[$row][$csv_col_name] = $csv_row[$csv_col_key];
								}
							} elseif ($nedss_mapping[$csv_col_name]['lookup'] == 2) {
								// code lookup
								$csv_data[$row][$csv_col_name] = getTrisanoIDByValue(getTrisanoValueFromChild($csv_row[$csv_col_key], $nedss_mapping[$csv_col_name]['vocab_category'], $clean['import_csv_type']), $nedss_mapping[$csv_col_name]['table'], $nedss_mapping[$csv_col_name]['category']);
							} elseif ($nedss_mapping[$csv_col_name]['lookup'] == 3) {
								// complex
								if (stripos($nedss_mapping[$csv_col_name]['xpath'], 'disease_id') !== false) {
									// check if this is for the disease_id path & do a code lookup instead
									$csv_data[$row][$csv_col_name] = getTrisanoIDByValue(getTrisanoValueFromChild($csv_row[$csv_col_key], $nedss_mapping[$csv_col_name]['vocab_category'], $clean['import_csv_type']), $nedss_mapping[$csv_col_name]['table'], $nedss_mapping[$csv_col_name]['category']);
								} else {
									if (($csv_col_name == 'dob') || (stripos($csv_col_name, '_dt') !== false)) {
										// handle date field adjustments
										$csv_data[$row][$csv_col_name] = dateCleanup1($csv_row[$csv_col_key]);
									} else {
										$csv_data[$row][$csv_col_name] = $csv_row[$csv_col_key];
									}
								}
							}
						}
					}
				}
				$row++;
			}
			
			echo '['.elapsed_time($time_start).'] - Starting import ID# '.$audit_event_id.'...<br>';
			
			#debug 
			echo "<pre>";
			#debug print_r($csv_data);
			#debug print_r($nedss_mapping);
			
			// iterate through all the results, try to process them
			foreach ($csv_data as $csv_data_arr) {
				#debug 
				echo '<hr style="width: 100%; height: 10px; background-color: purple;">';
				#debug 
				//print_r(htmlentities(formatXml(processToTrisano($trisano_template, $csv_data_arr)->asXML())));
				echo '['.elapsed_time($time_start).'] - Importing '.$csv_data_arr['ehars_uid'].'...<br>';
				processToTrisano($trisano_template, $csv_data_arr);
			}
			
			#debug 
			echo "</pre>";
			
			// log aggregate stats of rows found, inserted, etc.
			auditCSVAggregate();
			
			highlight('<strong>Import complete in '.elapsed_time($time_start).'!  Found '.$stats['found'].' records:</strong><ul><li><strong>'.$stats['success'].'</strong> records generated new TriSano events,</li><li><strong>'.$stats['invalid'].'</strong> records were skipped due to missing required data/not meeting import criteria, and </li><li><strong>'.$stats['error'].'</strong> records did not successfully process due to an error from TriSano.</li></ul>', 'ui-icon-elrsuccess');
		} else {
			suicide('Unable to process upload.  No file specified', -1);
		}
	}
	
	ini_set('max_execution_time', $old_max_time);

?>