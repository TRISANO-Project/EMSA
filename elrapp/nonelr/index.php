<?php

	if ($submenu < 2) {
		include './nonelr/import_ui.php';  // UI for non-ELR CSV import
	} elseif ($submenu == 2) {
		include './nonelr/audit_log.php';  // non-ELR audit log
	} elseif ($submenu == 3) {
		include './nonelr/structure_xml_csv.php';  // non-ELR XML mapping to NEDSS
	}
	
?>