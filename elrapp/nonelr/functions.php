<?php

	function elapsed_time($start_time) {
		return round((microtime(true) - $start_time), 3)."s";
	}
	
	function generateEventId() {
		$time_data = (object) getdate();
		$event_str = trim($time_data->year).trim($time_data->mon).trim($time_data->mday).trim($time_data->hours).trim($time_data->minutes).trim($time_data->seconds);
		return $event_str;
	}
	
	function getTrisanoValueFromChild($child_value, $vocab_category, $lab_name) {
		global $host_pa, $my_db_schema, $props;
		
		$lab_id_qry = 'SELECT id FROM '.$my_db_schema.'structure_labs WHERE ui_name = \''.$lab_name.'\';';
		$lab_id = @pg_fetch_result(@pg_query($host_pa, $lab_id_qry), 0, 'id');
		
		$qry = 'SELECT m2a.coded_value AS coded_value
				FROM '.$my_db_schema.'vocab_master2app m2a
				INNER JOIN '.$my_db_schema.'vocab_master_vocab mv ON (m2a.master_id = mv.id AND mv.category = '.intval($vocab_category).')
				INNER JOIN '.$my_db_schema.'vocab_child_vocab cv ON (mv.id = cv.master_id AND cv.lab_id = '.intval($lab_id).' AND cv.concept ILIKE \''.pg_escape_string(trim($child_value)).'\')
				WHERE m2a.app_id = 1;';
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			if (pg_num_rows($rs) == 1) {
				return pg_fetch_result($rs, 0, 'coded_value');
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	function getTrisanoIDByValue($coded_value, $table, $category) {
		if (strlen(trim($coded_value)) < 1) {
			return null;
		}
		
		switch ($table) {
			case 'codes':
				$primary_field = 'the_code';
				$secondary_field = 'code_name';
				break;
			case 'external_codes':
				$primary_field = 'the_code';
				$secondary_field = 'code_name';
				break;
			case 'diseases':
				$primary_field = 'disease_name';
				$secondary_field = null;
				break;
			case 'organisms':
				$primary_field = 'organism_name';
				$secondary_field = null;
				break;
			case 'common_test_types':
				$primary_field = 'common_name';
				$secondary_field = null;
				break;
			default:
				return -1;
		}
		
		if ((($table == 'codes') || ($table == 'external_codes')) && (strlen(trim($category)) < 1)) {
			return -1;
		}
		
		// prepare XML for query agent
		$base_xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><health_message><system>TRISANO</system><trisano_health/></health_message>';
		$base_sxe = new SimpleXMLElement($base_xml_string);
		
		$sub_xml = $base_sxe->trisano_health->addChild($table);
		$sub_xml->addChild($primary_field, trim($coded_value));
		
		if (!is_null($secondary_field)) {
			$sub_xml->addChild($secondary_field, trim($category));
		}
		
		$search_xml = $base_sxe->asXML();
		
		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e->getMessage(), -1, 1);
		} catch (SoapFault $f) {
			suicide($f->getMessage(), -1, 1);
		}
		
		if ($client) {
			try {
				$result = $client->findId(array("healthMessage" => $search_xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$return = simplexml_load_string($result->return);
			
			foreach ($return->trisano_health->$table as $return_item) {
				$return_val = intval($return_item->id);
			}
		}
		return $return_val;
	}
	
	function dateCleanup1($temp_datestring) {
		$temp_datestring = str_replace('........', '', $temp_datestring);
		$temp_datestring = str_replace('....', '0101', $temp_datestring);
		$temp_datestring = str_replace('..', '01', $temp_datestring);
		
		if (is_null($temp_datestring) || empty($temp_datestring)) {
			return null;
		} else {
			return date(DATE_W3C, strtotime($temp_datestring));
		}
	}
	
	function phoneSplit1($phone_string, $type) {
		if (is_null($phone_string) || empty($phone_string)) {
			// no phone number passed, return empty string
			return '';
		}
		
		$phone_number_part = null;
		$area_code_part = null;
		
		if (strlen(trim($phone_string)) >= 10) {
			// don't seem to be any greater than 10 yet, but just in case...
			// FYI, strlen() > 10 will probably cause TriSano to throw an error for the record (i.e. if the phone number part is strlen() > 7)
			$phone_number_part = substr(trim($phone_string), 3);
			$area_code_part = substr(trim($phone_string), 0, 3);
		} elseif (strlen(trim($phone_string)) > 7) {
			// not a full area code + phone number, but more than a 7-digit phone number
			$phone_number_part = substr(trim($phone_string), (strlen(trim($phone_string))-1));
			$area_code_part = substr(trim($phone_string), 0, (strlen(trim($phone_string))-1));
		} else {
			// just the phone number part only... well, at least most of it
			$phone_number_part = trim($phone_string);
			$area_code_part = '';
		}
			
		
		switch ($type) {
			case 'phone_number':
				return $phone_number_part;
				break;
			case 'area_code':
				return $area_code_part;
				break;
			default:
				return null;
				break;
		}
	}
	
	function processToTrisano($xml_template, $data) {
		global $nedss_mapping, $stats;
		$valid = true;
		$id_field = null;
		$jurisdiction_id = null;
		
		$xml_instance = simplexml_load_string($xml_template->asXML());
		
		foreach ($data as $colname => $value) {
			if ($nedss_mapping[$colname]['id_field'] === true) {
				// set record ID in case this column is flagged as the ID field
				$id_field = trim($value);
			}
			if ($nedss_mapping[$colname]['district_source'] === true) {
				// set Jurisdiction ID based on zip code
				$jurisdiction_id = assignJurisdictionByZip(trim($value));
			}
			if (($nedss_mapping[$colname]['required'] === true) && (is_null($value) || empty($value))) {
				$valid = $valid && false;
			} else {
				eval("\$xml_instance".str_replace('/', '->', $nedss_mapping[$colname]['xpath'])." = '".$value."';");
			}
		}
		if ($valid && alreadyAdded($id_field)) {
			$stats['invalid']++;
			auditCSVDetail($id_field, null, 0, 'Already Added');
			return cleanEmptyXML($xml_instance);
			//return false;
		} elseif ($valid) {
			if (!is_null($jurisdiction_id)) {
				// if jurisdiction ID found, set it in the XML
				$xml_instance->trisano_health->jurisdiction_attributes->place->id = $jurisdiction_id;
			}
			
			// clean XML to remove empty nodes
			$add_xml = cleanEmptyXML($xml_instance);
			
			// append 'attachments' node to XML after clean to prevent errors...
			$add_xml->trisano_health->addChild('attachments');
			$add_xml->trisano_health->attachments->addChild('master_id');
			
			// set jurisdiction...
			#to-do
			
			$addcmr_return = trisanoAddCmr($add_xml->asXML());
			$nedss_id = $addcmr_return->id;
			$nedss_error = $addcmr_return->info;
			if ($nedss_id !== false) {
				$stats['success']++;
				// processed successfully into TriSano
				auditCSVDetail($id_field, $nedss_id, 1, null);
			} else {
				$stats['error']++;
				// TriSano returned an error or did not return a valid Event ID
				auditCSVDetail($id_field, null, -1, 'TriSano error occurred: '.trim($nedss_error));
			}
			return $add_xml;
			//return true;
		} else {
			$stats['invalid']++;
			auditCSVDetail($id_field, null, 0, 'Missing required field');
			return cleanEmptyXML($xml_instance);
			//return false;
		}
	}
	
	function cleanEmptyXML($xml) {
		// remove empty nodes or nodes with default values
		$dxml = dom_import_simplexml($xml);
		foreach ($dxml->getElementsByTagName("*") as $node) {
			$node_has_children = false;
			// loop through child nodes of given element and see if any of them contain more XML element nodes
			foreach($node->childNodes as $childNode) {
				if (intval($childNode->nodeType) == 1) { 
					$node_has_children = $node_has_children || true;
				}
			}
			
			if (!$node_has_children) {
				if (!$node->hasChildNodes() || $node->nodeValue == '' || $node->nodeValue == '-1' || $node->nodeValue == '0001-01-01T12:00:00-07:00' || $node->nodeValue == '0001-01-01T00:00:00-07:00') {
					$elements_to_remove[] = $node;
				}
			}
		}
		
		if (isset($elements_to_remove) && is_array($elements_to_remove)) {
			foreach ($elements_to_remove as $node_to_remove) {
				$node_to_remove->parentNode->removeChild($node_to_remove);
			}
		}
		
		$cleaned_sxe = simplexml_import_dom($dxml);
		
		if (isset($elements_to_remove) && (count($elements_to_remove) > 0)) {
			// recurse to make sure all empty elements are removed
			return cleanEmptyXML($cleaned_sxe);
		} else {
			return $cleaned_sxe;
		}
	}
	
	function trisanoAddCmr($xml) {
		global $props;
		try {
			$soap_client = @new SoapClient($props['sqla_wsdl_url']);
			//$soap_client = @new SoapClient(WSDL_PATH2);
		} catch (Exception $e) {
			return (object) array('id' => false, 'info' => null);
		} catch (SoapFault $f) {
			return (object) array('id' => false, 'info' => null);
		}
		
		if ($soap_client) {
			try {
				$soap_result = $soap_client->addCmr(array("healthMessage" => $xml));
			} catch (Exception $e) {
				return (object) array('id' => false, 'info' => null);
			} catch (SoapFault $f) {
				return (object) array('id' => false, 'info' => null);
			}
			$addcmr_result = simplexml_load_string($soap_result->return);
			
			if (getTrisanoReturnStatus($addcmr_result->status_message)->status) {
				// added!
				return (object) array('id' => trim(reset($addcmr_result->xpath("trisano_health/events/id"))), 'info' => null);
			} else {
				// did not add successfully
				return (object) array('id' => false, 'info' => getTrisanoReturnStatus($addcmr_result->status_message)->errors);
			}
		}
	}
	
	function assignJurisdictionByZip($zip_code = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($zip_code)) {
			return null;
		}
		
		if (empty($zip_code) || (strlen(trim($zip_code)) == 0)) {
			return $props['out_of_state_jurisdiction_id'];  // out-of-state
		}
		
		$qry = 'SELECT d.system_external_id AS system_external_id FROM '.$my_db_schema.'system_districts d INNER JOIN '.$my_db_schema.'system_zip_codes z ON ((z.zipcode ILIKE \''.pg_escape_string(trim($zip_code)).'\') AND (z.system_district_id = d.id));';
		$rs = @pg_query($host_pa, $qry);
		if (($rs !== false) && (pg_num_rows($rs) > 0)) {
			return @pg_fetch_result($rs, 0, 'system_external_id');
		} else {
			return $props['out_of_state_jurisdiction_id'];  // out-of-state
		}
	}
	
	function alreadyAdded($id1) {
		global $host_pa, $my_db_schema, $clean;
		
		if (is_null($id1)) {
			return false;
		}
		
		$qry = 'SELECT count(id) AS inserts FROM '.$my_db_schema.'csv_audits_detail WHERE id1 = \''.pg_escape_string(trim($id1)).'\' AND csv_type = \''.pg_escape_string(getNonELRDataType(trim($clean['import_csv_type']), 2)).'\' AND id2 IS NOT NULL AND status = 1;';
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			$insert_count = @pg_fetch_result($rs, 0, 'inserts');
			if (intval($insert_count) > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	function auditCSVDetail($id1 = null, $id2 = null, $status = 0, $status_description = null) {
		global $host_pa, $my_db_schema, $audit_event_id, $clean;
		
		if (is_null($id1) || is_null($audit_event_id)) {
			return false;
		}
		
		$qry = 'INSERT INTO '.$my_db_schema.'csv_audits_detail (import_id, csv_type, id1, id2, status, status_description) VALUES (
			\''.pg_escape_string($audit_event_id).'\', 
			\''.pg_escape_string(getNonELRDataType(trim($clean['import_csv_type']), 2)).'\', 
			\''.pg_escape_string(trim($id1)).'\', 
			'.((!is_null($id2) && (strlen(trim($id2)) > 0)) ? '\''.pg_escape_string(trim($id2)).'\'' : 'NULL').', 
			'.intval($status).', 
			'.((!is_null($status_description) && (strlen(trim($status_description)) > 0)) ? '\''.pg_escape_string(trim($status_description)).'\'' : 'NULL').');';
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			return true;
		} else {
			return false;
		}
	}
	
	function auditCSVAggregate() {
		global $host_pa, $my_db_schema, $audit_event_id, $clean, $stats;
		
		$qry = 'INSERT INTO '.$my_db_schema.'csv_audits_aggregate (import_id, csv_type, rows_found, rows_success, rows_skipped, rows_error) VALUES (
			\''.pg_escape_string($audit_event_id).'\', 
			\''.pg_escape_string(getNonELRDataType(trim($clean['import_csv_type']), 2)).'\', 
			'.intval($stats['found']).', 
			'.intval($stats['success']).', 
			'.intval($stats['invalid']).', 
			'.intval($stats['error']).');';
		
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			return true;
		} else {
			return false;
		}
	}
	
?>