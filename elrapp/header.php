<?php

	$main_url = MAIN_URL.'/';
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>EMSA, Bureau of Epidemiology - Utah Department of Health</title>
	<link type="image/gif" href="images/favicon.gif" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/ui-lightness/jquery-ui.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Francois+One" />
	<!--[if !IE]> -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" />
	<!-- <![endif]-->
	<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400" />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:600" />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:700" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo $main_url;?>css/emsa_201401031300.css" media="screen" />
	<?php if (($selected_page == 1) || (($selected_page == 6) && ($submenu == 7))) { ?><link rel="stylesheet" type="text/css" href="<?php echo $main_url;?>css/reporting_common.css" /><?php } ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $main_url;?>css/screen.responsinav.css" media="screen" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="<?php echo $main_url;?>css/screen.responsinav.ie.css" media="screen" /><![endif]-->
	<!--[if lt IE 9]><script type="text/javascript" src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script><![endif]-->
	<!--[if !IE]> -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<!-- <![endif]-->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo $main_url;?>js/multiselect_jr_nolive.js"></script>
	<script type="text/javascript" src="<?php echo $main_url;?>js/jquery.hoverIntent.js"></script>
	<script type="text/javascript" src="<?php echo $main_url;?>js/jquery.responsinav.js"></script>
	<script type="text/javascript" src="<?php echo $main_url;?>js/elr.nav.js"></script>
</head>

<body class="common">
	<div id="container">
		<header>
			<span id="logo">EMSA</span>
			<form style="margin-right: 5px; float: right;" name="change_elr_view" id="change_elr_view" method="POST">
			<select id="override_role" name="override_role" class="ui-corner-all" title="Change active role">
				<option value="-1" selected>All assigned roles</option>
				<?php
					$header_roles = elrRoleNames();
					foreach ($header_roles as $header_role_id => $header_role_name) {
						echo '<option value="'.intval($header_role_id).'"'.((isset($_SESSION['override_user_role']) && (intval($_SESSION['override_user_role']) == $header_role_id)) ? ' selected' : '').'>'.trim($header_role_name).'</option>'.PHP_EOL;
					}
				?>
			</select>
			</form>
			<nav>
				<ul>
					<li><a href="<?php echo $main_url;?>" class="current">Home</a>
						<ul>
							<li><a href="<?php echo $main_url;?>"<?php echo (($selected_page == 1) ? " class=\"current\"" : ""); ?>>Dashboard</a></li>
							<?php if (checkPermission(URIGHTS_ENTRY)) { ?><li><a href="<?php echo $main_url;?>?selected_page=3&type=17"<?php echo (($selected_page == 3) ? " class=\"current\"" : ""); ?>>Entry</a></li><?php } ?>
							<?php if ((1 === 0) && checkPermission(URIGHTS_PENDING)) { ?><li><a href="<?php echo $main_url;?>?selected_page=2&type=12"<?php echo (($selected_page == 2) ? " class=\"current\"" : ""); ?>>Pending</a></li><?php } ?>
							<?php if (checkPermission(URIGHTS_ASSIGNED)) { ?><li><a href="<?php echo $main_url;?>?selected_page=4&type=14"<?php echo (($selected_page == 4) ? " class=\"current\"" : ""); ?>>Assigned</a></li><?php } ?>
							<?php if (checkPermission(URIGHTS_GRAY)) { ?><li><a href="<?php echo $main_url;?>?selected_page=5&type=2"<?php echo (($selected_page == 5) ? " class=\"current\"" : ""); ?>>Gray</a></li><?php } ?>
							<?php if (checkPermission(URIGHTS_ADMIN)) { ?><li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6) ? " class=\"current\"" : ""); ?>>Admin</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"current\"" : ""); ?>>Exception</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"current\"" : ""); ?>>E-Tasks</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"current\"" : ""); ?>>QA</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
										<ul>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
										</ul>
									</li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
										<ul>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
										</ul>
									</li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
										<ul>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
											<?php if ($server_environment == ELR_ENV_DEV) { ?>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
											<?php } else { ?>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
											<?php } ?>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
										</ul>
									</li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
										<ul>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
											<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
											
										</ul>
									</li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
								</ul>
							</li><?php } ?>
							<?php if (checkPermission(URIGHTS_NONELR)) { ?><li><a href="<?php echo $main_url;?>?selected_page=8&submenu=1"<?php echo (($selected_page == 8) ? " class=\"current\"" : ""); ?>>Non-ELR Data</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=1"<?php echo (($selected_page == 8 && $submenu == 1) ? " class=\"current\"" : ""); ?>>Non-ELR Data Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=3"<?php echo (($selected_page == 8 && $submenu == 3) ? " class=\"current\"" : ""); ?>>XML Mapping</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=2"<?php echo (($selected_page == 8 && $submenu == 2) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
								</ul>
							</li><?php } ?>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					
			<?php if ($selected_page == 6 && $submenu == 3) { ?>
			
					<?php if (checkPermission(URIGHTS_ADMIN)) { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6) ? " class=\"current\"" : ""); ?>>Admin</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"current\"" : ""); ?>>Exception</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"current\"" : ""); ?>>E-Tasks</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"current\"" : ""); ?>>QA</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
									<?php if ($server_environment == ELR_ENV_DEV) { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
									<?php } else { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
									<?php } ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
							
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"activetab\"" : ""); ?>>Master Dictionary</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"activetab\"" : ""); ?>>Child Dictionary</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"activetab\"" : ""); ?>>Master LOINC</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"activetab\"" : ""); ?>>Master SNOMED</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"activetab\"" : ""); ?>>Master Condition</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"activetab\"" : ""); ?>>Child LOINC</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"activetab\"" : ""); ?>>Child SNOMED</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"activetab\"" : ""); ?>>Import</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"activetab\"" : ""); ?>>Export</a></li>
					<?php } ?>
			
			<?php } elseif ($selected_page == 6 && $submenu == 4) { ?>
			
					<?php if (checkPermission(URIGHTS_ADMIN)) { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6) ? " class=\"current\"" : ""); ?>>Admin</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"current\"" : ""); ?>>Exception</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"current\"" : ""); ?>>E-Tasks</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"current\"" : ""); ?>>QA</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
									<?php if ($server_environment == ELR_ENV_DEV) { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
									<?php } else { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
									<?php } ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"activetab\"" : ""); ?>>Labs</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"activetab\"" : ""); ?>>HL7</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"activetab\"" : ""); ?>>Master XML</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"activetab\"" : ""); ?>>Application XML</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"activetab\"" : ""); ?>>XML Rules</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"activetab\"" : ""); ?>>Vocab Categories</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"activetab\"" : ""); ?>>Import</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"activetab\"" : ""); ?>>Export</a></li>
					<?php } ?>
			
			<?php } elseif ($selected_page == 6 && $submenu == 5) { ?>
			
					<?php if (checkPermission(URIGHTS_ADMIN)) { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6) ? " class=\"current\"" : ""); ?>>Admin</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"current\"" : ""); ?>>Exception</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"current\"" : ""); ?>>E-Tasks</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"current\"" : ""); ?>>QA</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
									<?php if ($server_environment == ELR_ENV_DEV) { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
									<?php } else { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
									<?php } ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
							<?php if ($server_environment == ELR_ENV_DEV) { ?>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
							<?php } else { ?>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
							<?php } ?>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"activetab\"" : ""); ?>>User Roles</a></li>
					<?php if ($server_environment == ELR_ENV_DEV) { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"activetab\"" : ""); ?>>Manual HL7</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"activetab\"" : ""); ?>>Generate HL7</a></li>
					<?php } else { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"activetab\"" : ""); ?>>Add HL7</a></li>
					<?php } ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"activetab\"" : ""); ?>>Zip Codes</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"activetab\"" : ""); ?>>Audit Log</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"activetab\"" : ""); ?>>Vocab Audit Log</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"activetab\"" : ""); ?>>XML Formatter</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"activetab\"" : ""); ?>>HL7 Review Generator</a></li>
					<?php } ?>
			
			<?php } elseif ($selected_page == 6 && $submenu == 9) { ?>
			
					<?php if (checkPermission(URIGHTS_ADMIN)) { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6) ? " class=\"current\"" : ""); ?>>Admin</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"current\"" : ""); ?>>Exception</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"current\"" : ""); ?>>E-Tasks</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"current\"" : ""); ?>>QA</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
									<?php if ($server_environment == ELR_ENV_DEV) { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
									<?php } else { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
									<?php } ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"activetab\"" : ""); ?>>Basic Settings</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"activetab\"" : ""); ?>>Virtual Jurisdictions</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"activetab\"" : ""); ?>>Notification Types</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"activetab\"" : ""); ?>>Rule Parameters</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"activetab\"" : ""); ?>>Rules</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"activetab\"" : ""); ?>>Pending Notifications</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"activetab\"" : ""); ?>>Log</a></li>
					<?php } ?>
			
			<?php } elseif ($selected_page == 6) { ?>
			
					<?php if (checkPermission(URIGHTS_ADMIN)) { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6) ? " class=\"current\"" : ""); ?>>Admin</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"current\"" : ""); ?>>Exception</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"current\"" : ""); ?>>E-Tasks</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"current\"" : ""); ?>>QA</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
									<?php if ($server_environment == ELR_ENV_DEV) { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
									<?php } else { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
									<?php } ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"activetab\"" : ""); ?>>Exception</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"activetab\"" : ""); ?>>E-Tasks</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"activetab\"" : ""); ?>>QA</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"activetab\"" : ""); ?>>Vocabulary</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
						</ul>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"activetab\"" : ""); ?>>Structure Manager</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
						</ul>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"activetab\"" : ""); ?>>Tools and Rules</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
							<?php if ($server_environment == ELR_ENV_DEV) { ?>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
							<?php } else { ?>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
							<?php } ?>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
						</ul>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
						</ul>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
					<?php } ?>
			
			<?php } elseif ($selected_page == 1 || $selected_page == 2 || $selected_page == 3 || $selected_page == 4 || $selected_page == 5 || $selected_page == 7) { ?>
			
					<li><a href="<?php echo $main_url;?>"<?php echo (($selected_page == 1) ? " class=\"activetab\"" : ""); ?>>Dashboard</a></li>
					<?php if (checkPermission(URIGHTS_ENTRY)) { ?><li><a href="<?php echo $main_url;?>?selected_page=3&type=17"<?php echo (($selected_page == 3) ? " class=\"activetab\"" : ""); ?>>Entry</a></li><?php } ?>
					<?php if ((1 === 0) && checkPermission(URIGHTS_PENDING)) { ?><li><a href="<?php echo $main_url;?>?selected_page=2&type=12"<?php echo (($selected_page == 2) ? " class=\"activetab\"" : ""); ?>>Pending</a></li><?php } ?>
					<?php if (checkPermission(URIGHTS_ASSIGNED)) { ?><li><a href="<?php echo $main_url;?>?selected_page=4&type=14"<?php echo (($selected_page == 4) ? " class=\"activetab\"" : ""); ?>>Assigned</a></li><?php } ?>
					<?php if (checkPermission(URIGHTS_GRAY)) { ?><li><a href="<?php echo $main_url;?>?selected_page=5&type=2"<?php echo (($selected_page == 5) ? " class=\"activetab\"" : ""); ?>>Gray</a></li><?php } ?>
					<?php if (checkPermission(URIGHTS_ADMIN)) { ?><li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6) ? " class=\"activetab\"" : ""); ?>>Admin</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=1&type=3"<?php echo (($selected_page == 6 && $submenu <2) ? " class=\"current\"" : ""); ?>>Exception</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=2"<?php echo (($selected_page == 6 && $submenu == 2) ? " class=\"current\"" : ""); ?>>E-Tasks</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=8&type=19"<?php echo (($selected_page == 6 && $submenu == 8) ? " class=\"current\"" : ""); ?>>QA</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3"<?php echo (($selected_page == 6 && $submenu == 3) ? " class=\"current\"" : ""); ?>>Vocabulary</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=8"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 8) ? " class=\"current\"" : ""); ?>>Master Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=9"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 9) ? " class=\"current\"" : ""); ?>>Child Dictionary</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=1"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab < 2) ? " class=\"current\"" : ""); ?>>Master LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=3"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 3) ? " class=\"current\"" : ""); ?>>Master SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=2"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 2) ? " class=\"current\"" : ""); ?>>Master Condition</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=5"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 5) ? " class=\"current\"" : ""); ?>>Child LOINC</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=4"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 4) ? " class=\"current\"" : ""); ?>>Child SNOMED</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=6"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 6) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=3&vocab=7"<?php echo (($selected_page == 6 && $submenu == 3 && $vocab == 7) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4"<?php echo (($selected_page == 6 && $submenu == 4) ? " class=\"current\"" : ""); ?>>Structure Manager</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=1"<?php echo (($selected_page == 6 && $submenu == 4 && $cat < 2) ? " class=\"current\"" : ""); ?>>Labs</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=5"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 5) ? " class=\"current\"" : ""); ?>>HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=2"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 2) ? " class=\"current\"" : ""); ?>>Master XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=4"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 4) ? " class=\"current\"" : ""); ?>>Application XML</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=3"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 3) ? " class=\"current\"" : ""); ?>>XML Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=6"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 6) ? " class=\"current\"" : ""); ?>>Vocab Categories</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=7"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 7) ? " class=\"current\"" : ""); ?>>Import</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=4&cat=8"<?php echo (($selected_page == 6 && $submenu == 4 && $cat == 8) ? " class=\"current\"" : ""); ?>>Export</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5"<?php echo (($selected_page == 6 && $submenu == 5) ? " class=\"current\"" : ""); ?>>Tools and Rules</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=1"<?php echo (($selected_page == 6 && $submenu == 5 && $cat < 2) ? " class=\"current\"" : ""); ?>>User Roles</a></li>
									<?php if ($server_environment == ELR_ENV_DEV) { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Manual HL7</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=8"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 8) ? " class=\"current\"" : ""); ?>>Generate HL7</a></li>
									<?php } else { ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=2"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 2) ? " class=\"current\"" : ""); ?>>Add HL7</a></li>
									<?php } ?>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=4"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 4) ? " class=\"current\"" : ""); ?>>Zip Codes</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=5"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 5) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=9"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 9) ? " class=\"current\"" : ""); ?>>Vocab Audit Log</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=6"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 6) ? " class=\"current\"" : ""); ?>>XML Formatter</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=5&cat=7"<?php echo (($selected_page == 6 && $submenu == 5 && $cat == 7) ? " class=\"current\"" : ""); ?>>HL7 Review Generator</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9"<?php echo (($selected_page == 6 && $submenu == 9) ? " class=\"current\"" : ""); ?>>E-mail Notification</a>
								<ul>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=1"<?php echo (($selected_page == 6 && $submenu == 9 && $cat < 2) ? " class=\"current\"" : ""); ?>>Basic Settings</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=2"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 2) ? " class=\"current\"" : ""); ?>>Virtual Jurisdictions</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=3"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 3) ? " class=\"current\"" : ""); ?>>Notification Types</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=7"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 7) ? " class=\"current\"" : ""); ?>>Rule Parameters</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=4"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 4) ? " class=\"current\"" : ""); ?>>Rules</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=5"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 5) ? " class=\"current\"" : ""); ?>>Pending Notifications</a></li>
									<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=9&cat=6"<?php echo (($selected_page == 6 && $submenu == 9 && $cat == 6) ? " class=\"current\"" : ""); ?>>Log</a></li>
								</ul>
							</li>
							<li><a href="<?php echo $main_url;?>?selected_page=6&submenu=7"<?php echo (($selected_page == 6 && $submenu == 7) ? " class=\"current\"" : ""); ?>>Reporting</a></li>
						</ul>
					</li><?php } ?>
					<?php if (checkPermission(URIGHTS_NONELR)) { ?><li><a href="<?php echo $main_url;?>?selected_page=8&submenu=1"<?php echo (($selected_page == 8) ? " class=\"activetab\"" : ""); ?>>Non-ELR Data</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=1"<?php echo (($selected_page == 8 && $submenu == 1) ? " class=\"current\"" : ""); ?>>Non-ELR Data Import</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=3"<?php echo (($selected_page == 8 && $submenu == 3) ? " class=\"current\"" : ""); ?>>XML Mapping</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=2"<?php echo (($selected_page == 8 && $submenu == 2) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
						</ul>
					</li><?php } ?>
					
			<?php } elseif ($selected_page == 8) { ?>
					<?php if (checkPermission(URIGHTS_NONELR)) { ?>
					<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=1"<?php echo (($selected_page == 8) ? " class=\"current\"" : ""); ?>>Non-ELR Data</a>
						<ul>
							<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=1"<?php echo (($selected_page == 8 && $submenu == 1) ? " class=\"current\"" : ""); ?>>Non-ELR Data Import</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=3"<?php echo (($selected_page == 8 && $submenu == 3) ? " class=\"current\"" : ""); ?>>XML Mapping</a></li>
							<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=2"<?php echo (($selected_page == 8 && $submenu == 2) ? " class=\"current\"" : ""); ?>>Audit Log</a></li>
						</ul>
						<span class="menu_divider">&raquo;</span>
					</li>
					<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=1"<?php echo (($selected_page == 8 && $submenu == 1) ? " class=\"activetab\"" : ""); ?>>Non-ELR Data Import</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=3"<?php echo (($selected_page == 8 && $submenu == 3) ? " class=\"activetab\"" : ""); ?>>XML Mapping</a></li>
					<li><a href="<?php echo $main_url;?>?selected_page=8&submenu=2"<?php echo (($selected_page == 8 && $submenu == 2) ? " class=\"activetab\"" : ""); ?>>Audit Log</a></li>
					<?php } ?>
					
			<?php } ?>
				</ul>
			</nav>
		</header>
		
		<div id="content-wrapper-1">
