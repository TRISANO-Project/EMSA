<?php

	unset($this_action);
	if (isset($_POST['action'])) {
		switch (strtolower(trim($_POST['action']))) {
			case "add":
			case "edit":
				$this_action = strtolower(trim($_POST['action']));
				break;
		}
	}
	
	if ($this_action == "add") {
		$name = trim($_POST['role_name']);
		
		if (strlen($name) >= 3) {
			if (roleNameUnique($name, $my_db_schema, $host_pa)) {
				$role_id = intval(trim($_POST['role_id']));
				$loincs = ((is_array($_POST['loincs'])) ? $_POST['loincs'] : array());
				$childsnomeds = ((is_array($_POST['childsnomeds'])) ? $_POST['childsnomeds'] : array());
				$menus = ((is_array($_POST['system_menu_id'])) ? $_POST['system_menu_id'] : array());
				
				$sql = "INSERT INTO ".$my_db_schema."system_roles(name, master_role_id) VALUES ('".pg_escape_string($name)."', ".$role_id.") ";
				$result = @pg_query($host_pa, $sql);
				
				if ($result) {
					//do
					$insert_query = get_db_result_set($host_pa, "SELECT lastval();");
					$insert_row = get_db_row_array($insert_query);
					
					$role_id = $insert_row['lastval'];
					
					if ($role_id > 0) {
						if (count($loincs) > 0) {
							foreach ($loincs as $lab_id => $diseases) {
								foreach ($diseases as $disease_key => $master_loinc) {
									$sql = 'INSERT INTO '.$my_db_schema.'system_role_loincs_by_lab (master_loinc_code, system_role_id, lab_id) 
										VALUES (\''.@pg_escape_string($master_loinc).'\', '.intval($role_id).', '.intval($lab_id).')';
									$result = get_db_result_set($host_pa, $sql);
								}
							}
						}
						
						if (count($childsnomeds) > 0) {
							foreach ($childsnomeds as $childsnomed_lab_id => $childsnomed) {
								foreach ($childsnomed as $childsnomed_key => $childsnomed_code) {
									$childsnomed_sql = 'INSERT INTO '.$my_db_schema.'system_role_child_snomeds_by_lab (child_snomed, system_role_id, lab_id) 
										VALUES (\''.@pg_escape_string($childsnomed_code).'\', '.intval($role_id).', '.intval($childsnomed_lab_id).')';
									$childsnomed_result = get_db_result_set($host_pa, $childsnomed_sql);
								}
							}
						}
						
						if (count($menus) > 0) {
							foreach ($menus as $menu) {
								$sql = "INSERT INTO ".$my_db_schema."system_role_menus (system_menu_id, system_role_id) VALUES('".pg_escape_string($menu)."',".intval($role_id).")";
								$result = get_db_result_set($host_pa, $sql);
							}
						}
					}
					highlight("New role '".htmlentities($name, ENT_QUOTES, "UTF-8")."' successfully added!", "ui-icon-elrsuccess");
				} else {
					suicide("Unable to add new role.", 1);
				}
			} else {
				suicide("Unable to add new role:  Role name must be unique.");
			} 
		} else {
			suicide("No role name specified.  Please enter a role name & try again.");
		}
	}
	
	if ($this_action == "edit") {
		$name = trim($_POST['role_name']);
		
		if (strlen($name) >= 3) {
			$system_role_id = intval(trim($_POST['system_role_id']));
			if (roleNameUnique($name, $my_db_schema, $host_pa, $system_role_id)) {
				$role_id = intval(trim($_POST['role_id']));
				$loincs = ((is_array($_POST['loincs'])) ? $_POST['loincs'] : array());
				$childsnomeds = ((is_array($_POST['childsnomeds'])) ? $_POST['childsnomeds'] : array());
				$menus = ((is_array($_POST['system_menu_id'])) ? $_POST['system_menu_id'] : array());
				
				$sql = "UPDATE ".$my_db_schema."system_roles SET name='".pg_escape_string($name)."', master_role_id=".intval($role_id)." WHERE id=".intval($system_role_id);
				$result = @pg_query($host_pa, $sql);
				
				if ($result) {
					//do
					if ($role_id > 0) {
						if (count($loincs) > 0) {
							$sql = 'DELETE FROM '.$my_db_schema.'system_role_loincs_by_lab WHERE system_role_id = '.intval($system_role_id).';';
							get_db_result_set($host_pa, $sql);
							
							foreach ($loincs as $lab_id => $diseases) {
								foreach ($diseases as $disease_key => $master_loinc) {
									$sql = 'INSERT INTO '.$my_db_schema.'system_role_loincs_by_lab (master_loinc_code, system_role_id, lab_id) 
										VALUES (\''.@pg_escape_string($master_loinc).'\', '.intval($system_role_id).', '.intval($lab_id).')';
									$result = get_db_result_set($host_pa, $sql);
								}
							}
						}
						
						if (count($childsnomeds) > 0) {
							$childsnomed_sql = 'DELETE FROM '.$my_db_schema.'system_role_child_snomeds_by_lab WHERE system_role_id = '.intval($system_role_id).';';
							get_db_result_set($host_pa, $childsnomed_sql);
							
							foreach ($childsnomeds as $childsnomed_lab_id => $childsnomed) {
								foreach ($childsnomed as $childsnomed_key => $childsnomed_code) {
									$childsnomed_sql = 'INSERT INTO '.$my_db_schema.'system_role_child_snomeds_by_lab (child_snomed, system_role_id, lab_id) 
										VALUES (\''.@pg_escape_string($childsnomed_code).'\', '.intval($system_role_id).', '.intval($childsnomed_lab_id).')';
									$childsnomed_result = get_db_result_set($host_pa, $childsnomed_sql);
								}
							}
						}
						
						if (count($menus) > 0) {
							$sql = "DELETE FROM ".$my_db_schema."system_role_menus WHERE system_role_id=".$system_role_id;
							get_db_result_set($host_pa, $sql);
							
							foreach ($menus as $menu) {
								$sql = "INSERT INTO ".$my_db_schema."system_role_menus (system_menu_id, system_role_id) VALUES('".pg_escape_string($menu)."', ".intval($system_role_id).")";
								$result = get_db_result_set($host_pa, $sql);
							}
						}
					}
					highlight("Changes to role '".htmlentities($name, ENT_QUOTES, "UTF-8")."' saved!", "ui-icon-elrsuccess");
				} else {
					suicide("Unable to save changes to role.", 1);
				}
			} else {
				suicide("Unable to save changes to role:  Role name must be unique.");
			}
		} else {
			suicide("No role name specified.  Please enter a role name & try again.");
		}
	}
	
	function roleNameUnique($name, $my_db_schema, $host_pa, $role_id = 0) {
		$role = array();
		
		if ($role_id > 0) {
			$sql = "SELECT name FROM ".$my_db_schema."system_roles WHERE id=".intval($role_id);
			$role = get_db_row_array(get_db_result_set($host_pa, $sql));
		}
		
		$sql = "SELECT name FROM ".$my_db_schema."system_roles WHERE name='".pg_escape_string($name)."'";
		$result = get_db_row_array(get_db_result_set($host_pa, $sql));
		
		if ((count($role) > 0) && is_array($result)) {
			if ($role['name'] == $result['name']) {
				return true;
			} elseif (count($result) > 0) {
				return false;
			} else {
				return true;
			}
		} elseif (is_array($result)) {
			if (count($result) > 0) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	
?>