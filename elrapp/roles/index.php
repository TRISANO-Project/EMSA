<script>
	$(function() {
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			var addAction = "<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=1&edit_flag=1";
			window.location.href = addAction;
		});
		
		$(".edit_role").button({
            icons: {
                primary: "ui-icon-elrpencil"
            }
        }).next().button({
            icons: {
                primary: "ui-icon-elrclose"
            }
        }).parent().buttonset();
		
		$(".edit_role").click(function() {
			var editAction = "<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=1&edit_flag=1&role_id="+$(this).val();
			window.location.href = editAction;
		});
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false
		});
		
		$(".delete_role").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=1&delete_flag=1&role_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$("#back_button").button({
            icons: {
                primary: "ui-icon-elrcancel"
            }
        }).click(function() {
			var backAction = "<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=1";
			window.location.href = backAction;
		});
		
		$("#submit_button").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elredituser"></span>User Role Management</h1>

<?php

	include_once WEBROOT_URL.'/includes/roles_functions.php';

	if (isset($_GET['delete_flag']) && (intval($_GET['delete_flag']) == 1)) {
		// handle delete function
		include_once 'delete.php';
	}
	
	if (isset($_GET['edit_flag']) && (intval($_GET['edit_flag']) == 1)) {
		// handle add/edit functions
		include_once 'add.php';
	} else {
	
?>

<div class="vocab_search ui-tabs ui-widget">
	<button id="addnew_button" title="Add a new user role">Add New Role</button>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th style="width: 40%;">Role</th>
				<th>Menus</th>
				<th>Master LOINCs</th>
				<th>Child SNOMEDs</th>
			</tr>
		</thead>
		<tbody>

<?php

	$o_sql = 'SELECT (select count(id) FROM '.$my_db_schema.'system_menus) AS overall_menu_count;';
	$o_rs = @pg_query($host_pa, $o_sql);
	if ($o_rs !== false) {
		$o_menu = @pg_fetch_result($o_rs, 0, 'overall_menu_count');
		@pg_free_result($o_rs);
	} else {
		$o_menu = 'N/A';
	}

	$sql = sprintf("SELECT sr.id AS id, sr.name AS name, sr.master_role_id AS master_role_id, COUNT(DISTINCT rm.id) AS mcount 
						FROM %ssystem_roles sr 
						LEFT JOIN %ssystem_role_menus rm ON (rm.system_role_id = sr.id)
						GROUP BY sr.id, sr.name, sr.master_role_id
						ORDER BY master_role_id;",
				$my_db_schema, $my_db_schema, $my_db_schema);
	$roles = @get_db_result_set($host_pa,$sql);
	
	if (!$roles) {
		suicide("Unable to retrieve list of User Roles.", 1);
	}
	
	if(count($roles)>0){
		while($role=get_db_row_array($roles)){ ?>
			<tr>
				<td>
					<button class="edit_role" title="Edit this user role" value="<?php echo $role['id']; ?>">Edit Role</button>
					<button class="delete_role" title="Remove this user role" value="<?php echo $role['id']; ?>">Delete Role</button>
				</td>
				<td><?php echo $role['name']; ?> <em>(Trisano Role: <?php echo getTrisanoRole($role['master_role_id']); ?>)</em></td>
				<td><?php echo $role['mcount'].' of '.$o_menu; ?></td>
				<td>
			<?php
				// for this role, get all assigned labs & master loincs per lab
				$role_lab_sql = 'SELECT DISTINCT rd.lab_id AS lab_id, sl.ui_name AS lab_name, COUNT(DISTINCT(rd.master_loinc_code)) AS assigned_loinc_count, (
						SELECT COUNT(DISTINCT(cl.master_loinc)) FROM '.$my_db_schema.'vocab_child_loinc cl WHERE cl.lab_id = rd.lab_id
					) AS lab_loinc_count
					FROM '.$my_db_schema.'system_role_loincs_by_lab rd
					INNER JOIN '.$my_db_schema.'structure_labs sl ON (rd.lab_id = sl.id)
					WHERE rd.system_role_id = '.intval($role['id']).' 
					GROUP BY rd.lab_id, sl.ui_name ORDER BY sl.ui_name;';
				$role_lab_rs = @pg_query($host_pa, $role_lab_sql);
				if ($role_lab_rs === false) {
					echo 'Unable to retrieve assigned Labs & Master LOINCs';
				} else {
					while ($role_lab_row = @pg_fetch_object($role_lab_rs)) {
						echo '<strong>'.htmlentities($role_lab_row->lab_name, ENT_QUOTES, 'UTF-8').'</strong> ('.$role_lab_row->assigned_loinc_count.' of '.$role_lab_row->lab_loinc_count.')<br>';
					}
					@pg_free_result($role_lab_rs);
				}
			?>
				</td>
				<td>
			<?php
				// for this role, get all assigned labs & master loincs per lab
				$role_lab_sql = 'SELECT DISTINCT cs.lab_id AS lab_id, sl.ui_name AS lab_name, COUNT(DISTINCT(cs.child_snomed)) AS assigned_snomed_count, (
						SELECT COUNT(DISTINCT(co.child_code)) FROM '.$my_db_schema.'vocab_child_organism co WHERE co.lab_id = cs.lab_id
					) AS lab_snomed_count
					FROM '.$my_db_schema.'system_role_child_snomeds_by_lab cs
					INNER JOIN '.$my_db_schema.'structure_labs sl ON (cs.lab_id = sl.id)
					WHERE cs.system_role_id = '.intval($role['id']).' 
					GROUP BY cs.lab_id, sl.ui_name ORDER BY sl.ui_name;';
				$role_lab_rs = @pg_query($host_pa, $role_lab_sql);
				if ($role_lab_rs === false) {
					echo 'Unable to retrieve assigned Child SNOMEDs';
				} else {
					while ($role_lab_row = @pg_fetch_object($role_lab_rs)) {
						echo '<strong>'.htmlentities($role_lab_row->lab_name, ENT_QUOTES, 'UTF-8').'</strong> ('.$role_lab_row->assigned_snomed_count.' of '.$role_lab_row->lab_snomed_count.')<br>';
					}
					@pg_free_result($role_lab_rs);
				}
			?>
				</td>
			</tr>
<?php

		}
	}
	
?>

		</tbody>
	</table>
	<br><br>
	
	<h3 style="color: crimson;">Unassigned Active Master LOINC Codes</h3>
	<div style="font-family: 'Open Sans', Arial, Helvetica; font-weight: 600 !important; color: dimgray;">The following Master LOINC codes are associated with Child LOINC codes, but have not been assigned to any user roles.  No incoming labs for these Master LOINCs will be displayed in EMSA queues for any users until they are assigned to at least one role.</div>
	<table id="labResults">
		<thead>
			<tr>
				<th>Child Lab</th>
				<th>Master LOINC Code</th>
				<th style="width: 60%;">LOINC Concept Name</th>
			</tr>
		</thead>
		<tbody>
<?php
	$orphan_sql = 'SELECT distinct l.ui_name AS lab_name, ml.loinc AS loinc, ml.concept_name AS concept_name 
		FROM '.$my_db_schema.'vocab_master_loinc ml 
		INNER JOIN '.$my_db_schema.'vocab_child_loinc cl ON (cl.master_loinc = ml.l_id) 
		INNER JOIN '.$my_db_schema.'structure_labs l ON (cl.lab_id = l.id) 
		LEFT JOIN '.$my_db_schema.'system_role_loincs_by_lab rd ON ((ml.loinc = rd.master_loinc_code) AND (rd.lab_id = cl.lab_id)) 
		WHERE rd.master_loinc_code IS NULL
		ORDER BY ml.loinc;';
	$orphan_rs = @pg_query($host_pa, $orphan_sql);
	if ($orphan_rs === false || @pg_num_rows($orphan_rs) === 0) {
		echo '<tr><td colspan="3"><em>All active Master LOINC codes assigned to roles!</em></td></tr>';
	} else {
		while ($orphan_row = @pg_fetch_object($orphan_rs)) {
			echo '<tr>';
			echo '<td><span style="display: inline-block; vertical-align: middle; padding-top: 3px;" class="ui-icon ui-icon-elrerror"></span> '.htmlentities($orphan_row->lab_name, ENT_QUOTES, 'UTF-8').'</td><td>'.htmlentities($orphan_row->loinc, ENT_QUOTES, 'UTF-8').'</td>';
			echo '<td>'.htmlentities($orphan_row->concept_name, ENT_QUOTES, 'UTF-8').'</td>';
			echo '</tr>';
		}
		@pg_free_result($orphan_rs);
	}
?>
		</tbody>
	</table>
	<br><br>
	
	<h3 style="color: crimson;">Unassigned Child SNOMED Codes</h3>
	<div style="font-family: 'Open Sans', Arial, Helvetica; font-weight: 600 !important; color: dimgray;">The following Child SNOMED codes are not assigned to any role.  Any messages received with this Child SNOMED code where the Child LOINC requires SNOMED code validation on nominal results will not be viewable by any users unless this code is assigned to at least one role.</div>
	<table id="labResults">
		<thead>
			<tr>
				<th>Child Lab</th>
				<th>Child SNOMED Code</th>
				<th style="width: 60%;">Organism [Test Result]</th>
			</tr>
		</thead>
		<tbody>
<?php
	$orphan_sql = 'SELECT distinct l.ui_name AS lab_name, co.child_code AS child_code, mv1.concept AS organism, mv2.concept AS test_result 
		FROM '.$my_db_schema.'vocab_child_organism co 
		LEFT JOIN '.$my_db_schema.'structure_labs l ON (co.lab_id = l.id) 
		LEFT JOIN '.$my_db_schema.'vocab_master_organism mo1 ON (co.organism = mo1.o_id) 
		LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv1 ON (mo1.organism = mv1.id) 
		LEFT JOIN '.$my_db_schema.'vocab_master_organism mo2 ON (co.test_result_id = mo2.o_id) 
		LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv2 ON (mo2.organism = mv2.id) 
		LEFT JOIN '.$my_db_schema.'system_role_child_snomeds_by_lab csn ON ((co.child_code = csn.child_snomed) AND (csn.lab_id = co.lab_id)) 
		WHERE csn.child_snomed IS NULL
		ORDER BY l.ui_name, co.child_code;';
	$orphan_rs = @pg_query($host_pa, $orphan_sql);
	if ($orphan_rs === false || @pg_num_rows($orphan_rs) === 0) {
		echo '<tr><td colspan="3"><em>All Child SNOMED codes assigned to roles!</em></td></tr>';
	} else {
		while ($orphan_row = @pg_fetch_object($orphan_rs)) {
			echo '<tr>';
			echo '<td><span style="display: inline-block; vertical-align: middle; padding-top: 3px;" class="ui-icon ui-icon-elrerror"></span> '.htmlentities($orphan_row->lab_name, ENT_QUOTES, 'UTF-8').'</td><td>'.htmlentities($orphan_row->child_code, ENT_QUOTES, 'UTF-8').'</td>';
			echo '<td>'.htmlentities($orphan_row->organism, ENT_QUOTES, 'UTF-8').' ['.htmlentities($orphan_row->test_result, ENT_QUOTES, 'UTF-8').']</td>';
			echo '</tr>';
		}
		@pg_free_result($orphan_rs);
	}
?>
		</tbody>
	</table>

	<br><br>
	
	<h3 style="color: maroon;">Unassigned Inactive Master LOINC Codes</h3>
	<div style="font-family: 'Open Sans', Arial, Helvetica; font-weight: 600 !important; color: dimgray;">The following Master LOINC codes have not been assigned to any user roles, but are not used by any Child LOINC codes.  Any existing messages in EMSA with this Master LOINC code will not be viewable unless it is assigned to at least one role.</div>
	<table id="labResults">
		<thead>
			<tr>
				<th>Master LOINC Code</th>
				<th>LOINC Concept Name</th>
			</tr>
		</thead>
		<tbody>
<?php
	$orphan_sql = 'SELECT distinct l.ui_name, ml.loinc, ml.concept_name 
		FROM '.$my_db_schema.'vocab_master_loinc ml 
		LEFT JOIN '.$my_db_schema.'vocab_child_loinc cl ON (cl.master_loinc = ml.l_id) 
		LEFT JOIN '.$my_db_schema.'structure_labs l ON (cl.lab_id = l.id) 
		LEFT JOIN '.$my_db_schema.'system_role_loincs_by_lab rd ON ((ml.loinc = rd.master_loinc_code) AND (rd.lab_id = cl.lab_id)) 
		WHERE rd.master_loinc_code IS NULL
		AND cl.child_loinc IS NULL
		ORDER BY ml.loinc;';
	$orphan_rs = @pg_query($host_pa, $orphan_sql);
	if ($orphan_rs === false || @pg_num_rows($orphan_rs) === 0) {
		echo '<tr><td colspan="3"><em>All inactive Master LOINC codes assigned to roles!</em></td></tr>';
	} else {
		while ($orphan_row = @pg_fetch_object($orphan_rs)) {
			echo '<tr>';
			echo '<td>'.htmlentities($orphan_row->loinc, ENT_QUOTES, 'UTF-8').'</td>';
			echo '<td>'.htmlentities($orphan_row->concept_name, ENT_QUOTES, 'UTF-8').'</td>';
			echo '</tr>';
		}
		@pg_free_result($orphan_rs);
	}
?>
		</tbody>
	</table>
</div>

<div id="confirm_delete_dialog" title="Delete this User Role?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This User Role will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<?php
	
	}
	
?>