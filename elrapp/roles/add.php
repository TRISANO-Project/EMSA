<?php

	unset($system_role_id);
	include 'lib/add.php';
	
	if (isset($_GET['role_id']) && (intval(trim($_GET['role_id'])) > 0)) {
		$system_role_id = intval(trim($_GET['role_id']));
	}
	
	if (isset($system_role_id)) {
		$sql = "SELECT name, master_role_id FROM ".$my_db_schema."system_roles WHERE id=".$system_role_id;
		$role_name = @pg_fetch_result(@pg_query($host_pa, $sql), 0, "name");
		$trisano_role_id = @pg_fetch_result(@pg_query($host_pa, $sql), 0, "master_role_id");
		$action = "edit";
		$loincs = getRoleDiseases($system_role_id);
		$childsnomeds = getRoleChildSNOMEDs($system_role_id);
	} else {
		$action = "add";
		$loincs = array();
		$childsnomeds = array();
	}

?>
<style>
	fieldset { padding: 10px; font-family: 'Open Sans', Arial, Helvetica, sans-serif !important; box-shadow: 2px 2px 4px darkgray; }
	legend { font-family: 'Francois One', serif; margin-left: 10px; color: firebrick; font-weight: 400; font-size: 1.5em; }
	fieldset label, fieldset strong { font-size: 0.9em; font-weight: 700 !important; }
	fieldset h2 {
		font-family: 'Francois One', serif;
		color: mediumblue;
		font-weight: 400;
		font-size: 1.3em;
		border-bottom: 1px darkgray solid;
		display: block;
		margin: 5px 10px;
		line-height: 1em;
	}
	
	select, input {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		font-weight: 400 !important;
		margin: 0px;
		font-size: 0.9em !important;
	}
	
	input {
		padding: 3px !important;
	}
	
	.tsmsselect {
		width: 45%;
		float: left;
	}
	
	.tsmsselect select {
		width: 100%;
	}
	
	.tsmsoptions {
		width: 10%;
		float: left;
	}
	
	.tsmsoptions p {
		margin: 2px;
		text-align: center;
		font-size: larger;
		cursor: pointer;
		font-size: 1.5em !important;
		line-height: 1.3em !important;
	}
	
	.tsmsoptions p:hover {
		color: red;
		background-color: yellow;
		font-weight: 700;
	}
</style>
<script type="text/javascript">
	$(function() {
		$("select, input").addClass("ui-corner-all");
	});
</script>

<form method="post">
	<input type="hidden" name="action" value="<?php echo $action;?>">
	<input type="hidden" name="system_role_id" value="<?php echo $system_role_id;?>">
	
	<fieldset class="ui-widget ui-widget-content ui-corner-all">
		<legend>Basic Settings</legend>
		<div class="addnew_field">
			<label for="role_name">Role Name:</label><br>
			<input style="width: 25em;" type="text" name="role_name" id="role_name" value="<?php echo $role_name;?>" />
		</div>
		<div class="addnew_field">
			<label for="role_id">Applies to users with TriSano Role...</label><br>
			<?php echo getTrisanoRoles($trisano_role_id, 'role_id');?>
		</div>
	    <div style="float:right;">
            <button id="submit_button" name="submit_button" type="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-elrsave"></span><span class="ui-button-text">Save User Role</span></button>&nbsp; <button id="back_button" name="back_button" type="reset" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-elrcancel"></span><span class="ui-button-text">Cancel</span></button>	
        </div>
    </fieldset><br>
	
	<fieldset class="ui-widget ui-widget-content ui-corner-all">
		<legend>Master LOINC Access</legend>
		
		<?php
			// iterate for each configured lab
			$lab_iterator_sql = 'SELECT id, ui_name FROM elr.structure_labs 
				WHERE id IN (
					SELECT DISTINCT lab_id FROM elr.vocab_child_loinc
				)
				ORDER BY ui_name;';
			$lab_iterator_rs = @pg_query($host_pa, $lab_iterator_sql);
			while ($lab_iterator_rs !== false && $lab_iterator_row = @pg_fetch_object($lab_iterator_rs)) {
				echo '<h2>'.htmlentities($lab_iterator_row->ui_name, ENT_QUOTES, 'UTF-8').' Master LOINCs</h2>';
				
				echo '<div style="clear: both; margin: 0px 15px; display: block;">';
				
				// get list of master LOINCs
				//$mc_sql = 'SELECT ml.l_id AS id, ml.loinc AS loinc, ml.concept_name AS name FROM '.$my_db_schema.'vocab_master_loinc ml 
					//ORDER BY ml.concept_name, ml.loinc;';
				$mc_sql = 'SELECT ml.l_id AS id, ml.loinc AS loinc, ml.concept_name AS test_name, m2a.coded_value AS condition FROM '.$my_db_schema.'vocab_master_loinc ml 
					LEFT JOIN '.$my_db_schema.'vocab_master_condition mc ON (ml.trisano_condition = mc.c_id)
					LEFT JOIN '.$my_db_schema.'vocab_master2app m2a ON (m2a.master_id = mc.condition) 
					WHERE ml.l_id IN (SELECT DISTINCT ml2.l_id FROM elr.vocab_master_loinc ml2
						INNER JOIN elr.vocab_child_loinc cl ON (cl.master_loinc = ml2.l_id AND lab_id = '.intval($lab_iterator_row->id).'))
					ORDER BY ml.loinc, ml.concept_name, m2a.coded_value;';
				$mc_rs = @pg_query($host_pa, $mc_sql);
				if ($mc_rs) {
					echo '<select size="6" multiple="multiple" class="loincs" name="loincs['.$lab_iterator_row->id.'][]" id="loincs_'.$lab_iterator_row->id.'">';
					while ($mc_row = @pg_fetch_object($mc_rs)) {
						if (is_array($loincs[intval($lab_iterator_row->id)]) && in_array($mc_row->loinc, $loincs[intval($lab_iterator_row->id)])) {
							echo '<option value="'.$mc_row->loinc.'" selected>('.$mc_row->loinc.') '.$mc_row->condition.' ['.$mc_row->test_name.']</option>';
						} else {
							echo '<option value="'.$mc_row->loinc.'">('.$mc_row->loinc.') '.$mc_row->condition.' ['.$mc_row->test_name.']</option>';
						}
					}
					echo "</select>";
				} else {
					suicide("Error:  Could not retrieve list of Master LOINCs.", 1);
				}
				@pg_free_result($mc_rs);
				
				echo '</div><div style="clear: both; display: block; height: 30px;"></div>';
				
			}
			@pg_free_result($lab_iterator_rs);
		?>
	</fieldset><br>
	
	<fieldset class="ui-widget ui-widget-content ui-corner-all">
		<legend>Child Result Code Access</legend>
		
		<?php
			// iterate for each configured lab
			$lab_iterator_sql = 'SELECT id, ui_name FROM elr.structure_labs 
				WHERE id IN (
					SELECT DISTINCT lab_id FROM elr.vocab_child_organism
				) 
				ORDER BY ui_name;';
			$lab_iterator_rs = @pg_query($host_pa, $lab_iterator_sql);
			while ($lab_iterator_rs !== false && $lab_iterator_row = @pg_fetch_object($lab_iterator_rs)) {
				echo '<h2>'.htmlentities($lab_iterator_row->ui_name, ENT_QUOTES, 'UTF-8').' Result Codes</h2>';
				
				echo '<div style="clear: both; margin: 0px 15px; display: block;">';
				
				// get list of child SNOMEDs
				$csn_sql = 'SELECT co.id AS id, co.child_code AS child_code, mv_o.concept AS organism, mv_r.concept AS result
					FROM '.$my_db_schema.'vocab_child_organism co 
					LEFT JOIN '.$my_db_schema.'vocab_master_organism mo ON (co.organism = mo.o_id)
					LEFT JOIN '.$my_db_schema.'vocab_master_organism mo_r ON (co.test_result_id = mo_r.o_id)
					LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_o ON (mv_o.id = mo.organism) 
					LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_r ON (mv_r.id = mo_r.organism) 
					WHERE co.lab_id = '.intval($lab_iterator_row->id).' 
					ORDER BY co.child_code;';
				$csn_rs = @pg_query($host_pa, $csn_sql);
				if ($csn_rs) {
					echo '<select size="6" multiple="multiple" class="child_snomeds" name="childsnomeds['.$lab_iterator_row->id.'][]" id="childsnomeds_'.$lab_iterator_row->id.'">';
					while ($csn_row = @pg_fetch_object($csn_rs)) {
						if (is_array($childsnomeds[intval($lab_iterator_row->id)]) && in_array($csn_row->child_code, $childsnomeds[intval($lab_iterator_row->id)])) {
							echo '<option value="'.$csn_row->child_code.'" selected>'.$csn_row->child_code.' ('.$csn_row->organism.' ['.$csn_row->result.'])</option>';
						} else {
							echo '<option value="'.$csn_row->child_code.'">'.$csn_row->child_code.' ('.$csn_row->organism.' ['.$csn_row->result.'])</option>';
						}
					}
					echo "</select>";
				} else {
					suicide("Error:  Could not retrieve list of Child SNOMEDs.", 1);
				}
				@pg_free_result($csn_rs);
				
				echo '</div><div style="clear: both; display: block; height: 30px;"></div>';
				
			}
			@pg_free_result($lab_iterator_rs);
		?>
	</fieldset><br>
	
	<fieldset class="ui-widget ui-widget-content ui-corner-all">
		<legend>Menus & Features</legend>
		
		<?php echo getSystemMenus($system_role_id);?>
	</fieldset><br>

    <button id="submit_button" name="submit_button" type="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-elrsave"></span><span class="ui-button-text">Save User Role</span></button>&nbsp; <button id="back_button" name="back_button" type="reset" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-elrcancel"></span><span class="ui-button-text">Cancel</span></button>	
	
	<br><br>
	
</form>
<script type="text/javascript">
	$(function() {
		$(".loincs").twosidedmultiselect();
		$(".child_snomeds").twosidedmultiselect();
		$("#system_menu_id").twosidedmultiselect();
		$(".loincs").not(".TakeOver").parent().prepend('<strong><span class="ui-icon ui-icon-elrcancel" style="display: inline-block; margin-bottom: -3px;"></span> Hidden (Inactive) LOINCs</strong><br>');
		$(".loincs.TakeOver").parent().prepend('<strong><span class="ui-icon ui-icon-elrsuccess" style="display: inline-block; margin-bottom: -3px;"></span> Visible (Active) LOINCs</strong><br>');
		$(".child_snomeds").not(".TakeOver").parent().prepend('<strong><span class="ui-icon ui-icon-elrcancel" style="display: inline-block; margin-bottom: -3px;"></span> Hidden (Inactive) Child SNOMEDs</strong><br>');
		$(".child_snomeds.TakeOver").parent().prepend('<strong><span class="ui-icon ui-icon-elrsuccess" style="display: inline-block; margin-bottom: -3px;"></span> Visible (Active) Child SNOMEDs</strong><br>');
		$(".tsmsoptions").prepend('<br>');
		$("#system_menu_idtsms").parent().prepend('<strong><span class="ui-icon ui-icon-elrcancel" style="display: inline-block; margin-bottom: -3px;"></span> Disabled Features</strong><br>');
		$("#system_menu_id").parent().prepend('<strong><span class="ui-icon ui-icon-elrsuccess" style="display: inline-block; margin-bottom: -3px;"></span> Enabled Features</strong><br>');
	});
</script>
