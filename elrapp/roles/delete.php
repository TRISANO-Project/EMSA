<?php

	if(isset($_GET['role_id']) && (intval(trim($_GET['role_id']) > 0))){
		$delete_role_id = intval(trim($_GET['role_id']));
		// batch-ify since we're deleting from 3 tables... make sure all's well in all 3 places...
		$delete_role_sql = "BEGIN;";
		
		$delete_role_sql .= "DELETE FROM ".$my_db_schema."system_roles WHERE id=".$delete_role_id.";";
		$delete_role_sql .= "DELETE FROM ".$my_db_schema."system_role_loincs_by_lab WHERE system_role_id=".$delete_role_id.";";
		$delete_role_sql .= "DELETE FROM ".$my_db_schema."system_role_menus WHERE system_role_id=".$delete_role_id.";";
		
		$delete_role_sql .= "COMMIT;";
		$delete_rs = @pg_query($host_pa,$delete_role_sql);
		if (!$delete_rs) {
			suicide("Could not delete User Role.", 1);
		} else {
			highlight("User Role deleted successfully!", "ui-icon-elrsuccess");
		}
	} else {
		suicide("Unable to delete User Role:  Invalid record ID specified!");
	}

?>