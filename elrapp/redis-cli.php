<?php

function clearRedisCacheById($id) {
    if (!is_numeric($id)) {
        throw new \InvalidArgumentException('Invalid ID!');
    }
    $id = (int)$id;

    $cmd = <<< CMD
redis-cli KEYS '*' | grep {$id} | xargs redis-cli DEL
CMD;
    // echo '<pre>' . var_export($cmd, true) . '</pre>';
    // echo exec('whoami');
    exec($cmd);
}

$id = $_GET['id'];

if (empty($id)) {
    exit();
}

// Start Logic
clearRedisCacheById($id);
