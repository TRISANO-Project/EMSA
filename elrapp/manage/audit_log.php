<script>
	$(function() {
		$("#addnew_form").show();
		
		$("#new_savelab").button({
			icons: {
				primary: "ui-icon-elrsearch"
			}
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
		$(".view_type_container").click(function() {
			$("input[type=radio]", this).first().prop("checked", true);
		});
		
		$(".date-range").datepicker();
		
	});
</script>

<style type="text/css">
	label.vocab_search_form { font-family: 'Francois One', serif; margin-left: 10px; font-weight: 400; font-size: 1.5em; }
	input[type="radio"] { min-width: 0 !important; }
	#new_lab_form div { display: inline-block; margin: 7px; padding: 10px; }
	#labResults td { border-bottom-width: 2px; border-bottom-color: darkorange; }
	.audit_log td { border-bottom-width: 1px !important; border-bottom-color: lightgray !important; }
	.view_type_container, .view_type_container label { font-family: 'Open Sans', Arial, Helvetica, sans-serif !important; font-weight: 600; }
</style>

<?php

	if (isset($_GET['text_search']) && strlen(trim($_GET['text_search'])) > 0) {
		$clean['keywords'] = filter_var(trim($_GET['text_search']), FILTER_SANITIZE_STRING);
	}
	
	if (isset($_GET['message_id']) && filter_var(trim($_GET['message_id']), FILTER_VALIDATE_INT)) {
		$clean['message_id'] = filter_var(trim($_GET['message_id']), FILTER_SANITIZE_NUMBER_INT);
	}
	
	if (isset($_GET['view_type']) && filter_var($_GET['view_type'], FILTER_VALIDATE_INT) && in_array(intval(trim($_GET['view_type'])), array(1, 2, 3))) {
		$clean['view_type'] = filter_var(trim($_GET['view_type']), FILTER_SANITIZE_NUMBER_INT);
	}
	
	if (isset($_GET['date_from']) && (strlen(trim($_GET['date_from'])) > 0)) {
		$clean['date_from'] = filter_var(trim($_GET['date_from']), FILTER_SANITIZE_STRING);
	}

	if (isset($_GET['date_to']) && (strlen(trim($_GET['date_to'])) > 0)) {
		$clean['date_to'] = filter_var(trim($_GET['date_to']), FILTER_SANITIZE_STRING);
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elraudit"></span>Audit Log Viewer</h1>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">View Audit Logs By:</label></div>
	<form id="new_lab_form" method="GET" action="<?php echo $main_page; ?>">
		<div class="view_type_container ui-widget-content ui-corner-all"><input type="radio" name="view_type" id="view_type_recent" value="3"<?php echo ((isset($clean['view_type']) && ($clean['view_type'] == 3)) ? " checked" : ""); ?>/><label class="vocab_search_form2" for="view_type_recent">View Most-Recent Messages</label></div>
		<div class="view_type_container ui-widget-content ui-corner-all"><input type="radio" name="view_type" id="view_type_date" value="2"<?php echo ((isset($clean['view_type']) && ($clean['view_type'] == 2)) ? " checked" : ""); ?>/><label class="vocab_search_form2" for="view_type_date">View by Date Range:</label> <input class="date-range ui-corner-all" type="text" name="date_from" id="date_from" value="<?php echo ((isset($clean['date_from'])) ? $clean['date_from'] : ""); ?>" placeholder="Any Time"> to <input class="date-range ui-corner-all" type="text" name="date_to" id="date_to" value="<?php echo ((isset($clean['date_to'])) ? $clean['date_to'] : ""); ?>" placeholder="Present"></div>
		<div class="view_type_container ui-widget-content ui-corner-all"><label class="vocab_search_form2" for="text_search">HL7 Keyword Search:</label> <input class="ui-corner-all" type="text" name="text_search" id="text_search" value="<?php echo ((isset($clean['keywords'])) ? $clean['keywords'] : ''); ?>"></div>
		<hr>
		<div class="view_type_container ui-widget-content ui-corner-all"><input type="radio" name="view_type" id="view_type_id" value="1"<?php echo ((isset($clean['view_type']) && ($clean['view_type'] == 1)) ? " checked" : ""); ?>/><label class="vocab_search_form2" for="view_type_id">View by Message ID#:</label> <input class="ui-corner-all" type="text" name="message_id" id="message_id" value="<?php echo ((isset($clean['message_id'])) ? $clean['message_id'] : ""); ?>"></div>
		<hr>
		<input type="hidden" name="selected_page" id="selected_page" value="<?php echo intval($selected_page); ?>">
		<input type="hidden" name="submenu" id="submenu" value="<?php echo intval($submenu); ?>">
		<input type="hidden" name="cat" id="cat" value="<?php echo intval($cat); ?>">
		<br><button type="submit" name="new_savelab" id="new_savelab">View Selected Audit Logs</button>
	</form>
</div>

<?php

	// check for view type
	if (isset($clean['view_type'])) {
		// valid 'view_type' flag
		
?>
<div class="lab_results_container ui-widget ui-corner-all">
	<h3>Message Audits...</h3>
	<table id="labResults">
		<thead>
			<tr>
				<th style="width: 25%;">Message Details</th>
				<th style="width: 14%;">Event Date/Time</th>
				<th style="width: 12%;">User</th>
				<th style="width: 8%;">Category</th>
				<th style="width: 35%;">Action</th>
				<th style="width: 6%;">Status</th>
			</tr>
		</thead>
		<tbody>
<?php

		if ($clean['view_type'] == 1) {
			// view by message id
			$outer_qry = "SELECT DISTINCT a.system_message_id, m.original_message_id, m.deleted, s.name, s.id AS queue_id, o.created_at 
				FROM ".$my_db_schema."system_messages_audits a 
				INNER JOIN ".$my_db_schema."system_messages m ON (a.system_message_id = m.id) 
				INNER JOIN ".$my_db_schema."system_original_messages o ON (m.original_message_id = o.id)
				LEFT JOIN ".$my_db_schema."system_statuses s ON (m.final_status = s.id) 
				WHERE (a.system_message_id = ".$clean['message_id'].") OR (m.original_message_id = ".$clean['message_id'].") 
				GROUP BY a.system_message_id, m.original_message_id, m.deleted, s.name, o.created_at, s.id
				ORDER BY o.created_at DESC;";
			
		} elseif ($clean['view_type'] == 2) {
			// view by date range
			unset($date_range_clause);
			unset($keyword_clause);
			if (isset($clean['date_from']) && isset($clean['date_to'])) {
				// between
				$date_range_clause = "WHERE o.created_at BETWEEN '" . date(DATE_W3C, strtotime($clean['date_from'])) . "' AND '" . date(DATE_W3C, strtotime($clean['date_to'])) . "' ";
			} elseif (isset($clean['date_from'])) {
				// start to infinity
				$date_range_clause = "WHERE o.created_at > '" . date(DATE_W3C, strtotime($clean['date_from'])) . "' ";
			} elseif (isset($clean['date_to'])) {
				// infinity to end
				$date_range_clause = "WHERE o.created_at < '" . date(DATE_W3C, strtotime($clean['date_to'])) . "' ";
			} else {
				$date_range_clause = "";
			}
			if (isset($clean['keywords'])) {
				if (isset($clean['date_from']) || isset($clean['date_to'])) {
					$keyword_clause = ' AND o.message ILIKE \'%'.pg_escape_string($clean['keywords']).'%\' ';
				} else {
					$keyword_clause = 'WHERE o.message ILIKE \'%'.pg_escape_string($clean['keywords']).'%\' ';
				}
			} else {
				$keyword_clause = '';
			}
			$outer_qry = "SELECT DISTINCT a.system_message_id, m.original_message_id, m.deleted, s.name, s.id AS queue_id, o.created_at 
				FROM ".$my_db_schema."system_messages_audits a 
				INNER JOIN ".$my_db_schema."system_messages m ON (a.system_message_id = m.id) 
				INNER JOIN ".$my_db_schema."system_original_messages o ON (m.original_message_id = o.id)
				LEFT JOIN ".$my_db_schema."system_statuses s ON (m.final_status = s.id) ".$date_range_clause.$keyword_clause."
				GROUP BY a.system_message_id, m.original_message_id, m.deleted, s.name, o.created_at, s.id
				ORDER BY o.created_at DESC;";
			
		} else {
			// view by most-recent
			if (isset($clean['keywords'])) {
				$keyword_clause = 'WHERE o.message ILIKE \'%'.pg_escape_string($clean['keywords']).'%\' ';
			} else {
				$keyword_clause = '';
			}
			$outer_qry = "SELECT DISTINCT a.system_message_id, m.original_message_id, m.deleted, s.name, s.id AS queue_id, o.created_at 
				FROM ".$my_db_schema."system_messages_audits a 
				INNER JOIN ".$my_db_schema."system_messages m ON (a.system_message_id = m.id) 
				INNER JOIN ".$my_db_schema."system_original_messages o ON (m.original_message_id = o.id)
				LEFT JOIN ".$my_db_schema."system_statuses s ON (m.final_status = s.id) ".$keyword_clause." 
				GROUP BY a.system_message_id, m.original_message_id, m.deleted, s.name, o.created_at, s.id
				ORDER BY o.created_at DESC 
				LIMIT 50;";
			
		}
		
		$outer_rs = @pg_query($host_pa, $outer_qry);
		if ($outer_rs) {
			if (pg_num_rows($outer_rs) > 0) {
				// draw results
				while ($outer_row = pg_fetch_object($outer_rs)) {
					unset ($message_link);
					$message_link = '?selected_page=6&submenu=6&focus='.trim($outer_row->system_message_id); // view individual msg page
					echo '<tr>';
					echo '<td style="text-align: left; vertical-align: top;"><strong>Original Msg ID/System Msg ID:</strong><br>'.intval($outer_row->original_message_id).'/'.intval($outer_row->system_message_id).'
						<br><br><strong>Received:</strong><br>'.date("Y-m-d H:i:s", strtotime($outer_row->created_at));
					if (intval($outer_row->deleted) > 0) {
						echo '<br><br><strong>Current Queue:</strong><br><span style="color: dimgray; font-weight: 700; text-decoration: line-through;">'.htmlentities($outer_row->name, ENT_QUOTES, "UTF-8").'</span> Deleted</td>';
					} else {
						echo '<br><br><strong>Current Queue:</strong><br><a style="font-weight: 700;" href="'.MAIN_URL.'/'.$message_link.'" target="_blank">'.htmlentities($outer_row->name, ENT_QUOTES, "UTF-8").'</a></td>';
					}
					echo '<td colspan="5" style="text-align: left; vertical-align: top;"><table class="audit_log">';
					echo '<tbody>';
					
					// get individual audits for this message
					unset($inner_qry);
					unset($inner_rs);
					unset($inner_row);
					$inner_qry = sprintf("SELECT au.user_id AS user_id, ac.message AS action, au.info AS info, ca.name AS category, au.created_at AS created_at, ss.name AS status 
						FROM %ssystem_messages_audits au 
						JOIN %ssystem_message_actions ac ON (au.message_action_id = ac.id)
						JOIN %ssystem_action_categories ca ON (ac.action_category_id = ca.id)
						JOIN %ssystem_statuses ss ON (au.system_status_id = ss.id)
						WHERE au.system_message_id = %s
						ORDER BY au.created_at DESC;",
					$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, intval($outer_row->system_message_id));
					$inner_rs = @pg_query($host_pa, $inner_qry);
					while($inner_row = pg_fetch_object($inner_rs)) {
						echo "<tr>";
						echo "<td style=\"width: 14%;\">".date("m/d/Y H:i:s", strtotime($inner_row->created_at))."</td>";
						echo "<td style=\"width: 12%;\">".userRealNameByUmdid(trim($inner_row->user_id))."</td>";
						echo "<td style=\"width: 8%;\">".htmlentities($inner_row->category, ENT_QUOTES, "UTF-8")."</td>";
						echo "<td style=\"width: 35%;\">".htmlentities($inner_row->action, ENT_QUOTES, "UTF-8").((strlen(trim($inner_row->info)) > 0) ? '<br>Comments: '.htmlentities($inner_row->info, ENT_QUOTES, "UTF-8") : '')."</td>";
						echo "<td style=\"width: 6%;\">".htmlentities($inner_row->status, ENT_QUOTES, "UTF-8")."</td>";
						echo "</tr>";
					}
					@pg_free_result($inner_rs);
					
					echo "</tbody></table></td>";
					echo "</tr>";
				}
			} else {
				// draw 'no results'
				echo "<tr><td colspan=\"6\"><em>No messages found that match your criteria</em></td></tr>";
				
			}
		} else {
			suicide("Could not connect to Audit Log database.", 1);
		
		}
		@pg_free_result($outer_rs);

?>

		</tbody>
	</table>
	
<?php

	}
	
?>
	
</div>