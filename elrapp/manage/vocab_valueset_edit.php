<?php

	$va = new VocabAudit();

	// verify we've got a valid ID to edit
	unset($vocab_id);
	if (is_numeric($_GET['edit_id']) && (intval(trim($_GET['edit_id'])) > 0)) {
		$validedit_sql = sprintf("SELECT count(id) AS id FROM %s%s WHERE id = %d", $my_db_schema, (($vocab != 9) ? "vocab_master_vocab" : "vocab_child_vocab"), intval(trim($_GET['edit_id'])));
		$validedit_count = @pg_fetch_result(@pg_query($host_pa, $validedit_sql), 0, id);
		if ($validedit_count == 1) {
			$vocab_id = intval(trim($_GET['edit_id']));
		}
	}
	
	if (!isset($vocab_id)) {
		suicide("Cannot edit vocabulary:  Record not found", -1, 1);
	}

	if (isset($_GET['save_flag'])) {
		// save changes
		$changes_saved = FALSE;
		
		// make sure rows exist in vocab_master2app for all apps and this master_id
		// in some cases, when there isn't a corresponding app value at the time the master value is created, 
		// editing won't update the row for that app since it doesn't exist yet
		createMissingMasterToAppVocab($vocab_id);
		
		if ($vocab != 9) {
			// master values
			$valid_edit = FALSE;
			$valid_valueset = isset($_GET['edit_codeset']);
			$valid_concept = ((isset($_GET['edit_masterconcept'])) && (ctype_print(trim($_GET['edit_masterconcept']))) && (strlen(trim($_GET['edit_masterconcept'])) > 0));
			$valid_category = ((isset($_GET['edit_category'])) && (ctype_print(trim($_GET['edit_category']))) && (strlen(trim($_GET['edit_category'])) > 0) && (trim($_GET['edit_category']) != "-1"));
			$valid_appid = ((isset($_GET['edit_appconcept'])) && (is_array($_GET['edit_appconcept'])));
			$valid_edit = $valid_valueset && $valid_concept && $valid_category && $valid_appid;
			if ($valid_edit) {
				$mv_old_vals = $va->getPreviousVals($vocab_id, VocabAudit::TABLE_MASTER_VOCAB);
				$m2a_old_vals = $va->getPreviousVals($vocab_id, VocabAudit::TABLE_MASTER_TO_APP);
				// check to make sure all app values passed correspond to actual configured applications
				unset($edit_apps);
				foreach ($_GET['edit_appconcept'] as $edit_appid => $edit_appvalue) {
					$appvalues_sql = sprintf("SELECT id, app_name FROM %svocab_app WHERE id = %d", $my_db_schema, intval($edit_appid));
					if ($appvalues_row = @pg_fetch_object(@pg_query($host_pa, $appvalues_sql))) {
						$edit_apps[$appvalues_row->id] = array("app_name" => $appvalues_row->app_name, "app_value" => $edit_appvalue);
					}
				}
				if (isset($edit_apps)) {
					// transact-ify the updates to make sure stuff doesn't get out of sync between master & master2app table
					$update_sql = "BEGIN;";
					$update_sql .= sprintf("UPDATE %svocab_master_vocab SET category = %s, codeset = %s, concept = '%s' WHERE id = %d;",
						$my_db_schema,
						((intval(trim($_GET['edit_category'])) > 0) ? intval(trim($_GET['edit_category'])) : "NULL"),
						((strlen(trim($_GET['edit_codeset'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_codeset']))."'" : "NULL"),
						pg_escape_string(trim($_GET['edit_masterconcept'])),
						$vocab_id);
					foreach ($edit_apps as $this_app_id => $this_app_data) {
						// insert app-specific values
						$update_sql .= sprintf("UPDATE %svocab_master2app SET coded_value = %s WHERE master_id = %d AND app_id = %d;",
							$my_db_schema,
							((strlen(trim($this_app_data['app_value'])) > 0) ? "'".pg_escape_string(trim($this_app_data['app_value']))."'" : "NULL"),
							$vocab_id,
							intval($this_app_id));
					}
					$update_sql .= "COMMIT;";
					
					if (@pg_query($host_pa, $update_sql)) {
						$changes_saved = TRUE;
						
						$va->resetAudit();
						$va->setOldVals($mv_old_vals);
						$va->setNewVals($va->prepareNewValues(VocabAudit::TABLE_MASTER_VOCAB, array('category' => intval(trim($_GET['edit_category'])), 'valueset' => trim($_GET['edit_codeset']), 'masterconcept' => trim($_GET['edit_masterconcept']))));
						$va->auditVocab($vocab_id, VocabAudit::TABLE_MASTER_VOCAB, VocabAudit::ACTION_EDIT);
						
						$va->resetAudit();
						$va->setOldVals($m2a_old_vals);
						$va->setNewVals($va->prepareNewValues(VocabAudit::TABLE_MASTER_TO_APP, array('app_id' => intval($this_app_id), 'appvalue' => trim($this_app_data['app_value']))));
						$va->auditVocab($vocab_id, VocabAudit::TABLE_MASTER_TO_APP, VocabAudit::ACTION_EDIT);
					} else {
						suicide("Unable to save changes to vocabulary", 1);
					}
				} else {
					suicide("Could not save changes:  Application(s) not found");
				}
			} else {
				suicide("Could not save changes:  Not all required fields had the correct values entered.");
			}
		} else {
			// child values
			// make sure master_id & lab_id are valid values
			$clean_lab = (isset($_GET['edit_child']) && ctype_digit($_GET['edit_child']) && (intval(trim($_GET['edit_child'])) > 0)) ? intval(trim($_GET['edit_child'])) : -1;
			$clean_masterid = (isset($_GET['edit_masterid']) && ctype_digit($_GET['edit_masterid']) && (intval(trim($_GET['edit_masterid'])) > 0)) ? intval(trim($_GET['edit_masterid'])) : -1;
			$valid_sql = sprintf("SELECT (
					(SELECT count(id) FROM %sstructure_labs WHERE id = %d) * 
					(SELECT count(id) FROM %svocab_master_vocab WHERE id = %d)
				) AS validcount", 
				$my_db_schema, $clean_lab, 
				$my_db_schema, $clean_masterid);
				
			if (($valid_ids = @pg_fetch_result(@pg_query($host_pa, $valid_sql), 0, validcount)) === FALSE) {
				suicide("Could not validate Lab or Master ID", 1, 1);
			}
			if ($valid_ids == 0) {
				suicide("Changes not saved:  Invalid Lab and/or Master Value selected.");
			} else {
				// update the database
				$update_sql = sprintf("UPDATE %svocab_child_vocab SET lab_id = %d, master_id = %d, concept = %s WHERE id = %d",
					$my_db_schema,
					$clean_lab,
					$clean_masterid,
					((strlen(trim($_GET['edit_childconcept'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_childconcept']))."'" : "NULL"),
					$vocab_id);
				if (@pg_query($host_pa, $update_sql)) {
					$changes_saved = TRUE;
				} else {
					suicide("Unable to save changes to vocabulary", 1);
				}
			}
		}
		
		#debug
		/*
		echo "<pre>";
		print_r($_GET);
		echo "</pre>";
		*/
		
		if ($changes_saved) {
			highlight("Changes to vocabulary successfully saved!", "ui-icon-elrsuccess");
		}
		
	} else {
		// draw 'edit' form
	?>
		<script>
			$(function() {
				$("#edit_cancel").button({
					icons: {
						primary: "ui-icon-elrcancel"
					}
				}).click(function(e) {
					e.preventDefault();
					var cancelAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo $vocab; ?>";
					window.location.href = cancelAction;
				});
				
				$("#edit_savevocab").button({
					icons: {
						primary: "ui-icon-elrsave"
					}
				});
				
			<?php
				if ($vocab != 9) {
					// load master data to form
					$editvals_sql = sprintf("SELECT category, codeset, concept FROM %svocab_master_vocab WHERE id = %d", $my_db_schema, $vocab_id);
					$editvals_row = @pg_fetch_object(@pg_query($host_pa, $editvals_sql));
					printf("$(\"#edit_category\").val(%s);\n", json_encode($editvals_row->category));
					printf("$(\"#edit_codeset\").val(%s);\n", json_encode($editvals_row->codeset));
					printf("$(\"#edit_masterconcept\").val(%s);\n", json_encode($editvals_row->concept));
					
					$editapp_sql = sprintf("SELECT id, app_name FROM %svocab_app ORDER BY app_name;", $my_db_schema);
					$editapp_result = @pg_query($host_pa, $editapp_sql) or suicide("Unable to retrieve list of Applications.", 1, 1);
					while ($editapp_row = pg_fetch_object($editapp_result)) {
						$editappvalue_sql = sprintf("SELECT coded_value FROM %svocab_master2app WHERE app_id = %d AND master_id = %d;", 
							$my_db_schema,
							intval($editapp_row->id),
							$vocab_id);
						$editappvalue_rs = @pg_query($host_pa, $editappvalue_sql) or suicide("Unable to retrieve application-specific concept name.", 1, 1);
						$editappvalue = ((pg_num_rows($editappvalue_rs) > 0) ? @pg_fetch_result($editappvalue_rs, 0, coded_value) : '');
						$editapppath = @pg_fetch_result($editappvalue_rs, 0, xpath);
						printf("$(\"#edit_appconcept_%d\").val(%s);\n", $editapp_row->id, json_encode($editappvalue));
					
					}
					pg_free_result($editapp_result);
				} else {
					// load child data to form
					$editvals_sql = sprintf("SELECT cv.lab_id AS lab_id, cv.concept AS concept, cv.master_id AS master_id, mv.category AS category FROM %svocab_child_vocab cv JOIN %svocab_master_vocab mv ON (cv.master_id = mv.id) WHERE cv.id = %d", $my_db_schema, $my_db_schema, $vocab_id);
					$editvals_row = @pg_fetch_object(@pg_query($host_pa, $editvals_sql));
					printf("$(\"#edit_child\").val(%s);\n", json_encode($editvals_row->lab_id));
					printf("$(\"#edit_childconcept\").val(%s);\n", json_encode($editvals_row->concept));
					printf("$(\"#edit_mastercat\").val(%s);\n", json_encode($editvals_row->category));
					echo "$(\"#edit_mastercat\").trigger('change');\n";
					printf("$(\"#edit_masterid\").val(%s);\n", json_encode($editvals_row->master_id));
				}
			?>
			});
		</script>
		
		<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrglossary"></span><?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?> Editor</h1>
		
		<div id="edit_form" class="edit_vocab_form ui-widget ui-widget-content ui-corner-all">
		<div style="clear: both;"><label class="vocab_search_form">Edit <?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?>:</label><br><br></div>
		<form id="edit_vocab_form" method="GET" action="<?php echo $main_page; ?>">
		<?php
			if ($vocab != 9) {
				// Add New Master Vocab form
		?>
				<div class="addnew_field_vs"><label class="vocab_add_form" for="edit_category">Category:</label><br>
					<select class="ui-corner-all" name="edit_category" id="edit_category">
						<option value="-1" selected>--</option>
					<?php
						// get list of top-level labs for alias menu
						$newcategory_sql = sprintf("SELECT id, label FROM %sstructure_category;", $my_db_schema);
						$newcategory_result = @pg_query($host_pa, $newcategory_sql) or suicide("Unable to retrieve list of Master Categories.", 1, 1);
						while ($newcategory_row = pg_fetch_object($newcategory_result)) {
							printf("<option value=\"%s\">%s</option>", intval($newcategory_row->id), htmlentities($newcategory_row->label, ENT_QUOTES, "UTF-8"));
						}
						pg_free_result($newcategory_result);
					?>
					</select>
				</div>
				<div class="addnew_field_vs"><label class="vocab_add_form" for="edit_codeset">Value Set Code:</label><br><input class="ui-corner-all" type="text" name="edit_codeset" id="edit_codeset" style="width: 90%;" /></div>
				<div class="addnew_field_vs"><label class="vocab_add_form" for="edit_masterconcept">Master Concept Name:</label><br><input class="ui-corner-all" type="text" name="edit_masterconcept" id="edit_masterconcept" style="width: 90%;" /></div>
				
			<?php
				// draw app-specific value input for each configured app
				$newapp_sql = sprintf("SELECT id, app_name FROM %svocab_app ORDER BY app_name;", $my_db_schema);
				$newapp_result = @pg_query($host_pa, $newapp_sql) or suicide("Unable to retrieve list of Applications.", 1, 1);
				while ($newapp_row = pg_fetch_object($newapp_result)) {
					printf("<div class=\"addnew_field_vs\"><label class=\"vocab_add_form\" for=\"edit_appconcept_%d\">%s Value:</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_appconcept[%d]\" id=\"edit_appconcept_%d\" style=\"width: 90%%;\" /></div>", 
						intval($newapp_row->id),
						htmlentities(trim($newapp_row->app_name), ENT_QUOTES, "UTF-8"),
						intval($newapp_row->id),
						intval($newapp_row->id));
				}
				pg_free_result($newapp_result);
				
			} else {
				// Add New Child Vocab form
		?>
				
				<div class="addnew_field_vs"><label class="vocab_add_form" for="edit_child">Lab:</label><br>
					<select class="ui-corner-all" name="edit_child" id="edit_child">
						<option value="-1" selected>--</option>
					<?php
						// get list of top-level labs for alias menu
						$newchild_sql = sprintf("SELECT id, ui_name FROM %sstructure_labs WHERE alias_for < 1 ORDER BY ui_name;", $my_db_schema);
						$newchild_result = @pg_query($host_pa, $newchild_sql) or suicide("Unable to retrieve list of Labs.", 1, 1);
						while ($newchild_row = pg_fetch_object($newchild_result)) {
							printf("<option value=\"%d\">%s</option>", intval($newchild_row->id), htmlentities($newchild_row->ui_name, ENT_QUOTES, "UTF-8"));
						}
						pg_free_result($newchild_result);
					?>
					</select>
				</div>
				<div class="addnew_field_vs"><label class="vocab_add_form" for="edit_childconcept">Child Code:</label><br><input class="ui-corner-all" type="text" name="edit_childconcept" id="edit_childconcept" /></div>
				<div class="addnew_field_vs"><label class="vocab_add_form" for="edit_mastercat">Master Category:</label>
					<select class="ui-corner-all" name="edit_mastercat" id="edit_mastercat">
						<option value="-1" selected>--</option>
					<?php
						// get unique categories for auto-populating master concept list
						$newmastercat_sql = sprintf("SELECT id, label FROM %sstructure_category;", $my_db_schema);
						$newmastercat_result = @pg_query($host_pa, $newmastercat_sql) or suicide("Unable to retrieve Master Vocabulary.", 1, 1);
						while ($newmastercat_row = pg_fetch_object($newmastercat_result)) {
							printf("<option value=\"%s\">%s</option>", intval($newmastercat_row->id), htmlentities($newmastercat_row->label, ENT_QUOTES, "UTF-8"));
						}
						pg_free_result($newmastercat_result);
					?>
					</select>
				<br><span class="ui-icon ui-icon-arrowreturnthick-1-e" style="float: left; margin-left: 30px; margin-top: 3px;"></span><label class="vocab_search_form2" for="edit_masterid">Master Concept Name:</label>
					<select class="ui-corner-all" name="edit_masterid" id="edit_masterid" style="width: auto; max-width: 60%; min-width: 10%;">
						<option value="-1" selected>--</option>
					<?php
						// get list of top-level labs for alias menu
						$newmasterid_sql = sprintf("SELECT id, category, concept FROM %svocab_master_vocab ORDER BY category, concept;", $my_db_schema);
						$newmasterid_result = @pg_query($host_pa, $newmasterid_sql) or suicide("Unable to retrieve Master Vocabulary.", 1, 1);
						//echo "<script type=\"text/javascript\">\nvar master_temp = [];\n";
						while ($newmasterid_row = pg_fetch_object($newmasterid_result)) {
							$temp_master[$newmasterid_row->category][intval($newmasterid_row->id)] = $newmasterid_row->concept;
						}
						pg_free_result($newmasterid_result);
					?>
					</select>
					
					<script type="text/javascript">
						var masterList = <?php echo json_encode($temp_master); ?>;
						
						$("#edit_mastercat").change(function () {
							var selectedCat = $("#edit_mastercat").val();
							
							$("#edit_masterid").empty();
							if (selectedCat != -1) {
								$.each(masterList[selectedCat], function(id, label) {
									$("#edit_masterid").append($("<option />").val(id).text(label));
								});
							}
							var master_id_list = $("#edit_masterid option");
							
							// major kudos to the fine folks at http://stackoverflow.com/questions/45888/what-is-the-most-efficient-way-to-sort-an-html-selects-options-by-value-while for this elegant option list sorting solution!!!
							// p.s. -- Sortable arrays, but no associative keys.  Associative object properties, but object properties can't be sorted... really, JavaScript?  Thanks for the help.</sarcasm>
							master_id_list.sort(function(a,b) {
								if (a.text > b.text) return 1;
								else if (a.text < b.text) return -1;
								else return 0;
							});
							$("#edit_masterid").empty();
							$("#edit_masterid").append($("<option />").val(-1).text("--"));
							$("#edit_masterid").append(master_id_list);
							$("#edit_masterid").val(-1);
						});
					</script>
				</div>
		<?php
				
			}
		?>
				<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
				<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
				<input type="hidden" name="vocab" value="<?php echo intval($vocab); ?>" />
				<input type="hidden" name="edit_id" value="<?php echo intval($vocab_id); ?>" />
				<input type="hidden" name="save_flag" value="1" />
				<br><br><button type="submit" name="edit_savevocab" id="edit_savevocab">Save Changes</button>
				<button type="button" name="edit_cancel" id="edit_cancel">Cancel</button>
			</form>
		</div>
		
		<div id="vocab_log" class="edit_vocab_form ui-widget ui-state-highlight ui-widget-content ui-corner-all">
			<div style="clear: both;"><label class="vocab_search_form">Audit Log</label><br><br></div>
			<?php echo $va->displayVocabAuditById(intval($vocab_id), array(VocabAudit::TABLE_MASTER_VOCAB, VocabAudit::TABLE_CHILD_VOCAB, VocabAudit::TABLE_MASTER_TO_APP)); ?>
		</div>
		
	<?php
	
		exit();
		// don't show the rest of the Value Set page
	}
	
?>