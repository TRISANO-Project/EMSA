<script type="text/javascript">
	$(function() {
		$("#addhl7form").show();
		$("#btn_save").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		$(".date-range").datepicker();
	});
</script>

<?php

	function elapsed_time($start_time) {
		return round((microtime(true) - $start_time), 3)."s";
	}
	
	if (isset($_POST['mode']) && (strlen(trim($_POST['mode'])) > 0)) {
		switch (filter_var(trim($_POST['mode']), FILTER_SANITIZE_NUMBER_INT)) {
			case 2:
				$clean['mode'] = 2; // messages (csv)
				break;
			default:
				$clean['mode'] = 1; // codes & results (xlsx)
				break;
		}
	} else {
		$clean['mode'] = 1;
	}

	if (isset($_POST['connector']) && (strlen(trim($_POST['connector'])) > 0)) {
		$clean['connector'] = filter_var(trim($_POST['connector']), FILTER_SANITIZE_STRING);
	}

	if (isset($_POST['date_from']) && (strlen(trim($_POST['date_from'])) > 0)) {
		$clean['date_from'] = filter_var(trim($_POST['date_from']), FILTER_SANITIZE_STRING);
	}

	if (isset($_POST['date_to']) && (strlen(trim($_POST['date_to'])) > 0)) {
		$clean['date_to'] = filter_var(trim($_POST['date_to']), FILTER_SANITIZE_STRING);
	}

	if (isset($_POST['generate_flag']) && filter_var($_POST['generate_flag'], FILTER_VALIDATE_INT) && (intval($_POST['generate_flag']) == 1)) {
		$old_max_time = ini_get('max_execution_time');
		ini_set('max_execution_time', 10800);
		ini_set('memory_limit', '5120M');
		$time_start = microtime(true); // elapsed time debugging
		
		unset($messages);
		unset($columns);
		unset($codes);
		unset($obr_codes);
		unset($connector_clause);
		unset($date_range_clause);
		
		if ($clean['mode'] == 2) {
			$messages = array();
			$columns = array();
		} else {
			$codes = array();
			$obr_codes = array();
			$result_codes = array();
		}
		
		if (!isset($clean['connector']) || empty($clean['connector'])) {
			suicide('No connector specified!  Unable to generate report', -1, 1);
		} else {
			$connector_clause = 'WHERE connector = \''.pg_escape_string($clean['connector']).'\'';
		}
		
		if (isset($clean['date_from']) && isset($clean['date_to'])) {
			// between
			$date_range_clause = " AND (created_at BETWEEN '" . date(DATE_W3C, strtotime($clean['date_from'])) . "' AND '" . date(DATE_W3C, strtotime($clean['date_to'])) . "')";
		} elseif (isset($clean['date_from'])) {
			// start to infinity
			$date_range_clause = " AND (created_at > '" . date(DATE_W3C, strtotime($clean['date_from'])) . "')";
		} elseif (isset($clean['date_to'])) {
			// infinity to end
			$date_range_clause = " AND (created_at < '" . date(DATE_W3C, strtotime($clean['date_to'])) . "')";
		} else {
			$date_range_clause = "";
		}
		
		if (isset($connector_clause) && isset($date_range_clause)) {
			$sql = 'SELECT id, message FROM '.$my_db_schema.'system_original_messages '.$connector_clause.$date_range_clause.' ORDER BY created_at;';
			$rs = @pg_query($host_pa, $sql);
			
			if ($rs !== false) {
				while ($row = @pg_fetch_object($rs)) {
					unset($this_message_segments);
					$messages[$row->id] = array();
					foreach (preg_split("/((\r?\n)|(\r\n?))/", str_replace('\015', "\r", $row->message)) as $line) {
						unset($line_segments);
						unset($this_segment_type);
						$line_segments = explode('|', $line);
						$this_segment_type = trim($line_segments[0]);
						if (isset($this_message_segments[$this_segment_type])) {
							$this_message_segments[$this_segment_type] = $this_message_segments[$this_segment_type] + 1;
						} else {
							$this_message_segments[$this_segment_type] = 1;
						}
						
						if ($clean['mode'] == 2) {
							foreach ($line_segments as $segment_id => $segment_value) {
								if ($segment_id > 0) {
									unset ($long_segment_id);
									$long_segment_id = (($this_message_segments[$this_segment_type] == 1) ? $this_segment_type.'-'.trim($segment_id) : $this_segment_type.'_'.trim($this_message_segments[$this_segment_type]).'-'.trim($segment_id));
									// add segment type+id to $columns
									if (!in_array($long_segment_id, $columns)) {
										$columns[] = $long_segment_id;
									}
									// add segment value to $messages for this message ID
									$messages[$row->id][$long_segment_id] = trim($segment_value);
								}
							}
						} else {
							// if OBX, add distinct codes & results to $codes
							if ($this_segment_type == 'OBX') {
								if (!isset($codes[$line_segments[3]])) {
									$codes[$line_segments[3]] = array();
								}
								if (!in_array(trim($line_segments[5]), $codes[$line_segments[3]])) {
									$codes[trim($line_segments[3])][] = trim($line_segments[5]);
								}
								if (isset($result_codes[trim($line_segments[3])][trim($line_segments[5])])) {
									$result_codes[trim($line_segments[3])][trim($line_segments[5])]++;
								} else {
									$result_codes[trim($line_segments[3])][trim($line_segments[5])] = 1;
								}
							}
							
							// if OBR, add distinct OBR LOINC codes $obr_codes
							if ($this_segment_type == 'OBR') {
								if (!isset($obr_codes[$line_segments[4]])) {
									$obr_codes[$line_segments[4]] = array();
								}
							}
						}
					}
				}
			} else {
				suicide('An error occurred while querying HL7 messages.', 1, 1);
			}
			pg_free_result($rs);
			
			if ($clean['mode'] == 2) {
				usort($columns, 'hl7ColumnSort'); // group by segment & order by segment element number (function in master_functions.php)
				
				// save our messages CSV output
				ob_clean();
				$this_filename = $clean['connector'].'_hl7_review_messages_'.date("YmdHis", time()).'.csv';
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment; filename='.$this_filename);
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				
				$output_file = fopen('php://output', 'w');
				
				// set column headings
				fwrite($output_file, 'MsgID,');
				fputcsv($output_file, $columns, ',', '"');
				
				// process data rows
				foreach ($messages as $message_id => $message_data) {
					fwrite($output_file, intval($message_id).',');
					unset($this_line);
					$this_line = array();
					foreach ($columns as $column_key => $column_name) {
						$this_line[] = trim($message_data[$column_name]);
					}
					fputcsv($output_file, $this_line, ',', '"');
				}
				
				unset($messages);
				unset($columns);
				
				fclose($output_file);
				exit;
			} else {
				/**
				 * Initialize PHPExcel stuff
				 * 
				 */
				require_once('./includes/classes/PHPExcel.php');
				unset($this_phe);
				unset($this_pheWriter);
				ksort($codes);
				ksort($obr_codes);
				ksort($result_codes);
				arsort($result_codes);
				
				$tabs_array = array($clean['connector'].' Test Codes', $clean['connector'].' Codes-Results', $clean['connector'].' OBR LOINC Codes');
			
				// create a new PHPExcel document & writer
				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array('memoryCacheSize'  => '2048MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
				
				$this_phe = new PHPExcel();
				
				for ($this_sheet_index = 0; $this_sheet_index < count($tabs_array); $this_sheet_index++) {
					unset($this_row_cursor);
					$this_row_cursor = 2;  // skip over header row
					if ($this_sheet_index > 0) {
						$this_phe->createSheet();
					}
					// set the current sheet & change the name
					$this_phe->setActiveSheetIndex($this_sheet_index);
					$this_phe_sheet = $this_phe->getActiveSheet();
					$this_phe_sheet->setTitle(substr($tabs_array[$this_sheet_index], 0, 30));
					
					if ($this_sheet_index == 0) {
						// set column headings
						$this_phe_sheet->setCellValue('A1', 'Value');
						$this_phe_sheet->setCellValue('B1', 'Notes');
						$this_phe_sheet->setCellValue('C1', 'Yellow');
						$this_phe_sheet->setCellValue('D1', 'Red');
						$this_phe_sheet->setCellValue('E1', 'Green');
						
						$this_phe_sheet->getStyle('A1:E1')->getFont()->setBold(true);
						$this_phe_sheet->getStyle('A1:E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$this_phe_sheet->getStyle('A1:B1')->getFill()->getStartColor()->setARGB('FFCCCCCC');
						$this_phe_sheet->getStyle('C1')->getFill()->getStartColor()->setARGB('FFFFFF00');
						$this_phe_sheet->getStyle('D1')->getFill()->getStartColor()->setARGB('FFFF0000');
						$this_phe_sheet->getStyle('E1')->getFill()->getStartColor()->setARGB('FF008000');
						
						// process data rows
						foreach ($codes as $test_code => $result_values) {
							$this_phe_sheet->setCellValue('A'.trim($this_row_cursor), trim($test_code));
							$this_row_cursor++;
						}
					} elseif ($this_sheet_index == 1) {
						// set column headings
						$this_phe_sheet->setCellValue('A1', 'Test Codes');
						$this_phe_sheet->setCellValue('B1', 'Result Values');
						$this_phe_sheet->setCellValue('C1', 'Result Value Counts');
						
						$this_phe_sheet->getStyle('A1:C1')->getFont()->setBold(true);
						$this_phe_sheet->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$this_phe_sheet->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('FFCCCCCC');
						
						// process data rows
						foreach ($codes as $test_code => $result_values) {
							sort($result_values);
							foreach ($result_values as $result_value) {
								$this_phe_sheet->setCellValue('A'.trim($this_row_cursor), trim($test_code));
								$this_phe_sheet->setCellValue('B'.trim($this_row_cursor), trim($result_value));
								$this_phe_sheet->setCellValue('C'.trim($this_row_cursor), trim($result_codes[trim($test_code)][trim($result_value)]));
								$this_row_cursor++;
							}
						}
					} elseif ($this_sheet_index == 2) {
						// set column headings
						$this_phe_sheet->setCellValue('A1', 'OBR LOINC Code');
						$this_phe_sheet->setCellValue('B1', 'OBR Test Name');
						$this_phe_sheet->setCellValue('C1', 'OBR Local Code');
						$this_phe_sheet->setCellValue('D1', 'OBR Local Test Name');
						
						$this_phe_sheet->getStyle('A1:D1')->getFont()->setBold(true);
						$this_phe_sheet->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$this_phe_sheet->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('FFCCCCCC');
						
						// process data rows
						foreach ($obr_codes as $obr_test_code => $obr_result_values) {
							unset($this_obr_code_arr);
							$this_obr_code_arr = explode('^', $obr_test_code);
							$this_phe_sheet->setCellValue('A'.trim($this_row_cursor), trim($this_obr_code_arr[0]));
							$this_phe_sheet->setCellValue('B'.trim($this_row_cursor), trim($this_obr_code_arr[1]));
							$this_phe_sheet->setCellValue('C'.trim($this_row_cursor), trim($this_obr_code_arr[3]));
							$this_phe_sheet->setCellValue('D'.trim($this_row_cursor), trim($this_obr_code_arr[4]));
							$this_row_cursor++;
						}
					} elseif ($this_sheet_index == 3) {
						$this_phe_sheet->setCellValue('A1', 'Number of Uses');
						$this_phe_sheet->setCellValue('B1', 'Result Code');
						
						$this_phe_sheet->getStyle('A1:B1')->getFont()->setBold(true);
						$this_phe_sheet->getStyle('A1:B1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$this_phe_sheet->getStyle('A1:B1')->getFill()->getStartColor()->setARGB('FFCCCCCC');
						
						// process data rows
						foreach ($result_codes as $result_code => $result_code_count) {
							$this_phe_sheet->setCellValue('A'.trim($this_row_cursor), trim($result_code_count));
							$this_phe_sheet->getStyle('A'.trim($this_row_cursor))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
							$this_phe_sheet->setCellValue('B'.trim($this_row_cursor), trim($result_code));
							$this_row_cursor++;
						}
						
						
						$this_phe_sheet->getColumnDimension("A")->setAutoSize(true);
						$this_phe_sheet->getColumnDimension("B")->setAutoSize(true);
					}
				}
				unset($codes);
				
				// save our workbook
				ob_clean();
				$this_filename = $clean['connector'].'_hl7_review_'.date("YmdHis", time()).'.xls';
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename='.$this_filename);
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				
				// move back to first sheet for opening
				$this_phe->setActiveSheetIndex(0);
				$this_pheWriter = new PHPExcel_Writer_Excel5($this_phe);
				$this_pheWriter->save('php://output');
				
				// clear the PHPExcel objects from memory
				// important to disconnect worksheets to prevent memory leak
				unset($this_pheWriter);
				$this_phe->disconnectWorksheets();
				unset($this_phe);
				exit;
			}
			
			unset($this_filename);
			
			#debug echo 'mem used: '.memory_get_peak_usage();
			#debug echo '<br>time: '.elapsed_time($time_start);
			
			/* DEBUG
			echo '<pre>';
			print_r($columns);
			echo '<hr style="background-color: purple; width: 100%; height: 20px;" noshade>';
			print_r($messages);
			echo '<hr style="background-color: purple; width: 100%; height: 20px;" noshade>';
			print_r($codes);
			echo '</pre>';
			*/
			
		} else {
			suicide('Missing connector or date range.', -1, 1);
		}
		ini_set('max_execution_time', $old_max_time);
	} else {
?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrxml"></span>Generate HL7 Message Review Spreadsheet</h1>

<div id="addhl7form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Generate HL7 Message Review</label><br><br></div>
	<form id="new_onboard_form" method="POST" target="_blank" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&cat=<?php echo intval($cat); ?>">
		<label class="vocab_search_form2" for="mode">Generate: </label><select class="ui-corner-all" name="mode" id="mode">
			<option value="1" selected>Test Codes/Results (.xls)</option>
			<option value="2">Messages (.csv)</option>
		</select>
		<label class="vocab_search_form2" for="connector">Lab / Connector: </label><select class="ui-corner-all" name="connector" id="connector">
			<option value="">--</option>
		<?php
			$c_sql = 'SELECT DISTINCT connector FROM '.$my_db_schema.'system_original_messages ORDER BY connector;';
			$c_rs = @pg_query($host_pa, $c_sql);
			if ($c_rs !== false) {
				while ($c_row = @pg_fetch_object($c_rs)) {
					echo '<option value="'.addslashes(trim($c_row->connector)).'">'.htmlentities(trim($c_row->connector), ENT_QUOTES, 'UTF-8').'</option>'.PHP_EOL;
				}
			}
		?>
		</select>
		<label class="vocab_search_form2">Date Range:</label> <input class="date-range ui-corner-all" type="text" name="date_from" id="date_from" value="<?php echo ((isset($clean['date_from'])) ? $clean['date_from'] : ""); ?>" placeholder="Any Time"> <label class="vocab_search_form2">to</label> <input class="date-range ui-corner-all" type="text" name="date_to" id="date_to" value="<?php echo ((isset($clean['date_to'])) ? $clean['date_to'] : ""); ?>" placeholder="Present">
		<input type="hidden" name="generate_flag" value="1" />
		<br><br><button type="submit" name="btn_save" id="btn_save">Generate</button>
	</form>
</div>

<?php
	}
?>