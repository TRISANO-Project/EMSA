<?php

	include WEBROOT_URL.'/includes/vocab_functions.php';
	include WEBROOT_URL.'/includes/classes/vocab_audit.php';

	/**
	 * check for session freshness since last update to session-stored model info
	 * (columns/filters are stored in session data, this hack gives us a way to force
	 * session data to be refreshed without forcing users to clear cookies if the data
	 * is updated mid-session, so that the current columns/filters are used)
	 */
	// set "freshness date" (manual by developer to force client updates)
	// $model_last_updated = strtotime("7:01 PM 9/20/2012");
	
	/**
	 * Change to use 'filemtime()' to dynamically get last modification date of file
	 * Much less hassle than having to manually set a 'freshness date' each edit
	 */
	$model_last_updated = filemtime("manage/vocab_valueset.php");
	
	// check "freshness date"...
	if (isset($_SESSION['valueset_model_fresh'])) {
		if ($_SESSION['valueset_model_fresh'] < $model_last_updated) {
			// old model data; unset vocab_valueset_params & set a new "freshness date"...
			unset($_SESSION["vocab_valueset_params"]);
			$_SESSION['valueset_model_fresh'] = time();
		}
	} else {
		// hack for sessions set before "freshness date" implemented
		unset($_SESSION["vocab_valueset_params"]);
		$_SESSION['valueset_model_fresh'] = time();
	}
	
	
	########## Session Prep ##########
	// switch vocab type based on 'vocab' querystring value
	$_SESSION["vocab_valueset_params"]["vocab"] = 8;
	if (isset($_GET["vocab"])) {
		switch (intval(trim($_GET["vocab"]))) {
			case 9:
				$_SESSION["vocab_valueset_params"]["vocab"] = 9;
				break;
			default:
				$_SESSION["vocab_valueset_params"]["vocab"] = 8;
				break;
		}
	}
		
	########## if edit_id passed, include editor ##########
	// this must happen after setting the session vocab value, else xref links between dependent items will fail
	if (isset($_GET['edit_id'])){
		include 'vocab_valueset_edit.php';
	}	
		
		########## Search/Filter Prep ##########
		// pre-build our vocab-specific search data...
		if (isset($_GET["q"])) {
			if ((trim($_GET["q"]) != "Enter search terms...") && (strlen(trim($_GET["q"])) > 0)) {
				$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_display"] = htmlentities(trim($_GET["q"]), ENT_QUOTES, "UTF-8");
				$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_sql"] = pg_escape_string(trim($_GET["q"]));
				if (!isset($_GET['f'])) {
					// search query found, but no filters selected
					// if any filters were previously SESSIONized, they've been deselected via the UI, so we'll clear them...
					unset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"]);
				}
			} else {
				// search field was empty/defaulted, so we'll destroy the saved search params...
				unset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_display"]);
				unset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_sql"]);
				// not only was search blank, but no filters selected, so clear them as well...
				unset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"]);
			}
		}
		
		// update SESSIONized filters or destroy them if no filters are selected...
		if (isset($_GET['f'])) {
			if (is_array($_GET['f'])) {
				$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"] = $_GET['f'];
			}
		}
		
		if (!isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]])) {
			// if no params exist for selected vocab, load default values...
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"] = DEFAULT_ROWS_PER_PAGE;
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"] = "ASC";
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"] = "category";
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"] = "Master Dictionary";
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["result_cols"] = array(
				"category" => "Category",
				"codeset" => "Value Set Code",
				"master_concept" => "Master Concept Name",
				"app_value" => "%s Value");
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["column_aliases"] = array(
				"category" => "sc.label",
				"codeset" => "mv.codeset",
				"master_concept" => "mv.concept",
				"app_value" => "m2a.coded_value");
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filter_cols"] = array(
				"category" => array(
					"fieldtable" => "structure_category sc",
					"itemval" => "sc.id",
					"itemlabel" => "sc.label",
					"fieldlabel" => "Category"),
				"codeset" => array(
					"fieldtable" => "vocab_master_vocab mv",
					"itemval" => "mv.codeset",
					"itemlabel" => NULL,
					"fieldlabel" => "Value Code Set"),
				"app" => array(
					"fieldtable" => "vocab_app a",
					"itemval" => "a.id",
					"itemlabel" => "a.app_name",
					"fieldlabel" => "Application"));
				
			switch (intval($_SESSION["vocab_valueset_params"]["vocab"])) {
				case 9:
					// child value set
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"] = DEFAULT_ROWS_PER_PAGE;
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"] = "ASC";
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"] = "Child Dictionary";
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"] = "category";
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["result_cols"] = array(
						"ui_name" => "Child Lab",
						"category" => "Master Category",
						"child_concept" => "Child Code",
						"master_concept" => "Master Concept Name");
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["column_aliases"] = array(
						"ui_name" => "l.ui_name",
						"category" => "sc.label",
						"master_concept" => "mv.concept",
						"child_concept" => "cv.concept");
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filter_cols"] = array(
						"category" => array(
							"fieldtable" => "structure_category sc",
							"itemval" => "sc.id",
							"itemlabel" => "sc.label",
							"fieldlabel" => "Master Category"),
						"lab" => array(
							"fieldtable" => "structure_labs l",
							"itemval" => "l.id",
							"itemlabel" => "l.ui_name",
							"fieldlabel" => "Child Lab"));
					break;
				default:
					// master value set
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"] = DEFAULT_ROWS_PER_PAGE;
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"] = "ASC";
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"] = "Master Dictionary";
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"] = "category";
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["result_cols"] = array(
						"category" => "Category",
						"codeset" => "Value Set Code",
						"master_concept" => "Master Concept Name",
						"app_value" => "%s Value");
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["column_aliases"] = array(
						"category" => "sc.label",
						"codeset" => "mv.codeset",
						"master_concept" => "mv.concept",
						"app_value" => "m2a.coded_value");
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filter_cols"] = array(
						"category" => array(
							"fieldtable" => "structure_category sc",
							"itemval" => "sc.id",
							"itemlabel" => "sc.label",
							"fieldlabel" => "Category"),
						"codeset" => array(
							"fieldtable" => "vocab_master_vocab mv",
							"itemval" => "mv.codeset",
							"itemlabel" => NULL,
							"fieldlabel" => "Value Code Set"),
						"app" => array(
							"fieldtable" => "vocab_app a",
							"itemval" => "a.id",
							"itemlabel" => "a.app_name",
							"fieldlabel" => "Application"));
					break;
			}
		}
		
		
		// check if app & lab are specified via filters... if not, default to 1 for each...
		switch (intval($_SESSION["vocab_valueset_params"]["vocab"])) {
			case 9:
				if (!isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"]["lab"])) {
					//$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"]["lab"][] = 1;
				}
				break;
			default:
				if (!isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"]["app"])) {
					$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"]["app"][] = 1;
				}
				break;
		
		}
		
		
		// set up our SELECT statement and list of approved columns for the selected value set type
		if ($_SESSION["vocab_valueset_params"]["vocab"] == 8) {
			// master value set
			$valid_cols = array("category","codeset","master_concept","app_value");
			$select_stmt = "SELECT mv.id AS id, sc.label AS category, mv.codeset AS codeset, mv.concept AS master_concept, m2a.coded_value AS app_value";
			$paging_select_stmt = "SELECT count(mv.id) AS counter";
			$from_stmt = 'FROM '.$my_db_schema.'vocab_master_vocab mv LEFT JOIN '.$my_db_schema.'vocab_master2app m2a ON (mv.id = m2a.master_id) LEFT JOIN '.$my_db_schema.'vocab_app a ON (m2a.app_id = a.id AND a.id IN ('.implode(', ', $_SESSION['vocab_valueset_params'][$_SESSION['vocab_valueset_params']['vocab']]['filters']['app']).')) LEFT JOIN '.$my_db_schema.'structure_category sc ON (mv.category = sc.id)';
		} elseif ($_SESSION["vocab_valueset_params"]["vocab"] == 9) {
			// child value set
			$valid_cols = array("ui_name","category","master_concept","child_concept");
			$select_stmt = "SELECT cv.id AS id, l.ui_name AS ui_name, sc.label AS category, mv.concept AS master_concept, cv.concept AS child_concept";
			$paging_select_stmt = "SELECT count(cv.id) AS counter";
			$from_stmt = sprintf("FROM %svocab_child_vocab cv JOIN %sstructure_labs l ON (cv.lab_id = l.id) JOIN %svocab_master_vocab mv ON (cv.master_id = mv.id) LEFT JOIN %sstructure_category sc ON (mv.category = sc.id)", $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
		}
		
		// sort out our sorting...
		if (isset($_GET["order"])) {
			$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"] = ($_GET["order"] == "2") ? "DESC" : "ASC";
		}
		
		// ensure requested sort column is valid
		if (isset($_GET["sort"]) && is_array($valid_cols)) {
			if (in_array(strtolower(trim($_GET["sort"])), $valid_cols)) {
				$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"] = strtolower(trim($_GET["sort"]));
			}
		}
		
		$where_stmt = "WHERE";
		
		$where_count = 0;
		// handle any search terms or filters...
		if (isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_sql"])) {
			// we've got some query params
			$where_count = 1;
			$where_stmt .= " (";
			
			foreach ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["result_cols"] as $searchcol => $searchcolname) {
				if ($where_count > 1) {
					$where_stmt .= " OR ";
				}
				$where_stmt .= sprintf("%s ILIKE '%%%s%%'", $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["column_aliases"][$searchcol], $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_sql"]);
				$where_count++;
			}
			
			$where_stmt .= ")";
		}
		
		if (isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"])) {
			// need to apply filters
			$filter_count = 0;
			if ($where_count == 0) {
				// not already a WHERE clause for search terms
				$where_stmt .= " ";
			}
			
			foreach ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"] as $sqlfiltercol => $sqlfiltervals) {
				if ($sqlfiltercol != 'app') {
					unset($filter_temp);
					$nullfilter = FALSE;
					if (($filter_count > 0) || ($where_count > 1)) {
						$where_stmt .= " AND (";
					} else {
						$where_stmt .= " (";
					}
					foreach ($sqlfiltervals as $sqlfilterval) {
						if (is_null($sqlfilterval) || (strlen(trim($sqlfilterval)) == 0)) {
							$nullfilter = TRUE;
						} else {
							$filter_temp[] = "'" . pg_escape_string($sqlfilterval) . "'";
						}
					}
					
					if ($nullfilter && is_array($filter_temp)) {
						$where_stmt .= $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filter_cols"][$sqlfiltercol]["itemval"] . " IN (" . implode(",", $filter_temp) . ") OR " . $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["column_aliases"][$sqlfiltercol] . " IS NULL OR " . $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["column_aliases"][$sqlfiltercol] . " = ''";
					} elseif (is_array($filter_temp)) {
						$where_stmt .= $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filter_cols"][$sqlfiltercol]["itemval"] . " IN (" . implode(",", $filter_temp) . ")";
					} elseif ($nullfilter) {
						$where_stmt .= $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filter_cols"][$sqlfiltercol]["itemval"] . " IS NULL OR " . $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["column_aliases"][$sqlfiltercol] . " = ''";
					}
					
					$where_stmt .= ")";
					$filter_count++;
				}
			}
		}
		
		if (trim($where_stmt) == 'WHERE') {
			// if no filters got added, clear 'WHERE' statement
			$where_stmt = '';
		}
		
		
		// finish up our 'ORDER BY' clause now that we have all of our 'WHERE' stuff figured out...
		switch (intval($_SESSION["vocab_valueset_params"]["vocab"])) {
			case 9:
				// child dictionary; sort by lab first, then specified sort column
                $order_stmt = sprintf("ORDER BY ui_name ASC");
                $sort_col = $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"];
                $sort_order = $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"];
                if(!empty($sort_col) || !empty($sort_order)) {
				    $order_stmt = sprintf("ORDER BY ui_name ASC, %s %s", $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"], $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"]);
                }    
				break;
			default:
				$order_stmt = sprintf("ORDER BY %s %s", $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"], $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"]);
		}
		
?>

<script>
	$(function() {
		$(".colheader_sort_down").button({
			icons: { secondary: "ui-icon-circle-arrow-s" }
		});
	
		$(".colheader_sort_up").button({
			icons: { secondary: "ui-icon-circle-arrow-n" }
		});
	
		$("#latestTasks tbody tr").addClass("all_row");
		//$("#latestTasks tbody tr:even").addClass("even_row");
		//$("#latestTasks tbody tr:odd").addClass("odd_row");
		
		$("#toggle_filters").button({
            icons: {
                secondary: "ui-icon-triangle-1-n"
            }
        }).click(function() {
			$(".vocab_filter").toggle("blind");
			var objIcons = $(this).button("option", "icons");
			if (objIcons['secondary'] == "ui-icon-triangle-1-s") {
				$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-n"});
				$(this).button("option", "label", "Hide Filters");
				$("#addnew_form").hide();
				$("#addnew_button").show();
			} else {
				$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
				$(this).button("option", "label", "Show Filters");
			}
		});
		
		$(".vocab_filter").hide();
		$("#toggle_filters").button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
		$("#toggle_filters").button("option", "label", "Show Filters");
		
		$("#clear_filters").button({
            icons: {
                primary: "ui-icon-elrcancel"
            }
        }).click(function() {
			$(".pseudo_select").removeAttr("checked");
			$(".pseudo_select_label").removeClass("pseudo_select_on");
			$("#search_form")[0].reset();
			$("#q").val("").blur();
			$("#search_form").submit();
		});
		
		$("#q_go").button({
			icons: {
                primary: "ui-icon-elrsearch"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
		$("#apply_filters").button({
			icons: {
                primary: "ui-icon-elroptions"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
		$("#q").addClass("search_empty").val("Enter search terms...").click(function() {
			var search_val = $.trim($("#q").val());
			if (search_val == "Enter search terms...") {
				$(this).removeClass("search_empty").val("");
			}
		}).blur(function() {
			var search_val_ln = $.trim($("#q").val()).length;
			if (search_val_ln == 0) {
				$("#q").addClass("search_empty").val("Enter search terms...");
			}
		});
		
		<?php
			if (isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_display"])) {
				if ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_display"] != "Enter search terms...") {
		?>
		$("#q").removeClass("search_empty").val("<?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["q_display"]; ?>");
		<?php
				}
			}
		?>
		
		$("#latestTasks tbody tr").hover(function() {
			$(this).addClass("vocab_row_hover");
		}, function() {
			$(this).removeClass("vocab_row_hover");
		});
		
		$("#user_rows").change(function() {
			$("#user_rowselect").submit();
		});
		
		$(".pseudo_select").change(function() {
			$(this).closest('label').toggleClass('pseudo_select_on');
		});
		
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_category").focus();
			$(this).hide();
		});
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_savevocab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$(".edit_vocab").button({
				icons: { primary: "ui-icon-elrpencil" }
			}).next().button({
				icons: { primary: "ui-icon-elrclose" }
			}).parent().buttonset();
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false,
			width: 400
		});
		
		$(".delete_vocab").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo $vocab; ?>&delete_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$(".edit_vocab").click(function(e) {
			e.preventDefault();
			var editAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo $vocab; ?>&edit_id="+$(this).val();
			window.location.href = editAction;
		});
		
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrglossary"></span><?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?> Manager</h1>

<?php

		if (isset($_GET['edit_id'])) {
			// edit code...
		} elseif (isset($_GET['delete_id'])) {
			// delete if valid-looking ID
			$valid_delete = FALSE;
			if (intval(trim($_GET['delete_id'])) > 0) {
				// delete code here
				$valid_delete = TRUE;
				$validdelete_sql = sprintf("SELECT count(id) AS id FROM %s%s WHERE id = %d", $my_db_schema, (($vocab != 9) ? "vocab_master_vocab" : "vocab_child_vocab"), intval(trim($_GET['delete_id'])));
				$validdelete_count = @pg_fetch_result(@pg_query($host_pa, $validdelete_sql), 0, id);
				$valid_delete = ($validdelete_count == 1) ? TRUE : FALSE;
				
				// dependency check
				if($valid_delete) {
					if ($vocab != 9) {
						// master
						// check for child value set dependents first
						$valid_delete = FALSE;
						$dependency_sql = sprintf("SELECT count(id) AS id FROM %svocab_child_vocab WHERE master_id = %d", $my_db_schema, intval(trim($_GET['delete_id'])));
						$dependency_count = @pg_fetch_result(@pg_query($host_pa, $dependency_sql), 0, id);
						if ($dependency_count > 0) {
							suicide(sprintf("Warning:  %d Child Vocabulary items refer to this Master Vocabulary item.  Please make sure no Child Vocabulary references this Master value and try again.", $dependency_count));
						} else {
							// if no child vocab dependents, check for master vocab that links to this ID
							$dependency_sql = sprintf("SELECT (
								(SELECT count(c_id) FROM %svocab_master_condition WHERE condition = %d) + 
								(SELECT count(o_id) FROM %svocab_master_organism WHERE (organism = %d) OR (status = %d)) + 
								(SELECT count(l_id) FROM %svocab_master_loinc WHERE (trisano_test_type = %d) OR (status = %d))
								) AS dependents;", 
								$my_db_schema, intval(trim($_GET['delete_id'])), 
								$my_db_schema, intval(trim($_GET['delete_id'])), intval(trim($_GET['delete_id'])), 
								$my_db_schema, intval(trim($_GET['delete_id'])), intval(trim($_GET['delete_id']))
								);
							$dependency_count = @pg_fetch_result(@pg_query($host_pa, $dependency_sql), 0, dependents);
							if ($dependency_count > 0) {
								suicide(sprintf("Warning:  %d Master LOINC/Condition/Organism items refer to this Master Vocabulary item.  Please make sure no Master Vocabulary references this Master value and try again.", $dependency_count));
							} else {
								$valid_delete = TRUE;
							}
						}
						
						if ($valid_delete) {
							$va = new VocabAudit();
							
							$deleted_mv_rows = array('id' => intval(trim($_GET['delete_id'])), 'old' => $va->getPreviousVals(intval(trim($_GET['delete_id'])), VocabAudit::TABLE_MASTER_VOCAB));
							$deleted_m2a_rows = array();
							$m2a_pre_delete_sql = 'SELECT id FROM '.$my_db_schema.'vocab_master2app WHERE master_id = '.intval(trim($_GET['delete_id'])).';';
							$m2a_pre_delete_rs = @pg_query($host_pa, $m2a_pre_delete_sql);
							if ($m2a_pre_delete_rs !== false) {
								while ($m2a_pre_delete_row = @pg_fetch_object($m2a_pre_delete_rs)) {
									$deleted_m2a_rows[] = array('id' => intval($m2a_pre_delete_row->id), 'old' => $va->getPreviousVals(intval(trim($_GET['delete_id'])), VocabAudit::TABLE_MASTER_TO_APP));
								}
							}
							@pg_free_result($m2a_pre_delete_rs);
							
							// execute delete
							// wrap in a transaction to make sure that we don't accidentally wipe out app-specific values and then abort on the master vocab delete
							$deletevocab_sql = "BEGIN;";
							$deletevocab_sql .= sprintf("DELETE FROM ONLY %svocab_master2app WHERE master_id = %d;", $my_db_schema, intval(trim($_GET['delete_id'])));
							$deletevocab_sql .= sprintf("DELETE FROM ONLY %svocab_master_vocab WHERE id = %d;", $my_db_schema, intval(trim($_GET['delete_id'])));
							$deletevocab_sql .= "COMMIT;";
							
							if (@pg_query($host_pa, $deletevocab_sql)) {
								highlight("Vocabulary successfully deleted!", "ui-icon-check");
								
								$va->resetAudit();
								$va->setOldVals($deleted_mv_rows['old']);
								$va->auditVocab(intval(trim($_GET['delete_id'])), VocabAudit::TABLE_MASTER_VOCAB, VocabAudit::ACTION_DELETE);
								
								foreach ($deleted_m2a_rows as $deleted_m2a_row => $deleted_m2a_row_data) {
									$va->resetAudit();
									$va->setOldVals($deleted_m2a_row_data['old']);
									$va->auditVocab(intval(trim($_GET['delete_id'])), VocabAudit::TABLE_MASTER_TO_APP, VocabAudit::ACTION_DELETE);
								}
							} else {
								suicide("A database error occurred while attempting to delete this vocabulary item.", 1);
							}
						}
					} else {
						// child
						// check for child value set dependents first
						$valid_delete = FALSE;
						$valid_delete = TRUE;
						
						if ($valid_delete) {
							$va = new VocabAudit();
							
							$prev_vals = $va->getPreviousVals(intval(trim($_GET['delete_id'])), VocabAudit::TABLE_CHILD_VOCAB);
							
							// execute delete
							$deletevocab_sql = sprintf("DELETE FROM ONLY %svocab_child_vocab WHERE id = %d;", $my_db_schema, intval(trim($_GET['delete_id'])));
							
							if (@pg_query($host_pa, $deletevocab_sql)) {
								highlight("Vocabulary successfully deleted!", "ui-icon-check");
								
								$va->resetAudit();
								$va->setOldVals($prev_vals);
								$va->auditVocab(intval(trim($_GET['delete_id'])), VocabAudit::TABLE_CHILD_VOCAB, VocabAudit::ACTION_DELETE);
							} else {
								suicide("A database error occurred while attempting to delete this vocabulary item.", 1);
							}
						}
					}
				
				} else {
					// valid integer passed, but no matching record found
					suicide("Cannot delete vocabulary:  Record not found");
				}
			} else {
				// not a valid integer, don't even try looking for it
				suicide("Cannot delete vocabulary:  Record not found");
			}
		} elseif (isset($_GET['add_flag'])) {
			$va = new VocabAudit();
			if ($vocab != 9) {
				// add new master vocab item
				$valid_add = FALSE;
				$valid_valueset = isset($_GET['new_codeset']);
				$valid_concept = ((isset($_GET['new_masterconcept'])) && (ctype_print(trim($_GET['new_masterconcept']))) && (strlen(trim($_GET['new_masterconcept'])) > 0));
				$valid_category = ((isset($_GET['new_category'])) && (is_numeric(trim($_GET['new_category']))) && (intval(trim($_GET['new_category'])) > 0));
				$valid_appid = ((isset($_GET['new_appconcept'])) && (is_array($_GET['new_appconcept'])));
				$valid_add = $valid_valueset && $valid_concept && $valid_category && $valid_appid;
				if ($valid_add) {
					// check to make sure all app values passed correspond to actual configured applications
					unset($new_apps);
					foreach ($_GET['new_appconcept'] as $new_appid => $new_appvalue) {
						$appvalues_sql = sprintf("SELECT id, app_name FROM %svocab_app WHERE id = %d", $my_db_schema, intval($new_appid));
						if ($appvalues_row = @pg_fetch_object(@pg_query($host_pa, $appvalues_sql))) {
							$new_apps[$appvalues_row->id] = array("app_name" => $appvalues_row->app_name, "app_value" => $new_appvalue);
						}
					}
					if (isset($new_apps)) {
						// insert master values & get new master_id
						$insertmaster_sql = sprintf("INSERT INTO %svocab_master_vocab (category, codeset, concept) VALUES (%s, %s, '%s'); 
							SELECT Currval('%svocab_master_vocab_id_seq') AS last_master_id LIMIT 1;", 
							$my_db_schema,
							((intval(trim($_GET['new_category'])) > 0) ? intval(trim($_GET['new_category'])) : "NULL"),
							((strlen(trim($_GET['new_codeset'])) > 0) ? "'".pg_escape_string(trim($_GET['new_codeset']))."'" : "NULL"),
							pg_escape_string(trim($_GET['new_masterconcept'])),
							$my_db_schema);
						$insertmaster_rs = @pg_query($host_pa, $insertmaster_sql);
						if ($insertmaster_row = @pg_fetch_object($insertmaster_rs)) {
							highlight("Master values added!", "ui-icon-check");
							
							$this_masterid = $insertmaster_row->last_master_id;
							
							$va->resetAudit();
							$va->setNewVals($va->prepareNewValues(VocabAudit::TABLE_MASTER_VOCAB, array('category' => intval(trim($_GET['new_category'])), 'valueset' => trim($_GET['new_codeset']), 'masterconcept' => trim($_GET['new_masterconcept']))));
							$va->auditVocab(intval($this_masterid), VocabAudit::TABLE_MASTER_VOCAB, VocabAudit::ACTION_ADD);
							
							foreach ($new_apps as $this_app_id => $this_app_data) {
								// insert app-specific values
								$insertappvalue_sql = sprintf("INSERT INTO %svocab_master2app (app_id, master_id, coded_value) VALUES (%d, %d, '%s') RETURNING id;", 
									$my_db_schema,
									intval($this_app_id),
									intval($this_masterid),
									pg_escape_string(trim($this_app_data['app_value'])));
								if (@pg_query($host_pa, $insertappvalue_sql)) {
									highlight(sprintf("%s value added!", $this_app_data['app_name']), "ui-icon-check");
									
									$va->resetAudit();
									$va->setNewVals($va->prepareNewValues(VocabAudit::TABLE_MASTER_TO_APP, array('app_id' => intval($this_app_id), 'appvalue' => trim($this_app_data['app_value']))));
									$va->auditVocab(intval($this_masterid), VocabAudit::TABLE_MASTER_TO_APP, VocabAudit::ACTION_ADD);
								} else {
									suicide(sprintf("Could not insert value for %s", $this_app_data['app_name']), 1);
								}
							}
						} else {
							suicide("Could not add new Master Vocabulary", 1);
						}
					} else {
						suicide("Could not add new Master Vocabulary:  Application(s) not found");
					}
				} else {
					suicide("Could not add new Master Vocabulary");
				}
			} else {
				// add new child vocab item
				$valid_add = FALSE;
				$valid_childcode = ((isset($_GET['new_childconcept'])) && (ctype_print(trim($_GET['new_childconcept']))) && (strlen(trim($_GET['new_childconcept'])) > 0));
				$valid_labid = ((isset($_GET['new_child'])) && (ctype_digit(trim($_GET['new_child']))));
				$valid_masterid = ((isset($_GET['new_masterid'])) && (ctype_digit(trim($_GET['new_masterid']))));
				if ($valid_labid) {
					$validlab_sql = sprintf("SELECT count(id) AS id FROM %sstructure_labs WHERE id = %d;", $my_db_schema, intval(trim($_GET['new_child'])));
					$validlab_count = @pg_fetch_result(@pg_query($host_pa, $validlab_sql), 0, id);
					$valid_labid = ($validlab_count == 1) ? TRUE : FALSE;
				}
				if ($valid_masterid) {
					$validmid_sql = sprintf("SELECT count(id) AS id FROM %svocab_master_vocab WHERE id = %d;", $my_db_schema, intval(trim($_GET['new_masterid'])));
					$validmid_count = @pg_fetch_result(@pg_query($host_pa, $validmid_sql), 0, id);
					$valid_masterid = ($validmid_count == 1) ? TRUE : FALSE;
				}
				$valid_add = $valid_childcode && $valid_labid && $valid_masterid;
				if ($valid_add) {
					// insert code here
					$insertvocab_sql = sprintf("INSERT INTO %svocab_child_vocab (lab_id, master_id, concept) VALUES (%d, %d, '%s') RETURNING id;", 
						$my_db_schema,
						intval(trim($_GET['new_child'])),
						intval(trim($_GET['new_masterid'])),
						pg_escape_string(trim($_GET['new_childconcept'])));
					//echo $insertvocab_sql;
					$insertvocab_rs = @pg_query($host_pa, $insertvocab_sql);
					if ($insertvocab_rs !== false) {
						$insertvocab_id = intval(@pg_fetch_result($insertvocab_rs, 0, 0));
						highlight("New Child Vocabulary added successfully!", "ui-icon-check");
						
						$va->resetAudit();
						$va->setNewVals($va->prepareNewValues(VocabAudit::TABLE_CHILD_VOCAB, array('lab_id' => intval(trim($_GET['new_child'])), 'master_id' => intval(trim($_GET['new_masterid'])), 'child_code' => trim($_GET['new_childconcept']))));
						$va->auditVocab($insertvocab_id, VocabAudit::TABLE_CHILD_VOCAB, VocabAudit::ACTION_ADD);
					} else {
						suicide("Could not add new Child Vocabulary", 1);
					}
				} else {
					suicide("Could not add new Child Vocabulary");
				}
			}
		}
	
		// -- Result Pagination --
		// find out how many results there will be...
		$rowcount_sql = sprintf("%s %s %s;", $paging_select_stmt, $from_stmt, $where_stmt);
		//echo $rowcount_sql."<br><br>";
		$counter_arr=pg_fetch_array(pg_query($host_pa,$rowcount_sql)) or suicide("Unable to calculate number of rows returned.", 1);
		$numrows=$counter_arr['counter'];
		if (intval($_SESSION["vocab_valueset_params"]["vocab"]) != 9) {
			$appname_sql = sprintf("SELECT a.app_name AS app_name FROM %svocab_app a WHERE a.id = %s;", $my_db_schema, intval($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"]["app"][0]));
			$appname_arr=pg_fetch_array(pg_query($host_pa,$appname_sql)) or suicide("Unable to retrieve Application Name.", 1);
			$app_name=$appname_arr['app_name'];
		}
		
		// number of rows to show per page
		$valid_rowsize = array(50, 100, 250, 500, 1000, -1);
		if(isset($_GET['user_rows'])){
			if (in_array(intval(trim($_GET['user_rows'])) , $valid_rowsize)) {
				$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"] = intval(trim($_GET['user_rows']));
			}
		}
		
		// find out total pages
		if ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"] > 0) {
			$totalpages = ceil($numrows / $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"]);
			
			// get the current page or set a default
			if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
				$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["current_page"] = intval($_GET['currentpage']);
			}
			
			if (isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["current_page"])) {
				$currentpage = $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["current_page"];
			} else {
				$currentpage = 1;
			}
			
			// if current page is greater than total pages...
			if ($currentpage > $totalpages) {
				// set current page to last page
				$currentpage = $totalpages;
			}
			
			// if current page is less than first page...
			if ($currentpage < 1) {
				// set current page to first page
				$currentpage = 1;
			}
			
			// the offset of the list, based on current page
			$offset = ($currentpage - 1) * $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"];
		} else {
			$totalpages = 1;
			$currentpage = 1;
		}
		
	
?>

<form name="search_form" id="search_form" method="GET" action="<?php echo $main_url; ?>">

<div class="vocab_search ui-tabs ui-widget">
	<label for="q" class="vocab_search_form">Search:</label><br><input type="text" name="q" id="q" class="vocab_query ui-corner-all">
	<button name="q_go" id="q_go" title="Search">Search</button>
	<button type="button" name="clear_filters" id="clear_filters" title="Clear all filters/search terms">Reset</button>
	<button type="button" name="toggle_filters" id="toggle_filters" title="Show/hide filters">Hide Filters</button>
	<button id="addnew_button" type="button" title="Add a new '<?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?>' record">Add new <?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?></button>
	
</div>

<?php
	############### If filters applied, display which ones ###############
	if (isset($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"])) {
?>
<div class="vocab_search ui-widget ui-widget-content ui-state-highlight ui-corner-all" style="padding: 5px;">
	<span class="ui-icon ui-icon-elroptions" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;">Active Filters:
<?php
		$active_filters = 0;
		foreach ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filters"] as $sqlfiltercol => $sqlfiltervals) {
			if ($active_filters == 0) {
				echo '<strong>' . $_SESSION['vocab_valueset_params'][$_SESSION['vocab_valueset_params']['vocab']]['filter_cols'][$sqlfiltercol]['fieldlabel'] . '</strong> <em style="color: darkgray;">('.verboseFilterValues($_SESSION['vocab_valueset_params'][$_SESSION['vocab_valueset_params']['vocab']]['filter_cols'][$sqlfiltercol], $sqlfiltervals).')</em>';
			} else {
				echo ', <strong>' . $_SESSION['vocab_valueset_params'][$_SESSION['vocab_valueset_params']['vocab']]['filter_cols'][$sqlfiltercol]['fieldlabel'] . '</strong> <em style="color: darkgray;">('.verboseFilterValues($_SESSION['vocab_valueset_params'][$_SESSION['vocab_valueset_params']['vocab']]['filter_cols'][$sqlfiltercol], $sqlfiltervals).')</em>';
			}
			$active_filters++;
		}
?>
	</p>
</div>
<?php
	}
?>

<div class="vocab_filter ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Filters:</label></div>
<?php
	foreach ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["filter_cols"] as $filtercol => $filtercolname) {
		echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">".$filtercolname["fieldlabel"]."</label><br>";
		echo "<div class=\"vocab_filter_checklist\">";
		
		if (is_null($filtercolname["itemlabel"])) {
			$filter_qry = sprintf("SELECT DISTINCT %s AS value, %s AS label FROM %s%s ORDER BY %s ASC;", 
				$filtercolname["itemval"], 
				$filtercolname["itemval"], 
				$my_db_schema, 
				$filtercolname["fieldtable"], 
				$filtercolname["itemval"]);
		} else {
			$filter_qry = sprintf("SELECT DISTINCT %s AS value, %s AS label FROM %s%s ORDER BY %s ASC;", 
				$filtercolname["itemval"], 
				$filtercolname["itemlabel"], 
				$my_db_schema, 
				$filtercolname["fieldtable"], 
				$filtercolname["itemlabel"]);
		}
		//echo $filter_qry;
		$filter_rs = @pg_query($host_pa, $filter_qry);
		$value_iterator = 0;
		while ($filter_row = @pg_fetch_object($filter_rs)) {
			$value_iterator++;
			$this_filter_selected = FALSE;
			if (is_array($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]['filters'][$filtercol])) {
				if (in_array($filter_row->value, $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]['filters'][$filtercol])) {
					$this_filter_selected = TRUE;
				}
			}
			
			if ($this_filter_selected) {
				echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"".htmlentities($filter_row->value, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
			} else {
				echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"".htmlentities($filter_row->value, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
			}
		}
		@pg_free_result($filter_rs);
		echo "</div></div>";
	}
?>
	<br><br><button name="apply_filters" id="apply_filters" title="Apply Filters" style="clear: both; float: left; margin: 5px;">Apply Filters</button>
</div>

<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
<input type="hidden" name="submenu" value="<?php echo $submenu; ?>">
<input type="hidden" name="vocab" value="<?php echo (($vocab<2) ? 1 : $vocab); ?>">

</form>


<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New <?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?>:</label><br><br></div>
	<form id="new_vocab_form" method="GET" action="<?php echo $main_page; ?>">
	<?php
		if ($vocab != 9) {
			// Add New Master Vocab form
	?>
			<div class="addnew_field"><label class="vocab_add_form" for="new_category">Category:</label><br>
				<select class="ui-corner-all" name="new_category" id="new_category">
					<option value="-1" selected>--</option>
				<?php
					// get list of top-level labs for alias menu
					$newcategory_sql = sprintf("SELECT id, label FROM %sstructure_category ORDER BY label;", $my_db_schema);
					$newcategory_result = @pg_query($host_pa, $newcategory_sql) or suicide("Unable to retrieve list of Master Categories.", 1, 1);
					while ($newcategory_row = pg_fetch_object($newcategory_result)) {
						printf("<option value=\"%s\">%s</option>", intval($newcategory_row->id), htmlentities($newcategory_row->label, ENT_QUOTES, "UTF-8"));
					}
					pg_free_result($newcategory_result);
				?>
				</select>
			</div>
			<div class="addnew_field"><label class="vocab_add_form" for="new_codeset">Value Set Code:</label><br><input class="ui-corner-all" type="text" name="new_codeset" id="new_codeset" /></div>
			<div class="addnew_field"><label class="vocab_add_form" for="new_masterconcept">Master Concept Name:</label><br><input class="ui-corner-all" type="text" name="new_masterconcept" id="new_masterconcept" /></div>
			
		<?php
			// draw app-specific value input for each configured app
			$newapp_sql = sprintf("SELECT id, app_name FROM %svocab_app ORDER BY app_name;", $my_db_schema);
			$newapp_result = @pg_query($host_pa, $newapp_sql) or suicide("Unable to retrieve list of Applications.", 1, 1);
			while ($newapp_row = pg_fetch_object($newapp_result)) {
				echo "<div class=\"add-form-divider\"></div>";
				printf("<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_appconcept_%d\">%s Value:</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_appconcept[%d]\" id=\"new_appconcept_%d\" /></div>", 
					intval($newapp_row->id),
					htmlentities(trim($newapp_row->app_name), ENT_QUOTES, "UTF-8"),
					intval($newapp_row->id),
					intval($newapp_row->id));
			}
			pg_free_result($newapp_result);
			
		} else {
			// Add New Child Vocab form
	?>
			
			<div class="addnew_field"><label class="vocab_add_form" for="new_child">Lab:</label><br>
				<select class="ui-corner-all" name="new_child" id="new_child">
					<option value="-1" selected>--</option>
				<?php
					// get list of top-level labs for alias menu
					$newchild_sql = sprintf("SELECT id, ui_name FROM %sstructure_labs WHERE alias_for < 1 ORDER BY ui_name;", $my_db_schema);
					$newchild_result = @pg_query($host_pa, $newchild_sql) or suicide("Unable to retrieve list of Labs.", 1, 1);
					while ($newchild_row = pg_fetch_object($newchild_result)) {
						printf("<option value=\"%d\">%s</option>", intval($newchild_row->id), htmlentities($newchild_row->ui_name, ENT_QUOTES, "UTF-8"));
					}
					pg_free_result($newchild_result);
				?>
				</select>
			</div>
			<div class="add-form-divider"></div>
			<div class="addnew_field" style="vertical-align: top;"><label class="vocab_add_form" for="new_childconcept">Child Code:</label><br><input class="ui-corner-all" type="text" name="new_childconcept" id="new_childconcept" /></div>
			<div class="addnew_field"><label class="vocab_search_form2" for="new_mastercat">Master Category:</label>
				<select class="ui-corner-all" name="new_mastercat" id="new_mastercat">
					<option value="-1" selected>--</option>
				<?php
					// get unique categories for auto-populating master concept list
					$newmastercat_sql = sprintf("SELECT id, label FROM %sstructure_category ORDER BY label;", $my_db_schema);
					$newmastercat_result = @pg_query($host_pa, $newmastercat_sql) or suicide("Unable to retrieve Master Vocabulary.", 1, 1);
					while ($newmastercat_row = pg_fetch_object($newmastercat_result)) {
						printf("<option value=\"%s\">%s</option>", intval($newmastercat_row->id), htmlentities($newmastercat_row->label, ENT_QUOTES, "UTF-8"));
					}
					pg_free_result($newmastercat_result);
				?>
				</select>
			<br><span class="ui-icon ui-icon-arrowreturnthick-1-e" style="float: left; margin-left: 30px; margin-top: 3px;"></span><label class="vocab_search_form2" for="new_masterid">Master Concept Name:</label>
				<select class="ui-corner-all" name="new_masterid" id="new_masterid" style="width: auto; min-width: 100px;">
					<option value="-1" selected>--</option>
				<?php
					// get list of top-level labs for alias menu
					$newmasterid_sql = sprintf("SELECT id, category, concept FROM %svocab_master_vocab ORDER BY category, concept;", $my_db_schema);
					$newmasterid_result = @pg_query($host_pa, $newmasterid_sql) or suicide("Unable to retrieve Master Vocabulary.", 1, 1);
					//echo "<script type=\"text/javascript\">\nvar master_temp = [];\n";
					while ($newmasterid_row = pg_fetch_object($newmasterid_result)) {
						$temp_master[$newmasterid_row->category][intval($newmasterid_row->id)] = $newmasterid_row->concept;
					}
					pg_free_result($newmasterid_result);
				?>
				</select>
				
				<script type="text/javascript">
					var masterList = <?php echo json_encode($temp_master); ?>;
					
					$("#new_mastercat").change(function () {
						var selectedCat = $("#new_mastercat").val();
						
						$("#new_masterid").empty();
						if (selectedCat != -1) {
							$.each(masterList[selectedCat], function(id, label) {
								$("#new_masterid").append($("<option />").val(id).text(label));
							});
						}
						var master_id_list = $("#new_masterid option");
						
						// major kudos to the fine folks at http://stackoverflow.com/questions/45888/what-is-the-most-efficient-way-to-sort-an-html-selects-options-by-value-while for this elegant option list sorting solution!!!
						// p.s. -- Sortable arrays, but no associative keys.  Associative object properties, but object properties can't be sorted... really, JavaScript?  Thanks for the help.</sarcasm>
						master_id_list.sort(function(a,b) {
							if (a.text > b.text) return 1;
							else if (a.text < b.text) return -1;
							else return 0;
						});
						$("#new_masterid").empty();
						$("#new_masterid").append($("<option />").val(-1).text("--"));
						$("#new_masterid").append(master_id_list);
						$("#new_masterid").val(-1);
					});
				</script>
			</div>
	<?php
			
		}
	?>
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="vocab" value="<?php echo intval($vocab); ?>" />
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_savevocab" id="new_savevocab">Save New <?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?></button>
		<button type="button" name="addnew_cancel" id="addnew_cancel">Cancel</button>
	</form>
</div>


<div class="vocab_paging_top">
<?php
	if ($numrows > 0) {
		echo "Page: ";
		/******  build the pagination links ******/
		// range of num links to show
		$range = 3;

		// if not on page 1, don't show back links
		if ($currentpage > 1) {
		   // show << link to go back to page 1
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;&lt;</a> ", $main_url, "1", $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // get previous page num
		   $prevpage = $currentpage - 1;
		   // show < link to go back to 1 page
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;</a> ", $main_url, $prevpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if 

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
		   // if it's a valid page number...
		   if (($x > 0) && ($x <= $totalpages)) {
			  // if we're on current page...
			  if ($x == $currentpage) {
				 // 'highlight' it but don't make a link
				 printf(" [<b>%s</b>] ", $x);
			  // if not current page...
			  } else {
				 // make it a link
				 printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">%s</a> ", $main_url, $x, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $x);
			  } // end else
		   } // end if 
		} // end for
						 
		// if not on last page, show forward and last page links        
		if ($currentpage != $totalpages) {
		   // get next page
		   $nextpage = $currentpage + 1;
			// echo forward link for next page 
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;</a> ", $main_url, $nextpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // echo forward link for lastpage
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;&gt;</a> ", $main_url, $totalpages, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if
		/****** end build pagination links ******/
	}
?>

</div>

<table id="latestTasks" class="ui-corner-all">
	<caption>
		<?php
			if ($numrows < 1) {
				printf("No %s records found!", $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]);
			} elseif ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"] > 0) {
				printf("%d %s records found (Displaying %d - %d)", $numrows, $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"], intval($offset+1), ((intval($offset+$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"])>$numrows)?$numrows:intval($offset+$_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"])));
			} else {
				printf("%d %s records found (Displaying all)", $numrows, $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]);
			}
		?>
		<form name="user_rowselect" id="user_rowselect" method="GET" action="<?php echo $main_url; ?>" style="display: inline; margin-left: 30px;">
			<select name="user_rows" id="user_rows">
			<?php
				foreach ($valid_rowsize as $this_rowsize) {
					echo "<option value=\"" . $this_rowsize . "\"" . (($this_rowsize == $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"]) ? " selected" : "") . ">" . (($this_rowsize > 0) ? $this_rowsize : "All") . "</option>";
				}
			?>
			</select>
			 rows per page
			<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
			<input type="hidden" name="submenu" value="<?php echo $submenu; ?>">
			<input type="hidden" name="vocab" value="<?php echo (($vocab<2) ? 1 : $vocab); ?>">
		</form>
	</caption>
	<thead>
		<tr>
			<th>Actions</th>
<?php

		foreach ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["result_cols"] as $headercol => $headername) {
			$sort_indicator = "";
			$sort_text = sprintf("Sort by '%s' [A-Z]", $headername);
			if ($headercol == $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"]) {
				if ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"] == "ASC") {
					$sort_indicator = "colheader_sort_down";
					$sort_text = sprintf("Sort by '%s' [Z-A]", $headername);
				} else {
					$sort_indicator = "colheader_sort_up";
					$sort_text = sprintf("Sort by '%s' [A-Z]", $headername);
				}
			}
			if ($headercol == "app_value") {
				printf("<th><a class=\"colheader %s\" title=\"%s\" href=\"%s?selected_page=%s&submenu=%s&vocab=%s&sort=%s&order=%s&currentpage=1\">%s</a></th>", $sort_indicator, sprintf($sort_text, $app_name), $main_url, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $headercol, ((($headercol == $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"]) && ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"] == "ASC")) ? "2" : "1"), sprintf($headername, $app_name));
			} else {
				printf("<th><a class=\"colheader %s\" title=\"%s\" href=\"%s?selected_page=%s&submenu=%s&vocab=%s&sort=%s&order=%s&currentpage=1\">%s</a></th>", $sort_indicator, $sort_text, $main_url, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $headercol, ((($headercol == $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_col"]) && ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["sort_order"] == "ASC")) ? "2" : "1"), $headername);
			}
		}
		echo "</tr></thead><tbody>";

		// go grab our data...
		if ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"] > 0) {
			$getvocab_sql = sprintf("%s %s %s %s LIMIT %d OFFSET %d;", $select_stmt, $from_stmt, $where_stmt, $order_stmt, $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["page_rows"], $offset);
		} else {
			$getvocab_sql = sprintf("%s %s %s %s;", $select_stmt, $from_stmt, $where_stmt, $order_stmt);
		}
		//echo $getvocab_sql."<hr>";
		$getvocab_rs = pg_query($host_pa,$getvocab_sql) or die("Can't Perform Query ".pg_last_error());
		
		while ($getvocab_row = pg_fetch_object($getvocab_rs)) {
			echo "<tr><td nowrap>";
			//printf("<a href=\"%s?selected_page=%s&submenu=%s&vocab=%s&edit_id=%s\">Edit</a><br>", $main_url, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $getvocab_row->id);
			printf("<button class=\"edit_vocab\" type=\"button\" value=\"%s\" title=\"Edit this vocabulary\">Edit</button>", $getvocab_row->id);
			printf("<button class=\"delete_vocab\" type=\"button\" value=\"%s\" title=\"Delete this vocabulary\">Delete</button>", $getvocab_row->id);
			echo "</td>";
			
			foreach ($_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["result_cols"] as $colname => $colvalue) {
				printf("<td class=\"vocab_data_cell\">%s</td>", htmlentities(trim($getvocab_row->$colname), ENT_QUOTES, "UTF-8"));
			}
			echo "</tr>";
		}
		
?>
	</tbody>
</table>

<div class="vocab_paging vocab_paging_bottom">
<?php
	if ($numrows > 0) {
		echo "Page: ";
		/******  build the pagination links ******/
		// range of num links to show
		$range = 3;

		// if not on page 1, don't show back links
		if ($currentpage > 1) {
		   // show << link to go back to page 1
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;&lt;</a> ", $main_url, "1", $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // get previous page num
		   $prevpage = $currentpage - 1;
		   // show < link to go back to 1 page
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;</a> ", $main_url, $prevpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if 

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
		   // if it's a valid page number...
		   if (($x > 0) && ($x <= $totalpages)) {
			  // if we're on current page...
			  if ($x == $currentpage) {
				 // 'highlight' it but don't make a link
				 printf(" [<b>%s</b>] ", $x);
			  // if not current page...
			  } else {
				 // make it a link
				 printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">%s</a> ", $main_url, $x, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $x);
			  } // end else
		   } // end if 
		} // end for
						 
		// if not on last page, show forward and last page links        
		if ($currentpage != $totalpages) {
		   // get next page
		   $nextpage = $currentpage + 1;
			// echo forward link for next page 
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;</a> ", $main_url, $nextpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // echo forward link for lastpage
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;&gt;</a> ", $main_url, $totalpages, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if
		/****** end build pagination links ******/
	}
?>
</div>

<div id="confirm_delete_dialog" title="Delete this <?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?> record?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This <?php echo $_SESSION["vocab_valueset_params"][$_SESSION["vocab_valueset_params"]["vocab"]]["vocab_verbose"]; ?> will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>


<?php

	pg_free_result($getvocab_rs);
	
?>
		