<?php
	
	/* Child Vocab > Child LOINC */
	define('EXPORT_QRY_CHILD_HL7XML', 'SELECT sl.ui_name AS lab_name, spm.message_version AS message_version, spm.xpath AS hl7_xpath, sp.xpath AS master_xpath, 
			spm.glue_string AS glue_string, spm.sequence AS sequence 
		FROM '.$my_db_schema.'structure_path_mirth spm
		INNER JOIN '.$my_db_schema.'structure_labs sl ON (spm.lab_id = sl.id) 
		LEFT JOIN '.$my_db_schema.'structure_path sp ON (spm.master_path_id = sp.id) 
		WHERE spm.lab_id = %d  
		ORDER BY sl.ui_name, spm.message_version, sp.xpath, spm.sequence, spm.xpath;');
	
?>