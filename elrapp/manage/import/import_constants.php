<?php
	
	/* Master Value Set */
	define('EXPORT_QRY_MASTERVOCAB', 'SELECT c.label AS category, mv.codeset AS codeset, mv.concept AS concept, mv.last_updated AS last_updated
		FROM '.$my_db_schema.'vocab_master_vocab mv
		INNER JOIN '.$my_db_schema.'structure_category c ON (mv.category = c.id)
		ORDER BY c.label, mv.concept;');
		
	/* Application-Specific Value Set */
	define('EXPORT_QRY_APPVALUE', 'SELECT a.app_name AS app_name, c.label AS category, m2a.coded_value AS coded_value, mv.concept AS master_concept, m2a.last_updated AS last_updated
		FROM '.$my_db_schema.'vocab_master2app m2a
		INNER JOIN '.$my_db_schema.'vocab_app a ON (m2a.app_id = a.id)
		INNER JOIN '.$my_db_schema.'vocab_master_vocab mv ON (m2a.master_id = mv.id)
		INNER JOIN '.$my_db_schema.'structure_category c ON (mv.category = c.id)
		ORDER BY c.label, m2a.coded_value;');
		
	/* MasterLoinc > Master Condition */
	define('EXPORT_QRY_ML_CONDITION', 'SELECT mv.concept AS condition, mv2.concept AS disease_category, 
				array_to_string(
					array(
						SELECT mv_int.concept FROM '.$my_db_schema.'vocab_master_vocab mv_int WHERE mv_int.id IN (
							SELECT arr[i]::integer FROM (
								SELECT generate_series(1, array_upper(arr, 1)) AS i, arr 
								FROM (SELECT regexp_split_to_array(mc.gateway_xref, E\';\') AS arr ) t 
							) t
						)
					)
				, \'; \') AS gateway_xref, 
				(CASE WHEN mc.immediate_notify IS TRUE THEN \'Yes\' ELSE \'No\' END) AS immediate_notify, 
				(CASE WHEN mc.require_specimen IS TRUE THEN \'Yes\' ELSE \'No\' END) AS require_specimen, 
				array_to_string(
					array(
						SELECT mv_int.concept FROM '.$my_db_schema.'vocab_master_vocab mv_int WHERE mv_int.id IN (
							SELECT arr[i]::integer FROM (
								SELECT generate_series(1, array_upper(arr, 1)) AS i, arr 
								FROM (SELECT regexp_split_to_array(mc.valid_specimen, E\';\') AS arr ) t 
							) t
						)
					)
				, \'; \') AS valid_specimen, 
				array_to_string(
					array(
						SELECT mv_int.concept FROM '.$my_db_schema.'vocab_master_vocab mv_int WHERE mv_int.id IN (
							SELECT arr[i]::integer FROM (
								SELECT generate_series(1, array_upper(arr, 1)) AS i, arr 
								FROM (SELECT regexp_split_to_array(mc.invalid_specimen, E\';\') AS arr ) t 
							) t
						)
					)
				, \'; \') AS invalid_specimen, 
				mc.white_rule AS white_rule, mc.contact_white_rule AS contact_white_rule, 
				(CASE WHEN mc.new_event IS TRUE THEN \'New\' ELSE \'Same\' END) AS new_event, 
				(CASE WHEN mc.notify_state IS TRUE THEN \'Yes\' ELSE \'No\' END) AS notify_state, 
				(CASE WHEN mc.autoapproval IS TRUE THEN \'Yes\' ELSE \'No\' END) AS autoapproval, 
				(CASE WHEN mc.check_xref_first IS TRUE THEN \'Yes\' ELSE \'No\' END) AS check_xref_first, 
				sd.health_district AS district_override, 
				mc.last_updated AS last_updated
			FROM '.$my_db_schema.'vocab_master_condition mc
			INNER JOIN '.$my_db_schema.'vocab_master_vocab mv ON (mc.condition = mv.id) 
			INNER JOIN '.$my_db_schema.'vocab_master_vocab mv2 ON (mc.disease_category = mv2.id) 
			LEFT JOIN '.$my_db_schema.'system_districts sd ON (mc.district_override = sd.id) 
			ORDER BY mv.concept;');
		
		/* MasterLoinc > Master LOINC */
		define('EXPORT_QRY_ML_LOINC', 'SELECT ml.loinc AS loinc, ml.concept_name AS preferred_concept, mv_c.concept AS condition, mv_o.concept AS organism, mv_t.concept AS test_type, mv_s.concept AS status, 
				mv_ss.concept AS specimen_source, l.name AS list, ml.gray_rule AS gray_rule, 
				(CASE WHEN vrml.allow_new_cmr IS TRUE THEN \'Yes\' ELSE \'No\' END) AS allow_new_cmr, 
				(CASE WHEN vrml.is_surveillance IS TRUE THEN \'Yes\' ELSE \'No\' END) AS is_surveillance, 
				(CASE WHEN ml.check_master_org IS TRUE THEN \'Yes\' ELSE \'No\' END) AS check_master_org, 
				(CASE WHEN ml.condition_from_result IS TRUE THEN \'Yes\' ELSE \'No\' END) AS condition_from_result, 
				(CASE WHEN ml.organism_from_result IS TRUE THEN \'Yes\' ELSE \'No\' END) AS organism_from_result, 
				vrml.conditions_structured AS rule_structured, 
				ml.last_updated AS last_updated
			FROM '.$my_db_schema.'vocab_master_loinc ml
			LEFT JOIN '.$my_db_schema.'vocab_rules_masterloinc vrml ON (ml.l_id = vrml.master_loinc_id AND vrml.app_id = 1) 
			LEFT JOIN '.$my_db_schema.'vocab_master_condition mc ON (ml.trisano_condition = mc.c_id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_c ON (mc.condition = mv_c.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_organism mo ON (ml.trisano_organism = mo.o_id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_o ON (mo.organism = mv_o.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_t ON (ml.trisano_test_type = mv_t.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_s ON (vrml.state_case_status_master_id = mv_s.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_ss ON (ml.specimen_source = mv_ss.id) 
			LEFT JOIN '.$my_db_schema.'system_statuses l ON (ml.list = l.id) 
			ORDER BY mv_c.concept, ml.concept_name;');
			
		/* MasterLoinc > Master Organism */
		define('EXPORT_QRY_ML_ORGANISM', 'SELECT mv_sc.concept AS snomed_category, mo.snomed AS snomed, mo.snomed_alt AS snomed_alt, mv_c.concept AS condition, mv_o.concept AS organism, mv_s.concept AS status, 
				l.name AS list, initcap(m2a.coded_value) AS test_result, 
				(CASE WHEN mo.nom_is_surveillance IS TRUE THEN \'Yes\' ELSE \'No\' END) AS nom_is_surveillance, 
				mo.last_updated AS last_updated
			FROM '.$my_db_schema.'vocab_master_organism mo
			LEFT JOIN '.$my_db_schema.'vocab_master_condition mc ON (mo.condition = mc.c_id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_c ON (mc.condition = mv_c.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_o ON (mo.organism = mv_o.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_s ON (mo.status = mv_s.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_sc ON (mo.snomed_category = mv_sc.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master2app m2a ON (mo.test_result = m2a.master_id AND m2a.app_id = 1) 
			LEFT JOIN '.$my_db_schema.'system_statuses l ON (mo.list = l.id) 
			ORDER BY mv_c.concept;');
			
		/* Child Vocab > Child Value Set */
		define('EXPORT_QRY_CHILD_VOCAB', 'SELECT c.label AS category, cv.concept as child_concept, mv.concept AS master_concept, cv.last_updated AS last_updated
			FROM '.$my_db_schema.'vocab_child_vocab cv
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (cv.master_id = mv.id)
			LEFT JOIN '.$my_db_schema.'structure_category c ON (mv.category = c.id)
			WHERE cv.lab_id = %d 
			ORDER BY c.label, cv.concept;');
		
		/* Child Vocab > Child Organism */
		define('EXPORT_QRY_CHILD_ORGANISM', 'SELECT mv.concept AS organism, mv_t.concept AS test_result_id, co.child_code AS child_code, mo.snomed AS snomed, mo_t.snomed AS test_result_snomed, co.app_test_result AS app_test_result, 
				co.value AS value, co.test_status AS test_status, co.last_updated AS last_updated, co.result_value AS result_value, co.comment AS comment
			FROM '.$my_db_schema.'vocab_child_organism co
			LEFT JOIN '.$my_db_schema.'vocab_master_organism mo ON (co.organism = mo.o_id)
			LEFT JOIN '.$my_db_schema.'vocab_master_organism mo_t ON (co.test_result_id = mo_t.o_id)
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (mo.organism = mv.id)
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_t ON (mo_t.organism = mv_t.id)
			WHERE co.lab_id = %d 
			ORDER BY mv.concept;');
		
		/* Child Vocab > Child LOINC */
		define('EXPORT_QRY_CHILD_LOINC', 'SELECT ml.loinc AS master_loinc, ml.concept_name AS preferred_concept_name, cl.child_loinc AS child_loinc, cl.child_concept_name AS child_concept_name, 
				cl.child_orderable_test_code AS cotc, cl.child_resultable_test_code AS crtc, cl.child_alias AS child_alias, 
				mv_rl.concept AS result_location, 
				(CASE WHEN cl.interpret_results IS TRUE THEN \'Yes\' ELSE \'No\' END) AS interpret_results, 
				(CASE WHEN cl.semi_auto IS TRUE THEN \'Yes\' ELSE \'No\' END) AS semi_auto, 
				(CASE WHEN cl.validate_child_snomed IS TRUE THEN \'Yes\' ELSE \'No\' END) AS validate_child_snomed, 
				cl.units AS units, 
				(CASE WHEN cl.pregnancy IS TRUE THEN \'Yes\' ELSE \'No\' END) AS pregnancy, 
				cl.refrange AS refrange, cl.hl7_refrange AS hl7_refrange, 
				initcap(m2a.coded_value) AS result, c2m.conditions_structured AS rule_structured, c2m.results_to_comments AS results_to_comments, 
				(CASE WHEN cl.archived IS TRUE THEN \'Yes\' ELSE \'No\' END) AS archived, 
				cl.last_updated AS last_updated
			FROM '.$my_db_schema.'vocab_child_loinc cl
			LEFT JOIN '.$my_db_schema.'vocab_c2m_testresult c2m ON (cl.id = c2m.child_loinc_id AND c2m.app_id = 1) 
			LEFT JOIN '.$my_db_schema.'vocab_master_loinc ml ON (cl.master_loinc = ml.l_id) 
			LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_rl ON (cl.result_location = mv_rl.id) 
			LEFT JOIN '.$my_db_schema.'vocab_master2app m2a ON (c2m.master_id = m2a.master_id AND m2a.app_id = 1) 
			WHERE cl.lab_id = %d  
			ORDER BY ml.loinc, cl.child_loinc;');
	
?>