<?php

	require_once './includes/classes/PHPExcel.php';
	require_once 'structure_import_functions.php';
	require_once 'structure_import_constants.php';
	
	set_time_limit(120);
	
	define('EXPORT_TIMESTAMP', date("YmdHis", time()));
	
	$export_filename = WEBROOT_URL.'/manage/import/upload/elr_structure_export__'.strtolower(EXPORT_SERVERNAME).'__'.EXPORT_TIMESTAMP.'.zip';
	
	$finished_files = array();  // placeholder for filenames to be zipped
	$export_files = array();
	
	/* 
	 * loop through all children with vocab & add to export_files list
	 */
	$child_sql = 'SELECT id, ui_name FROM '.$my_db_schema.'structure_labs WHERE ui_name <> \'eHARS\' ORDER BY id;';
	$child_rs = @pg_query($host_pa, $child_sql);
	if ($child_rs !== false) {
		while ($child_row = @pg_fetch_object($child_rs)) {
			$export_files[] = array(
				'filename' => WEBROOT_URL.'/manage/import/upload/'.trim(preg_replace('/[\\/:"*?<>|]+/', '', $child_row->ui_name)).'_HL7_Structure__'.EXPORT_SERVERNAME.'Export__'.EXPORT_TIMESTAMP.'.xls', 
				'lab_name' => trim($child_row->ui_name), 
				'sheets' => array(
					'Sheet1' => array(
						'query' => sprintf(EXPORT_QRY_CHILD_HL7XML, intval($child_row->id)), 
						'columns' => array(
							array(
								'width' => '25', 
								'label' => 'Lab Name', 
								'column' => 'lab_name'
							), 
							array(
								'width' => '15', 
								'label' => 'Message Version', 
								'column' => 'message_version'
							), 
							array(
								'width' => '50', 
								'label' => 'HL7 XPath', 
								'column' => 'hl7_xpath'
							), 
							array(
								'width' => '30', 
								'label' => 'Master XPath', 
								'column' => 'master_xpath'
							), 
							array(
								'width' => '15', 
								'label' => 'Concat String', 
								'column' => 'glue_string'
							), 
							array(
								'width' => '15', 
								'label' => 'Sequence', 
								'column' => 'sequence'
							)
						)
					)
				), 
				'object' => new PHPExcel()
			);
		}
	}
	
	foreach ($export_files as $export_file) {  // create all files expected for export
		$current_export_file = $export_file['object'];
	
		$current_export_file->getProperties()->setCreator("UDOH ELR Structure Exporter");
		$current_export_file->getProperties()->setLastModifiedBy("UDOH ELR Structure Exporter");
		
		$sheet_counter = 0;
		foreach ($export_file['sheets'] as $sheet_name => $sheet_data) {  // create all sheets for this file
			if ($sheet_counter > 0) {
				$current_export_file->createSheet();
			}
			$sheet_last_column = (count($sheet_data['columns']) - 1);
			$header_range = 'A1:'.getExcelColumnLabel($sheet_last_column).'1';
	
			$current_export_file->setActiveSheetIndex($sheet_counter);
			$current_export_file->getActiveSheet()->setTitle(substr($sheet_name, 0, 30));  // tab name limited to 31 chars
			
			if (isset($sheet_data['columns']) && is_array($sheet_data['columns']) && (count($sheet_data['columns']) > 0)) {
				// write out column headers
				foreach ($sheet_data['columns'] as $column_index => $column_data) {
					$current_export_file->getActiveSheet()->setCellValue(getExcelColumnLabel($column_index).'1', $column_data['label']);
				}
		
				// get data back for this sheet's columns
				unset($sheet_rs);
				unset($sheet_row);
				if (isset($sheet_data['query']) && !is_null($sheet_data['query'])) {
					$sheet_rs = @pg_query($host_pa, $sheet_data['query']);
				}
				
				if (isset($sheet_rs) && ($sheet_rs !== false) && (@pg_num_rows($sheet_rs) > 0)) {
					$sheet_row_index = 2;
					while ($sheet_row = @pg_fetch_object($sheet_rs)) {
						foreach ($sheet_data['columns'] as $column_index_fetch => $column_data_fetch) {
							if (isset($column_data_fetch['decode']) && !is_null($column_data_fetch['decode']) && isset($column_data_fetch['get_app_value']) && $column_data_fetch['get_app_value']) {
								$current_export_file->getActiveSheet()->setCellValueExplicit(
									getExcelColumnLabel($column_index_fetch).intval($sheet_row_index), 
									decodeJSONForExport($sheet_row->$column_data_fetch['column'], $column_data_fetch['decode'], $column_data_fetch['segment'], true, 1).' ', 
									PHPExcel_Cell_DataType::TYPE_STRING
								);
							} elseif (isset($column_data_fetch['decode']) && !is_null($column_data_fetch['decode'])) {
								$current_export_file->getActiveSheet()->setCellValueExplicit(
									getExcelColumnLabel($column_index_fetch).intval($sheet_row_index), 
									decodeJSONForExport($sheet_row->$column_data_fetch['column'], $column_data_fetch['decode'], $column_data_fetch['segment'], false).' ', 
									PHPExcel_Cell_DataType::TYPE_STRING
								);
							} else {
								$current_export_file->getActiveSheet()->setCellValueExplicit(
									getExcelColumnLabel($column_index_fetch).intval($sheet_row_index), 
									$sheet_row->$column_data_fetch['column'].' ', 
									PHPExcel_Cell_DataType::TYPE_STRING
								);
							}
							$current_export_file->getActiveSheet()->getStyle(getExcelColumnLabel($column_index_fetch).intval($sheet_row_index))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
						}
						$sheet_row_index++;
					}
				}
				
				// size columns based on defined widths
				foreach ($sheet_data['columns'] as $column_index_sizing => $column_data_trash) {
					$current_export_file->getActiveSheet()->getColumnDimension(getExcelColumnLabel($column_index_sizing))->setWidth($column_data_trash['width']);
				}
				
				// formatting & beautification
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setSize('12');
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->getStartColor()->setARGB('FFB0C4DE');
				
				if (stripos($export_file['filename'], 'child_values') !== false) {
					// add lab name header to file if child vocab
					$current_export_file->getActiveSheet()->insertNewRowBefore(1, 1);  // one new row inserted before current column headings
					$current_export_file->getActiveSheet()->setCellValue('A1', $export_file['lab_name']);
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setSize('16');
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->getStartColor()->setARGB('FFA4D246');
					$current_export_file->getActiveSheet()->mergeCells($header_range);
					$current_export_file->getActiveSheet()->freezePane('A3');
				} else {
					$current_export_file->getActiveSheet()->freezePane('A2');
				}
				
				// apply text wrapping to entire range of cells
				$current_export_file->getActiveSheet()->getStyle('A1:'.getExcelColumnLabel($sheet_last_column).trim($sheet_row_index))->getAlignment()->setWrapText(true);
			}
			
			/* cell writing & styling examples
			$this_phe->getActiveSheet()->setCellValue("B1", "Investigator");
			
			$this_phe->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold(true);
			$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setSize("14");
			$this_phe->getActiveSheet()->getStyle("A1:E1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$this_phe->getActiveSheet()->getStyle("A1:E1")->getFill()->getStartColor()->setARGB('FFAFEEEE');
			
			$this_phe->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
			
			$this_phe->getActiveSheet()->insertNewRowBefore(1, 1);  // one new row inserted before current column headings
			$this_phe->getActiveSheet()->mergeCells("A1:E1");
			
			$this_phe->getActiveSheet()->freezePane("A3");
			*/
			@pg_free_result($sheet_rs);
			$sheet_counter++;
		}
		
		$current_export_file->setActiveSheetIndex(0);  // set focus back to first sheet in workbook
	
		$current_writer = new PHPExcel_Writer_Excel5($current_export_file);
		$current_writer->save($export_file['filename']);
		
		unset($current_writer);
		$current_export_file->disconnectWorksheets();  // important to prevent memory leak
		unset($current_export_file);
		
		$finished_files[] = $export_file['filename'];
	}
	
	// package all files together, remove originals
	createZip($finished_files, $export_filename);
	
	echo 'Done!';

?>