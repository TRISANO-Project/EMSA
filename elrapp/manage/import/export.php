<?php

	require_once './includes/classes/PHPExcel.php';
	require_once 'import_functions.php';
	require_once 'import_constants.php';
	
	set_time_limit(120);
	
	define('EXPORT_TIMESTAMP', date("YmdHis", time()));
	
	$export_filename = WEBROOT_URL.'/manage/import/upload/elr_vocabexport_'.strtolower(EXPORT_SERVERNAME).'_'.EXPORT_TIMESTAMP.'.zip';
	
	$finished_files = array();  // placeholder for filenames to be zipped
	$export_files = array(
		array(
			'filename' => WEBROOT_URL.'/manage/import/upload/Master_Value_Set__'.EXPORT_SERVERNAME.'Export__'.EXPORT_TIMESTAMP.'.xls', 
			'sheets' => array(
				'Sheet1' => array(
					'query' => EXPORT_QRY_MASTERVOCAB, 
					'columns' => array(
						array(
							'width' => '25', 
							'label' => 'Category',
							'column' => 'category'
						), 
						array(
							'width' => '35', 
							'label' => 'Coding System', 
							'column' => 'codeset'
						), 
						array(
							'width' => '50', 
							'label' => 'Master Code', 
							'column' => 'concept'
						), 
						array(
							'width' => '40', 
							'label' => 'Last Updated', 
							'column' => 'last_updated'
						)
					)
				)
			), 
			'object' => new PHPExcel()
		), 
		array(
			'filename' => WEBROOT_URL.'/manage/import/upload/Application_Value_Set__'.EXPORT_SERVERNAME.'Export__'.EXPORT_TIMESTAMP.'.xls', 
			'sheets' => array(
				'Sheet1' => array(
					'query' => EXPORT_QRY_APPVALUE, 
					'columns' => array(
						array(
							'width' => '10', 
							'label' => 'App', 
							'column' => 'app_name'
						), 
						array(
							'width' => '25', 
							'label' => 'Category', 
							'column' => 'category'
						), 
						array(
							'width' => '50', 
							'label' => 'Master Value', 
							'column' => 'master_concept'
						), 
						array(
							'width' => '50', 
							'label' => 'App Value', 
							'column' => 'coded_value'
						), 
						array(
							'width' => '40', 
							'label' => 'Last Updated', 
							'column' => 'last_updated'
						)
					)
				)
			), 
			'object' => new PHPExcel()
		), 
		array(
			'filename' => WEBROOT_URL.'/manage/import/upload/MasterLoinc__'.EXPORT_SERVERNAME.'Export__'.EXPORT_TIMESTAMP.'.xls', 
			'sheets' => array(
				'Master Condition' => array(
					'query' => EXPORT_QRY_ML_CONDITION, 
					'columns' => array(
						array(
							'width' => '25', 
							'label' => 'Disease Category', 
							'column' => 'disease_category'
						), 
						array(
							'width' => '45', 
							'label' => 'Condition', 
							'column' => 'condition'
						), 
						array(
							'width' => '25', 
							'label' => 'Alias', 
							'column' => 'gateway_xref'
						), 
						array(
							'width' => '15', 
							'label' => 'Check Crossrefs First?', 
							'column' => 'check_xref_first'
						), 
						array(
							'width' => '15', 
							'label' => 'Immediately Notifiable', 
							'column' => 'immediate_notify'
						), 
						array(
							'width' => '15', 
							'label' => 'Require Specimen Source from Nominal Culture', 
							'column' => 'require_specimen'
						), 
						array(
							'width' => '30', 
							'label' => 'Valid Specimen Sources', 
							'column' => 'valid_specimen'
						), 
						array(
							'width' => '30', 
							'label' => 'Invalid Specimen Sources', 
							'column' => 'invalid_specimen'
						), 
						array(
							'width' => '25', 
							'label' => 'Morbidity Whitelist Rules', 
							'column' => 'white_rule'
						), 
						array(
							'width' => '25', 
							'label' => 'Contact Whitelist Rules', 
							'column' => 'contact_white_rule'
						), 
						array(
							'width' => '15', 
							'label' => 'Different Species/Toxin Same Event or New Event', 
							'column' => 'new_event'
						), 
						array(
							'width' => '15', 
							'label' => 'Notify State Upon Receipt', 
							'column' => 'notify_state'
						), 
						array(
							'width' => '15', 
							'label' => 'Autoapproval', 
							'column' => 'autoapproval'
						), 
						array(
							'width' => '40', 
							'label' => 'Jurisdiction Override', 
							'column' => 'district_override'
						), 
						array(
							'width' => '40', 
							'label' => 'Last Updated', 
							'column' => 'last_updated'
						)
					)
				), 
				'Master LOINC to Condition' => array(
					'query' => EXPORT_QRY_ML_LOINC, 
					'columns' => array(
						array(
							'width' => '15', 
							'label' => 'Test Concept Code (LOINC)', 
							'column' => 'loinc'
						), 
						array(
							'width' => '30', 
							'label' => 'Preferred Concept Name', 
							'column' => 'preferred_concept'
						), 
						array(
							'width' => '15', 
							'label' => 'Look Up Condition?', 
							'column' => 'condition_from_result'
						), 
						array(
							'width' => '25', 
							'label' => 'TriSano Disease', 
							'column' => 'condition'
						), 
						array(
							'width' => '15', 
							'label' => 'Look Up Organism?', 
							'column' => 'organism_from_result'
						), 
						array(
							'width' => '25', 
							'label' => 'TriSano Organism', 
							'column' => 'organism'
						), 
						array(
							'width' => '25', 
							'label' => 'TriSano Test Type', 
							'column' => 'test_type'
						), 
						array(
							'width' => '20', 
							'label' => 'Specimen Source', 
							'column' => 'specimen_source'
						), 
						array(
							'width' => '15', 
							'label' => 'List', 
							'column' => 'list'
						), 
						array(
							'width' => '25', 
							'label' => 'Graylist Rules', 
							'column' => 'gray_rule'
						), 
						array(
							'width' => '15', 
							'label' => 'Check Master Organism List?', 
							'column' => 'check_master_org'
						), 
						array(
							'width' => '10', 
							'label' => 'Operator A', 
							'column' => 'rule_structured', 
							'decode' => 'operator',
							'segment' => 0
						), 
						array(
							'width' => '15', 
							'label' => 'Operand A', 
							'column' => 'rule_structured', 
							'decode' => 'operand',
							'segment' => 0, 
							'get_app_value' => true
						), 
						array(
							'width' => '10', 
							'label' => 'Operator B', 
							'column' => 'rule_structured', 
							'decode' => 'operator',
							'segment' => 1
						), 
						array(
							'width' => '15', 
							'label' => 'Operand B', 
							'column' => 'rule_structured', 
							'decode' => 'operand',
							'segment' => 1, 
							'get_app_value' => true
						), 
						array(
							'width' => '15', 
							'label' => 'Create New CMR?', 
							'column' => 'allow_new_cmr'
						), 
						array(
							'width' => '15', 
							'label' => 'Surveillance?', 
							'column' => 'is_surveillance'
						), 
						array(
							'width' => '25', 
							'label' => 'Set State Case Status to:', 
							'column' => 'status'
						), 
						array(
							'width' => '40', 
							'label' => 'Last Updated', 
							'column' => 'last_updated'
						)
					)
				), 
				'Master Organism' => array(
					'query' => EXPORT_QRY_ML_ORGANISM, 
					'columns' => array(
						array(
							'width' => '20', 
							'label' => 'SNOMED Type', 
							'column' => 'snomed_category'
						), 
						array(
							'width' => '40', 
							'label' => 'Condition', 
							'column' => 'condition'
						), 
						array(
							'width' => '30', 
							'label' => 'SNOMED Code', 
							'column' => 'snomed'
						), 
						array(
							'width' => '30', 
							'label' => 'Secondary SNOMED Code', 
							'column' => 'snomed_alt'
						), 
						array(
							'width' => '40', 
							'label' => 'Organism', 
							'column' => 'organism'
						), 
						array(
							'width' => '20', 
							'label' => 'List', 
							'column' => 'list'
						), 
						array(
							'width' => '20', 
							'label' => 'TriSano Test Result', 
							'column' => 'test_result'
						), 
						array(
							'width' => '25', 
							'label' => 'Set State Case Status to:', 
							'column' => 'status'
						), 
						array(
							'width' => '25', 
							'label' => 'Nominal Surveillance?', 
							'column' => 'nom_is_surveillance'
						), 
						array(
							'width' => '40', 
							'label' => 'Last Updated', 
							'column' => 'last_updated'
						)
					)
				)
			), 
			'object' => new PHPExcel()
		)
	);
	
	/* 
	 * loop through all children with vocab & add to export_files list
	 */
	$child_sql = 'SELECT id, ui_name FROM '.$my_db_schema.'structure_labs WHERE ui_name <> \'eHARS\' ORDER BY id;';
	$child_rs = @pg_query($host_pa, $child_sql);
	if ($child_rs !== false) {
		while ($child_row = @pg_fetch_object($child_rs)) {
			$export_files[] = array(
				'filename' => WEBROOT_URL.'/manage/import/upload/'.trim(preg_replace('/[\\/:"*?<>|]+/', '', $child_row->ui_name)).'_Child_Values__'.EXPORT_SERVERNAME.'Export__'.EXPORT_TIMESTAMP.'.xls', 
				'lab_name' => trim($child_row->ui_name), 
				'sheets' => array(
					'Child LOINC' => array(
						'query' => sprintf(EXPORT_QRY_CHILD_LOINC, intval($child_row->id)), 
						'columns' => array(
							array(
								'width' => '15', 
								'label' => 'Test Concept Code (LOINC)', 
								'column' => 'master_loinc'
							), 
							array(
								'width' => '30', 
								'label' => 'Preferred Concept Name', 
								'column' => 'preferred_concept_name'
							), 
							array(
								'width' => '15', 
								'label' => 'Child Test Concept Code', 
								'column' => 'child_loinc'
							), 
							array(
								'width' => '15', 
								'label' => 'Archived?', 
								'column' => 'archived'
							), 
							array(
								'width' => '15', 
								'label' => 'Child Orderable Test Code**', 
								'column' => 'cotc'
							), 
							array(
								'width' => '15', 
								'label' => 'Child Resultable Test Code**', 
								'column' => 'crtc'
							), 
							array(
								'width' => '30', 
								'label' => 'Child Preferred Concept Name**', 
								'column' => 'child_concept_name'
							), 
							array(
								'width' => '20', 
								'label' => 'Alias**', 
								'column' => 'child_alias'
							), 
							array(
								'width' => '15', 
								'label' => 'Indicates Pregnancy?', 
								'column' => 'pregnancy'
							), 
							array(
								'width' => '15', 
								'label' => 'Requires Interpretation?', 
								'column' => 'interpret_results'
							), 
							array(
								'width' => '15', 
								'label' => 'Semi-Automated Entry?', 
								'column' => 'semi_auto'
							), 
							array(
								'width' => '15', 
								'label' => 'Validate Child SNOMEDs?', 
								'column' => 'validate_child_snomed'
							), 
							array(
								'width' => '10', 
								'label' => 'Operator A', 
								'column' => 'rule_structured', 
								'decode' => 'operator',
								'segment' => 0
							), 
							array(
								'width' => '15', 
								'label' => 'Operand A', 
								'column' => 'rule_structured', 
								'decode' => 'operand',
								'segment' => 0
							), 
							array(
								'width' => '10', 
								'label' => 'Operator B', 
								'column' => 'rule_structured', 
								'decode' => 'operator',
								'segment' => 1
							), 
							array(
								'width' => '15', 
								'label' => 'Operand B', 
								'column' => 'rule_structured', 
								'decode' => 'operand',
								'segment' => 1
							), 
							array(
								'width' => '20', 
								'label' => 'TriSano Test Result', 
								'column' => 'result'
							), 
							array(
								'width' => '20', 
								'label' => 'Result Location', 
								'column' => 'result_location'
							), 
							array(
								'width' => '15', 
								'label' => 'Units', 
								'column' => 'units'
							), 
							array(
								'width' => '25', 
								'label' => 'Reference Range', 
								'column' => 'refrange'
							), 
							array(
								'width' => '20', 
								'label' => 'HL7 Reference Range', 
								'column' => 'hl7_refrange'
							), 
							array(
								'width' => '20', 
								'label' => 'Results to Comments', 
								'column' => 'results_to_comments'
							), 
							array(
								'width' => '40', 
								'label' => 'Last Updated', 
								'column' => 'last_updated'
							)
						)
					), 
					'Child Organism' => array(
						'query' => sprintf(EXPORT_QRY_CHILD_ORGANISM, intval($child_row->id)), 
						'columns' => array(
							array(
								'width' => '20', 
								'label' => 'Child Code', 
								'column' => 'child_code'
							), 
							array(
								'width' => '20', 
								'label' => 'Organism SNOMED Code', 
								'column' => 'snomed'
							), 
							array(
								'width' => '50', 
								'label' => 'Organism', 
								'column' => 'organism'
							), 
							array(
								'width' => '20', 
								'label' => 'Test Result SNOMED Code', 
								'column' => 'test_result_snomed'
							), 
							array(
								'width' => '50', 
								'label' => 'Test Result', 
								'column' => 'test_result_id'
							), 
							array(
								'width' => '20', 
								'label' => 'Result Value', 
								'column' => 'result_value'
							), 
							array(
								'width' => '20', 
								'label' => 'Comments', 
								'column' => 'comment'
							), 
							array(
								'width' => '20', 
								'label' => '[old] TriSano Test Result', 
								'column' => 'app_test_result'
							), 
							array(
								'width' => '20', 
								'label' => '[old] Value', 
								'column' => 'value'
							), 
							array(
								'width' => '20', 
								'label' => '[old] Test Status', 
								'column' => 'test_status'
							), 
							array(
								'width' => '40', 
								'label' => 'Last Updated', 
								'column' => 'last_updated'
							)
						)
					), 
					'Child Vocab' => array(
						'query' => sprintf(EXPORT_QRY_CHILD_VOCAB, intval($child_row->id)), 
						'columns' => array(
							array(
								'width' => '25', 
								'label' => 'Category', 
								'column' => 'category'
							), 
							array(
								'width' => '50', 
								'label' => 'Child Value', 
								'column' => 'child_concept'
							), 
							array(
								'width' => '50', 
								'label' => 'Master Value', 
								'column' => 'master_concept'
							), 
							array(
								'width' => '40', 
								'label' => 'Last Updated', 
								'column' => 'last_updated'
							)
						)
					)
				), 
				'object' => new PHPExcel()
			);
		}
	}
	
	foreach ($export_files as $export_file) {  // create all files expected for export
		$current_export_file = $export_file['object'];
	
		$current_export_file->getProperties()->setCreator("UDOH ELR Vocabulary Exporter");
		$current_export_file->getProperties()->setLastModifiedBy("UDOH ELR Vocabulary Exporter");
		
		$sheet_counter = 0;
		foreach ($export_file['sheets'] as $sheet_name => $sheet_data) {  // create all sheets for this file
			if ($sheet_counter > 0) {
				$current_export_file->createSheet();
			}
			$sheet_last_column = (count($sheet_data['columns']) - 1);
			$header_range = 'A1:'.getExcelColumnLabel($sheet_last_column).'1';
	
			$current_export_file->setActiveSheetIndex($sheet_counter);
			$current_export_file->getActiveSheet()->setTitle(substr($sheet_name, 0, 30));  // tab name limited to 31 chars
			
			if (isset($sheet_data['columns']) && is_array($sheet_data['columns']) && (count($sheet_data['columns']) > 0)) {
				// write out column headers
				foreach ($sheet_data['columns'] as $column_index => $column_data) {
					$current_export_file->getActiveSheet()->setCellValue(getExcelColumnLabel($column_index).'1', $column_data['label']);
				}
		
				// get data back for this sheet's columns
				unset($sheet_rs);
				unset($sheet_row);
				if (isset($sheet_data['query']) && !is_null($sheet_data['query'])) {
					$sheet_rs = @pg_query($host_pa, $sheet_data['query']);
				}
				
				if (isset($sheet_rs) && ($sheet_rs !== false) && (@pg_num_rows($sheet_rs) > 0)) {
					$sheet_row_index = 2;
					while ($sheet_row = @pg_fetch_object($sheet_rs)) {
						foreach ($sheet_data['columns'] as $column_index_fetch => $column_data_fetch) {
							if (isset($column_data_fetch['decode']) && !is_null($column_data_fetch['decode']) && isset($column_data_fetch['get_app_value']) && $column_data_fetch['get_app_value']) {
								$current_export_file->getActiveSheet()->setCellValueExplicit(
									getExcelColumnLabel($column_index_fetch).intval($sheet_row_index), 
									decodeJSONForExport($sheet_row->$column_data_fetch['column'], $column_data_fetch['decode'], $column_data_fetch['segment'], true, 1).' ', 
									PHPExcel_Cell_DataType::TYPE_STRING
								);
							} elseif (isset($column_data_fetch['decode']) && !is_null($column_data_fetch['decode'])) {
								$current_export_file->getActiveSheet()->setCellValueExplicit(
									getExcelColumnLabel($column_index_fetch).intval($sheet_row_index), 
									decodeJSONForExport($sheet_row->$column_data_fetch['column'], $column_data_fetch['decode'], $column_data_fetch['segment'], false).' ', 
									PHPExcel_Cell_DataType::TYPE_STRING
								);
							} else {
								$current_export_file->getActiveSheet()->setCellValueExplicit(
									getExcelColumnLabel($column_index_fetch).intval($sheet_row_index), 
									$sheet_row->$column_data_fetch['column'].' ', 
									PHPExcel_Cell_DataType::TYPE_STRING
								);
							}
							$current_export_file->getActiveSheet()->getStyle(getExcelColumnLabel($column_index_fetch).intval($sheet_row_index))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
						}
						$sheet_row_index++;
					}
				}
				
				// size columns based on defined widths
				foreach ($sheet_data['columns'] as $column_index_sizing => $column_data_trash) {
					$current_export_file->getActiveSheet()->getColumnDimension(getExcelColumnLabel($column_index_sizing))->setWidth($column_data_trash['width']);
				}
				
				// formatting & beautification
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setSize('12');
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->getStartColor()->setARGB('FFB0C4DE');
				
				if (stripos($export_file['filename'], 'child_values') !== false) {
					// add lab name header to file if child vocab
					$current_export_file->getActiveSheet()->insertNewRowBefore(1, 1);  // one new row inserted before current column headings
					$current_export_file->getActiveSheet()->setCellValue('A1', $export_file['lab_name']);
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFont()->setSize('16');
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$current_export_file->getActiveSheet()->getStyle($header_range)->getFill()->getStartColor()->setARGB('FFA4D246');
					$current_export_file->getActiveSheet()->mergeCells($header_range);
					$current_export_file->getActiveSheet()->freezePane('A3');
				} else {
					$current_export_file->getActiveSheet()->freezePane('A2');
				}
				
				// apply text wrapping to entire range of cells
				$current_export_file->getActiveSheet()->getStyle('A1:'.getExcelColumnLabel($sheet_last_column).trim($sheet_row_index))->getAlignment()->setWrapText(true);
			}
			
			/* cell writing & styling examples
			$this_phe->getActiveSheet()->setCellValue("B1", "Investigator");
			
			$this_phe->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold(true);
			$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setSize("14");
			$this_phe->getActiveSheet()->getStyle("A1:E1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$this_phe->getActiveSheet()->getStyle("A1:E1")->getFill()->getStartColor()->setARGB('FFAFEEEE');
			
			$this_phe->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
			
			$this_phe->getActiveSheet()->insertNewRowBefore(1, 1);  // one new row inserted before current column headings
			$this_phe->getActiveSheet()->mergeCells("A1:E1");
			
			$this_phe->getActiveSheet()->freezePane("A3");
			*/
			@pg_free_result($sheet_rs);
			$sheet_counter++;
		}
		
		$current_export_file->setActiveSheetIndex(0);  // set focus back to first sheet in workbook
	
		$current_writer = new PHPExcel_Writer_Excel5($current_export_file);
		$current_writer->save($export_file['filename']);
		
		unset($current_writer);
		$current_export_file->disconnectWorksheets();  // important to prevent memory leak
		unset($current_export_file);
		
		$finished_files[] = $export_file['filename'];
	}
	
	// package all files together, remove originals
	createZip($finished_files, $export_filename);
	
	echo 'Done!';

?>