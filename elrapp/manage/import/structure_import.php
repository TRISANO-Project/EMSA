<?php

	$lab_qry = "SELECT id, ui_name FROM ".$my_db_schema."structure_labs ORDER BY ui_name";
	$lab_rs = pg_fetch_all(pg_query($host_pa, $lab_qry));
	
?>

<script>
	$(function() {
		$( "#upload_button" ).button({
			icons: {
				primary: "ui-icon-elrsave"
			}
		}).click(function(){
			$("#import_uploader").submit();
		});
		
		$("#vocab_type_master").click(function() {
			$("#vocab_child_lab").hide('fast');
		});
		
		$("#vocab_type_childmirth").click(function() {
			$("#vocab_child_lab").show('fast');
		});
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrimport"></span>Import New Structure</h1>

<div class="import_widget ui-widget" style="font-family: 'Open Sans', Arial, Helvetica;">
	<div>
		<p>To mass-import a new structure definition, select the Excel workbook below and click <em>Import New Structure</em>.</p>
	</div>
	<?php highlight("<strong>Warning:</strong>  This upload will replace ALL existing structure definitions stored in the database for the selected structure type.  This action cannot be undone.  Please ensure that all definitions are accurate & complete before continuing.</p>"); ?>
</div>

<div class="import_widget ui-widget ui-widget-content ui-corner-all" style="padding: 5px; font-family: 'Open Sans', Arial, Helvetica;">
	<form name="import_uploader" id="import_uploader" method="POST" enctype="multipart/form-data">
		<p><label class="vocab_search_form">Which structure type are you uploading?</label></p>
		<p style="margin-bottom: 20px;">
			<input type="radio" name="vocab_type" id="vocab_type_childmirth" value="childmirth"> <label for="vocab_type_childmirth">Lab-specific HL7 XML Structure (Specify Lab) <code>[&lt;LabName&gt;_HL7_Structure.xls]</code></label><br>
			<select class="ui-corner-all" name="vocab_child_lab" id="vocab_child_lab" style="display: none; margin-left: 25px;">
				<option value="" selected>--</option>
			<?php
				foreach ($lab_rs as $lab) {
					printf("<option value=\"%d\">%s</option>", intval($lab['id']), htmlentities($lab['ui_name']));
				}
			?>
			</select>
		</p>
		<p><label class="vocab_search_form">Select an Excel workbook to import:</label></p>
		<p style="margin-bottom: 20px;"><input type="file" name="vocab_source" id="vocab_source" class="ui-corner-all"></p>
		<p><button id="upload_button">Import New Structure</button></p>
		<input type="hidden" name="import_flag" id="import_flag" value="1">
		<input type="hidden" name="selected_page" id="selected_page" value="6">
		<input type="hidden" name="submenu" id="submenu" value="4">
		<input type="hidden" name="cat" id="cat" value="7">
	</form>
</div>