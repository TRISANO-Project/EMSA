<script>
	$(function() {
		$("#upload_button").button({
			icons: {
				primary: "ui-icon-elrsave"
			}
		}).click(function(){
			$("#import_uploader").submit();
		});
		
		$("#addanother_button").button({
			icons: {
				primary: "ui-icon-arrowreturnthick-1-w"
			}
		}).click(function(){
			window.location = '?selected_page=6&submenu=3&vocab=6';
		});
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrimport"></span><?php echo $clean["vocab_verbose"]; ?>Import New Vocabulary</h1>

<?php

	// check for valid import type/lab
	$lab_qry = "SELECT id FROM ".$my_db_schema."structure_labs;";
	$lab_rs = pg_query($host_pa, $lab_qry);
	while ($lab_row = pg_fetch_object($lab_rs)) {
		$valid_labs[] = intval($lab_row->id);
	}
	pg_free_result($lab_rs);
	
	if (isset($_POST['vocab_type'])) {
		switch (strtolower(trim($_POST['vocab_type']))) {
			case "master":
				$clean['vocab_type'] = "master";
				$required_tab_list = array(
					"Master Condition",
					"Master Organism",
					"Master LOINC to Condition"
				);
				break;
			case "mastervocab":
				$clean['vocab_type'] = "mastervocab";
				$required_tab_list = array(
					"Sheet1"
				);
				break;
			case "trisano":
				$clean['vocab_type'] = "trisano";
				$required_tab_list = array(
					"Sheet1"
				);
				break;
			case "childvocab":
				if (isset($_POST['vocab_child_lab'])) {
					if (in_array(intval(trim($_POST['vocab_child_lab'])), $valid_labs)) {
						$clean['lab_id'] = intval(trim($_POST['vocab_child_lab']));
						$clean['vocab_type'] = "childvocab";
						$required_tab_list = array(
							"Child Vocab"
						);
					} else {
						die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Child lab not selected.</p></div>");
					}
				} else {
					die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Child lab not selected.</p></div>");
				}
				break;
			case "child":
				if (isset($_POST['vocab_child_lab'])) {
					if (in_array(intval(trim($_POST['vocab_child_lab'])), $valid_labs)) {
						$clean['lab_id'] = intval(trim($_POST['vocab_child_lab']));
						$clean['vocab_type'] = "child";
						$required_tab_list = array(
							"Child LOINC",
							"Child Organism"
						);
					} else {
						die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Child lab not selected.</p></div>");
					}
				} else {
					die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Child lab not selected.</p></div>");
				}
				break;
			default:
				die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Vocuabulary type not specified.</p></div>");
		}
	} else {
		die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Vocuabulary type not specified.</p></div>");
	}

	$valid_upload = FALSE;
	$upload['upload_dir'] = "./manage/import/upload/";
	$upload['safe_path'] = $upload['upload_dir'] . basename($_FILES['vocab_source']['name']);
	
	if (isset($_FILES['vocab_source'])) {
		if (sizeof($_FILES['vocab_source']['error'] === 0) && move_uploaded_file($_FILES['vocab_source']['tmp_name'], $upload['safe_path'])) {
			$valid_upload = TRUE;
		}
	}
	
	if (!$valid_upload) {
		// file did not upload successfully or no file specified...
		die("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  No file was selected/file was not successfully uploaded.</p></div>");
	}
	
	/**
	 * Initialize PHPExcel stuff
	 * 
	 */
	require_once './includes/classes/PHPExcel.php';
	
	// Create a new Excel5 (Excel 97/XP) Reader
	$objReader = new PHPExcel_Reader_Excel5();
	// Load uploaded file to a PHPExcel Object
	try {
		$pxls = $objReader->load($upload['safe_path']);
	} catch (Exception $pxls_e) {
		die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  ".$pxls_e->getMessage()."</p></div>");
	}
	
	echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Reading new vocabulary data...</em></p></div>";
	
	// make sure required master/child tabs are included in workbook
	// tab names compared with strtoupper() due to case-sensitivity of in_array()
	$found_tabs = 0;
	
	function array_caps($srcArray) {
		return strtoupper($srcArray);
	}
	
	$uploaded_sheets = array_map("array_caps", $pxls->getSheetNames());
	
	if (is_array($uploaded_sheets)) {
		foreach ($required_tab_list as $req_tab_name) {
			if (in_array(strtoupper($req_tab_name), $uploaded_sheets)) {
				$found_tabs++;
			}
		}
	}
	
	if ($found_tabs !== sizeof($required_tab_list)) {
		// required tabs not found...
		die("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  One or more worksheets may be missing.</p></div>");
	}
	
	
	
	/**
	 * Master LOINC/Condition/Organism handling
	 *
	 */
	if ($clean['vocab_type'] == "master") {
		// Process condition first, then organism, then LOINCs for ID lookups to succeed...
		
		###############################################
		# Master Condition ############################
			unset($vocab_insert_sql);
			unset($insert_rs);
			unset($insert_error_rs);
			unset($delete_rs);
			unset($delete_error_rs);
			$pxls->setActiveSheetIndexByName("Master Condition");
			$this_sheet = $pxls->getActiveSheet()->toArray(null,true,false,true);
			
			$vmc_inserts = 0;
			foreach ($this_sheet as $this_key => $this_row) {
				// don't insert column headings or any rows with a null/empty condition
				if (($this_key > 1) && (strlen(trim($this_row['B'])) > 1)) {
					// codify 'gateway aliases' column
					unset($this_gateway_string);
					unset($this_gateway_aliases);
					unset($this_gateway_aliases_raw);
					$this_gateway_aliases_raw = explode(";", trim($this_row['C']));
					if (is_array($this_gateway_aliases_raw) && (count($this_gateway_aliases_raw) > 0)) {
						foreach ($this_gateway_aliases_raw as $this_gateway_alias_item) {
							// look up id for matching gateway_xref from Master Value Set...
							$getalias_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('gateway_xref') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_gateway_alias_item)));
							$getalias_row = @pg_fetch_object(@pg_query($host_pa, $getalias_sql));
							if (intval($getalias_row->id) > 0) {
								$this_gateway_aliases[] = intval($getalias_row->id);
							}
						}
					}
					if (is_array($this_gateway_aliases) && (count($this_gateway_aliases) > 0)) {
						$this_gateway_string = "'".implode(";", $this_gateway_aliases)."'";
					} else {
						$this_gateway_string = "NULL";
					}
					
					// codify 'valid specimen sources' column
					unset($this_specimen_string);
					unset($this_specimen_aliases);
					unset($this_specimen_aliases_raw);
					$this_specimen_aliases_raw = explode(";", trim($this_row['G']));
					if (is_array($this_specimen_aliases_raw) && (count($this_specimen_aliases_raw) > 0)) {
						foreach ($this_specimen_aliases_raw as $this_specimen_alias_item) {
							// look up id for matching gateway_xref from Master Value Set...
							$getspecimen_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_specimen_alias_item)));
							$getspecimen_row = @pg_fetch_object(@pg_query($host_pa, $getspecimen_sql));
							if (intval($getspecimen_row->id) > 0) {
								$this_specimen_aliases[] = intval($getspecimen_row->id);
							}
						}
					}
					if (is_array($this_specimen_aliases) && (count($this_specimen_aliases) > 0)) {
						$this_specimen_string = "'".implode(";", $this_specimen_aliases)."'";
					} else {
						$this_specimen_string = "NULL";
					}
					
					// codify 'invalid specimen sources' column
					unset($this_invalid_specimen_string);
					unset($this_invalid_specimen_aliases);
					unset($this_invalid_specimen_aliases_raw);
					$this_invalid_specimen_aliases_raw = explode(";", trim($this_row['H']));
					if (is_array($this_invalid_specimen_aliases_raw) && (count($this_invalid_specimen_aliases_raw) > 0)) {
						foreach ($this_invalid_specimen_aliases_raw as $this_invalid_specimen_alias_item) {
							// look up id for matching gateway_xref from Master Value Set...
							$getinvalidspecimen_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_invalid_specimen_alias_item)));
							$getinvalidspecimen_row = @pg_fetch_object(@pg_query($host_pa, $getinvalidspecimen_sql));
							if (intval($getinvalidspecimen_row->id) > 0) {
								$this_invalid_specimen_aliases[] = intval($getinvalidspecimen_row->id);
							}
						}
					}
					if (is_array($this_invalid_specimen_aliases) && (count($this_invalid_specimen_aliases) > 0)) {
						$this_invalid_specimen_string = "'".implode(";", $this_invalid_specimen_aliases)."'";
					} else {
						$this_invalid_specimen_string = "NULL";
					}
					
					// look up id for matching condition from Master Value Set...
					$getmaster_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('condition') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['B'])));
					//echo $getmaster_sql."<br>";
					$getmaster_row = @pg_fetch_object(@pg_query($host_pa, $getmaster_sql));
					
					// look up id for matching Disease Category from Master Value Set...
					$getcategory_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('disease_category') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['A'])));
					//echo $getmaster_sql."<br>";
					$getcategory_row = @pg_fetch_object(@pg_query($host_pa, $getcategory_sql));
					
					// look up id for matching Jurisdiction Override from system_districts...
					unset($getdistrictoverride_id);
					$getdistrictoverride_sql = sprintf("SELECT MAX(id) AS id FROM %ssystem_districts WHERE health_district ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['N'])));
					//echo $getmaster_sql."<br>";
					$getdistrictoverride_row = @pg_fetch_object(@pg_query($host_pa, $getdistrictoverride_sql));
					if (intval($getdistrictoverride_row->id) > 0) {
						$getdistrictoverride_id = intval($getdistrictoverride_row->id);
					} else {
						$getdistrictoverride_id = -1;
					}
					
					if (intval($getmaster_row->id) > 0) {
						//print_r ($this_row);
						$vmc_inserts++;
						$vocab_insert_sql[] = sprintf(
							"INSERT INTO %svocab_master_condition (
								condition, 
								gateway_xref, 
								check_xref_first, 
								immediate_notify, 
								require_specimen, 
								valid_specimen, 
								white_rule, 
								contact_white_rule, 
								new_event, 
								notify_state, 
								autoapproval, 
								disease_category, 
								district_override,
								invalid_specimen) 
							VALUES 
								(%d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %d, %s);", $my_db_schema, 
								intval($getmaster_row->id),
								$this_gateway_string,
								((trim($this_row['D']) == "Yes") ? "'t'" : "'f'"),
								((trim($this_row['E']) == "Yes") ? "'t'" : "'f'"),
								((trim($this_row['F']) == "Yes") ? "'t'" : "'f'"),
								$this_specimen_string,
								((strlen(trim($this_row['I'])) > 0) ? "'".pg_escape_string(trim($this_row['I']))."'" : "NULL"),
								((strlen(trim($this_row['J'])) > 0) ? "'".pg_escape_string(trim($this_row['J']))."'" : "NULL"),
								((trim($this_row['K']) == "New") ? "'t'" : "'f'"),
								((trim($this_row['L']) == "Yes") ? "'t'" : "'f'"),
								((trim($this_row['M']) == "Yes") ? "'t'" : "'f'"),
								intval($getcategory_row->id), 
								intval($getdistrictoverride_id), 
								$this_invalid_specimen_string
								);
					} else {
						die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Condition item.</p></div>");
					}
				}
			}
			
			// Vocabulary source file has now been scanned & INSERTs prepared... time to clear out the old data
			// Because queries are done in a transaction, errors must be collected along the way & dumped after the ROLLBACK
			
			// Before deleting, prepare the INSERT transaction to verify no errors (don't want to dump data if we're not going
			// to be able to insert it later!)
			
			$insert_rs[] = @pg_query($host_pa, "BEGIN");
				$insert_error_rs[] = pg_last_error();
				
			foreach ($vocab_insert_sql as $vocab_insert_exec) {
				$insert_rs[] = @pg_query($host_pa, $vocab_insert_exec);
					$insert_error_rs[] = pg_last_error();
			}
			
			if (in_array(FALSE, $insert_rs)) {
				// bad things happened preparing the INSERTs, run away
				@pg_query("ROLLBACK");
				
				// list-ify all of the errors
				$insert_errors = "";
				foreach ($insert_error_rs as $insert_error_item) {
					if (strlen($insert_error_item) > 0) {
						$insert_errors .= "<li>".$insert_error_item."</li>";
					}
				}
				die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Errors encountered:</p><ul>" . $insert_errors . "</ul></div>");
			} else {
				// name the prepared transaction so we can COMMIT it after successful purging of old data...
				$insert_prepare_token = sprintf("insert_vocab_%s", uniqid('', true));
				$insert_prepare_stmt = sprintf("PREPARE TRANSACTION '%s';", $insert_prepare_token);
				$commit_insert_stmt = sprintf("COMMIT PREPARED '%s'", $insert_prepare_token);
				$rollback_insert_stmt = sprintf("ROLLBACK PREPARED '%s'", $insert_prepare_token);
				
				@pg_query($insert_prepare_stmt);
				
				//$found_stats = sprintf("Found %s new Master Condition records</p>", $vmc_inserts);
				//echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Preparing new vocabulary dataset.  ".$found_stats."</em></div>";
			
			
				// INSERT prepared transaction looks good, let's DELETE!
				$delete_rs[] = @pg_query($host_pa, "BEGIN");
					$delete_error_rs[] = pg_last_error();
					
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master_condition;");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master_loinc;");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master_organism;");
					$delete_error_rs[] = pg_last_error();
				
				if (in_array(FALSE, $delete_rs)) {
					// bad things happened with the delete, run away
					@pg_query("ROLLBACK");
					
					// rollback the prepared INSERT as well
					@pg_query($rollback_insert_stmt);
					
					// list-ify all of the errors
					$delete_errors = "";
					foreach ($delete_error_rs as $error_item) {
						if (strlen($error_item) > 0) {
							$delete_errors .= "<li>".$error_item."</li>";
						}
					}
					die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to purge old Master Condition vocabulary data.  Errors encountered:</p><ul>" . $delete_errors . "</ul></div>");
				} else {
					// commit the DELETE
					@pg_query("COMMIT");
					//echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Old Master Condition vocabulary data successfully purged...</em></p></div>";
					
					// commit the previously-prepared INSERT
					@pg_query($commit_insert_stmt);
					echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>" . $vmc_inserts . " new Master Condition vocabulary records successfully imported!</strong></p></div>";
				}
				
			}
			
		# End Master Condition ########################
		###############################################
		
		
		###############################################
		# Master Organism #############################
			unset($vocab_insert_sql);
			unset($insert_rs);
			unset($insert_error_rs);
			unset($delete_rs);
			unset($delete_error_rs);
			$pxls->setActiveSheetIndexByName("Master Organism");
			$this_sheet = $pxls->getActiveSheet()->toArray(null,true,false,true);
			
			$vmo_inserts = 0;
			foreach ($this_sheet as $this_key => $this_row) {
				unset($org_masterid_lookup);
				// don't insert column headings or any rows with a null/empty SNOMED code OR a null/empty Organism
				if (($this_key > 1) && ((strlen(trim($this_row['C'])) > 1) || (strlen(trim($this_row['E'])) > 1))) {
					if (strlen(trim($this_row['A'])) > 0) {
						$getmastersntype_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('snomed_category') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['A'])));
						//echo $getmastercond_sql;
						$getmastersntype_row = @pg_fetch_object(@pg_query($host_pa, $getmastersntype_sql));
						if (intval($getmastersntype_row->id) > 0) {
							$org_masterid_lookup['snomed_category'] = intval($getmastersntype_row->id);
						} else {
							$org_masterid_lookup['snomed_category'] = -1;
							$missing_vocab['Master Organism']['SNOMED Type'][] = trim($this_row['A']);
							//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Condition item.</p></div>");
						}
					} else {
						$org_masterid_lookup['snomed_category'] = -1;
					}
					
					if (strlen(trim($this_row['B'])) > 0) {
						$getmastercond_sql = sprintf("SELECT MAX(mc.c_id) AS id FROM %svocab_master_vocab mv JOIN %svocab_master_condition mc ON mc.condition = mv.id WHERE mv.category = elr.vocab_category_id('condition') and mv.concept ILIKE '%s'", $my_db_schema, $my_db_schema, pg_escape_string(trim($this_row['B'])));
						//echo $getmastercond_sql;
						$getmastercond_row = @pg_fetch_object(@pg_query($host_pa, $getmastercond_sql));
						if (intval($getmastercond_row->id) > 0) {
							$org_masterid_lookup['condition'] = intval($getmastercond_row->id);
						} else {
							$org_masterid_lookup['condition'] = -1;
							$missing_vocab['Master Organism']['Condition'][] = trim($this_row['B']);
							//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Condition item.</p></div>");
						}
					} else {
						$org_masterid_lookup['condition'] = -1;
					}
					
					if (strlen(trim($this_row['E'])) > 0) {
						$getmasterorg_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('organism') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['E'])));
						//echo $getmasterorg_sql;
						$getmasterorg_row = @pg_fetch_object(@pg_query($host_pa, $getmasterorg_sql));
						if (intval($getmasterorg_row->id) > 0) {
							$org_masterid_lookup['organism'] = intval($getmasterorg_row->id);
						} else {
							$org_masterid_lookup['organism'] = -1;
							$missing_vocab['Master Organism']['Organism'][] = trim($this_row['E']);
							//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Organism item.</p></div>");
						}
					} else {
						$org_masterid_lookup['organism'] = -1;
					}
					
					if (strlen(trim($this_row['F'])) > 0) {
						$getlist_sql = sprintf("SELECT id FROM %ssystem_statuses WHERE type = 2 and name ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['F'])));
						//echo $getmasterorg_sql;
						$getlist_row = @pg_fetch_object(@pg_query($host_pa, $getlist_sql));
						if (intval($getlist_row->id) > 0) {
							$org_masterid_lookup['list'] = intval($getlist_row->id);
						} else {
							$org_masterid_lookup['list'] = -1;
							$missing_vocab['Master Organism']['List'][] = trim($this_row['F']);
							//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Organism item.</p></div>");
						}
					} else {
						$org_masterid_lookup['list'] = -1;
					}
					
					if (strlen(trim($this_row['G'])) > 0) {
						$getmasterresult = trisanoResultMasterVocabIdByName(trim($this_row['G']));
						if (intval($getmasterresult) > 0) {
							$org_masterid_lookup['result'] = intval($getmasterresult);
						} else {
							$org_masterid_lookup['result'] = -1;
							$missing_vocab['Master Organism']['Test Result'][] = trim($this_row['G']);
							//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Case Status item.</p></div>");
						}
					} else {
						$org_masterid_lookup['result'] = -1;
					}
					
					if (strlen(trim($this_row['H'])) > 0) {
						$getmasterstatus_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('case') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['H'])));
						//echo $getmasterstatus_sql;
						$getmasterstatus_row = @pg_fetch_object(@pg_query($host_pa, $getmasterstatus_sql));
						if (intval($getmasterstatus_row->id) > 0) {
							$org_masterid_lookup['status'] = intval($getmasterstatus_row->id);
						} else {
							$org_masterid_lookup['status'] = -1;
							$missing_vocab['Master Organism']['Case Status'][] = trim($this_row['H']);
							//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Case Status item.</p></div>");
						}
					} else {
						$org_masterid_lookup['status'] = -1;
					}
					
					//print_r ($this_row);
					$vmo_inserts++;
					$vocab_insert_sql[] = sprintf(
						"INSERT INTO %svocab_master_organism (
							snomed_category, 
							condition, 
							snomed, 
							snomed_alt, 
							organism, 
							list, 
							test_result, 
							status, 
							nom_is_surveillance) 
						VALUES 
							(%s, %s, %s, %s, %s, %s, %s, %s);", $my_db_schema, 
							$org_masterid_lookup['snomed_category'],
							$org_masterid_lookup['condition'],
							((strlen(trim($this_row['C'])) > 0) ? "'".pg_escape_string(trim($this_row['C']))."'" : "NULL"),
							((strlen(trim($this_row['D'])) > 0) ? "'".pg_escape_string(trim($this_row['D']))."'" : "NULL"),
							$org_masterid_lookup['organism'],
							$org_masterid_lookup['list'],
							$org_masterid_lookup['result'], 
							$org_masterid_lookup['status'], 
							((trim($this_row['I']) == "Yes") ? "'t'" : "'f'") 
							);
				}
			}
			
			// Vocabulary source file has now been scanned & INSERTs prepared... time to clear out the old data
			// Because queries are done in a transaction, errors must be collected along the way & dumped after the ROLLBACK
			
			// Before deleting, prepare the INSERT transaction to verify no errors (don't want to dump data if we're not going
			// to be able to insert it later!)
			
			$insert_rs[] = @pg_query($host_pa, "BEGIN");
				$insert_error_rs[] = pg_last_error();
				
			foreach ($vocab_insert_sql as $vocab_insert_exec) {
				$insert_rs[] = @pg_query($host_pa, $vocab_insert_exec);
					$insert_error_rs[] = pg_last_error();
			}
			
			if (in_array(FALSE, $insert_rs)) {
				// bad things happened preparing the INSERTs, run away
				@pg_query("ROLLBACK");
				
				// list-ify all of the errors
				$insert_errors = "";
				foreach ($insert_error_rs as $insert_error_item) {
					if (strlen($insert_error_item) > 0) {
						$insert_errors .= "<li>".$insert_error_item."</li>";
					}
				}
				die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Errors encountered:</p><ul>" . $insert_errors . "</ul></div>");
			} else {
				// name the prepared transaction so we can COMMIT it after successful purging of old data...
				$insert_prepare_token = sprintf("insert_vocab_%s", uniqid('', true));
				$insert_prepare_stmt = sprintf("PREPARE TRANSACTION '%s';", $insert_prepare_token);
				$commit_insert_stmt = sprintf("COMMIT PREPARED '%s'", $insert_prepare_token);
				$rollback_insert_stmt = sprintf("ROLLBACK PREPARED '%s'", $insert_prepare_token);
				
				@pg_query($insert_prepare_stmt);
				
				//$found_stats = sprintf("Found %s new Master Condition records</p>", $vmo_inserts);
				//echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Preparing new vocabulary dataset.  ".$found_stats."</em></div>";
			
			
				// INSERT prepared transaction looks good, let's DELETE!
				$delete_rs[] = @pg_query($host_pa, "BEGIN");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master_organism;");
					$delete_error_rs[] = pg_last_error();
				
				if (in_array(FALSE, $delete_rs)) {
					// bad things happened with the delete, run away
					@pg_query("ROLLBACK");
					
					// rollback the prepared INSERT as well
					@pg_query($rollback_insert_stmt);
					
					// list-ify all of the errors
					$delete_errors = "";
					foreach ($delete_error_rs as $error_item) {
						if (strlen($error_item) > 0) {
							$delete_errors .= "<li>".$error_item."</li>";
						}
					}
					die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to purge old Master Organism vocabulary data.  Errors encountered:</p><ul>" . $delete_errors . "</ul></div>");
				} else {
					// commit the DELETE
					@pg_query("COMMIT");
					//echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Old Master Organism vocabulary data successfully purged...</em></p></div>";
					
					// commit the previously-prepared INSERT
					@pg_query($commit_insert_stmt);
					echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>" . $vmo_inserts . " new Master Organism vocabulary records successfully imported!</strong></p></div>";
				}
				
			}
			
		# End Master Organism #########################
		###############################################
		
		
		###############################################
		# Master LOINC ################################
			unset($vocab_insert_sql);
			unset($insert_rs);
			unset($insert_error_rs);
			unset($delete_rs);
			unset($delete_error_rs);
			$pxls->setActiveSheetIndexByName("Master LOINC to Condition");
			$this_sheet = $pxls->getActiveSheet()->toArray(null,true,false,true);
			
			$vml_inserts = 0;
			$vml_inserted_loincs = array();
			unset($vml_cmr_rules);
			foreach ($this_sheet as $this_key => $this_row) {
				unset($loinc_masterid_lookup);
				unset($rule_status_lookup);
				unset($this_condition_arr);
				unset($this_condition_script);
				unset($this_condition_obj);
				// don't insert column headings or any rows with a null/empty loinc
				if (($this_key > 1) && (strlen(trim($this_row['A'])) > 1)) {
					/*
					 * Case Management Rule pre-processing
					 */
					if (strlen(trim($this_row['P'])) > 0) {
						// don't even try if there's not a value defined for 'create new cmr?' in the import file for this Master LOINC
						if ((strlen(trim($this_row['L'])) > 0) && (strlen(trim($this_row['M'])) > 0)) {
							// get primary interp rule condition, if it exists
							$this_condition_arr[] = array("operator" => operatorBySymbol(trim($this_row['L'])), "operand" => intval(trisanoResultMasterVocabIdByName(trim($this_row['M']))));
						}
						
						if ((strlen(trim($this_row['N'])) > 0) && (strlen(trim($this_row['O'])) > 0)) {
							// secondary condition (i.e. between) exists
							$this_condition_arr[] = array("operator" => operatorBySymbol(trim($this_row['N'])), "operand" => intval(trisanoResultMasterVocabIdByName(trim($this_row['O']))));
						}
						
						if (isset($this_condition_arr)) {
							$this_condition_script = "(";
							foreach ($this_condition_arr as $this_condition_obj) {
								$this_condition_script .= "(input ".operatorById($this_condition_obj['operator']) . " " . ((is_numeric($this_condition_obj['operand'])) ? $this_condition_obj['operand'] : "'".$this_condition_obj['operand']."'" ) . ") && ";
							}
							$this_condition_script = substr($this_condition_script, 0, strlen($this_condition_script)-4);
							$this_condition_script .= ")";
							
							if (strlen(trim($this_row['R'])) > 0) {
								$getstatus_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('case') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['R'])));
								//echo $getmasterorg_sql;
								$getstatus_row = @pg_fetch_object(@pg_query($host_pa, $getstatus_sql));
								if (intval($getstatus_row->id) > 0) {
									$rule_status_lookup = intval($getstatus_row->id);
								} else {
									$rule_status_lookup = -1;
									//$missing_vocab['Master LOINC']['Rule Status'][] = trim($this_row['R']);
									//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Organism item.</p></div>");
								}
							} else {
								$rule_status_lookup = -1;
							}
							
							
							
							// prepare to insert interpretive rule into $vcl_interp_rules array for later processing
							$vml_cmr_rules[trim($this_row['A'])][] = array(
								"allow_new_cmr" => ((trim($this_row['P']) == "Yes") ? "'t'" : "'f'"),
								"is_surveillance" => ((trim($this_row['Q']) == "Yes") ? "'t'" : "'f'"),
								"state_case_status_master_id" => intval($rule_status_lookup),
								"structured" => json_encode($this_condition_arr),
								"js" => $this_condition_script);
						}
					}
					
					// don't insert master LOINC again if already have inserted this master LOINC
					if (!in_array(trim($this_row['A']), $vml_inserted_loincs)) {
						$vml_inserted_loincs[] = trim($this_row['A']);
						if (strlen(trim($this_row['D'])) > 0) {
							$getmastercond_sql = sprintf("SELECT MAX(mc.c_id) AS id FROM %svocab_master_vocab mv JOIN %svocab_master_condition mc ON mc.condition = mv.id WHERE mv.category = elr.vocab_category_id('condition') and mv.concept ILIKE '%s'", $my_db_schema, $my_db_schema, pg_escape_string(trim($this_row['D'])));
							//echo $getmastercond_sql;
							$getmastercond_row = @pg_fetch_object(@pg_query($host_pa, $getmastercond_sql));
							if (intval($getmastercond_row->id) > 0) {
								$loinc_masterid_lookup['condition'] = intval($getmastercond_row->id);
							} else {
								$loinc_masterid_lookup['condition'] = -1;
								$missing_vocab['Master LOINC']['Condition'][] = trim($this_row['D']);
								//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master LOINC-&gt;Condition item.</p></div>");
							}
						} else {
							$loinc_masterid_lookup['condition'] = -1;
						}
						
						if (strlen(trim($this_row['F'])) > 0) {
							$getmasterorg_sql = sprintf("SELECT MAX(mo.o_id) AS id FROM %svocab_master_vocab mv JOIN %svocab_master_organism mo ON mo.organism = mv.id WHERE mv.category = elr.vocab_category_id('organism') and mv.concept ILIKE '%s'", $my_db_schema, $my_db_schema, pg_escape_string(trim($this_row['F'])));
							//echo $getmasterorg_sql;
							$getmasterorg_row = @pg_fetch_object(@pg_query($host_pa, $getmasterorg_sql));
							if (intval($getmasterorg_row->id) > 0) {
								$loinc_masterid_lookup['organism'] = intval($getmasterorg_row->id);
							} else {
								$loinc_masterid_lookup['organism'] = -1;
								$missing_vocab['Master LOINC']['Organism'][] = trim($this_row['F']);
								//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master LOINC-&gt;Organism item.</p></div>");
							}
						} else {
							$loinc_masterid_lookup['organism'] = -1;
						}
						
						if (strlen(trim($this_row['G'])) > 0) {
							$getmastertest_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('test_type') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['G'])));
							//echo $getmastertest_sql;
							$getmastertest_row = @pg_fetch_object(@pg_query($host_pa, $getmastertest_sql));
							if (intval($getmastertest_row->id) > 0) {
								$loinc_masterid_lookup['test_type'] = intval($getmastertest_row->id);
							} else {
								$loinc_masterid_lookup['test_type'] = -1;
								$missing_vocab['Master LOINC']['Test Type'][] = trim($this_row['G']);
								//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master LOINC-&gt;Test Type item.</p></div>");
							}
						} else {
							$loinc_masterid_lookup['test_type'] = -1;
						}
						
						if (strlen(trim($this_row['H'])) > 0) {
							$getmasterspecimen_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') AND concept ILIKE '%s';", $my_db_schema, pg_escape_string(trim($this_row['H'])));
							//echo $getmasterorg_sql;
							$getmasterspecimen_row = @pg_fetch_object(@pg_query($host_pa, $getmasterspecimen_sql));
							if (intval($getmasterspecimen_row->id) > 0) {
								$loinc_masterid_lookup['specimen_source'] = intval($getmasterspecimen_row->id);
							} else {
								$loinc_masterid_lookup['specimen_source'] = -1;
								$missing_vocab['Master LOINC']['Specimen Source'][] = trim($this_row['H']);
								//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Organism item.</p></div>");
							}
						} else {
							$loinc_masterid_lookup['specimen_source'] = -1;
						}
						
						if (strlen(trim($this_row['I'])) > 0) {
							$getlist_sql = sprintf("SELECT id FROM %ssystem_statuses WHERE type = 2 and name ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['I'])));
							//echo $getmasterorg_sql;
							$getlist_row = @pg_fetch_object(@pg_query($host_pa, $getlist_sql));
							if (intval($getlist_row->id) > 0) {
								$loinc_masterid_lookup['list'] = intval($getlist_row->id);
							} else {
								$loinc_masterid_lookup['list'] = -1;
								$missing_vocab['Master LOINC']['List'][] = trim($this_row['I']);
								//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for Master Organism-&gt;Organism item.</p></div>");
							}
						} else {
							$loinc_masterid_lookup['list'] = -1;
						}
						
						//print_r ($this_row);
						$vml_inserts++;
						$vocab_insert_sql[] = sprintf(
							"INSERT INTO %svocab_master_loinc (
								loinc, 
								concept_name, 
								trisano_condition, 
								trisano_organism, 
								check_master_org, 
								trisano_test_type, 
								specimen_source, 
								list, 
								gray_rule, 
								condition_from_result, 
								organism_from_result) 
							VALUES 
								(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);", $my_db_schema, 
								((strlen(trim($this_row['A'])) > 0) ? "'".pg_escape_string(trim($this_row['A']))."'" : "NULL"),
								((strlen(trim($this_row['B'])) > 0) ? "'".pg_escape_string(trim($this_row['B']))."'" : "NULL"),
								$loinc_masterid_lookup['condition'],
								$loinc_masterid_lookup['organism'],
								((trim($this_row['K']) == "Yes") ? "'t'" : "'f'"),
								$loinc_masterid_lookup['test_type'],
								$loinc_masterid_lookup['specimen_source'],
								$loinc_masterid_lookup['list'],
								((strlen(trim($this_row['J'])) > 0) ? "'".pg_escape_string(trim($this_row['J']))."'" : "NULL"), 
								((trim($this_row['C']) == "Yes") ? "'t'" : "'f'"),
								((trim($this_row['E']) == "Yes") ? "'t'" : "'f'")
								);
					}
				}
			}
			
			// Vocabulary source file has now been scanned & INSERTs prepared... time to clear out the old data
			// Because queries are done in a transaction, errors must be collected along the way & dumped after the ROLLBACK
			
			// Before deleting, prepare the INSERT transaction to verify no errors (don't want to dump data if we're not going
			// to be able to insert it later!)
			
			$insert_rs[] = @pg_query($host_pa, "BEGIN");
				$insert_error_rs[] = pg_last_error();
				
			foreach ($vocab_insert_sql as $vocab_insert_exec) {
				$insert_rs[] = @pg_query($host_pa, $vocab_insert_exec);
					$insert_error_rs[] = pg_last_error();
			}
			
			if (in_array(FALSE, $insert_rs)) {
				// bad things happened preparing the INSERTs, run away
				@pg_query("ROLLBACK");
				
				// list-ify all of the errors
				$insert_errors = "";
				foreach ($insert_error_rs as $insert_error_item) {
					if (strlen($insert_error_item) > 0) {
						$insert_errors .= "<li>".$insert_error_item."</li>";
					}
				}
				die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Errors encountered:</p><ul>" . $insert_errors . "</ul></div>");
			} else {
				// name the prepared transaction so we can COMMIT it after successful purging of old data...
				$insert_prepare_token = sprintf("insert_vocab_%s", uniqid('', true));
				$insert_prepare_stmt = sprintf("PREPARE TRANSACTION '%s';", $insert_prepare_token);
				$commit_insert_stmt = sprintf("COMMIT PREPARED '%s'", $insert_prepare_token);
				$rollback_insert_stmt = sprintf("ROLLBACK PREPARED '%s'", $insert_prepare_token);
				
				@pg_query($insert_prepare_stmt);
				
				//$found_stats = sprintf("Found %s new Master LOINC records</p>", $vml_inserts);
				//echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Preparing new vocabulary dataset.  ".$found_stats."</em></div>";
			
			
				// INSERT prepared transaction looks good, let's DELETE!
				$delete_rs[] = @pg_query($host_pa, "BEGIN");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master_loinc;");
					$delete_error_rs[] = pg_last_error();
				
				if (in_array(FALSE, $delete_rs)) {
					// bad things happened with the delete, run away
					@pg_query("ROLLBACK");
					
					// rollback the prepared INSERT as well
					@pg_query($rollback_insert_stmt);
					
					// list-ify all of the errors
					$delete_errors = "";
					foreach ($delete_error_rs as $error_item) {
						if (strlen($error_item) > 0) {
							$delete_errors .= "<li>".$error_item."</li>";
						}
					}
					die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to purge old Master LOINC vocabulary data.  Errors encountered:</p><ul>" . $delete_errors . "</ul></div>");
				} else {
					// commit the DELETE
					@pg_query("COMMIT");
					//echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Old Master LOINC vocabulary data successfully purged...</em></p></div>";
					
					// commit the previously-prepared INSERT
					@pg_query($commit_insert_stmt);
					echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-check\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>" . $vml_inserts . " new Master LOINC vocuabulary records successfully imported!</strong></p></div>";
					
					if (is_array($missing_vocab)) {
						echo "<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Warning:</strong>  The following vocabulary items could not be found in the Master Value Set:";
						echo "<pre>";
						//print_r ($missing_vocab);
						echo "</pre>";
						foreach ($missing_vocab as $missing_vocab_tab => $missing_vocab_items) {
							echo "<br><strong><em>From " . $missing_vocab_tab . "...<ul>";
							foreach ($missing_vocab_items as $missing_item_type => $missing_item_values) {
								foreach ($missing_item_values as $missing_item_value) {
									echo "<li>" . htmlentities($missing_item_type) . " =&gt; " . htmlentities($missing_item_value) . "</li>";
								}
							}
							echo "</ul>";
						}
						echo "</p></div>";
					}
				}
				
			}
			
		# End Master LOINC ############################
		###############################################
		
		// if Master LOINC, now that we've added them, go back & build Case Management Rules
		if (isset($vml_cmr_rules) && is_array($vml_cmr_rules) && (count($vml_cmr_rules) > 0)) {
			unset($cmr_sql);
			$cmr_sql = "BEGIN;".PHP_EOL;
			foreach ($vml_cmr_rules as $cmr_loinc => $cmr_conditions) {
				foreach ($cmr_conditions as $cmr_condition) {
					$cmr_sql .= "INSERT INTO ".$my_db_schema."vocab_rules_masterloinc (master_loinc_id, allow_new_cmr, is_surveillance, app_id, state_case_status_master_id, conditions_structured, conditions_js) 
						VALUES (
							(SELECT MAX(l_id) FROM ".$my_db_schema."vocab_master_loinc WHERE loinc = '".pg_escape_string(trim($cmr_loinc))."'), 
							".trim($cmr_condition['allow_new_cmr']).", 
							".trim($cmr_condition['is_surveillance']).", 
							1, 
							".((isset($cmr_condition['state_case_status_master_id']) && !is_null($cmr_condition['state_case_status_master_id']) && (strlen($cmr_condition['state_case_status_master_id']) > 0)) ? intval($cmr_condition['state_case_status_master_id']) : "NULL").", 
							'".pg_escape_string($cmr_condition['structured'])."', 
							'".pg_escape_string($cmr_condition['js'])."'
						);".PHP_EOL;
					//$cmr_sql .= "SELECT MAX(id) FROM ".$my_db_schema."vocab_child_loinc WHERE child_loinc = '".pg_escape_string(trim($interp_loinc))."' AND lab_id = ".$clean['lab_id'].";".PHP_EOL;
				}
			}
			//$cmr_sql .= "ROLLBACK;".PHP_EOL;
			$cmr_sql .= "COMMIT;".PHP_EOL;
			@pg_query($cmr_sql);
			echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">New Case Management Rules loaded...</em></p></div>";
			
			/* debug
			echo "<pre>";
			print_r($cmr_sql);
			echo "</pre>";
			*/
		}
		
		echo "<div class=\"import_widget ui-widget ui-corner-all\"><p><button id=\"addanother_button\">Import Another Vocabulary List?</button></p></div>";
		
	} else {
		// everything else except Master Condition/Organism/LOINC
		unset($vocab_insert_sql);
		unset($insert_rs);
		unset($insert_error_rs);
		unset($delete_rs);
		unset($delete_error_rs);
		foreach ($required_tab_list as $import_tab) {
			$pxls->setActiveSheetIndexByName($import_tab);
			$this_sheet = $pxls->getActiveSheet()->toArray(null,true,false,true);
			
			if (($clean['vocab_type'] == "mastervocab") && ($import_tab == "Sheet1")) {
				$vm_inserts = 0;
				foreach ($this_sheet as $this_key => $this_row) {
					// don't insert column headings or any rows with a null/empty master code
					if (($this_key > 1) && (strlen(trim($this_row['C'])) > 1)) {
						// look up category id from structure_category...
						unset($getstructurecategory_sql);
						unset($getstructurecategory_row);
						$getstructurecategory_sql = sprintf("SELECT MAX(id) AS id FROM %sstructure_category WHERE label ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['A'])));
						//echo $getmaster_sql."<br>";
						$getstructurecategory_row = @pg_fetch_object(@pg_query($host_pa, $getstructurecategory_sql));
						
						//print_r ($this_row);
						$vm_inserts++;
						$vocab_insert_sql[] = sprintf(
							"INSERT INTO %svocab_master_vocab (
								category, 
								codeset,
								concept) 
							VALUES 
								(%s, %s, %s);", $my_db_schema, 
								intval($getstructurecategory_row->id),
								((strlen(trim($this_row['B'])) > 0) ? "'".pg_escape_string(trim($this_row['B']))."'" : "NULL"),
								((strlen(trim($this_row['C'])) > 0) ? "'".pg_escape_string(trim($this_row['C']))."'" : "NULL")
								);
					}
				}
			
			} elseif (($clean['vocab_type'] == "trisano") && ($import_tab == "Sheet1")) {
				$vt_inserts = 0;
				foreach ($this_sheet as $this_key => $this_row) {
					// don't insert column headings or any rows with a null/empty master code
					if (($this_key > 1) && (strlen(trim($this_row['C'])) > 1)) {
						$getmaster_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = (SELECT MAX(id) FROM %sstructure_category WHERE label ILIKE '%s') and concept = '%s'", $my_db_schema, $my_db_schema, pg_escape_string(trim($this_row['B'])), pg_escape_string(trim($this_row['C'])));
						//echo $getmaster_sql;
						$getmaster_row = @pg_fetch_object(@pg_query($host_pa, $getmaster_sql));
						if (intval($getmaster_row->id) > 0) {
							//print_r ($getmaster_row);
							$vt_inserts++;
							$vocab_insert_sql[] = sprintf(
								"INSERT INTO %svocab_master2app (
									app_id, 
									master_id,
									coded_value) 
								VALUES 
									(%s, %s, %s);", $my_db_schema, 
									1,
									intval($getmaster_row->id),
									((strlen(trim($this_row['D'])) > 0) ? "'".pg_escape_string(trim($this_row['D']))."'" : "NULL")
									);
						} else {
							die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Vocabulary entry for TriSano item.</p></div>");
						}
					}
				}
			
			} elseif (($clean['vocab_type'] == "child") && ($import_tab == "Child LOINC")) {
				$vcl_inserts = 0;
				$vcl_inserted_loincs = array();
				unset($vcl_interp_rules);
				foreach ($this_sheet as $this_key => $this_row) {
					unset($this_condition_arr);
					unset($this_condition_script);
					unset($this_condition_obj);
					// don't insert column headings or any rows with a null/empty child LOINC code
					if (($this_key > 2) && (strlen(trim($this_row['C'])) > 1)) {
						/*
						 * Interpretive Result Rule pre-processing
						 */
						if (strlen(trim($this_row['Q'])) > 0) {
							// don't even try if there's not result value defined in the import file for this Child LOINC
							if ((strlen(trim($this_row['M'])) > 0) && (strlen(trim($this_row['N'])) > 0)) {
								// get primary interp rule condition, if it exists
								$this_condition_arr[] = array("operator" => operatorBySymbol(trim($this_row['M'])), "operand" => trim($this_row['N']));
							}
							
							if ((strlen(trim($this_row['O'])) > 0) && (strlen(trim($this_row['P'])) > 0)) {
								// secondary condition (i.e. between) exists
								$this_condition_arr[] = array("operator" => operatorBySymbol(trim($this_row['O'])), "operand" => trim($this_row['P']));
							}
							
							if (isset($this_condition_arr)) {
								$this_condition_script = "(";
								foreach ($this_condition_arr as $this_condition_obj) {
									$this_condition_script .= "(input ".operatorById($this_condition_obj['operator']) . " " . ((is_numeric($this_condition_obj['operand'])) ? $this_condition_obj['operand'] : "'".$this_condition_obj['operand']."'" ) . ") && ";
								}
								$this_condition_script = substr($this_condition_script, 0, strlen($this_condition_script)-4);
								$this_condition_script .= ")";
								
								// prepare to insert interpretive rule into $vcl_interp_rules array for later processing
								$vcl_interp_rules[trim($this_row['C'])][trim($this_row['D'])][] = array(
									"result_master_id" => trisanoResultMasterVocabIdByName(trim($this_row['P'])),
									"results_to_comments" => trim($this_row['V']),
									"structured" => json_encode($this_condition_arr),
									"js" => $this_condition_script);
							}
						}
						
						// don't insert child LOINC again if already have inserted this child LOINC
						if (!in_array(trim($this_row['C']).'_'.trim($this_row['A']), $vcl_inserted_loincs)) {
							$vcl_inserted_loincs[] = trim($this_row['C']).'_'.trim($this_row['A']);
							if (strlen(trim($this_row['A'])) > 0) {
								$getmaster_sql = sprintf("SELECT MAX(l_id) AS id FROM %svocab_master_loinc WHERE loinc ILIKE '%s';", $my_db_schema, pg_escape_string(trim($this_row['A'])));
								//echo $getmaster_sql;
								$getmaster_row = @pg_fetch_object(@pg_query($host_pa, $getmaster_sql));
								$this_masterloinc = intval($getmaster_row->id);
								if ($this_masterloinc < 1) {
									$this_masterloinc = -1;
									$missing_vocab['Child LOINC']['Master LOINC'][] = trim($this_row['A']);
									//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master LOINC entry for Child LOINC item.</p></div>");
								}
							} else {
								$this_masterloinc = -1;
							}
							
							// decode result location
							if (strlen(trim($this_row['R'])) > 0) {
								$getresultlocation_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('result_type') and concept ILIKE '%s'", $my_db_schema, pg_escape_string(trim($this_row['R'])));
								//echo $getmaster_sql;
								$getresultlocation_row = @pg_fetch_object(@pg_query($host_pa, $getresultlocation_sql));
								$this_resultlocation = intval($getresultlocation_row->id);
								if ($this_resultlocation < 1) {
									$this_resultlocation = -1;
									$missing_vocab['Child LOINC']['Result Location'][] = trim($this_row['R']);
									//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master LOINC entry for Child LOINC item.</p></div>");
								}
							} else {
								$this_resultlocation = -1;
							}
							
							//print_r ($getmaster_row);
							$vcl_inserts++;
							$vocab_insert_sql[] = sprintf(
								"INSERT INTO %svocab_child_loinc (
									lab_id, 
									master_loinc,
									child_loinc,
									child_orderable_test_code,
									child_resultable_test_code,
									child_concept_name,
									child_alias,
									units,
									refrange, 
									result_location, 
									interpret_results, 
									semi_auto, 
									validate_child_snomed, 
									hl7_refrange, 
									pregnancy, 
									archived) 
								VALUES 
									(%d, %d, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s, %s, %s);", $my_db_schema, 
									intval($clean['lab_id']),
									intval($this_masterloinc),
									((strlen(trim($this_row['C'])) > 0) ? "'".pg_escape_string(trim($this_row['C']))."'" : "NULL"),
									((strlen(trim($this_row['E'])) > 0) ? "'".pg_escape_string(trim($this_row['E']))."'" : "NULL"),
									((strlen(trim($this_row['F'])) > 0) ? "'".pg_escape_string(trim($this_row['F']))."'" : "NULL"),
									((strlen(trim($this_row['G'])) > 0) ? "'".pg_escape_string(trim($this_row['G']))."'" : "NULL"),
									((strlen(trim($this_row['H'])) > 0) ? "'".pg_escape_string(trim($this_row['H']))."'" : "NULL"),
									((strlen(trim($this_row['S'])) > 0) ? "'".pg_escape_string(trim($this_row['S']))."'" : "NULL"),
									((strlen(trim($this_row['T'])) > 0) ? "'".pg_escape_string(trim($this_row['T']))."'" : "NULL"), 
									intval($this_resultlocation),
									((trim($this_row['J']) == "Yes") ? "'t'" : "'f'"), 
									((trim($this_row['K']) == "Yes") ? "'t'" : "'f'"), 
									((trim($this_row['L']) == "Yes") ? "'t'" : "'f'"), 
									((strlen(trim($this_row['U'])) > 0) ? "'".pg_escape_string(trim($this_row['U']))."'" : "NULL"),
									((trim($this_row['I']) == "Yes") ? "'t'" : "'f'"), 
									((trim($this_row['D']) == "Yes") ? "'t'" : "'f'")
									);
						}
					}
				}
			
			} elseif (($clean['vocab_type'] == "child") && ($import_tab == "Child Organism")) {
				$vco_inserts = 0;
				foreach ($this_sheet as $this_key => $this_row) {
					// don't insert column headings or any rows with a null/empty child code
					if (($this_key > 2) && (strlen(trim($this_row['A'])) > 1)) {
						if (strlen(trim($this_row['C'])) > 0) {
							$getmaster_sql = sprintf("SELECT MAX(mo.o_id) AS id FROM %svocab_master_organism mo JOIN %svocab_master_vocab mv ON (mo.organism = mv.id AND mv.concept ILIKE '%s');", $my_db_schema, $my_db_schema, pg_escape_string(trim($this_row['C'])));
							//echo $getmaster_sql."<br><br>";
							$getmaster_row = @pg_fetch_object(@pg_query($host_pa, $getmaster_sql));
							$this_masterorganism = intval($getmaster_row->id);
							if ($this_masterorganism < 1) {
								$this_masterorganism = -1;
								$missing_vocab['Child Organism']['Master Organism Name'][] = trim($this_row['C']);
								//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Organism entry for Child Organism.</p></div>");
							}
						} else {
							$this_masterorganism = -1;
						}
						
						if (strlen(trim($this_row['E'])) > 0) {
							$getmaster_sql = sprintf("SELECT MAX(mo.o_id) AS id FROM %svocab_master_organism mo JOIN %svocab_master_vocab mv ON (mo.organism = mv.id AND mv.concept ILIKE '%s');", $my_db_schema, $my_db_schema, pg_escape_string(trim($this_row['E'])));
							//echo $getmaster_sql."<br><br>";
							$getmaster_row = @pg_fetch_object(@pg_query($host_pa, $getmaster_sql));
							$this_mastertestresult = intval($getmaster_row->id);
							if ($this_mastertestresult < 1) {
								$this_mastertestresult = -1;
								$missing_vocab['Child Organism']['Master Test Result Name'][] = trim($this_row['E']);
								//die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Could not find matching Master Organism entry for Child Organism.</p></div>");
							}
						} else {
							$this_mastertestresult = -1;
						}
						
						//print_r ($getmaster_row);
						$vco_inserts++;
						$vocab_insert_sql[] = sprintf(
							"INSERT INTO %svocab_child_organism (
								lab_id, 
								organism, 
								test_result_id, 
								child_code,
								result_value, 
								comment, 
								app_test_result,
								value,
								test_status) 
							VALUES 
								(%d, %d, %d, %s, %s, %s, %s, %s, %s);", $my_db_schema, 
								intval($clean['lab_id']),
								intval($this_masterorganism),
								intval($this_mastertestresult),
								((strlen(trim($this_row['A'])) > 0) ? "'".pg_escape_string(trim($this_row['A']))."'" : "NULL"),
								((strlen(trim($this_row['F'])) > 0) ? "'".pg_escape_string(trim($this_row['F']))."'" : "NULL"),
								((strlen(trim($this_row['G'])) > 0) ? "'".pg_escape_string(trim($this_row['G']))."'" : "NULL"),
								((strlen(trim($this_row['H'])) > 0) ? "'".pg_escape_string(trim($this_row['H']))."'" : "NULL"),
								((strlen(trim($this_row['I'])) > 0) ? "'".pg_escape_string(trim($this_row['I']))."'" : "NULL"),
								((strlen(trim($this_row['J'])) > 0) ? "'".pg_escape_string(trim($this_row['J']))."'" : "NULL")
								);
					}
				}
			
			} elseif ($import_tab == "Child Vocab") {
				$vc_inserts = 0;
				foreach ($this_sheet as $this_key => $this_row) {
					// don't insert column headings or any rows with a null/empty master code
					if (($this_key > 2) && (strlen(trim($this_row['C'])) > 1)) {
						$getmaster_sql = sprintf("SELECT MAX(id) AS id FROM %svocab_master_vocab WHERE category = (SELECT MAX(id) FROM %sstructure_category WHERE label ILIKE '%s') and concept = '%s'", 
							$my_db_schema, $my_db_schema, 
							pg_escape_string(trim($this_row['A'])), 
							pg_escape_string(trim($this_row['C']))
						);
						//echo $getmaster_sql."<br>";
						$getmaster_row = @pg_fetch_object(@pg_query($host_pa, $getmaster_sql));
						if (intval($getmaster_row->id) > 0) {
							//print_r ($getmaster_row);
							$vc_inserts++;
							$vocab_insert_sql[] = sprintf(
								"INSERT INTO %svocab_child_vocab (
									lab_id,
									master_id,
									concept) 
								VALUES 
									(%s, %s, %s);", 
									$my_db_schema, 
									$clean['lab_id'],
									intval($getmaster_row->id),
									((strlen(trim($this_row['B'])) > 0) ? "'".pg_escape_string(trim($this_row['B']))."'" : "NULL")
									);
						} else {
							$missing_mastervalue[] = array(
								"cat" => trim($this_row['A']),
								"needle" => trim($this_row['C'])
							);
						}
					}
				}
				
				if (is_array($missing_mastervalue)) {
					foreach ($missing_mastervalue as $missing_value) {
						$missing_string .= $missing_value["cat"] . " =&gt; " . $missing_value["needle"] . "<br>";
					}
					die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Could not find matching Master Vocabulary entries for all Child items.<br><br><em><strong>Missing items:</strong><br>" . $missing_string . "</em></p></div>");
				}
			}
		}
	
	
		// Vocabulary source file has now been scanned & INSERTs prepared... time to clear out the old data
		// Because queries are done in a transaction, errors must be collected along the way & dumped after the ROLLBACK
		
		// Before deleting, prepare the INSERT transaction to verify no errors (don't want to dump data if we're not going
		// to be able to insert it later!)
		
		$insert_rs[] = @pg_query($host_pa, "BEGIN");
			$insert_error_rs[] = pg_last_error();
			
		foreach ($vocab_insert_sql as $vocab_insert_exec) {
			$insert_rs[] = @pg_query($host_pa, $vocab_insert_exec);
				$insert_error_rs[] = pg_last_error();
		}
		
		if (in_array(FALSE, $insert_rs)) {
			// bad things happened preparing the INSERTs, run away
			@pg_query("ROLLBACK");
			
			// list-ify all of the errors
			$insert_errors = "";
			foreach ($insert_error_rs as $insert_error_item) {
				if (strlen($insert_error_item) > 0) {
					$insert_errors .= "<li>".$insert_error_item."</li>";
				}
			}
			die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to prepare to insert new vocabulary.  Errors encountered:</p><ul>" . $insert_errors . "</ul></div>");
		} else {
			// name the prepared transaction so we can COMMIT it after successful purging of old data...
			$insert_prepare_token = sprintf("insert_vocab_%s", uniqid('', true));
			$insert_prepare_stmt = sprintf("PREPARE TRANSACTION '%s';", $insert_prepare_token);
			$commit_insert_stmt = sprintf("COMMIT PREPARED '%s'", $insert_prepare_token);
			$rollback_insert_stmt = sprintf("ROLLBACK PREPARED '%s'", $insert_prepare_token);
			
			@pg_query($insert_prepare_stmt);
			
			if ($clean['vocab_type'] == "child") {
				$found_stats = sprintf("%s new Child LOINC records and %s new Child Organism records found...", $vcl_inserts, $vco_inserts);
			} elseif ($clean['vocab_type'] == "mastervocab") {
				$found_stats = sprintf("%s new Master Vocab records found...", $vm_inserts);
			} elseif ($clean['vocab_type'] == "childvocab") {
				$found_stats = sprintf("%s new Child Vocab records found...", $vc_inserts);
			} elseif ($clean['vocab_type'] == "trisano") {
				$found_stats = sprintf("%s new TriSano Vocab records found...", $vt_inserts);
			}
			echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Preparing new vocabulary dataset.  ".$found_stats."</em></div>";
		
		
			// INSERT prepared transaction looks good, let's DELETE!
			$delete_rs[] = @pg_query($host_pa, "BEGIN");
				$delete_error_rs[] = pg_last_error();
				
			if ($clean['vocab_type'] == "child") {
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_child_organism WHERE lab_id = {$clean['lab_id']};");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_c2m_testresult WHERE child_loinc_id IN (SELECT id FROM {$my_db_schema}vocab_child_loinc WHERE lab_id = {$clean['lab_id']}) AND app_id = 1;");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_child_loinc WHERE lab_id = {$clean['lab_id']};");
					$delete_error_rs[] = pg_last_error();
			} elseif ($clean['vocab_type'] == "mastervocab") {
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master2app;");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_child_vocab;");
					$delete_error_rs[] = pg_last_error();
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master_vocab;");
					$delete_error_rs[] = pg_last_error();
			} elseif ($clean['vocab_type'] == "trisano") {
				$delete_rs[] = @pg_query($host_pa, "DELETE FROM {$my_db_schema}vocab_master2app WHERE app_id = 1;");
					$delete_error_rs[] = pg_last_error();
			}
			
			if (in_array(FALSE, $delete_rs)) {
				// bad things happened with the delete, run away
				@pg_query("ROLLBACK");
				
				// rollback the prepared INSERT as well
				@pg_query($rollback_insert_stmt);
				
				// list-ify all of the errors
				$delete_errors = "";
				foreach ($delete_error_rs as $error_item) {
					if (strlen($error_item) > 0) {
						$delete_errors .= "<li>".$error_item."</li>";
					}
				}
				die ("<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Could not complete vocabulary input:</strong>  Unable to purge old vocabulary data.  Errors encountered:</p><ul>" . $delete_errors . "</ul></div>");
			} else {
				// commit the DELETE
				@pg_query("COMMIT");
				echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">Old vocabulary data successfully purged...</em></p></div>";
				
				// commit the previously-prepared INSERT
				@pg_query($commit_insert_stmt);
				
				// if child LOINC, now that we've added them, go back & build Interpretive Rules
				if (isset($vcl_interp_rules) && is_array($vcl_interp_rules) && (count($vcl_interp_rules) > 0)) {
					unset($interp_sql);
					$interp_sql = "BEGIN;".PHP_EOL;
					foreach ($vcl_interp_rules as $interp_loinc => $interp_archive_yn) {
						foreach ($interp_archive_yn as $interp_archive_yn_key => $interp_conditions) {
							foreach ($interp_conditions as $interp_condition) {
								$interp_sql .= "INSERT INTO ".$my_db_schema."vocab_c2m_testresult (child_loinc_id, master_id, app_id, results_to_comments, conditions_structured, conditions_js) 
									VALUES (
										(SELECT MAX(id) FROM ".$my_db_schema."vocab_child_loinc WHERE (archived ".((trim($interp_archive_yn_key) == 'Yes') ? 'IS TRUE' : 'IS FALSE').") AND (child_loinc = '".pg_escape_string(trim($interp_loinc))."') AND (lab_id = ".$clean['lab_id'].")), 
										".intval($interp_condition['result_master_id']).", 
										1, 
										".((isset($interp_condition['results_to_comments']) && !is_null($interp_condition['results_to_comments']) && (strlen($interp_condition['results_to_comments']) > 0)) ? "'".pg_escape_string($interp_condition['results_to_comments'])."'" : "NULL").", 
										'".pg_escape_string($interp_condition['structured'])."', 
										'".pg_escape_string($interp_condition['js'])."'
									);".PHP_EOL;
								//$interp_sql .= "SELECT MAX(id) FROM ".$my_db_schema."vocab_child_loinc WHERE child_loinc = '".pg_escape_string(trim($interp_loinc))."' AND lab_id = ".$clean['lab_id'].";".PHP_EOL;
							}
						}
					}
					//$interp_sql .= "ROLLBACK;".PHP_EOL;
					$interp_sql .= "COMMIT;".PHP_EOL;
					@pg_query($interp_sql);
					echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><em style=\"color: dimgray;\">New Result Interpretive Rules loaded...</em></p></div>";
					
					/* debug
					echo "<pre>";
					print_r($interp_sql);
					echo "</pre>";
					*/
				}
				
				echo "<div class=\"import_widget ui-widget import_error ui-state-highlight ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-elrsuccess\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>New vocabulary successfully imported!</strong></p><p><button id=\"addanother_button\">Import Another Vocabulary List?</button></p></div>";
				
				if (is_array($missing_vocab)) {
					echo "<div class=\"import_widget ui-widget import_error ui-state-error ui-corner-all\" style=\"padding: 5px;\"><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span><p style=\"margin-left: 20px;\"><strong>Warning:</strong>  No parent vocabulary match was found for the following items, but the records were imported with blank values in the respective fields:";
					echo "<pre>";
					//print_r ($missing_vocab);
					echo "</pre>";
					foreach ($missing_vocab as $missing_vocab_tab => $missing_vocab_items) {
						echo "<br><strong><em>From " . $missing_vocab_tab . "...<ul>";
						foreach ($missing_vocab_items as $missing_item_type => $missing_item_values) {
							foreach ($missing_item_values as $missing_item_value) {
								echo "<li>" . htmlentities($missing_item_type) . " =&gt; " . htmlentities($missing_item_value) . "</li>";
							}
						}
						echo "</ul>";
					}
					echo "</p></div>";
				}
			}
			
		}
	}
	
?>
