<?php

	exit;

	set_time_limit(300);

	$files = array(
		'address' => './manage/import/upload/tbl_addresses.txt',
		'dob' => './manage/import/upload/tbl_birthdate.txt',
		'fname' => './manage/import/upload/tbl_firstname.txt',
		'lname' => './manage/import/upload/tbl_lastname.txt'
	);
	
	$sql = 'BEGIN;'.PHP_EOL;
	foreach ($files as $category => $path) {
		$h = fopen($path, 'r');
		while (($line = fgetcsv($h)) !== false) {
			if ($category === 'address') {
				$sql .= 'INSERT INTO elr.random_address (street, city, zip, state, county)
						VALUES (
							\''.pg_escape_string(iconv('Windows-1252', 'UTF-8', $line[0])).'\', 
							\''.pg_escape_string(iconv('Windows-1252', 'UTF-8', $line[1])).'\', 
							\''.pg_escape_string(iconv('Windows-1252', 'UTF-8', $line[2])).'\', 
							\''.pg_escape_string(iconv('Windows-1252', 'UTF-8', $line[3])).'\', 
							\''.pg_escape_string(iconv('Windows-1252', 'UTF-8', $line[4])).'\'
						);'.PHP_EOL;
			} elseif ($category === 'dob') {
				$sql .= 'INSERT INTO elr.random_dob (dob) VALUES (\''.date("Y-m-d H:i:s", strtotime(trim($line[0]))).'\');'.PHP_EOL;
			} elseif ($category === 'fname') {
				$sql .= 'INSERT INTO elr.random_fname (name) VALUES (\''.pg_escape_string(iconv('Windows-1252', 'UTF-8', $line[0])).'\');'.PHP_EOL;
			} elseif ($category === 'lname') {
				$sql .= 'INSERT INTO elr.random_lname (name) VALUES (\''.pg_escape_string(iconv('Windows-1252', 'UTF-8', $line[0])).'\');'.PHP_EOL;
			}
		}
		
	}
	$sql .= 'COMMIT;';
	
	pg_query($host_pa, $sql);
	echo 'done';
	
?>