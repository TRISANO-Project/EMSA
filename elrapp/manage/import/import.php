<?php

	$lab_qry = "SELECT id, ui_name FROM ".$my_db_schema."structure_labs ORDER BY ui_name";
	$lab_rs = pg_fetch_all(pg_query($host_pa, $lab_qry));
	
?>

<script>
	$(function() {
		$( "#upload_button" ).button({
			icons: {
				primary: "ui-icon-elrsave"
			}
		}).click(function(){
			$("#import_uploader").submit();
		});
		
		$("#vocab_type_mastervocab").click(function() {
			$("#vocab_child_lab").hide('fast');
		});
		
		$("#vocab_type_trisano").click(function() {
			$("#vocab_child_lab").hide('fast');
		});
		
		$("#vocab_type_master").click(function() {
			$("#vocab_child_lab").hide('fast');
		});
		
		$("#vocab_type_child").click(function() {
			$("#vocab_child_lab").show('fast');
		});
		
		$("#vocab_type_childvocab").click(function() {
			$("#vocab_child_lab").show('fast');
		});
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrimport"></span><?php echo $clean["vocab_verbose"]; ?>Import New Vocabulary</h1>

<div class="import_widget ui-widget" style="font-family: 'Open Sans', Arial, Helvetica;">
	<div>
		<p>To mass-import a new vocabulary definition, select the Excel workbook below and click <em>Import New Vocabulary</em>.</p>
	</div>
	<?php highlight("<strong>Warning:</strong>  This upload will replace ALL existing vocabulary definitions stored in the database for the selected vocabulary type.  This action cannot be undone.  Please ensure that all definitions are accurate & complete before continuing.</p>"); ?>
</div>

<div class="import_widget ui-widget ui-widget-content ui-corner-all" style="padding: 5px; font-family: 'Open Sans', Arial, Helvetica;">
	<form name="import_uploader" id="import_uploader" method="POST" enctype="multipart/form-data">
		<p><label class="vocab_search_form">Which vocabulary type are you uploading?</label></p>
		<p style="margin-bottom: 20px;">
			<input type="radio" name="vocab_type" id="vocab_type_mastervocab" value="mastervocab"> <label for="vocab_type_mastervocab">Master Dictionary <code>[Master_Value_Set.xls]</code></label><br>
			<input type="radio" name="vocab_type" id="vocab_type_trisano" value="trisano"> <label for="vocab_type_trisano">TriSano Dictionary <code>[Application_Value_Set.xls]</code></label><br>
			<input type="radio" name="vocab_type" id="vocab_type_master" value="master"> <label for="vocab_type_master">Master LOINC/Condition/SNOMED <code>[MasterLoinc.xls]</code></label><br>
			<input type="radio" name="vocab_type" id="vocab_type_child" value="child"> <label for="vocab_type_child">Child LOINC/SNOMED (Specify Lab) <code>[&lt;LabName&gt;_Child_Values.xls]</code></label><br>
			<input type="radio" name="vocab_type" id="vocab_type_childvocab" value="childvocab"> <label for="vocab_type_childvocab">Child Value Set (Specify Lab) <code>[&lt;LabName&gt;_Child_Values.xls]</code></label><br>
			<select class="ui-corner-all" name="vocab_child_lab" id="vocab_child_lab" style="display: none; margin-left: 25px;">
				<option value="" selected>--</option>
			<?php
				foreach ($lab_rs as $lab) {
					printf("<option value=\"%d\">%s</option>", intval($lab['id']), htmlentities($lab['ui_name']));
				}
			?>
			</select>
		</p>
		<p><label class="vocab_search_form">Select an Excel workbook to import:</label></p>
		<p style="margin-bottom: 20px;"><input type="file" name="vocab_source" id="vocab_source" class="ui-corner-all"></p>
		<p><button id="upload_button">Import New Vocabulary</button></p>
		<input type="hidden" name="import_flag" id="import_flag" value="1">
		<input type="hidden" name="selected_page" id="selected_page" value="6">
		<input type="hidden" name="submenu" id="submenu" value="3">
		<input type="hidden" name="vocab" id="vocab" value="6">
	</form>
</div>