<?php

	/**
	 * Creates a compressed Zip archive and unlinks original files
	 * (Original source from David Walsh -- http://davidwalsh.name/create-zip-php)
	 * 
	 * @param array $files Array of filenames to zip together
	 * @param string $destination Target filename for Zip archive
	 * @param bool $overwrite If TRUE and target file already exists, file will be overwritten.  If FALSE and target file exists, createZip will return FALSE.
	 * @return bool TRUE if Zip archive is created successfully, FALSE if file already exists or file could not be created.
	 */
	function createZip($files = array(), $destination = '', $overwrite = false) {
		//if the zip file already exists and overwrite is false, return false
		if(file_exists($destination) && !$overwrite) { return false; }
		//vars
		$valid_files = array();
		//if files were passed in...
		if(is_array($files)) {
			//cycle through each file
			foreach($files as $file) {
				//make sure the file exists
				if(file_exists($file)) {
					$valid_files[] = $file;
				}
			}
		}
		//if we have good files...
		if(count($valid_files)) {
			//create the archive
			$zip = new ZipArchive();
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
				return false;
			}
			//add the files
			foreach($valid_files as $file) {
				$zip->addFile($file, basename($file));
			}
			//debug
			//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
			
			//close the zip -- done!
			$zip->close();
			
			// get rid of the files we zipped
			foreach ($valid_files as $kill_file) {
				unlink($kill_file);
			}
			
			// stream to download
			if (file_exists($destination)) {
				ob_clean();
				header('Pragma: public');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Cache-Control: public');
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename='.basename($destination));
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: '.filesize($destination));
				readfile($destination);
				unlink($destination);
				return true;
			} else {
				echo 'Error happened';
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	
	
	
	/**
	 * Decodes JSON-encoded interpretive rules data and converts it into exportable text.
	 *
	 * @param string $json_string JSON-encoded object to decode.
	 * @param string $tartget_var Variable to retrieve from the selected object.
	 * @param int $index Zero-based integer representing the index of the decoded object to search for.  [Optional; Defaults to first object if not specified.]
	 * @param bool $lookup_app_value Indicates whether the value decoded from the JSON object is a vocabulary ID that should be looked up [Optional; Default FALSE]
	 * @param int $app_id Specifies the application ID to look up values if $lookup_app_value is TRUE. [Optional; Defaults to 1 (NEDSS)]
	 * @return string Value from index->target_var, if present.  Empty string otherwise.
	 */
	function decodeJSONForExport($json_string = '', $target_var = '', $index = 0, $lookup_app_value = false, $app_id = 1) {
		$decoded_string = '';
		if (empty($json_string) || empty($target_var)) {
			return $decoded_string;
		}
		
		$json_array = @json_decode($json_string);
		if (isset($json_array) && ($json_array !== false) && is_array($json_array)) {
			if ($lookup_app_value && isset($json_array[$index]->$target_var)) {
				$decoded_string = getAppValueForDecodedID(intval($json_array[$index]->$target_var), intval($app_id));
			} elseif (isset($json_array[$index]->$target_var)) {
				$decoded_string = trim($json_array[$index]->$target_var);
			}
		}
		
		if ($target_var == 'operator' && !is_null($decoded_string) && !empty($decoded_string)) {
			$decoded_string = operatorById(intval($decoded_string));
			if ($decoded_string == '==') {
				$decoded_string = ' =';
			}
			if ($decoded_string == '!=') {
				$decoded_string = '<>';
			}
		}
		
		return $decoded_string;
	}
	
	
	
	
	/**
	 * Returns the application-specific vocabulary value for a given ID decoded from JSON-formatted rules.
	 * 
	 * @param int $master_id Master vocabulary ID decoded from JSON.
	 * @param int $app_id Application ID [Optional, default 1 (NEDSS)]
	 * @return string
	 */
	function getAppValueForDecodedID($master_id = null, $app_id = 1) {
		global $host_pa, $my_db_schema;
		
		if (is_null($master_id) || empty($master_id)) {
			return '';
		}
		
		$sql = 'SELECT initcap(coded_value) AS coded_value FROM '.$my_db_schema.'vocab_master2app WHERE app_id = '.intval($app_id).' AND master_id = '.intval($master_id).';';
		$result = @pg_fetch_result(@pg_query($host_pa, $sql), 0, 'coded_value');
		
		return trim($result);
	}

?>