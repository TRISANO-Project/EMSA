<script>
	$(function() {
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_element").focus();
			$(this).hide();
		});
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_savelab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$(".edit_lab").button({
            icons: { primary: "ui-icon-elrpencil" }
        }).next().button({
            icons: { primary: "ui-icon-elrclose" }
        }).parent().buttonset();
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false,
			width: 400
		});
		
		$(".delete_lab").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=4&cat=2&delete_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$("#edit_lab_dialog").dialog({
			autoOpen: false,
			width: 600,
			modal: true
		});
		
		$(".edit_lab").click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id) {
				$("#edit_id").val(jsonObj.id);
				$("#edit_element").val(jsonObj.element);
				$("#edit_xpath").val(jsonObj.xpath);
				$("#edit_category").val(jsonObj.category);
				$("#edit_datatype").val(jsonObj.datatype);
				if (jsonObj.required == "t") {
					$("#edit_required_yes").click();
				} else {
					$("#edit_required_no").click();
				}
				
				$("#edit_lab_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#edit_modal_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#edit_lab_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
	});
</script>

<?php

	if (isset($_GET['edit_id'])) {
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['edit_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to save changes to Master XML element.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to save changes to mapping -- Master XML element does not exist.");
		} else {
			$edit_sql = sprintf("UPDATE %sstructure_path SET element = %s, xpath = %s, data_type_id = %d, required = %s, category_id = %s WHERE id = %d;",
				$my_db_schema,
				((strlen(trim($_GET['edit_element'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_element']))."'" : "NULL"),
				((strlen(trim($_GET['edit_xpath'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_xpath']))."'" : "NULL"),
				((intval(trim($_GET['edit_datatype'])) > 0) ? intval(trim($_GET['edit_datatype'])) : -1),
				((trim($_GET['edit_required']) == "true") ? "true" : "false"),
				((intval(trim($_GET['edit_category'])) > 0) ? intval(trim($_GET['edit_category'])) : "NULL"),
				intval(trim($_GET['edit_id']))
			);
			if (@pg_query($host_pa, $edit_sql)) {
				highlight("Master XML element successfully updated!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to save changes to Master XML element.", 1);
			}
		}
	} elseif (isset($_GET['delete_id'])) {
		########## delete lab ##########
		
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to delete Master XML element.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to delete Master XML element -- record not found.");
		} else {
			// check for alias labs that depend on this row, throw a dependency warning instead of deleting...
			$dependency_sql = sprintf("SELECT (SELECT count(id) FROM %sstructure_path_rule WHERE path_id = %d)+(SELECT count(id) FROM %sstructure_path_application WHERE structure_path_id = %d) AS counter;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])), $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
			$dependency_result = @pg_query($host_pa, $dependency_sql) or suicide("Unable to delete Master XML element.", 1, 1);
			$dependency_count = @pg_fetch_result($dependency_result, 0, "counter");
			if ($dependency_count > 0) {
				suicide("Unable to delete Master XML Element -- ".$dependency_count." Application XML Structure and/or XML Rule entries use this XML Element Path.  Please resolve these dependency conflicts first and try again.");
			} else {
				// everything checks out, commit the delete...
				$delete_sql = sprintf("DELETE FROM ONLY %sstructure_path WHERE id = %d;", $my_db_schema, intval($_GET['delete_id']));
				if (@pg_query($host_pa, $delete_sql)) {
					highlight("Master XML element successfully deleted!", "ui-icon-elrsuccess");
				} else {
					suicide("Unable to delete Master XML element.", 1);
				}
			}
		}
	} elseif (isset($_GET['add_flag'])) {
		// add new lab
		if (strlen(trim($_GET['new_element'])) > 0) {
			$addlab_sql = sprintf("INSERT INTO %sstructure_path (element, xpath, data_type_id, required, category_id) VALUES (%s, %s, %d, %s, %s)",
				$my_db_schema,
				"'".pg_escape_string(trim($_GET['new_element']))."'",
				((strlen(trim($_GET['new_xpath'])) > 0) ? "'".pg_escape_string(trim($_GET['new_xpath']))."'" : "NULL"),
				((intval(trim($_GET['new_datatype'])) > 0) ? intval(trim($_GET['new_datatype'])) : -1),
				((trim($_GET['new_required']) == "true") ? "true" : "false"),
				((intval(trim($_GET['new_category'])) > 0) ? intval(trim($_GET['new_category'])) : -1)
			);
			@pg_query($host_pa, $addlab_sql) or suicide("Could not add new Master XML element.", 1);
			highlight("New Master XML element \"".htmlentities(trim($_GET['new_element']))."\" added successfully!", "ui-icon-elrsuccess");
		} else {
			suicide("No Master XML element name specified!  Enter an element name and try again.");
		}
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrxml"></span>Master XML Structure</h1>

<div class="vocab_search ui-tabs ui-widget">
<button id="addnew_button" title="Add a new Master XML element">Add New Master XML Element</button>
</div>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New Master XML Element:</label><br><br></div>
	<form id="new_lab_form" method="GET" action="<?php echo $main_page; ?>">
		<label class="vocab_search_form2" for="new_element">Field Name:</label><input class="ui-corner-all" type="text" name="new_element" id="new_element" />
		<label class="vocab_search_form2" for="new_xpath">Element XPath:</label><input class="ui-corner-all" type="text" name="new_xpath" id="new_xpath" />
		<br><br><label class="vocab_search_form2" for="new_datatype">Data Type:</label>
			<select class="ui-corner-all" name="new_datatype" id="new_datatype">
				<option value="0" selected>--</option>
			<?php
				// get list of data types for menu
				$datatype_sql = sprintf("SELECT DISTINCT id, label FROM %sstructure_data_type ORDER BY label;", $my_db_schema);
				$datatype_rs = @pg_query($host_pa, $datatype_sql) or suicide("Unable to retrieve list of data types.", 1, 1);
				while ($datatype_row = pg_fetch_object($datatype_rs)) {
					printf("<option value=\"%d\">%s</option>", intval($datatype_row->id), htmlentities($datatype_row->label));
				}
				pg_free_result($datatype_rs);
			?>
			</select>
		<label class="vocab_search_form2" for="new_category">Vocab Category:</label>
			<select class="ui-corner-all" name="new_category" id="new_category">
				<option value="0" selected>--</option>
			<?php
				// get list of categories for menu
				$category_sql = sprintf("SELECT DISTINCT id, label FROM %sstructure_category ORDER BY label;", $my_db_schema);
				$category_rs = @pg_query($host_pa, $category_sql) or suicide("Unable to retrieve list of categories.", 1, 1);
				while ($category_row = pg_fetch_object($category_rs)) {
					printf("<option value=\"%d\">%s</option>", intval($category_row->id), htmlentities($category_row->label));
				}
				pg_free_result($category_rs);
			?>
			</select>
		<br><br><label class="vocab_search_form2">Required?:</label>
			<label class="vocab_search_form2" for="new_required_yes"><input class="edit_radio ui-corner-all" type="radio" name="new_required" id="new_required_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label class="vocab_search_form2" for="new_required_no"><input class="edit_radio ui-corner-all" type="radio" name="new_required" id="new_required_no" value="false" /> No</label>
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_savelab" id="new_savelab">Save New Master XML Element</button>
		<button type="button" id="addnew_cancel">Cancel</button>
	</form>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th>Field Name</th>
				<th>Element XPath</th>
				<th>Category</th>
				<th>Data Type</th>
				<th>Required?</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$xml_qry = sprintf("SELECT xm.id AS id, xm.element AS element, xm.xpath AS xpath, xm.category_id AS category_id, xm.required AS required, xt.id AS datatype_id, xt.label AS datatype, xc.label AS category_label FROM %sstructure_path xm LEFT JOIN %sstructure_data_type xt ON (xm.data_type_id = xt.id) LEFT JOIN %sstructure_category xc ON (xm.category_id = xc.id) ORDER BY xm.xpath", $my_db_schema, $my_db_schema, $my_db_schema);
	$xml_rs = @pg_query($host_pa, $xml_qry) or die("Could not connect to database: ".pg_last_error());
	
	while ($xml_row = @pg_fetch_object($xml_rs)) {
		echo "<tr>";
		printf("<td class=\"action_col\">");
		unset($edit_lab_params);
		$edit_lab_params = array(
			"id" => intval($xml_row->id), 
			"element" => htmlentities($xml_row->element, ENT_QUOTES, "UTF-8"), 
			"xpath" => htmlentities($xml_row->xpath, ENT_QUOTES, "UTF-8"), 
			"category" => htmlentities($xml_row->category_id, ENT_QUOTES, "UTF-8"), 
			"datatype" => intval($xml_row->datatype_id), 
			"required" => trim($xml_row->required)
		);
		printf("<button class=\"edit_lab\" type=\"button\" value='%s' title=\"Edit this record\">Edit</button>", json_encode($edit_lab_params));
		printf("<button class=\"delete_lab\" type=\"button\" value=\"%s\" title=\"Delete this record\">Delete</button>", $xml_row->id);
		echo "</td>";
		echo "<td>".htmlentities($xml_row->element)."</td>";
		echo "<td>".htmlentities($xml_row->xpath)."</td>";
		echo "<td>".htmlentities($xml_row->category_label)."</td>";
		echo "<td>".htmlentities($xml_row->datatype)."</td>";
		
		echo "<td>".((trim($xml_row->required) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Required\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"Not Required\"></span>")."</td>";
		echo "</tr>";
	}
	
	pg_free_result($xml_rs);

?>

		</tbody>
	</table>
	
</div>

<div id="confirm_delete_dialog" title="Delete this Master XML Element?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This Master XML element will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="edit_lab_dialog" title="Edit Master XML Element">
	<form id="edit_modal_form" method="GET" action="<?php echo $main_page; ?>">
		<label for="edit_element">Field Name:</label><br><input class="ui-corner-all" type="text" name="edit_element" id="edit_element" /><br><br>
		<label for="edit_xpath">Element XPath:</label><br><input class="ui-corner-all" type="text" name="edit_xpath" id="edit_xpath" /><br><br>
		<label for="edit_datatype">Data Type:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_datatype" id="edit_datatype">
			<option value="0" selected>--</option>
		<?php
			// get list of data types for menu
			$datatype_sql = sprintf("SELECT DISTINCT id, label FROM %sstructure_data_type ORDER BY label;", $my_db_schema);
			$datatype_rs = @pg_query($host_pa, $datatype_sql) or suicide("Unable to retrieve list of data types.", 1, 1);
			while ($datatype_row = pg_fetch_object($datatype_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($datatype_row->id), htmlentities($datatype_row->label));
			}
			pg_free_result($datatype_rs);
		?>
		</select><br><br>
		<label for="edit_category">Vocab Category:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_category" id="edit_category">
			<option value="0" selected>--</option>
		<?php
			// get list of data types for menu
			$category_sql = sprintf("SELECT DISTINCT id, label FROM %sstructure_category ORDER BY label;", $my_db_schema);
			$category_rs = @pg_query($host_pa, $category_sql) or suicide("Unable to retrieve list of categories.", 1, 1);
			while ($category_row = pg_fetch_object($category_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($category_row->id), htmlentities($category_row->label));
			}
			pg_free_result($category_rs);
		?>
		</select><br><br>
		<label>Required?:</label><br>
			<label for="edit_required_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_required" id="edit_required_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label for="edit_required_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_required" id="edit_required_no" value="false" /> No</label>
		<input type="hidden" name="edit_id" id="edit_id" />
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
	</form>
</div>