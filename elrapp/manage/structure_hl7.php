<?php

	########## Session Prep ##########
	$_SESSION["structure_params"]["structure"] = 5;
	
	/*
	 * check for session freshness since last update to session-stored filter info
	 * (filters are stored in session data, this hack gives us a way to force
	 * session data to be refreshed without forcing users to clear cookies if the data
	 * is updated mid-session, so that the current filters are used)
	 */
	$model_last_updated = filemtime("manage/structure_hl7.php");
	
	// check "freshness date"...
	if (isset($_SESSION['structure_model_fresh'][$_SESSION["structure_params"]["structure"]])) {
		if ($_SESSION['structure_model_fresh'][$_SESSION["structure_params"]["structure"]] < $model_last_updated) {
			// old model data; unset structure_params & set a new "freshness date"...
			unset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]);
			$_SESSION['structure_model_fresh'][$_SESSION["structure_params"]["structure"]] = time();
		}
	} else {
		// hack for sessions set before "freshness date" implemented
		unset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]);
		$_SESSION['structure_model_fresh'][$_SESSION["structure_params"]["structure"]] = time();
	}
	
	
	// define filters
	$_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]['filter_cols'] = array(
		"lab" => array("colname" => "h.lab_id", "label" => "Lab", "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "has_id" => TRUE, "lookupqry" => sprintf("SELECT sl.id AS value, sl.ui_name AS label FROM %sstructure_labs sl WHERE sl.alias_for < 1 ORDER BY sl.ui_name;", $my_db_schema), "filtercolname" => "lab_id"), 
		"hl7_version" => array("colname" => "h.message_version", "label" => "Message Version", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "has_id" => FALSE, "lookupqry" => "SELECT DISTINCT message_version AS label FROM ".$my_db_schema."structure_path_mirth ORDER BY message_version;", "filtercolname" => "message_version"),
		"master_path" => array("colname" => "p.xpath", "label" => "Master XML Path", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "has_id" => TRUE, "lookupqry" => sprintf("SELECT DISTINCT id AS value, element||' ('||xpath||')' AS label FROM %sstructure_path ORDER BY 2;", $my_db_schema), "filtercolname" => "master_path_id"), 
		"hl7_path" => array("colname" => "h.xpath", "label" => "HL7 XML Path", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE)
	);
	
	
	/*
	 * Search/Filter Prep
	 * 
	 * this must happen after setting structure defaults, otherwise condition can occur where setting query params can
	 * fool the sysetm into thinking default structure data exists when it doesn't in cases of a linked query
	 */
	// pre-build our structure-specific search data...
	if (isset($_GET["q"])) {
		if ((trim($_GET["q"]) != "Enter search terms...") && (strlen(trim($_GET["q"])) > 0)) {
			$_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_display"] = htmlentities(trim($_GET["q"]), ENT_QUOTES, "UTF-8");
			$_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_sql"] = pg_escape_string(trim($_GET["q"]));
			if (!isset($_GET['f'])) {
				// search query found, but no filters selected
				// if any filters were previously SESSIONized, they've been deselected via the UI, so we'll clear them...
				unset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filters"]);
			}
		} else {
			// search field was empty/defaulted, so we'll destroy the saved search params...
			unset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_display"]);
			unset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_sql"]);
			// not only was search blank, but no filters selected, so clear them as well...
			unset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filters"]);
		}
	}
	
	// update SESSIONized filters or destroy them if no filters are selected...
	if (isset($_GET['f'])) {
		if (is_array($_GET['f'])) {
			$_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filters"] = $_GET['f'];
		}
	}
	
	
	// sanitize input data...
	unset($clean_hl7);
	if (isset($_REQUEST['delete_id']) && is_numeric($_REQUEST['delete_id']) && (intval(trim($_REQUEST['delete_id'])) > 0)) {
		$clean_hl7['delete_id'] = intval(trim($_REQUEST['delete_id']));
	}
	if (isset($_REQUEST['edit_id']) && is_numeric($_REQUEST['edit_id']) && (intval(trim($_REQUEST['edit_id'])) > 0)) {
		$clean_hl7['edit_id'] = intval(trim($_REQUEST['edit_id']));
	}
	if (isset($_REQUEST['version']) && (strlen(trim($_REQUEST['version'])) > 0)) {
		$clean_hl7['version'] = trim($_REQUEST['version']);
	}
	if (isset($_REQUEST['add_flag']) && is_numeric($_REQUEST['add_flag']) && (intval(trim($_REQUEST['add_flag'])) > 0)) {
		$clean_hl7['add_flag'] = 1;
	}
	if (isset($_REQUEST['onboard_lab_id']) && is_numeric($_REQUEST['onboard_lab_id']) && (intval(trim($_REQUEST['onboard_lab_id'])) > 0)) {
		$clean_hl7['onboard_lab_id'] = intval(trim($_REQUEST['onboard_lab_id']));
	}
	if (isset($_REQUEST['onboard_preview']) && is_numeric($_REQUEST['onboard_preview']) && (intval(trim($_REQUEST['onboard_preview'])) > 0)) {
		$clean_hl7['onboard_preview'] = true;
	}
	if (isset($_REQUEST['onboard_flag']) && is_numeric($_REQUEST['onboard_flag']) && (intval(trim($_REQUEST['onboard_flag'])) > 0)) {
		$clean_hl7['onboard_flag'] = 1;
	}
	if (isset($_REQUEST['onboard_pre_flag']) && is_numeric($_REQUEST['onboard_pre_flag']) && (intval(trim($_REQUEST['onboard_pre_flag'])) > 0)) {
		$clean_hl7['onboard_pre_flag'] = 1;
	}
	if (isset($_REQUEST['onboard_message_version']) && (strlen(trim($_REQUEST['onboard_message_version'])) > 0)) {
		$clean_hl7['onboard_message_version'] = trim($_REQUEST['onboard_message_version']);
	}
	if (isset($_REQUEST['onboard_message']) && is_array($_REQUEST['onboard_message']) && (count($_REQUEST['onboard_message']) > 0)) {
		foreach ($_REQUEST['onboard_message'] as $onboard_message_key => $onboard_message) {
			if (strlen(trim($onboard_message)) > 0) {
				$clean_hl7['onboard_message'][] = str_replace('^~\\\\&', '^~\\&', trim($onboard_message));
			}
		}
	}
	if (isset($_POST['onboard_paths']) && is_array($_POST['onboard_paths'])) {
		$clean_hl7['onboard_path_arr'] = $_POST['onboard_paths'];
	}
	if (isset($_REQUEST['new_lab_id']) && is_numeric($_REQUEST['new_lab_id']) && (intval(trim($_REQUEST['new_lab_id'])) > 0)) {
		$clean_hl7['new']['lab_id'] = intval(trim($_REQUEST['new_lab_id']));
	}
	if (isset($_REQUEST['new_xpath']) && (strlen(trim($_REQUEST['new_xpath'])) > 0)) {
		$clean_hl7['new']['xpath'] = trim($_REQUEST['new_xpath']);
	}
	if (isset($_REQUEST['new_master_xpath']) && is_numeric($_REQUEST['new_master_xpath']) && (intval(trim($_REQUEST['new_master_xpath'])) > 0)) {
		$clean_hl7['new']['master_xpath'] = intval(trim($_REQUEST['new_master_xpath']));
	}
	if (isset($_REQUEST['new_sequence']) && is_numeric($_REQUEST['new_sequence']) && (intval(trim($_REQUEST['new_sequence'])) > 0)) {
		$clean_hl7['new']['sequence'] = intval(trim($_REQUEST['new_sequence']));
	}
	if (isset($_REQUEST['new_message_version']) && (strlen(trim($_REQUEST['new_message_version'])) > 0)) {
		$clean_hl7['new']['message_version'] = trim($_REQUEST['new_message_version']);
	}
	if (isset($_REQUEST['new_glue'])) {
		$clean_hl7['new']['glue_string'] = $_REQUEST['new_glue'];  // don't trim glue string; allows for separators such as a blank space
	}
	if (isset($_REQUEST['edit_lab_id']) && is_numeric($_REQUEST['edit_lab_id']) && (intval(trim($_REQUEST['edit_lab_id'])) > 0)) {
		$clean_hl7['edit']['lab_id'] = intval(trim($_REQUEST['edit_lab_id']));
	}
	if (isset($_REQUEST['edit_xpath']) && (strlen(trim($_REQUEST['edit_xpath'])) > 0)) {
		$clean_hl7['edit']['xpath'] = trim($_REQUEST['edit_xpath']);
	}
	if (isset($_REQUEST['edit_master_xpath']) && is_numeric($_REQUEST['edit_master_xpath']) && (intval(trim($_REQUEST['edit_master_xpath'])) > 0)) {
		$clean_hl7['edit']['master_xpath'] = intval(trim($_REQUEST['edit_master_xpath']));
	}
	if (isset($_REQUEST['edit_sequence']) && is_numeric($_REQUEST['edit_sequence']) && (intval(trim($_REQUEST['edit_sequence'])) > 0)) {
		$clean_hl7['edit']['sequence'] = intval(trim($_REQUEST['edit_sequence']));
	}
	if (isset($_REQUEST['edit_message_version']) && (strlen(trim($_REQUEST['edit_message_version'])) > 0)) {
		$clean_hl7['edit']['message_version'] = trim($_REQUEST['edit_message_version']);
	}
	if (isset($_REQUEST['edit_glue'])) {
		$clean_hl7['edit']['glue_string'] = $_REQUEST['edit_glue'];  // don't trim glue string; allows for separators such as a blank space
	}
?>

<script>
	$(function() {
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_element").focus();
			$(this).hide();
		});
		
		$("#onboard_button").button({
            icons: {
                primary: "ui-icon-elrimport-small"
            }
        }).click(function() {
			$("#addnew_form").hide();
			$(".import_error").hide();
			$("#onboard_form").show();
			$("#new_element").focus();
			$(this).hide();
			//var onboardAction = "<?php echo $main_url; ?>?selected_page=6&submenu=4&cat=5&onboard=1&lab_id="+$(this).val();
			//window.location.href = onboardAction;
		});
		
		$("#onboard_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#onboard_form").hide();
			$("#onboard_button").show();
		});
		
		$("#onboard_save").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_savelab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$(".edit_lab").button({
            icons: { primary: "ui-icon-elrpencil" }
        }).next().button({
            icons: { primary: "ui-icon-elrclose" }
        }).parent().buttonset();
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false
		});
		
		$(".delete_lab").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=4&cat=5&delete_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$("#edit_lab_dialog").dialog({
			autoOpen: false,
			width: 800,
			modal: true
		});
		
		$(".edit_lab").click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.element_id) {
				$("#edit_id").val(jsonObj.element_id);
				$("#edit_lab_id").val(jsonObj.lab_id);
				$("#edit_message_version").val(jsonObj.version);
				$("#edit_xpath").val(jsonObj.xpath);
				$("#edit_master_xpath").val(jsonObj.master_xpath);
				$("#edit_glue").val(jsonObj.glue);
				$("#edit_sequence").val(jsonObj.sequence);
				
				$("#edit_lab_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#edit_modal_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#edit_lab_dialog").dialog("open");
			} else {
				return false;
			}
			
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
		$("#onboard_finish").button({
            icons: { primary: "ui-icon-elrsave" }
        });
		
		$("#toggle_filters").button({
            icons: {
                secondary: "ui-icon-triangle-1-n"
            }
        }).click(function() {
			$(".vocab_filter").toggle("blind");
			var objIcons = $(this).button("option", "icons");
			if (objIcons['secondary'] == "ui-icon-triangle-1-s") {
				$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-n"});
				$(this).button("option", "label", "Hide Filters");
				$("#addnew_form").hide();
				$("#addnew_button").show();
			} else {
				$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
				$(this).button("option", "label", "Show Filters");
			}
		});
		
		$(".vocab_filter").hide();
		$("#toggle_filters").button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
		$("#toggle_filters").button("option", "label", "Show Filters");
		
		$("#clear_filters").button({
            icons: {
                primary: "ui-icon-elrcancel"
            }
        }).click(function() {
			$(".pseudo_select").removeAttr("checked");
			$(".pseudo_select_label").removeClass("pseudo_select_on");
			$("#search_form")[0].reset();
			$("#q").val("").blur();
			$("#search_form").submit();
		});
		
		$("#q_go").button({
			icons: {
                primary: "ui-icon-elrsearch"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
		$("#apply_filters").button({
			icons: {
                primary: "ui-icon-elroptions"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
		$("#q").addClass("search_empty").val("Enter search terms...").click(function() {
			var search_val = $.trim($("#q").val());
			if (search_val == "Enter search terms...") {
				$(this).removeClass("search_empty").val("");
			}
		}).blur(function() {
			var search_val_ln = $.trim($("#q").val()).length;
			if (search_val_ln == 0) {
				$("#q").addClass("search_empty").val("Enter search terms...");
			}
		});
		
		<?php
			if (isset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_display"])) {
				if ($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_display"] != "Enter search terms...") {
		?>
		$("#q").removeClass("search_empty").val("<?php echo $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_display"]; ?>");
		<?php
				}
			}
		?>
		
		$(".pseudo_select").change(function() {
			$(this).closest('label').toggleClass('pseudo_select_on');
		});
		
	});
</script>

<?php

	/**
	 * Process Step 1 of onboarding a new message type...
	 * Display all found paths, and give the user the option to pre-enter data, then pass to step 2.
	 * Requires lab ID and Hl7 message
	 */
	if (isset($clean_hl7['onboard_lab_id']) && isset($clean_hl7['onboard_pre_flag']) && isset($clean_hl7['onboard_message']) && isset($clean_hl7['onboard_message_version'])) {
		unset($onboard);
		unset($mirth);
		// valid lab ID, message version & message, onboard flag is set... time to parse the message & onboard the new structure
		
		// get back Mirth XML from HL7 message
		try {
			$mirth = @new SoapClient($props['mirth_wsdl'], array("trace" => true, "exceptions" => true));
		} catch (Exception $e) {
			print_r($e);
		} catch (SoapFault $f) {
			print_r($f);
		}
		
		if ($mirth) {
			foreach ($clean_hl7['onboard_message'] as $clean_onboard_message_key => $clean_onboard_message) {
				unset($mirth_res);
				$mirth_res = $mirth->acceptMessage(array("arg0" => $clean_onboard_message));
				if (isset($mirth_res) && !is_null($mirth_res)) {
					$onboard['xml'][] = simplexml_load_string(str_replace('xmlns="urn:hl7-org:v2xml"', '', $mirth_res->return));
				} else {
					suicide("No Mirth XML returned from Web Service.  Please check to ensure that you have provided a well-formed HL7 message and try again.", -1, 1);
				}
			}
		}
		
		echo "<h1 class=\"elrhdg\"><span class=\"ui-icon ui-icon-header ui-icon-elrxml\"></span>HL7 Structure</h1>";
		
		if (isset($clean_hl7['onboard_preview']) && $clean_hl7['onboard_preview']) {
			echo "<h3>Mirth XML Preview</h3>";
			foreach ($onboard['xml'] as $preview_xml) {
				echo "<textarea class=\"ui-corner-all\" style=\"padding: 5px; font-family: 'Consolas'; font-weight: bold; width: 90%; height: 30em; color: darkred;\">".htmlentities(formatXml(trim($preview_xml->asXML())))."</textarea><br><br>";
			}
			exit;
		} else {
			echo "<h3>Onboarding - Step 2:  Discovered HL7 XML Paths</h3>";
			//highlight("Important!  Onboarding is not yet complete.  Please review the discovered paths below and complete any mapping/configuration necessary, then click the <strong>Finish Onboard</strong> button.", "ui-icon-elrerror");
			highlight('<strong style="font-weight: 700 !important; font-size: 1.1em;">Important!</strong><br>Completing this step will immediately replace all prior mappings for this Lab & Message Version with those specified below!<br>Only HL7 paths mapped to a selected Master XML Path will be included.  All unmapped paths will be discarded.', 'ui-icon-elrerror');
			//echo '<div class="ui-corner-all" style="width: 90%; height: 10em; margin: 10px; padding: 5px; border: 1px lightgray solid; background-color: lightcyan; font-size: 10pt; font-family: Consolas, \'Courier New\', courier, serif; overflow: scroll; white-space: nowrap;">'.nl2br(htmlspecialchars($clean_hl7['onboard_message'])).'</div>';
			echo "<form id=\"new_lab_form\" method=\"POST\" action=\"".$main_url."?selected_page=".intval($selected_page)."&submenu=".intval($submenu)."&cat=".intval($cat)."&onboard_message_version=".$clean_hl7['onboard_message_version']."&onboard_lab_id=".$clean_hl7['onboard_lab_id']."&onboard_flag=1\">";
			echo "<table id=\"labResults\"><thead><tr><th>HL7 XML Path</th><th>Example Values</th><th>Master XML Path</th><th>Concat String</th><th>Sequence</th></tr></thead><tbody>";
			
			
			if (isset($onboard['xml']) && is_array($onboard['xml']) && (count($onboard['xml']) > 0)) {
				unset($path_counter, $onboard['version'], $onboard['paths'], $onboard['found_paths']);
				// set message version
				$onboard['version'] = $clean_hl7['onboard_message_version'];
				$path_counter = 1;
				
				// parse through XML document & get all the XPaths for end-nodes
				foreach ($onboard['xml'] as $onboard_xml_key => $onboard_xml) {
					unset($dxml);
					$dxml = dom_import_simplexml($onboard_xml);
					foreach ($dxml->getElementsByTagName("*") as $node) {
						unset($this_nodepath);
						$node_has_children = false;
						// loop through child nodes of given element and see if any of them contain more XML element nodes
						foreach($node->childNodes as $childNode) {
							if (intval($childNode->nodeType) == 1) { 
								$node_has_children = $node_has_children || true;
							}
						}
						if (!$node_has_children) {
							//$onboard['paths'][] = $node->getNodePath();
							$this_nodepath = $node->getNodePath();
							$this_nodepath = preg_replace('/\[\w+\]/', '', $this_nodepath);  // remove repeating element indices from xpaths to only get unique xpaths
							if (isset($onboard['found_paths'][$this_nodepath]) && is_array($onboard['found_paths'][$this_nodepath]) && in_array(trim($node->nodeValue), $onboard['found_paths'][$this_nodepath])) {
								// value already set, do nothing
							} else {
								$onboard['found_paths'][$this_nodepath][] = trim($node->nodeValue);
							}
						}
					}
				}
					
				if (isset($onboard['found_paths']) && is_array($onboard['found_paths'])) {
					ksort($onboard['found_paths']);
					
					// get previously-mapped vals, if any, and try to preserve them (or allow for editing)
					$previous_map_sql = 'SELECT xpath, master_path_id, glue_string, sequence 
						FROM '.$my_db_schema.'structure_path_mirth 
						WHERE lab_id = '.intval($clean_hl7['onboard_lab_id']).' 
						AND message_version = \''.@pg_escape_string($clean_hl7['onboard_message_version']).'\';';
					$previous_map_rs = @pg_query($host_pa, $previous_map_sql);
					$previous_map = array();
					if ($previous_map_rs !== false) {
						while ($previous_map_row = @pg_fetch_object($previous_map_rs)) {
							if (isset($previous_map[trim($previous_map_row->xpath)]) && (intval($previous_map[trim($previous_map_row->xpath)]['master_path_id']) >= 1)) {
								// if this xpath already exists and is mapped to a master path (i.e. duplicate paths), don't overwrite
							} else {
								$previous_map[trim($previous_map_row->xpath)] = array(
									'glue_string' => trim($previous_map_row->glue_string), 
									'sequence' => intval($previous_map_row->sequence), 
									'master_path_id' => intval($previous_map_row->master_path_id)
								);
							}
						}
					}
					
					foreach ($onboard['found_paths'] as $hl7_nodepath => $hl7_nodepath_vals) {
						echo "<tr><td valign=\"top\" title=\"".$hl7_nodepath."\">". ((strlen($hl7_nodepath) > 40) ? "...".substr($hl7_nodepath, (strlen($hl7_nodepath)-40)) : $hl7_nodepath) ."<input type=\"hidden\" name=\"onboard_paths[".$path_counter."][hl7path]\" value=\"".$hl7_nodepath."\"></td>";
						echo "<td valign=\"top\" ><ul style=\"margin-left: 20px;\">";
						foreach ($hl7_nodepath_vals as $node_value) {
							echo '<li style="list-style-type: square; margin-bottom: 0.5em;">'.htmlentities($node_value, ENT_QUOTES, "UTF-8").'</li>';
						}
						echo "</ul></td>";
						echo "<td valign=\"top\" >";
						// master xml path drop-down...
						echo "<select class=\"ui-corner-all\" name=\"onboard_paths[".$path_counter."][masterpath]\" id=\"onboard_paths_".$path_counter."_masterpath\"><option value=\"0\" selected>--</option>";
						
						// get list of master xpaths for menu
						$masterpaths_sql = sprintf("SELECT DISTINCT id, xpath FROM %sstructure_path ORDER BY xpath;", $my_db_schema);
						$masterpaths_rs = @pg_query($host_pa, $masterpaths_sql) or suicide("Unable to retrieve list of Master XPaths.", 1, 1);
						while ($masterpaths_row = pg_fetch_object($masterpaths_rs)) {
							echo '<option value="'.intval($masterpaths_row->id).'"'.((isset($previous_map[trim($hl7_nodepath)]) && (intval($previous_map[trim($hl7_nodepath)]['master_path_id']) == intval($masterpaths_row->id))) ? ' selected' : '').'>'.htmlentities($masterpaths_row->xpath).'</option>';
						}
						pg_free_result($masterpaths_rs);
						
						echo '</select>';
						echo '</td>';
						echo '<td valign="top" ><input class="ui-corner-all" type="text" name="onboard_paths['.$path_counter.'][concat]" id="onboard_paths_'.$path_counter.'_concat" value="'.((isset($previous_map[trim($hl7_nodepath)])) ? htmlspecialchars(trim($previous_map[trim($hl7_nodepath)]['glue_string'])) : '').'"/></td>';
						echo '<td valign="top" ><input class="ui-corner-all" type="text" name="onboard_paths['.$path_counter.'][sequence]" id="onboard_paths_'.$path_counter.'_sequence" placeholder="1" value="'.((isset($previous_map[trim($hl7_nodepath)])) ? intval($previous_map[trim($hl7_nodepath)]['sequence']) : '').'"/></td>';
						echo '</tr>';
						$path_counter++;
						unset($previous_map[trim($hl7_nodepath)]);
					}
				}
				
				if (isset($previous_map) && is_array($previous_map) && (count($previous_map) > 0)) {
					foreach ($previous_map as $preexisting_xpath => $preexisting_mapping) {
						if (intval($preexisting_mapping['master_path_id']) > 0) {
							echo "<tr><td valign=\"top\" title=\"".$preexisting_xpath."\">". ((strlen($preexisting_xpath) > 40) ? "...".substr($preexisting_xpath, (strlen($preexisting_xpath)-40)) : $preexisting_xpath) ."<input type=\"hidden\" name=\"onboard_paths[".$path_counter."][hl7path]\" value=\"".$preexisting_xpath."\"></td>";
							echo '<td valign="top" style="font-style: italic; color: dodgerblue;">Path previously mapped, but not found in this sample HL7 message.  To remove, un-set the Master XML Path.</td>';
							echo "<td valign=\"top\" >";
							// master xml path drop-down...
							echo "<select class=\"ui-corner-all\" name=\"onboard_paths[".$path_counter."][masterpath]\" id=\"onboard_paths_".$path_counter."_masterpath\"><option value=\"0\" selected>--</option>";
							
							// get list of master xpaths for menu
							$masterpaths_sql = sprintf("SELECT DISTINCT id, xpath FROM %sstructure_path ORDER BY xpath;", $my_db_schema);
							$masterpaths_rs = @pg_query($host_pa, $masterpaths_sql) or suicide("Unable to retrieve list of Master XPaths.", 1, 1);
							while ($masterpaths_row = pg_fetch_object($masterpaths_rs)) {
								echo '<option value="'.intval($masterpaths_row->id).'"'.((isset($previous_map[trim($preexisting_xpath)]) && (intval($previous_map[trim($preexisting_xpath)]['master_path_id']) == intval($masterpaths_row->id))) ? ' selected' : '').'>'.htmlentities($masterpaths_row->xpath).'</option>';
							}
							pg_free_result($masterpaths_rs);
							
							echo '</select>';
							echo '</td>';
							echo '<td valign="top" ><input class="ui-corner-all" type="text" name="onboard_paths['.$path_counter.'][concat]" id="onboard_paths_'.$path_counter.'_concat" value="'.((isset($previous_map[trim($preexisting_xpath)])) ? htmlspecialchars(trim($previous_map[trim($preexisting_xpath)]['glue_string'])) : '').'"/></td>';
							echo '<td valign="top" ><input class="ui-corner-all" type="text" name="onboard_paths['.$path_counter.'][sequence]" id="onboard_paths_'.$path_counter.'_sequence" placeholder="1" value="'.((isset($previous_map[trim($preexisting_xpath)])) ? intval($previous_map[trim($preexisting_xpath)]['sequence']) : '').'"/></td>';
							echo '</tr>';
							$path_counter++;
						}
					}
				
				}
				
				echo "</tbody></table>";
				
				echo "<br><button type=\"submit\" id=\"onboard_finish\">Save New HL7 Structure Mapping</button><br><br>";
				echo "</form>";
				/*echo '<pre>';
				ksort($onboard['found_paths']);
				print_r($onboard['found_paths']);
				echo '</pre>';*/
				exit;
			
			} else {
				suicide("Unable to receive Mirth XML from web service.");
			}
		}
	}
	
	/**
	 * Process Step 2 of onboarding a new message type...
	 * Requires lab ID, message version and onboard_paths array
	 */
	if (isset($clean_hl7['onboard_lab_id']) && isset($clean_hl7['onboard_flag']) && isset($clean_hl7['onboard_path_arr']) && isset($clean_hl7['onboard_message_version'])) {
	
		// make sure we got at least one XPath back
		if (is_array($clean_hl7['onboard_path_arr']) && (count($clean_hl7['onboard_path_arr']) > 0)) {
			// remove old structure for this lab & version if it exists, then insert new structure
			$onboard_insert_sql = "BEGIN;\n";
			$onboard_insert_sql .= sprintf("DELETE FROM ONLY %sstructure_path_mirth WHERE lab_id = %d AND message_version = '%s';\n", $my_db_schema, $clean_hl7['onboard_lab_id'], pg_escape_string(trim($clean_hl7['onboard_message_version'])));
			foreach ($clean_hl7['onboard_path_arr'] as $op_key => $op_vals) {
				if (isset($op_vals['masterpath']) && !is_null($op_vals['masterpath']) && filter_var($op_vals['masterpath'], FILTER_VALIDATE_INT) && (intval($op_vals['masterpath']) > 0)) {
					// only insert new HL7 xpaths if mapped to master path
					$onboard_insert_sql .= sprintf("INSERT INTO %sstructure_path_mirth (lab_id, message_version, master_path_id, glue_string, xpath, sequence) VALUES (%d, '%s', %s, %s, '%s', %s);\n", 
						$my_db_schema,
						$clean_hl7['onboard_lab_id'],
						pg_escape_string(trim($clean_hl7['onboard_message_version'])),
						((isset($op_vals['masterpath']) && !is_null($op_vals['masterpath']) && filter_var($op_vals['masterpath'], FILTER_VALIDATE_INT) && (intval($op_vals['masterpath']) > 0)) ? intval($op_vals['masterpath']) : "NULL"),
						((isset($op_vals['concat']) && !is_null($op_vals['concat']) && (strlen(trim($op_vals['concat'])) > 0)) ? "'".pg_escape_string(trim($op_vals['concat']))."'" : "NULL"),
						pg_escape_string(trim($op_vals['hl7path'])),
						((isset($op_vals['sequence']) && !is_null($op_vals['sequence']) && filter_var($op_vals['sequence'], FILTER_VALIDATE_INT) && (intval($op_vals['sequence']) > 0)) ? intval($op_vals['sequence']) : 1)
					);
				}
			}
			//$onboard_insert_sql .= "ROLLBACK;\n";
			$onboard_insert_sql .= "COMMIT;\n";
			$onboard_insert_rs = @pg_query($host_pa, $onboard_insert_sql);
			if ($onboard_insert_rs) {
				/* debug
				echo "<pre>";
				echo $onboard_insert_sql;
				echo "</pre>";
				*/
				highlight("New message structure successfully onboarded!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to onboard new message structure.", 1);
			}
			@pg_free_result($onboard_insert_rs);
		} else {
			suicide("No valid XPaths found in generated Mirth XML.");
		}
		
	}

	if (isset($clean_hl7['edit_id'])) {
		/**
		 * Edit an existing HL7 element record, specified by ID
		 */
		// check to see if passed a valid row id...
		if (isset($clean_hl7['edit']['lab_id']) && isset($clean_hl7['edit']['message_version']) && isset($clean_hl7['edit']['xpath'])) {
			$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path_mirth WHERE id = %s;", $my_db_schema, pg_escape_string($clean_hl7['edit_id']));
			$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to save changes to HL7 element.", 1, 1);
			$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
			if ($valid_counter != 1) {
				suicide("Unable to save changes to mapping -- HL7 element does not exist.");
			} else {
				$edit_sql = sprintf("UPDATE %sstructure_path_mirth SET lab_id = %d, message_version = '%s', master_path_id = %d, glue_string = %s, xpath = '%s', sequence = %d WHERE id = %d;",
					$my_db_schema,
					$clean_hl7['edit']['lab_id'],
					pg_escape_string($clean_hl7['edit']['message_version']),
					(isset($clean_hl7['edit']['master_xpath']) ? $clean_hl7['edit']['master_xpath'] : "NULL"),
					(isset($clean_hl7['edit']['glue_string']) ? "'".pg_escape_string($clean_hl7['edit']['glue_string'])."'" : "NULL"),
					pg_escape_string($clean_hl7['edit']['xpath']),
					(isset($clean_hl7['edit']['sequence']) ? $clean_hl7['edit']['sequence'] : 1),
					$clean_hl7['edit_id']
				);
				if (@pg_query($host_pa, $edit_sql)) {
					highlight("HL7 element successfully updated!", "ui-icon-elrsuccess");
				} else {
					suicide("Unable to save changes to HL7 element.", 1);
				}
			}
		} else {
			suicide("Not all required fields were entered!  Please specify at least a Lab Name, Message Version, and HL7 Element Path and try again.");
		}
	} elseif (isset($clean_hl7['delete_id'])) {
		/**
		 * Delete an individual HL7 element, specified by ID
		 */
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path_mirth WHERE id = %s;", $my_db_schema, pg_escape_string($clean_hl7['delete_id']));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to delete Master XML element.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to delete Master XML element -- record not found.");
		} else {
			// commit the delete...
			$delete_sql = sprintf("DELETE FROM ONLY %sstructure_path_mirth WHERE id = %d;", $my_db_schema, $clean_hl7['delete_id']);
			if (@pg_query($host_pa, $delete_sql)) {
				highlight("HL7 element successfully deleted!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to delete HL7 element.", 1);
			}
		}
	} elseif (isset($_REQUEST['add_flag'])) {
		/**
		 * Add a new individual HL7 element
		 */
		if (isset($clean_hl7['new']['lab_id']) && isset($clean_hl7['new']['message_version']) && isset($clean_hl7['new']['xpath'])) {
			$addlab_sql = sprintf("INSERT INTO %sstructure_path_mirth (lab_id, message_version, master_path_id, glue_string, xpath, sequence) VALUES (%d, '%s', %d, %s, '%s', %d)",
				$my_db_schema,
				$clean_hl7['new']['lab_id'],
				pg_escape_string($clean_hl7['new']['message_version']),
				(isset($clean_hl7['new']['master_xpath']) ? $clean_hl7['new']['master_xpath'] : "NULL"),
				(isset($clean_hl7['new']['glue_string']) ? "'".pg_escape_string($clean_hl7['new']['glue_string'])."'" : "NULL"),
				pg_escape_string($clean_hl7['new']['xpath']),
				(isset($clean_hl7['new']['sequence']) ? $clean_hl7['new']['sequence'] : 1)
			);
			@pg_query($host_pa, $addlab_sql) or suicide("Could not add new HL7 element.", 1);
			highlight("New HL7 element \"".htmlentities($clean_hl7['new']['xpath'], ENT_QUOTES, "UTF-8")."\" added successfully!", "ui-icon-elrsuccess");
		} else {
			suicide("Not all required fields were entered!  Please specify at least a Lab Name, Message Version, and HL7 Element Path and try again.");
		}
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrxml"></span>HL7 Structure</h1>

<form name="search_form" id="search_form" method="GET" action="<?php echo $main_url; ?>">

<div class="vocab_search ui-tabs ui-widget">
	<label for="q" class="vocab_search_form">Search HL7/Master XML Paths:</label><br><input type="text" name="q" id="q" class="vocab_query ui-corner-all">
	<button name="q_go" id="q_go" title="Search">Search</button>
	<button type="button" name="clear_filters" id="clear_filters" title="Clear all search terms/filters">Reset</button>
	<button type="button" name="toggle_filters" id="toggle_filters" title="Show/hide filters">Hide Filters</button>
	<button type="button" id="addnew_button" title="Manually add a new HL7 message element">Add Individual HL7 Element</button>
	<button type="button" id="onboard_button" title="Onboard a new HL7 message">Onboard New Lab/Message</button>
</div>

<?php
	############### If filters applied, display which ones ###############
	if (isset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filters"])) {
?>
<div class="vocab_search ui-widget ui-widget-content ui-state-highlight ui-corner-all" style="padding: 5px;">
	<span class="ui-icon ui-icon-elroptions" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;">Filtering by 
<?php
		$active_filters = 0;
		foreach ($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filters"] as $sqlfiltercol => $sqlfiltervals) {
			if ($active_filters == 0) {
				echo "<strong>" . $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]['label'] . "</strong>";
			} else {
				echo ", <strong>" . $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]['label'] . "</strong>";			}
			$active_filters++;
		}
?>
	</p>
</div>
<?php
	}
?>

<div class="vocab_filter ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Filters:</label></div>
<?php
	############### Draw filter form elements based on 'filter_cols' array ###############
	foreach ($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"] as $filtercol => $filtercolname) {
		if ($filtercolname['filter']) {
			echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">".$filtercolname['label']."</label><br>";
			echo "<div class=\"vocab_filter_checklist\">";
			
			if ($filtercolname['filterlookup']) {
				$filter_qry =  $filtercolname['lookupqry'];
			} else {
				$filter_qry = sprintf("SELECT DISTINCT %s FROM %s%s ORDER BY %s ASC;", $filtercol, $my_db_schema, $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["query_data"]["table_name"], $filtercol);
			}
			#debug echo $filter_qry;
			$filter_rs = @pg_query($host_pa, $filter_qry);
			$value_iterator = 0;
			while ($filter_row = @pg_fetch_object($filter_rs)) {
				$value_iterator++;
				$this_filter_selected = FALSE;
				if (is_array($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]['filters'][$filtercol])) {
					if ($filtercolname['has_id']) {
						if (in_array($filter_row->value, $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]['filters'][$filtercol])) {
							$this_filter_selected = TRUE;
						}
					} else {
						if (in_array($filter_row->label, $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]['filters'][$filtercol])) {
							$this_filter_selected = TRUE;
						}
					}
				}
				
				if ($filtercolname['has_id']) {
					if ($this_filter_selected) {
						echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"".htmlentities($filter_row->value, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
					} else {
						echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"".htmlentities($filter_row->value, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
					}
				} else {
					if ($this_filter_selected) {
						echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"".htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
					} else {
						echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"".htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
					}
				}
			}
			
			// add a 'Blank' option for lookup-generated filter lists
			if ($filtercolname['filterlookup']) {
				$value_iterator++;
				if (is_array($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]['filters'][$filtercol]) && in_array(-1, $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]['filters'][$filtercol])) {
					echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"-1\">&nbsp;&lt;Blank&gt;</label><br>";
				} else {
					echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"-1\">&nbsp;&lt;Blank&gt;</label><br>";
				}
			}
			
			@pg_free_result($filter_rs);
			echo "</div></div>";
		}
	}
?>
	<br><br><button name="apply_filters" id="apply_filters" title="Apply Filters" style="clear: both; float: left; margin: 5px;">Apply Filters</button>
</div>

<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
<input type="hidden" name="submenu" value="<?php echo $submenu; ?>">
<input type="hidden" name="cat" value="<?php echo $cat; ?>">

</form>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New HL7 Message Element:</label><br><br></div>
	<form id="new_lab_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&cat=<?php echo intval($cat); ?>">
		<label class="vocab_search_form2" for="new_lab_id">Lab Name:</label>
			<select class="ui-corner-all" name="new_lab_id" id="new_lab_id">
				<option value="0" selected>--</option>
			<?php
				// get list of labs for menu
				$addnew_sql = sprintf("SELECT DISTINCT id, ui_name FROM %sstructure_labs WHERE alias_for < 1 ORDER BY ui_name;", $my_db_schema);
				$addnew_rs = @pg_query($host_pa, $addnew_sql) or suicide("Unable to retrieve list of labs.", 1, 1);
				while ($addnew_row = pg_fetch_object($addnew_rs)) {
					printf("<option value=\"%d\">%s</option>", intval($addnew_row->id), htmlentities($addnew_row->ui_name));
				}
				@pg_free_result($addnew_rs);
			?>
			</select>
		<label class="vocab_search_form2" for="new_message_version">Message Version:</label><input class="ui-corner-all" type="text" name="new_message_version" id="new_message_version" />
		<br><br><label class="vocab_search_form2" for="new_xpath">HL7 XML XPath:</label><input class="ui-corner-all" type="text" name="new_xpath" id="new_xpath" />
		<label class="vocab_search_form2" for="new_master_xpath">Master XML Path:</label>
			<select class="ui-corner-all" name="new_master_xpath" id="new_master_xpath">
				<option value="0" selected>--</option>
			<?php
				// get list of XML paths for menu
				$path_sql = sprintf("SELECT DISTINCT id, element, xpath FROM %sstructure_path ORDER BY element;", $my_db_schema);
				$path_rs = @pg_query($host_pa, $path_sql) or suicide("Unable to retrieve list of XML paths.", 1, 1);
				while ($path_row = pg_fetch_object($path_rs)) {
					printf("<option value=\"%d\">%s (%s)</option>", intval($path_row->id), htmlentities($path_row->element, ENT_QUOTES, "UTF-8"), htmlentities($path_row->xpath, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($path_rs);
			?>
			</select>
		<br><br><label class="vocab_search_form2" for="new_glue">Concatenation String:</label><input class="ui-corner-all" type="text" name="new_glue" id="new_glue" />
		<label class="vocab_search_form2" for="new_sequence">Sequence:</label><input class="ui-corner-all" type="text" name="new_sequence" id="new_sequence" />
		<input type="hidden" name="lab_id" value="" />
		<input type="hidden" name="version" value="" />
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_savelab" id="new_savelab">Save New HL7 Message Element</button>
		<button type="button" id="addnew_cancel">Cancel</button>
	</form>
</div>

<div id="onboard_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Onboard New Message Structure - Step 1:</label><br><br></div>
	<form id="new_onboard_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&cat=<?php echo intval($cat); ?>">
		<label class="vocab_search_form2" for="onboard_preview">Preview Full XML?</label>
			<select class="ui-corner-all" name="onboard_preview" id="onboard_preview">
				<option value="1">Yes, view full Mirth XML document</option>
				<option value="0" selected>No, onboard/view paths</option>
			</select>
		<br><br><label class="vocab_search_form2" for="onboard_lab_id">Lab Name:</label>
			<select class="ui-corner-all" name="onboard_lab_id" id="onboard_lab_id">
				<option value="0" selected>--</option>
			<?php
				// get list of labs for menu
				$onboard_sql = sprintf("SELECT DISTINCT id, ui_name FROM %sstructure_labs WHERE alias_for < 1 ORDER BY ui_name;", $my_db_schema);
				$onboard_rs = @pg_query($host_pa, $onboard_sql) or suicide("Unable to retrieve list of labs.", 1, 1);
				while ($onboard_row = pg_fetch_object($onboard_rs)) {
					printf("<option value=\"%d\">%s</option>", intval($onboard_row->id), htmlentities($onboard_row->ui_name));
				}
				@pg_free_result($onboard_rs);
			?>
			</select>
		<label class="vocab_search_form2" for="onboard_message_version">Message Version:</label><input class="ui-corner-all" type="text" name="onboard_message_version" id="onboard_message_version" />
		<br><br><label class="vocab_search_form2" for="onboard_message_1">HL7 Message #1:</label><br>
		<textarea class="ui-corner-all" name="onboard_message[]" id="onboard_message_1" style="width: 70%; height: 12em;"></textarea>
		<br><br><label class="vocab_search_form2" for="onboard_message_2">HL7 Message #2 (Optional):</label><br>
		<textarea class="ui-corner-all" name="onboard_message[]" id="onboard_message_2" style="width: 70%; height: 12em;"></textarea>
		<br><br><label class="vocab_search_form2" for="onboard_message_3">HL7 Message #3 (Optional):</label><br>
		<textarea class="ui-corner-all" name="onboard_message[]" id="onboard_message_3" style="width: 70%; height: 12em;"></textarea>
		<br><br><label class="vocab_search_form2" for="onboard_message_4">HL7 Message #4 (Optional):</label><br>
		<textarea class="ui-corner-all" name="onboard_message[]" id="onboard_message_4" style="width: 70%; height: 12em;"></textarea>
		<br><br><label class="vocab_search_form2" for="onboard_message_5">HL7 Message #5 (Optional):</label><br>
		<textarea class="ui-corner-all" name="onboard_message[]" id="onboard_message_5" style="width: 70%; height: 12em;"></textarea>
		
		<input type="hidden" name="lab_id" value="" />
		<input type="hidden" name="version" value="" />
		<input type="hidden" name="onboard_pre_flag" value="1" />
		<br><br><button type="submit" name="onboard_save" id="onboard_save">Parse & Generate Structure</button>
		<button type="button" id="onboard_cancel">Cancel</button>
	</form>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th>Lab</th>
				<th>Message Version</th>
				<th>HL7 XML Path</th>
				<th>Master XML Path</th>
				<th>Concat String</th>
				<th>Sequence</th>
			</tr>
		</thead>
		<tbody>

<?php

	$where_clause = '';
	
	$where_count = 0;
	// handle any search terms or filters...
	if (isset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_sql"])) {
		// we've got some query params
		$where_count = 1;
		$where_clause .= " WHERE (";
		
		foreach ($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"] as $searchcol => $searchcoldata) {
			if ($searchcoldata['textsearch']) {
				if ($where_count > 1) {
					$where_clause .= " OR ";
				}
				$where_clause .= sprintf("%s ILIKE '%%%s%%'", $searchcoldata['colname'], $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["q_sql"]);
				$where_count++;
			}
		}
		
		$where_clause .= ")";
	}
	
	if (isset($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filters"])) {
		// need to apply filters
		$filter_count = 0;
		if ($where_count == 0) {
			// not already a WHERE clause for search terms
			$where_clause .= " WHERE";
		}
		
		foreach ($_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filters"] as $sqlfiltercol => $sqlfiltervals) {
			unset($filter_temp);
			$nullfilter = FALSE;
			if (($filter_count > 0) || ($where_count > 1)) {
				$where_clause .= " AND (";
			} else {
				$where_clause .= " (";
			}
			foreach ($sqlfiltervals as $sqlfilterval) {
				if (is_null($sqlfilterval) || (strlen(trim($sqlfilterval)) == 0)) {
					$nullfilter = TRUE;
				} else {
					$filter_temp[] = "'" . pg_escape_string($sqlfilterval) . "'";
				}
			}
			
			if ($nullfilter && is_array($filter_temp)) {
				$where_clause .= $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]["filtercolname"] . " IN (" . implode(",", $filter_temp) . ") OR " . $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]["filtercolname"] . " IS NULL OR " . $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]["filtercolname"] . " = ''";
			} elseif (is_array($filter_temp)) {
				$where_clause .= $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]["filtercolname"] . " IN (" . implode(",", $filter_temp) . ")";
			} elseif ($nullfilter) {
				$where_clause .= $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]["filtercolname"] . " IS NULL OR " . $_SESSION["structure_params"][$_SESSION["structure_params"]["structure"]]["filter_cols"][$sqlfiltercol]["filtercolname"] . " = ''";
			}
			
			$where_clause .= ")";
			$filter_count++;
		}
	}
	
	$hl7_qry = 'SELECT h.id AS id, h.message_version AS message_version, h.xpath AS xpath, h.master_path_id AS master_path_id, h.sequence AS sequence, h.glue_string AS glue_string, l.id AS lab_id, l.ui_name AS lab_name, p.xpath AS master_xpath, p.element AS master_element
		FROM '.$my_db_schema.'structure_path_mirth h 
		INNER JOIN '.$my_db_schema.'structure_labs l ON (h.lab_id = l.id) 
		LEFT JOIN '.$my_db_schema.'structure_path p ON (h.master_path_id = p.id) 
		'.$where_clause.' 
		ORDER BY h.lab_id, h.message_version, h.master_path_id, h.sequence, h.xpath';
	$hl7_rs = @pg_query($host_pa, $hl7_qry) or suicide("Could not connect to database:", 1);
	
	while ($hl7_row = @pg_fetch_object($hl7_rs)) {
		echo "<tr>";
		printf("<td class=\"action_col\">");
		unset($edit_lab_params);
		$edit_lab_params = array(
			"element_id" => intval($hl7_row->id), 
			"lab_id" => intval($hl7_row->lab_id), 
			"version" => htmlentities($hl7_row->message_version, ENT_QUOTES, "UTF-8"), 
			"xpath" => htmlentities($hl7_row->xpath, ENT_QUOTES, "UTF-8"), 
			"master_xpath" => intval($hl7_row->master_path_id), 
			"glue" => htmlentities($hl7_row->glue_string, ENT_QUOTES, "UTF-8"), 
			"sequence" => intval($hl7_row->sequence)
		);
		printf("<button class=\"edit_lab\" type=\"button\" value='%s' title=\"Edit this record\">Edit</button>", json_encode($edit_lab_params));
		printf("<button class=\"delete_lab\" type=\"button\" value=\"%s\" title=\"Delete this record\">Delete</button>", $hl7_row->id);
		echo "</td>";
		echo "<td>".htmlentities($hl7_row->lab_name, ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities($hl7_row->message_version, ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities(str_replace("/", " /", $hl7_row->xpath), ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities($hl7_row->master_element, ENT_QUOTES, "UTF-8")." (".htmlentities($hl7_row->master_xpath, ENT_QUOTES, "UTF-8").")</td>";
		echo "<td>".htmlentities($hl7_row->glue_string, ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".intval($hl7_row->sequence)."</td>";
		echo "</tr>";
	}
	
	@pg_free_result($hl7_rs);

?>

		</tbody>
	</table>
	
</div>

<div id="confirm_delete_dialog" title="Delete this HL7 Element?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This HL7 element will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="edit_lab_dialog" title="Edit HL7 Element">
	<form id="edit_modal_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&cat=<?php echo intval($cat); ?>">
		<label for="edit_lab_id">Lab Name:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_lab_id" id="edit_lab_id">
			<option value="0" selected>--</option>
		<?php
			// get list of labs for menu
			$editlabs_sql = sprintf("SELECT DISTINCT id, ui_name FROM %sstructure_labs WHERE alias_for < 1 ORDER BY ui_name;", $my_db_schema);
			$editlabs_rs = @pg_query($host_pa, $editlabs_sql) or suicide("Unable to retrieve list of labs.", 1, 1);
			while ($editlabs_row = pg_fetch_object($editlabs_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($editlabs_row->id), htmlentities($editlabs_row->ui_name));
			}
			pg_free_result($editlabs_rs);
		?>
		</select><br><br>
		<label for="edit_message_version">Message Version:</label><br><input class="ui-corner-all" type="text" name="edit_message_version" id="edit_message_version" /><br><br>
		<label for="edit_xpath">HL7 XPath:</label><br><input class="ui-corner-all" type="text" name="edit_xpath" id="edit_xpath" /><br><br>
		<label for="edit_master_xpath">Master XML Path:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_master_xpath" id="edit_master_xpath">
			<option value="0" selected>--</option>
		<?php
			// get list of XML paths for menu
			$path_sql = sprintf("SELECT DISTINCT id, element, xpath FROM %sstructure_path ORDER BY element;", $my_db_schema);
			$path_rs = @pg_query($host_pa, $path_sql) or suicide("Unable to retrieve list of XML paths.", 1, 1);
			while ($path_row = pg_fetch_object($path_rs)) {
				printf("<option value=\"%d\">%s (%s)</option>", intval($path_row->id), htmlentities($path_row->element, ENT_QUOTES, "UTF-8"), htmlentities($path_row->xpath, ENT_QUOTES, "UTF-8"));
			}
			pg_free_result($path_rs);
		?>
		</select><br><br>
		<label for="edit_glue">Concatenation String:</label><br><input class="ui-corner-all" type="text" name="edit_glue" id="edit_glue" /><br><br>
		<label for="edit_sequence">Sequence:</label><br><input class="ui-corner-all" type="text" name="edit_sequence" id="edit_sequence" /><br><br>
		<input type="hidden" name="edit_id" id="edit_id" />
	</form>
</div>