<script>
	$(function() {
		$(".add_jurisdiction_zip").button({
			icons: { primary: "ui-icon-elrplus" }
		}).click(function() {
			var this_jID = $(this).val();
			var this_zipVal = $("#newzip_"+this_jID).val();
			if (this_zipVal == "") {
				alert("No Zip Code specified!\n\nPlease enter a valid Zip Code & try again.");
			} else {
				$("#new_zip").val(this_zipVal);
				$("#j_id").val(this_jID);
				$("#add_zip_form").submit();
				
			}
		});
		
		$(".edit_jurisdiction").button({
			icons: { primary: "ui-icon-elrpencil" }
		}).click(function() {
			var this_mID = $(this).val();
			window.location.href = "<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=4&manage_id="+this_mID;
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
		$(".newzip").keyup(function(e) {
			if (e.keyCode == 27) {
				$(this).val("");
				$(this).blur();
			} else if (e.keyCode == 13) {
				$(this).siblings(".add_jurisdiction_zip").trigger("click");
			}
			
		});
		
		$(".edit_zip").button({
			icons: { primary: "ui-icon-radio-on" }
		});
		
		$(".edit_zip").change(function() {
			if ($(this).is(":checked")) {
				$(this).button("option", { icons: { primary: "ui-icon-closethick" } });
			} else {
				$(this).button("option", { icons: { primary: "ui-icon-radio-on" } });
			}
		});
		
		$("#delete_zips").button({
			icons: { primary: "ui-icon-elrclose" }
		});
		
		$("#insert_zips").button({
			icons: { primary: "ui-icon-elrplus" }
		});
		
	});
</script>

<?php

	if (isset($_POST['add_zip']) && filter_var($_POST['add_zip'], FILTER_VALIDATE_INT) && (intval($_POST['add_zip']) == 1)) {
		// manually add one-off zip code from main view
		if ((isset($_POST['new_zip']) && filter_var($_POST['new_zip'], FILTER_VALIDATE_INT) && (intval($_POST['new_zip']) > 0)) && (isset($_POST['j_id']) && filter_var($_POST['j_id'], FILTER_VALIDATE_INT) && (intval($_POST['j_id']) > 0))) {
			unset($add_dup_count);
			$add_dup_sql = "SELECT count(id) AS counter FROM ".$my_db_schema."system_zip_codes WHERE system_district_id = ".intval($_POST['j_id'])." AND zipcode ILIKE '".intval($_POST['new_zip'])."';";
			$add_dup_rs = @pg_query($host_pa, $add_dup_sql);
			if ($add_dup_rs) {
				$add_dup_count = intval(@pg_fetch_result($add_dup_rs, 0, "counter"));
			}
			@pg_free_result($add_dup_rs);
			if (isset($add_dup_count)) {
				if ($add_dup_count == 0) {
					$add_sql = "INSERT INTO ".$my_db_schema."system_zip_codes (zipcode, system_district_id) VALUES ('".intval($_POST['new_zip'])."', ".intval($_POST['j_id']).");";
					#debug echo $add_sql;
					$add_rs = @pg_query($host_pa, $add_sql);
					if ($add_rs) {
						highlight("Zip Code '".intval($_POST['new_zip'])."' successfully added!", "ui-icon-elrsuccess");
					} else {
						suicide("Could not add Zip Code to selected jurisdiction.", 1);
					}
					@pg_free_result($add_rs);
				} else {
					suicide("Could not add Zip Code.  Zip Code '".intval($_POST['new_zip'])."' already exists for selected jurisdiction.");
				}
			} else {
				suicide("Could not add Zip Code.  Unable to check for duplicates.", 1);
			}
		} else {
			suicide("Could not add Zip Code.  Missing/invald Zip Code or Jurisdiction specified.");
		}
	} elseif (isset($_POST['edit_zip']) && is_array($_POST['edit_zip']) && (count($_POST['edit_zip']) > 0)) {
		// delete one-to-many zip codes for a selected jurisdiction
		if (isset($_POST['prune_jurisdiction']) && (intval($_POST['prune_jurisdiction']) > 0)) {
			// valid jurisdiction ID
			$prune_sql = "DELETE FROM ONLY ".$my_db_schema."system_zip_codes WHERE id IN (".implode(", ", $_POST['edit_zip']).") AND system_district_id = ".intval($_POST['prune_jurisdiction']).";";
			if ($prune_affected_rows = @pg_affected_rows(@pg_query($host_pa, $prune_sql))) {
				highlight(intval($prune_affected_rows)." Zip Codes deleted!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to delete Zip Codes.", 1);
			}
		} else {
			suicide("Unable to delete Zip Codes.  Invalid/missing Jurisdiction ID");
		}
	} elseif (isset($_POST['grow_zips']) && (strlen(trim($_POST['grow_zips'])) > 0)) {
		if (isset($_POST['grow_jurisdiction']) && (intval($_POST['grow_jurisdiction']) > 0)) {
			// valid jurisdiction ID
			$grow_zip_arr = preg_split("/[\s]*[,][\s]*/", filter_var(trim($_POST['grow_zips']), FILTER_SANITIZE_STRING));
			$grow_sql = "BEGIN;\n";
			foreach ($grow_zip_arr as $grow_zip) {
				$grow_sql .= "INSERT INTO ".$my_db_schema."system_zip_codes (zipcode, system_district_id) VALUES ('".intval($grow_zip)."', ".intval($_POST['grow_jurisdiction']).");\n";
			}
			$grow_sql .= "COMMIT;";
			if (@pg_query($host_pa, $grow_sql)) {
				highlight("Zip Codes successfully added!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to add Zip Codes.", 1);
			}
		} else {
			suicide("Unable to add Zip Codes.  Invalid/missing Jurisdiction ID");
		}
	}

	if (isset($_GET['edit_id'])) {
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_labs WHERE id = %s;", $my_db_schema, pg_escape_string(intval(trim($_GET['edit_id']))));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to save changes to lab.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to save changes to lab -- lab does not exist.");
		} else {
			$edit_sql = sprintf("UPDATE %sstructure_labs SET ui_name = %s, xml_name = %s, hl7_name = %s, alias_for = %s WHERE id = %s;",
				$my_db_schema,
				((strlen(trim($_GET['edit_labname'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_labname']))."'" : "NULL"),
				((strlen(trim($_GET['edit_xmlname'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_xmlname']))."'" : "NULL"),
				((strlen(trim($_GET['edit_hl7name'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_hl7name']))."'" : "NULL"),
				(((intval(trim($_GET['edit_alias']))) > 0) ? intval(trim($_GET['edit_alias'])) : 0),
				intval(trim($_GET['edit_id']))
			);
			if (@pg_query($host_pa, $edit_sql)) {
				highlight("Lab successfully updated!", "ui-icon-check");
			} else {
				suicide("Unable to save changes to lab.", 1);
			}
		}
	} elseif (isset($_GET['delete_id'])) {
		########## delete lab ##########
		
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_labs WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to delete lab.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to delete lab -- record not found.");
		} else {
			// check for alias labs that depend on this row, throw a dependency warning instead of deleting...
			$dependency_sql = sprintf("SELECT count(alias_for) AS counter FROM %sstructure_labs WHERE alias_for = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
			$dependency_result = @pg_query($host_pa, $dependency_sql) or suicide("Unable to delete lab.", 1, 1);
			$dependency_count = @pg_fetch_result($dependency_result, 0, "counter");
			if ($dependency_count > 0) {
				suicide("Unable to delete lab -- ".$dependency_count." alias".(($dependency_count > 1) ? "es of this lab exist" : " of this lab exists").".  Please delete any aliases for this lab first and try again.");
			} else {
				// everything checks out, commit the delete...
				$delete_sql = sprintf("DELETE FROM %sstructure_labs WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
				if (@pg_query($host_pa, $delete_sql)) {
					highlight("Lab successfully deleted!", "ui-icon-check");
				} else {
					suicide("Unable to delete lab.", 1);
				}
			}
		}
	} elseif (isset($_GET['add_flag'])) {
		// add new lab
		if (strlen(trim($_GET['new_labname'])) > 0) {
			$addlab_sql = sprintf("INSERT INTO %sstructure_labs (ui_name, xml_name, hl7_name, alias_for) VALUES (%s, %s, %s, %d)",
				$my_db_schema,
				"'".pg_escape_string(trim($_GET['new_labname']))."'",
				((strlen(trim($_GET['new_xmlname'])) > 0) ? "'".pg_escape_string(trim($_GET['new_xmlname']))."'" : "NULL"),
				((strlen(trim($_GET['new_hl7name'])) > 0) ? "'".pg_escape_string(trim($_GET['new_hl7name']))."'" : "NULL"),
				((intval(trim($_GET['new_alias'])) > 0) ? intval(trim($_GET['new_alias'])) : 0)
			);
			@pg_query($host_pa, $addlab_sql) or suicide("Could not add new lab.", 1);
			highlight("New lab \"".htmlentities(trim($_GET['new_labname']))."\" added successfully!");
		} else {
			suicide("No lab name specified!  Enter a lab name and try again.");
		}
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrsettings"></span>Manage Jurisdiction Zip Codes</h1>

<?php

	if (isset($_GET['manage_id']) && filter_var($_GET['manage_id'], FILTER_VALIDATE_INT) && (intval($_GET['manage_id']) > 0)) {
	
		$manage_j_qry = "SELECT health_district FROM ".$my_db_schema."system_districts WHERE id = ".intval($_GET['manage_id']).";";
		$manage_district_name = @pg_fetch_result(@pg_query($host_pa, $manage_j_qry), 0, "health_district");
	
?>

<style type="text/css">
	.ui-button { display: inline-block; margin: 5px; font-family: Consolas, 'Courier New', serif; font-size: 11pt !important; }
</style>

<h3>Remove <?php echo htmlentities(trim($manage_district_name)); ?> Zip Codes</h3>

<div class="lab_results_container ui-widget ui-corner-all">
	<p>To remove Zip Codes from this jurisdiction, check the desired Zip Codes & click <em>Delete Selected Zip Codes</em>.</p>
	<form id="prune_jurisdiction_form" method="POST" action="<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=4">
	<?php
		// get selected zip codes for specified jurisdiction
		$manage_z_qry = "SELECT id, zipcode FROM ".$my_db_schema."system_zip_codes WHERE system_district_id = ".intval($_GET['manage_id'])." ORDER BY zipcode;";
		$manage_z_rs = @pg_query($host_pa, $manage_z_qry);
		if ($manage_z_rs) {
			while ($manage_z_row = pg_fetch_object($manage_z_rs)) {
				echo "<input type=\"checkbox\" class=\"edit_zip\" name=\"edit_zip[]\" id=\"edit_zip_".intval($manage_z_row->id)."\" value=\"".intval($manage_z_row->id)."\"><label for=\"edit_zip_".intval($manage_z_row->id)."\">".intval($manage_z_row->zipcode)."</label>";
			}
			echo "<br><br><button id=\"delete_zips\" type=\"submit\">Delete Selected Zip Codes</button>";
		} else {
			suicide("Unable to retrieve list of Zip Codes for ".$manage_district_name.".", 1);
		}
	?>
		<input type="hidden" name="prune_jurisdiction" id="prune_jurisdiction" value="<?php echo intval($_GET['manage_id']); ?>">
	</form>
</div>

<h3>Add New <?php echo htmlentities(trim($manage_district_name)); ?> Zip Codes</h3>

<div class="lab_results_container ui-widget ui-corner-all">
	<p>To add Zip Codes to this jurisdiction, enter Zip Codes (separate multiple Zip Codes with a comma) & click <em>Add New Zip Codes</em>.</p>
	<form id="grow_jurisdiction_form" method="POST" action="<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=4">
		<textarea class="ui-corner-all" style="background-color: lightcyan; font-family: Consolas, 'Courier New'; width: 50%; height: 7em;" name="grow_zips" id="grow_zips"></textarea>
		<input type="hidden" name="grow_jurisdiction" id="grow_jurisdiction" value="<?php echo intval($_GET['manage_id']); ?>">
		<br><button id="insert_zips" type="submit">Add New Zip Codes</button>
	</form>

<?php } else { ?>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th>Jurisdiction</th>
				<th>Zip Codes</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$district_qry = sprintf("SELECT d.id, d.health_district, count(z.id) AS zip_count from %ssystem_districts d LEFT JOIN %ssystem_zip_codes z ON (d.id = z.system_district_id) GROUP BY d.id, d.health_district ORDER BY d.health_district;", $my_db_schema, $my_db_schema);
	$district_rs = @pg_query($host_pa, $district_qry);
	if ($district_rs) {
		while ($district_row = pg_fetch_object($district_rs)) {
			echo "<tr>";
			echo "<td style=\"white-space: nowrap;\" class=\"action_col\">";
			printf("<input style=\"background-color: lightcyan; font-family: Consolas, 'Courier New'; line-height: 1.9em; padding-left: 10px; width: 7em;\" type=\"text\" class=\"newzip ui-corner-all\" name=\"newzip_%s\" id=\"newzip_%s\" placeholder=\"New Zip Code\" maxlength=\"5\" />", $district_row->id, $district_row->id);
			printf("<button class=\"add_jurisdiction_zip\" type=\"button\" value=\"%s\" title=\"Add a Zip Code to this jurisdiction\">Add Zip Code</button>", $district_row->id);
			printf("<button class=\"edit_jurisdiction\" type=\"button\" value=\"%s\" title=\"Manage Zip Codes for this juristiction\">Manage Zip Codes</button>", $district_row->id);
			echo "</td>";
			echo "<td>".htmlentities($district_row->health_district)."</td>";
			echo "<td>".intval($district_row->zip_count)."</td>";
			echo "</tr>";
		}
	} else {
		suicide("Could not retrieve list of jurisdictions.", 1);
	}
	
	pg_free_result($district_rs);

?>

		</tbody>
	</table>
	
</div>

<form id="add_zip_form" method="POST" action="<?php echo $main_url; ?>?selected_page=6&submenu=5&cat=4">
	<input type="hidden" name="new_zip" id="new_zip">
	<input type="hidden" name="j_id" id="j_id">
	<input type="hidden" name="add_zip" id="add_zip" value="1">
</form>

<?php } ?>