<script>
	$(function() {
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrnotify"></span>Pending ELR E-mail Notifications</h1>

<div class="lab_results_container ui-widget ui-corner-all">
	
	<table id="labResults">
		<thead>
			<tr>
				<th>Notification Type</th>
				<th>Record Number</th>
				<th>Investigator</th>
				<th>Notify State?</th>
				<th>Notify Jurisdiction?</th>
				<th>Jurisdiction</th>
				<th>Condition [<em>Test Type</em>]</th>
				<th>Generated</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$pending_qry = sprintf("SELECT n.*, t.label AS label FROM %sbatch_notifications n INNER JOIN %sbatch_notification_types t ON (n.notification_type = t.id) WHERE (n.date_sent_lhd IS NULL AND n.notify_lhd IS TRUE) OR (n.date_sent_state IS NULL AND n.notify_state IS TRUE) ORDER BY n.date_created", $my_db_schema, $my_db_schema);
	$pending_rs = @pg_query($host_pa, $pending_qry);
	if ($pending_rs) {
	
		while ($pending_row = pg_fetch_object($pending_rs)) {
			echo "<tr>";
			echo "<td>".htmlentities($pending_row->label)."</td>";
			echo "<td>".htmlentities($pending_row->record_number)."</td>";
			echo "<td>".htmlentities($pending_row->investigator)."</td>";
			echo "<td>".((trim($pending_row->notify_state) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Yes\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"No\"></span>")."</td>";
			echo "<td>".((trim($pending_row->notify_lhd) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Yes\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"No\"></span>")."</td>";
			echo "<td>".((trim($pending_row->custom) == 't') ? '<span style="color: darkgray;">'.customLhdName(intval($pending_row->jurisdiction_id)).'</span>' : '<strong>'.lhdName(intval($pending_row->jurisdiction_id)).'</strong>')."</td>";
			echo "<td>".htmlentities($pending_row->condition)." [<em>".htmlentities($pending_row->test_type)."; ".htmlentities($pending_row->test_result)."</em>]</td>";
			echo "<td>".date("m/d/Y H:i:s", strtotime($pending_row->date_created))."</td>";
			echo "</tr>";
		}
	} else {
		suicide("Could not connect to ELR notification database.", 1);
	}
	
	@pg_free_result($pending_rs);

?>

		</tbody>
	</table>
	<br><br>
	
</div>