<script>
	$(function() {
		$("#addnew_form").show();
		
		$("#new_savelab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
		$("#udoh_enable").button({
			icons: { primary: "ui-icon-elroff" }
		});
		$("#lhd_enable").button({
			icons: { primary: "ui-icon-elroff" }
		});
		
		$(".notify_checkbox").each(function() {
			if ($(this).is(":checked")) {
				$(this).button("option", { icons: { primary: "ui-icon-elron" } });
			} else {
				$(this).button("option", { icons: { primary: "ui-icon-elroff" } });
			}
		});
		
		$(".notify_checkbox").change(function() {
			if ($(this).is(":checked")) {
				$(this).button("option", { icons: { primary: "ui-icon-elron" } });
			} else {
				$(this).button("option", { icons: { primary: "ui-icon-elroff" } });
			}
		});
		
	});
</script>
<style type="text/css">
	fieldset { padding: 10px; font-family: 'Open Sans', Arial, Helvetica, sans-serif !important; }
	legend { font-family: 'Francois One', serif; margin-left: 10px; color: firebrick; font-weight: 400; font-size: 1.5em; }
	fieldset label { font-weight: 600 !important; }
	.ui-dialog-content label, #addnew_form label.vocab_search_form2 {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		font-weight: 600;
	}
	.ui-dialog-content select, .ui-dialog-content input, #addnew_form select, #addnew_form input {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		font-weight: 400;
		background-color: lightcyan;
	}
	.ui-dialog-content {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		font-weight: 400;
	}
	.ui-dialog-title {
		font-family: 'Francois One', serif; font-weight: 400; font-size: 1.5em;
		text-shadow: 1px 1px 6px dimgray;
	}
	.ui-dialog-content h3 {
		font-family: 'Francois One', serif; font-weight: 400; font-size: 1.3em;
		color: firebrick;
	}
	#addnew_form label.vocab_search_form {
		font-family: 'Francois One', serif; font-weight: 400; font-size: 1.5em;
	}
	.ui-dialog {
		box-shadow: 4px 4px 15px dimgray;
	}
</style>

<?php

	include WEBROOT_URL.'/includes/email_functions.php';

	if (isset($_POST['edit_flag']) && filter_var($_POST['edit_flag'], FILTER_VALIDATE_INT) && (intval($_POST['edit_flag']) == 1)) {
			$edit_sql = sprintf("UPDATE %sbatch_notification_config SET udoh_enable = %s, lhd_enable = %s, udoh_email = %s WHERE id = 1;",
				$my_db_schema,
				((intval(trim($_POST['udoh_enable'])) == 1) ? "'t'" : "'f'"),
				((intval(trim($_POST['lhd_enable'])) == 1) ? "'t'" : "'f'"),
				((strlen(trim($_POST['udoh_email'])) > 0) ? "'".pg_escape_string(filter_var(trim($_POST['udoh_email']), FILTER_SANITIZE_EMAIL))."'" : "NULL")
			);
			
			if (@pg_query($host_pa, $edit_sql)) {
				highlight("Settings successfully updated!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to save settings.", 1);
			}
	}
	
	// get current state of config options...
	unset($current_lhd_enable);
	unset($current_udoh_enable);
	unset($current_udoh_email);
	
	$current_qry = "SELECT * FROM ".$my_db_schema."batch_notification_config WHERE id = 1;";
	$current_rs = @pg_query($host_pa, $current_qry);
	if ($current_rs) {
		$current_row = @pg_fetch_object($current_rs);
		$current_udoh_enable = (($current_row->udoh_enable == "t") ? true : false);
		$current_lhd_enable = (($current_row->lhd_enable == "t") ? true : false);
		$current_udoh_email = ((filter_var($current_row->udoh_email, FILTER_VALIDATE_EMAIL)) ? filter_var($current_row->udoh_email, FILTER_SANITIZE_EMAIL) : "" );
	} else {
		suicide("Unable to retrieve current ELR Notification configuration.", 1);
	}
	@pg_free_result($current_rs);

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrnotify"></span>ELR Notification Configuration</h1>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Edit Notification Settings:</label><br><br></div>
	<form id="new_lab_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&cat=<?php echo intval($cat); ?>">
		<input type="checkbox" class="notify_checkbox" name="udoh_enable" id="udoh_enable" value="1"<?php echo (($current_udoh_enable) ? " checked" : ""); ?> /><label class="vocab_search_form2" for="udoh_enable">Send State-level Notifications</label>
		<label class="vocab_search_form2" for="udoh_email">State-level Notification E-mail Address:</label><input class="ui-corner-all" type="text" name="udoh_email" id="udoh_email" value="<?php echo $current_udoh_email; ?>" /><br><br>
		<input type="checkbox" class="notify_checkbox" name="lhd_enable" id="lhd_enable" value="1"<?php echo (($current_lhd_enable) ? " checked" : ""); ?> /><label class="vocab_search_form2" for="lhd_enable">Send LHD Notifications</label>
		<input type="hidden" name="edit_flag" value="1" />
		<br><br><button type="submit" name="new_savelab" id="new_savelab">Save Changes</button>
	</form>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<h3>Configured LHD Recipients...</h3>
	<table id="labResults">
		<thead>
			<tr>
				<th>Jurisdiction</th>
				<th>E-mail Addresses</th>
			</tr>
		</thead>
		<tbody>
<?php
	$j_qry = 'SELECT health_district, system_external_id FROM '.$my_db_schema.'system_districts ORDER BY health_district;';
	$j_rs = @pg_query($host_pa, $j_qry);
	if ($j_rs !== false) {
		while ($j_row = @pg_fetch_object($j_rs)) {
			unset($this_recipients);
			unset($this_recipients_tmp);
			$this_recipients = getEmailAddressesByJurisdiction(intval($j_row->system_external_id));
			foreach ($this_recipients as $this_email) {
				if (filter_var(trim($this_email), FILTER_VALIDATE_EMAIL)) {
					$this_recipients_tmp[] = filter_var(trim($this_email), FILTER_SANITIZE_EMAIL);
				}
			}
			
			echo '<tr>';
			echo '<td>'.htmlentities($j_row->health_district).'</td>';
			echo '<td>'.( (count($this_recipients_tmp) > 0) ? implode('<br>', $this_recipients_tmp) : '<em style="color: lightgray;">&mdash;No Recipients&mdash;</em>').'</td>';
			echo '</tr>';
		}
		@pg_free_result($j_rs);
	}
?>
		</tbody>
	</table>
	<br><br>
	
</div>