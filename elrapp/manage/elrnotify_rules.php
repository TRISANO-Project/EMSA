<?php

	include_once WEBROOT_URL.'/includes/classes/notification.php';
	$nc = new NotificationContainer();

	if (isset($_GET['edit_conditions']) && (intval($_GET['edit_conditions']) == 1)) {
		// handle add/edit functions
		include_once 'elrnotify_rule_conditions.php';
	} else {
	
?>

<script>
	$(function() {
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_labname").focus();
			$(this).hide();
		});
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_saverule").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$(".edit_rule").button({
				icons: { primary: "ui-icon-elrpencil" }
			}).next().button({
				icons: { primary: "ui-icon-elrclose" }
			}).next().button({
				icons: { primary: "ui-icon-elrrules" }
			}).parent().buttonset();
		
		$(".button_disabled").button( "option", "disabled", true );
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false
		});
		
		$(".manage_rule").click(function(e) {
			e.preventDefault();
			var manageConditionAction = "<?php echo $main_url; ?>?selected_page=6&submenu=9&cat=4&edit_conditions=1&edit_id="+$(this).val();
			window.location.href = manageConditionAction;
		});
		
		$(".delete_rule").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=9&cat=4&delete_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$("#edit_lab_dialog").dialog({
			autoOpen: false,
			modal: true,
			resizable: false
		});
		
		$(".edit_rule").click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id) {
				$("#edit_id").val(jsonObj.id);
				$("#edit_name").val(jsonObj.name);
				$("#edit_notification_type").val(jsonObj.n_type_id);
				
				if (jsonObj.enabled == "t") {
					$("#edit_enabled_yes").click();
				} else {
					$("#edit_enabled_no").click();
				}
				
				if (jsonObj.send_to_state == "t") {
					$("#edit_send_to_state_yes").click();
				} else {
					$("#edit_send_to_state_no").click();
				}
				
				if (jsonObj.send_to_lhd == "t") {
					$("#edit_send_to_lhd_yes").click();
				} else {
					$("#edit_send_to_lhd_no").click();
				}
				
				$("#edit_lab_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#edit_modal_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#edit_lab_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
	});
</script>
<style type="text/css">
	fieldset { padding: 10px; font-family: 'Open Sans', Arial, Helvetica, sans-serif !important; }
	legend { font-family: 'Francois One', serif; margin-left: 10px; color: firebrick; font-weight: 400; font-size: 1.5em; }
	fieldset label { font-weight: 600 !important; }
	#ruleconditions_cancel { float: right; }
	.rule_chain {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		display: inline-block;
		margin: 2px 5px 2px 5px;
		padding: 5px 10px;
		border-left: 2px green solid;
		border-right: 2px green solid;
		border-top: 0;
		border-bottom: 0;
		background-color: rgba(102, 205, 170, 0.1);
		box-shadow: 1px 1px 7px dimgray;
		cursor: default;
		vertical-align: top;
	}
	.rule_operator {
		font-family: Consolas, 'Courier New', Courier, serif !important;
		display: inline-block;
		/* border-left: 1px dimgray dotted;
		border-right: 1px dimgray dotted;
		border-top: 0;
		border-bottom: 0;
		background-color: whitesmoke;
		margin: 2px 5px 2px 5px; */
		font-weight: 700;
		padding: 0px 5px;
		color: darkred;
		text-transform: none;
		cursor: default;
		min-width: 25px;
		text-align: center;
		vertical-align: top;
	}
	.rule_link {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		display: inline-block;
		border-left: 1px navy dotted;
		border-right: 1px navy dotted;
		border-top: 0;
		border-bottom: 0;
		font-weight: 400;
		margin: 2px 5px 2px 5px;
		padding: 5px 10px;
		color: black;
		background-color: whitesmoke;
		cursor: default;
		vertical-align: middle;
	}
	.rule_actions {
		display: inline-block;
		padding-left: 10px;
		vertical-align: top;
	}
	.link_wrapper {
		margin: 10px;
	}
	#condition_container {
		margin: 15px 0px;
	}
	.ui-dialog-content label, #addnew_form label.vocab_search_form2 {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		font-weight: 600;
	}
	.ui-dialog-content select, .ui-dialog-content input, #addnew_form select, #addnew_form input {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		font-weight: 400;
		background-color: lightcyan;
	}
	.ui-dialog-content {
		font-family: 'Open Sans', Arial, Helvetica, sans-serif !important;
		font-weight: 400;
	}
	.ui-dialog-title {
		font-family: 'Francois One', serif; font-weight: 400; font-size: 1.5em;
		text-shadow: 1px 1px 6px dimgray;
	}
	.ui-dialog-content h3 {
		font-family: 'Francois One', serif; font-weight: 400; font-size: 1.3em;
		color: firebrick;
	}
	#addnew_form label.vocab_search_form {
		font-family: 'Francois One', serif; font-weight: 400; font-size: 1.5em;
	}
	.ui-dialog {
		box-shadow: 4px 4px 15px dimgray;
	}
</style>

<?php

	if (isset($_GET['edit_id'])) {
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sbn_rules WHERE id = %s;", $my_db_schema, pg_escape_string(intval(trim($_GET['edit_id']))));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to save changes to notification rule.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to save changes to notification rule -- record does not exist.");
		} else {
			$edit_sql = sprintf("UPDATE %sbn_rules SET name = %s, send_to_state = %s, send_to_lhd = %s, notification_type = %s, enabled = %s WHERE id = %s;",
				$my_db_schema,
				((strlen(trim($_GET['edit_name'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_name']))."'" : "NULL"),
				((trim($_GET['edit_send_to_state']) == 'true') ? 'TRUE' : 'FALSE'),
				((trim($_GET['edit_send_to_lhd']) == 'true') ? 'TRUE' : 'FALSE'),
				intval(trim($_GET['edit_notification_type'])),
				((trim($_GET['edit_enabled']) == 'true') ? 'TRUE' : 'FALSE'),
				intval(trim($_GET['edit_id']))
			);
			if (@pg_query($host_pa, $edit_sql)) {
				highlight("Notification Rule successfully updated!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to save changes to Notification Rule.", 1);
			}
		}
	} elseif (isset($_GET['delete_id'])) {
		########## delete lab ##########
		
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sbn_rules WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to delete Notification Rule.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to delete Notification Rule -- record not found.");
		} else {
			// everything checks out, commit the delete...
			$delete_sql = 'BEGIN TRANSACTION;'.PHP_EOL;
			$delete_sql .= 'DELETE FROM '.$my_db_schema.'bn_rules WHERE id = '.intval($_GET['delete_id']).';'.PHP_EOL;
			$delete_sql .= 'DELETE FROM '.$my_db_schema.'bn_expression_chain WHERE rule_id = '.intval($_GET['delete_id']).';'.PHP_EOL;
			$delete_sql .= 'COMMIT;';
			if (@pg_query($host_pa, $delete_sql)) {
				highlight("Notification Rule successfully deleted!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to delete Notification Rule.", 1);
			}
		}
	} elseif (isset($_GET['add_flag'])) {
		// add new lab
		if (strlen(trim($_GET['new_name'])) > 0) {
			if (intval(trim($_GET['new_notification_type'])) > 0) {
				$addlab_sql = sprintf("INSERT INTO %sbn_rules (name, send_to_state, send_to_lhd, notification_type) VALUES (%s, %s, %s, %s);",
					$my_db_schema,
					"'".pg_escape_string(trim($_GET['new_name']))."'",
					((trim($_GET['new_send_to_state']) == 'true') ? 'TRUE' : 'FALSE'),
					((trim($_GET['new_send_to_lhd']) == 'true') ? 'TRUE' : 'FALSE'),
					intval(trim($_GET['new_notification_type']))
				);
				$addlab_rs = @pg_query($host_pa, $addlab_sql);
				if ($addlab_rs !== false) {
					//$rule_id = @pg_fetch_result($addlab_rs, 0, 'id');
					//$add_foundation_condgroup_sql = 'INSERT INTO '.$my_db_schema.'bn_expression_chain (rule_id, parent_chain_id, left_id, left_operator_id, link_type) VALUES ('.intval($rule_id).', 0, 0, 0, '.NotificationContainer::LINKTYPE_CHAIN.');';
					//@pg_query($host_pa, $add_foundation_condgroup_sql);
					highlight("New Notification Rule \"".htmlentities(trim($_GET['new_name']))."\" added successfully!");
				} else {
					suicide("Could not add new Notification Rule.", 1);
				}
			} else {
				suicide("No Notification Type selected!  Select a notification type and try again.");
			}
		} else {
			suicide("No Notification Rule name specified!  Enter a name and try again.");
		}
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrxmlrules"></span>Notification Rule Management</h1>

<div class="vocab_search ui-tabs ui-widget">
<button id="addnew_button" title="Add a new notification rule">Add New Rule</button>
</div>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New Notification Rule</label><br><br></div>
	<form id="new_lab_form" method="GET" action="<?php echo $main_page; ?>">
		<label class="vocab_search_form2 required" for="new_name">Rule Name:</label><input style="width: 20em;" class="ui-corner-all" type="text" name="new_name" id="new_name" />
		<br><br>
		<label class="vocab_search_form2 required" for="new_notification_type">Triggers Notification Type:</label>
		<select class="ui-corner-all" id="new_notification_type" name="new_notification_type">
			<option selected value="-1">--</option>
			<?php
				$new_ntypes_qry = 'SELECT id, label FROM '.$my_db_schema.'batch_notification_types ORDER BY sort;';
				$new_ntypes_rs = @pg_query($host_pa, $new_ntypes_qry);
				if ($new_ntypes_rs !== false) {
					while ($new_ntypes_row = @pg_fetch_object($new_ntypes_rs)) {
						echo '<option value="'.intval($new_ntypes_row->id).'">'.htmlentities($new_ntypes_row->label, ENT_QUOTES, 'UTF-8').'</option>'.PHP_EOL;
					}
				} else {
					suicide('Unable to get list of notification types', 1);
				}
				@pg_free_result($new_ntypes_rs);
			?>
		</select>
		<br><br>
		<label class="vocab_search_form2">Send to UDOH?:</label>
			<label class="vocab_search_form2" for="new_send_to_state_yes"><input class="edit_radio ui-corner-all" type="radio" name="new_send_to_state" id="new_send_to_state_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label class="vocab_search_form2" for="new_send_to_state_no"><input class="edit_radio ui-corner-all" type="radio" name="new_send_to_state" id="new_send_to_state_no" value="false" /> No</label>
		<br><br>
		<label class="vocab_search_form2">Send to LHD/Virtual Jurisdiction?:</label>
			<label class="vocab_search_form2" for="new_send_to_lhd_yes"><input class="edit_radio ui-corner-all" type="radio" name="new_send_to_lhd" id="new_send_to_lhd_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label class="vocab_search_form2" for="new_send_to_lhd_no"><input class="edit_radio ui-corner-all" type="radio" name="new_send_to_lhd" id="new_send_to_lhd_no" value="false" /> No</label>
		
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_saverule" id="new_saverule">Save New Notification Rule</button>
		<button type="button" id="addnew_cancel">Cancel</button>
	</form>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th></th>
				<th>Rule Name</th>
				<th>Triggers Notification Type</th>
				<th>Send to UDOH?</th>
				<th>Send to LHD/Virtual Jurisdiction?</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$qry = 'SELECT r.id AS id, n.id AS n_type_id, r.name, r.send_to_state, r.send_to_lhd, n.label, r.enabled 
		FROM '.$my_db_schema.'bn_rules r 
		LEFT JOIN '.$my_db_schema.'batch_notification_types n ON (r.notification_type = n.id) 
		ORDER BY r.name';
	$rs = pg_query($host_pa, $qry) or die("Could not connect to database: ".pg_last_error());
	
	while ($row = pg_fetch_object($rs)) {
		echo "<tr>";
		echo "<td style=\"white-space: nowrap;\" class=\"action_col\">";
		unset($edit_lab_params, $num_of_conditions);
		$num_of_conditions = $nc->getNextChain($row->id, 0, 0);
		$edit_lab_params = array(
			"id" => intval($row->id), 
			"n_type_id" => intval($row->n_type_id), 
			"name" => htmlentities($row->name, ENT_QUOTES, "UTF-8"), 
			"send_to_state" => htmlentities($row->send_to_state, ENT_QUOTES, "UTF-8"), 
			"send_to_lhd" => htmlentities($row->send_to_lhd, ENT_QUOTES, "UTF-8"), 
			"enabled" => htmlentities($row->enabled, ENT_QUOTES, "UTF-8")
		);
		printf("<button class=\"edit_rule\" type=\"button\" value='%s' title=\"Edit basic settings for this notification rule\">Edit Basics</button>", json_encode($edit_lab_params));
		printf("<button class=\"delete_rule\" type=\"button\" value=\"%s\" title=\"Permanently delete this notification rule\">Delete</button>", $row->id);
		printf("<button class=\"manage_rule\" type=\"button\" value=\"%s\" title=\"Manage conditions this notification rule\">Conditions</button>", $row->id);
		echo "</td>";
		echo "<td>".((trim($row->enabled) == "t") ? (($num_of_conditions === false) ? "<span style=\"float: right;\" class=\"ui-icon ui-icon-elrerror\" title=\"Rule Enabled, No Conditions Defined!\"></span>" : "<span style=\"float: right;\" class=\"ui-icon ui-icon-elron\" title=\"Rule Valid & Enabled\"></span>") : "<span style=\"float: right;\" class=\"ui-icon ui-icon-elrstop\" title=\"Rule Disabled\"></span>")."</td>";
		echo "<td>".htmlentities($row->name)."</td>";
		echo "<td>".htmlentities($row->label)."</td>";
		echo "<td>".((trim($row->send_to_state) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Yes\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"No\"></span>")."</td>";
		echo "<td>".((trim($row->send_to_lhd) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Yes\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"No\"></span>")."</td>";
		echo "</tr>";
	}
	
	pg_free_result($rs);

?>

		</tbody>
	</table>
	
</div>

<div id="confirm_delete_dialog" title="Delete this Notification Rule?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span> This Notification Rule will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="edit_lab_dialog" title="Edit Notification Rule">
	<form id="edit_modal_form" method="GET" action="<?php echo $main_page; ?>">
		<label>Enabled?:</label><br>
			<label for="edit_enabled_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_enabled" id="edit_enabled_yes" value="true" /> Yes (Enabled)</label>
			&nbsp;&nbsp;<label for="edit_enabled_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_enabled" id="edit_enabled_no" value="false" /> No (Disabled)</label>
		<br><br>
		<label class="required" for="edit_name">Rule Name:</label><br><input class="ui-corner-all" type="text" name="edit_name" id="edit_name" /><br><br>
		<label class="required" for="edit_notification_type">Triggers Notification Type:</label><br>
		<select class="ui-corner-all" id="edit_notification_type" name="edit_notification_type">
			<option selected value="-1">--</option>
			<?php
				$edit_ntypes_qry = 'SELECT id, label FROM '.$my_db_schema.'batch_notification_types ORDER BY sort;';
				$edit_ntypes_rs = @pg_query($host_pa, $edit_ntypes_qry);
				if ($edit_ntypes_rs !== false) {
					while ($edit_ntypes_row = @pg_fetch_object($edit_ntypes_rs)) {
						echo '<option value="'.intval($edit_ntypes_row->id).'">'.htmlentities($edit_ntypes_row->label, ENT_QUOTES, 'UTF-8').'</option>'.PHP_EOL;
					}
				} else {
					suicide('Unable to get list of notification types', 1);
				}
				@pg_free_result($edit_ntypes_rs);
			?>
		</select><br><br>
		<label>Send to UDOH?:</label><br>
			<label for="edit_send_to_state_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_send_to_state" id="edit_send_to_state_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label for="edit_send_to_state_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_send_to_state" id="edit_send_to_state_no" value="false" /> No</label>
		<br><br>
		<label>Send to LHD/Virtual Jurisdiction?:</label><br>
			<label for="edit_send_to_lhd_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_send_to_lhd" id="edit_send_to_lhd_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label for="edit_send_to_lhd_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_send_to_lhd" id="edit_send_to_lhd_no" value="false" /> No</label>
		<input type="hidden" name="edit_id" id="edit_id" />
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
	</form>
</div>

<?php
	}
?>