<script>
	$(function() {
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_labname").focus();
			$(this).hide();
		});
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_savelab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$(".edit_lab").button({
				icons: { primary: "ui-icon-elrpencil" }
			}).next().button({
				icons: { primary: "ui-icon-elrclose" }
			}).next().button({
				icons: { primary: "ui-icon-elrxml-small" }
			}).parent().buttonset();
		
		$(".button_disabled").button( "option", "disabled", true );
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false
		});
		
		$(".delete_lab").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=4&cat=1&delete_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$(".edit_hl7").click(function() {
			var hl7Action = "<?php echo $main_url; ?>?selected_page=6&submenu=4&cat=5&f[lab][]="+$(this).val();
			window.location.href = hl7Action;
		});
		
		$("#edit_lab_dialog").dialog({
			autoOpen: false,
			modal: true
		});
		
		$(".edit_lab").click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.lab_id) {
				$("#edit_id").val(jsonObj.lab_id);
				$("#edit_labname").val(jsonObj.lab_name);
				$("#edit_xmlname").val(jsonObj.xml_name);
				$("#edit_hl7name").val(jsonObj.hl7_name);
				$("#edit_alias").val(jsonObj.alias);
				$("#edit_district").val(jsonObj.district);
				
				$("#edit_lab_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#edit_modal_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#edit_lab_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
	});
	
	function edit_lab(lab_id, lab_name, xml_name, hl7_name, alias) {
		$("#edit_id").val(lab_id);
		$("#edit_labname").val(lab_name);
		$("#edit_xmlname").val(xml_name);
		$("#edit_hl7name").val(hl7_name);
		$("#edit_alias").val(alias);
		
		$("#edit_lab_dialog").dialog('option', 'buttons', {
				"Save Changes" : function() {
					$(this).dialog("close");
					$("#edit_modal_form").submit();
					},
				"Cancel" : function() {
					$(this).dialog("close");
					}
				});

		$("#edit_lab_dialog").dialog("open");
	};
</script>

<?php

	if (isset($_GET['edit_id'])) {
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_labs WHERE id = %s;", $my_db_schema, pg_escape_string(intval(trim($_GET['edit_id']))));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to save changes to lab.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to save changes to lab -- lab does not exist.");
		} else {
			$edit_sql = sprintf("UPDATE %sstructure_labs SET ui_name = %s, xml_name = %s, hl7_name = %s, alias_for = %s, default_jurisdiction_id = %s WHERE id = %s;",
				$my_db_schema,
				((strlen(trim($_GET['edit_labname'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_labname']))."'" : "NULL"),
				((strlen(trim($_GET['edit_xmlname'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_xmlname']))."'" : "NULL"),
				((strlen(trim($_GET['edit_hl7name'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_hl7name']))."'" : "NULL"),
				((intval(trim($_GET['edit_alias'])) > 0) ? intval(trim($_GET['edit_alias'])) : 0),
				((intval(trim($_GET['edit_district'])) > 0) ? intval(trim($_GET['edit_district'])) : "NULL"),
				intval(trim($_GET['edit_id']))
			);
			if (@pg_query($host_pa, $edit_sql)) {
				highlight("Lab successfully updated!", "ui-icon-check");
			} else {
				suicide("Unable to save changes to lab.", 1);
			}
		}
	} elseif (isset($_GET['delete_id'])) {
		########## delete lab ##########
		
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_labs WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to delete lab.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to delete lab -- record not found.");
		} else {
			// check for alias labs that depend on this row, throw a dependency warning instead of deleting...
			$dependency_sql = sprintf("SELECT count(alias_for) AS counter FROM %sstructure_labs WHERE alias_for = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
			$dependency_result = @pg_query($host_pa, $dependency_sql) or suicide("Unable to delete lab.", 1, 1);
			$dependency_count = @pg_fetch_result($dependency_result, 0, "counter");
			if ($dependency_count > 0) {
				suicide("Unable to delete lab -- ".$dependency_count." alias".(($dependency_count > 1) ? "es of this lab exist" : " of this lab exists").".  Please delete any aliases for this lab first and try again.");
			} else {
				// everything checks out, commit the delete...
				$delete_sql = sprintf("DELETE FROM %sstructure_labs WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
				if (@pg_query($host_pa, $delete_sql)) {
					highlight("Lab successfully deleted!", "ui-icon-check");
				} else {
					suicide("Unable to delete lab.", 1);
				}
			}
		}
	} elseif (isset($_GET['add_flag'])) {
		// add new lab
		if (strlen(trim($_GET['new_labname'])) > 0) {
			$addlab_sql = sprintf("INSERT INTO %sstructure_labs (ui_name, xml_name, hl7_name, alias_for, default_jurisdiction_id) VALUES (%s, %s, %s, %d, %s)",
				$my_db_schema,
				"'".pg_escape_string(trim($_GET['new_labname']))."'",
				((strlen(trim($_GET['new_xmlname'])) > 0) ? "'".pg_escape_string(trim($_GET['new_xmlname']))."'" : "NULL"),
				((strlen(trim($_GET['new_hl7name'])) > 0) ? "'".pg_escape_string(trim($_GET['new_hl7name']))."'" : "NULL"),
				((intval(trim($_GET['new_alias'])) > 0) ? intval(trim($_GET['new_alias'])) : 0),
				((intval(trim($_GET['new_district'])) > 0) ? intval(trim($_GET['new_district'])) : "NULL")
			);
			@pg_query($host_pa, $addlab_sql) or suicide("Could not add new lab.", 1);
			highlight("New lab \"".htmlentities(trim($_GET['new_labname']))."\" added successfully!");
		} else {
			suicide("No lab name specified!  Enter a lab name and try again.");
		}
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrlab"></span>Lab Management</h1>

<div class="vocab_search ui-tabs ui-widget">
<button id="addnew_button" title="Add a new lab">Add New Lab</button>
</div>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New Lab:</label><br><br></div>
	<form id="new_lab_form" method="GET" action="<?php echo $main_page; ?>">
		<label class="vocab_search_form2" for="new_labname">Lab Name:</label><input class="ui-corner-all" type="text" name="new_labname" id="new_labname" />
		<label class="vocab_search_form2" for="new_xmlname">XML Name:</label><input class="ui-corner-all" type="text" name="new_xmlname" id="new_xmlname" />
		<label class="vocab_search_form2" for="new_hl7name">HL7 Name:</label><input class="ui-corner-all" type="text" name="new_hl7name" id="new_hl7name" />
		<label class="vocab_search_form2" for="new_district">Default Jurisdiction:</label>
			<select class="ui-corner-all" name="new_district" id="new_district">
				<option value="0" selected>--</option>
			<?php
				// get list of top-level labs for alias menu
				$newdistrict_sql = sprintf("SELECT id, health_district FROM %ssystem_districts ORDER BY health_district;", $my_db_schema);
				$newdistrict_result = @pg_query($host_pa, $newdistrict_sql) or suicide("Unable to retrieve list of Jurisdictions.", 1, 1);
				while ($newdistrict_row = pg_fetch_object($newdistrict_result)) {
					printf("<option value=\"%d\">%s</option>", intval($newdistrict_row->id), htmlentities($newdistrict_row->health_district));
				}
				pg_free_result($newdistrict_result);
			?>
			</select><br><br>
		<label class="vocab_search_form2" for="new_alias">Alias For:</label>
			<select class="ui-corner-all" name="new_alias" id="new_alias">
				<option value="0" selected>--</option>
			<?php
				// get list of top-level labs for alias menu
				$newalias_sql = sprintf("SELECT DISTINCT id, ui_name FROM %sstructure_labs WHERE alias_for < 1;", $my_db_schema);
				$newalias_result = @pg_query($host_pa, $newalias_sql) or suicide("Unable to retrieve list of aliases.", 1, 1);
				while ($newalias_row = pg_fetch_object($newalias_result)) {
					printf("<option value=\"%d\">%s</option>", intval($newalias_row->id), htmlentities($newalias_row->ui_name));
				}
				pg_free_result($newalias_result);
			?>
			</select>
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_savelab" id="new_savelab">Save New Lab</button>
		<button type="button" id="addnew_cancel">Cancel</button>
	</form>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th>Lab Name</th>
				<th>XML Name</th>
				<th>HL7 Name</th>
				<th>Alias For</th>
				<th>Default Jurisdiction</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$lab_qry = sprintf("SELECT l.*, j.health_district from %sstructure_labs l LEFT JOIN %ssystem_districts j ON (l.default_jurisdiction_id = j.id) ORDER BY ui_name", $my_db_schema, $my_db_schema);
	$lab_rs = pg_query($host_pa, $lab_qry) or die("Could not connect to lab management database: ".pg_last_error());
	
	while ($lab_row = pg_fetch_object($lab_rs)) {
		echo "<tr>";
		echo "<td style=\"white-space: nowrap;\" class=\"action_col\">";
		unset($edit_lab_params);
		$edit_lab_params = array(
			"lab_id" => intval($lab_row->id), 
			"lab_name" => htmlentities($lab_row->ui_name, ENT_QUOTES, "UTF-8"), 
			"xml_name" => htmlentities($lab_row->xml_name, ENT_QUOTES, "UTF-8"), 
			"hl7_name" => htmlentities($lab_row->hl7_name, ENT_QUOTES, "UTF-8"), 
			"alias" => intval($lab_row->alias_for),
			"district" => intval($lab_row->default_jurisdiction_id)
		);
		printf("<button class=\"edit_lab\" type=\"button\" value='%s' title=\"Edit this lab\">Edit</button>", json_encode($edit_lab_params));
		printf("<button class=\"delete_lab\" type=\"button\" value=\"%s\" title=\"Permanently delete this lab\">Delete</button>", $lab_row->id);
		printf("<button class=\"edit_hl7%s\" type=\"button\" value=\"%s\" title=\"%s\">HL7</button>", ((intval($lab_row->alias_for) > 0) ? " button_disabled" : ""), $lab_row->id, ((intval($lab_row->alias_for) > 0) ? "" : "Manage HL7 mapping for this lab"));
		echo "</td>";
		echo "<td>".htmlentities($lab_row->ui_name)."</td>";
		echo "<td>".htmlentities($lab_row->xml_name)."</td>";
		echo "<td>".htmlentities($lab_row->hl7_name)."</td>";
		
		echo "<td>";
		if (intval($lab_row->alias_for) > 0) {
			$alias_qry = sprintf("SELECT ui_name FROM %sstructure_labs WHERE id = %d", $my_db_schema, intval($lab_row->alias_for));
			echo pg_fetch_result(pg_query($host_pa, $alias_qry), 0, "ui_name");
		} else {
			echo "--";
		}
		echo "</td>";
		echo "<td>".htmlentities($lab_row->health_district)."</td>";
		echo "</tr>";
	}
	
	pg_free_result($lab_rs);

?>

		</tbody>
	</table>
	
</div>

<div id="confirm_delete_dialog" title="Delete this lab?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This lab and any associated HL7 mappings will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="edit_lab_dialog" title="Edit Lab">
	<form id="edit_modal_form" method="GET" action="<?php echo $main_page; ?>">
		<label for="edit_labname">Lab Name:</label><br><input class="ui-corner-all" type="text" name="edit_labname" id="edit_labname" /><br><br>
		<label for="edit_xmlname">XML Name:</label><br><input class="ui-corner-all" type="text" name="edit_xmlname" id="edit_xmlname" /><br><br>
		<label for="edit_hl7name">HL7 Name:</label><br><input class="ui-corner-all" type="text" name="edit_hl7name" id="edit_hl7name" /><br><br>
		<label for="edit_district">Default Jurisdiction:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_district" id="edit_district">
			<option value="0" selected>--</option>
		<?php
			// get list of top-level labs for alias menu
			$newdistrict_sql = sprintf("SELECT id, health_district FROM %ssystem_districts ORDER BY health_district;", $my_db_schema);
			$newdistrict_result = @pg_query($host_pa, $newdistrict_sql) or suicide("Unable to retrieve list of Jurisdictions.", 1, 1);
			while ($newdistrict_row = pg_fetch_object($newdistrict_result)) {
				printf("<option value=\"%d\">%s</option>", intval($newdistrict_row->id), htmlentities($newdistrict_row->health_district));
			}
			pg_free_result($newdistrict_result);
		?>
		</select><br><br>
		<label for="edit_alias">Alias For:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_alias" id="edit_alias">
			<option value="0" selected>--</option>
		<?php
			// get list of top-level labs for alias menu
			$newalias_sql = sprintf("SELECT DISTINCT id, ui_name FROM %sstructure_labs WHERE alias_for < 1;", $my_db_schema);
			$newalias_result = @pg_query($host_pa, $newalias_sql) or suicide("Unable to retrieve list of aliases.", 1, 1);
			while ($newalias_row = pg_fetch_object($newalias_result)) {
				printf("<option value=\"%d\">%s</option>", intval($newalias_row->id), htmlentities($newalias_row->ui_name));
			}
			pg_free_result($newalias_result);
		?>
		</select>
		<input type="hidden" name="edit_id" id="edit_id" />
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
	</form>
</div>