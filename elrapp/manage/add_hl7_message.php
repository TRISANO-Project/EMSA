<script type="text/javascript">
	$(function() {
		$("#addhl7form").show();
		$("#hl7message").focus();
		
		$("#btn_save").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
	});
</script>

<?php

	switch (trim($_POST['lab_selector'])) {
		case 'ELR':
		case 'ELR-ARUP':
		case 'ELR-USLPH':
			$connector = trim($_POST['lab_selector']);
			break;
		default:
			$connector = false;
	}

	if (isset($_POST['add_flag']) && filter_var($_POST['add_flag'], FILTER_VALIDATE_INT) && (intval($_POST['add_flag']) == 1) && $connector !== false) {
		if (strlen(trim($_POST['hl7message'])) > 0) {
			$sql = "INSERT INTO ".$my_db_schema."system_original_messages (message, connector, sent, status, created_at, port, message_type, channel) VALUES ('".pg_escape_string($_POST['hl7message'])."', '".$connector."', 0, 0, '".date("Y-m-d H:i:s", time())."', 'ELR', 'HL7', 6) RETURNING id";
			$rs = @pg_query($host_pa, $sql);
			$row = @pg_fetch_object($rs);
			
			if (intval($row->id) > 0) {
				$message_id = intval($row->id);
				highlight("HL7 Message added and will be processed.  The Original Message ID: <span style=\"display: inline-block;\" class=\"ui-icon ui-icon-newwin\"></span><a title=\"View Audit Log\" style=\"color: royalblue; font-weight: bold; text-decoration: underline;\" href=\"".$main_url."/?view_type=1&message_id=".$message_id."&selected_page=6&submenu=5&cat=5\" target=\"_blank\">".$message_id."</a> can be used to track the message.</p>", "ui-icon-elrsuccess");
			} else {
				suicide("An error occurred while attempting to stage the new HL7 Message.", 1);
			}
		} else {
			suicide("No HL7 message detected.  Please try again.");
		}
	}
?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrxml"></span>Manually Add HL7 Message</h1>

<div id="addhl7form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New HL7 Message:</label><br><br></div>
	<form id="new_onboard_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&cat=<?php echo intval($cat); ?>">
		<label style="font-weight: 600;" for="lab_selector">Choose Sending Laboratory:</label><br>
		<select class="ui-corner-all" id="lab_selector" name="lab_selector">
			<option <?php if (($connector === false) || ($connector == 'ELR-ARUP')) { echo 'selected'; } ?> value="ELR-ARUP">ARUP Laboratories</option>
			<option <?php if ($connector == 'ELR-USLPH') { echo 'selected'; } ?> value="ELR-USLPH">UPHL</option>
		</select><br><br>
		<label style="font-weight: 600;" for="lab_selector">Enter HL7 Message:</label><br>
		<textarea class="ui-corner-all" name="hl7message" id="hl7message" style="width: 70%; height: 12em;"></textarea>
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="btn_save" id="btn_save">Submit</button>
	</form>
</div>
