<?php

	include WEBROOT_URL.'/includes/vocab_functions.php';
	include WEBROOT_URL.'/includes/classes/vocab_audit.php';
	
	$va = new VocabAudit();  // set up vocabulary auditer for later

	/**
	 * hard-code to TriSano App for now, leave room for growth if adding more apps later
	 */
	$app_id = 1;
	
	
	/**
	 * check for session freshness since last update to session-stored model info
	 * (columns/filters are stored in session data, this hack gives us a way to force
	 * session data to be refreshed without forcing users to clear cookies if the data
	 * is updated mid-session, so that the current columns/filters are used)
	 */
	// set "freshness date" (manual by developer to force client updates)
	// $model_last_updated = strtotime("3:09 PM 9/24/2012");
	
	/**
	 * Change to use 'filemtime()' to dynamically get last modification date of file
	 * Much less hassle than having to manually set a 'freshness date' each edit
	 */
	$model_last_updated = filemtime("manage/vocabulary.php");
	
	// check "freshness date"...
	if (isset($_SESSION['vocab_model_fresh'])) {
		if ($_SESSION['vocab_model_fresh'] < $model_last_updated) {
			// old model data; unset vocab_params & set a new "freshness date"...
			unset($_SESSION["vocab_params"]);
			$_SESSION['vocab_model_fresh'] = time();
		}
	} else {
		// hack for sessions set before "freshness date" implemented
		unset($_SESSION["vocab_params"]);
		$_SESSION['vocab_model_fresh'] = time();
	}
	
	
	########## Session Prep ##########
	// switch vocab type based on 'vocab' querystring value
	$_SESSION["vocab_params"]["vocab"] = 1;
	if (isset($_GET["vocab"])) {
		switch (intval(trim($_GET["vocab"]))) {
			case 2:
				$_SESSION["vocab_params"]["vocab"] = 2;
				break;
			case 3:
				$_SESSION["vocab_params"]["vocab"] = 3;
				break;
			case 4:
				$_SESSION["vocab_params"]["vocab"] = 4;
				break;
			case 5:
				$_SESSION["vocab_params"]["vocab"] = 5;
				break;
			default:
				$_SESSION["vocab_params"]["vocab"] = 1;
				break;
		}
	}
	
	/**
	 * Check if adding/editing a child LOINC interpretive result rule...
	 */
	if (isset($_GET['rulemod_action']) && in_array(trim($_GET['rulemod_action']), array("add", "edit", "delete"))) {
		include 'result_rules.php';
	}
	
	/**
	 * Check if adding/editing a Master LOINC case management rule...
	 */
	if (isset($_GET['rulemod_cmr_action']) && in_array(trim($_GET['rulemod_cmr_action']), array("add", "edit", "delete"))) {
		include 'case_management_rules.php';
	}
		
		
		if (!isset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]])) {
			// if no params exist for selected vocab, load default values for Master LOINC...
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"] = DEFAULT_ROWS_PER_PAGE;
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_order"] = "ASC";
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["table_name"] = "vocab_master_loinc";
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"] = "l_id";
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["from_tables"] = sprintf("FROM %svocab_master_loinc vml 
				LEFT JOIN %ssystem_statuses ss ON (ss.id = vml.list) 
				LEFT JOIN %svocab_master_condition vmc ON (vmc.c_id = vml.trisano_condition) 
				LEFT JOIN %svocab_master_organism vmo ON (vmo.o_id = vml.trisano_organism) 
				LEFT JOIN %svocab_master_vocab mv_c ON (mv_c.id = vmc.condition) 
				LEFT JOIN %svocab_master_vocab mv_o ON (mv_o.id = vmo.organism) 
				LEFT JOIN %svocab_master_vocab mv_t ON (mv_t.id = vml.trisano_test_type) 
				LEFT JOIN %svocab_master_vocab mv_p ON (mv_p.id = vml.specimen_source)", 
				$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"] = "loinc";
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"] = "Master LOINC";
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] = array(
				"l_id" => array("colname" => "vml.l_id", "label" => "l_id", "display" => FALSE, "filter" => FALSE, "textsearch" => FALSE),
				"loinc" => array("colname" => "vml.loinc", "label" => "LOINC", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
				"concept_name" => array("colname" => "vml.concept_name", "label" => "Preferred Concept Name", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
				"condition_from_result" => array("colname" => "vml.condition_from_result", "label" => "Look Up Condition?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vml.condition_from_result"),
				"trisano_condition" => array("colname" => "mv_c.concept", "label" => "Master Condition", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmc.c_id AS value, mv.concept AS label FROM %svocab_master_condition vmc JOIN %svocab_master_vocab mv ON (vmc.condition = mv.id) ORDER BY mv.concept;", $my_db_schema, $my_db_schema), "filtercolname" => "vml.trisano_condition"),
				"organism_from_result" => array("colname" => "vml.organism_from_result", "label" => "Look Up Organism?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vml.organism_from_result"),
				"trisano_organism" => array("colname" => "mv_o.concept", "label" => "Master Organism", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmo.o_id AS value, mv.concept AS label FROM %svocab_master_organism vmo JOIN %svocab_master_vocab mv ON (vmo.organism = mv.id) ORDER BY mv.concept;", $my_db_schema, $my_db_schema), "filtercolname" => "vml.trisano_organism"),
				"trisano_test_type" => array("colname" => "mv_t.concept", "label" => "Test Type", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('test_type')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vml.trisano_test_type"),
				"specimen_source" => array("colname" => "mv_p.concept", "label" => "Specimen Source", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('specimen')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vml.specimen_source"),
				"list" => array("colname" => "ss.name", "label" => "List", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT id AS value, name AS label FROM %ssystem_statuses WHERE id IN (%s) ORDER BY label;", $my_db_schema, implode(',', array(WHITE_STATUS, BLACK_STATUS))), "filtercolname" => "vml.list"),
				"gray_rule" => array("colname" => "vml.gray_rule", "label" => "Graylist Rules", "display" => TRUE, "filter" => FALSE));
			
			switch (intval($_SESSION["vocab_params"]["vocab"])) {
				case 2:
					// Master Condition
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["table_name"] = "vocab_master_condition";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"] = "c_id";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["from_tables"] = sprintf("FROM %svocab_master_condition vmc 
						LEFT JOIN %svocab_master_vocab mv_c ON (mv_c.id = vmc.condition)
						LEFT JOIN %svocab_master_vocab mv_dc ON (mv_dc.id = vmc.disease_category) 
						LEFT JOIN %ssystem_districts sd ON (sd.id = vmc.district_override)", 
						$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"] = "Master Condition";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"] = "condition";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] = array(
						"c_id" => array("colname" => "vmc.c_id", "label" => "c_id", "display" => FALSE, "filter" => FALSE, "textsearch" => FALSE),
						"disease_category" => array("colname" => "mv_dc.concept", "label" => "Disease Category", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('disease_category')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vmc.disease_category"), 
						"condition" => array("colname" => "mv_c.concept", "label" => "Condition", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmc.c_id AS value, mv.concept AS label FROM %svocab_master_condition vmc JOIN %svocab_master_vocab mv ON (vmc.condition = mv.id) ORDER BY mv.concept;", $my_db_schema, $my_db_schema), "filtercolname" => "vmc.c_id"),
						"gateway_xref" => array("colname" => "vmc.gateway_xref", "label" => "Gateway Crossrefs", "display" => TRUE, "filter" => FALSE, "textsearch" => FALSE, "filtercolname" => "vmc.gateway_xref"),
						"check_xref_first" => array("colname" => "vmc.check_xref_first", "label" => "Check Crossrefs First", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vmc.check_xref_first"),
						"immediate_notify" => array("colname" => "vmc.immediate_notify", "label" => "Immediately Notifiable", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vmc.immediate_notify"),
						"require_specimen" => array("colname" => "vmc.require_specimen", "label" => "Require Specimen Source From Nominal", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vmc.require_specimen"),
						"valid_specimen" => array("colname" => "vmc.valid_specimen", "label" => "Valid Specimen Sources", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE, "filtercolname" => "vmc.valid_specimen"),
						"invalid_specimen" => array("colname" => "vmc.invalid_specimen", "label" => "Invalid Specimen Sources", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE, "filtercolname" => "vmc.invalid_specimen"),
						"white_rule" => array("colname" => "vmc.white_rule", "label" => "Morbidity Whitelist Rules", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filtercolname" => "vmc.white_rule"),
						"contact_white_rule" => array("colname" => "vmc.contact_white_rule", "label" => "Contact Whitelist Rules", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filtercolname" => "vmc.contact_white_rule"),
						"new_event" => array("colname" => "vmc.new_event", "label" => "Different Species/Toxin New or Same Event", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vmc.new_event"),
						"notify_state" => array("colname" => "vmc.notify_state", "label" => "Notify State Upon Receipt", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vmc.notify_state"),
						"autoapproval" => array("colname" => "vmc.autoapproval", "label" => "Autoapproval", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vmc.autoapproval"), 
						"district_override" => array("colname" => "sd.health_district", "label" => "Jurisdiction Override", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT sd.id AS value, sd.health_district AS label FROM %ssystem_districts sd ORDER BY sd.health_district;", $my_db_schema), "filtercolname" => "vmc.district_override"));
					break;
				case 3:
					// Master Organism
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["table_name"] = "vocab_master_organism";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"] = "o_id";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["from_tables"] = sprintf("FROM %svocab_master_organism vmo 
						LEFT JOIN %ssystem_statuses ss ON (ss.id = vmo.list) 
						LEFT JOIN %svocab_master_condition vmc ON (vmc.c_id = vmo.condition) 
						LEFT JOIN %svocab_master_vocab mv_c ON (mv_c.id = vmc.condition) 
						LEFT JOIN %svocab_master_vocab mv_o ON (mv_o.id = vmo.organism) 
						LEFT JOIN %svocab_master_vocab mv_s ON (mv_s.id = vmo.status)
						LEFT JOIN %svocab_master_vocab mv_sc ON (mv_sc.id = vmo.snomed_category) 
						LEFT JOIN %svocab_master_vocab mv_t ON (mv_t.id = vmo.test_result)", 
						$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"] = "Master SNOMED";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"] = "condition";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] = array(
						"o_id" => array("colname" => "vmo.o_id", "label" => "o_id", "display" => FALSE, "filter" => FALSE, "textsearch" => FALSE),
						"snomed_category" => array("colname" => "mv_sc.concept", "label" => "SNOMED Type", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('snomed_category')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vmo.snomed_category"), 
						"snomed" => array("colname" => "vmo.snomed", "label" => "SNOMED Code", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
						"snomed_alt" => array("colname" => "vmo.snomed_alt", "label" => "Secondary SNOMED Code", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
						"condition" => array("colname" => "mv_c.concept", "label" => "Master Condition", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmc.c_id AS value, mv.concept AS label FROM %svocab_master_condition vmc JOIN %svocab_master_vocab mv ON (vmc.condition = mv.id) ORDER BY mv.concept;", $my_db_schema, $my_db_schema), "filtercolname" => "vmo.condition"),
						"organism" => array("colname" => "mv_o.concept", "label" => "Type Concept Name", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmo.o_id AS value, mv.concept AS label FROM %svocab_master_organism vmo JOIN %svocab_master_vocab mv ON (vmo.organism = mv.id) ORDER BY mv.concept;", $my_db_schema, $my_db_schema), "filtercolname" => "vmo.o_id"),
						"list" => array("colname" => "ss.name", "label" => "List", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT id AS value, name AS label FROM %ssystem_statuses WHERE id IN (%s) ORDER BY label;", $my_db_schema, implode(',', array(WHITE_STATUS, BLACK_STATUS))), "filtercolname" => "vmo.list"),
						"test_result" => array("colname" => "mv_t.concept", "label" => "Test Result", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('test_result')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vmo.test_result"), 
						"status" => array("colname" => "mv_s.concept", "label" => "Set State Case Status To...", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('case')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vmo.status"), 
						"nom_is_surveillance" => array("colname" => "vmo.nom_is_surveillance", "label" => "Nominal Surveillance?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vmo.nom_is_surveillance"));
					break;
				case 4:
					// Child Organism
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["table_name"] = "vocab_child_organism";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"] = "vco.id";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["from_tables"] = sprintf("FROM %svocab_child_organism vco 
						LEFT JOIN %svocab_master_organism vmo ON (vco.organism = vmo.o_id) 
						LEFT JOIN %svocab_master_organism vmo_t ON (vco.test_result_id = vmo_t.o_id) 
						LEFT JOIN %sstructure_labs sl ON (vco.lab_id = sl.id) 
						LEFT JOIN %svocab_master_vocab mv_o ON (mv_o.id = vmo.organism) 
						LEFT JOIN %svocab_master_vocab mv_t ON (mv_t.id = vmo_t.organism)", 
						$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"] = "Child SNOMED";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"] = "organism";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] = array(
						"id" => array("colname" => "vco.id", "label" => "id", "display" => FALSE, "filter" => FALSE, "textsearch" => FALSE),
						"lab" => array("colname" => "sl.ui_name", "label" => "Child Lab", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT sl.id AS value, sl.ui_name AS label FROM %sstructure_labs sl WHERE sl.alias_for < 1 ORDER BY sl.ui_name;", $my_db_schema), "filtercolname" => "vco.lab_id"),
						"child_code" => array("colname" => "vco.child_code", "label" => "Child Code", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
						"snomed" => array("colname" => "vmo.snomed", "label" => "Master Organism SNOMED", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE, "linkback" => "3"),
						"organism" => array("colname" => "mv_o.concept", "label" => "Master Organism", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmo.o_id AS value, mv.concept AS label FROM %svocab_master_organism vmo JOIN %svocab_master_vocab mv ON (vmo.organism = mv.id) INNER JOIN %svocab_master_vocab mv2 ON (vmo.snomed_category = mv2.id) WHERE mv2.category = %svocab_category_id('snomed_category') AND mv2.concept = 'Organism' ORDER BY mv.concept;", $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema), "filtercolname" => "vco.organism"),
						"test_result_snomed" => array("colname" => "vmo_t.snomed", "label" => "Master Test Result SNOMED", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE, "linkback" => "3"),
						"test_result" => array("colname" => "mv_t.concept", "label" => "Master Test Result", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmo.o_id AS value, mv.concept AS label FROM %svocab_master_organism vmo JOIN %svocab_master_vocab mv ON (vmo.organism = mv.id) INNER JOIN %svocab_master_vocab mv2 ON (vmo.snomed_category = mv2.id) WHERE mv2.category = %svocab_category_id('snomed_category') AND mv2.concept = 'Test Result' ORDER BY mv.concept;", $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema), "filtercolname" => "vco.test_result_id"),
						"result_value" => array("colname" => "vco.result_value", "label" => "Result Value", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE, "filtercolname" => "vco.result_value"),
						"comment" => array("colname" => "vco.comment", "label" => "Comments", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE, "filtercolname" => "vco.comment"));
					break;
				case 5:
					// Child LOINC
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["table_name"] = "vocab_child_loinc";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"] = "vcl.id";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["from_tables"] = sprintf("FROM %svocab_child_loinc vcl 
						LEFT JOIN %svocab_master_loinc vml ON (vcl.master_loinc = vml.l_id) 
						LEFT JOIN %svocab_master_vocab mv_l ON (mv_l.id = vcl.result_location) 
						LEFT JOIN %sstructure_labs sl ON (vcl.lab_id = sl.id)", 
						$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"] = "Child LOINC";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"] = "child_loinc";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] = array(
						"id" => array("colname" => "vcl.id", "label" => "id", "display" => FALSE, "filter" => FALSE, "textsearch" => FALSE),
						"archived" => array("colname" => "vcl.archived", "label" => "'Archived' Flag", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vcl.archived"),
						"lab" => array("colname" => "sl.ui_name", "label" => "Child Lab", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT sl.id AS value, sl.ui_name AS label FROM %sstructure_labs sl WHERE sl.alias_for < 1 ORDER BY sl.ui_name;", $my_db_schema), "filtercolname" => "vcl.lab_id"),
						"child_loinc" => array("colname" => "vcl.child_loinc", "label" => "Child LOINC", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
						"master_loinc" => array("colname" => "vml.loinc", "label" => "Master LOINC", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vml.l_id AS value, vml.loinc AS label FROM %svocab_master_loinc vml ORDER BY vml.loinc;", $my_db_schema), "filtercolname" => "vcl.master_loinc", "linkback" => "1"),
						"concept_name" => array("colname" => "vml.concept_name", "label" => "Preferred Concept Name", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vml.l_id AS value, vml.concept_name AS label FROM %svocab_master_loinc vml ORDER BY vml.concept_name;", $my_db_schema, $my_db_schema), "filtercolname" => "vcl.master_loinc"),
						"child_concept_name" => array("colname" => "vcl.child_concept_name", "label" => "Child Concept Name", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE), 
						"result_location" => array("colname" => "mv_l.concept", "label" => "Result Location", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('result_type')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vcl.result_location"),
						"interpret_results" => array("colname" => "vcl.interpret_results", "label" => "Quantitative Result?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vcl.interpret_results"),
						"semi_auto" => array("colname" => "vcl.semi_auto", "label" => "Semi-Auto Entry?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vcl.semi_auto"),
						"validate_child_snomed" => array("colname" => "vcl.validate_child_snomed", "label" => "Validate Child SNOMED?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vcl.validate_child_snomed"),
						"units" => array("colname" => "vcl.units", "label" => "Units", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
						"pregnancy" => array("colname" => "vcl.pregnancy", "label" => "Indicates Pregnancy?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vcl.pregnancy"),
						"refrange" => array("colname" => "vcl.refrange", "label" => "Reference Range", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE), 
						"hl7_refrange" => array("colname" => "vcl.hl7_refrange", "label" => "HL7 Reference Range", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE));
					break;
				default:
					// Master LOINC fallback
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["table_name"] = "vocab_master_loinc";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"] = "l_id";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["from_tables"] = sprintf("FROM %svocab_master_loinc vml 
						LEFT JOIN %ssystem_statuses ss ON (ss.id = vml.list) 
						LEFT JOIN %svocab_master_condition vmc ON (vmc.c_id = vml.trisano_condition) 
						LEFT JOIN %svocab_master_organism vmo ON (vmo.o_id = vml.trisano_organism) 
						LEFT JOIN %svocab_master_vocab mv_c ON (mv_c.id = vmc.condition) 
						LEFT JOIN %svocab_master_vocab mv_o ON (mv_o.id = vmo.organism) 
						LEFT JOIN %svocab_master_vocab mv_t ON (mv_t.id = vml.trisano_test_type) 
						LEFT JOIN %svocab_master_vocab mv_p ON (mv_p.id = vml.specimen_source)", 
						$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"] = "loinc";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"] = "Master LOINC";
					$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] = array(
						"l_id" => array("colname" => "vml.l_id", "label" => "l_id", "display" => FALSE, "filter" => FALSE, "textsearch" => FALSE),
						"loinc" => array("colname" => "vml.loinc", "label" => "LOINC", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
						"concept_name" => array("colname" => "vml.concept_name", "label" => "Preferred Concept Name", "display" => TRUE, "filter" => FALSE, "textsearch" => TRUE),
						"condition_from_result" => array("colname" => "vml.condition_from_result", "label" => "Look Up Condition?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vml.condition_from_result"),
						"trisano_condition" => array("colname" => "mv_c.concept", "label" => "Master Condition", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmc.c_id AS value, mv.concept AS label FROM %svocab_master_condition vmc JOIN %svocab_master_vocab mv ON (vmc.condition = mv.id) ORDER BY mv.concept;", $my_db_schema, $my_db_schema), "filtercolname" => "vml.trisano_condition"),
						"organism_from_result" => array("colname" => "vml.organism_from_result", "label" => "Look Up Organism?", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filtercolname" => "vml.organism_from_result"),
						"trisano_organism" => array("colname" => "mv_o.concept", "label" => "Master Organism", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vmo.o_id AS value, mv.concept AS label FROM %svocab_master_organism vmo JOIN %svocab_master_vocab mv ON (vmo.organism = mv.id) ORDER BY mv.concept;", $my_db_schema, $my_db_schema), "filtercolname" => "vml.trisano_organism"),
						"trisano_test_type" => array("colname" => "mv_t.concept", "label" => "Test Type", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('test_type')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vml.trisano_test_type"),
						"specimen_source" => array("colname" => "mv_p.concept", "label" => "Specimen Source", "display" => TRUE, "filter" => TRUE, "textsearch" => TRUE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id('specimen')) ORDER BY vm.concept;", $my_db_schema), "filtercolname" => "vml.specimen_source"),
						"list" => array("colname" => "ss.name", "label" => "List", "display" => TRUE, "filter" => TRUE, "textsearch" => FALSE, "filterlookup" => TRUE, "lookupqry" => sprintf("SELECT id AS value, name AS label FROM %ssystem_statuses WHERE id IN (%s) ORDER BY label;", $my_db_schema, implode(',', array(WHITE_STATUS, BLACK_STATUS))), "filtercolname" => "vml.list"),
						"gray_rule" => array("colname" => "vml.gray_rule", "label" => "Graylist Rules", "display" => TRUE, "filter" => FALSE));
					break;
			}
		}
		
		/**
		 * Search/Filter Prep
		 * 
		 * this must happen after setting vocab defaults, otherwise condition can occur where setting query params can
		 * fool the sysetm into thinking default vocab data exists when it doesn't in cases of a linked query
		 */
		// pre-build our vocab-specific search data...
		if (isset($_GET["q"])) {
			if ((trim($_GET["q"]) != "Enter search terms...") && (strlen(trim($_GET["q"])) > 0)) {
				$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_display"] = htmlentities(trim($_GET["q"]), ENT_QUOTES, "UTF-8");
				$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_sql"] = pg_escape_string(trim($_GET["q"]));
				if (!isset($_GET['f'])) {
					// search query found, but no filters selected
					// if any filters were previously SESSIONized, they've been deselected via the UI, so we'll clear them...
					unset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"]);
				}
			} else {
				// search field was empty/defaulted, so we'll destroy the saved search params...
				unset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_display"]);
				unset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_sql"]);
				// not only was search blank, but no filters selected, so clear them as well...
				unset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"]);
			}
		}
		
		// update SESSIONized filters or destroy them if no filters are selected...
		if (isset($_GET['f'])) {
			if (is_array($_GET['f'])) {
				$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"] = $_GET['f'];
			}
		}
		
		// if child loinc & no filter for archived bit, default to hide archived loincs
		if (($_SESSION["vocab_params"]["vocab"] == 5) && (!isset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"]["archived"]))) {
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"]["archived"][] = 'f';
		}
		
		/**
		 * if edit_id passed, include editor
		 *
		 * this must happen after setting the session vocab value & vocab type defaults, else xref links between dependent items will fail
		 */
		if (isset($_GET['edit_id'])){
			include 'vocabulary_edit.php';
		}
		
		// sort out our sorting...
		if (isset($_GET["order"])) {
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_order"] = ($_GET["order"] == "2") ? "DESC" : "ASC";
		}
		
		// check that column name is a valid column...
		$valid_cols_qry = "SELECT column_name FROM information_schema.columns WHERE table_name IN ('vocab_master_loinc', 'vocab_master_organism', 'vocab_master_condition', 'vocab_child_loinc', 'vocab_child_organism');";
		$valid_cols_rs = pg_query($host_pa,$valid_cols_qry) or die("Can't Perform Query ".pg_last_error());
		while ($valid_cols_row = pg_fetch_object($valid_cols_rs)) {
			$valid_cols[] = $valid_cols_row->column_name;
		}
		pg_free_result($valid_cols_rs);
		
		// get a list of all columns that are boolean...
		$bool_cols_qry = "SELECT column_name FROM information_schema.columns WHERE table_name IN ('vocab_master_loinc', 'vocab_master_organism', 'vocab_master_condition', 'vocab_child_loinc', 'vocab_child_organism') AND data_type = 'boolean';";
		$bool_cols_rs = pg_query($host_pa,$bool_cols_qry) or die("Can't Perform Query ".pg_last_error());
		while ($bool_cols_row = pg_fetch_object($bool_cols_rs)) {
			$bool_cols[] = $bool_cols_row->column_name;
		}
		pg_free_result($bool_cols_rs);
		
		if (isset($_GET["sort"]) && is_array($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"])) {
			if (is_array($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][strtolower(trim($_GET["sort"]))])) {
				$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"] = strtolower(trim($_GET["sort"]));
			}
		}
		
		
		$vocab_fromwhere = sprintf("%s", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["from_tables"]);
		
		$where_count = 0;
		// handle any search terms or filters...
		if (isset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_sql"])) {
			// we've got some query params
			$where_count = 1;
			$vocab_fromwhere .= " WHERE (";
			
			foreach ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] as $searchcol => $searchcoldata) {
				if ($searchcoldata['textsearch']) {
					if (!in_array($searchcol, $bool_cols)) {
						if ($where_count > 1) {
							$vocab_fromwhere .= " OR ";
						}
						$vocab_fromwhere .= sprintf("%s ILIKE '%%%s%%'", $searchcoldata['colname'], $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_sql"]);
						$where_count++;
					}
				}
			}
			
			$vocab_fromwhere .= ")";
		}
		
		if (isset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"])) {
			// need to apply filters
			$filter_count = 0;
			if ($where_count == 0) {
				// not already a WHERE clause for search terms
				$vocab_fromwhere .= " WHERE";
			}
			
			foreach ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"] as $sqlfiltercol => $sqlfiltervals) {
				unset($filter_temp);
				$nullfilter = FALSE;
				if (($filter_count > 0) || ($where_count > 1)) {
					$vocab_fromwhere .= " AND (";
				} else {
					$vocab_fromwhere .= " (";
				}
				foreach ($sqlfiltervals as $sqlfilterval) {
					if (is_null($sqlfilterval) || (strlen(trim($sqlfilterval)) == 0)) {
						$nullfilter = TRUE;
					} else {
						$filter_temp[] = "'" . pg_escape_string($sqlfilterval) . "'";
					}
				}
				
				if ($nullfilter && is_array($filter_temp)) {
					$vocab_fromwhere .= $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]["filtercolname"] . " IN (" . implode(",", $filter_temp) . ") OR " . $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]["filtercolname"] . " IS NULL OR " . $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]["filtercolname"] . " = ''";
				} elseif (is_array($filter_temp)) {
					$vocab_fromwhere .= $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]["filtercolname"] . " IN (" . implode(",", $filter_temp) . ")";
				} elseif ($nullfilter) {
					$vocab_fromwhere .= $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]["filtercolname"] . " IS NULL OR " . $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]["filtercolname"] . " = ''";
				}
				
				$vocab_fromwhere .= ")";
				$filter_count++;
			}
		}
		
		
		// finish up our 'ORDER BY' clause now that we have all of our 'WHERE' stuff figured out...
		// if child vocab, always sort by lab first, then by the specified sort column.
		if ( (intval($_SESSION["vocab_params"]["vocab"]) == 4) || (intval($_SESSION["vocab_params"]["vocab"]) == 5) || (intval($_SESSION["vocab_params"]["vocab"]) == 9) ) {
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["order_by"] = sprintf("ORDER BY lab ASC, %s %s", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"]]["colname"], $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_order"]);
		} else {
			$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["order_by"] = sprintf("ORDER BY %s %s", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"]]["colname"], $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_order"]);
		}
		
?>

<script>
	$(function() {
		$(".vocab_filter_selectall").click(function() {
			var thisFilter = $(this).attr("rel");
			$("div.addnew_lookup_checklist[rel='"+thisFilter+"']").find($(":input")).each(function() {
				if (!$(this).is(':checked')) {
					$(this).trigger('click');
				}
			});
		});
		
		$(".vocab_filter_selectnone").click(function() {
			var thisFilter = $(this).attr("rel");
			$("div.addnew_lookup_checklist[rel='"+thisFilter+"']").find($(":input")).each(function() {
				if ($(this).is(':checked')) {
					$(this).trigger('click');
				}
			});
		});
		
		$("#latestTasks tr").click(function() {
			$(this).toggleClass("focus_row");
		});
		
		$(".colheader_sort_down").button({
			icons: { secondary: "ui-icon-circle-arrow-s" }
		});
	
		$(".colheader_sort_up").button({
			icons: { secondary: "ui-icon-circle-arrow-n" }
		});
	
		$("#latestTasks tbody tr").addClass("all_row");
		
	<?php
		if (isset($_GET['edit_id'])){
	?>
		$("#vocab_row_<?php echo intval(trim($_GET['edit_id'])); ?>").addClass("focus_row");
		setTimeout(function() {
			//deprecated... var container = (($.browser.msie || $.browser.mozilla) ? $("body,html") : $("body"));
			var container = $("body,html");
			var scrollTo = $("#vocab_row_<?php echo intval(trim($_GET['edit_id'])); ?>");
			container.scrollTop(
				scrollTo.offset().top - container.offset().top + container.scrollTop()
			);
		}, 10);
	<?php
		}
	
		if (isset($_GET['focus_id'])){
	?>
		$("#vocab_row_<?php echo intval(trim($_GET['focus_id'])); ?>").addClass("focus_row");
		setTimeout(function() {
			//deprecated... var container = (($.browser.msie || $.browser.mozilla) ? $("body,html") : $("body"));
			var container = $("body,html");
			var scrollTo = $("#vocab_row_<?php echo intval(trim($_GET['focus_id'])); ?>");
			container.scrollTop(
				scrollTo.offset().top - container.offset().top + container.scrollTop()
			);
		}, 10);
	<?php
		}
	?>
		
		$("#toggle_filters").button({
            icons: {
                secondary: "ui-icon-triangle-1-n"
            }
        }).click(function() {
			$(".vocab_filter").toggle("blind");
			var objIcons = $(this).button("option", "icons");
			if (objIcons['secondary'] == "ui-icon-triangle-1-s") {
				$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-n"});
				$(this).button("option", "label", "Hide Filters");
				$("#addnew_form").hide();
				$("#addnew_button").show();
			} else {
				$(this).button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
				$(this).button("option", "label", "Show Filters");
			}
		});
		
		$(".vocab_filter").hide();
		$("#toggle_filters").button("option", "icons", {secondary: "ui-icon-triangle-1-s"});
		$("#toggle_filters").button("option", "label", "Show Filters");
		
		$("#clear_filters").button({
            icons: {
                primary: "ui-icon-elrcancel"
            }
        }).click(function() {
			$(".pseudo_select").removeAttr("checked");
			$(".pseudo_select_label").removeClass("pseudo_select_on");
			$("#search_form")[0].reset();
			$("#q").val("").blur();
			$("#search_form").submit();
		});
		
		$("#q_go").button({
			icons: {
                primary: "ui-icon-elrsearch"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
		$("#apply_filters").button({
			icons: {
                primary: "ui-icon-elroptions"
            }
		}).click(function(){
			$("#search_form").submit();
		});
		
		$("#q").addClass("search_empty").val("Enter search terms...").click(function() {
			var search_val = $.trim($("#q").val());
			if (search_val == "Enter search terms...") {
				$(this).removeClass("search_empty").val("");
			}
		}).blur(function() {
			var search_val_ln = $.trim($("#q").val()).length;
			if (search_val_ln == 0) {
				$("#q").addClass("search_empty").val("Enter search terms...");
			}
		});
		
		<?php
			if (isset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_display"])) {
				if ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_display"] != "Enter search terms...") {
		?>
		$("#q").removeClass("search_empty").val("<?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["q_display"]; ?>");
		<?php
				}
			}
		?>
		
		$("#latestTasks tbody tr").hover(function() {
			$(this).addClass("vocab_row_hover");
		}, function() {
			$(this).removeClass("vocab_row_hover");
		});
		
		$("#user_rows").change(function() {
			$("#user_rowselect").submit();
		});
		
		$(".pseudo_select").change(function() {
			$(this).closest('label').toggleClass('pseudo_select_on');
		});
		
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_category").focus();
			$(this).hide();
		});
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_savevocab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$("#new_vocab_form").submit(function(e) {
			var incomplete_fields = 0;
			$(":input.required", this).each(function() {
				if ($(this).val() == "" || $(this).val() == -1) {
					alert($('label[for="'+$(this).attr('id')+'"]').text()+' requires a value');
					incomplete_fields++;
				}
			});
			if (incomplete_fields > 0) {
				return false;
			} else {
				return true;
			}
		});
		
		$("#rule_mod_dialog").dialog({
			autoOpen: false,
			width: 600,
			modal: true
		});
		
		$("#rule_mod_cmr_dialog").dialog({
			autoOpen: false,
			width: 700,
			modal: true
		});
		
		$("#rulemod_add_condition").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function(e) {
			var numConditions = parseInt($("#rulemod_condition_counter").val());
			$("#rulemod_condition_counter").val((numConditions+1));
			$("#rulemod_condition_container").append("<div class=\"rulemod_condition\" id=\"rulemod_condition_"+(numConditions+1)+"\"></div>");
			$("#rulemod_condition_"+(numConditions+1)).html("<button type=\"button\" class=\"rulemod_delete_condition\" value=\""+(numConditions+1)+"\" title=\"Delete This Condition\">Delete Condition</button><label for=\"rulemod_operator_"+(numConditions+1)+"\">Result </label><select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_operator["+(numConditions+1)+"]\" id=\"rulemod_operator_"+(numConditions+1)+"\" title=\"Choose a comparison type\"><option value=\"0\" selected>--</option></select> <label for=\"rulemod_operand_value_"+(numConditions+1)+"\"> this value:</label> <input class=\"ui-corner-all\" type=\"text\" name=\"rulemod_operand_value["+(numConditions+1)+"]\" id=\"rulemod_operand_value_"+(numConditions+1)+"\" title=\"Compare message result with this value\" />");
		<?php
			// get list of logical operators for menu
			$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
			$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
			while ($operator_row = pg_fetch_object($operator_rs)) {
				?> $("#rulemod_operator_"+(numConditions+1)).append("<?php printf("<option value='%d'>%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical); ?>"); <?php
			}
			pg_free_result($operator_rs);
		?>
			$(".rulemod_delete_condition").button({
				icons: {
					primary: "ui-icon-elrclose"
				},
				text: false
			}).click(function(e) {
				var deleteCondition = $(this).val();
				$("#rulemod_condition_"+deleteCondition).remove();
			});
		});
		
		$("#rulemod_cmr_add_condition").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function(e) {
			var numConditions = parseInt($("#rulemod_cmr_condition_counter").val());
			$("#rulemod_cmr_condition_counter").val((numConditions+1));
			$("#rulemod_cmr_condition_container").append("<div class=\"rulemod_cmr_condition\" id=\"rulemod_cmr_condition_"+(numConditions+1)+"\"></div>");
			$("#rulemod_cmr_condition_"+(numConditions+1)).html("<button type=\"button\" class=\"rulemod_cmr_delete_condition\" value=\""+(numConditions+1)+"\" title=\"Delete This Condition\">Delete Condition</button><label for=\"rulemod_cmr_operator_"+(numConditions+1)+"\">Test Result </label><select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_cmr_operator["+(numConditions+1)+"]\" id=\"rulemod_cmr_operator_"+(numConditions+1)+"\" title=\"Choose a comparison type\"><option value=\"0\" selected>--</option></select> <select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_cmr_operand_value["+(numConditions+1)+"]\" id=\"rulemod_cmr_operand_value_"+(numConditions+1)+"\"><option value=\"0\" selected>--</option></select>");
		<?php
			// get list of logical operators for menu
			$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
			$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
			while ($operator_row = pg_fetch_object($operator_rs)) {
				?> $("#rulemod_cmr_operator_"+(numConditions+1)).append("<?php printf("<option value='%d'>%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical); ?>"); <?php
			}
			pg_free_result($operator_rs);
		?>
		<?php
			// get list of master test results for menu
			$masterresult_sql = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('test_result') ORDER BY concept;", $my_db_schema);
			$masterresult_rs = @pg_query($host_pa, $masterresult_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
			while ($masterresult_row = pg_fetch_object($masterresult_rs)) {
				?> $("#rulemod_cmr_operand_value_"+(numConditions+1)).append("<?php printf("<option value='%d'>%s</option>", intval($masterresult_row->id), htmlentities($masterresult_row->concept, ENT_QUOTES, "UTF-8")); ?>");<?php echo "\n"; ?> <?php
			}
			pg_free_result($masterresult_rs);
		?>
			$(".rulemod_cmr_delete_condition").button({
				icons: {
					primary: "ui-icon-elrclose"
				},
				text: false
			}).click(function(e) {
				var deleteCondition = $(this).val();
				$("#rulemod_cmr_condition_"+deleteCondition).remove();
			});
		});
		
		$(".add_result_rule").button({
            icons: {
                primary: "ui-icon-elrplus"
            },
			text: false
        }).click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id && jsonObj.action) {
				$("#rulemod_condition_container").empty();
				$("#rulemod_condition_counter").val(1);
				$("#rulemod_condition_container").append("<div class=\"rulemod_condition\" id=\"rulemod_condition_1\"></div>");
				$("#rulemod_condition_1").html("<button type=\"button\" class=\"rulemod_delete_condition\" value=\"1\" title=\"Delete This Condition\">Delete Condition</button><label for=\"rulemod_operator_1\">Result </label><select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_operator[1]\" id=\"rulemod_operator_1\" title=\"Choose a comparison type\"><option value=\"0\" selected>--</option></select> <label for=\"rulemod_operand_value_1\"> this value:</label> <input class=\"ui-corner-all\" type=\"text\" name=\"rulemod_operand_value[1]\" id=\"rulemod_operand_value_1\" title=\"Compare message result with this value\" />");
			<?php
				// get list of logical operators for menu
				$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
				$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
				while ($operator_row = pg_fetch_object($operator_rs)) {
					?> $("#rulemod_operator_1").append("<?php printf("<option value='%d'>%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical); ?>"); <?php
				}
				pg_free_result($operator_rs);
			?>
				$("#rulemod_operator_1").val(0);
				$("#rulemod_operand_value_1").val("");
				
				$(".rulemod_delete_condition").button({
					icons: {
						primary: "ui-icon-elrclose"
					},
					text: false
				}).click(function(e) {
					var deleteCondition = $(this).val();
					$("#rulemod_condition_"+deleteCondition).remove();
				});
				
				$("#rulemod_child_loinc").html("Add rule for "+jsonObj.lab_name+" LOINC '"+jsonObj.child_loinc+"':");
				$("#rulemod_id").val(jsonObj.id);
				$("#rulemod_focus_id").val(jsonObj.focus_id);
				$("#rulemod_action").val(jsonObj.action);
				$("#rulemod_application").val(1);
				$("#rulemod_master_result").val(0);
				$("#rulemod_comments").val("");
				
				$("#rule_mod_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#rule_mod_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#rule_mod_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$(".add_cmr_rule").button({
            icons: {
                primary: "ui-icon-elrplus"
            },
			text: false
        }).click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id && jsonObj.action) {
				$("#rulemod_cmr_condition_container").empty();
				$("#rulemod_cmr_condition_counter").val(1);
				$("#rulemod_cmr_condition_container").append("<div class=\"rulemod_cmr_condition\" id=\"rulemod_cmr_condition_1\"></div>");
				$("#rulemod_cmr_condition_1").html("<button type=\"button\" class=\"rulemod_cmr_delete_condition\" value=\"1\" title=\"Delete This Condition\">Delete Condition</button><label for=\"rulemod_cmr_operator_1\">Test Result </label><select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_cmr_operator[1]\" id=\"rulemod_cmr_operator_1\" title=\"Choose a comparison type\"><option value=\"0\" selected>--</option></select> <select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_cmr_operand_value[1]\" id=\"rulemod_cmr_operand_value_1\"><option value=\"0\" selected>--</option></select>");
			<?php
				// get list of logical operators for menu
				$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
				$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
				while ($operator_row = pg_fetch_object($operator_rs)) {
					?> $("#rulemod_cmr_operator_1").append("<?php printf("<option value='%d'>%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical); ?>");<?php echo "\n"; ?> <?php
				}
				pg_free_result($operator_rs);
			?>
			<?php
				// get list of master test results for menu
				$masterresult_sql = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('test_result') ORDER BY concept;", $my_db_schema);
				$masterresult_rs = @pg_query($host_pa, $masterresult_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
				while ($masterresult_row = pg_fetch_object($masterresult_rs)) {
					?> $("#rulemod_cmr_operand_value_1").append("<?php printf("<option value='%d'>%s</option>", intval($masterresult_row->id), htmlentities($masterresult_row->concept, ENT_QUOTES, "UTF-8")); ?>");<?php echo "\n"; ?> <?php
				}
				pg_free_result($masterresult_rs);
			?>
				$("#rulemod_cmr_operator_1").val(0);
				$("#rulemod_cmr_operand_value_1").val(0);
				
				$(".rulemod_cmr_delete_condition").button({
					icons: {
						primary: "ui-icon-elrclose"
					},
					text: false
				}).click(function(e) {
					var deleteCondition = $(this).val();
					$("#rulemod_cmr_condition_"+deleteCondition).remove();
				});
				
				$("#rulemod_cmr_loinc").html("Add rule for Master LOINC '"+jsonObj.loinc+"':");
				$("#rulemod_cmr_id").val(jsonObj.id);
				$("#rulemod_cmr_focus_id").val(jsonObj.focus_id);
				$("#rulemod_cmr_action").val(jsonObj.action);
				$("#rulemod_cmr_application").val(1);
				$("#rulemod_state_case_status").val(0);
				
				$("#rule_mod_cmr_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#rule_mod_cmr_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#rule_mod_cmr_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$(".edit_result_rule").button({
            icons: {
                primary: "ui-icon-elrpencil"
            },
			text: false
        }).click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			var numConditions = parseInt($("#rulemod_condition_counter").val());
			
			if (jsonObj.id && jsonObj.action) {
				$("#rulemod_condition_container").empty();
				$("#rulemod_condition_counter").val((numConditions+jsonObj.conditions.length));
				
				for (var i = 0; i < jsonObj.conditions.length; i++) {
					$("#rulemod_condition_container").append("<div class=\"rulemod_condition\" id=\"rulemod_condition_"+(i)+"\"></div>");
					$("#rulemod_condition_"+(i)).html("<button type=\"button\" class=\"rulemod_delete_condition\" value=\""+(i)+"\" title=\"Delete This Condition\">Delete Condition</button><label for=\"rulemod_operator_"+(i)+"\">Result </label><select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_operator["+(i)+"]\" id=\"rulemod_operator_"+(i)+"\" title=\"Choose a comparison type\"><option value=\"0\" selected>--</option></select> <label for=\"rulemod_operand_value_"+(i)+"\"> this value:</label> <input class=\"ui-corner-all\" type=\"text\" name=\"rulemod_operand_value["+(i)+"]\" id=\"rulemod_operand_value_"+(i)+"\" title=\"Compare message result with this value\" />");
				<?php
					// get list of logical operators for menu
					$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
					$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
					while ($operator_row = pg_fetch_object($operator_rs)) {
						?> $("#rulemod_operator_"+(i)).append("<?php printf("<option value='%d'>%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical); ?>"); <?php
					}
					pg_free_result($operator_rs);
				?>
					$("#rulemod_operator_"+(i)).val(jsonObj.conditions[i].operator);
					$("#rulemod_operand_value_"+(i)).val(jsonObj.conditions[i].operand);
				}
				
				$(".rulemod_delete_condition").button({
					icons: {
						primary: "ui-icon-elrclose"
					},
					text: false
				}).click(function(e) {
					var deleteCondition = $(this).val();
					$("#rulemod_condition_"+deleteCondition).remove();
				});
				
				$("#rulemod_child_loinc").html("Edit rule for "+jsonObj.lab_name+" LOINC '"+jsonObj.child_loinc+"':");
				$("#rulemod_id").val(jsonObj.id);
				$("#rulemod_focus_id").val(jsonObj.focus_id);
				$("#rulemod_action").val(jsonObj.action);
				$("#rulemod_application").val(jsonObj.application);
				$("#rulemod_master_result").val(jsonObj.master_result);
				$("#rulemod_comments").val(jsonObj.comments);
				
				$("#rule_mod_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#rule_mod_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#rule_mod_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$(".edit_cmr_rule").button({
            icons: {
                primary: "ui-icon-elrpencil"
            },
			text: false
        }).click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			var numConditions = parseInt($("#rulemod_cmr_condition_counter").val());
			
			if (jsonObj.id && jsonObj.action) {
				$("#rulemod_cmr_condition_container").empty();
				$("#rulemod_cmr_condition_counter").val((numConditions+jsonObj.conditions.length));
				
				for (var i = 0; i < jsonObj.conditions.length; i++) {
					$("#rulemod_cmr_condition_container").append("<div class=\"rulemod_cmr_condition\" id=\"rulemod_cmr_condition_"+(i)+"\"></div>");
					$("#rulemod_cmr_condition_"+(i)).html("<button type=\"button\" class=\"rulemod_cmr_delete_condition\" value=\""+(i)+"\" title=\"Delete This Condition\">Delete Condition</button><label for=\"rulemod_cmr_operator_"+(i)+"\">Test Result </label><select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_cmr_operator["+(i)+"]\" id=\"rulemod_cmr_operator_"+(i)+"\" title=\"Choose a comparison type\"><option value=\"0\" selected>--</option></select> <select class=\"ui-corner-all\" style=\"margin: 0px;\" name=\"rulemod_cmr_operand_value["+(i)+"]\" id=\"rulemod_cmr_operand_value_"+(i)+"\"><option value=\"0\" selected>--</option></select>");
				<?php
					// get list of logical operators for menu
					$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
					$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
					while ($operator_row = pg_fetch_object($operator_rs)) {
						?> $("#rulemod_cmr_operator_"+(i)).append("<?php printf("<option value='%d'>%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical); ?>"); <?php
					}
					pg_free_result($operator_rs);
				?>
				<?php
					// get list of master test results for menu
					$masterresult_sql = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('test_result') ORDER BY concept;", $my_db_schema);
					$masterresult_rs = @pg_query($host_pa, $masterresult_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
					while ($masterresult_row = pg_fetch_object($masterresult_rs)) {
						?> $("#rulemod_cmr_operand_value_"+(i)).append("<?php printf("<option value='%d'>%s</option>", intval($masterresult_row->id), htmlentities($masterresult_row->concept, ENT_QUOTES, "UTF-8")); ?>");<?php echo "\n"; ?> <?php
					}
					pg_free_result($masterresult_rs);
				?>
					$("#rulemod_cmr_operator_"+(i)).val(jsonObj.conditions[i].operator);
					$("#rulemod_cmr_operand_value_"+(i)).val(jsonObj.conditions[i].operand);
				}
				
				$(".rulemod_cmr_delete_condition").button({
					icons: {
						primary: "ui-icon-elrclose"
					},
					text: false
				}).click(function(e) {
					var deleteCondition = $(this).val();
					$("#rulemod_cmr_condition_"+deleteCondition).remove();
				});
				
				$("#rulemod_cmr_loinc").html("Edit rule for Master LOINC '"+jsonObj.loinc+"':");
				$("#rulemod_cmr_id").val(jsonObj.id);
				$("#rulemod_cmr_focus_id").val(jsonObj.focus_id);
				$("#rulemod_cmr_action").val(jsonObj.action);
				$("#rulemod_cmr_application").val(jsonObj.application);
				$("#rulemod_state_case_status").val(jsonObj.master_result);
				$("#rulemod_new_cmr").val(jsonObj.allow_new_cmr);
				$("#rulemod_is_surveillance").val(jsonObj.is_surveillance);
				
				$("#rule_mod_cmr_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#rule_mod_cmr_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#rule_mod_cmr_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$(".delete_result_rule").button({
            icons: {
                primary: "ui-icon-elrclose"
            },
			text: false
        }).click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id) {
				var deleteRuleAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo $vocab; ?>&rulemod_action=delete&rulemod_id="+jsonObj.id+"&focus_id="+jsonObj.focus_id;

				$("#confirm_deleterule_dialog").dialog('option', 'buttons', {
						"Delete" : function() {
							window.location.href = deleteRuleAction;
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#confirm_deleterule_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$(".delete_cmr_rule").button({
            icons: {
                primary: "ui-icon-elrclose"
            },
			text: false
        }).click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id) {
				var deleteRuleAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo $vocab; ?>&rulemod_cmr_action=delete&rulemod_cmr_id="+jsonObj.id+"&focus_id="+jsonObj.focus_id;

				$("#confirm_deleterule_cmr_dialog").dialog('option', 'buttons', {
						"Delete" : function() {
							window.location.href = deleteRuleAction;
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#confirm_deleterule_cmr_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$(".edit_vocab").button({
				icons: { primary: "ui-icon-elrpencil" }, 
				text: false
			}).next().button({
				icons: { primary: "ui-icon-elrclose" }, 
				text: false
			}).parent().buttonset();

		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false,
			width: 400
		});
		
		$("#confirm_deleterule_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false,
			width: 400
		});
		
		$("#confirm_deleterule_cmr_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false,
			width: 400
		});
		
		$(".delete_vocab").click(function(e) {
			e.preventDefault();
			
			var jsonObj = jQuery.parseJSON($(this).val());
			if (jsonObj.current_id) {
				var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo $vocab; ?>&delete_id="+jsonObj.current_id+"&focus_id="+jsonObj.previous_id;

				$("#confirm_delete_dialog").dialog('option', 'buttons', {
						"Delete" : function() {
							window.location.href = deleteAction;
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#confirm_delete_dialog").dialog("open");
			}

		});
		
		$(".edit_vocab").click(function(e) {
			e.preventDefault();
			var editAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo $vocab; ?>&edit_id="+$(this).val();
			window.location.href = editAction;
		});
		
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrglossary"></span><?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?> Manager</h1>

<?php
		if (isset($_GET['delete_id'])) {
			/*
			 * delete existing record
			 */
			if (intval(trim($_GET['delete_id'])) > 0) {
				// make sure passed ID is a valid integer
				unset($delete_sql);
				unset($dependency_sql);
				$delete_id = intval(trim($_GET['delete_id']));
				
				switch (intval($_SESSION["vocab_params"]["vocab"])) {
					case 5:
						// Child LOINC
						$va_prev_vals = $va->getPreviousVals($delete_id, VocabAudit::TABLE_CHILD_LOINC);
						$va_table = VocabAudit::TABLE_CHILD_LOINC;
						$valid_sql = sprintf("SELECT count(id) AS counter FROM %svocab_child_loinc WHERE id = %d", $my_db_schema, $delete_id);
						$delete_sql = sprintf("BEGIN;\nDELETE FROM ONLY %svocab_c2m_testresult WHERE child_loinc_id = %d;\nDELETE FROM ONLY %svocab_child_loinc WHERE id = %d;\nCOMMIT;", $my_db_schema, $delete_id, $my_db_schema, $delete_id);
						// no dependencies
						break;
					case 4:
						// Child Organism
						$va_prev_vals = $va->getPreviousVals($delete_id, VocabAudit::TABLE_CHILD_SNOMED);
						$va_table = VocabAudit::TABLE_CHILD_SNOMED;
						$valid_sql = sprintf("SELECT count(id) AS counter FROM %svocab_child_organism WHERE id = %d", $my_db_schema, $delete_id);
						$delete_sql = sprintf("DELETE FROM ONLY %svocab_child_organism WHERE id = %d", $my_db_schema, $delete_id);
						// no dependencies
						break;
					case 3:
						// Master Organism
						$va_prev_vals = $va->getPreviousVals($delete_id, VocabAudit::TABLE_MASTER_SNOMED);
						$va_table = VocabAudit::TABLE_MASTER_SNOMED;
						$valid_sql = sprintf("SELECT count(o_id) AS counter FROM %svocab_master_organism WHERE o_id = %d", $my_db_schema, $delete_id);
						$delete_sql = sprintf("DELETE FROM ONLY %svocab_master_organism WHERE o_id = %d", $my_db_schema, $delete_id);
						$dependency_sql = sprintf("SELECT ((SELECT count(l_id) FROM %svocab_master_loinc WHERE trisano_organism = %d) + (SELECT count(id) FROM %svocab_child_organism WHERE organism = %d)) AS dependents;",
							$my_db_schema, $delete_id, 
							$my_db_schema, $delete_id);
						break;
					case 2:
						// Master Condition
						$va_prev_vals = $va->getPreviousVals($delete_id, VocabAudit::TABLE_MASTER_CONDITION);
						$va_table = VocabAudit::TABLE_MASTER_CONDITION;
						$valid_sql = sprintf("SELECT count(c_id) AS counter FROM %svocab_master_condition WHERE c_id = %d", $my_db_schema, $delete_id);
						$delete_sql = sprintf("DELETE FROM ONLY %svocab_master_condition WHERE c_id = %d", $my_db_schema, $delete_id);
						$dependency_sql = sprintf("SELECT ((SELECT count(l_id) FROM %svocab_master_loinc WHERE trisano_condition = %d) + (SELECT count(o_id) FROM %svocab_master_organism WHERE condition = %d)) AS dependents;",
							$my_db_schema, $delete_id, 
							$my_db_schema, $delete_id);
						break;
					default:
						// Master LOINC
						$va_prev_vals = $va->getPreviousVals($delete_id, VocabAudit::TABLE_MASTER_LOINC);
						$va_table = VocabAudit::TABLE_MASTER_LOINC;
						$valid_sql = sprintf("SELECT count(l_id) AS counter FROM %svocab_master_loinc WHERE l_id = %d", $my_db_schema, $delete_id);
						$delete_sql = sprintf("DELETE FROM ONLY %svocab_master_loinc WHERE l_id = %d", $my_db_schema, $delete_id);
						$dependency_sql = sprintf("SELECT count(id) AS dependents FROM %svocab_child_loinc WHERE master_loinc = %d;",
							$my_db_schema, $delete_id);
						break;
				}
				
				// check to make sure that the record ID passed actually exists in the correct table...
				$valid_rs = @pg_query($host_pa, $valid_sql);
				$valid_count = @pg_fetch_result($valid_rs, 0, "counter");
				if ($valid_count > 0) {
					// check for dependents
					$dependency_rs = @pg_query($host_pa, $dependency_sql);
					$dependency_count = @pg_fetch_result($dependency_rs, 0, "dependents");
					if ($dependency_count > 0) {
						suicide(sprintf("Cannot delete vocabulary:  There are %s dependents of this %s record.  Please resolve these dependencies and try again.", $dependency_count, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]));
					} else {
						// commit the delete
						if (@pg_query($host_pa, $delete_sql)) {
							highlight(sprintf("%s record was successfully deleted!", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]), "ui-icon-check");
							
							$va->resetAudit();
							$va->setOldVals($va_prev_vals);
							$va->auditVocab($delete_id, $va_table, VocabAudit::ACTION_DELETE);
						} else {
							suicide("Could not delete this record.", 1);
						}
					}
				} else {
					suicide("Cannot delete vocabulary:  Record not found");
				}
			} else {
				suicide("Cannot delete vocabulary:  Record not found");
			}
		
		} elseif (isset($_POST['add_flag'])) {
			/*
			 * add a new record
			 */
			unset($new_fields);
			unset($new_table);
			
			switch (intval($_SESSION["vocab_params"]["vocab"])) {
				case 5:
					// Child LOINC
					$va_table = VocabAudit::TABLE_CHILD_LOINC;
					$va_id_col = 'id';
					$new_table = sprintf("%svocab_child_loinc", $my_db_schema);
					$new_fields = array(
							array("get_field" => "new_lab_id", "colname" => "lab_id", "validtype" => "list-required"),
							array("get_field" => "new_archived", "colname" => "archived", "validtype" => "bool"),
							array("get_field" => "new_child_loinc", "colname" => "child_loinc", "validtype" => "text-required"),
							array("get_field" => "new_master_loinc", "colname" => "master_loinc", "validtype" => "list-required"),
							array("get_field" => "new_child_orderable_test_code", "colname" => "child_orderable_test_code", "validtype" => "text"),
							array("get_field" => "new_child_resultable_test_code", "colname" => "child_resultable_test_code", "validtype" => "text"),
							array("get_field" => "new_child_concept_name", "colname" => "child_concept_name", "validtype" => "text"),
							array("get_field" => "new_child_alias", "colname" => "child_alias", "validtype" => "text"),
							array("get_field" => "new_interpret_results", "colname" => "interpret_results", "validtype" => "bool"),
							array("get_field" => "new_semi_auto", "colname" => "semi_auto", "validtype" => "bool"),
							array("get_field" => "new_validate_child_snomed", "colname" => "validate_child_snomed", "validtype" => "bool"),
							array("get_field" => "new_location", "colname" => "result_location", "validtype" => "list"),
							array("get_field" => "new_units", "colname" => "units", "validtype" => "text"),
							array("get_field" => "new_pregnancy", "colname" => "pregnancy", "validtype" => "bool"),
							array("get_field" => "new_refrange", "colname" => "refrange", "validtype" => "text"), 
							array("get_field" => "new_hl7_refrange", "colname" => "hl7_refrange", "validtype" => "text")
						);
					break;
				case 4:
					// Child Organism
					$va_table = VocabAudit::TABLE_CHILD_SNOMED;
					$va_id_col = 'id';
					$new_table = sprintf("%svocab_child_organism", $my_db_schema);
					$new_fields = array(
							array("get_field" => "new_lab_id", "colname" => "lab_id", "validtype" => "list-required"),
							array("get_field" => "new_child_code", "colname" => "child_code", "validtype" => "text-required"),
							array("get_field" => "new_organism", "colname" => "organism", "validtype" => "list"),
							array("get_field" => "new_test_result_id", "colname" => "test_result_id", "validtype" => "list"),
							array("get_field" => "new_result_value", "colname" => "result_value", "validtype" => "text"),
							array("get_field" => "new_comment", "colname" => "comment", "validtype" => "text")
						);
					break;
				case 3:
					// Master Organism
					$va_table = VocabAudit::TABLE_MASTER_SNOMED;
					$va_id_col = 'o_id';
					$new_table = sprintf("%svocab_master_organism", $my_db_schema);
					$new_fields = array(
							array("get_field" => "new_snomed_category", "colname" => "snomed_category", "validtype" => "list-required"),
							array("get_field" => "new_condition", "colname" => "condition", "validtype" => "list"),
							array("get_field" => "new_snomed", "colname" => "snomed", "validtype" => "text"),
							array("get_field" => "new_snomed_alt", "colname" => "snomed_alt", "validtype" => "text"),
							array("get_field" => "new_organism", "colname" => "organism", "validtype" => "list"),
							array("get_field" => "new_list", "colname" => "list", "validtype" => "list"),
							array("get_field" => "new_test_result", "colname" => "test_result", "validtype" => "list"), 
							array("get_field" => "new_status", "colname" => "status", "validtype" => "list"), 
							array("get_field" => "new_nom_is_surveillance", "colname" => "nom_is_surveillance", "validtype" => "bool")
						);
					break;
				case 2:
					// Master Condition
					$va_table = VocabAudit::TABLE_MASTER_CONDITION;
					$va_id_col = 'c_id';
					$new_table = sprintf("%svocab_master_condition", $my_db_schema);
					$new_fields = array(
							array("get_field" => "new_condition", "colname" => "condition", "validtype" => "list-required"),
							array("get_field" => "new_disease_category", "colname" => "disease_category", "validtype" => "list-required"),
							array("get_field" => "new_gateways", "colname" => "gateway_xref", "validtype" => "multi"),
							array("get_field" => "new_check_xref_first", "colname" => "check_xref_first", "validtype" => "bool"),
							array("get_field" => "new_valid_specimen", "colname" => "valid_specimen", "validtype" => "multi"),
							array("get_field" => "new_invalid_specimen", "colname" => "invalid_specimen", "validtype" => "multi"),
							array("get_field" => "new_white_rule", "colname" => "white_rule", "validtype" => "text"),
							array("get_field" => "new_contact_white_rule", "colname" => "contact_white_rule", "validtype" => "text"),
							array("get_field" => "new_immediate_notify", "colname" => "immediate_notify", "validtype" => "bool"),
							array("get_field" => "new_require_specimen", "colname" => "require_specimen", "validtype" => "bool"),
							array("get_field" => "new_new_event", "colname" => "new_event", "validtype" => "bool"),
							array("get_field" => "new_notify_state", "colname" => "notify_state", "validtype" => "bool"),
							array("get_field" => "new_autoapproval", "colname" => "autoapproval", "validtype" => "bool"), 
							array("get_field" => "new_district_override", "colname" => "district_override", "validtype" => "list")
						);
					break;
				default:
					// Master LOINC
					$va_table = VocabAudit::TABLE_MASTER_LOINC;
					$va_id_col = 'l_id';
					$new_table = sprintf("%svocab_master_loinc", $my_db_schema);
					$new_fields = array(
							array("get_field" => "new_loinc", "colname" => "loinc", "validtype" => "text-required"),
							array("get_field" => "new_concept_name", "colname" => "concept_name", "validtype" => "text"),
							array("get_field" => "new_condition_from_result", "colname" => "condition_from_result", "validtype" => "bool"),
							array("get_field" => "new_trisano_condition", "colname" => "trisano_condition", "validtype" => "list"),
							array("get_field" => "new_organism_from_result", "colname" => "organism_from_result", "validtype" => "bool"),
							array("get_field" => "new_trisano_organism", "colname" => "trisano_organism", "validtype" => "list"),
							array("get_field" => "new_trisano_test_type", "colname" => "trisano_test_type", "validtype" => "list-required"),
							array("get_field" => "new_specimen_source", "colname" => "specimen_source", "validtype" => "list"),
							array("get_field" => "new_list", "colname" => "list", "validtype" => "list"),
							array("get_field" => "new_gray_rule", "colname" => "gray_rule", "validtype" => "text")
						);
					break;
				
			}
			
			// check for pre-existing records in cases where duplicate entries are not allowed
			$duplicate_exists = false;
			if (intval($_SESSION["vocab_params"]["vocab"]) == 4) {
				// child organism, check child snomed code
				$dup_co_sql = 'SELECT id 
					FROM '.$my_db_schema.'vocab_child_organism 
					WHERE lab_id = '.intval($_POST['new_lab_id']).' 
					AND child_code ILIKE \''.@pg_escape_string(trim($_POST['new_child_code'])).'\';';
				$dup_co_rs = @pg_query($host_pa, $dup_co_sql);
				if (($dup_co_rs !== false) && (@pg_num_rows($dup_co_rs) > 0)) {
					$duplicate_exists = true;
					suicide('Cannot add new Child SNOMED code:  Code already exists for this lab!');
				}
				@pg_free_result($dup_co_rs);
			}
			
			if (!$duplicate_exists) {
				if (is_array($new_fields)) {
					$valid_add = TRUE;
					// verify all fields passed are valid types
					foreach ($new_fields as $new_field) {
						switch ($new_field['validtype']) {
							case "list-required":
								if (isset($_POST[$new_field['get_field']]) && is_numeric(trim($_POST[$new_field['get_field']])) && (intval(trim($_POST[$new_field['get_field']])) > 0)):
									$valid_add = $valid_add && TRUE;
								else:
									$valid_add = $valid_add && FALSE;
								endif;
								break;
							case "list":
								if (isset($_POST[$new_field['get_field']]) && is_numeric(trim($_POST[$new_field['get_field']]))):
									$valid_add = $valid_add && TRUE;
								else:
									$valid_add = $valid_add && FALSE;
								endif;
								break;
							case "text":
								if (isset($_POST[$new_field['get_field']])):
									$valid_add = $valid_add && TRUE;
								else:
									$valid_add = $valid_add && FALSE;
								endif;
								break;
							case "text-required":
								if (isset($_POST[$new_field['get_field']]) && ctype_print(trim($_POST[$new_field['get_field']])) && (strlen(trim($_POST[$new_field['get_field']])) > 0)):
									$valid_add = $valid_add && TRUE;
								else:
									$valid_add = $valid_add && FALSE;
								endif;
								break;
							case "multi":
								$valid_add = $valid_add && TRUE;
								break;
							case "bool":
								if (isset($_POST[$new_field['get_field']]) && ctype_lower(trim($_POST[$new_field['get_field']])) && ((trim($_POST[$new_field['get_field']]) == "t") || (trim($_POST[$new_field['get_field']]) == "f"))):
									$valid_add = $valid_add && TRUE;
								else:
									$valid_add = $valid_add && FALSE;
								endif;
								break;
						}
					}
					if ($valid_add) {
						$add_sql = 'INSERT INTO '.$new_table.' (';
						foreach ($new_fields as $new_field) {
							$add_sql .= $new_field['colname'] . ', ';
						}
						$add_sql = substr($add_sql, 0, -2);
						$add_sql .= ') VALUES (';
						foreach ($new_fields as $new_fieldval) {
							switch ($new_fieldval['validtype']) {
								case "list":
								case "list-required":
									$add_sql .= intval($_POST[$new_fieldval['get_field']]) . ', ';
									break;
								case "text":
								case "text-required":
									$add_sql .= ((strlen(trim($_POST[$new_fieldval['get_field']])) > 0) ? "'".pg_escape_string(trim($_POST[$new_fieldval['get_field']]))."'" : "NULL") . ', ';
									break;
								case "bool":
									$add_sql .= ((trim($_POST[$new_fieldval['get_field']]) == "t") ? "'t'" : "'f'") . ', ';
									break;
								case "multi":
									if (is_array($_POST[$new_fieldval['get_field']]) && (count($_POST[$new_fieldval['get_field']]) > 0)) {
										$multi_string = implode(';', $_POST[$new_fieldval['get_field']]);
									}
									$add_sql .= ((strlen(trim($multi_string)) > 0) ? "'".pg_escape_string(trim($multi_string))."'" : "NULL") . ', ';
									break;
							}
						}
						$add_sql = substr($add_sql, 0, -2);
						$add_sql .= ') RETURNING '.$va_id_col.';';
						
						$add_rs = @pg_query($host_pa, $add_sql);
						
						if ($add_rs !== false) {
							highlight(sprintf("%s added successfully!", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]), "ui-icon-check");
							
							$add_id = intval(@pg_fetch_result($add_rs, 0, 0));
							
							$va_new_vals = array();
							foreach ($new_fields as $va_new_field) {
								$va_new_vals[$va_new_field['colname']] = $_POST[$va_new_field['get_field']];
							}
							
							$va_prepared_new_vals = $va->prepareNewValues($va_table, $va_new_vals);
							
							$va->resetAudit();
							$va->setNewVals($va_prepared_new_vals);
							$va->auditVocab($add_id, $va_table, VocabAudit::ACTION_ADD);
							
							// for successful child SNOMED add, also add the new child SNOMED to the QA & Admin roles by default
							if (intval($_SESSION["vocab_params"]["vocab"]) == 4) {
								$qa_new_co_sql = 'BEGIN TRANSACTION;'.PHP_EOL;
								$qa_new_co_sql .= 'INSERT INTO '.$my_db_schema.'system_role_child_snomeds_by_lab
									(system_role_id, lab_id, child_snomed) 
									VALUES 
									('.$props['elr_qa_role'].', '.intval(trim($_POST['new_lab_id'])).', \''.@pg_escape_string(trim($_POST['new_child_code'])).'\');
								'.PHP_EOL;
								$qa_new_co_sql .= 'INSERT INTO '.$my_db_schema.'system_role_child_snomeds_by_lab
									(system_role_id, lab_id, child_snomed) 
									VALUES 
									('.ELR_ADMIN_ROLE.', '.intval(trim($_POST['new_lab_id'])).', \''.@pg_escape_string(trim($_POST['new_child_code'])).'\');
								'.PHP_EOL;
								$qa_new_co_sql .= 'COMMIT;'.PHP_EOL;
								$qa_new_co_rs = @pg_query($host_pa, $qa_new_co_sql);
								if ($qa_new_co_rs === false) {
									suicide('Failed to add new Child SNOMED code to default QA & Admin roles.', 1);
								}
								@pg_free_result($qa_new_co_rs);
							}
							
							// for successful child LOINC add, check if QA role has been assigned the corresponding master LOINC yet; add if not
							if (intval($_SESSION["vocab_params"]["vocab"]) == 5) {
								$qa_ml_sql = 'SELECT loinc 
									FROM '.$my_db_schema.'vocab_master_loinc 
									WHERE l_id = '.intval(trim($_POST['new_master_loinc'])).';
								';
								$qa_add_master_loinc = trim(@pg_fetch_result(@pg_query($host_pa, $qa_ml_sql), 0, 'loinc'));
								
								if (!empty($qa_add_master_loinc)) {
									$qa_new_cl_checkqa_sql = 'SELECT id 
										FROM '.$my_db_schema.'system_role_loincs_by_lab 
										WHERE (system_role_id = '.$props['elr_qa_role'].') 
										AND (lab_id = '.intval(trim($_POST['new_lab_id'])).') 
										AND (master_loinc_code = \''.@pg_escape_string($qa_add_master_loinc).'\');
									';
									$qa_new_cl_checkqa_rs = @pg_query($host_pa, $qa_new_cl_checkqa_sql);
									if (($qa_new_cl_checkqa_rs === false) || (@pg_num_rows($qa_new_cl_checkqa_rs) == 0)) {
										// not yet assigned; add to QA role
										$qa_new_clqa_sql = 'INSERT INTO '.$my_db_schema.'system_role_loincs_by_lab 
											(system_role_id, lab_id, master_loinc_code) 
											VALUES 
											('.$props['elr_qa_role'].', '.intval(trim($_POST['new_lab_id'])).', \''.@pg_escape_string($qa_add_master_loinc).'\');
										';
										$qa_new_clqa_rs = @pg_query($host_pa, $qa_new_clqa_sql);
										if ($qa_new_clqa_rs === false) {
											suicide('Failed to add Master LOINC associated with new Child LOINC to default QA role.', 1);
										}
										@pg_free_result($qa_new_clqa_rs);
									}
									
									$qa_new_cl_checkadm_sql = 'SELECT id 
										FROM '.$my_db_schema.'system_role_loincs_by_lab 
										WHERE (system_role_id = '.ELR_ADMIN_ROLE.') 
										AND (lab_id = '.intval(trim($_POST['new_lab_id'])).') 
										AND (master_loinc_code = \''.@pg_escape_string($qa_add_master_loinc).'\');
									';
									$qa_new_cl_checkadm_rs = @pg_query($host_pa, $qa_new_cl_checkadm_sql);
									if (($qa_new_cl_checkadm_rs === false) || (@pg_num_rows($qa_new_cl_checkadm_rs) == 0)) {
										// not yet assigned; add to QA role
										$qa_new_cladm_sql = 'INSERT INTO '.$my_db_schema.'system_role_loincs_by_lab 
											(system_role_id, lab_id, master_loinc_code) 
											VALUES 
											('.ELR_ADMIN_ROLE.', '.intval(trim($_POST['new_lab_id'])).', \''.@pg_escape_string($qa_add_master_loinc).'\');
										';
										$qa_new_cladm_rs = @pg_query($host_pa, $qa_new_cladm_sql);
										if ($qa_new_cladm_rs === false) {
											suicide('Failed to add Master LOINC associated with new Child LOINC to default Admin role.', 1);
										}
										@pg_free_result($qa_new_cladm_rs);
									}
									
								} else {
									suicide('Failed to add Master LOINC associated with new Child LOINC to default QA role:  Unable to find Master LOINC code.');
								}
							}
						} else {
							suicide(sprintf("Could not add new %s.", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]), 1);
						}
					} else {
						suicide(sprintf("Could not add new %s:  Some values were missing/invalid", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]));
					}
				}
			}
				
		}
		
		
		#################################################
		############### Result Pagination ###############
		#################################################
		// find out how many results there will be...
		$rowcount_sql = sprintf("SELECT count(%s) AS counter %s;", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"], $vocab_fromwhere);
		#debug echo $rowcount_sql."<br><br>";
		$counter=pg_fetch_array(pg_query($host_pa,$rowcount_sql)) or die("Can't Perform Query ".pg_last_error());
		$numrows=$counter['counter'];
		
		// number of rows to show per page
		$valid_rowsize = array(50, 100, 250, 500, 1000, -1);
		if(isset($_GET['user_rows'])){
			if (in_array(intval(trim($_GET['user_rows'])) , $valid_rowsize)) {
				$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"] = intval(trim($_GET['user_rows']));
			}
		}
		
		// find out total pages
		if ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"] > 0) {
			$totalpages = ceil($numrows / $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"]);
			
			// get the current page or set a default
			if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
				$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["current_page"] = intval($_GET['currentpage']);
			}
			
			if (isset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["current_page"])) {
				$currentpage = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["current_page"];
			} else {
				$currentpage = 1;
			}
			
			// if current page is greater than total pages...
			if ($currentpage > $totalpages) {
				// set current page to last page
				$currentpage = $totalpages;
			}
			
			// if current page is less than first page...
			if ($currentpage < 1) {
				// set current page to first page
				$currentpage = 1;
			}
			
			// the offset of the list, based on current page
			$offset = ($currentpage - 1) * $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"];
		} else {
			$totalpages = 1;
			$currentpage = 1;
		}
		
?>

<form name="search_form" id="search_form" method="GET" action="<?php echo $main_url; ?>">

<div class="vocab_search ui-tabs ui-widget">
	<label for="q" class="vocab_search_form">Search:</label><br><input type="text" name="q" id="q" class="vocab_query ui-corner-all">
	<button name="q_go" id="q_go" title="Search">Search</button>
	<button type="button" name="clear_filters" id="clear_filters" title="Clear all filters/search terms">Reset</button>
	<button type="button" name="toggle_filters" id="toggle_filters" title="Show/hide filters">Hide Filters</button>
	<button id="addnew_button" type="button" title="Add a new '<?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?>' record">Add new <?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?></button>
	
</div>

<?php
	############### If filters applied, display which ones ###############
	if (isset($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"])) {
?>
<div class="vocab_search ui-widget ui-widget-content ui-state-highlight ui-corner-all" style="padding: 5px;">
	<span class="ui-icon ui-icon-elroptions" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;">Active Filters:   
<?php
		$active_filters = 0;
		foreach ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["filters"] as $sqlfiltercol => $sqlfiltervals) {
			if ($active_filters == 0) {
				echo "<strong>" . $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]['label'] . "</strong>";
			} else {
				echo ", <strong>" . $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"][$sqlfiltercol]['label'] . "</strong>";			}
			$active_filters++;
		}
?>
	</p>
</div>
<?php
	}
?>

<div class="vocab_filter ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Filters:</label></div>
<?php
	############### Draw filter form elements based on 'result_cols' array ###############
	foreach ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] as $filtercol => $filtercolname) {
		if ($filtercolname['filter']) {
			echo "<div class=\"vocab_filter_container\"><label class=\"vocab_search_form2\">".$filtercolname['label']."</label><br>";
			echo "<div class=\"vocab_filter_checklist\">";
			
			if ($filtercolname['filterlookup']) {
				$filter_qry =  $filtercolname['lookupqry'];
			} else {
				$filter_qry = sprintf("SELECT DISTINCT %s FROM %s%s ORDER BY %s ASC;", $filtercol, $my_db_schema, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["table_name"], $filtercol);
			}
			#debug echo $filter_qry;
			$filter_rs = @pg_query($host_pa, $filter_qry);
			$value_iterator = 0;
			while ($filter_row = @pg_fetch_object($filter_rs)) {
				$value_iterator++;
				$this_filter_selected = FALSE;
				if (is_array($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]['filters'][$filtercol])) {
					if ($filtercolname['filterlookup']) {
						if (in_array($filter_row->value, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]['filters'][$filtercol])) {
							$this_filter_selected = TRUE;
						}
					} else {
						if (in_array($filter_row->$filtercol, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]['filters'][$filtercol])) {
							$this_filter_selected = TRUE;
						}
					}
				}
				
				if (in_array($filtercol, $bool_cols)) {
					if ($filtercol == "new_event") {
						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"".htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")."\">&nbsp;" . (($filter_row->$filtercol == "t") ? "New" : "Same") . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"".htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")."\">&nbsp;" . (($filter_row->$filtercol == "t") ? "New" : "Same") . "</label><br>";
						}
					} else {
						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"".htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")."\">&nbsp;" . (($filter_row->$filtercol == "t") ? "Yes" : "No") . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"".htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")."\">&nbsp;" . (($filter_row->$filtercol == "t") ? "Yes" : "No") . "</label><br>";
						}
					}
				} else {
					if ($filtercolname['filterlookup']) {
						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"".htmlentities($filter_row->value, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"".htmlentities($filter_row->value, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->label, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->label, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
					} else {
						if ($this_filter_selected) {
							echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"".htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")) . "</label><br>";
						} else {
							echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"".htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($filter_row->$filtercol, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
					}
				}
			}
			
			// add a 'Blank' option for lookup-generated filter lists
			if ($filtercolname['filterlookup']) {
				$value_iterator++;
				if (is_array($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]['filters'][$filtercol]) && in_array(-1, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]['filters'][$filtercol])) {
					echo "<label class=\"pseudo_select_label pseudo_select_on\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" checked value=\"-1\">&nbsp;&lt;Blank&gt;</label><br>";
				} else {
					echo "<label class=\"pseudo_select_label\" for=\"f_".$filtercol."_".$value_iterator."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"f[".$filtercol."][]\" id=\"f_".$filtercol."_".$value_iterator."\" value=\"-1\">&nbsp;&lt;Blank&gt;</label><br>";
				}
			}
			
			@pg_free_result($filter_rs);
			echo "</div></div>";
		}
	}
?>
	<br><br><button name="apply_filters" id="apply_filters" title="Apply Filters" style="clear: both; float: left; margin: 5px;">Apply Filters</button>
</div>

<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
<input type="hidden" name="submenu" value="<?php echo $submenu; ?>">
<input type="hidden" name="vocab" value="<?php echo (($vocab<2) ? 1 : $vocab); ?>">

</form>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both; margin-bottom: 10px;">
		<label class="vocab_search_form">Add New <?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?>:</label>
	</div>
	<form id="new_vocab_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&vocab=<?php echo intval($vocab); ?>">
	<?php
		if (intval($_SESSION["vocab_params"]["vocab"]) == 5) {
			// Child LOINC
			#lab_id lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_lab_id\">Child Lab</label><br><select class=\"ui-corner-all required\" name=\"new_lab_id\" id=\"new_lab_id\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["lab"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#child_loinc text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_child_loinc\">Child Test Concept Code</label><br><input class=\"ui-corner-all required\" type=\"text\" name=\"new_child_loinc\" id=\"new_child_loinc\" /></div>";
			#archived y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_archived\">Archived?</label><br><select class=\"ui-corner-all\" name=\"new_archived\" id=\"new_archived\">\n";
			echo "<option value=\"-1\">--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			#master_loinc lookup (incl master concept name)
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_master_loinc\">Master Test Concept Code (LOINC)</label><br><select class=\"ui-corner-all required\" name=\"new_master_loinc\" id=\"new_master_loinc\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = sprintf("SELECT vml.l_id AS value, vml.loinc AS label, vml.concept_name AS concept FROM %svocab_master_loinc vml ORDER BY vml.loinc;", $my_db_schema);
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s - (%s)</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"), htmlentities($lookup_row->concept, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#interpret_results y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_interpret_results\">Quantitative Result?</label><br><select class=\"ui-corner-all\" name=\"new_interpret_results\" id=\"new_interpret_results\">\n";
			echo "<option value=\"-1\">--</option>\n";
			echo "<option value=\"t\" selected>Yes</option>\n";
			echo "<option value=\"f\">No</option>\n";
			echo "</select></div>";
			#semi_auto y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_semi_auto\">Semi-Automated Entry?</label><br><select class=\"ui-corner-all\" name=\"new_semi_auto\" id=\"new_semi_auto\">\n";
			echo "<option value=\"-1\">--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			#validate_child_snomed y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_validate_child_snomed\">Validate Child SNOMED?</label><br><select class=\"ui-corner-all\" name=\"new_validate_child_snomed\" id=\"new_validate_child_snomed\">\n";
			echo "<option value=\"-1\">--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			#result location lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_location\">Result Location</label><br><select class=\"ui-corner-all\" name=\"new_location\" id=\"new_location\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["result_location"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of Result Locations.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#units text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_units\">Units</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_units\" id=\"new_units\" /></div>";
			#refrange text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_refrange\">Reference Range</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_refrange\" id=\"new_refrange\" /></div>";
			#hl7_refrange text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_hl7_refrange\">HL7 Reference Range</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_hl7_refrange\" id=\"new_hl7_refrange\" /></div>";
			#pregnancy y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_pregnancy\">Indicates Pregnancy?</label><br><select class=\"ui-corner-all\" name=\"new_pregnancy\" id=\"new_pregnancy\">\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#--extended--
			echo "<div style=\"clear: both; margin-bottom: 10px;\"><label class=\"vocab_search_form\">Extended Child LOINC Fields</label></div>";
			#child_orderable_test_code text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_child_orderable_test_code\">Child Orderable Test Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_child_orderable_test_code\" id=\"new_child_orderable_test_code\" /></div>";
			#child_resultable_test_code text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_child_resultable_test_code\">Child Resultable Test Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_child_resultable_test_code\" id=\"new_child_resultable_test_code\" /></div>";
			#child_concept_name text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_child_concept_name\">Child Concept Name</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_child_concept_name\" id=\"new_child_concept_name\" /></div>";
			#child_alias text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_child_alias\">Alias</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_child_alias\" id=\"new_child_alias\" /></div>";
			
		} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 4) {
			// Child Organism
			#lab_id lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_lab_id\">Child Lab</label><br><select class=\"ui-corner-all required\" name=\"new_lab_id\" id=\"new_lab_id\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["lab"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#child_code text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_child_code\">Child Code</label><br><input class=\"ui-corner-all required\" type=\"text\" name=\"new_child_code\" id=\"new_child_code\" /></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#organism lookup (incl master snomed)
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_organism\">Master Organism SNOMED</label><br><select class=\"ui-corner-all\" name=\"new_organism\" id=\"new_organism\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = 'SELECT vmo.o_id AS value, vmo.snomed AS snomed, mv.concept AS label 
					FROM '.$my_db_schema.'vocab_master_organism vmo 
					LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (vmo.organism = mv.id) 
					INNER JOIN '.$my_db_schema.'vocab_master_vocab mv2 ON (vmo.snomed_category = mv2.id) 
					WHERE mv2.category = '.$my_db_schema.'vocab_category_id(\'snomed_category\') AND mv2.concept = \'Organism\'
					ORDER BY mv.concept, vmo.snomed;';
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of organisms.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s [SNOMED %s]</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"), htmlentities($lookup_row->snomed, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			echo "<div class=\"add-form-divider\"></div>";
			
			#test result lookup (incl master snomed)
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_test_result_id\">Master Test Result SNOMED</label><br><select class=\"ui-corner-all\" name=\"new_test_result_id\" id=\"new_test_result_id\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = 'SELECT vmo.o_id AS value, vmo.snomed AS snomed, mv.concept AS label 
					FROM '.$my_db_schema.'vocab_master_organism vmo 
					LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (vmo.organism = mv.id) 
					INNER JOIN '.$my_db_schema.'vocab_master_vocab mv2 ON (vmo.snomed_category = mv2.id) 
					WHERE mv2.category = '.$my_db_schema.'vocab_category_id(\'snomed_category\') AND mv2.concept = \'Test Result\'
					ORDER BY mv.concept, vmo.snomed;';
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of organisms.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s [SNOMED %s]</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"), htmlentities($lookup_row->snomed, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			echo "<div class=\"add-form-divider\"></div>";
			
			#result_value text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_result_value\">Result Value</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_result_value\" id=\"new_result_value\" /></div>";
			#comment text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_comment\">Comments</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_comment\" id=\"new_comment\" /></div>";
			
		} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 3) {
			// Master Organism
			#snomed_category lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_snomed_category\">SNOMED Type</label><br><select class=\"ui-corner-all required\" name=\"new_snomed_category\" id=\"new_snomed_category\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = sprintf("SELECT vm.id AS value, m2a.coded_value AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('snomed_category') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY m2a.coded_value;", $my_db_schema, $my_db_schema, $app_id);
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of SNOMED categories.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#snomed text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_snomed\">SNOMED Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_snomed\" id=\"new_snomed\" /></div>";
			#snomed_alt text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_snomed_alt\">Secondary SNOMED Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_snomed_alt\" id=\"new_snomed_alt\" /></div>";
			echo "<div class=\"add-form-divider\"></div>";
			
			#condition lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_condition\">Master Condition</label><br><select class=\"ui-corner-all\" name=\"new_condition\" id=\"new_condition\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["condition"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#organism lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_organism\">Type Concept Name</label><br><select class=\"ui-corner-all\" name=\"new_organism\" id=\"new_organism\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = sprintf("SELECT vm.id AS value, m2a.coded_value AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('organism') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY m2a.coded_value;", $my_db_schema, $my_db_schema, $app_id);
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of organisms.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			echo "<div class=\"add-form-divider\"></div>";
			
			#list lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_list\">List</label><br><select class=\"ui-corner-all\" name=\"new_list\" id=\"new_list\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["list"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of lists.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#test_result lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_test_result\">Test Result</label><br><select class=\"ui-corner-all\" name=\"new_test_result\" id=\"new_test_result\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["test_result"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of test results.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#status lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_status\">Set State Case Status To...</label><br><select class=\"ui-corner-all\" name=\"new_status\" id=\"new_status\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["status"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#nom_is_surveillance y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_nom_is_surveillance\">Nominal Surveillance?</label><br><select class=\"ui-corner-all required\" name=\"new_nom_is_surveillance\" id=\"new_nom_is_surveillance\">\n";
			echo "<option value=\"-1\">--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			
		} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 2) {
			// Master Condition
			#disease_category lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_disease_category\">Disease Category</label><br><select class=\"ui-corner-all required\" name=\"new_disease_category\" id=\"new_disease_category\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = sprintf("SELECT vm.id AS value, m2a.coded_value AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('disease_category') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY m2a.coded_value;", $my_db_schema, $my_db_schema, $app_id);
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of disease categories.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#condition lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_condition\">Condition</label><br><select class=\"ui-corner-all required\" name=\"new_condition\" id=\"new_condition\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = sprintf("SELECT vm.id AS value, m2a.coded_value AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('condition') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY m2a.coded_value;", $my_db_schema, $my_db_schema, $app_id);
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#valid_specimen multi
			echo "<div class=\"addnew_field\">";
			echo "<div class=\"addnew_lookup_container\"><label class=\"vocab_add_form\">Valid Specimen Sources</label> <span rel=\"f_validspecimen\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_validspecimen\" class=\"vocab_filter_selectnone\">None</span><br>";
			echo "<div class=\"addnew_lookup_checklist ui-corner-all\" rel=\"f_validspecimen\">";
			$specimen_qry = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') ORDER BY concept;", $my_db_schema);
			$specimen_rs = @pg_query($host_pa, $specimen_qry);
			while ($specimen_row = @pg_fetch_object($specimen_rs)) {
				echo "<label class=\"pseudo_select_label\" for=\"new_valid_specimen_".$specimen_row->id."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"new_valid_specimen[]\" id=\"new_valid_specimen_".$specimen_row->id."\" value=\"".htmlentities($specimen_row->id, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
			}
			@pg_free_result($specimen_rs);
			echo "</div></div></div>";
			#invalid_specimen multi
			echo "<div class=\"addnew_field\">";
			echo "<div class=\"addnew_lookup_container\"><label class=\"vocab_add_form\">Invalid Specimen Sources</label> <span rel=\"f_invalidspecimen\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_invalidspecimen\" class=\"vocab_filter_selectnone\">None</span><br>";
			echo "<div class=\"addnew_lookup_checklist ui-corner-all\" rel=\"f_invalidspecimen\">";
			$specimen_qry = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') ORDER BY concept;", $my_db_schema);
			$specimen_rs = @pg_query($host_pa, $specimen_qry);
			while ($specimen_row = @pg_fetch_object($specimen_rs)) {
				echo "<label class=\"pseudo_select_label\" for=\"new_invalid_specimen_".$specimen_row->id."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"new_invalid_specimen[]\" id=\"new_invalid_specimen_".$specimen_row->id."\" value=\"".htmlentities($specimen_row->id, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
			}
			@pg_free_result($specimen_rs);
			echo "</div></div></div>";
			
			#white_rule text
			echo "<div class=\"addnew_field\" style=\"vertical-align: top;\"><label class=\"vocab_add_form\" for=\"new_white_rule\">Morbidity Whitelist Rules</label><br><textarea class=\"ui-corner-all\" name=\"new_white_rule\" id=\"new_white_rule\"></textarea></div>";
			#contact_white_rule text
			echo "<div class=\"addnew_field\" style=\"vertical-align: top;\"><label class=\"vocab_add_form\" for=\"new_contact_white_rule\">Contact Whitelist Rules</label><br><textarea class=\"ui-corner-all\" name=\"new_contact_white_rule\" id=\"new_contact_white_rule\"></textarea></div>";
			#gateway_xref text
			echo "<div class=\"addnew_field\">";
			echo "<div class=\"addnew_lookup_container\"><label class=\"vocab_add_form\">Gateway Crossrefs</label><br>";
			echo "<div class=\"addnew_lookup_checklist ui-corner-all\">";
			$gateway_xref_qry = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('gateway_xref') ORDER BY concept;", $my_db_schema);
			$gateway_rs = @pg_query($host_pa, $gateway_xref_qry);
			while ($gateway_row = @pg_fetch_object($gateway_rs)) {
				echo "<label class=\"pseudo_select_label\" for=\"new_gateways_".$gateway_row->id."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"new_gateways[]\" id=\"new_gateways_".$gateway_row->id."\" value=\"".htmlentities($gateway_row->id, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($gateway_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($gateway_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($gateway_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
			}
			@pg_free_result($gateway_rs);
			echo "</div></div></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#check_xref_first y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_check_xref_first\">Check Crossrefs First?</label><br><select class=\"ui-corner-all\" name=\"new_check_xref_first\" id=\"new_check_xref_first\">\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			#immediate_notify y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_immediate_notify\">Immediately Notifiable?</label><br><select class=\"ui-corner-all required\" name=\"new_immediate_notify\" id=\"new_immediate_notify\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\">No</option>\n";
			echo "</select></div>";
			#require_specimen y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_require_specimen\">Require Specimen Source<br>From Nominal Culture?</label><br><select class=\"ui-corner-all required\" name=\"new_require_specimen\" id=\"new_require_specimen\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\">No</option>\n";
			echo "</select></div>";
			#new_event y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_new_event\">Different Species/Toxin<br>Same Event or New Event?</label><br><select class=\"ui-corner-all required\" name=\"new_new_event\" id=\"new_new_event\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
			echo "<option value=\"t\">New</option>\n";
			echo "<option value=\"f\">Same</option>\n";
			echo "</select></div>";
			#notify_state y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_notify_state\">Notify State Upon Receipt?</label><br><select class=\"ui-corner-all required\" name=\"new_notify_state\" id=\"new_notify_state\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\">No</option>\n";
			echo "</select></div>";
			#autoapproval y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_autoapproval\">Autoapproval?</label><br><select class=\"ui-corner-all required\" name=\"new_autoapproval\" id=\"new_autoapproval\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\">No</option>\n";
			echo "</select></div>";
			#district_override lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_district_override\">Jurisdiction Override</label><br><select class=\"ui-corner-all\" name=\"new_district_override\" id=\"new_district_override\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["district_override"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of jurisdictions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			
		} else {
			// Master LOINC
			#loinc text
			echo "<div class=\"addnew_field\" style=\"vertical-align: top;\"><label class=\"vocab_add_form required\" for=\"new_loinc\">Test Concept Code (LOINC)</label><br><input class=\"ui-corner-all required\" type=\"text\" name=\"new_loinc\" id=\"new_loinc\" /></div>";
			#concept_name text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_concept_name\">Preferred Concept Name</label><br><textarea class=\"ui-corner-all\" name=\"new_concept_name\" id=\"new_concept_name\"></textarea></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#condition_from_result y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_condition_from_result\">Look Up Condition?</label><br><select class=\"ui-corner-all\" name=\"new_condition_from_result\" id=\"new_condition_from_result\">\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			#trisano_condition lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_trisano_condition\">Master Condition</label><br><select class=\"ui-corner-all\" name=\"new_trisano_condition\" id=\"new_trisano_condition\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["trisano_condition"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#organism_from_result y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_organism_from_result\">Look Up Organism?</label><br><select class=\"ui-corner-all\" name=\"new_organism_from_result\" id=\"new_organism_from_result\">\n";
			echo "<option value=\"t\">Yes</option>\n";
			echo "<option value=\"f\" selected>No</option>\n";
			echo "</select></div>";
			#trisano_organism lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_trisano_organism\">Master Organism</label><br><select class=\"ui-corner-all\" name=\"new_trisano_organism\" id=\"new_trisano_organism\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["trisano_organism"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of organisms.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			
			echo "<div class=\"add-form-divider\"></div>";
			#trisano_test_type lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"new_trisano_test_type\">Test Type</label><br><select class=\"ui-corner-all required\" name=\"new_trisano_test_type\" id=\"new_trisano_test_type\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["trisano_test_type"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of test types.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#specimen_source lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_specimen_source\">Specimen Source</label><br><select class=\"ui-corner-all\" name=\"new_specimen_source\" id=\"new_specimen_source\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["specimen_source"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of specimen sources.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			#list lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_list\">List</label><br><select class=\"ui-corner-all\" name=\"new_list\" id=\"new_list\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["list"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of lists.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			/* 
			#allow_new_cmr y/n
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_allow_new_cmr\">Create New CMRs?</label><br><select class=\"ui-corner-all\" name=\"new_allow_new_cmr\" id=\"new_allow_new_cmr\">\n";
			echo "<option value=\"t\" selected>Yes</option>\n";
			echo "<option value=\"f\">No</option>\n";
			echo "</select></div>";
			*/
			#gray_rule text
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_gray_rule\">Graylist Rules</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"new_gray_rule\" id=\"new_gray_rule\" /></div>";
			/*
			#status lookup
			echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"new_status\">Set State Case Status To...</label><br><select class=\"ui-corner-all\" name=\"new_status\" id=\"new_status\">\n";
			echo "<option value=\"-1\" selected>--</option>\n";
				$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["status"]["lookupqry"];
				$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of statuses.", 1);
				while ($lookup_row = @pg_fetch_object($lookup_rs)) {
					echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
				}
				pg_free_result($lookup_rs);
			echo "</select></div>";
			*/
			
		}
	?>
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_savevocab" id="new_savevocab">Save New <?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?></button>
		<button type="button" name="addnew_cancel" id="addnew_cancel">Cancel</button>
	</form>
</div>


<div class="vocab_paging_top">
<?php
	if ($numrows > 0) {
		echo "Page: ";
		/******  build the pagination links ******/
		// range of num links to show
		$range = 3;

		// if not on page 1, don't show back links
		if ($currentpage > 1) {
		   // show << link to go back to page 1
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;&lt;</a> ", $main_url, "1", $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // get previous page num
		   $prevpage = $currentpage - 1;
		   // show < link to go back to 1 page
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;</a> ", $main_url, $prevpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if 

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
		   // if it's a valid page number...
		   if (($x > 0) && ($x <= $totalpages)) {
			  // if we're on current page...
			  if ($x == $currentpage) {
				 // 'highlight' it but don't make a link
				 printf(" [<b>%s</b>] ", $x);
			  // if not current page...
			  } else {
				 // make it a link
				 printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">%s</a> ", $main_url, $x, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $x);
			  } // end else
		   } // end if 
		} // end for
						 
		// if not on last page, show forward and last page links        
		if ($currentpage != $totalpages) {
		   // get next page
		   $nextpage = $currentpage + 1;
			// echo forward link for next page 
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;</a> ", $main_url, $nextpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // echo forward link for lastpage
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;&gt;</a> ", $main_url, $totalpages, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if
		/****** end build pagination links ******/
	}
?>

</div>

<table id="latestTasks" class="ui-corner-all">
	<caption>
		<?php
			if ($numrows < 1) {
				printf("No %s records found!", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]);
			} elseif ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"] > 0) {
				printf("%d %s records found (Displaying %d - %d)", $numrows, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"], intval($offset+1), ((intval($offset+$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"])>$numrows)?$numrows:intval($offset+$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"])));
			} else {
				printf("%d %s records found (Displaying all)", $numrows, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]);
			}
		?>
		<form name="user_rowselect" id="user_rowselect" method="GET" action="<?php echo $main_url; ?>" style="display: inline; margin-left: 30px;">
			<select name="user_rows" id="user_rows">
			<?php
				foreach ($valid_rowsize as $this_rowsize) {
					echo "<option value=\"" . $this_rowsize . "\"" . (($this_rowsize == $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"]) ? " selected" : "") . ">" . (($this_rowsize > 0) ? $this_rowsize : "All") . "</option>";
				}
			?>
			</select>
			 rows per page
			<input type="hidden" name="selected_page" value="<?php echo $selected_page; ?>">
			<input type="hidden" name="submenu" value="<?php echo $submenu; ?>">
			<input type="hidden" name="vocab" value="<?php echo (($vocab<2) ? 1 : $vocab); ?>">
		</form>
	</caption>
	<thead>
		<tr>
			<th>Actions</th>
<?php

		// draw column headers...
		foreach ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] as $headercol => $headername) {
			if ($headername['display']) {
				$sort_indicator = "";
				$sort_text = sprintf("Sort by '%s' [A-Z]", $headername['label']);
				if ($headercol == $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"]) {
					if ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_order"] == "ASC") {
						$sort_indicator = "colheader_sort_down";
						$sort_text = sprintf("Sort by '%s' [Z-A]", $headername['label']);
					} else {
						$sort_indicator = "colheader_sort_up";
						$sort_text = sprintf("Sort by '%s' [A-Z]", $headername['label']);
					}
				}
				printf("<th><a class=\"colheader %s\" title=\"%s\" href=\"%s?selected_page=%s&submenu=%s&vocab=%s&sort=%s&order=%s&currentpage=1\">%s</a></div></th>", $sort_indicator, $sort_text, $main_url, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $headercol, ((($headercol == $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_col"]) && ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["sort_order"] == "ASC")) ? "2" : "1"), $headername['label']);
			}
		}
		if (intval($_SESSION["vocab_params"]["vocab"]) == 5) {
			// per configured application, draw a 'result rules' column...
			$app_sql = "SELECT id, app_name FROM ".$my_db_schema."vocab_app ORDER BY app_name;";
			$app_rs = @pg_query($host_pa, $app_sql);
			if ($app_rs) {
				while ($app_row = @pg_fetch_object($app_rs)) {
					echo "<th>".$app_row->app_name." Result Rules</th>";
				}
			} else {
				suicide("Unable to retrieve list of Applications.", 1);
			}
			@pg_free_result($app_rs);
			unset($app_row);
		}
		if (intval($_SESSION["vocab_params"]["vocab"]) == 1) {
			// per configured application, draw a 'master loinc rules' column...
			$app_sql = "SELECT id, app_name FROM ".$my_db_schema."vocab_app ORDER BY app_name;";
			$app_rs = @pg_query($host_pa, $app_sql);
			if ($app_rs) {
				while ($app_row = @pg_fetch_object($app_rs)) {
					echo "<th>".$app_row->app_name." Case Management Rules</th>";
				}
			} else {
				suicide("Unable to retrieve list of Applications.", 1);
			}
			@pg_free_result($app_rs);
			unset($app_row);
		}
		echo "</tr></thead><tbody>";
		
		$vocab_select = "SELECT ";
		foreach ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] as $selectcol => $selectcoldata) {
			$vocab_select .= $selectcoldata['colname'] . " AS " . $selectcol . ", ";
		}

		// go grab our data...
		if ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"] > 0) {
			$getvocab_sql = sprintf("%s %s %s LIMIT %d OFFSET %d;", trim($vocab_select, ", "), $vocab_fromwhere, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["order_by"], $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["page_rows"], $offset);
		} else {
			$getvocab_sql = sprintf("%s %s %s;", trim($vocab_select, ", "), $vocab_fromwhere, $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["order_by"]);
		}
		#debug echo $getvocab_sql;
		$getvocab_rs = pg_query($host_pa,$getvocab_sql) or die("Can't Perform Query ".pg_last_error());
		
		$previous_row_id = '';
		while ($getvocab_row = pg_fetch_object($getvocab_rs)) {
			unset($this_rowclass);
			if (isset($getvocab_row->archived) && ($getvocab_row->archived == 't')) {
				$this_rowstyle = 'text-decoration: line-through !important; color: darkgray !important;';
			} else {
				$this_rowstyle = 'text-decoration: none;';
			}
			if ((intval($_SESSION["vocab_params"]["vocab"]) == 4) || (intval($_SESSION["vocab_params"]["vocab"]) == 5)) {
				echo '<tr id="vocab_row_'.$getvocab_row->id.'" style="'.$this_rowstyle.'"><td nowrap>';
			} else {
				echo '<tr id="vocab_row_'.$getvocab_row->$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"].'" style="'.$this_rowstyle.'"><td nowrap>';
			}
			if ((intval($_SESSION["vocab_params"]["vocab"]) == 4) || (intval($_SESSION["vocab_params"]["vocab"]) == 5)) {
				// hard-code 'id' for child vocabs
				$delete_array = array(
					'current_id' => $getvocab_row->id,
					'previous_id' => $previous_row_id
				);
				printf("<button class=\"edit_vocab\" type=\"button\" value=\"%s\" title=\"Edit this vocabulary\">Edit</button>", $getvocab_row->id);
				printf("<button class=\"delete_vocab\" type=\"button\" value='%s' title=\"Delete this vocabulary\">Delete</button>", @json_encode($delete_array));
				$previous_row_id = $getvocab_row->id;
			} else {
				// use configured 'id_column' for master vocabs
				$delete_array = array(
					'current_id' => $getvocab_row->$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"],
					'previous_id' => $previous_row_id
				);
				printf("<button class=\"edit_vocab\" type=\"button\" value=\"%s\" title=\"Edit this vocabulary\">Edit</button>", $getvocab_row->$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"]);
				printf("<button class=\"delete_vocab\" type=\"button\" value='%s' title=\"Delete this vocabulary\">Delete</button>", @json_encode($delete_array));
				$previous_row_id = $getvocab_row->$_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["query_data"]["id_column"];
			}
			echo "</ul></td>";
			
			foreach ($_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"] as $colname => $colvalue) {
				if ($colvalue['display']) {
					if (in_array($colname, $bool_cols)) {
						if ($colname == "new_event") {
							printf("<td class=\"vocab_data_cell\">%s</td>", ((trim($getvocab_row->$colname) == "t") ? "New" : "Same"));
						} else {
							printf("<td class=\"vocab_data_cell\">%s</td>", ((trim($getvocab_row->$colname) == "t") ? "Yes" : "No"));
						}
					} else {
						if ($colname == "gateway_xref") {
							printf("<td class=\"vocab_data_cell\">%s</td>", ((strlen(trim($getvocab_row->$colname)) > 0) ? htmlentities(gatewayCrossrefIdValues(trim($getvocab_row->$colname)), ENT_QUOTES, "UTF-8") : ""));
						} elseif (($colname == "valid_specimen") || ($colname == "invalid_specimen")) {
							printf("<td class=\"vocab_data_cell\">%s</td>", ((strlen(trim($getvocab_row->$colname)) > 0) ? htmlentities(specimenIdValues(trim($getvocab_row->$colname)), ENT_QUOTES, "UTF-8") : ""));
						} else {
							if (isset($colvalue['linkback']) && (intval($colvalue['linkback']) > 0)) {
								printf("<td class=\"vocab_data_cell\"><a href=\"%s\">%s</a></td>", $main_url."?selected_page=6&submenu=3&vocab=".intval($colvalue['linkback'])."&q=".urlencode(trim($getvocab_row->$colname)), htmlentities(trim($getvocab_row->$colname), ENT_QUOTES, "UTF-8"));
							} else {
								printf("<td class=\"vocab_data_cell\">%s</td>", htmlentities(trim($getvocab_row->$colname), ENT_QUOTES, "UTF-8"));
							}
						}
					}
				}
			}
			if (intval($_SESSION["vocab_params"]["vocab"]) == 5) {
				/**
				 * Interpretive rule management
				 * display separate column for each configured app...
				 */
				$app_sql = "SELECT id, app_name FROM ".$my_db_schema."vocab_app ORDER BY app_name;";
				$app_rs = @pg_query($host_pa, $app_sql);
				if ($app_rs) {
					while ($app_row = @pg_fetch_object($app_rs)) {
						echo "<td>";
						$result_rules_sql = sprintf("SELECT c2m.*, m2a.coded_value AS app_value FROM %svocab_c2m_testresult c2m
							INNER JOIN %svocab_master2app m2a ON (m2a.master_id = c2m.master_id AND m2a.app_id = c2m.app_id) 
							WHERE c2m.child_loinc_id = %d ORDER BY c2m.id", $my_db_schema, $my_db_schema, intval($getvocab_row->id));
						$result_rules_rs = @pg_query($host_pa, $result_rules_sql);
						if ($result_rules_rs) {
							unset($rule_mod_params);
							echo "<table id=\"result_rules\" width=\"100%\"><thead><tr><th>Actions</th><th>Rule</th></tr></thead><tbody>";
							if (pg_num_rows($result_rules_rs) < 1) {
								echo "<tr><td nowrap class=\"action_col\"><span class=\"ui-icon ui-icon-elrerror\" style=\"float: right;\"></span></td><td nowrap><strong style=\"color: orangered;\">No Result Rules defined for this LOINC!</strong></td></tr>";
							}
							while ($result_rules_row = @pg_fetch_object($result_rules_rs)) {
								unset($rule_mod_params);
								$rule_mod_params = array(
									"id" => intval($result_rules_row->id),
									"focus_id" => intval($getvocab_row->id),
									"action" => "edit",
									"lab_name" => trim($getvocab_row->lab),
									"child_loinc" => trim($getvocab_row->child_loinc),
									"application" => intval(trim($result_rules_row->app_id)),
									"conditions" => json_decode($result_rules_row->conditions_structured, true),
									"master_result" => intval(trim($result_rules_row->master_id)),
									"comments" => trim($result_rules_row->results_to_comments));
								echo "<tr><td nowrap class=\"action_col\"><button class=\"edit_result_rule\" value='".json_encode($rule_mod_params)."' type=\"button\" title=\"Edit this Result Rule\">Edit</button>";
								unset($rule_mod_params);
								$rule_mod_params = array(
									"id" => intval($result_rules_row->id),
									"focus_id" => intval($getvocab_row->id),
									"action" => "delete",
									"lab_name" => trim($getvocab_row->lab),
									"child_loinc" => trim($getvocab_row->child_loinc));
								echo "<button class=\"delete_result_rule\" value='".json_encode($rule_mod_params)."' type=\"button\" title=\"Delete this Result Rule\">Delete</button></td>";
								
								echo "<td nowrap style=\"width: 100%;\" title=\"Add Results to Comments: ".$result_rules_row->results_to_comments."\">[";
								$this_rule_decoded_conditions = @json_decode($result_rules_row->conditions_structured);
								$this_rule_counter = 1;
								foreach ($this_rule_decoded_conditions as $this_condition) {
									echo "Result <strong style=\"color: green; font-size: 1.1em;\">" . graphicalOperatorById($this_condition->operator, $host_pa, $my_db_schema) . "</strong> &quot;" . htmlentities($this_condition->operand, ENT_QUOTES, "UTF-8") . "&quot;";
									if ($this_rule_counter < sizeof($this_rule_decoded_conditions)) {
										echo " <strong style=\"color: darkred; font-size: 1.1em;\">&</strong> ";
									}
									$this_rule_counter++;
								}
								echo "] <strong style=\"color: blue; font-size: 1.1em;\">&rArr;</strong> <strong>".htmlentities(trim($result_rules_row->app_value), ENT_QUOTES, "UTF-8");
								echo "</strong></td></tr>";
							}
							unset($rule_mod_params);
							$rule_mod_params = array(
								"id" => intval($getvocab_row->id),
								"focus_id" => intval($getvocab_row->id),
								"action" => "add",
								"lab_name" => trim($getvocab_row->lab),
								"child_loinc" => trim($getvocab_row->child_loinc));
							echo "<tr><td nowrap class=\"action_col\"><button class=\"add_result_rule\" value='".json_encode($rule_mod_params)."' type=\"button\" title=\"Add new Result Rule for this Child LOINC\">Add New</button></td><td style=\"width: 100%;\" nowrap><em>&lt;Add New Result Rule&gt;</em></td></tr>";
							echo "</tbody></table>";
						} else {
							suicide("Unable to retrieve list of ".htmlentities($app_row->app_name, ENT_QUOTES, "UTF-8")." Result Rules for LOINC ".htmlentities($getvocab_row->child_loinc, ENT_QUOTES, "UTF-8").".", 1);
						}
						@pg_free_result($result_rules_rs);
						echo "</td>";
					}
				} else {
					suicide("Unable to retrieve list of Applications.", 1);
				}
				@pg_free_result($app_rs);
				unset($app_row);
			}
			
			if (intval($_SESSION["vocab_params"]["vocab"]) == 1) {
				/**
				 * Case Management (Master LOINC) rule management
				 * display separate column for each configured app...
				 */
				$app_sql = "SELECT id, app_name FROM ".$my_db_schema."vocab_app ORDER BY app_name;";
				$app_rs = @pg_query($host_pa, $app_sql);
				if ($app_rs) {
					while ($app_row = @pg_fetch_object($app_rs)) {
						echo "<td>";
						$result_rules_sql = 'SELECT vrml.*, mv.concept AS master_status_value, 
							CASE WHEN vrml.allow_new_cmr IS TRUE THEN \'Yes\' ELSE \'No\' END AS allow_new_cmr_verbose,  
							CASE WHEN vrml.is_surveillance IS TRUE THEN \'Yes\' ELSE \'No\' END AS is_surveillance_verbose 
							FROM '.$my_db_schema.'vocab_rules_masterloinc vrml
							LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (mv.id = vrml.state_case_status_master_id) 
							WHERE vrml.master_loinc_id = '.intval($getvocab_row->l_id).' ORDER BY vrml.id;';
						$result_rules_rs = @pg_query($host_pa, $result_rules_sql);
						if ($result_rules_rs) {
							unset($rule_mod_params);
							echo "<table id=\"result_rules\" width=\"100%\"><thead><tr><th>Actions</th><th>Condition</th><th nowrap>New CMR?</th><th nowrap>Surveillance?</th><th nowrap>State Case Status</th></tr></thead><tbody>";
							if (pg_num_rows($result_rules_rs) < 1) {
								echo "<tr><td nowrap class=\"action_col\"><span class=\"ui-icon ui-icon-elrerror\" style=\"float: right;\"></span></td><td nowrap colspan=\"3\"><strong style=\"color: orangered;\">No Case Management Rules defined for this LOINC!</strong></td></tr>";
							}
							while ($result_rules_row = @pg_fetch_object($result_rules_rs)) {
								unset($rule_mod_params);
								$rule_mod_params = array(
									"id" => intval($result_rules_row->id),
									"focus_id" => intval($getvocab_row->l_id),
									"action" => "edit",
									"loinc" => trim($getvocab_row->loinc),
									"application" => intval(trim($result_rules_row->app_id)),
									"conditions" => json_decode($result_rules_row->conditions_structured, true),
									"master_result" => intval(trim($result_rules_row->state_case_status_master_id)),
									"allow_new_cmr" => trim($result_rules_row->allow_new_cmr), 
									"is_surveillance" => trim($result_rules_row->is_surveillance));
								echo "<tr><td nowrap class=\"action_col\"><button class=\"edit_cmr_rule\" value='".json_encode($rule_mod_params)."' type=\"button\" title=\"Edit this Rule\">Edit</button>";
								unset($rule_mod_params);
								$rule_mod_params = array(
									"id" => intval($result_rules_row->id),
									"focus_id" => intval($getvocab_row->l_id),
									"action" => "delete",
									"loinc" => trim($getvocab_row->loinc));
								echo "<button class=\"delete_cmr_rule\" value='".json_encode($rule_mod_params)."' type=\"button\" title=\"Delete this Rule\">Delete</button></td>";
								
								echo "<td nowrap style=\"width: 100%;\">";
								$this_rule_decoded_conditions = @json_decode($result_rules_row->conditions_structured);
								$this_rule_counter = 1;
								foreach ($this_rule_decoded_conditions as $this_condition) {
									echo "Test Result <strong style=\"color: green; font-size: 1.1em;\">" . graphicalOperatorById($this_condition->operator, $host_pa, $my_db_schema) . "</strong> &quot;" . appCodedValueByMasterID($this_condition->operand) . "&quot;";
									if ($this_rule_counter < sizeof($this_rule_decoded_conditions)) {
										echo " <strong style=\"color: darkred; font-size: 1.1em;\">&</strong> ";
									}
									$this_rule_counter++;
								}
								echo "</td>";
								if (trim($result_rules_row->allow_new_cmr_verbose) == 'Yes') {
									echo '<td nowrap><span class="ui-icon ui-icon-elrsuccess" title="Yes"></span></td>';
								} else {
									echo '<td nowrap><span class="ui-icon ui-icon-elrcancel" title="No"></span></td>';
								}
								if (trim($getvocab_row->organism_from_result) == "t") {
									echo '<td nowrap><em>From Org.</em></td>';
								} else {
									if (trim($result_rules_row->is_surveillance_verbose) == 'Yes') {
										echo '<td nowrap><span class="ui-icon ui-icon-elrsuccess" title="Yes"></span></td>';
									} else {
										echo '<td nowrap><span class="ui-icon ui-icon-elrcancel" title="No"></span></td>';
									}
								}
								echo "<td nowrap><strong>".((strlen(trim($result_rules_row->master_status_value)) > 0) ? htmlentities(trim($result_rules_row->master_status_value), ENT_QUOTES, "UTF-8") : '--');
								echo "</strong></td></tr>";
							}
							unset($rule_mod_params);
							$rule_mod_params = array(
								"id" => intval($getvocab_row->l_id),
								"focus_id" => intval($getvocab_row->l_id),
								"action" => "add",
								"loinc" => trim($getvocab_row->loinc));
							echo "<tr><td nowrap class=\"action_col\"><button class=\"add_cmr_rule\" value='".json_encode($rule_mod_params)."' type=\"button\" title=\"Add new Case Management Rule for this Master LOINC\">Add New</button></td><td style=\"width: 100%;\" nowrap colspan=\"4\"><em>&lt;Add New Case Management Rule&gt;</em></td></tr>";
							echo "</tbody></table>";
						} else {
							suicide("Unable to retrieve list of ".htmlentities($app_row->app_name, ENT_QUOTES, "UTF-8")." Case Management Rules for Master LOINC ".htmlentities($getvocab_row->loinc, ENT_QUOTES, "UTF-8").".", 1);
						}
						@pg_free_result($result_rules_rs);
						echo "</td>";
					}
				} else {
					suicide("Unable to retrieve list of Applications.", 1);
				}
				@pg_free_result($app_rs);
				unset($app_row);
			}
			echo "</tr>";
		}
		
?>
	</tbody>
</table>

<div class="vocab_paging vocab_paging_bottom">
<?php
	if ($numrows > 0) {
		echo "Page: ";
		/******  build the pagination links ******/
		// range of num links to show
		$range = 3;

		// if not on page 1, don't show back links
		if ($currentpage > 1) {
		   // show << link to go back to page 1
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;&lt;</a> ", $main_url, "1", $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // get previous page num
		   $prevpage = $currentpage - 1;
		   // show < link to go back to 1 page
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&lt;</a> ", $main_url, $prevpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if 

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
		   // if it's a valid page number...
		   if (($x > 0) && ($x <= $totalpages)) {
			  // if we're on current page...
			  if ($x == $currentpage) {
				 // 'highlight' it but don't make a link
				 printf(" [<b>%s</b>] ", $x);
			  // if not current page...
			  } else {
				 // make it a link
				 printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">%s</a> ", $main_url, $x, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab), $x);
			  } // end else
		   } // end if 
		} // end for
						 
		// if not on last page, show forward and last page links        
		if ($currentpage != $totalpages) {
		   // get next page
		   $nextpage = $currentpage + 1;
			// echo forward link for next page 
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;</a> ", $main_url, $nextpage, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		   // echo forward link for lastpage
		   printf(" <a href=\"%s?currentpage=%s&selected_page=%s&submenu=%s&vocab=%s\">&gt;&gt;</a> ", $main_url, $totalpages, $selected_page, $submenu, (($vocab<2) ? 1 : $vocab));
		} // end if
		/****** end build pagination links ******/
	}
?>
</div>

<div id="confirm_delete_dialog" title="Delete this <?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?> record?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This <?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?> will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="confirm_deleterule_dialog" title="Delete this Result Rule?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This Result Interpretation Rule will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="confirm_deleterule_cmr_dialog" title="Delete this Case Management Rule?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This Case Management Rule will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<?php if ($_SESSION["vocab_params"]["vocab"] == 5) { ?>

<div id="rule_mod_dialog" title="Add/Edit Result Interpretation Rule">
	<form id="rule_mod_form" method="GET" action="<?php echo $main_page; ?>">
		<h2 id="rulemod_child_loinc"></h2>
		<label for="rulemod_application">Rule applies to:</label>
		<select class="ui-corner-all" style="margin: 0px;" name="rulemod_application" id="rulemod_application" title="Final result values will be transformed for this application">
			<option value="0" selected>--</option>
		<?php
			// get list of data types for menu
			$apps_sql = sprintf("SELECT DISTINCT id, app_name FROM %svocab_app ORDER BY app_name;", $my_db_schema);
			$apps_rs = @pg_query($host_pa, $apps_sql) or suicide("Unable to retrieve list of Applications.", 1, 1);
			while ($apps_row = pg_fetch_object($apps_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($apps_row->id), htmlentities($apps_row->app_name));
			}
			pg_free_result($apps_rs);
		?>
		</select><br><br>
		<h3>If Child Result meets these conditions...</h3>
		<div id="rulemod_condition_container">
			<div class="rulemod_condition" id="rulemod_condition_1">
				<button type="button" class="rulemod_delete_condition" value="1" title="Delete This Condition">Delete Condition</button><label for="rulemod_operator_1">Result </label> 
				<select class="ui-corner-all" style="margin: 0px;" name="rulemod_operator[1]" id="rulemod_operator_1" title="Choose a comparison type">
					<option value="0" selected>--</option>
				<?php
					// get list of logical operators for menu
					$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
					$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
					while ($operator_row = pg_fetch_object($operator_rs)) {
						printf("<option value=\"%d\">%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical);
					}
					pg_free_result($operator_rs);
				?>
				</select>
				<label for="rulemod_operand_value_1">this value:</label> <input class="ui-corner-all" type="text" name="rulemod_operand_value[1]" id="rulemod_operand_value_1" title="Compare message result with this value" />
			</div>
		</div>
		<input type="hidden" name="rulemod_condition_counter" id="rulemod_condition_counter" value="1">
		<button type="button" id="rulemod_add_condition" title="Add New Condition">Add Condition</button><br><br>
		<h3>Then set...</h3>
		<table>
			<tbody>
				<tr>
					<td>
						<label for="rulemod_master_result">Test Result:</label>
					</td>
					<td>
						<select class="ui-corner-all" style="margin: 0px;" name="rulemod_master_result" id="rulemod_master_result" title="Transform result to this Master Test Result value">
							<option value="0" selected>--</option>
						<?php
							// get list of master conditions for menu
							$masterresult_sql = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('test_result') ORDER BY concept;", $my_db_schema);
							$masterresult_rs = @pg_query($host_pa, $masterresult_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
							while ($masterresult_row = pg_fetch_object($masterresult_rs)) {
								printf("<option value=\"%d\">%s</option>", intval($masterresult_row->id), htmlentities($masterresult_row->concept, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($masterresult_rs);
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="rulemod_comments">Add Results to Comments:</label>
					</td>
					<td>
						<input class="ui-corner-all" type="text" name="rulemod_comments" id="rulemod_comments" />
					</td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" name="rulemod_id" id="rulemod_id" />
		<input type="hidden" name="focus_id" id="rulemod_focus_id" />
		<input type="hidden" name="rulemod_action" id="rulemod_action" />
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="vocab" value="<?php echo intval($vocab); ?>" />
	</form>
</div>

<?php } ?>

<?php if ($_SESSION["vocab_params"]["vocab"] == 1) { ?>

<div id="rule_mod_cmr_dialog" title="Add/Edit Case Management Rule">
	<form id="rule_mod_cmr_form" method="GET" action="<?php echo $main_page; ?>">
		<h2 id="rulemod_cmr_loinc"></h2>
		<label for="rulemod_cmr_application">Rule applies to:</label>
		<select class="ui-corner-all" style="margin: 0px;" name="rulemod_cmr_application" id="rulemod_cmr_application" title="Values will be transformed for this application">
			<option value="0" selected>--</option>
		<?php
			// get list of data types for menu
			$apps_sql = sprintf("SELECT DISTINCT id, app_name FROM %svocab_app ORDER BY app_name;", $my_db_schema);
			$apps_rs = @pg_query($host_pa, $apps_sql) or suicide("Unable to retrieve list of Applications.", 1, 1);
			while ($apps_row = pg_fetch_object($apps_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($apps_row->id), htmlentities($apps_row->app_name));
			}
			pg_free_result($apps_rs);
		?>
		</select><br><br>
		<h3>If...</h3>
		<div id="rulemod_cmr_condition_container">
			<div class="rulemod_condition" id="rulemod_cmr_condition_1">
				<button type="button" class="rulemod_cmr_delete_condition" value="1" title="Delete This Condition">Delete Condition</button><label for="rulemod_cmr_operator_1">Test Result</label>
				<select class="ui-corner-all" style="margin: 0px;" name="rulemod_cmr_operator[1]" id="rulemod_cmr_operator_1" title="Choose a comparison type">
					<option value="0" selected>--</option>
				<?php
					// get list of logical operators for menu
					$operator_sql = sprintf("SELECT DISTINCT id, label, graphical FROM %sstructure_operator WHERE operator_type = 1 ORDER BY id;", $my_db_schema);
					$operator_rs = @pg_query($host_pa, $operator_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
					while ($operator_row = pg_fetch_object($operator_rs)) {
						printf("<option value=\"%d\">%s (%s)</option>", intval($operator_row->id), htmlentities($operator_row->label, ENT_QUOTES, "UTF-8"), $operator_row->graphical);
					}
					pg_free_result($operator_rs);
				?>
				</select>
				<select class="ui-corner-all" style="margin: 0px;" name="rulemod_cmr_operand_value[1]" id="rulemod_cmr_operand_value_1">
					<option value="0" selected>--</option>
				<?php
					// get list of master test results for menu
					$masterresult_sql = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('test_result') ORDER BY concept;", $my_db_schema);
					$masterresult_rs = @pg_query($host_pa, $masterresult_sql) or suicide("Unable to retrieve list of Operators.", 1, 1);
					while ($masterresult_row = pg_fetch_object($masterresult_rs)) {
						printf("<option value=\"%d\">%s</option>", intval($masterresult_row->id), htmlentities($masterresult_row->concept, ENT_QUOTES, "UTF-8"));
					}
					pg_free_result($masterresult_rs);
				?>
				</select>
			</div>
		</div>
		<input type="hidden" name="rulemod_cmr_condition_counter" id="rulemod_cmr_condition_counter" value="1">
		<button type="button" id="rulemod_cmr_add_condition" title="Add New Condition">Add Condition</button><br><br>
		<h3>Then...</h3>
		<table>
			<tbody>
				<tr>
					<td>
						<label for="rulemod_new_cmr">Create New CMR?</label>
					</td>
					<td>
						<select class="ui-corner-all" style="margin: 0px;" name="rulemod_new_cmr" id="rulemod_new_cmr" title="Create a new Morbidity event based on this test result?">
							<option value="t" selected>Yes</option>
							<option value="f">No</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="rulemod_is_surveillance">Surveillance Event?</label>
					</td>
					<td>
						<select class="ui-corner-all" style="margin: 0px;" name="rulemod_is_surveillance" id="rulemod_is_surveillance" title="Indicates a Surveillance Event?">
							<option value="t" selected>Yes</option>
							<option value="f">No</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="rulemod_master_result">Set State Case Status to:</label>
					</td>
					<td>
						<select class="ui-corner-all" style="margin: 0px;" name="rulemod_state_case_status" id="rulemod_state_case_status" title="For a new Morbidity event, set this State Case Status.">
							<option value="0" selected>--</option>
						<?php
							// get list of state case statuses
							$casestatus_sql = 'SELECT vm.id AS value, vm.concept AS label FROM '.$my_db_schema.'vocab_master_vocab vm WHERE (vm.category = elr.vocab_category_id(\'case\')) ORDER BY vm.concept;';
							$casestatus_rs = @pg_query($host_pa, $casestatus_sql) or suicide("Unable to retrieve list of statuses.", 1);
							while ($casestatus_row = @pg_fetch_object($casestatus_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $casestatus_row->value, htmlentities($casestatus_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($casestatus_rs);
						?>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" name="rulemod_cmr_id" id="rulemod_cmr_id" />
		<input type="hidden" name="focus_id" id="rulemod_cmr_focus_id" />
		<input type="hidden" name="rulemod_cmr_action" id="rulemod_cmr_action" />
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="vocab" value="<?php echo intval($vocab); ?>" />
	</form>
</div>

<?php } ?>


<?php

	pg_free_result($getvocab_rs);
	
?>
		