<script type="text/javascript">
	$(function() {
		$("#addhl7form").show();
		$("#raw_xml").focus();
		
		$("#btn_save").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrsettings"></span>XML Formatter</h1>

<?php

	if (strlen(trim($_POST['raw_xml'])) > 0) {
		echo "<h3>Results:</h3>";
		echo "<textarea class=\"ui-corner-all\" style=\"padding: 5px; font-family: 'Consolas'; font-weight: bold; width: 90%; height: 15em; color: darkred;\">".htmlentities(formatXml(trim($_POST['raw_xml'])))."</textarea>";
	} else {
		suicide("No XML detected.");
	}
?>

<div id="addhl7form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add XML String to Format:</label><br><br></div>
	<form id="new_onboard_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&cat=<?php echo intval($cat); ?>">
		<textarea class="ui-corner-all" name="raw_xml" id="raw_xml" style="width: 70%; height: 12em;"></textarea>
		<br><br><button type="submit" name="btn_save" id="btn_save">Make Pretty</button>
	</form>
</div>
