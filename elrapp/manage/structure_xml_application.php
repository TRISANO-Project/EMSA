<script>
	$(function() {
		$("#addnew_button").button({
            icons: {
                primary: "ui-icon-elrplus"
            }
        }).click(function() {
			$("#addnew_form").show();
			$(".import_error").hide();
			$("#new_element").focus();
			$(this).hide();
		});
		
		$("#addnew_cancel").button({
			icons: {
				primary: "ui-icon-elrcancel"
			}
		}).click(function() {
			$("#addnew_form").hide();
			$("#addnew_button").show();
		});
		
		$("#new_savelab").button({
            icons: {
                primary: "ui-icon-elrsave"
            }
        });
		
		$(".edit_lab").button({
            icons: { primary: "ui-icon-elrpencil" }
        }).next().button({
            icons: { primary: "ui-icon-elrclose" }
        }).parent().buttonset();
		
		$("#confirm_delete_dialog").dialog({
			autoOpen: false,
			modal: true,
			draggable: false,
			resizable: false,
			width: 400
		});
		
		$(".delete_lab").click(function(e) {
			e.preventDefault();
			var deleteAction = "<?php echo $main_url; ?>?selected_page=6&submenu=4&cat=4&delete_id="+$(this).val();


			$("#confirm_delete_dialog").dialog('option', 'buttons', {
					"Delete" : function() {
						window.location.href = deleteAction;
						},
					"Cancel" : function() {
						$(this).dialog("close");
						}
					});

			$("#confirm_delete_dialog").dialog("open");

		});
		
		$("#edit_lab_dialog").dialog({
			autoOpen: false,
			width: 600,
			modal: true
		});
		
		$(".edit_lab").click(function(e) {
			e.preventDefault();
			var jsonObj = jQuery.parseJSON($(this).val());
			
			if (jsonObj.id) {
				$("#edit_id").val(jsonObj.id);
				$("#edit_element").val(jsonObj.element);
				$("#edit_xpath").val(jsonObj.xpath);
				$("#edit_application").val(jsonObj.app_id);
				$("#edit_category").val(jsonObj.category);
				$("#edit_lookup_operator").val(jsonObj.operator_id);
				$("#edit_master_xpath").val(jsonObj.master_path_id);
				if (jsonObj.required == "t") {
					$("#edit_required_yes").click();
				} else {
					$("#edit_required_no").click();
				}
			
				$("#edit_lab_dialog").dialog('option', 'buttons', {
						"Save Changes" : function() {
							$(this).dialog("close");
							$("#edit_modal_form").submit();
							},
						"Cancel" : function() {
							$(this).dialog("close");
							}
						});

				$("#edit_lab_dialog").dialog("open");
			} else {
				return false;
			}
		});
		
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
		
	});
</script>

<?php

	if (isset($_GET['edit_id'])) {
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path_application WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['edit_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to save changes to Application XML element.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to save changes to mapping -- Application XML element does not exist.");
		} else {
			$edit_sql = sprintf("UPDATE %sstructure_path_application SET element = %s, xpath = %s, app_id = %d, required = %s, structure_lookup_operator_id = %d, structure_path_id = %d, category_application_id = %s WHERE id = %d;",
				$my_db_schema,
				((strlen(trim($_GET['edit_element'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_element']))."'" : "NULL"),
				((strlen(trim($_GET['edit_xpath'])) > 0) ? "'".pg_escape_string(trim($_GET['edit_xpath']))."'" : "NULL"),
				((intval(trim($_GET['edit_application'])) > 0) ? intval(trim($_GET['edit_application'])) : 1),
				((trim($_GET['edit_required']) == "true") ? "true" : "false"),
				((intval(trim($_GET['edit_lookup_operator'])) > 0) ? intval(trim($_GET['edit_lookup_operator'])) : 1),
				((intval(trim($_GET['edit_master_xpath'])) > 0) ? intval(trim($_GET['edit_master_xpath'])) : -1),
				((intval(trim($_GET['edit_category'])) > 0) ? intval(trim($_GET['edit_category'])) : "NULL"),
				intval(trim($_GET['edit_id']))
			);
			if (@pg_query($host_pa, $edit_sql)) {
				highlight("Application XML element successfully updated!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to save changes to Application XML element.", 1);
			}
		}
	} elseif (isset($_GET['delete_id'])) {
		########## delete lab ##########
		
		// check to see if passed a valid row id...
		$valid_sql = sprintf("SELECT count(id) AS counter FROM %sstructure_path_application WHERE id = %s;", $my_db_schema, pg_escape_string(intval($_GET['delete_id'])));
		$valid_result = @pg_query($host_pa, $valid_sql) or suicide("Unable to delete Application XML element.", 1, 1);
		$valid_counter = @pg_fetch_result($valid_result, 0, "counter");
		if ($valid_counter != 1) {
			suicide("Unable to delete Application XML element -- record not found.");
		} else {
			// everything checks out, commit the delete...
			$delete_sql = sprintf("DELETE FROM ONLY %sstructure_path_application WHERE id = %d;", $my_db_schema, intval($_GET['delete_id']));
			if (@pg_query($host_pa, $delete_sql)) {
				highlight("Application XML element successfully deleted!", "ui-icon-elrsuccess");
			} else {
				suicide("Unable to delete Application XML element.", 1);
			}
		}
	} elseif (isset($_GET['add_flag'])) {
		// add new lab
		if (strlen(trim($_GET['new_element'])) > 0) {
			$addlab_sql = sprintf("INSERT INTO %sstructure_path_application (element, xpath, required, app_id, structure_lookup_operator_id, structure_path_id, category_application_id) VALUES (%s, %s, %s, %d, %d, %d, %s)",
				$my_db_schema,
				"'".pg_escape_string(trim($_GET['new_element']))."'",
				((strlen(trim($_GET['new_xpath'])) > 0) ? "'".pg_escape_string(trim($_GET['new_xpath']))."'" : "NULL"),
				((trim($_GET['new_required']) == "true") ? "true" : "false"),
				((intval(trim($_GET['new_application'])) > 0) ? intval(trim($_GET['new_application'])) : 1),
				((intval(trim($_GET['new_lookup_operator'])) > 0) ? intval(trim($_GET['new_lookup_operator'])) : 1),
				((intval(trim($_GET['new_master_xpath'])) > 0) ? intval(trim($_GET['new_master_xpath'])) : -1),
				((intval(trim($_GET['new_category'])) > 0) ? intval(trim($_GET['new_category'])) : "NULL")
			);
			@pg_query($host_pa, $addlab_sql) or suicide("Could not add new Application XML element.", 1);
			highlight("New Application XML element \"".htmlentities(trim($_GET['new_element']))."\" added successfully!", "ui-icon-elrsuccess");
		} else {
			suicide("No Application XML element name specified!  Enter an element name and try again.");
		}
	}

?>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrxml"></span>Application-Specific XML Structure</h1>

<div class="vocab_search ui-tabs ui-widget">
<button id="addnew_button" title="Add a new Application XML element">Add New Application XML element</button>
</div>

<div id="addnew_form" class="addnew_lab ui-widget ui-widget-content ui-corner-all">
	<div style="clear: both;"><label class="vocab_search_form">Add New Application XML element:</label><br><br></div>
	<form id="new_lab_form" method="GET" action="<?php echo $main_page; ?>">
		<label class="vocab_search_form2" for="new_application">Application:</label>
			<select class="ui-corner-all" name="new_application" id="new_application">
				<option value="0" selected>--</option>
			<?php
				// get list of data types for menu
				$apps_sql = sprintf("SELECT DISTINCT id, app_name FROM %svocab_app ORDER BY app_name;", $my_db_schema);
				$apps_rs = @pg_query($host_pa, $apps_sql) or suicide("Unable to retrieve list of Applications.", 1, 1);
				while ($apps_row = pg_fetch_object($apps_rs)) {
					printf("<option value=\"%d\">%s</option>", intval($apps_row->id), htmlentities($apps_row->app_name));
				}
				pg_free_result($apps_rs);
			?>
			</select>
		<br><br>
		<label class="vocab_search_form2" for="new_master_xpath">Map to Master XPath:</label>
			<select class="ui-corner-all" name="new_master_xpath" id="new_master_xpath">
				<option value="0" selected>--</option>
			<?php
				// get list of master xpaths for menu
				$masterpaths_sql = sprintf("SELECT DISTINCT id, xpath FROM %sstructure_path ORDER BY xpath;", $my_db_schema);
				$masterpaths_rs = @pg_query($host_pa, $masterpaths_sql) or suicide("Unable to retrieve list of Master XPaths.", 1, 1);
				while ($masterpaths_row = pg_fetch_object($masterpaths_rs)) {
					printf("<option value=\"%d\">%s</option>", intval($masterpaths_row->id), htmlentities($masterpaths_row->xpath));
				}
				pg_free_result($masterpaths_rs);
			?>
			</select>
		<br><br><label class="vocab_search_form2" for="new_element">Field Name:</label><input class="ui-corner-all" type="text" name="new_element" id="new_element" />
		<label class="vocab_search_form2" for="new_xpath">Element XPath:</label><input class="ui-corner-all" type="text" name="new_xpath" id="new_xpath" />
		<label class="vocab_search_form2" for="new_lookup_operator">Lookup Type:</label>
			<select class="ui-corner-all" name="new_lookup_operator" id="new_lookup_operator">
				<option value="0" selected>--</option>
			<?php
				// get list of lookup types for menu
				$lookup_sql = sprintf("SELECT DISTINCT id, label FROM %sstructure_lookup_operator ORDER BY label;", $my_db_schema);
				$lookup_rs = @pg_query($host_pa, $lookup_sql) or suicide("Unable to retrieve list of Lookup Types.", 1, 1);
				while ($lookup_row = pg_fetch_object($lookup_rs)) {
					printf("<option value=\"%d\">%s</option>", intval($lookup_row->id), htmlentities($lookup_row->label));
				}
				pg_free_result($lookup_rs);
			?>
			</select>
		<br><br><label class="vocab_search_form2" for="new_category">Vocab Category:</label>
			<select class="ui-corner-all" name="new_category" id="new_category">
				<option value="0" selected>--</option>
			<?php
				// get list of categories for menu
				$category_sql = sprintf("SELECT sca.id AS id, sca.app_table AS app_table, sca.app_category AS app_category, a.app_name AS app_name FROM %sstructure_category_application sca INNER JOIN %svocab_app a ON (sca.app_id = a.id) WHERE sca.app_table <> '' ORDER BY app_name, app_table, app_category;", $my_db_schema, $my_db_schema);
				$category_rs = @pg_query($host_pa, $category_sql) or suicide("Unable to retrieve list of categories.", 1, 1);
				while ($category_row = pg_fetch_object($category_rs)) {
					printf("<option value=\"%d\">[%s] %s\%s</option>", intval($category_row->id), htmlentities($category_row->app_name), htmlentities($category_row->app_table), htmlentities($category_row->app_category));
				}
				pg_free_result($category_rs);
			?>
			</select>
		<br><br><label class="vocab_search_form2">Required?:</label>
			<label class="vocab_search_form2" for="new_required_yes"><input class="edit_radio ui-corner-all" type="radio" name="new_required" id="new_required_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label class="vocab_search_form2" for="new_required_no"><input class="edit_radio ui-corner-all" type="radio" name="new_required" id="new_required_no" value="false" /> No</label>
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
		<input type="hidden" name="add_flag" value="1" />
		<br><br><button type="submit" name="new_savelab" id="new_savelab">Save New Application XML element</button>
		<button type="button" id="addnew_cancel">Cancel</button>
	</form>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	<table id="labResults">
		<thead>
			<tr>
				<th>Actions</th>
				<th>Application</th>
				<th>Field Name</th>
				<th>Master XPath</th>
				<th>Lookup Type</th>
				<th>Vocab Category</th>
				<th>Application XPath</th>
				<th>Required?</th>
			</tr>
		</thead>
		<tbody>

<?php
	
	$xml_qry = sprintf("SELECT spa.id AS id, spa.element AS element, spa.xpath AS xpath, spa.required AS required, spa.structure_lookup_operator_id AS operator_id, spa.structure_path_id AS master_path_id, spa.category_application_id AS category_id, sca.app_table AS app_table, sca.app_category AS app_category, sp.xpath AS master_path_text, o.label AS lookup_type, a.id AS app_id, a.app_name AS app_name 
		FROM %sstructure_path_application spa 
		INNER JOIN %svocab_app a ON (a.id = spa.app_id) 
		LEFT JOIN %sstructure_category_application sca ON ((sca.app_id = a.id) AND (sca.id = spa.category_application_id)) 
		LEFT JOIN %sstructure_path sp ON (spa.structure_path_id = sp.id) 
		INNER JOIN %sstructure_lookup_operator o ON (spa.structure_lookup_operator_id = o.id) 
		ORDER BY a.app_name, spa.xpath", $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema);
	$xml_rs = @pg_query($host_pa, $xml_qry) or die("Could not connect to database: ".pg_last_error());
	
	while ($xml_row = @pg_fetch_object($xml_rs)) {
		echo "<tr>";
		echo "<td nowrap class=\"action_col\">";
		unset($edit_lab_params);
		$edit_lab_params = array(
			"id" => intval($xml_row->id), 
			"element" => htmlentities($xml_row->element, ENT_QUOTES, "UTF-8"), 
			"xpath" => htmlentities($xml_row->xpath, ENT_QUOTES, "UTF-8"), 
			"required" => trim($xml_row->required), 
			"app_id" => intval($xml_row->app_id), 
			"operator_id" => intval($xml_row->operator_id), 
			"master_path_id" => intval($xml_row->master_path_id),
			"category" => intval($xml_row->category_id)
		);
		printf("<button class=\"edit_lab\" type=\"button\" value='%s' title=\"Edit this record\">Edit</button>", json_encode($edit_lab_params));
		printf("<button class=\"delete_lab\" type=\"button\" value=\"%s\" title=\"Delete this record\">Delete</button>", intval($xml_row->id));
		echo "</td>";
		echo "<td>".htmlentities($xml_row->app_name, ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities($xml_row->element, ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities(str_replace("/", " /", $xml_row->master_path_text), ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities($xml_row->lookup_type, ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities($xml_row->app_table, ENT_QUOTES, "UTF-8")."\\".htmlentities($xml_row->app_category, ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".htmlentities(str_replace("/", " /", $xml_row->xpath), ENT_QUOTES, "UTF-8")."</td>";
		echo "<td>".((trim($xml_row->required) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Required\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"Not Required\"></span>")."</td>";
		echo "</tr>";
	}
	
	pg_free_result($xml_rs);

?>

		</tbody>
	</table>
	
</div>

<div id="confirm_delete_dialog" title="Delete this Application XML element?">
	<p><span class="ui-icon ui-icon-elrerror" style="float:left; margin:0 7px 50px 0;"></span>This Application XML element will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>

<div id="edit_lab_dialog" title="Edit Application XML element">
	<form id="edit_modal_form" method="GET" action="<?php echo $main_page; ?>">
		<label for="edit_application">Application:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_application" id="edit_application">
			<option value="0" selected>--</option>
		<?php
			// get list of data types for menu
			$apps_sql = sprintf("SELECT DISTINCT id, app_name FROM %svocab_app ORDER BY app_name;", $my_db_schema);
			$apps_rs = @pg_query($host_pa, $apps_sql) or suicide("Unable to retrieve list of Applications.", 1, 1);
			while ($apps_row = pg_fetch_object($apps_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($apps_row->id), htmlentities($apps_row->app_name));
			}
			pg_free_result($apps_rs);
		?>
		</select><br><br>
		<label for="edit_master_xpath">Map to Master XPath:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_master_xpath" id="edit_master_xpath">
			<option value="0" selected>--</option>
		<?php
			// get list of master xpaths for menu
			$masterpaths_sql = sprintf("SELECT DISTINCT id, xpath FROM %sstructure_path ORDER BY xpath;", $my_db_schema);
			$masterpaths_rs = @pg_query($host_pa, $masterpaths_sql) or suicide("Unable to retrieve list of Master XPaths.", 1, 1);
			while ($masterpaths_row = pg_fetch_object($masterpaths_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($masterpaths_row->id), htmlentities($masterpaths_row->xpath));
			}
			pg_free_result($masterpaths_rs);
		?>
		</select><br><br>
		<label for="edit_element">Field Name:</label><br><input class="ui-corner-all" type="text" name="edit_element" id="edit_element" /><br><br>
		<label for="edit_xpath">Element XPath:</label><br><input class="ui-corner-all" type="text" name="edit_xpath" id="edit_xpath" /><br><br>
		<label for="edit_lookup_operator">Lookup Type:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_lookup_operator" id="edit_lookup_operator">
			<option value="0" selected>--</option>
		<?php
			// get list of lookup types for menu
			$lookup_sql = sprintf("SELECT DISTINCT id, label FROM %sstructure_lookup_operator ORDER BY label;", $my_db_schema);
			$lookup_rs = @pg_query($host_pa, $lookup_sql) or suicide("Unable to retrieve list of Lookup Types.", 1, 1);
			while ($lookup_row = pg_fetch_object($lookup_rs)) {
				printf("<option value=\"%d\">%s</option>", intval($lookup_row->id), htmlentities($lookup_row->label));
			}
			pg_free_result($lookup_rs);
		?>
		</select><br><br>
		<label for="edit_category">Vocab Category:</label><br>
		<select class="ui-corner-all" style="margin: 0px;" name="edit_category" id="edit_category">
			<option value="0" selected>--</option>
		<?php
			// get list of categories for menu
			$category_sql = sprintf("SELECT sca.id AS id, sca.app_table AS app_table, sca.app_category AS app_category, a.app_name AS app_name FROM %sstructure_category_application sca INNER JOIN %svocab_app a ON (sca.app_id = a.id) WHERE sca.app_table <> '' ORDER BY app_name, app_table, app_category;", $my_db_schema, $my_db_schema);
				$category_rs = @pg_query($host_pa, $category_sql) or suicide("Unable to retrieve list of categories.", 1, 1);
				while ($category_row = pg_fetch_object($category_rs)) {
					printf("<option value=\"%d\">[%s] %s\%s</option>", intval($category_row->id), htmlentities($category_row->app_name), htmlentities($category_row->app_table), htmlentities($category_row->app_category));
				}
				pg_free_result($category_rs);
		?>
		</select><br><br>
		<label>Required?:</label><br>
			<label for="edit_required_yes"><input class="edit_radio ui-corner-all" type="radio" name="edit_required" id="edit_required_yes" value="true" /> Yes</label>
			&nbsp;&nbsp;<label for="edit_required_no"><input class="edit_radio ui-corner-all" type="radio" name="edit_required" id="edit_required_no" value="false" /> No</label>
		<input type="hidden" name="edit_id" id="edit_id" />
		<input type="hidden" name="selected_page" value="<?php echo intval($selected_page); ?>" />
		<input type="hidden" name="submenu" value="<?php echo intval($submenu); ?>" />
		<input type="hidden" name="cat" value="<?php echo intval($cat); ?>" />
	</form>
</div>