<?php

	##### set up some vocab-specific values for later use... #####
	unset($this);
	switch (intval($_SESSION["vocab_params"]["vocab"])) {
		case 5:
			// Child LOINC
			$va_table = VocabAudit::TABLE_CHILD_LOINC;
			$this['id'] = "id";
			$this['table'] = sprintf("%svocab_child_loinc", $my_db_schema);
			$this['fields'] = array(
					array("get_field" => "edit_lab_id", "colname" => "lab_id", "validtype" => "list-required"),
					array("get_field" => "edit_archived", "colname" => "archived", "validtype" => "bool"),
					array("get_field" => "edit_child_loinc", "colname" => "child_loinc", "validtype" => "text-required"),
					array("get_field" => "edit_master_loinc", "colname" => "master_loinc", "validtype" => "list-required"),
					array("get_field" => "edit_child_orderable_test_code", "colname" => "child_orderable_test_code", "validtype" => "text"),
					array("get_field" => "edit_child_resultable_test_code", "colname" => "child_resultable_test_code", "validtype" => "text"),
					array("get_field" => "edit_child_concept_name", "colname" => "child_concept_name", "validtype" => "text"),
					array("get_field" => "edit_child_alias", "colname" => "child_alias", "validtype" => "text"),
					array("get_field" => "edit_interpret_results", "colname" => "interpret_results", "validtype" => "bool"),
					array("get_field" => "edit_semi_auto", "colname" => "semi_auto", "validtype" => "bool"),
					array("get_field" => "edit_validate_child_snomed", "colname" => "validate_child_snomed", "validtype" => "bool"),
					array("get_field" => "edit_location", "colname" => "result_location", "validtype" => "list"),
					array("get_field" => "edit_units", "colname" => "units", "validtype" => "text"),
					array("get_field" => "edit_pregnancy", "colname" => "pregnancy", "validtype" => "bool"),
					array("get_field" => "edit_refrange", "colname" => "refrange", "validtype" => "text"), 
					array("get_field" => "edit_hl7_refrange", "colname" => "hl7_refrange", "validtype" => "text")
				);
			break;
		case 4:
			// Child Organism
			$va_table = VocabAudit::TABLE_CHILD_SNOMED;
			$this['id'] = "id";
			$this['table'] = sprintf("%svocab_child_organism", $my_db_schema);
			$this['fields'] = array(
					array("get_field" => "edit_lab_id", "colname" => "lab_id", "validtype" => "list-required"),
					array("get_field" => "edit_child_code", "colname" => "child_code", "validtype" => "text-required"),
					array("get_field" => "edit_organism", "colname" => "organism", "validtype" => "list"),
					array("get_field" => "edit_test_result_id", "colname" => "test_result_id", "validtype" => "list"),
					array("get_field" => "edit_result_value", "colname" => "result_value", "validtype" => "text"),
					array("get_field" => "edit_comment", "colname" => "comment", "validtype" => "text")
				);
			break;
		case 3:
			// Master Organism
			$va_table = VocabAudit::TABLE_MASTER_SNOMED;
			$this['id'] = "o_id";
			$this['table'] = sprintf("%svocab_master_organism", $my_db_schema);
			$this['fields'] = array(
					array("get_field" => "edit_snomed_category", "colname" => "snomed_category", "validtype" => "list-required"),
					array("get_field" => "edit_condition", "colname" => "condition", "validtype" => "list"),
					array("get_field" => "edit_snomed", "colname" => "snomed", "validtype" => "text"),
					array("get_field" => "edit_snomed_alt", "colname" => "snomed_alt", "validtype" => "text"),
					array("get_field" => "edit_organism", "colname" => "organism", "validtype" => "list"),
					array("get_field" => "edit_list", "colname" => "list", "validtype" => "list"),
					array("get_field" => "edit_test_result", "colname" => "test_result", "validtype" => "list"), 
					array("get_field" => "edit_status", "colname" => "status", "validtype" => "list"), 
					array("get_field" => "edit_nom_is_surveillance", "colname" => "nom_is_surveillance", "validtype" => "bool")
				);
			break;
		case 2:
			// Master Condition
			$va_table = VocabAudit::TABLE_MASTER_CONDITION;
			$this['id'] = "c_id";
			$this['table'] = sprintf("%svocab_master_condition", $my_db_schema);
			$this['fields'] = array(
					array("get_field" => "edit_condition", "colname" => "condition", "validtype" => "list-required"),
					array("get_field" => "edit_disease_category", "colname" => "disease_category", "validtype" => "list-required"),
					array("get_field" => "edit_gateways", "colname" => "gateway_xref", "validtype" => "multi"),
					array("get_field" => "edit_check_xref_first", "colname" => "check_xref_first", "validtype" => "bool"),
					array("get_field" => "edit_valid_specimen", "colname" => "valid_specimen", "validtype" => "multi"),
					array("get_field" => "edit_invalid_specimen", "colname" => "invalid_specimen", "validtype" => "multi"),
					array("get_field" => "edit_white_rule", "colname" => "white_rule", "validtype" => "text"),
					array("get_field" => "edit_contact_white_rule", "colname" => "contact_white_rule", "validtype" => "text"),
					array("get_field" => "edit_immediate_notify", "colname" => "immediate_notify", "validtype" => "bool"),
					array("get_field" => "edit_require_specimen", "colname" => "require_specimen", "validtype" => "bool"),
					array("get_field" => "edit_new_event", "colname" => "new_event", "validtype" => "bool"),
					array("get_field" => "edit_notify_state", "colname" => "notify_state", "validtype" => "bool"),
					array("get_field" => "edit_autoapproval", "colname" => "autoapproval", "validtype" => "bool"), 
					array("get_field" => "edit_district_override", "colname" => "district_override", "validtype" => "list")
				);
			break;
		default:
			// Master LOINC
			$va_table = VocabAudit::TABLE_MASTER_LOINC;
			$this['id'] = "l_id";
			$this['table'] = sprintf("%svocab_master_loinc", $my_db_schema);
			$this['fields'] = array(
					array("get_field" => "edit_loinc", "colname" => "loinc", "validtype" => "text-required"),
					array("get_field" => "edit_concept_name", "colname" => "concept_name", "validtype" => "text"),
					array("get_field" => "edit_condition_from_result", "colname" => "condition_from_result", "validtype" => "bool"),
					array("get_field" => "edit_trisano_condition", "colname" => "trisano_condition", "validtype" => "list"),
					array("get_field" => "edit_organism_from_result", "colname" => "organism_from_result", "validtype" => "bool"),
					array("get_field" => "edit_trisano_organism", "colname" => "trisano_organism", "validtype" => "list"),
					array("get_field" => "edit_trisano_test_type", "colname" => "trisano_test_type", "validtype" => "list-required"),
					array("get_field" => "edit_specimen_source", "colname" => "specimen_source", "validtype" => "list"),
					array("get_field" => "edit_list", "colname" => "list", "validtype" => "list"),
					array("get_field" => "edit_gray_rule", "colname" => "gray_rule", "validtype" => "text")
				);
			break;
	}
	
	##### verify we've got a valid ID to edit #####
	unset($vocab_id);
	if (is_numeric($_GET['edit_id']) && (intval(trim($_GET['edit_id'])) > 0)) {
		$validedit_sql = sprintf("SELECT count(%s) AS id FROM %s WHERE %s = %d", $this['id'], $this['table'], $this['id'], intval(trim($_GET['edit_id'])));
		$validedit_count = @pg_fetch_result(@pg_query($host_pa, $validedit_sql), 0, id);
		if ($validedit_count == 1) {
			$vocab_id = intval(trim($_GET['edit_id']));
		}
	}
	
	if (!isset($vocab_id)) {
		suicide("Cannot edit vocabulary:  Record not found", -1, 1);
	}
	
	if (isset($_POST['save_flag'])) {
		$va_prev_vals = $va->getPreviousVals($vocab_id, $va_table);
		$va_new_vals = array();
		foreach ($this['fields'] as $va_new_field) {
			$va_new_vals[$va_new_field['colname']] = $_POST[$va_new_field['get_field']];
		}
		$va_prepared_new_vals = $va->prepareNewValues($va_table, $va_new_vals);
		
		##### save changes #####
		$changes_saved = TRUE;
		// verify all fields passed are valid types
		foreach ($this['fields'] as $new_field) {
			switch ($new_field['validtype']) {
				case "list-required":
					if (isset($_POST[$new_field['get_field']]) && is_numeric(trim($_POST[$new_field['get_field']])) && (intval(trim($_POST[$new_field['get_field']])) > 0)):
						$changes_saved = $changes_saved && TRUE;
					else:
						$changes_saved = $changes_saved && FALSE;
                    endif;
					break;
				case "list":
					if (isset($_POST[$new_field['get_field']]) && is_numeric(trim($_POST[$new_field['get_field']]))):
						$changes_saved = $changes_saved && TRUE;
					else:
						$changes_saved = $changes_saved && FALSE;
					endif;
					break;
				case "text":
					if (isset($_POST[$new_field['get_field']])):
						$changes_saved = $changes_saved && TRUE;
					else:
						$changes_saved = $changes_saved && FALSE;
					endif;
					break;
				case "text-required":
					if (isset($_POST[$new_field['get_field']]) && ctype_print(trim($_POST[$new_field['get_field']])) && (strlen(trim($_POST[$new_field['get_field']])) > 0)):
						$changes_saved = $changes_saved && TRUE;
					else:
						$changes_saved = $changes_saved && FALSE;
					endif;
					break;
				case "multi":
					$changes_saved = $changes_saved && TRUE;
					break;
				case "bool":
					if (isset($_POST[$new_field['get_field']]) && ctype_lower(trim($_POST[$new_field['get_field']])) && ((trim($_POST[$new_field['get_field']]) == "t") || (trim($_POST[$new_field['get_field']]) == "f"))):
						$changes_saved = $changes_saved && TRUE;
					else:
						$changes_saved = $changes_saved && FALSE;
					endif;
					break;
			}
		}
		
		if ($changes_saved) {
			$edit_sql = sprintf("UPDATE %s SET ", $this['table']);
			foreach ($this['fields'] as $new_fieldval) {
				$edit_sql .= $new_fieldval['colname'] . " = ";
				switch ($new_fieldval['validtype']) {
					case "list":
					case "list-required":
						$edit_sql .= intval($_POST[$new_fieldval['get_field']]) . ", ";
						break;
					case "text":
					case "text-required":
						$edit_sql .= ((strlen(trim($_POST[$new_fieldval['get_field']])) > 0) ? "'".pg_escape_string(trim($_POST[$new_fieldval['get_field']]))."'" : "NULL") . ", ";
						break;
					case "bool":
						$edit_sql .= ((trim($_POST[$new_fieldval['get_field']]) == "t") ? "'t'" : "'f'") . ", ";
						break;
					case "multi":
						unset($multi_string);
						if (is_array($_POST[$new_fieldval['get_field']]) && (count($_POST[$new_fieldval['get_field']]) > 0)) {
							$multi_string = implode(";", $_POST[$new_fieldval['get_field']]);
						}
						$edit_sql .= ((strlen(trim($multi_string)) > 0) ? "'".pg_escape_string(trim($multi_string))."'" : "NULL") . ", ";
						break;
				}
			}
			$edit_sql = substr($edit_sql, 0, -2);
			$edit_sql .= sprintf(" WHERE %s = %d;", $this['id'], $vocab_id);
			
			if (@pg_query($host_pa, $edit_sql)) {
				highlight(sprintf("%s updated successfully!", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]), "ui-icon-elrsuccess");
				$va->resetAudit();
				$va->setOldVals($va_prev_vals);
				$va->setNewVals($va_prepared_new_vals);
				$va->auditVocab($vocab_id, $va_table, VocabAudit::ACTION_EDIT);
			} else {
				suicide(sprintf("Could not save changes to %s.", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]), 1);
			}
		} else {
			suicide(sprintf("XX 2 Could not save changes to %s:  Some values were missing/invalid", $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]));
		}
		
	} else {
		##### draw edit form #####
	?>
		<script>
			var unsavedChanges = false;
			window.onbeforeunload = function(e) {
				if (unsavedChanges) {
					return 'Changes to this vocabulary item not saved!';
				}
			};
			
			$(function() {
				$(".vocab_filter_selectall").click(function() {
					var thisFilter = $(this).attr("rel");
					$("div.addnew_lookup_checklist[rel='"+thisFilter+"']").find($(":input")).each(function() {
						if (!$(this).is(':checked')) {
							$(this).trigger('click');
						}
					});
				});
				
				$(".vocab_filter_selectnone").click(function() {
					var thisFilter = $(this).attr("rel");
					$("div.addnew_lookup_checklist[rel='"+thisFilter+"']").find($(":input")).each(function() {
						if ($(this).is(':checked')) {
							$(this).trigger('click');
						}
					});
				});
				
				$("#edit_vocab_form input").change(function() {
					unsavedChanges = true;
				});
				
				$("#edit_vocab_form select").change(function() {
					unsavedChanges = true;
				});
				
				$("#edit_vocab_form textarea").change(function() {
					unsavedChanges = true;
				});
				
				$("#edit_cancel").button({
					icons: {
						primary: "ui-icon-elrcancel"
					}
				}).click(function(e) {
					e.preventDefault();
					var cancelAction = "<?php echo $main_url; ?>?selected_page=6&submenu=3&vocab=<?php echo intval($_SESSION["vocab_params"]["vocab"]); ?>&focus_id=<?php echo $vocab_id; ?>";
					window.location.href = cancelAction;
				});
				
				$("#edit_savevocab").button({
					icons: {
						primary: "ui-icon-elrsave"
					}
				}).click(function() {
					unsavedChanges = false;
					return true;
				});
				
				$(".pseudo_select").change(function() {
					$(this).closest('label').toggleClass('pseudo_select_on');
				});
				
				$("#edit_vocab_form").submit(function(e) {
					var incomplete_fields = 0;
					$(":input.required", this).each(function() {
						if ($(this).val() == "" || $(this).val() == -1) {
							alert($('label[for="'+$(this).attr('id')+'"]').text()+' requires a value');
							incomplete_fields++;
						}
					});
					if (incomplete_fields > 0) {
						return false;
					} else {
						return true;
					}
				});
				
			<?php
				$editvals_sql = "SELECT ";
				foreach ($this['fields'] as $edit_field) {
					$editvals_sql .= $edit_field['colname'] . ", ";
				}
				$editvals_sql = substr($editvals_sql, 0, -2);
				$editvals_sql .= sprintf(" FROM %s WHERE %s = %d", $this['table'], $this['id'], $vocab_id);
				$editvals_row = @pg_fetch_object(@pg_query($host_pa, $editvals_sql));
				
				foreach ($this['fields'] as $load_field) {
					if ($load_field['validtype'] == "multi") {
						$multi_vals = explode(";", $editvals_row->$load_field['colname']);
						if (is_array($multi_vals) && (count($multi_vals) > 0)) {
							foreach ($multi_vals as $multi_val) {
								printf("$(\"#%s_%d\").prop(\"checked\", !$(this).prop(\"checked\"));\n", $load_field['get_field'], $multi_val);
								printf("$(\"#%s_%d\").closest(\"label\").addClass(\"pseudo_select_on\");\n", $load_field['get_field'], $multi_val);
							}
						}
					} else {
						printf("$(\"#%s\").val(%s);\n", $load_field['get_field'], json_encode($editvals_row->$load_field['colname']));
						printf("$(\"input:text#%s\").width('%dem');\n", $load_field['get_field'], (0.6*strlen($editvals_row->$load_field['colname'])));
					}
				}
				
			?>
			});
		</script>
		
		<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrglossary"></span><?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?> Editor</h1>
		
		<div id="edit_form" class="edit_vocab_form ui-widget ui-widget-content ui-corner-all">
		<div style="clear: both;"><label class="vocab_search_form">Edit <?php echo $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["vocab_verbose"]; ?>:</label><br><br></div>
			<form id="edit_vocab_form" method="POST" action="<?php echo $main_page; ?>?selected_page=<?php echo intval($selected_page); ?>&submenu=<?php echo intval($submenu); ?>&vocab=<?php echo intval($vocab); ?>&edit_id=<?php echo intval($vocab_id); ?>">
			
				<?php
					if (intval($_SESSION["vocab_params"]["vocab"]) == 5) {
						// Child LOINC
						#lab_id lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_lab_id\">Child Lab</label><br><select class=\"ui-corner-all required\" name=\"edit_lab_id\" id=\"edit_lab_id\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["lab"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#child_loinc text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_child_loinc\">Child Test Concept Code</label><br><input class=\"ui-corner-all required\" type=\"text\" name=\"edit_child_loinc\" id=\"edit_child_loinc\" /></div>";
						#archived y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_archived\">Archived?</label><br><select class=\"ui-corner-all\" name=\"edit_archived\" id=\"edit_archived\">\n";
						echo "<option value=\"-1\">--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\" selected>No</option>\n";
						echo "</select></div>";
						#master_loinc lookup (incl master concept name)
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_master_loinc\">Master Test Concept Code (LOINC)</label><br><select class=\"ui-corner-all required\" name=\"edit_master_loinc\" id=\"edit_master_loinc\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = sprintf("SELECT vml.l_id AS value, vml.loinc AS label, vml.concept_name AS concept FROM %svocab_master_loinc vml ORDER BY vml.loinc;", $my_db_schema);
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s - (%s)</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"), htmlentities($lookup_row->concept, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
						#interpret_results y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_interpret_results\">Quantitative Result?</label><br><select class=\"ui-corner-all\" name=\"edit_interpret_results\" id=\"edit_interpret_results\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#semi_auto y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_semi_auto\">Semi-Automated Entry?</label><br><select class=\"ui-corner-all\" name=\"edit_semi_auto\" id=\"edit_semi_auto\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#validate_child_snomed y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_validate_child_snomed\">Validate Child SNOMED?</label><br><select class=\"ui-corner-all\" name=\"edit_validate_child_snomed\" id=\"edit_validate_child_snomed\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#result location lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_location\">Result Location</label><br><select class=\"ui-corner-all\" name=\"edit_location\" id=\"edit_location\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["result_location"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list Result Locations.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#units text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_units\">Units</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_units\" id=\"edit_units\" /></div>";
						#refrange text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_refrange\">Reference Range</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_refrange\" id=\"edit_refrange\" /></div>";
						#hl7_refrange text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_hl7_refrange\">HL7 Reference Range</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_hl7_refrange\" id=\"edit_hl7_refrange\" /></div>";
						#pregnancy y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_pregnancy\">Indicates Pregnancy?</label><br><select class=\"ui-corner-all\" name=\"edit_pregnancy\" id=\"edit_pregnancy\">\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\" selected>No</option>\n";
						echo "</select></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
						#--extended--
						echo "<div style=\"clear: both; margin-bottom: 10px;\"><label class=\"vocab_search_form\">Extended Child LOINC Fields</label></div>";
						#child_orderable_test_code text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_child_orderable_test_code\">Child Orderable Test Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_child_orderable_test_code\" id=\"edit_child_orderable_test_code\" /></div>";
						#child_resultable_test_code text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_child_resultable_test_code\">Child Resultable Test Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_child_resultable_test_code\" id=\"edit_child_resultable_test_code\" /></div>";
						#child_concept_name text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_child_concept_name\">Child Concept Name</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_child_concept_name\" id=\"edit_child_concept_name\" /></div>";
						#child_alias text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_child_alias\">Alias</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_child_alias\" id=\"edit_child_alias\" /></div>";
						
					} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 4) {
						// Child Organism
						#lab_id lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_lab_id\">Child Lab</label><br><select class=\"ui-corner-all required\" name=\"edit_lab_id\" id=\"edit_lab_id\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["lab"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#child_code text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_child_code\">Child Code</label><br><input class=\"ui-corner-all required\" type=\"text\" name=\"edit_child_code\" id=\"edit_child_code\" /></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
						#organism lookup (incl master snomed)
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_organism\">Master Organism SNOMED</label><br><select class=\"ui-corner-all\" name=\"edit_organism\" id=\"edit_organism\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = 'SELECT vmo.o_id AS value, vmo.snomed AS snomed, mv.concept AS label 
								FROM '.$my_db_schema.'vocab_master_organism vmo 
								LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (vmo.organism = mv.id) 
								INNER JOIN '.$my_db_schema.'vocab_master_vocab mv2 ON (vmo.snomed_category = mv2.id) 
								WHERE mv2.category = '.$my_db_schema.'vocab_category_id(\'snomed_category\') AND mv2.concept = \'Organism\'
								ORDER BY mv.concept, vmo.snomed;';
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of organisms.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s [SNOMED %s]</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"), htmlentities($lookup_row->snomed, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						echo "<div class=\"add-form-divider\"></div>";
						
						#test_result_id lookup (incl master snomed)
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_test_result_id\">Master Test Result SNOMED</label><br><select class=\"ui-corner-all\" name=\"edit_test_result_id\" id=\"edit_test_result_id\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = 'SELECT vmo.o_id AS value, vmo.snomed AS snomed, mv.concept AS label 
								FROM '.$my_db_schema.'vocab_master_organism vmo 
								LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (vmo.organism = mv.id) 
								INNER JOIN '.$my_db_schema.'vocab_master_vocab mv2 ON (vmo.snomed_category = mv2.id) 
								WHERE mv2.category = '.$my_db_schema.'vocab_category_id(\'snomed_category\') AND mv2.concept = \'Test Result\'
								ORDER BY mv.concept, vmo.snomed;';
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of organisms.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s [SNOMED %s]</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"), htmlentities($lookup_row->snomed, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						echo "<div class=\"add-form-divider\"></div>";
						
						#result_value text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_result_value\">Result Value</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_result_value\" id=\"edit_result_value\" /></div>";
						#comment text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_comment\">Comments</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_comment\" id=\"edit_comment\" /></div>";
						
					} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 3) {
						// Master Organism
						#snomed_category lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_snomed_category\">SNOMED Type</label><br><select class=\"ui-corner-all required\" name=\"edit_snomed_category\" id=\"edit_snomed_category\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = sprintf("SELECT vm.id AS value, m2a.coded_value AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('snomed_category') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY m2a.coded_value;", $my_db_schema, $my_db_schema, $app_id);
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of disease categories.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#snomed text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_snomed\">SNOMED Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_snomed\" id=\"edit_snomed\" /></div>";
						#snomed_alt text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_snomed_alt\">Secondary SNOMED Code</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_snomed_alt\" id=\"edit_snomed_alt\" /></div>";
						echo "<div class=\"add-form-divider\"></div>";
						#condition lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_condition\">Master Condition</label><br><select class=\"ui-corner-all\" name=\"edit_condition\" id=\"edit_condition\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["condition"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#organism lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_organism\">Type Concept Name</label><br><select class=\"ui-corner-all\" name=\"edit_organism\" id=\"edit_organism\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = sprintf("SELECT vm.id AS value, vm.concept AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('organism') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY vm.concept;", $my_db_schema, $my_db_schema, $app_id);
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						echo "<div class=\"add-form-divider\"></div>";
						
						#list lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_list\">List</label><br><select class=\"ui-corner-all\" name=\"edit_list\" id=\"edit_list\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["list"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of lists.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#test_result lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_test_result\">Test Result</label><br><select class=\"ui-corner-all\" name=\"edit_test_result\" id=\"edit_test_result\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["test_result"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of test results.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#status lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_status\">Set State Case Status To...</label><br><select class=\"ui-corner-all\" name=\"edit_status\" id=\"edit_status\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["status"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#nom_is_surveillance y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_nom_is_surveillance\">Nominal Surveillance?</label><br><select class=\"ui-corner-all required\" name=\"edit_nom_is_surveillance\" id=\"edit_nom_is_surveillance\">\n";
						echo "<option value=\"-1\">--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\" selected>No</option>\n";
						echo "</select></div>";
						
					} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 2) {
						// Master Condition
						#disease_category lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_disease_category\">Disease Category</label><br><select class=\"ui-corner-all required\" name=\"edit_disease_category\" id=\"edit_disease_category\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = sprintf("SELECT vm.id AS value, m2a.coded_value AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('disease_category') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY m2a.coded_value;", $my_db_schema, $my_db_schema, $app_id);
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of disease categories.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#condition lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_condition\">Condition</label><br><select class=\"ui-corner-all required\" name=\"edit_condition\" id=\"edit_condition\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = sprintf("SELECT vm.id AS value, m2a.coded_value AS label FROM %svocab_master_vocab vm JOIN %svocab_master2app m2a ON (vm.category = elr.vocab_category_id('condition') AND m2a.app_id = %d AND vm.id = m2a.master_id) ORDER BY m2a.coded_value;", $my_db_schema, $my_db_schema, $app_id);
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
						#valid_specimen multi
						echo "<div class=\"addnew_field\">";
						echo "<div class=\"addnew_lookup_container\"><label class=\"vocab_add_form\">Valid Specimen Sources</label> <span rel=\"f_validspecimen\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_validspecimen\" class=\"vocab_filter_selectnone\">None</span><br>";
						echo "<div class=\"addnew_lookup_checklist ui-corner-all\" rel=\"f_validspecimen\">";
						$specimen_qry = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') ORDER BY concept;", $my_db_schema);
						$specimen_rs = @pg_query($host_pa, $specimen_qry);
						while ($specimen_row = @pg_fetch_object($specimen_rs)) {
							echo "<label class=\"pseudo_select_label\" for=\"edit_valid_specimen_".$specimen_row->id."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"edit_valid_specimen[]\" id=\"edit_valid_specimen_".$specimen_row->id."\" value=\"".htmlentities($specimen_row->id, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
						@pg_free_result($specimen_rs);
						echo "</div></div></div>";
						#invalid_specimen multi
						echo "<div class=\"addnew_field\">";
						echo "<div class=\"addnew_lookup_container\"><label class=\"vocab_add_form\">Invalid Specimen Sources</label> <span rel=\"f_invalidspecimen\" class=\"vocab_filter_selectall\">Select All</span> <span class=\"vocab_filter_select\">|</span> <span rel=\"f_invalidspecimen\" class=\"vocab_filter_selectnone\">None</span><br>";
						echo "<div class=\"addnew_lookup_checklist ui-corner-all\" rel=\"f_invalidspecimen\">";
						$specimen_qry = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') ORDER BY concept;", $my_db_schema);
						$specimen_rs = @pg_query($host_pa, $specimen_qry);
						while ($specimen_row = @pg_fetch_object($specimen_rs)) {
							echo "<label class=\"pseudo_select_label\" for=\"edit_invalid_specimen_".$specimen_row->id."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"edit_invalid_specimen[]\" id=\"edit_invalid_specimen_".$specimen_row->id."\" value=\"".htmlentities($specimen_row->id, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($specimen_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
						@pg_free_result($specimen_rs);
						echo "</div></div></div>";
						
						#white_rule text
						echo "<div class=\"addnew_field\" style=\"vertical-align: top;\"><label class=\"vocab_add_form\" for=\"edit_white_rule\">Morbidity Whitelist Rules</label><br><textarea class=\"ui-corner-all\" name=\"edit_white_rule\" id=\"edit_white_rule\"></textarea></div>";
						#contact_white_rule text
						echo "<div class=\"addnew_field\" style=\"vertical-align: top;\"><label class=\"vocab_add_form\" for=\"edit_contact_white_rule\">Contact Whitelist Rules</label><br><textarea class=\"ui-corner-all\" name=\"edit_contact_white_rule\" id=\"edit_contact_white_rule\"></textarea></div>";
						#gateway_xref text
						echo "<div class=\"addnew_field\">";
						echo "<div class=\"addnew_lookup_container\"><label class=\"vocab_add_form\">Gateway Crossrefs</label><br>";
						echo "<div class=\"addnew_lookup_checklist ui-corner-all\">";
						$gateway_xref_qry = sprintf("SELECT id, concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('gateway_xref') ORDER BY concept;", $my_db_schema);
						$gateway_rs = @pg_query($host_pa, $gateway_xref_qry);
						while ($gateway_row = @pg_fetch_object($gateway_rs)) {
							echo "<label class=\"pseudo_select_label\" for=\"edit_gateways_".$gateway_row->id."\"><input class=\"pseudo_select\" type=\"checkbox\" name=\"edit_gateways[]\" id=\"edit_gateways_".$gateway_row->id."\" value=\"".htmlentities($gateway_row->id, ENT_QUOTES, "UTF-8")."\">&nbsp;" . ((is_null(htmlentities($gateway_row->concept, ENT_QUOTES, "UTF-8")) || (strlen(trim(htmlentities($gateway_row->concept, ENT_QUOTES, "UTF-8"))) == 0)) ? "&lt;Blank&gt;" : htmlentities($gateway_row->concept, ENT_QUOTES, "UTF-8")) . "</label><br>";
						}
						@pg_free_result($gateway_rs);
						echo "</div></div></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
					#check_xref_first y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_check_xref_first\">Check Crossrefs First?</label><br><select class=\"ui-corner-all\" name=\"edit_check_xref_first\" id=\"edit_check_xref_first\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#immediate_notify y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_immediate_notify\">Immediately Notifiable?</label><br><select class=\"ui-corner-all required\" name=\"edit_immediate_notify\" id=\"edit_immediate_notify\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#require_specimen y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_require_specimen\">Require Specimen Source<br>From Nominal Culture?</label><br><select class=\"ui-corner-all required\" name=\"edit_require_specimen\" id=\"edit_require_specimen\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#new_event y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_new_event\">Different Species/Toxin<br>Same Event or New Event?</label><br><select class=\"ui-corner-all required\" name=\"edit_new_event\" id=\"edit_new_event\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">New</option>\n";
						echo "<option value=\"f\">Same</option>\n";
						echo "</select></div>";
						#notify_state y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_notify_state\">Notify State Upon Receipt?</label><br><select class=\"ui-corner-all required\" name=\"edit_notify_state\" id=\"edit_notify_state\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#autoapproval y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_autoapproval\">Autoapproval?</label><br><select class=\"ui-corner-all required\" name=\"edit_autoapproval\" id=\"edit_autoapproval\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
						echo "<option value=\"t\">Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#district_override lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_district_override\">Jurisdiction Override</label><br><select class=\"ui-corner-all\" name=\"edit_district_override\" id=\"edit_district_override\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["district_override"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of jurisdictions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						
					} else {
						// Master LOINC
						#loinc text
						echo "<div class=\"addnew_field\" style=\"vertical-align: top;\"><label class=\"vocab_add_form required\" for=\"edit_loinc\">Test Concept Code (LOINC)</label><br><input class=\"ui-corner-all required\" type=\"text\" name=\"edit_loinc\" id=\"edit_loinc\" /></div>";
						#concept_name text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_concept_name\">Preferred Concept Name</label><br><textarea class=\"ui-corner-all\" name=\"edit_concept_name\" id=\"edit_concept_name\"></textarea></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
						#condition_from_result y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_condition_from_result\">Look Up Condition?</label><br><select class=\"ui-corner-all\" name=\"edit_condition_from_result\" id=\"edit_condition_from_result\">\n";
						echo "<option value=\"t\" selected>Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#trisano_condition lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_trisano_condition\">Master Condition</label><br><select class=\"ui-corner-all\" name=\"edit_trisano_condition\" id=\"edit_trisano_condition\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["trisano_condition"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of conditions.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
						#organism_from_result y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_organism_from_result\">Look Up Organism?</label><br><select class=\"ui-corner-all\" name=\"edit_organism_from_result\" id=\"edit_organism_from_result\">\n";
						echo "<option value=\"t\" selected>Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						#trisano_organism lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_trisano_organism\">Master Organism</label><br><select class=\"ui-corner-all\" name=\"edit_trisano_organism\" id=\"edit_trisano_organism\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["trisano_organism"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of organisms.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						
						echo "<div class=\"add-form-divider\"></div>";
						#trisano_test_type lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form required\" for=\"edit_trisano_test_type\">Test Type</label><br><select class=\"ui-corner-all required\" name=\"edit_trisano_test_type\" id=\"edit_trisano_test_type\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["trisano_test_type"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of test types.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#specimen_source lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_specimen_source\">Specimen Source</label><br><select class=\"ui-corner-all\" name=\"edit_specimen_source\" id=\"edit_specimen_source\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["specimen_source"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of specimen sources.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						#list lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_list\">List</label><br><select class=\"ui-corner-all\" name=\"edit_list\" id=\"edit_list\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["list"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of lists.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						/*
						#allow_new_cmr y/n
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_allow_new_cmr\">Create New CMRs?</label><br><select class=\"ui-corner-all\" name=\"edit_allow_new_cmr\" id=\"edit_allow_new_cmr\">\n";
						echo "<option value=\"t\" selected>Yes</option>\n";
						echo "<option value=\"f\">No</option>\n";
						echo "</select></div>";
						*/
						#gray_rule text
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_gray_rule\">Graylist Rules</label><br><input class=\"ui-corner-all\" type=\"text\" name=\"edit_gray_rule\" id=\"edit_gray_rule\" /></div>";
						/*
						#status lookup
						echo "<div class=\"addnew_field\"><label class=\"vocab_add_form\" for=\"edit_status\">Set State Case Status To...</label><br><select class=\"ui-corner-all\" name=\"edit_status\" id=\"edit_status\">\n";
						echo "<option value=\"-1\" selected>--</option>\n";
							$lookup_qry = $_SESSION["vocab_params"][$_SESSION["vocab_params"]["vocab"]]["result_cols"]["status"]["lookupqry"];
							$lookup_rs = @pg_query($host_pa, $lookup_qry) or suicide("Unable to retrieve list of statuses.", 1);
							while ($lookup_row = @pg_fetch_object($lookup_rs)) {
								echo sprintf("<option value=\"%d\">%s</option>\n", $lookup_row->value, htmlentities($lookup_row->label, ENT_QUOTES, "UTF-8"));
							}
							pg_free_result($lookup_rs);
						echo "</select></div>";
						*/
						
					}
				?>
			
				<input type="hidden" name="save_flag" value="1" />
				<br><br><button type="submit" name="edit_savevocab" id="edit_savevocab">Save Changes</button>
				<button type="button" name="edit_cancel" id="edit_cancel">Cancel</button>
			</form>
		</div>
		
	<?php
		########## Show dependencies/child items for selected vocabulary item ##########
		if (intval($_SESSION["vocab_params"]["vocab"]) < 4) {
	?>
		<div id="dependencies" class="edit_vocab_form ui-widget ui-widget-content ui-corner-all">
		<?php
			if (intval($_SESSION["vocab_params"]["vocab"]) == 1) {
				// Master LOINC dependents
		?>
				<div style="clear: both;"><label class="vocab_search_form">Dependent Child LOINCs...</label></div>
					<table id="labResults">
						<thead>
							<tr><th>Child Lab</th><th>Child LOINC</th></tr>
						</thead>
						<tbody>
		<?php
				$dependent_sql = sprintf("SELECT vcl.id AS id, sl.ui_name AS lab_name, vcl.child_loinc AS child_loinc 
					FROM %svocab_child_loinc vcl JOIN %sstructure_labs sl ON (vcl.lab_id = sl.id)
					WHERE vcl.master_loinc = %d
					ORDER BY sl.ui_name, vcl.child_loinc",
					$my_db_schema, $my_db_schema, $vocab_id);
				$dependent_rs = @pg_query($host_pa, $dependent_sql) or suicide("Unable to retrieve Child LOINCs.", 1);
				if (@pg_num_rows($dependent_rs) < 1) {
					echo "<tr><td colspan=\"4\"><em>No records found</em></td></tr>";
				}
				while ($dependent_row = @pg_fetch_object($dependent_rs)) {
					printf("<tr><td>%s</td><td><a class=\"vocab_xref\" title=\"View/Edit\" href=\"%s\">%s</a></td></tr>",
						$dependent_row->lab_name,
						($main_url . sprintf("?selected_page=%s&submenu=%s&vocab=%d&edit_id=%s", $selected_page, $submenu, 5, $dependent_row->id)),
						$dependent_row->child_loinc);
				}
				@pg_free_result($dependent_rs);
				echo "<tbody></table>";
				
			} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 2) {
				// Master Condition dependents
		?>
				<div style="clear: both;"><label class="vocab_search_form">Dependent Master LOINCs...</label></div>
					<table id="labResults">
						<tbody>
		<?php
				$dependent_sql = sprintf("SELECT vml.l_id AS id, vml.loinc AS loinc, vml.concept_name AS concept_name 
					FROM %svocab_master_loinc vml 
					WHERE vml.trisano_condition = %d ORDER BY vml.concept_name;",
					$my_db_schema, $vocab_id);
				$dependent_rs = @pg_query($host_pa, $dependent_sql) or suicide("Unable to retrieve dependent Master LOINCs.", 1);
				if (@pg_num_rows($dependent_rs) < 1) {
					echo "<tr><td><em>No records found</em></td></tr>";
				}
				while ($dependent_row = @pg_fetch_object($dependent_rs)) {
					printf("<tr><td><a class=\"vocab_xref\" title=\"View/Edit\" href=\"%s\">%s (LOINC %s)</a></td></tr>",
						($main_url . sprintf("?selected_page=%s&submenu=%s&vocab=%d&edit_id=%s", $selected_page, $submenu, 1, $dependent_row->id)),
						$dependent_row->concept_name,
						$dependent_row->loinc);
				}
				@pg_free_result($dependent_rs);
				echo "<tbody></table><br>";
		?>
				<div style="clear: both;"><label class="vocab_search_form">Dependent Master Organisms...</label></div>
					<table id="labResults">
						<tbody>
		<?php
				$dependent_sql = sprintf("SELECT vmo.o_id AS id, m2a.coded_value AS concept, vmo.snomed AS snomed 
					FROM %svocab_master_organism vmo
					LEFT JOIN %svocab_master_vocab mv ON (vmo.organism = mv.id)
					LEFT JOIN %svocab_master2app m2a ON (mv.id = m2a.master_id AND m2a.app_id = %d)
					WHERE vmo.condition = %d ORDER BY m2a.coded_value, vmo.snomed;",
					$my_db_schema, $my_db_schema, $my_db_schema, $app_id, $vocab_id);
				$dependent_rs = @pg_query($host_pa, $dependent_sql) or suicide("Unable to retrieve dependent Master Organisms.", 1);
				if (@pg_num_rows($dependent_rs) < 1) {
					echo "<tr><td><em>No records found</em></td></tr>";
				}
				while ($dependent_row = @pg_fetch_object($dependent_rs)) {
					printf("<tr><td><a class=\"vocab_xref\" title=\"View/Edit\" href=\"%s\">%s (SNOMED %s)</a></td></tr>",
						($main_url . sprintf("?selected_page=%s&submenu=%s&vocab=%d&edit_id=%s", $selected_page, $submenu, 3, $dependent_row->id)),
						$dependent_row->concept,
						$dependent_row->snomed);
				}
				@pg_free_result($dependent_rs);
				echo "<tbody></table>";
		
			} elseif (intval($_SESSION["vocab_params"]["vocab"]) == 3) {
				// Master Organism dependents
		?>
				<div style="clear: both;"><label class="vocab_search_form">Dependent Master LOINCs...</label></div>
					<table id="labResults">
						<tbody>
		<?php
				$dependent_sql = sprintf("SELECT vml.l_id AS id, vml.loinc AS loinc, vml.concept_name AS concept_name 
					FROM %svocab_master_loinc vml 
					WHERE vml.trisano_organism = %d ORDER BY vml.concept_name;",
					$my_db_schema, $vocab_id);
				$dependent_rs = @pg_query($host_pa, $dependent_sql) or suicide("Unable to retrieve dependent Master LOINCs.", 1);
				if (@pg_num_rows($dependent_rs) < 1) {
					echo "<tr><td><em>No records found</em></td></tr>";
				}
				while ($dependent_row = @pg_fetch_object($dependent_rs)) {
					printf("<tr><td><a class=\"vocab_xref\" title=\"View/Edit\" href=\"%s\">%s (LOINC %s)</a></td></tr>",
						($main_url . sprintf("?selected_page=%s&submenu=%s&vocab=%d&edit_id=%s", $selected_page, $submenu, 1, $dependent_row->id)),
						$dependent_row->concept_name,
						$dependent_row->loinc);
				}
				@pg_free_result($dependent_rs);
				echo "<tbody></table><br>";
		?>
				<div style="clear: both;"><label class="vocab_search_form">Child Organisms...</label></div>
					<table id="labResults">
						<thead>
							<tr><th>Child Lab</th><th>Child Code</th></tr>
						</thead>
						<tbody>
		<?php
				$dependent_sql = sprintf("SELECT vco.id AS id, sl.ui_name AS lab_name, vco.child_code AS child_code
					FROM %svocab_child_organism vco JOIN %sstructure_labs sl ON (vco.lab_id = sl.id)
					WHERE (vco.organism = %d) OR (vco.test_result_id = %d)
					ORDER BY sl.ui_name, vco.child_code",
					$my_db_schema, $my_db_schema, $vocab_id, $vocab_id);
				$dependent_rs = @pg_query($host_pa, $dependent_sql) or suicide("Unable to retrieve Child LOINCs.", 1);
				if (@pg_num_rows($dependent_rs) < 1) {
					echo "<tr><td colspan=\"2\"><em>No records found</em></td></tr>";
				}
				while ($dependent_row = @pg_fetch_object($dependent_rs)) {
					printf("<tr><td>%s</td><td><a class=\"vocab_xref\" title=\"View/Edit\" href=\"%s\">%s</a></td></tr>",
						$dependent_row->lab_name,
						($main_url . sprintf("?selected_page=%s&submenu=%s&vocab=%d&edit_id=%s", $selected_page, $submenu, 4, $dependent_row->id)),
						$dependent_row->child_code);
				}
				@pg_free_result($dependent_rs);
				echo "<tbody></table>";
		
			}
		?>
		</div>
		
	<?php
		}
		
	?>
	
	<div id="vocab_log" class="edit_vocab_form ui-widget ui-state-highlight ui-widget-content ui-corner-all">
		<div style="clear: both;"><label class="vocab_search_form">Audit Log</label></div>
		<?php
			if ($va_table === VocabAudit::TABLE_CHILD_LOINC) {
				echo $va->displayVocabAuditById(intval($vocab_id), array($va_table, VocabAudit::TABLE_CHILD_TESTRESULT));
			} elseif ($va_table === VocabAudit::TABLE_MASTER_LOINC) {
				echo $va->displayVocabAuditById(intval($vocab_id), array($va_table, VocabAudit::TABLE_CMR_RULES));
			} else {
				echo $va->displayVocabAuditById(intval($vocab_id), array($va_table));
			}
		?>
	</div>
	
	<?php
		
		##### don't show the rest of the Vocabulary page if 'edit' form is drawn #####
		exit();
	}
	
?>