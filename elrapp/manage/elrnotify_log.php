<script>
	$(function() {
		$("#labResults tr").hover(function() {
			$(this).find("td").toggleClass("labresults_hover");
		});
	});
</script>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrnotify"></span>ELR E-mail Notification Log</h1>

<div class="vocab_search ui-tabs ui-widget">
<div style="float: left; width: 50%; font-style: italic; font-family: 'Open Sans', Arial, Helvetica, sans-serif; margin: 5px;">
	E-mail Notifications sent by EMSA within the last 24 hours.
</div>
</div>

<div class="lab_results_container ui-widget ui-corner-all">
	
	<table id="labResults">
		<thead>
			<tr>
				<th>Date/Time Sent</th>
				<th>E-mail Address</th>
				<th>State/LHD?</th>
				<th>Successful?</th>
			</tr>
		</thead>
		<tbody>
		
<?php
	
	$log_qry = sprintf("SELECT * FROM %sbatch_notification_log WHERE created > (CURRENT_TIMESTAMP - interval '24 hours') ORDER BY date_trunc('hour', created) DESC, jurisdiction;", $my_db_schema);
	$log_rs = @pg_query($host_pa, $log_qry);
	if ($log_rs) {
	
		while ($log_row = pg_fetch_object($log_rs)) {
			echo "<tr>";
			echo "<td>".date("d M Y, g:i a", strtotime($log_row->created))."</td>";
			echo "<td>".htmlentities($log_row->email)."</td>";
			echo "<td>".((is_null($log_row->jurisdiction) || (strlen(trim($log_row->jurisdiction)) < 1)) ? '<strong style="color: darkslategray;" title="State-Level Notification">State</strong>' : ((trim($log_row->custom) == 't') ? '<span style="color: darkgray;" title="Virtual Jurisdiction">'.customLhdName(intval($log_row->jurisdiction)).'</span>' : '<strong title="Local Health Department">'.lhdName(intval($log_row->jurisdiction)).'</strong>') )."</td>";
			echo "<td>".((trim($log_row->success) == "t") ? "<span class=\"ui-icon ui-icon-elrsuccess\" title=\"Yes\"></span>" : "<span class=\"ui-icon ui-icon-elrcancel\" title=\"No\"></span>")."</td>";
			echo "</tr>";
		}
	} else {
		suicide("Could not connect to ELR notification log database.", 1);
	}
	
	@pg_free_result($log_rs);

?>
		
		</tbody>
	</table>
	<br><br>
	
</div>