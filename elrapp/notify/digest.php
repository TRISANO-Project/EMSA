<?php

	// prevent caching...
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');
	
	$ignore_permission = true;
	
	// hack include_path for crontab'bed calls to account for structural differences between test & dev/prod
	set_include_path(get_include_path() . PATH_SEPARATOR . '/var/www/html/elrapp' . PATH_SEPARATOR . '/srv/www/htdocs/elrapp');
	
	include_once 'includes/app_config.php';
	require_once WEBROOT_URL.'/includes/email_functions.php';
	require_once WEBROOT_URL.'/includes/class.phpmailer.php';
	require_once WEBROOT_URL.'/includes/class.smtp.php';
	require_once WEBROOT_URL.'/includes/classes/PHPExcel.php';
	
	unset($recipients);
	$recipients = array();
	unset($notify_types);
	unset($state_local_iterator);
	
	$state_local_iterator = array('udoh', 'lhd', 'custom');  // used to loop through state-level notifications and then lhd-level notifications
	
	
	// default config values
	$udoh_enable = false;
	$lhd_enable = false;
	$udoh_notify_email = 'elr@utah.gov';
	
	// get config from db
	$config_qry = 'SELECT udoh_enable, lhd_enable, udoh_email FROM '.$my_db_schema.'batch_notification_config WHERE id = 1;';
	$config_rs = @pg_query($host_pa, $config_qry);
	if ($config_rs) {
		$config_row = @pg_fetch_object($config_rs);
		$udoh_enable = (($config_row->udoh_enable == 't') ? true : false);
		$lhd_enable = (($config_row->lhd_enable == 't') ? true : false);
		$udoh_notify_email = ((filter_var($config_row->udoh_email, FILTER_VALIDATE_EMAIL)) ? filter_var($config_row->udoh_email, FILTER_SANITIZE_EMAIL) : 'elr@utah.gov' );
	}
	@pg_free_result($config_rs);
	
	
	// pre-build unique filename template
	$base_filename = WEBROOT_URL."/manage/import/upload/".date("Ymd", time())."_elrnotify_%s.xls";
	
	// get list of jurisdictions found with unsent messages, also get e-mail addresses to send notifications to
	$j_qry = "SELECT DISTINCT jurisdiction_id FROM ".$my_db_schema."batch_notifications WHERE (custom IS FALSE) AND (date_sent_lhd IS NULL) AND (notify_lhd IS TRUE);";
	$j_rs = @pg_query($host_pa, $j_qry);
	while ($j_row = @pg_fetch_object($j_rs)) {
		$recipients[intval($j_row->jurisdiction_id)] = getEmailAddressesByJurisdiction(intval($j_row->jurisdiction_id));
	}
	@pg_free_result($j_rs);
	
	// get list of custom notifications found with unsent messages, also get e-mail addresses to send notifications to
	$jc_qry = "SELECT DISTINCT jurisdiction_id FROM ".$my_db_schema."batch_notifications WHERE (custom IS TRUE) AND (date_sent_lhd IS NULL) AND (notify_lhd IS TRUE);";
	$jc_rs = @pg_query($host_pa, $jc_qry);
	while ($jc_row = @pg_fetch_object($jc_rs)) {
		$custom_recipients[intval($jc_row->jurisdiction_id)] = getEmailAddressesByCustomJurisdiction(intval($jc_row->jurisdiction_id));
	}
	@pg_free_result($jc_rs);
	
	// get list of notification types for message grouping
	$n_qry = "SELECT id, label, state_use, lhd_use, custom FROM ".$my_db_schema."batch_notification_types ORDER BY sort;";
	$n_rs = @pg_query($host_pa, $n_qry);
	if ($n_rs !== false) {
		while ($n_row = @pg_fetch_object($n_rs)) {
			$notify_types[intval($n_row->id)] = array(
				'state_use' => ((trim($n_row->state_use) == 't') ? true : false),
				'lhd_use' => ((trim($n_row->lhd_use) == 't') ? true : false),
				'label' => trim($n_row->label), 
				'custom' => ((strlen(trim($n_row->custom)) > 0)  ? intval(trim($n_row->custom)) : false)
			);
		}
		@pg_free_result($n_rs);
	} else {
		suicide("Unable to get list of notification types.  Please see a system administrator.");
	}
	
	// text-based intro for sending attachments
	$text_body = "You are receiving this message because you have chosen to receive notifications from UDOH for notifiable lab reports/case updates.  Please find attached all applicable daily ELR notifications for your jurisdiction.";
	
	
	/**
	 * iterate through state & local notification levles
	 */
	foreach ($state_local_iterator as $state_or_local) {
		if ($state_or_local == "lhd" && $lhd_enable) {
			if (isset($recipients) && is_array($recipients) && (count($recipients) > 0)) {
				// iterate through each jurisdiction & message type to build messages...
				foreach ($recipients as $j_id => $j_recipients) {
					unset($affected_notifications);
					unset($affected_recipients);
					
					// attempt to generate xls file for lhd
					unset($this_phe);
					unset($this_pheWriter);
					unset($this_filename);
					$this_filename = sprintf($base_filename, strtolower(preg_replace('/\s+/', '', trim(lhdName($j_id)))));
					
					// create a new PHPExcel document & writer
					$this_phe = new PHPExcel();
					$this_phe->getProperties()->setCreator("UDOH ELR");
					$this_phe->getProperties()->setLastModifiedBy("UDOH ELR");
					$this_sheet_index = 0;
					
					foreach ($notify_types as $n_id => $type_data) {
						if ($type_data['lhd_use'] && $type_data['custom'] === false) {
							unset($type_label);
							$type_label = $type_data['label'];
							
							unset($this_row_cursor);
							$this_row_cursor = 2;  // skip over header row
							if ($this_sheet_index > 0) {
								$this_phe->createSheet();
							}
							$this_phe->setActiveSheetIndex($this_sheet_index);
							$this_phe->getActiveSheet()->setTitle(substr($type_label, 0, 30));  // tab name limited to 31 chars
							
							// set column headings
							$this_phe->getActiveSheet()->setCellValue("A1", "TriSano Event (Click to View)");
							$this_phe->getActiveSheet()->setCellValue("B1", "Investigator");
							$this_phe->getActiveSheet()->setCellValue("C1", "Date/Time Received");
							$this_phe->getActiveSheet()->setCellValue("D1", "Condition");
							$this_phe->getActiveSheet()->setCellValue("E1", "Test Type");
							$this_phe->getActiveSheet()->setCellValue("F1", "Test Result");
							
							$this_phe->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
							$this_phe->getActiveSheet()->getStyle("A1:F1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$this_phe->getActiveSheet()->getStyle("A1:F1")->getFill()->getStartColor()->setARGB('FFAFEEEE');
							
							$qry = "SELECT * FROM ".$my_db_schema."batch_notifications WHERE jurisdiction_id = ".intval($j_id)." AND notification_type = ".intval($n_id)." AND date_sent_lhd IS NULL AND notify_lhd IS TRUE order by date_created;";
							$rs = pg_query($host_pa, $qry);
							
							if (pg_num_rows($rs) > 0) {
								while ($row = pg_fetch_object($rs)) {
									// keep track of which notifications from the database have been affected by this batch
									$affected_notifications[] = intval($row->id);
									
									// set cell contents from db
									$this_phe->getActiveSheet()->setCellValue("A".$this_row_cursor, "Record# ".filter_var($row->record_number, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->getCell("A".$this_row_cursor)->getHyperlink()->setUrl($props['trisano_url'].((stripos($row->event_type, 'morbid') !== false) ? 'cmrs/' : 'contact_events/').intval($row->event_id));
									$this_phe->getActiveSheet()->setCellValue("B".$this_row_cursor, filter_var($row->investigator, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->setCellValue("C".$this_row_cursor, date("m/d/Y g:i A", strtotime($row->date_created)));
									$this_phe->getActiveSheet()->setCellValue("D".$this_row_cursor, filter_var($row->condition, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->setCellValue("E".$this_row_cursor, filter_var($row->test_type, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->setCellValue("F".$this_row_cursor, filter_var($row->test_result, FILTER_SANITIZE_STRING));
									
									$this_row_cursor++;
								}
							}
							
							@pg_free_result($rs);
							
							// auto-size column widths
							$this_phe->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
							
							// repeat notification type in sheet as well as for tab name
							$this_phe->getActiveSheet()->insertNewRowBefore(1, 1);  // one new row inserted before current column headings
							$this_phe->getActiveSheet()->setCellValue("A1", $type_label);
							$this_phe->getActiveSheet()->mergeCells("A1:F1");
							$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
							$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setSize("14");
							$this_phe->getActiveSheet()->getStyle("A1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$this_phe->getActiveSheet()->getStyle("A1")->getFill()->getStartColor()->setARGB('FFFFD700');
							
							// freeze headings
							$this_phe->getActiveSheet()->freezePane("A3");
							
							$this_sheet_index++;
						}
					}
					
					// move back to first sheet for opening
					$this_phe->setActiveSheetIndex(0);
					
					// save our workbook
					$this_pheWriter = new PHPExcel_Writer_Excel5($this_phe);
					$this_pheWriter->save($this_filename);
					
					// send the notifications to detected recipients
					if (isset($j_recipients) && is_array($j_recipients) && (count($j_recipients) > 0)) {
						foreach ($j_recipients as $j_r_index => $recipient_email) {
							if (filter_var($recipient_email, FILTER_VALIDATE_EMAIL)) {
								if (sendmail("elr@utah.gov", "UDOH ELR", "UDOH Daily ELR Notification Summary - ".date("m/d/Y"), $text_body, filter_var($recipient_email, FILTER_SANITIZE_EMAIL), null, $this_filename)) {
									$affected_recipients['success'][] = filter_var($recipient_email, FILTER_SANITIZE_EMAIL);
								} else {
									$affected_recipients['failure'][] = filter_var($recipient_email, FILTER_SANITIZE_EMAIL);
								}
							} else {
								$affected_recipients['failure'][] = filter_var($recipient_email, FILTER_SANITIZE_STRING);
							}
						}
						// temp copy to Josh for ldh QA
						@sendmail("elr@utah.gov", "UDOH ELR", "[QA Copy - ".lhdName($j_id)."] UDOH Daily ELR Notification Summary - ".date("m/d/Y"), $text_body, 'jridderhoff@utah.gov', null, $this_filename);
					} else {
						// no e-mail addresses found for jurisdiction, notify elr@utah.gov
						@sendmail("elr@utah.gov", "UDOH ELR", "ELR LHD Notification Failure (".lhdName($j_id).") - ".date("m/d/Y"), 'While attempting to send a "UDOH Daily ELR Notification Summary" e-mail to the "'.lhdName($j_id).'" jurisdiction, no users with e-mail addresses could be found to send the notification to.', 'elr@utah.gov', null, $this_filename);
					}
					
					if (isset($affected_recipients['success']) && is_array($affected_recipients['success']) && (count($affected_recipients['success']) > 0)) {
						// at least one notification was sent, update the affected notifications to set a date_sent time
						$datesent_qry = "UPDATE ".$my_db_schema."batch_notifications SET date_sent_lhd = current_timestamp WHERE id IN (" . pg_escape_string(implode(",", $affected_notifications)) . ");";
						$datesent_rs = @pg_query($host_pa, $datesent_qry);
						@pg_free_result($datesent_rs);
						
						// log the successful e-mails
						foreach ($affected_recipients['success'] as $successful_email) {
							unset($batchlog_qry);
							$batchlog_qry = "INSERT INTO ".$my_db_schema."batch_notification_log (email, jurisdiction, notification_ids, success, custom) VALUES ('".pg_escape_string($successful_email)."', ".intval($j_id).", '".pg_escape_string(implode(",", $affected_notifications))."', 't', 'f');";
							$batchlog_rs = @pg_query($host_pa, $batchlog_qry);
							@pg_free_result($batchlog_rs);
						}
					}
					
					if (isset($affected_recipients['failure']) && is_array($affected_recipients['failure']) && (count($affected_recipients['failure']) > 0)) {
						// log the failed e-mails as well
						foreach ($affected_recipients['failure'] as $failed_email) {
							unset($batchlog_qry);
							$batchlog_qry = "INSERT INTO ".$my_db_schema."batch_notification_log (email, jurisdiction, notification_ids, success, custom) VALUES ('".pg_escape_string($failed_email)."', ".intval($j_id).", '".pg_escape_string(implode(",", $affected_notifications))."', 'f', 'f');";
							$batchlog_rs = @pg_query($host_pa, $batchlog_qry);
							@pg_free_result($batchlog_rs);
						}
					}
					
					/* debug
					echo intval($j_id)."<br>";
					echo "<pre>";
					print_r($affected_notifications);
					print_r($affected_recipients);
					echo "</pre>";
					echo "<hr>";
					*/
					
					// clear the PHPExcel objects from memory
					// important to disconnect worksheets to prevent memory leak
					unset($this_pheWriter);
					$this_phe->disconnectWorksheets();
					unset($this_phe);
					
					// delete the temporary Excel file saved to disk
					unlink($this_filename);
				}
			}
		} elseif ($state_or_local == "custom") {
			if (isset($custom_recipients) && is_array($custom_recipients) && (count($custom_recipients) > 0)) {
				// iterate through each jurisdiction & message type to build messages...
				foreach ($custom_recipients as $jc_id => $jc_recipients) {
					unset($affected_notifications);
					unset($affected_recipients);
					unset($link_to_lab);
					
					$link_to_lab = useLinkToLab($jc_id);
					
					// attempt to generate xls file for lhd
					unset($this_phe);
					unset($this_pheWriter);
					unset($this_filename);
					$this_filename = sprintf($base_filename, strtolower(preg_replace('/\s+/', '', trim(customLhdName($jc_id)))));
					
					// create a new PHPExcel document & writer
					$this_phe = new PHPExcel();
					$this_phe->getProperties()->setCreator("UDOH ELR");
					$this_phe->getProperties()->setLastModifiedBy("UDOH ELR");
					$this_sheet_index = 0;
					
					foreach ($notify_types as $n_id => $type_data) {
						if ($type_data['custom'] == $jc_id) {
							unset($type_label);
							$type_label = $type_data['label'];
							
							unset($this_row_cursor);
							$this_row_cursor = 2;  // skip over header row
							if ($this_sheet_index > 0) {
								$this_phe->createSheet();
							}
							$this_phe->setActiveSheetIndex($this_sheet_index);
							$this_phe->getActiveSheet()->setTitle(substr($type_label, 0, 30));  // tab name limited to 31 chars
							
							// set column headings
							$this_phe->getActiveSheet()->setCellValue("A1", "TriSano Event (Click to View)");
							$this_phe->getActiveSheet()->setCellValue("B1", "Investigator");
							$this_phe->getActiveSheet()->setCellValue("C1", "Date/Time Received");
							$this_phe->getActiveSheet()->setCellValue("D1", "Condition");
							$this_phe->getActiveSheet()->setCellValue("E1", "Test Type");
							$this_phe->getActiveSheet()->setCellValue("F1", "Test Result");
							if ($link_to_lab) {
								$this_phe->getActiveSheet()->setCellValue("G1", "Original Lab (Click to View)");
								$this_phe->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
								$this_phe->getActiveSheet()->getStyle("A1:G1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
								$this_phe->getActiveSheet()->getStyle("A1:G1")->getFill()->getStartColor()->setARGB('FFAFEEEE');
							} else {
								$this_phe->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
								$this_phe->getActiveSheet()->getStyle("A1:F1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
								$this_phe->getActiveSheet()->getStyle("A1:F1")->getFill()->getStartColor()->setARGB('FFAFEEEE');
							}
							
							$qry = "SELECT * FROM ".$my_db_schema."batch_notifications WHERE jurisdiction_id = ".intval($jc_id)." AND notification_type = ".intval($n_id)." AND date_sent_lhd IS NULL AND notify_lhd IS TRUE order by date_created;";
							$rs = pg_query($host_pa, $qry);
							
							if (pg_num_rows($rs) > 0) {
								while ($row = pg_fetch_object($rs)) {
									// keep track of which notifications from the database have been affected by this batch
									$affected_notifications[] = intval($row->id);
									
									// set cell contents from db
									$this_phe->getActiveSheet()->setCellValue("A".$this_row_cursor, "Record# ".filter_var($row->record_number, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->getCell("A".$this_row_cursor)->getHyperlink()->setUrl($props['trisano_url'].((stripos($row->event_type, 'morbid') !== false) ? 'cmrs/' : 'contact_events/').intval($row->event_id));
									$this_phe->getActiveSheet()->setCellValue("B".$this_row_cursor, filter_var($row->investigator, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->setCellValue("C".$this_row_cursor, date("m/d/Y g:i A", strtotime($row->date_created)));
									$this_phe->getActiveSheet()->setCellValue("D".$this_row_cursor, filter_var($row->condition, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->setCellValue("E".$this_row_cursor, filter_var($row->test_type, FILTER_SANITIZE_STRING));
									$this_phe->getActiveSheet()->setCellValue("F".$this_row_cursor, filter_var($row->test_result, FILTER_SANITIZE_STRING));
									if ($link_to_lab) {
										$this_phe->getActiveSheet()->setCellValue("G".$this_row_cursor, "View Original Lab");
										$this_phe->getActiveSheet()->getCell("G".$this_row_cursor)->getHyperlink()->setUrl(MAIN_URL.'/?selected_page=6&submenu=6&focus='.intval($row->system_message_id));
									}
									
									$this_row_cursor++;
								}
							}
							
							@pg_free_result($rs);
							
							// auto-size column widths
							$this_phe->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
							$this_phe->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
							if ($link_to_lab) {
								$this_phe->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
							}
							
							// repeat notification type in sheet as well as for tab name
							$this_phe->getActiveSheet()->insertNewRowBefore(1, 1);  // one new row inserted before current column headings
							$this_phe->getActiveSheet()->setCellValue("A1", $type_label);
							if ($link_to_lab) {
								$this_phe->getActiveSheet()->mergeCells("A1:G1");
							} else {
								$this_phe->getActiveSheet()->mergeCells("A1:F1");
							}
							$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
							$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setSize("14");
							$this_phe->getActiveSheet()->getStyle("A1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$this_phe->getActiveSheet()->getStyle("A1")->getFill()->getStartColor()->setARGB('FFFFD700');
							
							// freeze headings
							$this_phe->getActiveSheet()->freezePane("A3");
							
							$this_sheet_index++;
						}
					}
					
					// move back to first sheet for opening
					$this_phe->setActiveSheetIndex(0);
					
					// save our workbook
					$this_pheWriter = new PHPExcel_Writer_Excel5($this_phe);
					$this_pheWriter->save($this_filename);
					
					// send the notifications to detected recipients
					if (isset($jc_recipients) && is_array($jc_recipients) && (count($jc_recipients) > 0)) {
						foreach ($jc_recipients as $jc_r_index => $custom_recipient_email) {
							if (filter_var($custom_recipient_email, FILTER_VALIDATE_EMAIL)) {
								if (sendmail("elr@utah.gov", "UDOH ELR", "UDOH Daily ELR Notification Summary - ".date("m/d/Y"), $text_body, filter_var($custom_recipient_email, FILTER_SANITIZE_EMAIL), null, $this_filename)) {
									$affected_recipients['success'][] = filter_var($custom_recipient_email, FILTER_SANITIZE_EMAIL);
								} else {
									$affected_recipients['failure'][] = filter_var($custom_recipient_email, FILTER_SANITIZE_EMAIL);
								}
							} else {
								$affected_recipients['failure'][] = filter_var($custom_recipient_email, FILTER_SANITIZE_STRING);
							}
						}
						// temp copy to Josh for custom validation
						@sendmail("elr@utah.gov", "UDOH ELR", "[QA Copy - ".customLhdName($jc_id)."] UDOH Daily ELR Notification Summary - ".date("m/d/Y"), $text_body, 'jridderhoff@utah.gov', null, $this_filename);
					} else {
						// no e-mail addresses found for jurisdiction, notify elr@utah.gov
						@sendmail("elr@utah.gov", "UDOH ELR", "ELR LHD Notification Failure (".customLhdName($jc_id).") - ".date("m/d/Y"), 'While attempting to send a "UDOH Daily ELR Notification Summary" e-mail to the "'.customLhdName($jc_id).'" jurisdiction, no users with e-mail addresses could be found to send the notification to.', 'elr@utah.gov', null, $this_filename);
					}
					
					if (isset($affected_recipients['success']) && is_array($affected_recipients['success']) && (count($affected_recipients['success']) > 0)) {
						// at least one notification was sent, update the affected notifications to set a date_sent time
						$datesent_qry = "UPDATE ".$my_db_schema."batch_notifications SET date_sent_lhd = current_timestamp WHERE id IN (" . pg_escape_string(implode(",", $affected_notifications)) . ");";
						$datesent_rs = @pg_query($host_pa, $datesent_qry);
						@pg_free_result($datesent_rs);
						
						// log the successful e-mails
						foreach ($affected_recipients['success'] as $successful_email) {
							unset($batchlog_qry);
							$batchlog_qry = "INSERT INTO ".$my_db_schema."batch_notification_log (email, jurisdiction, notification_ids, success, custom) VALUES ('".pg_escape_string($successful_email)."', ".intval($jc_id).", '".pg_escape_string(implode(",", $affected_notifications))."', 't', 't');";
							$batchlog_rs = @pg_query($host_pa, $batchlog_qry);
							@pg_free_result($batchlog_rs);
						}
					}
					
					if (isset($affected_recipients['failure']) && is_array($affected_recipients['failure']) && (count($affected_recipients['failure']) > 0)) {
						// log the failed e-mails as well
						foreach ($affected_recipients['failure'] as $failed_email) {
							unset($batchlog_qry);
							$batchlog_qry = "INSERT INTO ".$my_db_schema."batch_notification_log (email, jurisdiction, notification_ids, success, custom) VALUES ('".pg_escape_string($failed_email)."', ".intval($jc_id).", '".pg_escape_string(implode(",", $affected_notifications))."', 'f', 't');";
							$batchlog_rs = @pg_query($host_pa, $batchlog_qry);
							@pg_free_result($batchlog_rs);
						}
					}
					
					/* debug
					echo intval($j_id)."<br>";
					echo "<pre>";
					print_r($affected_notifications);
					print_r($affected_recipients);
					echo "</pre>";
					echo "<hr>";
					*/
					
					// clear the PHPExcel objects from memory
					// important to disconnect worksheets to prevent memory leak
					unset($this_pheWriter);
					$this_phe->disconnectWorksheets();
					unset($this_phe);
					
					// delete the temporary Excel file saved to disk
					unlink($this_filename);
				}
			}
		} elseif ($state_or_local == "udoh" && $udoh_enable) {
			unset($affected_notifications);
			unset($affected_recipients);
			
			// attempt to generate xls file for lhd
			unset($this_phe);
			unset($this_pheWriter);
			unset($this_filename);
			$this_filename = sprintf($base_filename, "udoh");
			
			// create a new PHPExcel document & writer
			$this_phe = new PHPExcel();
			$this_phe->getProperties()->setCreator("UDOH ELR");
			$this_phe->getProperties()->setLastModifiedBy("UDOH ELR");
			$this_sheet_index = 0;
			
			foreach ($notify_types as $n_id => $type_data) {
				if ($type_data['state_use'] && $type_data['custom'] === false) {
					unset($type_label);
					$type_label = $type_data['label'];
					
					unset($this_row_cursor);
					$this_row_cursor = 2;  // skip over header row
					if ($this_sheet_index > 0) {
						$this_phe->createSheet();
					}
					$this_phe->setActiveSheetIndex($this_sheet_index);
					$this_phe->getActiveSheet()->setTitle(substr($type_label, 0, 30));  // tab name limited to 31 chars
					
					// set column headings
					$this_phe->getActiveSheet()->setCellValue("A1", "TriSano Event (Click to View)");
					$this_phe->getActiveSheet()->setCellValue("B1", "Investigator");
					$this_phe->getActiveSheet()->setCellValue("C1", "Date/Time Received");
					$this_phe->getActiveSheet()->setCellValue("D1", "Condition");
					$this_phe->getActiveSheet()->setCellValue("E1", "Test Type");
					$this_phe->getActiveSheet()->setCellValue("F1", "Test Result");
					
					$this_phe->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
					$this_phe->getActiveSheet()->getStyle("A1:F1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$this_phe->getActiveSheet()->getStyle("A1:F1")->getFill()->getStartColor()->setARGB('FFAFEEEE');
					
					$qry = "SELECT * FROM ".$my_db_schema."batch_notifications WHERE notification_type = ".intval($n_id)." AND date_sent_state IS NULL AND notify_state IS TRUE order by date_created;";
					$rs = pg_query($host_pa, $qry);
					
					if (pg_num_rows($rs) > 0) {
						while ($row = pg_fetch_object($rs)) {
							// keep track of which notifications from the database have been affected by this batch
							$affected_notifications[] = intval($row->id);
							
							// set cell contents from db
							$this_phe->getActiveSheet()->setCellValue("A".$this_row_cursor, "Record# ".filter_var($row->record_number, FILTER_SANITIZE_STRING));
							$this_phe->getActiveSheet()->getCell("A".$this_row_cursor)->getHyperlink()->setUrl($props['trisano_url'].((stripos($row->event_type, 'morbid') !== false) ? 'cmrs/' : 'contact_events/').intval($row->event_id));
							$this_phe->getActiveSheet()->setCellValue("B".$this_row_cursor, filter_var($row->investigator, FILTER_SANITIZE_STRING));
							$this_phe->getActiveSheet()->setCellValue("C".$this_row_cursor, date("m/d/Y g:i A", strtotime($row->date_created)));
							$this_phe->getActiveSheet()->setCellValue("D".$this_row_cursor, filter_var($row->condition, FILTER_SANITIZE_STRING));
							$this_phe->getActiveSheet()->setCellValue("E".$this_row_cursor, filter_var($row->test_type, FILTER_SANITIZE_STRING));
							$this_phe->getActiveSheet()->setCellValue("F".$this_row_cursor, filter_var($row->test_result, FILTER_SANITIZE_STRING));
							
							$this_row_cursor++;
						}
					}
					
					@pg_free_result($rs);
					
					// auto-size column widths
					$this_phe->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
					$this_phe->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
					$this_phe->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
					$this_phe->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
					$this_phe->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
					$this_phe->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
					
					// repeat notification type in sheet as well as for tab name
					$this_phe->getActiveSheet()->insertNewRowBefore(1, 1);  // one new row inserted before current column headings
					$this_phe->getActiveSheet()->setCellValue("A1", $type_label);
					$this_phe->getActiveSheet()->mergeCells("A1:F1");
					$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
					$this_phe->getActiveSheet()->getStyle("A1")->getFont()->setSize("14");
					$this_phe->getActiveSheet()->getStyle("A1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$this_phe->getActiveSheet()->getStyle("A1")->getFill()->getStartColor()->setARGB('FFFFD700');
					
					// freeze headings
					$this_phe->getActiveSheet()->freezePane("A3");
					
					$this_sheet_index++;
				}
			}
			
			// move back to first sheet for opening
			$this_phe->setActiveSheetIndex(0);
			
			// save our workbook
			$this_pheWriter = new PHPExcel_Writer_Excel5($this_phe);
			$this_pheWriter->save($this_filename);
			
			if (isset($affected_notifications) && is_array($affected_notifications) && (count($affected_notifications) > 0)) {
				if (sendmail("elr@utah.gov", "UDOH ELR", "UDOH Daily ELR Notification Summary - ".date("m/d/Y"), $text_body, $udoh_notify_email, null, $this_filename)) {
					$affected_recipients['success'][] = $udoh_notify_email;
				} else {
					$affected_recipients['failure'][] = $udoh_notify_email;
				}
				// temp hack to copy Susan on all ELR notifications
				@sendmail("elr@utah.gov", "UDOH ELR", "[QA COPY - UDOH Epi] UDOH Daily ELR Notification Summary - ".date("m/d/Y"), $text_body, 'smottice@utah.gov', null, $this_filename);
				@sendmail("elr@utah.gov", "UDOH ELR", "[QA COPY - UDOH Epi] UDOH Daily ELR Notification Summary - ".date("m/d/Y"), $text_body, 'jridderhoff@utah.gov', null, $this_filename);
			}
			
			if (isset($affected_recipients['success']) && is_array($affected_recipients['success']) && (count($affected_recipients['success']) > 0)) {
				// at least one notification was sent, update the affected notifications to set a date_sent time
				$datesent_qry = "UPDATE ".$my_db_schema."batch_notifications SET date_sent_state = current_timestamp WHERE id IN (" . pg_escape_string(implode(",", $affected_notifications)) . ");";
				$datesent_rs = @pg_query($host_pa, $datesent_qry);
				@pg_free_result($datesent_rs);
				
				// log the successful e-mails
				foreach ($affected_recipients['success'] as $successful_email) {
					unset($batchlog_qry);
					$batchlog_qry = "INSERT INTO ".$my_db_schema."batch_notification_log (email, notification_ids, success, custom) VALUES ('".pg_escape_string($successful_email)."', '".pg_escape_string(implode(",", $affected_notifications))."', 't', 'f');";
					$batchlog_rs = @pg_query($host_pa, $batchlog_qry);
					@pg_free_result($batchlog_rs);
				}
			}
			
			if (isset($affected_recipients['failure']) && is_array($affected_recipients['failure']) && (count($affected_recipients['failure']) > 0)) {
				// log the failed e-mails as well
				foreach ($affected_recipients['failure'] as $failed_email) {
					unset($batchlog_qry);
					$batchlog_qry = "INSERT INTO ".$my_db_schema."batch_notification_log (email, notification_ids, success, custom) VALUES ('".pg_escape_string($failed_email)."', '".pg_escape_string(implode(",", $affected_notifications))."', 'f', 'f');";
					$batchlog_rs = @pg_query($host_pa, $batchlog_qry);
					@pg_free_result($batchlog_rs);
				}
			}
			
			/* debug
			echo "UDOH<br>";
			echo "<pre>";
			print_r($affected_notifications);
			print_r($affected_recipients);
			echo "</pre>";
			echo "<hr>";
			*/
			
			// clear the PHPExcel objects from memory
			// important to disconnect worksheets to prevent memory leak
			unset($this_pheWriter);
			$this_phe->disconnectWorksheets();
			unset($this_phe);
			
			// delete the temporary Excel file saved to disk
			unlink($this_filename);
		}
	}
	
?>