<?php

	if ($_POST['from_date'] && $_POST['to_date']) {
		// update sessionized date params if updated by user
		$from_unix = strtotime(trim($_POST['from_date']));
		$to_unix = strtotime(trim($_POST['to_date']));
		if (($from_unix !== false) && ($to_unix !== false)) {
			$_SESSION['reporting_date_from'] = date("m/d/Y", $from_unix);
			$_SESSION['reporting_date_to'] = date("m/d/Y", $to_unix);
		}
	}
	
	if ($_POST['lab_filter']) {
		if (filter_var($_POST['lab_filter'], FILTER_VALIDATE_INT)) {
			$_SESSION['dashboard_lab_filter'] = filter_var($_POST['lab_filter'], FILTER_SANITIZE_NUMBER_INT);
		}
	}
	
	if (!isset($_SESSION['reporting_date_from']) || !isset($_SESSION['reporting_date_to'])) {
		// if no sessionized date params, default to today
		$_SESSION['reporting_date_from'] = date("m/d/Y", strtotime("-1 week", time()));
		$_SESSION['reporting_date_to'] = date("m/d/Y");
	}
	
	if (!isset($_SESSION['dashboard_lab_filter'])) {
		$_SESSION['dashboard_lab_filter'] = -1;
	}
	
	session_write_close(); // done writing to session; prevent blocking
	
	//$dashboard_summary = getDashboardSummary($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);

?>

<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type="text/javascript">
	google.load('visualization', '1', {'packages': ['corechart', 'table']});
	
	$(function() {
		$(".date_range").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
		
		$("#apply_date").button({
			icons: { primary: "ui-icon-elrretry" }
		});
		
		$("#lab_filter").val('<?php echo intval($_SESSION['reporting_lab_filter']); ?>');
		
		$("#report_list li").click(function() {
			$("#reporting_area").html('<img style="vertical-align: bottom;" src="<?php echo $main_url; ?>img/ajax-loader.gif" height="16" width="16" border="0" /> <em>Generating report...</em>');
			$("#content-wrapper-1").addClass('busy');
			var reportId = $(this).attr("report");
			$.post("reports/viewer.php", { report_id: reportId }, function(reportData) {
				$("#reporting_area").html(reportData);
				$("#content-wrapper-1").removeClass('busy');
				drawChart();
			}).error(function(vaErrXhr, vaErrText, vaErrThrown) {
				$("#reporting_area").html('');
				$("#content-wrapper-1").removeClass('busy');
				alert("Could not load report.\n\nError Details:\n"+vaErrText+" ("+vaErrThrown+")");
			});
		});
		
		<?php
			if (isset($_REQUEST['report_id']) && !empty($_REQUEST['report_id'])) {
				echo '$("#report_list li[report=\''.$_REQUEST['report_id'].'\']").trigger("click");';
			} elseif (isset($_SESSION['reporting_current_report']) && !empty($_SESSION['reporting_current_report'])) {
				echo '$("#report_list li[report=\''.$_SESSION['reporting_current_report'].'\']").trigger("click");';
			}
		?>
		
		$("#lab_filter").val('<?php echo intval($_SESSION['dashboard_lab_filter']); ?>');
	});
</script>	

<style type="text/css" media="all">
	strong.big_strong { font-size: 17pt; margin: 0; color: darkolivegreen; }
	strong.medium_strong { margin-top: 1.5em; display: block; font-size: 14pt; color: firebrick; font-family: 'Francois One', sans-serif; border-bottom: 1px lightgray solid; font-weight: 400; }
	em { font-size: 10pt; color: dimgray; }
	#reporting_filters label { font-family: 'Open Sans', Arial; font-weight: 700; color: black; }
	.reporting_area { cursor: default; display: inline-block; position: relative; float: left; width: 70%; height: auto; margin: 10px; top: -5px; }
	.reporting_filters { background-color: ghostwhite; display: block; padding: 10px; position: relative; width: 20%; height: 100%; top: -5px; float: left; border-right: 1px darkgray solid; }
	#report_list { margin-top: 10px; border-top: 1px darkgray dotted; font-weight: 600 !important; color: midnightblue; }
	#report_list li { padding: 2px; border-bottom: 1px darkgray dotted; cursor: pointer; }
	#report_list li:hover { background-color: cornflowerblue; color: white; text-decoration: underline; }
	.busy { cursor: progress !important; }
</style>

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrdashboard"></span>Reporting</h1>

<div class="reporting_filters">
	<form id="reporting_filters" method="POST" action="<?php echo $main_url; ?>?selected_page=6&submenu=7">
		<label>Reporting Period:</label><br>
			<input class="ui-corner-all date_range" type="text" name="from_date" size="10" id="from_date" value="<?php echo $_SESSION['reporting_date_from']; ?>"> 
			<label>to</label> 
			<input class="ui-corner-all date_range" type="text" name="to_date" size="10" id="to_date" value="<?php echo $_SESSION['reporting_date_to']; ?>"><br><br>
			<label>Include:</label><br><select title="Filter results by lab" class="ui-corner-all lab_selector" id="lab_filter" name="lab_filter">
				<option value="-1">All Labs</option>
				<?php
					$lab_qry = "SELECT id, ui_name FROM ".$my_db_schema."structure_labs ORDER BY ui_name;";
					$lab_rs = @pg_query($host_pa, $lab_qry);
					if ($lab_rs) {
						while ($lab_row = @pg_fetch_object($lab_rs)) {
							echo "<option value=\"".intval($lab_row->id)."\">".$lab_row->ui_name."</option>".PHP_EOL;
						}
					}
					@pg_free_result($lab_rs);	
				?>
			</select><br><br>
		<?php 
			/*
		<label>Include:</label><br>
		<select title="Filter results by lab" class="ui-corner-all lab_selector" id="lab_filter" name="lab_filter">
			<option value="-1">All Labs</option>
			<?php
				$lab_qry = "SELECT id, ui_name FROM ".$my_db_schema."structure_labs ORDER BY ui_name;";
				$lab_rs = @pg_query($host_pa, $lab_qry);
				if ($lab_rs) {
					while ($lab_row = @pg_fetch_object($lab_rs)) {
						echo "<option value=\"".intval($lab_row->id)."\">".$lab_row->ui_name."</option>".PHP_EOL;
					}
				}
				@pg_free_result($lab_rs);	
			?>
		</select>
			*/
		?>
		<button id="apply_date" type="submit">Update</button>
		<br><br>
		<label>Available Reports:</label>
		<ul id="report_list">
			<li report="qa_flags"><span class="ui-icon ui-icon-elrview" style="display: inline-block; vertical-align: top; margin-right: 3px;"></span>QA Flag Stats</li>
			<li report="other_flag_reasons"><span class="ui-icon ui-icon-elrview" style="display: inline-block; vertical-align: top; margin-right: 3px;"></span>'Other' QA Flag Reasons</li>
			<li report="de_flag_reasons"><span class="ui-icon ui-icon-elrview" style="display: inline-block; vertical-align: top; margin-right: 3px;"></span>'Data Entry Error' QA Flag Types</li>
			<li report="orphaned_messages"><span class="ui-icon ui-icon-elrview" style="display: inline-block; vertical-align: top; margin-right: 3px;"></span>Orphaned ELR Messages</li>
			<li report="discarded_messages"><span class="ui-icon ui-icon-elrview" style="display: inline-block; vertical-align: top; margin-right: 3px;"></span>Discarded Duplicate ELR Messages</li>
			<li report="dashboard_tabular"><span class="ui-icon ui-icon-elrview" style="display: inline-block; vertical-align: top; margin-right: 3px;"></span>Tabular Dashboard Data</li>
		</ul>
	</form>
</div>

<div id="reporting_area" class="reporting_area ui-corner-all">
	<em>No report loaded.</em>
</div>