<?php
	
	echo '<strong class="big_strong">&ldquo;Data Entry Error&rdquo; QA Flag Types</strong>';
	echo '<br><em>(For messages received '.$_SESSION['reporting_date_from'].' to '.$_SESSION['reporting_date_to'].')</em>';
	
	$results_array = array();
	
	if ($_SESSION['dashboard_lab_filter'] > 0) {
		$lab_sql = 'AND sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']);
	} else {
		$lab_sql = '';
	}
	
	$sql = 'SELECT DISTINCT fc.info AS info, COUNT(fc.info) AS counter 
			FROM '.$my_db_schema.'system_message_flag_comments fc 
			WHERE fc.system_message_id IN ( 
				SELECT sm.id FROM '.$my_db_schema.'system_messages sm 
				WHERE sm.message_flags & '.EMSA_FLAG_DE_ERROR.' != 0 
				'.$lab_sql.' 
				AND sm.created_at::date >= \''.$_SESSION['reporting_date_from'].'\' 
				AND sm.created_at::date <= \''.$_SESSION['reporting_date_to'].'\') 
				AND fc.system_message_flag_id = '.EMSA_FLAG_DE_ERROR.' 
			GROUP BY fc.info 
			ORDER BY 2 DESC, 1;';

	$rs = @pg_query($host_pa, $sql);
	if ($rs !== false) {
		while ($row = @pg_fetch_object($rs)) {
			//print_r($row);
			$results_array[] = array(
				'Number of Occurrences'	=> intval($row->counter), 
				'Type'					=> trim($row->info)
			);
		}
		@pg_free_result($rs);
	}
	
?>

<div id="summary_chart" style="margin-top: 15px; height: 100%;"></div>

<script type='text/javascript'>
	function drawChart() {
		var summaryData = new google.visualization.DataTable();
		summaryData.addColumn('number', '# of Occurrences');
		summaryData.addColumn('string', 'Type');
	<?php
		foreach ($results_array as $user_key => $user_stats) {
			if (intval($user_stats['Number of Occurrences']) > 0) {
				echo "summaryData.addRow([".intval($user_stats['Number of Occurrences']).", '".htmlentities($user_stats['Type'], ENT_QUOTES, 'UTF-8')."']);".PHP_EOL;
			}
		}
	?>
		var summaryOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sortColumn: 0, 
			sortAscending: false
		};
		
		var summaryChart = new google.visualization.Table(document.getElementById('summary_chart'));
		summaryChart.draw(summaryData, summaryOptions);
	};
</script>