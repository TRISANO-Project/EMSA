<?php
	
	echo '<strong class="big_strong">QA Flag Statistics</strong>';
	echo '<br><em>(For user activity from '.$_SESSION['reporting_date_from'].' to '.$_SESSION['reporting_date_to'].')</em>';
	
	$results_array = array();
	
	$sql = 'SELECT DISTINCT user_id, 
				SUM(CASE WHEN message_action_id IN (23, 24) THEN 1 ELSE 0 END) AS assign_new, 
				SUM(CASE WHEN message_action_id IN (22, 28, 29) THEN 1 ELSE 0 END) AS assign_update, 
				SUM(CASE WHEN message_action_id = 7 THEN 1 ELSE 0 END) AS duplicate, 
				SUM(CASE WHEN message_action_id = 8 THEN 1 ELSE 0 END) AS delete, 
				SUM(CASE WHEN message_action_id = 13 THEN 1 ELSE 0 END) AS exception, 
				SUM(CASE WHEN message_action_id = 25 THEN 1 ELSE 0 END) AS move_manual, 
				SUM(CASE WHEN message_action_id = 27 THEN 1 ELSE 0 END) AS move_whitelist, 
				SUM(CASE WHEN message_action_id = 32 THEN 1 ELSE 0 END) AS set_flag, 
				SUM(CASE WHEN message_action_id = 33 THEN 1 ELSE 0 END) AS clear_flag
			FROM elr.system_messages_audits 
			WHERE user_id <> \'9999\'
			AND message_action_id IN (7, 8, 13, 23, 24, 25, 27, 22, 28, 29, 32, 33)
			AND created_at::date >= \''.$_SESSION['reporting_date_from'].'\'
			AND created_at::date <= \''.$_SESSION['reporting_date_to'].'\'
			GROUP BY user_id 
			ORDER BY user_id;';

		
	$rs = @pg_query($host_pa, $sql);
	if ($rs !== false) {
		while ($row = @pg_fetch_object($rs)) {
			//print_r($row);
			$results_array[userRealNameByUmdid($row->user_id)] = array(
				'New CMRs'				=> intval($row->assign_new), 
				'Cases Updated'			=> intval($row->assign_update), 
				'Duplicate Messages'	=> intval($row->duplicate), 
				'Messages Deleted'		=> intval($row->delete), 
				'Exceptions'			=> intval($row->exception), 
				'Messages Moved'		=> intval($row->move_manual), 
				'Messages Graylisted'	=> intval($row->move_whitelist), 
				'QA Flags Set'			=> intval($row->set_flag), 
				'QA Flags Cleared'		=> intval($row->clear_flag)
			);
		}
		@pg_free_result($rs);
	}
	
?>

<div id="summary_chart" style="margin-top: 15px; height: 100%;"></div>

<script type='text/javascript'>
	function drawChart() {
		var summaryData = new google.visualization.DataTable();
		summaryData.addColumn('string', 'User');
		summaryData.addColumn('number', 'New CMRs');
		summaryData.addColumn('number', 'Cases Updated');
		summaryData.addColumn('number', 'Duplicate Messages');
		summaryData.addColumn('number', 'Messages Deleted');
		summaryData.addColumn('number', 'Exceptions');
		summaryData.addColumn('number', 'Messages Moved');
		summaryData.addColumn('number', 'Messages Graylisted');
		summaryData.addColumn('number', 'QA Flags Set');
		summaryData.addColumn('number', 'QA Flags Cleared');
	<?php
		foreach ($results_array as $user => $user_stats) {
			echo "summaryData.addRow(['".$user."', ".intval($user_stats['New CMRs']).", ".intval($user_stats['Cases Updated']).", ".intval($user_stats['Duplicate Messages']).", ".intval($user_stats['Messages Deleted']).", ".intval($user_stats['Exceptions']).", ".intval($user_stats['Messages Moved']).", ".intval($user_stats['Messages Graylisted']).", ".intval($user_stats['QA Flags Set']).", ".intval($user_stats['QA Flags Cleared'])."]);".PHP_EOL;
		}
	?>
		var summaryOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sortColumn: 0
		};
		
		var summaryChart = new google.visualization.Table(document.getElementById('summary_chart'));
		summaryChart.draw(summaryData, summaryOptions);
	};
</script>