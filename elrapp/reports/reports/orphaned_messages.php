<?php
	
	echo '<strong class="big_strong">Orphaned (Invisible) ELR Messages</strong>';
	echo '<br><em>(These messages in EMSA are invisible to all users due to Master LOINC Codes that are not assigned to any User Roles)</em>';
	
	$results_array = array();
	
	$sql = 'SELECT sm.id, l.ui_name AS lab_name, sm.loinc_code, sm.disease, s.name AS queue_name, sm.created_at, sm.assigned_date, 
				sm.child_test_code AS local_code, sm.child_loinc AS local_loinc_code, sm.local_result_value AS local_result_value, sm.local_result_value_2 AS local_result_value_2 
			FROM '.$my_db_schema.'system_messages sm 
			INNER JOIN '.$my_db_schema.'structure_labs l ON (sm.lab_id = l.id)
			INNER JOIN '.$my_db_schema.'system_statuses s ON (sm.final_status = s.id)
			LEFT JOIN '.$my_db_schema.'system_role_loincs_by_lab rd ON ( ( rd.lab_id = sm.lab_id ) AND ( rd.master_loinc_code = sm.loinc_code ) )
			WHERE (rd.master_loinc_code IS NULL) AND (sm.loinc_code IS NOT NULL) AND (
				(sm.deleted = 0) OR (sm.deleted IS NULL)
			) 
			ORDER BY sm.lab_id, sm.final_status, sm.loinc_code, sm.created_at;';

	$rs = @pg_query($host_pa, $sql);
	if ($rs !== false) {
		while ($row = @pg_fetch_object($rs)) {
			//print_r($row);
			$results_array[] = array(
				'Message ID'		=> intval($row->id), 
				'Lab'				=> trim($row->lab_name), 
				'Queue'				=> trim($row->queue_name), 
				'Child Code'		=> trim($row->local_code), 
				'Child LOINC'		=> trim($row->local_loinc_code), 
				'Master LOINC'		=> trim($row->loinc_code), 
				'Child SCT Result'	=> trim($row->local_result_value), 
				'Child L Result'	=> trim($row->local_result_value_2), 
				'Condition'			=> trim($row->disease), 
				'Date Created'		=> trim(date("Y-m-d H:i:s A", strtotime($row->created_at))), 
				'Date Assigned'		=> ((strlen(trim($row->assigned_date)) > 0) ? trim(date("Y-m-d H:i:s A", strtotime($row->assigned_date))) : '--')
			);
		}
		@pg_free_result($rs);
	}
	
?>

<div id="total_container" style="font-family: 'Open Sans', Arial, sans-serif; margin-top: 15px; font-size: 11pt; font-weight: 600;"></div>
<div id="summary_chart" style="margin-top: 15px; height: 100%;"></div>

<script type='text/javascript'>
	function drawChart() {
		var summaryData = new google.visualization.DataTable();
		summaryData.addColumn('string', 'Message ID');
		summaryData.addColumn('string', 'Lab');
		summaryData.addColumn('string', 'Queue');
		summaryData.addColumn('string', 'Child LOINC [Local Code]');
		summaryData.addColumn('string', 'Child SMOMED [Local Result]');
		summaryData.addColumn('string', 'Master LOINC');
		summaryData.addColumn('string', 'Condition');
		summaryData.addColumn('string', 'Date Created');
		summaryData.addColumn('string', 'Date Assigned');
	<?php
		foreach ($results_array as $user_key => $user_stats) {
			echo "summaryData.addRow(['<a title=\"Click to view message\" href=\"".$main_url."?selected_page=6&submenu=6&focus=".intval($user_stats['Message ID'])."\" target=\"_blank\">".intval($user_stats['Message ID'])."</a>'
				, '".htmlentities($user_stats['Lab'], ENT_QUOTES, 'UTF-8')."'
				, '".htmlentities($user_stats['Queue'], ENT_QUOTES, 'UTF-8')."'
				, '".htmlentities($user_stats['Child LOINC'], ENT_QUOTES, 'UTF-8')." [".htmlentities($user_stats['Child Code'], ENT_QUOTES, 'UTF-8')."]'
				, '".htmlentities($user_stats['Child SCT Result'], ENT_QUOTES, 'UTF-8')." [".htmlentities($user_stats['Child L Result'], ENT_QUOTES, 'UTF-8')."]'
				, '".htmlentities($user_stats['Master LOINC'], ENT_QUOTES, 'UTF-8')."'
				, '".htmlentities($user_stats['Condition'], ENT_QUOTES, 'UTF-8')."'
				, '".htmlentities($user_stats['Date Created'], ENT_QUOTES, 'UTF-8')."'
				, '".htmlentities($user_stats['Date Assigned'], ENT_QUOTES, 'UTF-8')."'
			]);".PHP_EOL;
		}
	?>
		var summaryOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			allowHtml: true
		};
		
		var summaryChart = new google.visualization.Table(document.getElementById('summary_chart'));
		summaryChart.draw(summaryData, summaryOptions);
		$("#total_container").text('Total Orphaned Messages: '+summaryData.getNumberOfRows());
	};
</script>