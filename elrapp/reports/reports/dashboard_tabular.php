<?php
	
	echo '<strong class="big_strong">ELR Dashboard &mdash; Tabular Data</strong>';
	echo '<br><em>(For messages received from '.$_SESSION['reporting_date_from'].' to '.$_SESSION['reporting_date_to'].')</em>';
	
	$dashboard_summary = getDashboardSummary($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_avgnewcmr = getAvgCMRCreateTime($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_newcase = getDashboardNewCase($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_appendcase = getDashboardAppendedCase($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_discardcase = getDashboardDiscardedCase($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_blacklist_summary = getDashboardBlacklistSummary($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_graylist_summary = getDashboardGraylistSummary($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_mqf = getDashboardMessageQuality($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_lab = getDashboardLab($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$condition_summary = getDashboardConditionSummary($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	$dashboard_automation_factor = getDashboardAutomationFactor($_SESSION['reporting_date_from'], $_SESSION['reporting_date_to']);
	
	// consolidate new/updated assigned counts into a single list for canonical count
	foreach ($dashboard_newcase as $dashboard_newcase_category => $dashboard_newcase_count) {
		$dashboard_appendcase[$dashboard_newcase_category]['new_cmr'] = $dashboard_newcase_count;
	}
	
	// calculate automation percentage
	$automation_denominator = array_sum($dashboard_automation_factor);
	foreach ($dashboard_automation_factor as $auto_method => $auto_method_count) {
		$dashboard_automation_factor2[$auto_method] = array(
			'count'		=> $auto_method_count, 
			'percent'	=> (($automation_denominator > 0) ? trim(round((floatval(($auto_method_count)/($automation_denominator))*100), 1)).'%' : '--')
		);
	}
	
	// Add totals to applicable data
	$assigned_totals = array();
	foreach($dashboard_appendcase as $appendcase_totals_category => $appendcase_totals_data) {
		$assigned_totals['new_cmr'][]			= $appendcase_totals_data['new_cmr'];
		$assigned_totals['labs_updated'][]		= $appendcase_totals_data['labs_updated'];
		//$assigned_totals['contact_counter'][]	= $appendcase_totals_data['contact_counter'];
	}
	$assigned_newcmr_total = array_sum($assigned_totals['new_cmr']);
	$assigned_cmrcounter_total = array_sum($assigned_totals['labs_updated']);
	//$assigned_contactcounter_total = array_sum($assigned_totals['contact_counter']);
	
	$dashboard_appendcase['[Totals]'] = array(
		'new_cmr' 			=> $assigned_newcmr_total,
		'labs_updated'		=> $assigned_cmrcounter_total
	);
	
	$discard_total = array_sum($dashboard_discardcase);
	$graylist_total = array_sum($dashboard_graylist_summary);
	$blacklist_total = array_sum($dashboard_blacklist_summary);
	$mqf_total = array_sum($dashboard_mqf);
	
	$dashboard_discardcase['[Total]'] = intval($discard_total);
	$dashboard_graylist_summary['[Total]'] = intval($graylist_total);
	$dashboard_blacklist_summary['[Total]'] = intval($blacklist_total);
	$dashboard_mqf['[Total]'] = intval($mqf_total);
	
?>

<strong class="medium_strong">ELR Queue Overview <em>(Current queue for messages received during reporting period; Excludes blacklisted messages)</em></strong>
<div id="summary_chart" style="margin-top: 15px; height: 100%;"></div>

<strong class="medium_strong">Disease Summary</strong>
<div id="condition_chart" style="margin-top: 15px; height: 100%;"></div>

<strong class="medium_strong">Assigned Labs by CDC Category</strong>
<div id="assigned_chart" style="margin-top: 15px; height: 100%;"></div>

<strong class="medium_strong"># of Messages Blacklisted by Laboratory</strong>
<div id="blacklist_summary_chart" style="margin-top: 15px; height: 100%;"></div>

<strong class="medium_strong">Duplicate Labs Discarded by CDC Category</strong>
<div id="discardcase_chart" style="margin-top: 15px; height: 100%;"></div>

<strong class="medium_strong">Labs Graylisted by CDC Category</strong>
<div id="graylist_summary_chart" style="margin-top: 15px; height: 100%;"></div>

<strong class="medium_strong">Messages with QA Errors</strong>
<div id="mqf_chart" style="margin-top: 15px; height: 100%;"></div>

<strong class="medium_strong">Automation Percentage <em>(For assigned messages)</em></strong>
<div id="autofactor_chart" style="margin-top: 15px; height: 100%;"></div>

<script type='text/javascript'>
	function drawChart() {
		
		var summaryData = new google.visualization.DataTable();
		var conditionData = new google.visualization.DataTable();
		var assignedLabData = new google.visualization.DataTable();
		var discardCaseData = new google.visualization.DataTable();
		var graylistData = new google.visualization.DataTable();
		var blacklistData = new google.visualization.DataTable();
		var mqfData = new google.visualization.DataTable();
		var autoFactorData = new google.visualization.DataTable();
		summaryData.addColumn('string', 'Message Queue');
		summaryData.addColumn('number', '# of Messages');
		conditionData.addColumn('string', 'Condition');
		conditionData.addColumn('number', 'Messages Received');
		conditionData.addColumn('number', 'Assigned Labs');
		assignedLabData.addColumn('string', 'Disease Category');
		assignedLabData.addColumn('number', 'Lab Created New CMR');
		assignedLabData.addColumn('number', 'Lab Updated Event');
		assignedLabData.addColumn('number', 'Total Assigned');
		discardCaseData.addColumn('string', 'Disease Category');
		discardCaseData.addColumn('number', 'Cases Discarded');
		graylistData.addColumn('string', 'Disease Category');
		graylistData.addColumn('number', 'Cases Graylisted');
		blacklistData.addColumn('string', 'Lab');
		blacklistData.addColumn('number', 'Messages Blacklisted');
		mqfData.addColumn('string', 'Quality Issue');
		mqfData.addColumn('number', 'Messages with Issue');
		autoFactorData.addColumn('string', 'Method Processed');
		autoFactorData.addColumn('number', 'Number of Messages');
		autoFactorData.addColumn('string', 'Percentage');
	<?php
		foreach ($dashboard_summary as $summary_slice => $summary_slice_count) {
			echo "summaryData.addRow(['".$summary_slice."', ".intval($summary_slice_count)."]);".PHP_EOL;
		}
		foreach ($condition_summary as $condition_summary_slice => $condition_summary_slice_data) {
			echo "conditionData.addRow(['".$condition_summary_slice."', ".intval($condition_summary_slice_data['total_received']).", ".intval($condition_summary_slice_data['total_labs'])."]);".PHP_EOL;
		}
		foreach ($dashboard_appendcase as $appendcase_slice => $appendcase_slice_count) {
			echo "assignedLabData.addRow(['".$appendcase_slice."', ".intval($appendcase_slice_count['new_cmr']).", ".intval($appendcase_slice_count['labs_updated']).", ".((intval($appendcase_slice_count['new_cmr']))+(intval($appendcase_slice_count['labs_updated'])))."]);".PHP_EOL;
		}
		foreach ($dashboard_discardcase as $discardcase_slice => $discardcase_slice_count) {
			echo "discardCaseData.addRow(['".$discardcase_slice."', ".intval($discardcase_slice_count)."]);".PHP_EOL;
		}
		foreach ($dashboard_graylist_summary as $graylist_summary_slice => $graylist_summary_slice_count) {
			echo "graylistData.addRow(['".$graylist_summary_slice."', ".intval($graylist_summary_slice_count)."]);".PHP_EOL;
		}
		foreach ($dashboard_blacklist_summary as $blacklist_summary_slice => $blacklist_summary_slice_count) {
			echo "blacklistData.addRow(['".$blacklist_summary_slice."', ".intval($blacklist_summary_slice_count)."]);".PHP_EOL;
		}
		foreach ($dashboard_mqf as $mqf_slice => $mqf_count) {
			echo "mqfData.addRow(['".$mqf_slice."', ".intval($mqf_count)."]);".PHP_EOL;
		}
		foreach ($dashboard_automation_factor2 as $auto_factor_slice => $auto_factor_slice_data) {
			echo "autoFactorData.addRow(['".$auto_factor_slice."', ".intval($auto_factor_slice_data['count']).", '".$auto_factor_slice_data['percent']."']);".PHP_EOL;
		}
	?>
		var summaryOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sort: 'disable'
		};
		var conditionOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sortColumn: 1, 
			sortAscending: false
		};
		var assignedOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sort: 'disable'
		};
		var discardCaseOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sort: 'disable'
		};
		var blacklistSummaryOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sort: 'disable'
		};
		var graylistSummaryOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sort: 'disable'
		};
		var mqfOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sort: 'disable'
		};
		var autoFactorOptions = {
			fontName: 'Open Sans',
			fontSize: 10,
			legend: { position: 'top', maxLines: 5 }, 
			tooltip: { showColorCode: true }, 
			sort: 'disable'
		};
		
		var summaryChart = new google.visualization.Table(document.getElementById('summary_chart'));
		var conditionChart = new google.visualization.Table(document.getElementById('condition_chart'));
		var assignedChart = new google.visualization.Table(document.getElementById('assigned_chart'));
		var discardCaseChart = new google.visualization.Table(document.getElementById('discardcase_chart'));
		var blacklistSummaryChart = new google.visualization.Table(document.getElementById('blacklist_summary_chart'));
		var graylistSummaryChart = new google.visualization.Table(document.getElementById('graylist_summary_chart'));
		var mqfChart = new google.visualization.Table(document.getElementById('mqf_chart'));
		var autoFactorChart = new google.visualization.Table(document.getElementById('autofactor_chart'));
		summaryChart.draw(summaryData, summaryOptions);
		conditionChart.draw(conditionData, conditionOptions);
		assignedChart.draw(assignedLabData, assignedOptions);
		discardCaseChart.draw(discardCaseData, discardCaseOptions);
		blacklistSummaryChart.draw(blacklistData, blacklistSummaryOptions);
		graylistSummaryChart.draw(graylistData, graylistSummaryOptions);
		mqfChart.draw(mqfData, mqfOptions);
		autoFactorChart.draw(autoFactorData, autoFactorOptions);
	};
</script>