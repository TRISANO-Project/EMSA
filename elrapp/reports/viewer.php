<?php
	
	// prevent caching...
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');
	
	include '../includes/app_config.php';
	include WEBROOT_URL.'/includes/dashboard_functions.php';
	
	$report_name = false;
	
	if (isset($_POST['report_id']) && !empty($_POST['report_id'])) {
		switch ($_POST['report_id']) {
			case 'qa_flags':
			case 'other_flag_reasons':
			case 'de_flag_reasons':
			case 'orphaned_messages':
			case 'discarded_messages':
			case 'dashboard_tabular':
				$report_name = filter_var($_POST['report_id'], FILTER_SANITIZE_STRING);
				break;
			default:
				$report_name = false;
		}
	}
	
	if ($report_name !== false) {
		$_SESSION['reporting_current_report'] = $report_name;
		include 'reports/'.$report_name.'.php';
	} else {
		header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error", TRUE, 500);  // no record found with that id
	}			
	
?>