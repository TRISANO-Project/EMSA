<?php

	/**
	 * Allows user to manually select from one of their available roles
	 * so that only that role's diseases are used for filtering/EMSA queues
	 */
	 
	$check_override_role = filter_var($_POST['override_role'], FILTER_SANITIZE_NUMBER_INT);
	
	if (($check_override_role > 0) && isset($_SESSION['user_system_roles']) && is_array($_SESSION['user_system_roles']) && in_array($check_override_role, $_SESSION['user_system_roles'])) {
		// role ID passed is a valid role for this user
		$_SESSION['override_user_role'] = $check_override_role;
	} else {
		unset($_SESSION['override_user_role']);
	}

?>