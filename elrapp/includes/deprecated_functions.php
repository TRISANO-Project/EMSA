<?php

	/*
	 * Functions not currently in use, but preserving in case of future desired functionality.
	 */
	 
	function getSelectHtml($sql, $name, $select_val, $select_val_key, $select_val_value) {
		global $host_pa;
		
		$labs = get_db_result_set($host_pa, $sql);
		
		$html = "<select class=\"ui-corner-all\" name=\"".$name."\" id=\"".$name."\">";
		$html .= "<option value=\"-1\">Choose:</option>";
		
		while ($lab = pg_fetch_array($labs)) {
			if ($select_val) {
				if ($select_val == $lab[$select_val_key]) {
					$sel = "SELECTED";
				} else {
					$sel = "";
				}
			} else {
				$sel = "";
			}
			
			$html .= "<option value=\"".$lab[$select_val_key]."\" ".$sel.">".$lab[$select_val_value]."</option>";
		}
		
		$html .= "</select>";
		
		return $html;
	}
	
	
	
	
	/**
	 * Return a boolean true/false indicator of whether a Master Condition
	 * (specified by an Application-specific coded value) is a surveillance event or not.
	 *
	 * @param string $disease Application-specific disease identifier.
	 * @param int $app_id (Optional) Integer specifying which Application to match disease against.  Default value is 1 (TriSano).
	 * @return bool
	 */
	function isSurveillanceEvent($disease = null, $app_id = 1) {
		global $host_pa, $my_db_schema;
		
		if (is_null($disease) || (strlen(trim($disease)) < 1)) {
			trigger_error("isSurveillanceEvent() requires a coded disease value in parameter 0, but a null or empty value was found.", E_USER_WARNING);
		} else {
			$qry = "SELECT c.surveillance AS surveillance FROM ".$my_db_schema."vocab_master_condition c INNER JOIN ".$my_db_schema."vocab_master2app a ON (c.condition = a.master_id AND a.app_id = ".intval($app_id)." AND a.coded_value = '".pg_escape_string(trim($disease))."');";
			$rs = @pg_query($host_pa, $qry);
			if ($rs) {
				$surveillance_flag = @pg_fetch_result($rs, 0, surveillance);
				if ($surveillance_flag == "t") {
					@pg_free_result($rs);
					return true;
				} else {
					@pg_free_result($rs);
					return false;
				}
			} else {
				trigger_error("Could not connect to database.", E_USER_ERROR);
			}
		}
	}
	
	
	
	
	/**
	 * Indicates whether the logged-in user is a member of the ELR role specified by name.
	 * Returns TRUE if user is a member of one or more of the specified roles, or FALSE if user is not a member (or if no role name specified)
	 *
	 * @param array $role_names Array of role names to verify membership in.
	 * @return bool
	 */
	function roleMembershipByName($role_names = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($role_names) || !is_array($role_names) || (count($role_names) < 1)) {
			return false;
		}
		
		$is_member = false;
		
		foreach ($role_names as $role_name) {
			unset($qry);
			unset($this_role_id);
			$qry = "SELECT master_role_id FROM ".$my_db_schema."system_roles WHERE name ILIKE '".pg_escape_string(trim($role_name))."';";
			$this_role_id = intval(@pg_fetch_result(@pg_query($host_pa, $qry), 0, "master_role_id"));
			if (in_array($this_role_id, $_SESSION['user_roles'])) {
				$is_member = true;
			}
		}
		
		return $is_member;
	}
	
	
	
	
	/**
	 * Returns the TriSano ID for a given Description value from the _SESSION-ized TriSano External Codes table.
	 *
	 * @param string $table Table name to get value from
	 * @param string $description Description to search for
	 * @return int TriSano ID number for use in NEDSS XML
	 */
	function setExternalCodeValueByDescription($table = null, $description = null) {
		if (is_null($table) || is_null($description) || empty($table) || empty($description)) {
			return -1;
		}
		
		$result = -1;
		if (isset($_SESSION['trisano_codes']['external_codes']) && is_array($_SESSION['trisano_codes']['external_codes'])) {
			foreach ($_SESSION['trisano_codes']['external_codes'][trim($table)] as $id => $vals) {
				if ($vals['code_description'] == trim($description)) {
					$result = intval($id);
				}
			}
		}
		return $result;
	}
	
	
	
	
	function whitelistArrayToEventIdString($whitelist_array) {
		$event_list = array();
		$event_string = '';
		foreach ($whitelist_array as $whitelist_event_id => $whitelist_person_id) {
			$event_list[] = trisanoRecordNumberByEventId($whitelist_event_id);
		}
		$event_string = implode(', ', $event_list);
		return $event_string;
	}
	
	
	
	
	

?>