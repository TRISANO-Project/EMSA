<?php
	/**
	 * Read a java-type properties file
     * @return a hash table containing
	 * the properties as key/value pairs
	 */
	function read_properties($properties_file) {
	
		$result = array();
		
		$buffer = file_get_contents($properties_file);
		if(!$buffer) {
			die('<div class="import_widget ui-widget import_error ui-state-error ui-corner-all" style="padding: 5px;"><span class="ui-icon ui-icon-elrerror" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;"><strong>'
			.'Could not read '.$properties_file.
			'</strong></p></div>');
		}

		$lines = explode("\n", $buffer);
		$key = "";

		$is_waiting_other_line = false;
		foreach($lines as $i=>$line) {

			if(empty($line) || (!$is_waiting_other_line && strpos($line,"#") === 0)) 
				continue;

			if(!$is_waiting_other_line) {
				$key = substr($line,0,strpos($line,'='));
				$value = substr($line,strpos($line,'=') + 1, strlen($line));
			}
			else {
				$value .= $line;
			}
			$value = trim($value);
			
			/* Check if ends with single '\' */
			if(strrpos($value,"\\") === strlen($value)-strlen("\\")) {
				$value = substr($value, 0, strlen($value)-1)."\n";
				$is_waiting_other_line = true;
			}
			else {
				$is_waiting_other_line = false;
			}

			$value = trim($value);
			$result[$key] = $value;
			unset($lines[$i]);
		}
		return $result;
	}	

    $props = read_properties('/var/lib/elr_application.properties');
//$debug_output = '';
//foreach ($props as $k=>$v)
//{
//   $debug_output = $debug_output . $k . ':' . $v . '|';
//}
//error_log( "props >>>>" . $debug_output);

	date_default_timezone_set($props['time_zone']);

	$my_db_schema = 'elr.';
	$db_schema_select = 'SELECTED';
	$import_folder_path = './uploads';
	
	if ($server_environment == ELR_ENV_PROD) {
		/*
		 * Production Environment Configuration
		 */
		define('WEBROOT_URL', '/var/www/html/elrapp');
		define('EXPORT_SERVERNAME', 'Prod');
		define('LOGOUT_URL', '/phaccess');
		define('MAIN_URL', '/elrapp');
		define('NOACCESS_URL', '/elrapp/noaccess.php');
		
		define('ELR_NOTIFY_ROLE', 18);
		define('ELR_ADMIN_ROLE', 12);
		
		define('EMSA_FLAG_INVESTIGATION_COMPLETE', 2);
		define('EMSA_FLAG_CLEAN_DATA', 4);
		define('EMSA_FLAG_QA_MANDATORY', 8);
		define('EMSA_FLAG_QA_CODING', 16);
		define('EMSA_FLAG_QA_MQF', 32);
		define('EMSA_FLAG_DE_ERROR', 64);
		define('EMSA_FLAG_FIX_DUPLICATE', 128);
		define('EMSA_FLAG_DE_OTHER', 256);
		define('EMSA_FLAG_DE_NEEDFIX', 512);
		
		#turn automated entry on or off
		#true = ON; false = OFF
		define('AUTOMATION_ENABLED', true);
	} elseif ($server_environment == ELR_ENV_DEV) {
		/*
		 * Development Environment Configuration
		 */
		define('WEBROOT_URL', '/var/www/html/elrapp');
		define('EXPORT_SERVERNAME', 'Dev');
		define('LOGOUT_URL', '/elrapp');
		define('MAIN_URL', '/elrapp');
		define('NOACCESS_URL', '/elrapp/noaccess.php');
		
		define('ELR_NOTIFY_ROLE', 13);
		define('ELR_ADMIN_ROLE', 12);
		
		define('EMSA_FLAG_INVESTIGATION_COMPLETE', 4);
		define('EMSA_FLAG_CLEAN_DATA', 8);
		define('EMSA_FLAG_QA_MANDATORY', 16);
		define('EMSA_FLAG_QA_CODING', 32);
		define('EMSA_FLAG_QA_MQF', 64);
		define('EMSA_FLAG_DE_ERROR', 128);
		define('EMSA_FLAG_FIX_DUPLICATE', 256);
		define('EMSA_FLAG_DE_OTHER', 512);
		define('EMSA_FLAG_DE_NEEDFIX', 1024);
		
		#turn automated entry on or off
		#true = ON; false = OFF
		define('AUTOMATION_ENABLED', false);
	} else {
		/*
		 * Test Environment Configuration
		 */
		define('WEBROOT_URL', '/srv/www/htdocs/elrapp');
		define('EXPORT_SERVERNAME', 'Test');
		define('LOGOUT_URL', '/phaccess');
		define('MAIN_URL', '/elrapp');
		define('NOACCESS_URL', '/elrapp/noaccess.php');
		
		define('ELR_NOTIFY_ROLE', 13);
		define('ELR_ADMIN_ROLE', 12);
		
		define('EMSA_FLAG_INVESTIGATION_COMPLETE', 4);
		define('EMSA_FLAG_CLEAN_DATA', 8);
		define('EMSA_FLAG_QA_MANDATORY', 16);
		define('EMSA_FLAG_QA_CODING', 32);
		define('EMSA_FLAG_QA_MQF', 64);
		define('EMSA_FLAG_DE_ERROR', 128);
		define('EMSA_FLAG_FIX_DUPLICATE', 256);
		define('EMSA_FLAG_DE_OTHER', 512);
		define('EMSA_FLAG_DE_NEEDFIX', 1024);
		
		#turn automated entry on or off
		#true = ON; false = OFF
		define('AUTOMATION_ENABLED', true);
	}

?>
