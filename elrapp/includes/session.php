<?php

	if (!isset($_SESSION)) {
		session_start();
		if (!isset($_SESSION['CREATED'])) {
			// invalidate old session data and ID
			session_regenerate_id(true);
			$_SESSION['CREATED'] = time();
		}	
	}
	
	if (isset($_REQUEST['pid']) && $_REQUEST['pid']) {
		$_SESSION['pid'] = $_REQUEST['pid'];
	}
	
	if (isset($_REQUEST['admin']) && $_REQUEST['admin']) {
		$_SESSION['admin'] = $_REQUEST['admin'];
	}

?>