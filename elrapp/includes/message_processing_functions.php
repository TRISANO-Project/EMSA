<?php


	/**
	 * Wrapper for bulk message retrying.  Requires an array of system_message IDs.
	 *
	 * @param array $system_message_id_arr Array containing ID numbers of the messages in system_messages to retry
	 * @return void
	 */
	function bulkRetryWrapper($system_message_id_arr) {
		global $retry_results, $props;

		$retry_results = array(
			'success' => array(),
			'error' => array(),
			'exception' => array(),
			'exception_list' => array(),
			'details' => array()
		);

		highlight(trim(count($system_message_id_arr)).' messages found to retry...', 'ui-icon-elrretry');

		foreach ($system_message_id_arr as $retry_msg) {
			switch (bulkRetryByMessageId($retry_msg)) {
				case -1: // error
					$retry_results['error'][] = $retry_msg;
					break;
				case 0: // exception
					$retry_results['exception'][] = $retry_msg;
					break;
				case 1: // success
					$retry_results['success'][] = $retry_msg;
					break;
			}
		}

		$distinct_exceptions = array();
		if (count($retry_results['exception']) > 0) {
			foreach ($retry_results['exception_list'] as $distinct_exception => $distinct_exception_count) {
				$distinct_exceptions[] = $distinct_exception;
			}
			$distinct_exceptions_string = '<br><em style="font-size: 9pt;">[<strong>Exception(s) Found:</strong>  '.implode(', ', $distinct_exceptions).']</em>';
		} else {
			$distinct_exceptions_string = '';
		}

		highlight('<strong>Result Summary:</strong><ul><li>'.trim(count($retry_results['success'])).' messages processed successfully,</li><li>'.trim(count($retry_results['exception'])).' messages still contain exceptions,'.$distinct_exceptions_string.'</li><li>'.trim(count($retry_results['error'])).' messages failed to retry due to a system error</li></ul>', ((count($retry_results['success']) == count($retry_msgs)) ? 'ui-icon-elrsuccess' : 'ui-icon-elrerror'));

		$detailed_results = <<<EOH
<table class="audit_log">
		<thead>
			<tr style="border-bottom: 2px darkgray solid;">
				<th style="text-align: left;">Message ID</th>
				<th style="text-align: left;">Status</th>
				<th style="text-align: left;">Error Details</th>
			</tr>
		</thead>
		<tbody>
EOH;
		foreach ($retry_results['details'] as $detail_msg_id => $detail_msg_exceptions) {
			$detailed_results .= '<tr style="border-bottom: 1px darkgray dotted;"><td style="vertical-align: top;"><a href="'.MAIN_URL.'/?selected_page=6&submenu=6&focus='.trim($detail_msg_id).'" target="_blank">'.trim($detail_msg_id).'</a></td><td style="vertical-align: top;">';
			if (intval($detail_msg_exceptions['status']) == -1) {
				$detailed_results .= '<span class="ui-icon ui-icon-elrstop" style="display: inline-block; padding: 0px 5px 2px 0px; vertical-align: middle;"></span>System Error';
			} elseif (intval($detail_msg_exceptions['status']) == 0) {
				$detailed_results .= '<span class="ui-icon ui-icon-elrerror" style="display: inline-block; padding: 0px 5px 2px 0px; vertical-align: middle;"></span>Exception';
			} else {
				$detailed_results .= '<span class="ui-icon ui-icon-elrsuccess" style="display: inline-block; padding: 0px 5px 2px 0px; vertical-align: middle;"></span>Success';
			}
			$detailed_results .= '</td><td><table class="audit_log"><tbody>';
			foreach ($detail_msg_exceptions['exceptions'] as $exception_key => $exception_details) {
				$detailed_results .= '<tr><td>'.htmlentities($exception_details['exception']).' ('.htmlentities($exception_details['info']).')</td></tr>';
			}
			$detailed_results .= '</tbody></table></td></tr>';
		}
		$detailed_results .= '</tbody></table>';
		highlight('<button type="button" id="view_detailed_results">Show/Hide Detailed Results</button><div id="detailed_results" style="display: none;">'.$detailed_results.'</div>', 'ui-icon-elroptions');
	}




	/**
	 * Attempts to re-process an excepted message from ELR through the Master Save process
	 * in order to revalidate & clear exceptions.  Used from the bulk "E-Task" list.
	 *
	 * Returns -1 on error, 0 if exception persists after revalidation, or 1 on successful Master Save process.
	 *
	 * @param int $system_message_id ID number of the message in system_messages to retry
	 * @return int
	 */
	function bulkRetryByMessageId($system_message_id) {
		global $host_pa, $my_db_schema, $retry_results, $props;
		$xml = null;

		// ensure a valid message ID was passed
		if (!filter_var($system_message_id, FILTER_VALIDATE_INT)) {
			return -1;
		}

		// get a handle to the current message in $retry_results for logging retry details
		if (isset($retry_results['details'][filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT)])) {
			$retry_details = &$retry_results['details'][filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT)];
		} else {
			$retry_results['details'][filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT)] = array(
				'status' => null,
				'exceptions' => array()
			);
			$retry_details = &$retry_results['details'][filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT)];
		}

		$sql = 'SELECT master_xml, final_status FROM '.$my_db_schema.'system_messages WHERE id = '.filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT).';';
		$rs = @pg_query($host_pa, $sql);
		if ($rs === false || @pg_num_rows($rs) !== 1) {
			$retry_details['status'] = -1;
			$retry_details['exceptions'][] = array('exception' => 'Message not found', 'info' => null);
			@pg_free_result($rs);
			return -1;
		} else {
			$xml = trim(@pg_fetch_result($rs, 0, 'master_xml'));
			$msg_type = intval(@pg_fetch_result($rs, 0, 'final_status'));
			@pg_free_result($rs);
		}

		if (is_null($xml) || empty($xml)) {
			$retry_details['status'] = -1;
			$retry_details['exceptions'][] = array('exception' => 'XML does not exist', 'info' => null);
			return -1;
		} else {
			$sxe = simplexml_load_string($xml);
			unset($sxe->exceptions);  // no longer used, clean up to avoid confusion -- exceptions stored in db

			$xmlstring = $sxe->asXML();

			// send updated master xml through master save process
			$encoded_masterxml = urlencode(str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $xmlstring));

			$xml_for_masterservice = new SimpleXMLElement("<health_message></health_message>");
			$xml_for_masterservice->addChild('user_id', '9999');
			$xml_for_masterservice->addChild('system_message_id', filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT));
			$xml_for_masterservice->addChild('health_xml', $encoded_masterxml);

			#debug echo "<hr><strong>Debug -- Sent XML:<br>".htmlentities($xml_for_masterservice->asXML())."</strong><hr>";

			try {
				$master_client = @new SoapClient($props['master_wsdl_url']);
			} catch (Exception $e) {
				$retry_details['status'] = -1;
				$retry_details['exceptions'][] = array('exception' => 'SOAP Error', 'info' => $e->getMessage());
				return -1;
			} catch (SoapFault $f) {
				$retry_details['status'] = -1;
				$retry_details['exceptions'][] = array('exception' => 'SOAP Error', 'info' => $f->getMessage());
				return -1;
			}

			if ($master_client) {
				try {
					$master_result = $master_client->saveMaster(array('healthMessage' => $xml_for_masterservice->asXML()));
				} catch (Exception $e) {
					$retry_details['status'] = -1;
					$retry_details['exceptions'][] = array('exception' => 'SOAP Error', 'info' => $e->getMessage());
					return -1;
				} catch (SoapFault $f) {
					$retry_details['status'] = -1;
					$retry_details['exceptions'][] = array('exception' => 'SOAP Error', 'info' => $f->getMessage());
					return -1;
				}
				$master_return = simplexml_load_string($master_result->return);
				if (getTrisanoReturnStatus($master_return->status_message)->status) {
					auditMessage(filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT), 21, $msg_type);

					$current_sql = 'SELECT se.description AS description, sme.info AS info, ss.name AS type FROM '.$my_db_schema.'system_message_exceptions sme
						INNER JOIN '.$my_db_schema.'system_exceptions se ON (sme.exception_id = se.exception_id)
						INNER JOIN '.$my_db_schema.'system_statuses ss ON (se.exception_type_id = ss.id)
						WHERE sme.system_message_id = '.filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT).'
						ORDER BY sme.id;';
					$current_rs = @pg_query($host_pa, $current_sql);
					if ($current_rs) {
						if (pg_num_rows($current_rs) > 0) {
							$retry_details['status'] = 0;
							while ($current_row = @pg_fetch_object($current_rs)) {
								$retry_details['exceptions'][] = array('exception' => $current_row->description, 'info' => $current_row->info);
								if (isset($retry_results['exception_list'][$current_row->description])) {
									$retry_results['exception_list'][$current_row->description]++;
								} else {
									$retry_results['exception_list'][$current_row->description] = 1;
								}
							}
							@pg_free_result($current_rs);
							return 0;
						} else {
							@pg_free_result($current_rs);
							$retry_details['status'] = 1;
							return 1;
						}
					} else {
						$retry_details['status'] = -1;
						$retry_details['exceptions'][] = array('exception' => 'PostgreSQL Error', 'info' => trim(@pg_last_error()));
						return -1;
					}

				} else {
					$retry_details['status'] = -1;
					$retry_details['exceptions'][] = array('exception' => 'TriSano Error', 'info' => getTrisanoReturnStatus($master_return->status_message)->errors);
					return -1;
				}
			}
		}
	}




	/**
	 * Checks to see if a specified message is still active in the specified queue (not deleted, moved, assigned, etc.)
	 * clearing the way for the message to be processed by action handlers (edit, retry, addCmr, updateCmr, etc.).  Prevents changes to
	 * messages that may have already been processed by another user or process, as well as supresses accidental duplicate
	 * message processing due to EMSA page reloads.
	 *
	 * Returns TRUE if message is not deleted and in same 'final_status' as specified, FALSE otherwise
	 *
	 * @param int $system_message_id ID number of the message in system_messages
	 * @param int $indicated_queue ID of the EMSA queue ('type') that the message processing was attempted from
	 * @return bool
	 */
	function messageUnprocessed($system_message_id, $indicated_queue) {
		global $host_pa, $my_db_schema;

		if (!filter_var($system_message_id, FILTER_VALIDATE_INT) || !filter_var($indicated_queue, FILTER_VALIDATE_INT)) {
			return false;
		}

		$sql = 'SELECT final_status FROM '.$my_db_schema.'system_messages
				WHERE (id = '.intval($system_message_id).')
				AND (final_status = '.intval($indicated_queue).')
				AND ((deleted IS NULL) OR (deleted = 0));';
		$rs = @pg_query($host_pa, $sql);
		if ($rs === false) {
			return false;
		} elseif (@pg_num_rows($rs) === 1) {
			@pg_free_result($rs);
			return true;
		} else {
			@pg_free_result($rs);
			return false;
		}
	}




	/**
	 * Indicates whether a message should generate a new CMR based upon Master LOINC code & interpreted test result.
	 *
	 * @param string $loinc Master LOINC Code
	 * @param string $test_result Test result name from Master XML
	 * @param int $app_id Application ID (Optional, Default NEDSS)
	 * @return int Returns ALLOW_CMR_YES if a new CMR should be created, ALLOW_CMR_NO_NEG if the message should be graylisted due to negative results,
	 * or ALLOW_CMR_NO_INFORM if message should be graylisted due to informative test results.
	 * Returns ALLOW_CMR_ERR_* if error.
	 */
	function allowNewCMR($loinc = null, $test_result = null, &$master_xml, $app_id = 1) {
		global $host_pa, $my_db_schema, $props;

		if($props['emsa_environment'] == SNHD) {
			// look for a quest message where the patient name is a test order id number
			// something like A1934963, leave these in an entry queue so the DIIS can
			// find the correct event and assign it
			if($master_xml->labs->lab == "QUEST" && preg_match("/^A[0-9]/", $master_xml->person->last_name) === 1 ) {
				return ALLOW_CMR_NO_QUEST_ID_AS_NAME;
			}
		}

		$test_result_verbose = trim($test_result);
		$master_loinc = trim($loinc);

		if (empty($test_result_verbose) || empty($master_loinc)) {
			return ALLOW_CMR_ERR_XML_REQ_FIELDS;
		}

		$positive_master_id = trisanoResultMasterVocabIdByName('POSITIVE', intval($app_id));
		$result_master_id = trisanoResultMasterVocabIdByName($test_result_verbose, intval($app_id));
		$is_positive = (($positive_master_id == $result_master_id) ? true : false);

		$rules = array();
		$matched_rules = array();

		$sql = 'SELECT r.allow_new_cmr AS allow_new_cmr, r.conditions_structured AS conditions_structured
			FROM '.$my_db_schema.'vocab_rules_masterloinc r
			INNER JOIN '.$my_db_schema.'vocab_master_loinc ml ON (r.master_loinc_id = ml.l_id)
			WHERE r.app_id = '.intval($app_id).'
			AND ml.loinc = \''.pg_escape_string($master_loinc).'\';';
		$rs = @pg_query($host_pa, $sql);
		if (($rs === false) || (@pg_num_rows($rs) < 1)) {
			return ALLOW_CMR_ERR_NO_RULES_DEFINED;
		}

		while ($row = @pg_fetch_object($rs)) {
			$rules[] = array(
				'test' => @json_decode($row->conditions_structured),
				'new_cmr' => ((trim($row->allow_new_cmr) == 't') ? true : false)
			);
		}

		foreach ($rules as $rule) {
			unset($this_condition);
			$this_condition = true;
			foreach ($rule['test'] as $test) {
				unset($this_operator);
				$this_operator = operatorById($test->operator);
				switch ($this_operator) {
					case '==':
						$this_condition = $this_condition && ($result_master_id == $test->operand);
						break;
					case '!=':
						$this_condition = $this_condition && ($result_master_id != $test->operand);
						break;
					case '>':
						$this_condition = $this_condition && ($result_master_id > $test->operand);
						break;
					case '<':
						$this_condition = $this_condition && ($result_master_id < $test->operand);
						break;
					case '>=':
						$this_condition = $this_condition && ($result_master_id >= $test->operand);
						break;
					case '<=':
						$this_condition = $this_condition && ($result_master_id <= $test->operand);
						break;
					default:
						$this_condition = $this_condition && false;
						break;
				}
			}

			if ($this_condition) {
				$matched_rules[] = $rule['new_cmr'];
			}
		}

		if (count($matched_rules) == 1) {
			$bool_new_cmr = reset($matched_rules);
			if ($bool_new_cmr) {
				return ALLOW_CMR_YES;  // create new cmr
			} elseif ($is_positive) {
				return ALLOW_CMR_NO_INFORM;  // no, informative
			} else {
				return ALLOW_CMR_NO_NEG;  // no, negative
			}
		} elseif (count($matched_rules) < 1) {
			return ALLOW_CMR_ERR_NO_RULES_MATCHED;  // no matched rules
		} elseif (count($matched_rules) > 1) {
			return ALLOW_CMR_ERR_MULT_RULES_MATCHED;  // too many rules matched
		}
	}




	/**
	 * Checks to see if the condition for the message specifies valid specimen sources,
	 * and if the specimen source for the incoming message is valid.
	 *
	 * @param string $child_loinc Child LOINC Code
	 * @param string $result_value Child result value
	 * @param string $specimen_source Specimen source from Master XML
	 * @param int $lab_id
	 * @return int SPECIMEN_VALID if a valid specimen source, SPECIMEN_INVALID if an invalid specimen source,
	 *   or SPECIMEN_EXCEPTION if one of valid or invalid specimen sources defined and provided specimen source is not in the list
	 */
	function isValidSpecimenSource($child_loinc, $result_value, $specimen_source = null, $lab_id) {
		global $host_pa, $my_db_schema;
		$is_valid = SPECIMEN_EXCEPTION;
		$valid_specimens = array();
		$invalid_specimens = array();

		$specimens = '';
		$i_specimens = '';

		// get condition ID, see if list of specimen sources exists
		$qry = 'SELECT valid_specimen, invalid_specimen FROM '.$my_db_schema.'vocab_master_condition WHERE c_id = '.$my_db_schema.'loinc_condition_id(\''.@pg_escape_string($child_loinc).'\', \''.@pg_escape_string($result_value).'\', '.intval($lab_id).');';
		$rs = @pg_query($host_pa, $qry);
		if ($rs !== false) {
			$specimens = implode(',', explode(';', trim(@pg_fetch_result($rs, 0, 'valid_specimen'))));
			$i_specimens = implode(',', explode(';', trim(@pg_fetch_result($rs, 0, 'invalid_specimen'))));
		}

		if (strlen(trim($specimens)) > 0) {
			$qry2 = 'SELECT coded_value FROM '.$my_db_schema.'vocab_master2app WHERE master_id IN ('.trim($specimens).');';
			$rs2 = @pg_query($host_pa, $qry2);
			if ($rs2 === false || (@pg_num_rows($rs2) < 1)) {
				$is_valid = SPECIMEN_EXCEPTION;
			} else {
				while ($row = @pg_fetch_object($rs2)) {
					$valid_specimens[] = trim($row->coded_value);
				}
				if (!empty($specimen_source) && count($valid_specimens) > 0 && in_array($specimen_source, $valid_specimens)) {
					$is_valid = SPECIMEN_VALID;
				}
			}
		}

		if (strlen(trim($i_specimens)) > 0) {
			$qry2 = 'SELECT coded_value FROM '.$my_db_schema.'vocab_master2app WHERE master_id IN ('.trim($i_specimens).');';
			$rs2 = @pg_query($host_pa, $qry2);
			if ($rs2 === false || (@pg_num_rows($rs2) < 1)) {
				$is_valid = SPECIMEN_EXCEPTION;
			} else {
				while ($row = @pg_fetch_object($rs2)) {
					$invalid_specimens[] = trim($row->coded_value);
				}
				if (!empty($specimen_source) && count($invalid_specimens) > 0 && in_array($specimen_source, $invalid_specimens)) {
					$is_valid = SPECIMEN_INVALID;
				}
			}
		}

		if ((strlen(trim($specimens)) < 1) && (strlen(trim($i_specimens)) < 1)) {
			$is_valid = SPECIMEN_VALID; // no valid or invalid specimen sources defined, accept all the things!
		}
		return $is_valid;
	}




	/**
	 * Indicates whether a message should create a surveillance event or not, based upon Master LOINC code & interpreted test result.
	 *
	 * @param string $loinc Master LOINC Code
	 * @param string $organism Organism name from Master XML
	 * @param string $test_result Test result name from Master XML
	 * @param int $app_id Application ID (Optional, Default NEDSS)
	 * @return int Returns IS_SURVEILLANCE_YES if a Surveillance event is mandated, IS_SURVEILLANCE_NO if not.
	 * Returns IS_SURVEILLANCE_ERR_* if error.
	 */
	function isSurveillance($loinc = null, $organism = null, $test_result = null, $app_id = 1) {
		global $host_pa, $my_db_schema;

		$test_result_verbose = trim($test_result);
		$master_loinc = trim($loinc);
		$org_name = trim($organism);

		if (empty($master_loinc)) {
			return IS_SURVEILLANCE_ERR_REQFIELD_LOINC;
		}

		// check if master LOINC looks up organism from nominal result
		$nomcheck_sql = 'SELECT l_id
			FROM '.$my_db_schema.'vocab_master_loinc
			WHERE loinc = \''.@pg_escape_string($master_loinc).'\'
			AND organism_from_result IS TRUE;';
		$nomcheck_rs = @pg_query($host_pa, $nomcheck_sql);
		if (($nomcheck_rs !== false) && (@pg_num_rows($nomcheck_rs) > 0)) {
			if (empty($org_name)) {
				// per S. Mottice 2014-01-08, if nominal & organism isn't derived (e.g. "NON_REACT"), probably
				// not initiating a CMR anyway, so default to yes.
				//return IS_SURVEILLANCE_ERR_REQFIELD_ORGNAME;
				return IS_SURVEILLANCE_YES;
			}
			$nomsurv_sql = 'SELECT o.o_id
				FROM '.$my_db_schema.'vocab_master_organism o
				INNER JOIN '.$my_db_schema.'vocab_master_vocab v ON (v.id = o.organism)
				INNER JOIN '.$my_db_schema.'vocab_master2app m2a ON (m2a.master_id = v.id)
				WHERE m2a.app_id = 1
				AND v.category = '.$my_db_schema.'vocab_category_id(\'organism\')
				AND m2a.coded_value ILIKE \''.@pg_escape_string($org_name).'\'
				AND o.nom_is_surveillance IS TRUE;';
			$nomsurv_rs = @pg_query($host_pa, $nomsurv_sql);
			if ($nomsurv_rs !== false) {
				if (@pg_num_rows($nomsurv_rs) === 1) {
					return IS_SURVEILLANCE_YES;
				} elseif (@pg_num_rows($nomsurv_rs) > 1) {
					return IS_SURVEILLANCE_ERR_MULT_ORGS_MATCHED;
				}
			}
			return IS_SURVEILLANCE_NO;
		}


		// if not a nominal organism lookup, check case mgmt rules for master LOINC/test result
		if (empty($test_result_verbose)) {
			return IS_SURVEILLANCE_ERR_REQFIELD_TESTRESULT;
		}

		$positive_master_id = trisanoResultMasterVocabIdByName('POSITIVE', intval($app_id));
		$result_master_id = trisanoResultMasterVocabIdByName($test_result_verbose, intval($app_id));
		$is_positive = (($positive_master_id == $result_master_id) ? true : false);

		$rules = array();
		$matched_rules = array();

		$sql = 'SELECT r.is_surveillance AS is_surveillance, r.conditions_structured AS conditions_structured
			FROM '.$my_db_schema.'vocab_rules_masterloinc r
			INNER JOIN '.$my_db_schema.'vocab_master_loinc ml ON (r.master_loinc_id = ml.l_id)
			WHERE r.app_id = '.intval($app_id).'
			AND ml.loinc = \''.pg_escape_string($master_loinc).'\';';
		$rs = @pg_query($host_pa, $sql);
		if (($rs === false) || (@pg_num_rows($rs) < 1)) {
			return IS_SURVEILLANCE_ERR_NO_RULES_DEFINED;
		}

		while ($row = @pg_fetch_object($rs)) {
			$rules[] = array(
				'test' => @json_decode($row->conditions_structured),
				'is_surveillance' => ((trim($row->is_surveillance) == 't') ? true : false)
			);
		}

		foreach ($rules as $rule) {
			unset($this_condition);
			$this_condition = true;
			foreach ($rule['test'] as $test) {
				unset($this_operator);
				$this_operator = operatorById($test->operator);
				switch ($this_operator) {
					case '==':
						$this_condition = $this_condition && ($result_master_id == $test->operand);
						break;
					case '!=':
						$this_condition = $this_condition && ($result_master_id != $test->operand);
						break;
					case '>':
						$this_condition = $this_condition && ($result_master_id > $test->operand);
						break;
					case '<':
						$this_condition = $this_condition && ($result_master_id < $test->operand);
						break;
					case '>=':
						$this_condition = $this_condition && ($result_master_id >= $test->operand);
						break;
					case '<=':
						$this_condition = $this_condition && ($result_master_id <= $test->operand);
						break;
					default:
						$this_condition = $this_condition && false;
						break;
				}
			}

			if ($this_condition) {
				$matched_rules[] = $rule['is_surveillance'];
			}
		}

		if (count($matched_rules) == 1) {
			$bool_is_surveillance = reset($matched_rules);
			if ($bool_is_surveillance) {
				return IS_SURVEILLANCE_YES;  // create surveillance event
			} else {
				return IS_SURVEILLANCE_NO;  // create morbidity event
			}
		} elseif (count($matched_rules) < 1) {
			return IS_SURVEILLANCE_ERR_NO_RULES_MATCHED;  // no matched rules
		} elseif (count($matched_rules) > 1) {
			return IS_SURVEILLANCE_ERR_MULT_RULES_MATCHED;  // too many rules matched
		}
	}




	function isMasterLoincInQA($loinc_code = null, $lab_id = null) {
		global $host_pa, $my_db_schema, $props;

		if (is_null($loinc_code) || (strlen(trim($loinc_code)) < 1) || is_null($lab_id)) {
			return false;
		}

		$result = false;
		$sql = 'SELECT id FROM '.$my_db_schema.'system_role_loincs_by_lab
				WHERE (master_loinc_code = \''.@pg_escape_string(trim($loinc_code)).'\')
				AND (lab_id = '.intval($lab_id).')
				AND (system_role_id = '.$props['elr_qa_role'].');';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			$result = true;
		}
		@pg_free_result($rs);
		return $result;
	}




	function isChildSnomedInQA($child_snomed = null, $child_loinc = null, $child_test_code = null, $lab_id = null) {
		global $host_pa, $my_db_schema, $props;

		// use child LOINC unless not set, then fall back to child test code
		$child_final = ((is_null($child_loinc) || (strlen(trim($child_loinc)) < 1)) ? $child_test_code : $child_loinc);

		if (is_null($child_snomed) || (strlen(trim($child_snomed)) < 1) || is_null($child_final) || (strlen(trim($child_final)) < 1) || is_null($lab_id)) {
			return false;
		}

		$sql_pre = 'SELECT validate_child_snomed
			FROM '.$my_db_schema.'vocab_child_loinc
			WHERE (lab_id = '.intval($lab_id).')
			AND (child_loinc = \''.@pg_escape_string(trim($child_final)).'\')
			AND (validate_child_snomed IS TRUE);';
		$rs_pre = @pg_query($host_pa, $sql_pre);
		if (($rs_pre === false) || (@pg_num_rows($rs_pre) < 1)) {
			@pg_free_result($rs_pre);
			return false; // this lab does not QA by child SNOMED code for noms
		}

		$result = false;
		$sql = 'SELECT id FROM '.$my_db_schema.'system_role_child_snomeds_by_lab
				WHERE child_snomed = \''.@pg_escape_string(trim($child_snomed)).'\'
				AND lab_id = '.intval($lab_id).'
				AND system_role_id = '.$props['elr_qa_role'].';';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			$result = true;
		}
		@pg_free_result($rs);
		return $result;
	}




	function isLoincInRandomQA($loinc_code = null, $lab_id = null) {
		global $host_pa, $my_db_schema;

		if (is_null($loinc_code) || (strlen(trim($loinc_code)) < 1) || is_null($lab_id)) {
			return false;
		}

		$result = false;
		$sql = 'SELECT id FROM '.$my_db_schema.'system_role_loincs_by_lab
				WHERE (master_loinc_code = \''.@pg_escape_string(trim($loinc_code)).'\')
				AND (lab_id = '.intval($lab_id).')
				AND (system_role_id = 30);';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			$result = true;
		}
		@pg_free_result($rs);

		/*
		#debug
		if ($loinc_code == '43304-5') {
			$result = true;
		}
		*/
		$lottery = rand(1, 100);
		#debug error_log($lottery);
		if ($result & ($lottery <= 5)) {
			return true;
		} else {
			return false;
		}
	}




	function isLoincSemiAutoEntry($child_loinc = null, $child_test_code = null, $lab_id = null) {
		global $host_pa, $my_db_schema;

		// use child LOINC unless not set, then fall back to child test code
		$child_final = ((is_null($child_loinc) || (strlen(trim($child_loinc)) < 1)) ? $child_test_code : $child_loinc);

		if (is_null($lab_id) || is_null($child_final) || (strlen(trim($child_final)) < 1)) {
			return false;
		}

		$result = false;
		$sql = 'SELECT id FROM '.$my_db_schema.'vocab_child_loinc
				WHERE (child_loinc = \''.@pg_escape_string(trim($child_final)).'\')
				AND (lab_id = '.intval($lab_id).')
				AND (semi_auto IS TRUE);';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			$result = true;
		}
		@pg_free_result($rs);
		return $result;
	}




	/**
	 * Return a boolean TRUE/FALSE indicator of whether a Child LOINC indicates pregnancy or not.
	 *
	 * @param string $child_loinc Child LOINC/test code
	 * @param int $lab_id Integer specifying which laboratory to search against.  Defaults to ARUP [id = 1] if none set.
	 * @return bool
	 */
	function isPregnancyIndicated($child_loinc = null, $lab_id = 1) {
		global $host_pa, $my_db_schema;

		if (is_null($child_loinc) || empty($child_loinc)) {
			return false;
		}

		$sql = 'SELECT pregnancy FROM '.$my_db_schema.'vocab_child_loinc WHERE child_loinc = \''.@pg_escape_string(trim($child_loinc)).'\' AND lab_id = '.intval($lab_id).' AND pregnancy IS TRUE;';
		$rs = @pg_query($host_pa, $sql);
		if ($rs === false || @pg_num_rows($rs) != 1) {
			return false;
		} else {
			@pg_free_result($rs);
			return true;
		}
	}




	/**
	 * Get back a TriSano Record Number for a given Event ID Number
	 * If event not found, returns FALSE.
	 *
	 * @param int $event_id TriSano Event ID
	 * @return string|bool
	 */
	function trisanoRecordNumberByEventId($event_id = null) {
		global $props;
		if (!is_null($event_id)) {
			$id = intval(trim($event_id));
			try {
				$soap = @new SoapClient($props['sqla_wsdl_url']);
			} catch (Exception $e) {
				return false;
			} catch (SoapFault $f) {
				return false;
			}

			$qry = <<<EOQ
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health>
		<events>
			<id>$id</id>
		</events>
	</trisano_health>
</health_message>
EOQ;

			if ($soap) {
				try {
					$result = $soap->findEvent(array("healthMessage" => $qry));
				} catch (Exception $e) {
					return false;
				} catch (SoapFault $f) {
					return false;
				}

				$xml = simplexml_load_string($result->return);
				$record_numbers = $xml->xpath("trisano_health/events/record_number");

				foreach ($record_numbers as $record_number_obj => $record_number_val) {
					$record_number = trim($record_number_val);
				}

				unset($xml);
				unset($result);
				unset($soap);

				return $record_number;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}




	/**
	 * Records an ELR notification in the batch_notifications table for later distribution
	 * to affected State/Local Health Departments.  Returns TRUE on success, FALSE on failure.
	 * Notification types are (see 'batch_notification_types' table):
	 * 1:  Notify State Upon Receipt
	 * 2:  Surveillance Event
	 * 3:  Contact Event Lab Updates
	 * 4:  Closed Morbidity Event Lab Updates
	 *
	 * @param int $system_message_id System Message ID for ELR message that triggered the notification
	 * @param int $event_id Event ID
	 * @param string $record_number Record Number
	 * @param int $notify_type Type of notification being logged
	 * @param string $condition Condition Name
	 * @param string $test_type Test Type
	 * @param bool $notify_state Indicates whether to send notification to UDOH (Optional, default FALSE)
	 * @param bool $notify_lhd Indicates whether to send notification to LHD (Optional, default FALSE)
	 * @param int $jurisdiction_id Jurisdiction ID for LHD notification (Optional; Required if $notify_lhd is TRUE)
	 * @param string $event_type Type of NEDSS event ('MorbidityEvent' or 'AssessmentEvent' or 'ContactEvent'; Optional, default 'MorbidityEvent')
	 * @param string $investigator Name of Investigator assigned to event (in case of CMR/Contact update; Optional)
	 * @param string $test_result Test result of the lab triggering the notification (Optional)
	 * @return bool
	 */
	function logNotification($system_message_id = null, $event_id = null, $record_number = null, $notify_type = null, $condition = null, $test_type = null, $notify_state = false, $notify_lhd = false, $jurisdiction_id = null, $event_type = 'MorbidityEvent', $investigator = '', $test_result = '') {
		global $host_pa, $my_db_schema;

		if (is_null($system_message_id) || is_null($event_id) || is_null($record_number) || is_null($notify_type) || is_null($condition) || is_null($test_type)) {
			return false;
		} else {
			// sanitize input, prepare variables
			$sm_id = filter_var($system_message_id, FILTER_VALIDATE_INT);
			$e_id = filter_var($event_id, FILTER_VALIDATE_INT);
			$r_id = trim(filter_var($record_number, FILTER_SANITIZE_STRING));
			$n_id = filter_var($notify_type, FILTER_VALIDATE_INT);
			$c_name = trim(filter_var($condition, FILTER_SANITIZE_STRING));
			$t_type = trim(filter_var($test_type, FILTER_SANITIZE_STRING));
			$s_notify = filter_var($notify_state, FILTER_VALIDATE_BOOLEAN);
			$j_notify = filter_var($notify_lhd, FILTER_VALIDATE_BOOLEAN);
			$e_type = trim(filter_var($event_type, FILTER_SANITIZE_STRING));
			switch($e_type) {
				case 'AssessmentEvent':
				case 'ContactEvent':
					break;
				default:
					$e_type = 'MorbidityEvent';
			}
			$i_name = trim(filter_var($investigator, FILTER_SANITIZE_STRING));
			$t_result = trim(filter_var($test_result, FILTER_SANITIZE_STRING));

			// check for valid values in system message ID, event ID, record number, notification type, condition, and test type
			if (!$sm_id || !$e_id || !$r_id || !$n_id || !$c_name || !$t_type || (strlen($r_id) == 0) || (strlen($c_name) == 0) || (strlen($t_type) == 0)) {
				return false;
			}

			// if notify LHD, check to make sure jurisdiction ID is provided
			if ($j_notify && is_null($jurisdiction_id)) {
				return false;
			} elseif ($j_notify) {
				// check for a valid jurisdiction ID
				$j_id = filter_var($jurisdiction_id, FILTER_VALIDATE_INT);
				if (!$j_id) {
					return false;
				}
			}
		}

		$sql = sprintf("INSERT INTO %sbatch_notifications (notification_type, system_message_id, event_id, record_number, condition, test_type, notify_state, notify_lhd, jurisdiction_id, event_type, investigator, custom, test_result) VALUES (%d, %d, %d, '%s', '%s', '%s', '%s', '%s', %s, '%s', %s, %s, %s);",
			$my_db_schema,
			$n_id, $sm_id, $e_id, @pg_escape_string($r_id), @pg_escape_string($c_name), @pg_escape_string($t_type),
			(($s_notify) ? "t" : "f"),
			(($j_notify) ? "t" : "f"),
			((isset($j_id) && ($j_id > 0)) ? $j_id : "NULL"),
			@pg_escape_string($e_type),
			((strlen(trim($i_name)) > 0) ? "'".@pg_escape_string($i_name)."'" : "NULL"),
			((isCustomNotificationType($n_id)) ? "true" : "false"),
			((strlen(trim($t_result)) > 0) ? "'".@pg_escape_string($t_result)."'" : "NULL"));
		$rs = @pg_query($host_pa, $sql);
		if ($rs) {
			return true;
		} else {
			return false;
		}
	}




	/**
	 * Checks if batch notification type is used for custom jurisdictions or not.
	 * Returns virtual jurisdiction ID corresponding to notification type if custom.
	 *
	 * @param int $notification_type_id ID corresponding to the batch notification type to check.
	 * @return bool
	 */
	function isCustomNotificationTypeWithId($notification_type_id = null) {
		global $host_pa, $my_db_schema;

		if (is_null($notification_type_id)) {
			return false;
		}

		$sql = 'SELECT custom FROM '.$my_db_schema.'batch_notification_types WHERE id = '.intval($notification_type_id).' AND custom IS NOT NULL AND custom > 0;';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			$custom = intval(@pg_fetch_result($rs, 0, 'custom'));
			@pg_free_result($rs);
			return $custom;
		} else {
			@pg_free_result($rs);
			return false;
		}
	}




	/**
	 * Checks if batch notification type is used for custom jurisdictions or not.
	 *
	 * @param int $notification_type_id ID corresponding to the batch notification type to check.
	 * @return bool
	 */
	function isCustomNotificationType($notification_type_id = null) {
		global $host_pa, $my_db_schema;

		if (is_null($notification_type_id)) {
			return false;
		}

		$sql = 'SELECT custom FROM '.$my_db_schema.'batch_notification_types WHERE id = '.intval($notification_type_id).' AND custom IS NOT NULL AND custom > 0;';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			@pg_free_result($rs);
			return true;
		} else {
			@pg_free_result($rs);
			return false;
		}
	}




	/**
	 * Returns the TriSano-coded jurisdiction ID based on an ID value from system_districts
	 *
	 * @param int $d_id District ID of the LHD to look up.
	 * @return int
	 */
	function getJurisdictionByDistrictID($d_id = 0) {
		if (filter_var($d_id, FILTER_SANITIZE_NUMBER_INT) < 1) {
			return -1;
		}

		$result = -1;
		global $host_pa, $my_db_schema;
		$sql = 'SELECT system_external_id FROM '.$my_db_schema.'system_districts WHERE id = '.intval($d_id).';';
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false && @pg_num_rows($rs) > 0) {
			$result = intval(@pg_fetch_result($rs, 0, 'system_external_id'));
		}
		@pg_free_result($rs);
		return $result;
	}




	/**
	 * Removes nodes from the Trisano XML that are not needed for 'updateCmr' calls but can remove data from the event if they are incomplete
	 *
	 * @return void
	 */
	function removeUnusedNodesForUpdate() {
		global $orig_sxe;

		unset($orig_sxe->interested_party_attributes);	// not needed on update since no demographic information is updated/changed via ELR
		unset($orig_sxe->jurisdiction_attributes);		// no change to jurisdiction for an update
	}




	/**
	 * Set the workflow status of an event being assigned.
	 *
	 * 2013-06-26:  Switch auto-close to use 'closed' instead of 'approved_by_lhd' (request by S. Mottice)
	 * 2013-12-12:  Change function to be generic and allow any workflow status to be sent (with filtered list).
	 *			  Renamed from 'closeSurveillanceEvent(SimpleXMLElement)' to 'setWorkflow(SimpleXMLElement, string)'
	 *
	 * @param SimpleXMLElement $nedss_obj SimpleXMLElement object representing the NEDSS XML
	 * @param string $status Workflow status to be set
	 * @return void
	 */
	function setWorkflow($nedss_obj, $status) {
		switch (strtolower(trim($status))) {
			case 'approved_by_lhd':
			case 'closed':
			case 'assigned_to_investigator':
				$status_clean = strtolower(trim($status));
				break;
			default:
				$status_clean = null;
		}
		if (!empty($status_clean)) {
			if (isset($nedss_obj->events->workflow_state)) {
				$nedss_obj->events->workflow_state = $status_clean;
			} else {
				$nedss_obj->events->addChild('workflow_state', $status_clean);
			}
		}
	}




	/**
	 * Checks whether the LHD specified by a TriSano Jurisdiction ID wants their new Surveillance Events auto-approved or not.
	 *
	 * @param int $j_id Jurisdiction ID of the LHD to check.
	 * @return bool
	 */
	function closeSurveillanceBoolByJurisdiction($j_id = 0) {
		if (filter_var($j_id, FILTER_SANITIZE_NUMBER_INT) < 1) {
			return false;
		}
		global $host_pa, $my_db_schema;
		$sql = 'SELECT id FROM '.$my_db_schema.'system_districts WHERE system_external_id = '.intval($j_id).' AND close_surveillance IS TRUE;';
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false && @pg_num_rows($rs) > 0) {
			@pg_free_result($rs);
			return true;
		} else {
			@pg_free_result($rs);
			return false;
		}
	}




	/**
	 * For 'addCmr' actions (creating a new CMR for a new person), decodes the coded Abnormal Flag values & appends to comments.
	 *
	 * @param SimpleXMLElement $master_obj SimpleXMLElement object representing the Master XML
	 * @param SimpleXMLElement $nedss_obj SimpleXMLElement object representing the TriSano (UTNEDSS) XML
	 * @return void
	 */
	function addCmrAbnormalFlagToComments($master_obj, $nedss_obj) {
		$DEBUG = false;
		global $clean_actions;

		if ($DEBUG) {
			error_log('[*] Entering: '. __FUNCTION__);
			error_log('    Parameters:');
			error_log(json_encode(func_get_args()));
			// error_log('    - $clean_actions: '.json_encode($clean_actions));
			// error_log('    - $master_xml: '.json_encode($master_xml));
			// error_log('    - $orig_sxe: '.json_encode($orig_sxe));
			// error_log('    - $hl7Message: '.json_encode($hl7Message));
		}

		if (isset($master_obj->reporting->agency) && !empty($master_obj->reporting->agency)) {
			$comments = 'Facility: '.trim($master_obj->reporting->agency);
		}

		if (isset($nedss_obj->labs_attributes->lab_results->comment) && !empty($nedss_obj->labs_attributes->lab_results->comment)) {
			$comments = $comments.trim($nedss_obj->labs_attributes->lab_results->comment);
		}

		if (isset($master_obj->labs->abnormal_flag) && !empty($master_obj->labs->abnormal_flag)) {
			$abnormal_flag = 'Abnormal Flag:  '.decodeAbnormalFlag(trim($master_obj->labs->abnormal_flag));
		}

		// also append Local test name, if any
		if (isset($master_obj->labs->local_code_test_name) && !empty($master_obj->labs->local_code_test_name)) {
			if (isset($abnormal_flag)) {
				// append if ab. flag already set
				$abnormal_flag .= '; '.trim($master_obj->labs->lab).' Local Code Test Name: '.htmlspecialchars(trim($master_obj->labs->local_code_test_name));
			} else {
				$abnormal_flag = trim($master_obj->labs->lab).' Local Code Test Name: '.htmlspecialchars(trim($master_obj->labs->local_code_test_name));
			}
		} elseif (isset($master_obj->labs->local_test_name) && !empty($master_obj->labs->local_test_name)) {
			if (isset($abnormal_flag)) {
				// append if ab. flag already set
				$abnormal_flag .= '; '.trim($master_obj->labs->lab).' Local Test Name: '.htmlspecialchars(trim($master_obj->labs->local_test_name));
			} else {
				$abnormal_flag = trim($master_obj->labs->lab).' Local Test Name: '.htmlspecialchars(trim($master_obj->labs->local_test_name));
			}
		}

		$new_comments = '';

		if (isset($comments) && isset($abnormal_flag)) {
			$new_comments = $comments.'; '.$abnormal_flag;
		} elseif (isset($comments)) {
			$new_comments = $comments;
		} elseif (isset($abnormal_flag)) {
			$new_comments = $abnormal_flag;
		}

		if (isset($master_obj->hospital_info->medical_record) && !empty($master_obj->hospital_info->medical_record)) {
			if (!empty($new_comments)) {
				$new_comments .= "\n";
			}
			$new_comments .= 'Medical Record#:  '.trim($master_obj->hospital_info->medical_record)."\n";
		}

		if (isset($nedss_obj->clinician_attributes)) {
			$clinician = 'Clinician: '.trim($nedss_obj->clinician_attributes->clinician->last_name).', '.trim($nedss_obj->clinician_attributes->clinician->first_name).' '.trim($nedss_obj->clinician_attributes->clinician->middle_name)."\n";
			$clinician .= 'Telephone ('.trim($master_xml->clinicians->phone_type).'): ('.trim($nedss_obj->clinician_attributes->clinician->area_code).') '.trim($nedss_obj->clinician_attributes->clinician->phone).' '.trim($nedss_obj->clinician_attributes->clinician->extension)."\n\n";
			if (!empty($new_comments)) {
				$new_comments .= "\n";
			}
			$new_comments .= $clinician;
		}

		if (isset($nedss_obj->diagnostic_facilities_attributes)) {
			$diagnostic_facility = 'Diagnostic Facility: '.trim($nedss_obj->diagnostic_facilities_attributes->place->name)."\n";
			$diagnostic_facility .= 'Facility Type: '.trim($master_xml->diagnostic->types)."\n";
			$diagnostic_facility .= 'Address: '.trim($nedss_obj->diagnostic_facilities_attributes->addresses->street_name).((isset($nedss_obj->diagnostic_facilities_attributes->addresses->unit_number) && !empty($nedss_obj->diagnostic_facilities_attributes->addresses->unit_number) && (strlen(trim($nedss_obj->diagnostic_facilities_attributes->addresses->unit_number)) > 0)) ? ', Unit '.trim($nedss_obj->diagnostic_facilities_attributes->addresses->unit_number) : '').', '.trim($nedss_obj->diagnostic_facilities_attributes->addresses->city).', '.trim($master_xml->diagnostic->state).' '.trim($nedss_obj->diagnostic_facilities_attributes->addresses->postal_code)."\n\n";

			if (!empty($new_comments)) {
				$new_comments .= "\n";
			}
			$new_comments .= $diagnostic_facility;
		}

		$hl7CustomProps = array();

		// Adding Alternate Patient ID
		$pidSegment4 = getAlternatePatientIDFromHL7($clean_actions['id']);
		if (!empty($pidSegment4)) {
			// if (!empty($new_comments)) {
			// 	$new_comments .= "\n";
			// }
			// $new_comments .= "[Alternate Patient ID : {$pidSegment4}]\n";
			$hl7CustomProps['PID.4.1'] = $pidSegment4;
		}

		// Adding HL7 to Comment
		if (!empty($hl7CustomProps)) {
			if (!empty($new_comments)) {
				$new_comments .= "\n";
			}
			$new_comments .= '[HL7: \''.json_encode($hl7CustomProps).'\']';
		}

		if (!empty($new_comments)) {

			$new_comments = '[----- Lab added by ELR '.date(DATE_RSS, time())." ----------]\n" . $new_comments ."\n";
			$new_comments .= '[----- End ELR Update ----------]'."\n\n";

			if (!isset($nedss_obj->labs_attributes->lab_results->comment)) {
				$nedss_obj->labs_attributes->lab_results->addChild('comment', $new_comments);
			} else {
				// htmlspecialchars_decode() to prevent double-encoding of html entities
				$nedss_obj->labs_attributes->lab_results->comment = htmlspecialchars_decode($new_comments);
			}
		}
	}




	/**
	 * Set the 'Pregnancy Status' flag in the NEDSS event.
	 *
	 * @param SimpleXMLElement $nedss_obj SimpleXMLElement object representing the NEDSS XML
	 * @param bool $is_pregnant [Optional] Indicate whether to set the flag to 'Yes' or 'No' (Default 'Yes')
	 * @return void
	 */
	function setPatientPregnant($nedss_obj, $is_pregnant = true) {
		if ($is_pregnant) {
			$pregnant_nedss_id = getNedssIdByCode('Y', 'external_codes', 'yesno');
		} else {
			$pregnant_nedss_id = getNedssIdByCode('N', 'external_codes', 'yesno');
		}

		if (!isset($nedss_obj->interested_party_attributes->risk_factor_attributes)) {
			$nedss_obj->interested_party_attributes->addChild('risk_factor_attributes');  // add risk_factor_attributes if not present
		}

		if (!isset($nedss_obj->interested_party_attributes->risk_factor_attributes->pregnant_id)) {
			$nedss_obj->interested_party_attributes->risk_factor_attributes->addChild('pregnant_id', intval($pregnant_nedss_id));
		} else {
			// htmlspecialchars_decode() to prevent double-encoding of html entities
			$nedss_obj->interested_party_attributes->risk_factor_attributes->pregnant_id = intval($pregnant_nedss_id);
		}
	}




	/**
	 * Remove glue string-only values in NEDSS XML's <parent_guardian/> field.
	 * Due to HL7 concatenation, an empty NK1 can turn into "," and must be removed prior to sending to TriSano
	 *
	 * @param SimpleXMLElement $nedss_obj SimpleXMLElement object representing the TriSano (UTNEDSS) XML
	 * @return void
	 */
	function removeParentGuardianEmptyGlue($nedss_obj) {
		if (isset($nedss_obj->events->parent_guardian) && (trim($nedss_obj->events->parent_guardian) == ',')) {
			unset ($nedss_obj->events->parent_guardian);
		}
	}



	/**
	 * For CMR updates, appends Clinician & Diagnostic Facility information to Lab Comments (instead of duplicating in 'Clinical' tab
	 * Also handles stitching in comments for lab results and retaining existing lab comments so existing values
	 * aren't overwritten and decoding/appending Abnormal Flags to comments.
	 *
	 * @param string $old_comments Existing comments from the lab being updated
	 * @param bool $is_new_lab Indicates whether the comments apply to a new lab being added to the CMR (TRUE), or an existing lab in the CMR being updated (FALSE; default)
	 * @return void
	 */
	function clinicalDataToComments($old_comments, $is_new_lab = false) {
		$DEBUG = false;
		global $master_xml, $orig_sxe, $clean_actions;

		// $hl7Message = getParsedHL7($clean_actions['id']);

		if ($DEBUG) {
			error_log('[*] Entering: '. __FUNCTION__);
			error_log('    Parameters:');
			// error_log(json_encode(func_get_args()));
			// error_log('    - $clean_actions: '.json_encode($clean_actions));
			// error_log('    - $master_xml: '.json_encode($master_xml));
			// error_log('    - $orig_sxe: '.json_encode($orig_sxe));
			// error_log('    - $hl7Message: '.json_encode($hl7Message));
		}

		if (isset($orig_sxe->labs_attributes->lab_results->comment) && !empty($orig_sxe->labs_attributes->lab_results->comment)) {
			$new_comments = 'New Comments:  '.trim($orig_sxe->labs_attributes->lab_results->comment)."\n";
		} else {
			$orig_sxe->labs_attributes->lab_results->addChild('comment');
			$new_comments = '';
			if (isset($master_xml->reporting->agency) && !empty($master_xml->reporting->agency)) {
				$new_comments = 'Facility: '.trim($master_xml->reporting->agency).' ';
			}
		}

		if (isset($master_xml->labs->abnormal_flag) && !empty($master_xml->labs->abnormal_flag)) {
			$new_comments .= 'Abnormal Flag:  '.decodeAbnormalFlag(trim($master_xml->labs->abnormal_flag))."\n";
		}

		// also append Local test name, if any
		if (isset($master_xml->labs->local_code_test_name) && !empty($master_xml->labs->local_code_test_name)) {
			$new_comments .= trim($master_xml->labs->lab).' Local Code Test Name: '.htmlspecialchars(trim($master_xml->labs->local_code_test_name))."\n";
		} elseif (isset($master_xml->labs->local_test_name) && !empty($master_xml->labs->local_test_name)) {
			$new_comments .= trim($master_xml->labs->lab).' Local Test Name: '.htmlspecialchars(trim($master_xml->labs->local_test_name))."\n";
		}

		if (isset($master_xml->hospital_info->medical_record) && !empty($master_xml->hospital_info->medical_record)) {
			$new_comments .= 'Medical Record#:  '.trim($master_xml->hospital_info->medical_record)."\n";
		}

		$new_comments .= "\n";

		if ($is_new_lab) {
			$comment_header = '[===== Lab added by ELR '.date(DATE_RSS, time())." ==========]\n";
		} else {
			$comment_header = '[===== Lab updated by ELR '.date(DATE_RSS, time())." ==========]\n";
		}

		$comment_footer = '[===== End ELR Update ==========]'."\n\n";

		if (isset($orig_sxe->clinician_attributes)) {
			$clinician = 'Clinician: '.trim($orig_sxe->clinician_attributes->clinician->last_name).', '.trim($orig_sxe->clinician_attributes->clinician->first_name).' '.trim($orig_sxe->clinician_attributes->clinician->middle_name)."\n";
			$clinician .= 'Telephone ('.trim($master_xml->clinicians->phone_type).'): ('.trim($orig_sxe->clinician_attributes->clinician->area_code).') '.trim($orig_sxe->clinician_attributes->clinician->phone).' '.trim($orig_sxe->clinician_attributes->clinician->extension)."\n\n";
			if (!empty($new_comments)) {
				$new_comments .= "\n";
			}
			$new_comments .= $clinician;
		}

		if (isset($orig_sxe->diagnostic_facilities_attributes)) {
			$diagnostic_facility = 'Diagnostic Facility: '.trim($orig_sxe->diagnostic_facilities_attributes->place->name)."\n";
			$diagnostic_facility .= 'Facility Type: '.trim($master_xml->diagnostic->types)."\n";
			$diagnostic_facility .= 'Address: '.trim($orig_sxe->diagnostic_facilities_attributes->addresses->street_name).((isset($orig_sxe->diagnostic_facilities_attributes->addresses->unit_number) && !empty($orig_sxe->diagnostic_facilities_attributes->addresses->unit_number) && (strlen(trim($orig_sxe->diagnostic_facilities_attributes->addresses->unit_number)) > 0)) ? ', Unit '.trim($orig_sxe->diagnostic_facilities_attributes->addresses->unit_number) : '').', '.trim($orig_sxe->diagnostic_facilities_attributes->addresses->city).', '.trim($master_xml->diagnostic->state).' '.trim($orig_sxe->diagnostic_facilities_attributes->addresses->postal_code)."\n\n";
			if (!empty($new_comments)) {
				$new_comments .= "\n";
			}
			$new_comments .= $diagnostic_facility;
		}

		$hl7CustomProps = array();

		// Adding Alternate Patient ID
		$pidSegment4 = getAlternatePatientIDFromHL7($clean_actions['id']);
		if (!empty($pidSegment4)) {
			// $new_comments .= "[Alternate Patient ID : {$pidSegment4}]\n";
			$hl7CustomProps['PID.4.1'] = $pidSegment4;
		}

		// Adding HL7 to Comment
		if (!empty($hl7CustomProps)) {
			if (!empty($new_comments)) {
				$new_comments .= "\n";
			}
			$new_comments .= '[HL7: \''.json_encode($hl7CustomProps).'\']';
		}

		// blow away all clinician, diagnostic facility, and hospitalization data
		unset($orig_sxe->clinician_attributes);
		unset($orig_sxe->diagnostic_facilities_attributes);
		unset($orig_sxe->hospitalization_facilities_attributes);

		// set the new comment
		if (!isset($orig_sxe->labs_attributes->lab_results->comment)) {
			$orig_sxe->labs_attributes->lab_results->addChild('comment', $comment_header.$new_comments.$comment_footer.$old_comments);
		} else {
			// htmlspecialchars_decode() to prevent double-encoding of html entities
			$orig_sxe->labs_attributes->lab_results->comment = htmlspecialchars_decode($comment_header.$new_comments.$comment_footer.$old_comments);
		}

		if ($DEBUG) {
			error_log('    - $new_comments: '. json_encode($new_comments));
			error_log('    - $orig_sxe->labs_attributes->lab_results: '. json_encode($orig_sxe->labs_attributes->lab_results));
			error_log('    Exit: '. __FUNCTION__);
		}
	}




	/**
	 * Checks Patient, Address & Telephone information for an incoming lab for differences and moves them into 'Notes'
	 * Used when updating an event in NEDSS ('updateCmr') so duplicates aren't added and existing values aren't overwritten.
	 *
	 * @param SimpleXMLElement $old_sxe SimpleXMLElement containing the results of 'findEvent' for the CMR to be updated
	 * @param bool $is_addcmr Flag indicating whether function is being called as part of creating a new CMR resulting from no matching events being found from any of the selected persons
	 * @param int $system_message_id ID number of the message in system_messages used to obtain the HL7 string
	 * @return bool
	 */
	function updatedContactInfoToNotes($old_sxe, $is_addcmr = false, $system_message_id = null) {
		global $master_xml, $orig_sxe, $laboratory_id;

		$same_name = true;
		$same_address = true;
		$same_dob = true;

		unset($notes);
		$updates = array();

		$telephones = $orig_sxe->interested_party_attributes->telephones;

		/*
		 * compare patient/demographic info first...
		 */
		// first name
		if (isset($orig_sxe->interested_party_attributes->person->first_name)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->first_name)) > 0) {
				// has a value
				if (strtolower(trim($orig_sxe->interested_party_attributes->person->first_name)) != strtolower(trim($old_sxe->trisano_health->interested_party_attributes->person->first_name))) {
					// different value, push new value to notes
					$same_name = $same_name && false;
					$updates[] = array(
						'label' => 'First Name',
						'value' => trim($orig_sxe->interested_party_attributes->person->first_name)
					);
				} else {
					$same_name = $same_name && true;
				}
			}
			unset($orig_sxe->interested_party_attributes->person->first_name);  // remove old value from XML to not overwrite value in NEDSS
		}

		// last name
		if (isset($orig_sxe->interested_party_attributes->person->last_name)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->last_name)) > 0) {
				// has a value
				if (strtolower(trim($orig_sxe->interested_party_attributes->person->last_name)) != strtolower(trim($old_sxe->trisano_health->interested_party_attributes->person->last_name))) {
					// different value, push new value to notes
					$same_name = $same_name && false;
					$updates[] = array(
						'label' => 'Last Name',
						'value' => trim($orig_sxe->interested_party_attributes->person->last_name)
					);
				} else {
					$same_name = $same_name && true;
				}
			}
			unset($orig_sxe->interested_party_attributes->person->last_name);  // remove old value from XML to not overwrite value in NEDSS
		}

		// middle name
		if (isset($orig_sxe->interested_party_attributes->person->middle_name)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->middle_name)) > 0) {
				// has a value
				if (strtolower(trim($orig_sxe->interested_party_attributes->person->middle_name)) != strtolower(trim($old_sxe->trisano_health->interested_party_attributes->person->middle_name))) {
					// different value, push new value to notes
					$same_name = $same_name && false;
					$updates[] = array(
						'label' => 'Middle Name',
						'value' => trim($orig_sxe->interested_party_attributes->person->middle_name)
					);
				} else {
					$same_name = $same_name && true;
				}
			}
			unset($orig_sxe->interested_party_attributes->person->middle_name);  // remove old value from XML to not overwrite value in NEDSS
		}

		// gender
		if (isset($orig_sxe->interested_party_attributes->person->birth_gender_id)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->birth_gender_id)) > 0) {
				// has a value
				if (intval($orig_sxe->interested_party_attributes->person->birth_gender_id) != intval($old_sxe->trisano_health->interested_party_attributes->person->birth_gender_id)) {
					// different value, push new value to notes
					$updates[] = array(
						'label' => 'Gender',
						'value' => getMasterConceptFromChildCode(trim($master_xml->person->gender), 'gender', $laboratory_id)
					);
				}
			}
			unset($orig_sxe->interested_party_attributes->person->birth_gender_id);  // remove old value from XML to not overwrite value in NEDSS
		}

		// ethnicity
		if (isset($orig_sxe->interested_party_attributes->person->ethnicity_id)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->ethnicity_id)) > 0) {
				// has a value
				if (intval($orig_sxe->interested_party_attributes->person->ethnicity_id) != intval($old_sxe->trisano_health->interested_party_attributes->person->ethnicity_id)) {
					// different value, push new value to notes
					$updates[] = array(
						'label' => 'Ethnicity',
						'value' => getMasterConceptFromChildCode(trim($master_xml->person->ethnicity), 'ethnicity', $laboratory_id)
					);
				}
			}
			unset($orig_sxe->interested_party_attributes->person->ethnicity_id);  // remove old value from XML to not overwrite value in NEDSS
		}

		// language
		if (isset($orig_sxe->interested_party_attributes->person->primary_language_id)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->primary_language_id)) > 0) {
				// has a value
				if (intval($orig_sxe->interested_party_attributes->person->primary_language_id) != intval($old_sxe->trisano_health->interested_party_attributes->person->primary_language_id)) {
					// different value, push new value to notes
					$updates[] = array(
						'label' => 'Language',
						'value' => getMasterConceptFromChildCode(trim($master_xml->person->language), 'language', $laboratory_id)
					);
				}
			}
			unset($orig_sxe->interested_party_attributes->person->primary_language_id);  // remove old value from XML to not overwrite value in NEDSS
		}

		// approx age
		if (isset($orig_sxe->interested_party_attributes->person->approximate_age_no_birthday)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->approximate_age_no_birthday)) > 0) {
				// has a value
				if (intval($orig_sxe->interested_party_attributes->person->approximate_age_no_birthday) != intval($old_sxe->trisano_health->interested_party_attributes->person->approximate_age_no_birthday)) {
					// different value, push new value to notes
					$updates[] = array(
						'label' => 'Approximate Age',
						'value' => trim(intval($orig_sxe->interested_party_attributes->person->approximate_age_no_birthday))
					);
				}
			}
			unset($orig_sxe->interested_party_attributes->person->approximate_age_no_birthday);  // remove old value from XML to not overwrite value in NEDSS
		}

		// date of birth
		if (isset($orig_sxe->interested_party_attributes->person->birth_date)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->birth_date)) > 0) {
				// has a value
				$old_date_arr = date_parse(trim($old_sxe->trisano_health->interested_party_attributes->person->birth_date));
				$new_date_arr = date_parse(trim($orig_sxe->interested_party_attributes->person->birth_date));
				$old_date = mktime(0, 0, 0, $old_date_arr['month'], $old_date_arr['day'], $old_date_arr['year']);
				$new_date = mktime(0, 0, 0, $new_date_arr['month'], $new_date_arr['day'], $new_date_arr['year']);
				if ($old_date !== $new_date) {
					// different value, push new value to notes
					$same_dob = $same_dob && false;
					$updates[] = array(
						'label' => 'Date of Birth',
						'value' => date("F j, Y", strtotime(trim($orig_sxe->interested_party_attributes->person->birth_date)))
					);
				} else {
					$same_dob = $same_dob && true;
				}
			}
			unset($orig_sxe->interested_party_attributes->person->birth_date);  // remove old value from XML to not overwrite value in NEDSS
		}

		// date of death
		if (isset($orig_sxe->interested_party_attributes->person->date_of_death)) {
			if (strlen(trim($orig_sxe->interested_party_attributes->person->date_of_death)) > 0) {
				// has a value
				$old_date_arr = date_parse(trim($old_sxe->trisano_health->interested_party_attributes->person->date_of_death));
				$new_date_arr = date_parse(trim($orig_sxe->interested_party_attributes->person->date_of_death));
				$old_date = mktime(0, 0, 0, $old_date_arr['month'], $old_date_arr['day'], $old_date_arr['year']);
				$new_date = mktime(0, 0, 0, $new_date_arr['month'], $new_date_arr['day'], $new_date_arr['year']);
				if ($old_date !== $new_date) {
					// different value, push new value to notes
					$updates[] = array(
						'label' => 'Date of Death',
						'value' => date("F j, Y", strtotime(trim($orig_sxe->interested_party_attributes->person->date_of_death)))
					);
				}
			}
			unset($orig_sxe->interested_party_attributes->person->date_of_death);  // remove old value from XML to not overwrite value in NEDSS
		}


		// compare address collections
		$addresses = $orig_sxe->addresses;
		$old_addresses = $old_sxe->trisano_health->addresses;
		$address_same = array(
			'county' => false,
			'state' => false,
			'street' => false,
			'unit' => false,
			'zip' => false,
			'city' => false
		);
		if (!is_null($addresses) && (count($addresses) > 0)) {
			foreach ($addresses as $address) {
				// should actually only be one incoming address, but easier to process this way just in case
				if (!is_null($old_addresses) && (count($old_addresses) > 0)) {
					foreach ($old_addresses as $old_address) {
						// only compare addresses tied to this event
						if (isset($old_address->event_id) && !empty($old_address->event_id) && (intval($old_address->event_id) == intval($old_sxe->trisano_health->events->id))) {
							// compare each element from new to old, push to $updates if different
							if (!isset($address->county_id) || (strlen(trim($address->county_id)) == 0) || (strtolower(trim($address->county_id)) == strtolower(trim($old_address->county_id)))) {
								$address_same['county'] = ($address_same['county'] || true);
							}
							if (!isset($address->state_id) || (strlen(trim($address->state_id)) == 0) || (strtolower(trim($address->state_id)) == strtolower(trim($old_address->state_id)))) {
								$address_same['state'] = ($address_same['state'] || true);
							}
							if (!isset($address->street_name) || (strlen(trim($address->street_name)) == 0) || (strtolower(trim($address->street_name)) == strtolower(trim($old_address->street_name)))) {
								$address_same['street'] = ($address_same['street'] || true);
							}
							if (!isset($address->unit_number) || (strlen(trim($address->unit_number)) == 0) || (strtolower(trim($address->unit_number)) == strtolower(trim($old_address->unit_number)))) {
								$address_same['unit'] = ($address_same['unit'] || true);
							}
							if (!isset($address->postal_code) || (strlen(trim($address->postal_code)) == 0) || (strtolower(trim($address->postal_code)) == strtolower(trim($old_address->postal_code)))) {
								$address_same['zip'] = ($address_same['zip'] || true);
							}
							if (!isset($address->city) || (strlen(trim($address->city)) == 0) || (strtolower(trim($address->city)) == strtolower(trim($old_address->city)))) {
								$address_same['city'] = ($address_same['city'] || true);
							}
						}
					}
				} else {
					// no addresses in existing case, push all to $updates
					unset($address_same);
				}

				if (!isset($address_same) || ($address_same['street'] === false) || ($address_same['zip'] === false) || ($address_same['city'] === false)) {
					// street, city, and/or zip code different, push entire address to $updates
					$same_address = $same_address && false;
					$updates[] = array(
						'label' => 'Street',
						'value' => trim($master_xml->person->street_name)
					);
					$updates[] = array(
						'label' => 'Unit',
						'value' => trim($master_xml->person->unit)
					);
					$updates[] = array(
						'label' => 'City',
						'value' => trim($master_xml->person->city)
					);
					$updates[] = array(
						'label' => 'Zip/Postal Code',
						'value' => trim($master_xml->person->zip)
					);
					$updates[] = array(
						'label' => 'County',
						'value' => trim($master_xml->person->county)
					);
					$updates[] = array(
						'label' => 'State',
						'value' => trim($master_xml->person->state)
					);
				} else {
					$same_address = $same_address && true;
				}
			}
		}

		// compare telephone collections
		$telephones = $orig_sxe->interested_party_attributes->telephones;
		$old_telephones = $old_sxe->trisano_health->interested_party_attributes->telephones;
		$telephone_same = false;
		if (!is_null($telephones) && (count($telephones) > 0)) {
			foreach ($telephones as $telephone) {
				unset($telephone_hash);
				$telephone_hash = md5(strtolower(trim($telephone->area_code)).strtolower(trim($telephone->phone_number)).strtolower(trim($telephone->extension)));
				// should actually only be one incoming telephone, but easier to process this way just in case
				if (!is_null($old_telephones) && (count($old_telephones) > 0)) {
					foreach ($old_telephones as $old_telephone) {
						unset($old_telephone_hash);
						$old_telephone_hash = md5(strtolower(trim($old_telephone->area_code)).strtolower(trim($old_telephone->phone_number)).strtolower(trim($old_telephone->extension)));

						// compare entire telephone element from new to old, push to $updates if different
						if ($telephone_hash == $old_telephone_hash) {
							$telephone_same = ($telephone_same || true);
						}
					}
				} else {
					// no telephones in existing case, push all to $updates
					unset($telephone_same);
				}

				if (!isset($telephone_same) || $telephone_same === false) {
					// telephone not found, push to $updates
					$updates[] = array(
						'label' => 'Area Code',
						'value' => trim($master_xml->person->area_code)
					);
					$updates[] = array(
						'label' => 'Phone Number',
						'value' => trim($master_xml->person->phone)
					);
					$updates[] = array(
						'label' => 'Extension',
						'value' => trim($master_xml->person->extension)
					);
					$updates[] = array(
						'label' => 'Phone Type',
						'value' => trim($master_xml->person->phone_type)
					);
				}
			}
		}

		// compare race collections
		$races = $orig_sxe->interested_party_attributes->people_races;
		$old_races = $old_sxe->trisano_health->interested_party_attributes->people_races;
		$race_same = array(
			'race_id' => false
		);
		if (!is_null($races) && (count($races) > 0)) {
			foreach ($races as $race) {
				// should actually only be one incoming race, but easier to process this way just in case
				if (!is_null($old_races) && (count($old_races) > 0)) {
					foreach ($old_races as $old_race) {
						// compare each element from new to old, push to $updates if different
						if (!isset($race->serial_version_uid->race_id) || (strlen(trim($race->serial_version_uid->race_id)) == 0) || (strtolower(trim($race->serial_version_uid->race_id)) == strtolower(trim($old_race->serial_version_uid->race_id)))) {
							$race_same['race_id'] = ($race_same['race_id'] || true);
						}
					}
				} else {
					// no races in existing case, push all to $updates
					unset($race_same);
				}

				if (!isset($race_same) || $race_same['race_id'] === false) {
					// county different, push to $updates
					$updates[] = array(
						'label' => 'Race',
						'value' => getMasterConceptFromChildCode(trim($master_xml->person->race), 'race', $laboratory_id)
					);
				}
			}
		}

		if (count($updates) > 0) {
			// one or more fields have updated values, update the notes...
			$notes = "Potential updated information from ELR.  Affected fields and their new values are:\n\n";

			foreach ($updates as $update_key => $update_val) {
				$notes .= $update_val['label'].': '.$update_val['value']."\n";
			}
		}

		$notes .= "\nOriginal HL7 Message:\n".htmlspecialchars(trim(getHL7($system_message_id)));

		unset($orig_sxe->addresses);  // blow away all addresses nodes
		unset($orig_sxe->interested_party_attributes->telephones); // blow away all telephones nodes
		unset($orig_sxe->interested_party_attributes->people_races); // blow away all race nodes
		unset($orig_sxe->notes);  // clear notes, just to be safe

		if (isset($notes)) {
			$orig_sxe->addChild('notes');
			$orig_sxe->notes->addChild('note', $notes);
			$orig_sxe->notes->addChild('user_id', '9999');
			$orig_sxe->notes->addChild('note_type', 'administrative');
		}

		if ($is_addcmr && (!$same_name || !$same_address || !$same_dob)) {
			// check if name, address, or DOB from the ELR message is different than the NEDSS person we'll be creating the event for
			// if any different, return false & trigger an exception
			return false;
		} else {
			return true;
		}
	}


	/**
	 * Appends original HL7 message to TriSano 'Note' when adding/updating an event from ELR
	 *
	 * @param string $hl7_string Original HL7 message
	 * @param SimpleXMLElement $nedss_obj SimpleXMLElement containing the TriSano XML being prepared to add/update
	 * @return void
	 */
	function addCmrHL7ToNotes($hl7_string, $nedss_obj) {
		addNote("Original HL7 Message:\n".htmlspecialchars(trim($hl7_string)), $nedss_obj);
	}


	/**
	 * Appends $note to TriSano 'Note' when adding/updating an event from ELR
	 *
	 * @param string $note the note to add
	 * @param SimpleXMLElement $nedss_obj SimpleXMLElement containing the TriSano XML being prepared to add/update
	 * @return void
	 */
	function addNote($note, $nedss_obj) {
		if(isset($nedss_obj->notes)) {
			$nedss_obj->notes->note = $nedss_obj->notes->note."\n\n".$note;
		} else {
			$nedss_obj->addChild('notes');
			$nedss_obj->notes->addChild('note', $note);
			$nedss_obj->notes->addChild('user_id', '9999');
			$nedss_obj->notes->addChild('note_type', 'administrative');
		}
	}


	/**
	 * Adds geocoding to NEDSS XML addresses prior to adding a new CMR
	 *
	 * @param SimpleXMLElement $nedss_obj SimpleXMLElement containing the TriSano XML being prepared to add
	 * @return void
	 */
	function geocodeNedssXml($nedss_obj) {
		global $props;
		unset($this_lat, $this_long, $this_street, $this_zip);

		if (isset($nedss_obj->addresses) && !empty($nedss_obj->addresses)) {
			$this_street = trim($nedss_obj->addresses->street_name);
			$this_zip = trim($nedss_obj->addresses->postal_code);

			$geo_curl = curl_init();
			$geo_url = 'http://'.$props['geo_url'].'/geo/find?street_name='.@urlencode($this_street).'&zip='.@urlencode($this_zip);
			curl_setopt($geo_curl, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($geo_curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($geo_curl, CURLOPT_RETURNTRANSFER, true);
			// Warning: only use CURLOPT_VERBOSE for debugging non-production data; can cause addresses to be exposed in error_log
			// curl_setopt($geo_curl, CURLOPT_VERBOSE, true);
			curl_setopt($geo_curl, CURLOPT_URL, $geo_url);

			$smarty_return = curl_exec($geo_curl);
			curl_close($geo_curl);

			if ($smarty_return) {
				$smarty_arr = @json_decode($smarty_return);
				if (is_array($smarty_arr) && (count($smarty_arr) == 1)) {  // validate that exactly one match found
					// make sure the lat & long are well-formed
					$this_lat = (preg_match('/^(\-?\d+(\.\d+)?)$/', trim($smarty_arr[0]->metadata->latitude))) ? trim($smarty_arr[0]->metadata->latitude) : null;
					$this_long = (preg_match('/^(\-?\d+(\.\d+)?)$/', trim($smarty_arr[0]->metadata->longitude))) ? trim($smarty_arr[0]->metadata->longitude) : null;
				}
			}

			if (isset($this_lat) && isset($this_long) && !empty($this_lat) && !empty($this_long)) {
				$nedss_obj->addresses->addChild('latitude', $this_lat);
				$nedss_obj->addresses->addChild('longitude', $this_long);
			}
		}
	}




	/**
	 * Retrieve the HL7 for a specified message ID
	 *
	 * @param int $system_message_id ID number of the message in system_messages to obtain the HL7 string for
	 * @return string
	 */
	function getHL7($system_message_id) {
		global $host_pa, $my_db_schema;
		$message = '';

		// ensure a valid message ID was passed
		if (!filter_var($system_message_id, FILTER_VALIDATE_INT)) {
			return $message;
		}

		$sql = 'SELECT o.message FROM '.$my_db_schema.'system_original_messages o
				INNER JOIN '.$my_db_schema.'system_messages m ON ((o.id = m.original_message_id) AND (m.id = '.filter_var($system_message_id, FILTER_SANITIZE_NUMBER_INT).'));';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			$message = str_replace('\015', "\n", trim(@pg_fetch_result($rs, 0, 0)));
			@pg_free_result($rs);
		} else {
			$message = '';
		}
		return $message;
	}


	function getParsedHL7($system_message_id) {
		$hl7 = getHL7($system_message_id);

		$msg = new HL7\Message($hl7);

		return $msg;
	}

	function getAlternatePatientIDFromHL7($system_message_id) {
		$DEBUG = false;
		$hl7Message = getParsedHL7($system_message_id);
		$result = null;
		// Adding PID.4.1 - Alternate Patient ID
		$pidSegment = $hl7Message->getSegmentsByName("PID");
		if ($DEBUG) {
			error_log('    - $pidSegment: '. json_encode($pidSegment) . ' ' . gettype($pidSegment));
			// error_log('                   '. var_export($pidSegment, true));
		}
		if (!empty($pidSegment)) {
			$pidSegment4 = (isset($pidSegment[0])) ? $pidSegment[0]->getField(4): null;
			if ($DEBUG) {
				error_log('    - $pidSegment4: '. json_encode($pidSegment4));
			}
			if (!empty($pidSegment4)) {
				if (is_string($pidSegment4)) {
					$result = $pidSegment4;
				}
			}
		}

		return $result;
	}


	/**
	 * Gets the correct derived local result value for the lab being processed
	 *
	 * @param SimpleXMLElement $master_xml_obj SimpleXMLElement containing the Master XML for the message being processed
	 * @param int $lab_id ID of the sending facility
	 * @return string
	 */
	function getLocalResultValue($master_xml_obj, $lab_id = 1) {
		global $host_pa, $my_db_schema;

		$child_local_loinc_code = trim($master_xml_obj->labs->local_loinc_code);
		$child_local_code = trim($master_xml_obj->labs->local_code);
		$child_loinc = ((isset($child_local_loinc_code) && !is_null($child_local_loinc_code) && !empty($child_local_loinc_code)) ? $child_local_loinc_code : $child_local_code);
		$lrv1 = trim($master_xml_obj->labs->local_result_value);
		$lrv2 = trim($master_xml_obj->labs->local_result_value_2);

		$sql = 'SELECT '.$my_db_schema.'get_local_result_value(
				\''.pg_escape_string($child_loinc).'\',
				\''.pg_escape_string($lrv1).'\',
				\''.pg_escape_string($lrv2).'\',
				'.intval($lab_id).'
			);';
		//echo $sql;
		$result = trim(@pg_fetch_result(@pg_query($host_pa, $sql), 0, 0));
		return $result;
	}




	/**
	 * Indicates whether the specified ELR message is for a female patient whose last name has potentially changed due to marriage.
	 * or for a minor whose family name has potentially changed due to marriage.
	 * Returns TRUE if patient is female, has same DOB & first name, but different last name; FALSE otherwise (or on error).
	 * If $disease_id is specified, only checks for events with the same disease, and returns an array of matching TriSano event IDs
	 *
	 * @param int $message_id Message ID (from system_messages) for the ELR message being checked
	 * @param int $disease_id TriSano Disease ID (Optional; if specified, limit matches to same-disease events)
	 * @return mixed
	 */
	function hasFamilyNameChanged($message_id = null, $disease_id = null) {
		global $host_pa, $my_db_schema, $props;
		$events = array();
		$d_id = (is_null($disease_id)) ? null : intval(filter_var($disease_id, FILTER_SANITIZE_NUMBER_INT));

		if (is_null($message_id) || !filter_var($message_id, FILTER_VALIDATE_INT)) {
			return false;
		}

		$sql = 'SELECT master_xml FROM '.$my_db_schema.'system_messages WHERE id = '.intval(filter_var($message_id, FILTER_SANITIZE_NUMBER_INT)).';';
		$rs = @pg_query($host_pa, $sql);
		if ($rs === false) {
			return false;
		}
		$master_raw = @pg_fetch_result($rs, 0, 'master_xml');
		$master_sxe = simplexml_load_string($master_raw);

		// calc patient age
		$age_years = 999;
		if (isSet($master_sxe->person->date_of_birth)) {
			$seconds_now = time();
			$seconds_birth = strtotime($master_sxe->person->date_of_birth);
			$age_seconds = $seconds_now - $seconds_birth;
			$age_years = $age_seconds/(365.25*24*3600);
			$age_years = (int)$age_years;
		}
		$gender = (strlen(trim($master_sxe->person->gender)) > 0) ? strtolower(trim($master_sxe->person->gender)) : false;
//error_log(" TODO Jay hasFamilyNameChanged : ".$master_sxe->person->first_name." ".$master_sxe->person->last_name." ".$age_years." gender:".$gender);
		// if the patient is not a female or a minor
		if ($gender !== 'f' && $age_years >= 18) {
			return false;
		}

		$fname = (strlen(trim($master_sxe->person->first_name)) > 0) ? strtolower(trim($master_sxe->person->first_name)) : false;
		$lname = (strlen(trim($master_sxe->person->last_name)) > 0) ? strtolower(trim($master_sxe->person->last_name)) : false;
		$dob_search = (strlen(trim($master_sxe->person->date_of_birth)) > 0) ? date("Y-m-d", strtotime(trim($master_sxe->person->date_of_birth))) : false;

		if ($dob_search === false || $fname === false || $lname === false) {
			return false;
		} else {
			$dob_arr = date_parse(trim($master_sxe->person->date_of_birth));
			$dob_day = intval($dob_arr['day']);
			$dob_month = intval($dob_arr['month']);
			$dob_year = intval($dob_arr['year']);
		}

		unset($master_sxe);
		@pg_free_result($rs);

		$xml = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health>
		<interested_party_attributes>
			<person>
				<first_name>$fname</first_name>
				<birth_date>$dob_search</birth_date>
			</person>
		</interested_party_attributes>
	</trisano_health>
</health_message>
EOX;

		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			return false;
		} catch (SoapFault $f) {
			return false;
		}

		if ($client) {
			try {
				$result = $client->searchPerson(array("healthMessage" => $xml));
			} catch (Exception $e) {
				return false;
			} catch (SoapFault $f) {
				return false;
			}
			$return = simplexml_load_string($result->return);

			foreach ($return->trisano_health as $return_item) {
				// check results found here...
				unset($this_person);
				$this_person = array(
					'fname' => ((strlen(trim($return_item->interested_party_attributes->person->first_name)) > 0) ? strtolower(trim($return_item->interested_party_attributes->person->first_name)) : false),
					'lname' => ((strlen(trim($return_item->interested_party_attributes->person->last_name)) > 0) ? strtolower(trim($return_item->interested_party_attributes->person->last_name)) : false),
					'gender' => intval(trim($return_item->interested_party_attributes->person->birth_gender_id)),
					'dob' => ((strlen(trim($return_item->interested_party_attributes->person->birth_date)) > 0) ? date_parse(trim($return_item->interested_party_attributes->person->birth_date)) : false)
				);
				if ($this_person['dob'] === false || $this_person['fname'] === false || $this_person['lname'] === false ||
					($this_person['gender'] != 2 && $age_years >= 18)) {
					break;
				} else {
					if (($this_person['fname'] == $fname) && ($this_person['lname'] != $lname) && (intval($this_person['dob']['day']) == $dob_day) &&
						  (intval($this_person['dob']['month']) == $dob_month) && (intval($this_person['dob']['year']) == $dob_year)) {
						if (!is_null($d_id) && isset($return_item->disease_events)) {
							// if disease specified, check for same-disease events here...
							foreach ($return_item->disease_events as $disease_event) {
								if (intval(trim($disease_event->disease_id)) == $d_id) {
									$events[intval(trim($disease_event->event_id))] = true;
								}
							}
						} elseif (is_null($d_id)) {
							return true;
						}
					}
				}
			}

			if (count($events) > 0) {
				return $events;
			}
		}

		return false;
	}




	/**
	 * Returns the name of a LHD Investigator from TriSano specified by User ID
	 *
	 * @param string $user_id TriSano User ID
	 * @return string
	 */
	function getInvestigatorNameById($user_id) {
		global $props;
		$user_name = '';
		$u_id = filter_var($user_id, FILTER_SANITIZE_NUMBER_INT);
		$xml = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health>
		<role_memberships>
			<user_id>$u_id</user_id>
		</role_memberships>
	</trisano_health>
</health_message>
EOX;

		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e->getMessage(), -1, 1);
		} catch (SoapFault $f) {
			suicide($f->getMessage(), -1, 1);
		}

		if ($client) {
			try {
				$result = $client->getUsers(array("healthMessage" => $xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$return = simplexml_load_string($result->return);

			foreach ($return->trisano_health as $th_obj) {
				$user_name = trim($th_obj->users_attributes->users->givenName);
			}
		}
		return $user_name;
	}




	/**
	 * Records an audit of actions modifiying NEDSS XML, including previous version of NEDSS XML from TriSano in case of an update.
	 *
	 * @param string $old_xml Previous NEDSS XML (if updating a previous event)
	 * @param string $new_xml Modified version of the NEDSS XML being sent to TriSano to add/update an event
	 * @param int $system_messages_audits_id ID from system_messages_audits that this update generated
	 * @param bool $is_update If true, this is for an 'updateCmr', otherwise for an 'addCmr'
	 * @return int|bool
	 */
	function auditXML($old_xml = null, $new_xml = null, $system_messages_audits_id = null, $is_update = false) {
		global $host_pa, $my_db_schema;

		if (is_null($new_xml) || is_null($system_messages_audits_id) || empty($system_messages_audits_id) || empty($new_xml)) {
			return false;
		}

		$sql = 'INSERT INTO '.$my_db_schema.'system_nedss_xml_audits (is_update, system_messages_audits_id, previous_xml, sent_xml) VALUES (
			'.(($is_update === true) ? '\'t\'' : '\'f\'').',
			'.intval($system_messages_audits_id).',
			'.((!is_null($old_xml) && !empty($old_xml)) ? '\''.pg_escape_string($old_xml).'\'' : 'NULL').',
			\''.pg_escape_string($new_xml).'\') RETURNING id;';
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false) {
			return @pg_fetch_result($rs, 0, "id");
		} else {
			return false;
		}

		@pg_free_result($rs);
	}




	/**
	 * Generate an Exception event for an ELR message and move the message to the Exception queue.
	 * Returns TRUE on success, or FALSE on error.
	 *
	 * @param int $message_id ELR Message ID (from system_messages)
	 * @param int $audit_id Audit Log ID to tie into system_audit_exceptions
	 * @param int $action_id ID corresponding to the generated exception (from system_exceptions)
	 * @param string $info Optional exception details
	 * @return bool
	 */
	function messageException($message_id = null, $audit_id = null, $action_id = null, $info = null) {
		global $host_pa, $my_db_schema;

		if (is_null($message_id) || is_null($action_id) || is_null($audit_id)) {
			return false;
		}

		$qry = "BEGIN;".PHP_EOL;
		$qry .= "INSERT INTO ".$my_db_schema."system_audit_exceptions (system_messages_audits_id, system_exceptions_id, info) VALUES (
			".filter_var($audit_id, FILTER_SANITIZE_NUMBER_INT).",
			".filter_var($action_id, FILTER_SANITIZE_NUMBER_INT).",
			".((strlen(trim($info)) > 0) ? "'".pg_escape_string(trim($info))."'" : "NULL").");".PHP_EOL;
		$qry .= "INSERT INTO ".$my_db_schema."system_message_exceptions (system_message_id, exception_id, info) VALUES (
			".filter_var($message_id, FILTER_SANITIZE_NUMBER_INT).",
			".filter_var($action_id, FILTER_SANITIZE_NUMBER_INT).",
			".((strlen(trim($info)) > 0) ? "'".pg_escape_string(trim($info))."'" : "NULL").");".PHP_EOL;
		$qry .= "UPDATE ".$my_db_schema."system_messages SET final_status = 3 WHERE id = ".filter_var($message_id, FILTER_SANITIZE_NUMBER_INT).";".PHP_EOL;
		$qry .= "COMMIT;";
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			return true;
		} else {
			return false;
		}

		@pg_free_result($rs);
	}




	function nedssLinkByEventId($event_id) {
		$record_number = trisanoRecordNumberByEventId($event_id);
		return '<button type="button" class="emsa_btn_viewnedss" id="emsa_btn_viewnedss_'.$event_id.'" value="'.$event_id.'" title="View event in TriSano">Record# '.$record_number.'</button> [Event ID#: '.$event_id.']<br>';
	}




	function nedssLinkByEventArray($event_array) {
		$event_list = '';
		foreach ($event_array as $each_event_id => $each_event_person) {
			$event_list .= nedssLinkByEventId($each_event_id);
		}
		return $event_list;
	}


	/**
	 * Check a facility name to see if it is known to be a hospital in the SNHD area.
	 *
	 * @param string $name -- the facility name
	 */
	function isSNHDHospital($name) {

		// check for names containing 'Hospital', or St Rose, or UMC, or the HCA hospitals
		if (preg_match("/Hospital/i", $name) || preg_match("/Rose/", $name) || preg_match("/UMC/", $name) ||
			preg_match("/HCA/", $name)){
			return true;
		}
		return false;
	}

	/**
	 * Check a facility name to see if it is a deriviative of SNPHL but not SNPHL-RHSHC.
	 *
	 * @param string $name -- the facility name
	 */
	function isSnphlNotShc($name) {
		if (preg_match("/SNPHL/i", $name) && !preg_match("/SNPHL-SHC/i", $name)) {
			return true;
		}
		return false;
	}

	/**
	 * Check a facility name to see if it is a deriviative of SNPHL.
	 *
	 * @param string $name -- the facility name
	 */
	function isSnphl($name) {
		return preg_match("/SNPHL/i", $name);
	}

	/**
	 * Modify the xml for creating a NEW CMR to include SNHD whitelist actions
	 *
	 */
	function addSNHDNewWhitelistSettings($system_message_id, $white_rule, $test_result, &$new_sxe, &$person_identifier, $disease_name) {
		global $props;

		if($props['emsa_environment'] != 'SNHD') {
			return;
		}
		unset($new_sxe->destination_queue);
		unset($new_sxe->create_assessment_event);

		if(isPropertyTrue($props['set_lhd_case_status'])) {
			$new_sxe->events->lhd_case_status_id = $new_sxe->events->state_case_status_id;
		}
		if(!isSet($new_sxe->disease_events->date_diagnosed)) {
			$new_sxe->disease_events->date_diagnosed = $new_sxe->labs_attributes->lab_results->lab_test_date;
		}

		// if this is an STD condition
		if (stripos($white_rule, 'snhd std') !== false) {
			$isHiv = (stripos($white_rule, 'hiv') !== false);
			$isSyph = (stripos($white_rule, 'syph') !== false);
			$isNgct = (stripos($white_rule, 'ngct') !== false);
//error_log("TODO Jay isHiv: ".$isHiv);
//error_log("TODO Jay isSyph: ".$isSyph);
//error_log("TODO Jay isNgct: ".$isNgct);


			// All new STD events are AEs, except of CMRS created by positive NG/CT results
			if($test_result != 'POSITIVE' || !$isNgct) {
				$new_sxe->create_assessment_event = true;
			}
			if($isHiv) {
				hivNewAEProcessing($new_sxe, $test_result, $person_identifier);
			} else if ($isSyph) {
				syphNewAEProcessing($new_sxe, $test_result, $person_identifier);
			} else {
				ngctNewEventProcessing($new_sxe);
			}
		// else if a TB condition
		} else if (stripos($white_rule, 'snhd tb') !== false) {
error_log("TODO Jay white rule: ".$white_rule);
			tbNewAEProcessing($new_sxe, $test_result, $person_identifier);
		} else { // OOE condition

			$diagnostic_facility = '';
			if(isSet($new_sxe->diagnostic_facilities_attributes->place->name)) {
				$diagnostic_facility = $new_sxe->diagnostic_facilities_attributes->place->name;
			}

			// flu events should be closed if the reporting agency is not a hospital
			// and not an SNPHL Lab, and the person is not deceased (coroner involvement)
			if (stripos($white_rule, 'snhd flu') !== false) {
				$coronerInvolved = isCoronerInvolved($system_message_id);
				if($coronerInvolved) {
					addNote("This lab contains a reference to the coroner.", $new_sxe);
				}
				if(!isSNHDHospital($reporting_agency) && !isSNHDHospital($diagnostic_facility) &&
					!isSnphl($reporting_agency) && !$coronerInvolved) {
					setWorkflow($new_sxe, 'closed');
				}
			}

			// RSV events should be closed UNLESS an SNPHL Lab
			if($disease_name == 'RSV' && !isSnphl($reporting_agency)) {
				setWorkflow($new_sxe, 'closed');
			}

			// by default OOE events go to the 'OOE New' queue
			if (!isSet($new_sxe->events->workflow_state) || $new_sxe->events->workflow_state != 'closed'){
				$new_sxe->destination_queue = 'OOE New';
			}

			if (stripos($white_rule, 'snhd hepc') !== false) {
				$new_sxe->destination_queue = 'HepCChronic-Snhdooe';
			} elseif (stripos($white_rule, 'snhd hepb') !== false && stripos($white_rule, 'igm') === false) {
				$new_sxe->destination_queue = 'HepBChronic-Snhdooe';
			}

            // new Zika events are AEs
            if(strtoupper(substr($disease_name,0,4)) == 'ZIKA') {
                $new_sxe->create_assessment_event = true;
            }
		}
error_log('TODO NEW Event Jay disease name: '.$disease_name.'\nnew_sxe: '.$new_sxe->asXML());
	}
	/**
	 * Modify the xml for UPDATING a CMR to include SNHD whitelist actions
	 *
	 */
	 function addSNHDUpdateWhitelistSettings($white_rule, $test_result, $result_value, &$add_sxe, &$orig_xml, $reported_date,
		$event_disease_name, $workflow, $investigator_id, $event_queue) {
		global $props;

		if($props['emsa_environment'] != 'SNHD') {
			return;
		}
		// error_log('[x] DEBUG: addSNHDUpdateWhitelistSettings:'.__LINE__.' $add_sxe object = '.json_encode($add_sxe));
		unset($add_sxe->destination_queue);
		unset($add_sxe->add_positive_lab_task);
error_log("addSNHDUpdate white rule: ".$white_rule." test result: ".$test_result);
		// if this is an hiv lab
		if(stripos($white_rule, 'snhd std hiv') !== false) {
			hivAttachLabProcessing($add_sxe, $orig_xml, $test_result, $workflow, $investigator_id, $event_disease_name, $event_queue);
		// else if this is a Syphilis lab
		} else if(stripos($white_rule, 'snhd std syph') !== false) {
			syphAttachLabProcessing($add_sxe, $orig_xml, $test_result, $workflow, $investigator_id, $event_disease_name, $event_queue);
		// else if this is an NG or CT lab
		} else if(stripos($white_rule, 'snhd std ngct') !== false) {
			ngctAttachLabProcessing($add_sxe, $test_result, $workflow, $investigator_id, $event_queue);
		} else if(stripos($white_rule, 'snhd tb') !== false) {
			tbAttachLabProcessing($add_sxe, $test_result, $workflow, $investigator_id, $event_queue);
		} else {
			// OOE lab

			// if we are adding a positive result
	//error_log("TODO JAY  result value:".$test_result." white rule: ".$white_rule);
			if($test_result == 'POSITIVE') {
	//error_log("TODO JAY pos result date:".$reported_date." white rule: ".$white_rule);
				// if a hep b igm result and the existing case is less than 6 months old
				$seconds_since_reported_date = time() - $reported_date;
	//error_log("TODO JAY pos result seconds:".$seconds_since_reported_date);
				if(stripos($white_rule, 'snhd hepb igm') !== false  &&
					$seconds_since_reported_date < (365.25*24*3600/2)) {
					// move the existing case to the "OOE New" queue
	//error_log("TODO JAY pos result Hep B adding to queue");
					$add_sxe->destination_queue = 'OOE New';
				}
				// if an ALT result > 400 being assigned to a Hep C case less than 6 months old
				// Vocab for ALT test should be set for 400+ => positive, 399- => equivocal
	//error_log("TODO JAY loinc:".$add_sxe->labs_attributes->lab_results->loinc_code);
				if(isset($add_sxe->labs_attributes->lab_results->loinc_code) &&
					$add_sxe->labs_attributes->lab_results->loinc_code == '1742-6' &&
					$seconds_since_reported_date < (365.25*24*3600/2)) {
					// move the existing case to the "OOE New" queue
					$add_sxe->destination_queue = 'OOE New';
					// Add a note
					addNote("ALT result above 400 added: ", $add_sxe);
				}

				// For OOE cases, add a task for positive labs
				if(isset($investigator_id) && $investigator_id != null) {
					$add_sxe->add_positive_lab_task = 'SNHDOOE';
				}
			}
		}
error_log('TODO Jay UPDATE add_sxe: '.$add_sxe->asXML());
	}

	/**
	 * Modify the result_events array to contain the single newest event matching SNHD STD white list rules
	 *  return true if this was done, false otherwise
	 *
	 */
	function useSNHDStdWhiteList(&$orig_sxe, &$orig_xml, &$result_events, $whitelist_rule, $condition, $test_name, $test_result) {
		global $props;

//error_log("UseSNHDstdWhiteList rule: ".$whitelist_rule);
		if ($props['emsa_environment'] != 'SNHD' || $whitelist_rule != 'snhd_std' || !isset($result_events) || count(result_events) < 1) {
			return false;
		}

		unset($orig_sxe->move_to_gray_list);
		unset($orig_sxe->discard_lab);
		unset($orig_sxe->history_of_hiv);

		$result_events_tmp = array();
		$time_selected_event = -1;
		$old_count = 0;

		$loinc = trim($orig_xml->labs_attributes->lab_results->loinc_code);

error_log("TODO JAY STD count people total: ".count($result_events)." condition: ".$condition." ".$test_name." ".$loinc);

		// if any closed HIV or HIV/co-infection closed CMR exists AND
		// (this is a CD4 or Viral Load lab OR this is an HIV lab and the patient is not a female aged 15-49)
		if($condition == 'hiv' && checkSNHDRules('testHIVGrayListRules', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			// move lab to gray list
			$result_events_tmp = array();
			error_log("TODO JAY HIV Lab to be moved to Gray list");
			$orig_sxe->move_to_gray_list = true;
		// if any closed HIV or HIV/co-infection closed CMR exists AND
		// this is an HIV genotype lab
		} else if($condition == 'hiv' && checkSNHDRules('testHIVGenotypeRule', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			// move lab to gray list
			$result_events_tmp = array();
			error_log("TODO JAY HIV Genotype Lab discarded");
			$orig_sxe->discard_lab = true;
		// else if this is a CD4 or negative viral load lab
		} else if($condition == 'hiv' && isCD4orNegHivViralTest($loinc, $test_result)) {
			$result_events_tmp = array();
			error_log("TODO JAY HIV Lab will be discarded");
			// discard this lab
			$orig_sxe->discard_lab = true;
		// else if there is any open STD assessment or morbidity events
		} else if(checkSNHDRules('testOpenSNHDEvent', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			error_log("TODO JAY STD count of people/events on open event pass: ".count($result_events_tmp));
		// then check for any STD event where the most recent lab test date equals the date for the lab we are processing
		} else if(checkSNHDRules('testMatchingSNHDLabDate', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			error_log("TODO JAY STD count of people/events on matching lab test date pass: ".count($result_events_tmp));
		// then check for a any std event within 30 days (including closed cases)
		} else if(checkSNHDRules('testStd30Day', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			error_log("TODO JAY STD count of people/events on  30 day pass: ".count($result_events_tmp));
		}

		// if we are going to create a new (non-HIV) Event, check for a history of HIV
		if($condition != 'hiv' && count($result_events_tmp) < 1) {
			foreach ($result_events as $person_id => $person_events) {
				foreach ($person_events as $event_id => $event_data) {
					if ($result_events[$person_id][$event_id]['event_type'] == 'MorbidityEvent' &&
						isSNHDHivDiseaseEvent($event_id, $result_events[$person_id][$event_id])) {
						$orig_sxe->history_of_hiv = true;
						break 2;
					}
				}
			}
		}

		// make the result list equal the single result we chose (if we chose one).
error_log("TODO JAY STD count of people on last pass: ".count($result_events_tmp));
		$result_events = $result_events_tmp;
		return true;
	}


	/**
	 * Modify the result_events array to contain the single newest event matching SNHD STD white list rules
	 *  return true if this was done, false otherwise
	 *
	 */
	function useSNHDTBWhiteList(&$orig_sxe, &$orig_xml, &$result_events, $whitelist_rule, $condition, $test_name, $test_result) {
		global $props;

error_log("UseSNHDTBWhiteList rule: ".$whitelist_rule);
		if ($props['emsa_environment'] != 'SNHD' || $whitelist_rule != 'snhd_tb' || !isset($result_events) || count(result_events) < 1) {
			return false;
		}

		unset($orig_sxe->move_to_gray_list);
		unset($orig_sxe->discard_lab);
		unset($orig_sxe->history_of_hiv);

		$result_events_tmp = array();
		$time_selected_event = -1;
		$old_count = 0;

error_log("TODO JAY TB TB TB count people total: ".count($result_events)." condition: ".$condition." ".$test_name);

		//if there is any open TB event
		if(checkSNHDRules('testOpenSNHDEvent', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			error_log("TODO JAY TB TB TB count of people/events on open event pass: ".count($result_events_tmp));
		// then check for any TB TB TB event where the most recent lab test date equals the date for the lab we are processing
		} else if(checkSNHDRules('testMatchingSNHDLabDate', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			error_log("TODO JAY TB TB TB count of people/events on matching lab test date pass: ".count($result_events_tmp));
		} else if(checkSNHDRules('testTBPcrProbeActive', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			error_log("TODO JAY TB TB TB count of people/events on PCR/Probe Active pass: ".count($result_events_tmp));
//		} else if(checkSNHDRules('testTBQftTspot', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
//			error_log("TODO JAY TB TB TB count of people/events on QFT/TSpot pass: ".count($result_events_tmp));
		} else if($test_result == 'POSITIVE' &&
			checkSNHDRules('testTBPcrProbe3MonthLTBICoInfect', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
			error_log("TODO JAY TB TB TB count of people/events on LTBI PCR/Probe pass: ".count($result_events_tmp));
//		} else if(checkSNHDRules('testTB3To12Month', $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $test_name)) {
//			error_log("TODO JAY TB TB TB count of people/events on TB 3 to 12 Month pass: ".count($result_events_tmp));
		}

		// make the result list equal the single result we chose (if we chose one).
error_log("TODO JAY TB TB TB count of people/events on last pass: ".count($result_events_tmp));
		$result_events = $result_events_tmp;
		return true;
	}

	/**
	 * Test the list of result events to see if it matches the passed
	 *   SNHD rule ($testFunction) for lab assignment
	 */
	function checkSNHDRules($testFunction, &$orig_xml, &$result_events_tmp, &$result_events, &$time_selected_event, $test_name='') {
		foreach ($result_events as $person_id => $person_events) {
			foreach ($person_events as $event_id => $event_data) {
error_log("TODO Jay checkSNHDRules person id: ".$person_id." event id: ".$event_id. " function: ".$testFunction);
				if(call_user_func($testFunction, $orig_xml, $result_events_tmp, $result_events, $time_selected_event, $event_id, $person_id, $test_name)) {
					$result_events_tmp = array();
					$time_selected_event = $result_events[$person_id][$event_id]['reported_date'];
					if(!isset($time_selected_event) || $time_selected_event == null) {
						$time_selected_event = 0;
					}
					$result_events_tmp[$person_id][$event_id] = $result_events[$person_id][$event_id];
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * if the event in result_events specified by $person_id/$event_id is an active STD AE/CMR/Contact and it has a more recent reported_date
	 *  than $time_selected_event, return true
	 *  Otherwise return false
	 */
	function testOpenSNHDEvent(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {

		$event_reported_date = $result_events[$person_id][$event_id]['reported_date'];
	    if(!isset($event_reported_date) || $event_reported_date == null) {
			$event_reported_date = 0;
		}
error_log("TestOPEN: event type: ".$result_events[$person_id][$event_id]['event_type']." workflow: ".$result_events[$person_id][$event_id]['workflow']);
error_log("TestOPEN: selected time: ".$time_selected_event." event time: ".$event_reported_date);
		if (in_array($result_events[$person_id][$event_id]['event_type'], array('MorbidityEvent', 'AssessmentEvent', 'ContactEvent')) &&
				!in_array($result_events[$person_id][$event_id]['workflow'], array('closed', 'approved_by_lhd', 'not_routed')) &&
				$event_reported_date > $time_selected_event){
error_log("TODO Jay >>> active std event person_id: ".$person_id." event id: ".$event_id);
//error_log("TODO Jay >>> active std event result event: ".print_r($result_events[$person_id][$event_id], true));
			return true;
		}
		return false;
	}

	/**
	 * if the event in result_events specified by $person_id/$event_id is an STD AE/CMR and it's most recent lab date matches
	 *  the collection date from $orig_xml and it has a more recent reported_date
	 *  than $time_selected_event, return true
	 *  Otherwise return false
	 */
	function testMatchingSNHDLabDate(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {
		// extract the date (not including time) for the current lab and the most recent lab of the event
		$new_lab_date = strftime("%Y-%m-%d", strtotime($orig_xml->labs_attributes->lab_results->collection_date));
		$old_lab_date = strftime("%Y-%m-%d", strtotime($result_events[$person_id][$event_id]['last_lab_date']));
//error_log("TODO Jay match lab date, new lab date: ".$new_lab_date." old lab date: ".$old_lab_date." time selected event: ".$time_selected_event);
		if (in_array($result_events[$person_id][$event_id]['event_type'], array('MorbidityEvent', 'AssessmentEvent', 'ContactEvent')) &&
				$old_lab_date == $new_lab_date &&
				$result_events[$person_id][$event_id]['reported_date'] > $time_selected_event) {
//error_log("TODO Jay >>> person id:".$person_id." event id:".$event_id);
//error_log("TODO Jay >>> matching test date result event: ".print_r($result_events[$person_id][$event_id], true));
			return true;
		}
		return false;
	}

	/**
	 * if the (open or closed) event in result_events specified by $person_id/$event_id is an STD AE/CMR and
     *   the event (earliest lab collection or reported date) is within 30 (+/-) days of the current lab,
     *   and the event is newer than the already selected event,
     *     return true
	 *  Otherwise return false
	 */
	function testStd30Day(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {
		if (in_array($result_events[$person_id][$event_id]['event_type'], array('MorbidityEvent', 'AssessmentEvent', 'ContactEvent')) &&
			$result_events[$person_id][$event_id]['first_lab_or_report'] > $time_selected_event) {
			$delta_time = '+30 days';
			$first_lab_or_report_plus30 = strtotime($delta_time, $result_events[$person_id][$event_id]['first_lab_or_report']);
			$delta_time = '-30 days';
			$first_lab_or_report_minus30 = strtotime($delta_time, $result_events[$person_id][$event_id]['first_lab_or_report']);
//error_log("TODO Jay testStd30Day event date: ".$result_events[$person_id][$event_id]['first_lab_or_report']);
//error_log("TODO Jay >>>> first_lab_or_report_plus30: ".$first_lab_or_report_plus30." collection date: ".strtotime(trim($orig_xml->labs_attributes->lab_results->collection_date)));
			$collection_date=$orig_xml->labs_attributes->lab_results->collection_date;
			if(isset($collection_date) && $collection_date != NULL) {
				$collection_date = trim($collection_date);
				if (!empty($collection_date) && $first_lab_or_report_plus30 > strtotime($collection_date) &&
                    $first_lab_or_report_minus30 < strtotime($collection_date)) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * if any closed HIV or co-infection closed CMR exists AND
	 * (this is a CD4 or Viral Load lab OR this is an HIV lab and the patient is NOT a female aged 15-49), return true
	 *  Otherwise return false
	 */
	function testHIVGrayListRules(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {
		$loinc = trim($orig_xml->labs_attributes->lab_results->loinc_code);
//error_log("TODO testHIVGrayListRules :".$result_events[$person_id][$event_id]['event_type']." ".$result_events[$person_id][$event_id]['workflow']." ".$test_name);
		if (isSNHDHivDiseaseEvent($event_id, $result_events[$person_id][$event_id]) &&
			in_array($result_events[$person_id][$event_id]['event_type'], array('MorbidityEvent')) &&
			in_array($result_events[$person_id][$event_id]['workflow'], array('closed', 'approved_by_lhd', 'not_routed')) &&
			(isCD4orHivViralTest($loinc) || !isFemale15to49($orig_xml))) {
error_log("TODO testHIVGrayListRules returning TRUE");
			return true;
		}
		return false;
	}

	/**
	 * if any closed HIV or co-infection closed CMR exists AND
	 * this is a genotype lab, return true
	 *  Otherwise return false
	 */
	function testHIVGenotypeRule(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {
//error_log("TODO testHIVGenotypeRules :".$result_events[$person_id][$event_id]['event_type']." ".$result_events[$person_id][$event_id]['workflow']." ".$test_name);
		if (isSNHDHivDiseaseEvent($event_id, $result_events[$person_id][$event_id]) &&
			in_array($result_events[$person_id][$event_id]['event_type'], array('MorbidityEvent')) &&
			in_array($result_events[$person_id][$event_id]['workflow'], array('closed', 'approved_by_lhd', 'not_routed')) &&
			(isHivGenotypeTest($test_name))) {
//error_log("TODO testHIVGenotypeRule returning TRUE");
			return true;
		}
		return false;
	}

	/**
	 * if the lab test is a PCR/Probe and event in result_events specified by $person_id/$event_id is a TB,Active CMR <= 12 months old
	 *  return true
	 *  Otherwise return false
	 */
	function testTBPcrProbeActive(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {

		if($result_events[$person_id][$event_id]['event_type'] == 'MorbidityEvent') {
			$delta_time = '+1 year';
			$reported_date_plus_year = strtotime($delta_time, $result_events[$person_id][$event_id]['reported_date']);
			$disease_name = $result_events[$person_id][$event_id]['disease_name'];

			$loinc = trim($orig_xml->labs_attributes->lab_results->loinc_code);
//error_log("TODO Jay test TBPcrProbeActive test: ".$test_name." disease: ".$result_events[$person_id][$event_id]['disease_name'].
//				" event date+year: ".$reported_date_plus_year. " loinc: ".$loinc." now: ".time());

			if($disease_name == "Tuberculosis, Active" && isPCRProbeTest($loinc) && $reported_date_plus_year > time()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * if the lab test is a PCR/Probe and event in result_events specified by $person_id/$event_id is an LTBI or co-infection CMR <= 3 months old
	 *  return true
	 *  Otherwise return false
	 */
	function testTBPcrProbe3MonthLTBICoInfect(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {

		if($result_events[$person_id][$event_id]['event_type'] == 'MorbidityEvent') {
			$delta_time = '+3 months';
			$reported_date_plus_3months = strtotime($delta_time, $result_events[$person_id][$event_id]['reported_date']);
			$disease_name = $result_events[$person_id][$event_id]['disease_name'];

			$loinc = trim($orig_xml->labs_attributes->lab_results->loinc_code);
error_log("TODO Jay test testTBPcrProbe3MonthLTBICoInfect reported_date: ".date("Y-m-d", $result_events[$person_id][$event_id]['reported_date'])." : ".
	$result_events[$person_id][$event_id]['reported_date']);

error_log("TODO Jay test testTBPcrProbe3MonthLTBICoInfect test: ".$test_name." disease: ".$disease_name.
				" event date+3 months ".date("Y-m-d", $reported_date_plus_3months). " loinc: ".$loinc." now: ".time());

			if((strpos($disease_name, 'LTBI') !== false || strpos($disease_name, 'TB Co-Infection') !== false) &&
				isPCRProbeTest($loinc) && $reported_date_plus_3months > time()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * if the event in result_events specified by $person_id/$event_id is an LTBI CMR over 3 months old or a TB,Active CMR <= 12 months old
	 *  return true
	 *  Otherwise return false
	 */
	function testTB3To12Month(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {

		if($result_events[$person_id][$event_id]['event_type'] == 'MorbidityEvent') {
			$delta_time = '+1 year';
			$reported_date_plus_year = strtotime($delta_time, $result_events[$person_id][$event_id]['reported_date']);
//error_log("TODO Jay result event: ".print_r($result_events[$person_id][$event_id], true));
//			$delta_time = '+3 months';
			$reported_date_plus_3months = strtotime($delta_time, $result_events[$person_id][$event_id]['reported_date']);
			$disease_name = $result_events[$person_id][$event_id]['disease_name'];

			$loinc = trim($orig_xml->labs_attributes->lab_results->loinc_code);
//error_log("TODO Jay test testTB3To12Month test: ".$test_name." disease: ".$disease_name."event date: ".$result_events[$person_id][$event_id]['reported_date'].
//				" event date+3months: ".date("Y-m-d", $reported_date_plus_3months). " loinc: ".$loinc." now: ".date("Y-m-d",time()));

			// LTBI CMR > 3 months old
			if(strpos($disease_name, 'LTBI') !== false && $reported_date_plus_3months < time()) {
				return true;
			}
			// Tuberculosis, Active CMR <= 12 months
			if($disease_name == "Tuberculosis, Active" && $reported_date_plus_year > time()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * if the lab test is a QFT/TSpot and event in result_events specified by $person_id/$event_id is a TB CMR
	 *  return true
	 *  Otherwise return false
	 */
	function testTBQftTspot(&$orig_xml, &$result_events_tmp, &$result_events, $time_selected_event, $event_id, $person_id, $test_name) {

		$loinc = trim($orig_xml->labs_attributes->lab_results->loinc_code);
//error_log("TODO Jay test testTBQftTspot test: ".$test_name." disease: ".$result_events[$person_id][$event_id]['disease_name'].
//			"type: ".$result_events[$person_id][$event_id]['event_type']." loinc: ".$loinc);

		if($result_events[$person_id][$event_id]['event_type'] == 'MorbidityEvent' &&
			isQftTspotTest($loinc) ) {
			return true;
		}
		return false;
	}


    /**
	 * Return true if patient is not a female of child-bearing age
	 *
	 */
	function isFemale15to49(&$xml) {
//error_log("female15to49 xml: ".$xml->asXml());
//error_log("female15to49 gender: ".$xml->interested_party_attributes->person->birth_gender_id);
		if($xml->interested_party_attributes->person->birth_gender_id != 2) {
			return false;
		}
		$age_years = calcPatientAge($xml);
//error_log("female15to49 age: ".$age_years);
		if($age_years < 15 || $age_years > 49) {
			return false;
		}
		return true;
	}

	/**
	 * Return true if the passed event is in the list of SNHD HIV conditions (including HIV co-infection)
	 * This only returns TRUE if this is an HIV co-infect (not syph/ct for example)
	 */
	function isSNHDHivDiseaseEvent($event_id, $event) {
		global $props, $host_trisano;
		if ($props['emsa_environment'] == 'SNHD') {
			$disease_name = $event['disease_name'];
//error_log("isSNHDHivDiseaseEvent: ".$disease_name." host:".$host_trisano);
//error_log("isSNHD event: ".print_r($event, true));
			// if this is a co-infection
			if(stripos($disease_name, 'co-infection') !== false) {
				// get the HIV co-infection condition from the event
				$sql = "select text_answer from answers  INNER JOIN questions  ON questions.id = answers.question_id AND ".
					"questions.short_name ='hiv_condition' WHERE answers.event_id=".trim($event_id).";";
				$rs = @pg_query($host_trisano, $sql);
//error_log("isSNHDHivDiseaseEvent sql: ".$sql." rs:".$rs);
				if ($rs !== false && @pg_num_rows($rs) == 1) {
					// if this is an HIV co-infect
					$hiv_condition = trim(@pg_fetch_result($rs, 0, 'text_answer'));
					@pg_free_result($rs);
//error_log("isSNHDHivDiseaseEvent hiv_condition: ".$hiv_condition);
					if(strlen($hiv_condition) > 0) {
						return true;
					}
				}
				return false;
			}

			if(strpos($disease_name, 'HIV') !== false || strpos($disease_name, 'AIDS') !== false) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return true if the passed test_name is for a HIV test of the specified type
	 *
	 */
	function isHIVTestType($loinc, $type) {
		global $host_pa, $my_db_schema, $props;
		if ($props['emsa_environment'] == 'SNHD') {

            $sql = "SELECT vml.concept_name, vma.coded_value FROM ".$my_db_schema."vocab_master_loinc vml ".
                "INNER JOIN ".$my_db_schema."vocab_master2app vma ON vml.trisano_test_type = vma.master_id ".
                "INNER JOIN ".$my_db_schema."vocab_master_condition vmc ON vmc.c_id = vml.trisano_condition ".
                "INNER JOIN ".$my_db_schema."vocab_master_vocab vmv ON vmv.id = vmc.condition "."AND vmv.concept ILIKE '%HIV%' ".
                "AND vma.coded_value ILIKE '%".$type."%' AND vml.loinc = '".$loinc."'";

//error_log("isHivTextType sql " . $sql);
            $rs = @pg_query($host_pa, $sql);
//error_log("rs: ". $rs);
//error_log("Count: ".@pg_num_rows($rs));
            if ($rs === false || @pg_num_rows($rs) < 1) {
//error_log("isHivTextType returning false ");
                return false;
            }
			$concept = trim(@pg_fetch_result($rs, 0, 'concept_name'));
			$value = trim(@pg_fetch_result($rs, 0, 'coded_value'));
//error_log("isHivTextType returning true '".$value."'-->".$concept);
            return true;
		}
		return false;
	}


	/**
	 * Return true if the passed test_name is for a CD4 or HIV viral load test
	 *
	 */
	function isCD4orHivViralTest($loinc) {
		global $props;
		if ($props['emsa_environment'] == 'SNHD') {
            return (isHIVTestType($loinc, "CD4") || isHIVTestType($loinc, "Viral load"));
		}
		return false;
	}

    /**
	 * Return true if the test_name/result is for a CD4 or Neg HIV viral load test
	 *
	 */
	function isCD4orNegHivViralTest($loinc, $test_result) {
		global $props;
		if ($props['emsa_environment'] == 'SNHD') {
            return (isHIVTestType($loinc, "CD4") || (strtoupper(substr($test_result,0,8)) == 'NEGATIVE' && isHIVTestType($loinc, "Viral load")));
 		}
		return false;
	}

	/**
	 * Return true if the passed test_name is for an HIV Genotype test
	 *
	 */
	function isHivGenotypeTest($test_name) {
//error_log("TODO isHivGenotypeTest: ".$test_name);
		if(stripos($test_name, 'genotype') !== false) {
error_log("TODO isHivGenotypeTest returning TRUE");
			return true;
		}
		return false;
	}

	/**
	 * Return true if the passed loinc is for a TB PCR or Probe Test
	 *
	 */
	function isPcrProbeTest($loinc) {
		if(strlen($loinc) > 0 && strpos('13956-8,17296-5', $loinc) !== false) {
error_log("TODO isPcrProbeTest returning TRUE for loinc: ".$loinc);
			return true;
		}
error_log("TODO isPcrProbeTest returning FALSE for loinc: ".$loinc);
	return false;
	}

	/**
	 * Return true if the passed loinc is for a TB Qft or TSpot test
	 *
	 */
	function isQftTspotTest($loinc) {
		if(strlen($loinc) > 0 &&
		strpos('39017-9,45323-3,46216-8,71772-8,71773-6,71774-4,74280-9,74281-7', $loinc) !== false) {
error_log("TODO isQftTspot returning TRUE for loinc: ".$loinc);
			return true;
		}
error_log("TODO isQftTspot returning FALSE for loinc: ".$loinc);
	return false;
	}

	/**
	 * Return true if the passed loinc is for a TB AFB, Smear or culture test
	 * this function assumes it has been passed a loincs for a TB test
	 *
	 */
	function isAfbSmearCultureTest($loinc) {
		if(strlen($loinc) > 0 && !isQftTspotTest($loinc) && !isPcrProbeTest($loinc)) {
error_log("TODO isAfbSmear returning TRUE for loinc: ".$loinc);
			return true;
		}
error_log("TODO isAfbSmear returning FALSE for loinc: ".$loinc);
	return false;
	}


	/**
	 * If the passed lab result contains a valid Syphilis titer result, return the number for the
	 * result (i.e. for 1:32 return 32), otherwise return 0 (including for non numeric results such as "not detected")
	 *
	 */
	function newerTiterFromLab($lab_result, $time_old) {
		global $props;
//error_log("TODO Jay Titer collection date: ".strtotime($lab_result->collection_date));
error_log("TODO Jay Titer time old: ".$time_old);
error_log("TODO Jay Titer test type id: ".$lab_result->test_type_id);
error_log("TODO Jay Titer result value: ".$lab_result->result_value);
		if($props['emsa_environment'] == 'SNHD' &&
			isset($lab_result->collection_date) && strlen(trim($lab_result->collection_date)) > 0 &&
			strtotime($lab_result->collection_date) >= $time_old &&
			isset($lab_result->test_type_id) && strlen(trim($lab_result->test_type_id)) > 0 &&
			strpos($props['rpr_titer_test_ids'], $lab_result->test_type_id.",") !== false &&
			isset($lab_result->result_value) && strlen(trim($lab_result->result_value)) > 0) {

			// calculate the titer value for comparison (remove all whitespace from string first)
			$new_titer = preg_replace("/\s+/", "", $lab_result->result_value);
error_log("TODO Jay Titer value: '".$new_titer."' position:".strpos($new_titer, '1:'));
			if(strpos($new_titer, '1:') == 0) {
				$new_titer = substr($new_titer, 2);
error_log("TODO Jay new titer: '".$new_titer."'");
				$titer = intval($new_titer);
				if($titer > 0) {
error_log("TODO Jay Titer returning pos titer : ".$titer);
					return $titer;
				}
			}
		}
error_log("TODO Jay Titer returning 0");
		return 0;
	}

	function calcPatientAge(&$xml) {
		// calc patient age
		$age_years = -1;
		if (isSet($xml->interested_party_attributes->person->birth_date)) {
			$seconds_now = time();
			$seconds_birth = strtotime($xml->interested_party_attributes->person->birth_date);
			$age_seconds = $seconds_now - $seconds_birth;
			$age_years = $age_seconds/(365.25*24*3600);
			$age_years = (int)$age_years;
		}
		return $age_years;
	}

	function hivAttachLabProcessing(&$sxe, &$orig_xml, $test_result, $workflow, $investigator_id, $event_disease_name, $event_queue) {
//error_log(" hivAttachLabProcessing person: ".$test_result);
		// if the lab is positive
		if($test_result == 'POSITIVE') {
			// if the client has a history of LTBI
			$person_identifier = array (
				'first_name' => (string)$orig_xml->interested_party_attributes->person->first_name,
				'last_name' => (string)$orig_xml->interested_party_attributes->person->last_name,
				'birth_date' => (string)$orig_xml->interested_party_attributes->person->birth_date);
error_log(" hivAttachLabProcessing person: ".print_r($person_identifier, true));
			if(historyOfSpecificDisease($person_identifier, array('LTBI'))) {
				addNote("Client has a history of LTBI.", $sxe);
			}

			// if the event is open and assigned to an investigator
//error_log(" hivAttachLabProcessing workflow: ".$workflow." investigator id: ".$investigator_id);
			if($workflow != 'closed' && $workflow != 'approved_by_lhd' && isset($investigator_id) && $investigator_id != null) {
				// add a task to notify the investigator
				$sxe->add_positive_lab_task = 'SNHD-HIV-STD-TB';
			} else {
               	setDestinationQueueWithPriority($sxe, $event_queue, 'DDCS High Priority');
			}
		}

		// if this is NOT an HIV event
		if(strpos($disease_name, 'HIV') === false && strpos($disease_name, 'AIDS') === false) {
			addNote("HIV Lab attached.", $sxe);
		}
	}

	function syphAttachLabProcessing(&$sxe, &$orig_xml, $test_result, $workflow, $investigator_id, $event_disease_name, $event_queue) {
		global $props;
error_log("TODO Jay syphAttach test result: ".$test_result);
		// if the lab is positive
		if($test_result == 'POSITIVE') {

			// if the event is open and assigned to an investigator
error_log("TODO Jay syphAttach work_flow:".$workflow." investigator id: ".$investigator_id);
			if($workflow != 'closed' && $workflow != 'approved_by_lhd' && isset($investigator_id) && $investigator_id != null) {
				// add a task to notify the investigator
				$sxe->add_positive_lab_task = 'SNHD-HIV-STD-TB';
			} else {
				addNote("Positive syphilis lab attached.", $sxe);
				// error_log('[x] DEBUG: Updating to "DDCS Syphilis" from "'.$sxe->destination_queue.'" - ' . __LINE__);
               	setDestinationQueueWithPriority($sxe, $event_queue, 'DDCS Syphilis');
			}
		}

		// if this is a syphilis event
		if(stripos($event_disease_name, 'Syphilis') !== false) {
			// if this is a titer lab
			$test_type_id = isset($sxe->labs_attributes->lab_results->test_type_id) ? trim($sxe->labs_attributes->lab_results->test_type_id) : "";
			$result_value = isset($sxe->labs_attributes->lab_results->result_value) ? trim($sxe->labs_attributes->lab_results->result_value) : "";
error_log("TODO Jay syph attach, test type: ".$test_type_id." value:".$result_value. " types:".$props['rpr_titer_test_ids']);
			if(strlen($test_type_id) > 0 && strlen($result_value) > 0 && strpos($props['rpr_titer_test_ids'], $test_type_id.",") !== false) {
				$last_titer = findLastTiter($orig_xml);
				if(is4xTiterIncrease($test_type_id, $result_value, $last_titer)) {
					addNote('Syphilis titer increase by factor of 4 or higher ', $sxe);
//error_log("TODO Jay DDCS Syphilis -- High Titer");

					// error_log('[x] DEBUG: Checking logic '. $investigator_id. ' and ' . $sxe->destination_queue . ' - ' . __LINE__);

					// Note: Only assign to "DDCS Syphilis" Queue when result is POSITIVE and no one assign to it
					if ($test_result == 'POSITIVE' && empty($investigator_id) ) {
						// error_log('[x] DEBUG: Updating to "DDCS Syphilis" from "'.$sxe->destination_queue.'" - ' . __LINE__);
                        setDestinationQueueWithPriority($sxe, $event_queue, 'DDCS Syphilis');
					}
				}
			}
		}
	}

	/**
	 * If this is a titer lab test and there is a titer increase of 4x or greater (or change from not detected to detected)
	 * return true
	 *
	 */
	function is4xTiterIncrease($test_type_id, $result_value, $last_titer) {
		global $props;
		// if this is a Titer lab and titer increased 4x from last titer, or last titer not found
//error_log("TODO Jay test type: ".$test_type_id." last titer:".$last_titer);
		if(strlen($test_type_id) > 0 && strpos($props['rpr_titer_test_ids'], $test_type_id.",") !== false) {

            // if this titer result does not match the expected format (1:8, 1:32 or similar) then we will treat this as
            // a 4x increase so that a DIIS will look at the bogus lab result
            error_log('[x] is4xTiterIncrease : '.$result_value. ' - ' . __LINE__);
            if(!preg_match("/^1:[0-9]+$/", $result_value)) {
            	error_log('[x] is4xTiterIncrease : return true with bogus lab result = "'.$result_value.'"');
                return true;
            }

			$new_titer = intval(substr(trim($result_value), 2));
error_log("TODO Jay last titer:".$last_titer." new titer:".$new_titer." test type:".$test_type_id);
			if($new_titer > 0 && $new_titer >= (4*$last_titer)) {
error_log("TODO Jay is4xReturning True");
				return true;
			}
		}
error_log("TODO Jay is4xReturning False");
		return false;
	}

	function ngctAttachLabProcessing(&$sxe, $test_result, $workflow, $investigator_id, $event_queue) {
		// if the lab is positive
		if($test_result == 'POSITIVE') {

			// if the event is open and assigned to an investigator
			if($workflow != 'closed' && $workflow != 'approved_by_lhd' && isset($investigator_id) && $investigator_id != null) {
				// add a task to notify the investigator
				$sxe->add_positive_lab_task = 'SNHD-HIV-STD-TB';
			} else {
               	setDestinationQueueWithPriority($sxe, $event_queue, 'DDCS CT/NG');
			}
		}
	}

	function tbAttachLabProcessing(&$sxe, $test_result, $workflow, $investigator_id, $event_queue) {
		// if the lab is positive
		if($test_result == 'POSITIVE') {

			// if the event is open and assigned to an investigator
			if($workflow != 'closed' && $workflow != 'approved_by_lhd' && isset($investigator_id) && $investigator_id != null) {
				// add a task to notify the investigator
				$sxe->add_positive_lab_task = 'SNHD-HIV-STD-TB';
			} else {
               	setDestinationQueueWithPriority($sxe, $event_queue, 'DDCS TB');
			}
		}
	}

	function tbNewAEProcessing(&$sxe, $test_result, &$person_identifier, $event_queue) {
		global $props;
		// NEW TB AE processing
		$sxe->create_assessment_event = true;
		$sxe->disease_events->disease_id = 185; // TB testing is the default disease
		$snhd_ordered_lab = false;
		$loinc = trim($sxe->labs_attributes->lab_results->loinc_code);
		$is_qft = isQftTspotTest($loinc);

		// if the TB group ordered this lab
		$clinician = $sxe->clinician_attributes->clinician->last_name;
		if(stripos($clinician, 'snhd') !== false) {
			addNote("SNHD ordered lab.", $sxe);
			$snhd_ordered_lab = true;
		}
		// if the lab is Negative
		if($test_result == 'NEGATIVE' || ($test_result == 'EQUIVOCAL' && $is_qft && !$snhd_ordered_lab)) {
			// Create and close a TB testing AE
			// close the AE
			setWorkflow($sxe, 'approved_by_lhd');
		} else {
            $age_years = calcPatientAge($sxe);
            if ($age_years < 5) {
               	$sxe->destination_queue = 'DDCS TB Priority';
            } else {
                if($is_qft) {
                    if($snhd_ordered_lab && $test_result == 'EQUIVOCAL') {
                        $sxe->destination_queue = 'DDCS TB';
                    // if there is NO prior TB CMR of any age
                    } else if(!historyOfSpecificDisease($person_identifier, array('Tuber', 'TB'))) {
                        // if the patient has a history of HIV
                        if(historyOfSpecificDisease($person_identifier, array('HIV', 'AIDS'))) {
                            addNote("Client has a history of HIV or co-infection.", $sxe);
                            $sxe->destination_queue = 'DDCS TB Priority';
                        } else {
                            // Event type:  LTBI CMR
                            $sxe->disease_events->disease_id = 129;
                            $sxe->create_assessment_event = false;
                            setWorkflow($sxe, 'approved_by_lhd');
                        }
                    } else {
                        setWorkflow($sxe, 'approved_by_lhd');
                    }
                } else if (isPCRProbeTest($loinc)) {
                    $sxe->destination_queue = 'DDCS TB Priority';
                } else {
                    $sxe->destination_queue = 'DDCS TB';
                }
            }
		}
	}

	function hivNewAEProcessing(&$sxe, $test_result, &$person_identifier) {
		global $props;
		// NEW HIV AE processing
		// if the lab is positive
		if($test_result == 'POSITIVE') {
			$sxe->destination_queue = 'DDCS High Priority';
		} else {
			// set disease to STD testing event
			$sxe->disease_events->disease_id = $props['std_testing_id'];
			// close the AE
			setWorkflow($sxe, 'approved_by_lhd');
		}

		// if a female of child-bearing age
		if(isFemale15To49($sxe)) {
			addNote("Review ordering provider for possible pregnancy.", $sxe);
		}
		// if the patient has a history of LTBI
		if(historyOfSpecificDisease($person_identifier, array('LTBI'))) {
			addNote("Client has a history of LTBI.", $sxe);
		}
	}

	function syphNewAEProcessing(&$sxe, $test_result, &$person_identifier) {
		global $props;

		$lab_result = $sxe->labs_attributes->lab_results;
error_log("TODO Jay syphNewAE test type id: ".$sxe->labs_attributes->lab_results->test_type_id." result_value: ".
    $sxe->labs_attributes->lab_results->result_value. " test result:".$test_result);

		// if the lab is negative
		if($test_result == 'NEGATIVE') {
			// set disease to STD testing event
			$sxe->disease_events->disease_id = $props['std_testing_id'];
			// close the AE
			setWorkflow($sxe, 'approved_by_lhd');
		} else {
			// if there is a pre-existing Syphilis CMR for this person
			if(historyOfSpecificDisease($person_identifier, array('Syphilis'))) {
				// if this is a titer lab
				if(isset($sxe->labs_attributes->lab_results->test_type_id) &&
						isset($sxe->labs_attributes->lab_results->result_value)) {
					$test_type_id = trim($sxe->labs_attributes->lab_results->test_type_id);
					$result_value = trim($sxe->labs_attributes->lab_results->result_value);
error_log("TODO Jay syphNewAE test type id: ".$test_type_id." result_value: ".$result_value);

					if(strlen($test_type_id) > 0 && strpos($props['rpr_titer_test_ids'], $test_type_id.",") !== false) {

						$last_titer = findLastTiter($sxe);

						// if the current titer shows a 4x increase
						if(is4xTiterIncrease($test_type_id, $result_value, $last_titer)) {
							addNote('Syphilis titer increase by factor of 4 or more ', $sxe);
						} else {
							// set disease to STD testing event
							$sxe->disease_events->disease_id = $props['std_testing_id'];
							// close the AE
							setWorkflow($sxe, 'approved_by_lhd');

							// we are done
							return;
						}
					}
				}
			}
			// echo '<pre>' . var_export($sxe, true) . '</pre>';
			// error_log('[x] DEBUG: Updating to "DDCS Syphilis" from "'.$sxe->destination_queue.'" - ' . __LINE__);
			$sxe->destination_queue = 'DDCS Syphilis';

			//if the patient has a history of HIV
			if($sxe->history_of_hiv) {
				addNote("Client has a history of HIV or co-infection.", $sxe);
				$sxe->destination_queue = 'DDCS High Priority';
			}
		}
	}

	function ngctNewEventProcessing(&$sxe) {
		global $props;

		// if we are creating an AE
		if($sxe->create_assessment_event) {
			// set disease to STD testing event
			$sxe->disease_events->disease_id = $props['std_testing_id'];
		} else {
			// creating a CMR
			$reporting_agency = '';
			if(isSet($sxe->reporting_agency_attributes->place->name)) {
				$reporting_agency = $sxe->reporting_agency_attributes->place->name;
			}
			// if the STD group ordered this lab
			$clinician = $sxe->clinician_attributes->clinician->last_name;
			if(stripos($clinician, 'snhd') !== false) {
				$sxe->destination_queue = 'DDCS CT/NG';
				addNote("SNHD ordered lab.", $sxe);
			}

			// calc patient age
			$age_years = calcPatientAge($sxe);

			// if the patient last name, first name or date of birth is missing,
			// or the patient is <= 13 years, or if lab is from SNPHL but not RHSHC
			// assign it to the morbidity review queue
			if(isSnphlNotShc($reporting_agency) || $age_years < 14 || !isSet($sxe->interested_party_attributes->person->first_name) ||
				!isSet($sxe->interested_party_attributes->person->last_name)) {
				$sxe->destination_queue = 'DDCS CT/NG';
			}

			// if the patient has a history of HIV
			if($sxe->history_of_hiv) {
				addNote("Client has a history of HIV or co-infection: ".$result_events[$person_id][$event_id]['record_number'], $sxe);
				$sxe->destination_queue = 'DDCS High Priority';
			}
		}
		// if the new event will not be assigned to a queue
		if(!isset($sxe->destination_queue)) {
			// close NG / CT Events unless they have been assigned to a queue
			// by setting the case status to Approved by Local health district
			setWorkflow($sxe, 'approved_by_lhd');
		}
	}

	/*
	 *  Return true if the passed patient has a morbidity CMR for any disease whose name contains one of the disease_keys
	*/
	function historyOfSpecificDisease(&$person_identifier, $disease_keys) {

error_log("historyOfSpecificDisease person: ".print_r($person_identifier, true)." search keys: ".print_r($disease_keys, true));

		if(!isSet($person_identifier) || !isSet($person_identifier['first_name']) || !isSet($person_identifier['last_name'])) {
			return false;
		}

		try {
			$event_results = getPeopleSearchResults($person_identifier['first_name'], $person_identifier['last_name'],
				$person_identifier['birth_date'], NULL, 100);
		} catch (Exception $e) {
			error_log("historyOfSpecificDisease exception: ".$e);
			throw new Exception('Jay unknown: '.$e);
		} catch (SoapFault $f) {
			error_log("historyOfSpecificDisease soap fault: ".$f);
			throw new Exception('Jay unknown f: '.$f);
		}

//error_log("historyOfSpecificDisease events found: ".print_r($event_results, true));

		if (count($event_results['five_star']) > 0) {
			$events_by_person_id = $event_results['events_by_person_id'];
			$results = $event_results['results'];
			foreach ($results as $result_key => $result_item) {
				foreach ($events_by_person_id[intval($result_key)] as $key => $event) {
error_log("historyOfSpecificDisease event: ".print_r($event, true));
//error_log("historyOfSpecificDisease event_type: ".print_r($event['event_type'], true));
					if($event['event_type'] == 'Morbidity Event') {
						foreach ($disease_keys as $disease_key) {
							if(strpos($event[disease_name], $disease_key) !== false) {
error_log("historyOfSpecificDisease found : ".$disease_key." ".$event[disease_name]);
							return true;
							}
						}
					}
				}
			}
		}
error_log("historyOfSpecificDisease Not Found: ");
		return false;
	}

	/*
	 *  Look through prior Syphilis events, for the most recent titer result from a CMR or AE
	 *  return 0 if we find no syph Events or no titer results, or only not detected types of results
	 *  return the most recent titer result otherwise
	*/
	function findLastTiter(&$sxe) {
		global $props;

		$last_titer = 0;
error_log("TODO Jay findLastTiter person: ".$sxe->interested_party_attributes->person->first_name." "
				.$sxe->interested_party_attributes->person->last_name." "
				.$sxe->interested_party_attributes->person->birth_date);
		$event_results = getPeopleSearchResults($sxe->interested_party_attributes->person->first_name,
				$sxe->interested_party_attributes->person->last_name,
				$sxe->interested_party_attributes->person->birth_date, "", 100);
		if (count($event_results['five_star']) > 0) {
			$events_by_person_id = $event_results['events_by_person_id'];
			$results = $event_results['results'];
			$last_titer_date = -1;
			foreach ($results as $result_key => $result_item) {
				foreach ($events_by_person_id[intval($result_key)] as $key => $event) {
//error_log("TODO Jay findLastTiter event: ".$event['event_type']." ".$event['disease_name']." ".$event['event_id']);
					if(($event['event_type'] == 'Morbidity Event' || $event['event_type'] == 'Assessment Event') &&
						stripos($event['disease_name'], 'Syphilis') !== false) {

						$event_id = $event['event_id'];
error_log("TODO Jay findLastTiter syph CMR/AE found id:".$event_id);
						$event_labs_qry = <<<EOEX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health>
		<events>
			<id>$event_id</id>
		</events>
	</trisano_health>
</health_message>
EOEX;

						// init SOAP client
						try {
							$soap_client = @new SoapClient($props['sqla_wsdl_url']);
						} catch (Exception $e) {
						} catch (SoapFault $f) {
						}
						if ($soap_client) {
							try {
								$event_labs_result = $soap_client->findEvent(array('healthMessage' => $event_labs_qry));
							} catch (Exception $e) {
							} catch (SoapFault $f) {
							}
							$event_labs_sxe = simplexml_load_string($event_labs_result->return);
//error_log("TODO Jay findLastTiter labs sxe: ".$event_labs_sxe->asXML());

							foreach($event_labs_sxe->trisano_health->labs_attributes as $labs_attribute) {
//error_log("TODO Jay findLastTiter labs attribute: ".$labs_attribute->asXML());
								foreach ($labs_attribute->lab_results as $lab_result) {
//error_log("TODO Jay findLastTiter lab result: ".$lab_result->asXML());
									$titer = newerTiterFromLab($lab_result, $last_titer_date);
                                    $new_titer_date = strtotime($lab_result->collection_date);
//error_log("TODO Jay findLastTiter titer: ".$titer." collection date: ".$lab_result->collection_date);
                                    // We might get two results from the same lab (a 0 and some other value like 1:8)
                                    // we only overwrite with the new titer if the value is > 0 in this case
                                    if($last_titer_date == $new_titer_date) {
                                        if($titer > 0) {
                                            $last_titer = $titer;
                                        }
                                    } else {
										$last_titer_date = $new_titer_date;
										$last_titer = $titer;
error_log("TODO Jay setting last titer: ".$last_titer." collection date: ".$last_titer_date);
									}
								}
							}
						} else {
//error_log("TODO Jay findLastTiter soap_client failed");
						}
						unset($soap_client);
					}
				}
			}
		}
		return $last_titer;
	}

    // set the destination queue only if the new queue is a higher priority queue
	function setDestinationQueueWithPriority(&$sxe, $queue_old, $queue_new) {
        $priority = array(
            "DDCS CT/NG" => 1,
            "DDCS Syphilis" => 2,
            "DDCS High Priority" => 3,
            "DDCS TB" => 3,
            "DDCS TB Priority" => 4
        );
error_log("TODO JAY >>>>> queue old/new: ".$queue_old."/".$queue_new);
        $priority_old = $priority[$queue_old];
        $priority_new = $priority[$queue_new];
        if ($priority_old == NULL || ($priority_new != NULL && ($priority_new > $priority_old))) {
            $sxe->destination_queue = $queue_new;
        }
    }

	// look in the system message table and return true if this lab is a duplicate of a lab already received.
	function isDuplicateLab($id, &$master_sxe, $fCheckLocalResult) {
		global $host_pa, $my_db_schema, $props;
//error_log("************ isDuplicateLab");
//error_log("*** id: ".$id." master: ".$master_sxe->asXML());
		if (isset($master_sxe)) {

			// we count a lab as a match IFF an existing lab in the system meets these criteria
			// Existing lab was received after test lab collection date (or 7 days ago if no collection date given)
			// first lab received is never counted as duplicate
			// Patient first name, last name, dob match
			// Accession number match
			// collection dates match
			// loinc code match
			// result value match
			// lab status match (final vs prelim etc...)
			$start_date = date("Y-m-d", time()-7*24*3600);
			$collection_date_sql = "<collection_date/>";
			if(isset($master_sxe->labs->collection_date)) {
				$start_date = $master_sxe->labs->collection_date;
				$collection_date_sql = "<collection_date>".$master_sxe->labs->collection_date;
			}
			$test_status_sql = "<test_status/>";
			if(isset($master_sxe->labs->test_status) && trim($master_sxe->labs->test_status) != "") {
				$test_status_sql = "<test_status>".$master_sxe->labs->test_status;
			}
			$result_sql = "%' AND master_xml LIKE '%<result_value>".$master_sxe->labs->result_value;
			if($fCheckLocalResult) {
				$result_sql = "%' AND master_xml LIKE '%<local_result_value>".$master_sxe->labs->local_result_value;
			}

            $sql = "SELECT id FROM ".$my_db_schema."system_messages WHERE  created_at > '".$start_date."' AND id < ".$id.
				" AND loinc_code = '".$master_sxe->labs->loinc_code."' AND lower(lname) = '".strtolower($master_sxe->person->last_name).
				"' AND lower(fname) = '".strtolower($master_sxe->person->first_name).
				"' AND master_xml LIKE '%<date_of_birth>".substr($master_sxe->person->date_of_birth,0,10).
				"%' AND master_xml LIKE '%".$collection_date_sql.$result_sql.
				"%' AND master_xml LIKE '%".$test_status_sql."%'";

//error_log("isDuplicate sql " . $sql);
            $rs = @pg_query($host_pa, $sql);
            if ($rs === false || @pg_num_rows($rs) < 1) {
//error_log("isDuplicate returning false ");
				return false;
			}
//error_log("isDuplicate returning true ");
			return true;
		}
		return false;
	}

	// return true if the originating HL7 for this message contains the text "coroner"
	function isCoronerInvolved($system_message_id) {
		global $host_pa, $my_db_schema;
		$sql = "SELECT som.message from ".$my_db_schema."system_original_messages som ".
			"INNER JOIN ".$my_db_schema."system_messages sm ON sm.id = ".$system_message_id." AND sm.original_message_id = som.id";
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false && @pg_num_rows($rs) > 0) {
			$hl7 = trim(@pg_fetch_result($rs, 0, 'message'));
			if(preg_match("/coroner/i", $hl7) || preg_match("/CCCOME/i", $hl7)) {
//error_log("isCoroner returning true ");
				return true;
			}
		}
//error_log("isCoroner returning false ");
		return false;
	}

?>
