<?php

	//define stuff here for this config
	/* Environment */
	define('ELR_ENV_TEST', 1);
	define('ELR_ENV_DEV', 3);
	define('ELR_ENV_PROD', 2);
	
	/* Case Management Rules */
	define('ALLOW_CMR_YES', 1);
	define('ALLOW_CMR_NO_NEG', 0);
	define('ALLOW_CMR_NO_INFORM', -1);
	define('ALLOW_CMR_NO_QUEST_ID_AS_NAME', -2);
	define('ALLOW_CMR_ERR_XML_REQ_FIELDS', -99);
	define('ALLOW_CMR_ERR_NO_RULES_DEFINED', -98);
	define('ALLOW_CMR_ERR_NO_RULES_MATCHED', -97);
	define('ALLOW_CMR_ERR_MULT_RULES_MATCHED', -96);
	
	/* Specimen Source Validity */
	define('SPECIMEN_VALID', 1);
	define('SPECIMEN_INVALID', 2);
	define('SPECIMEN_EXCEPTION', 3);
	
	/* Surveillance Event Detection */
	define('IS_SURVEILLANCE_YES', 1);
	define('IS_SURVEILLANCE_NO', 0);
	define('IS_SURVEILLANCE_ERR_NO_RULES_DEFINED', -99);
	define('IS_SURVEILLANCE_ERR_NO_RULES_MATCHED', -98);
	define('IS_SURVEILLANCE_ERR_MULT_RULES_MATCHED', -97);
	define('IS_SURVEILLANCE_ERR_MULT_ORGS_MATCHED', -96);
	define('IS_SURVEILLANCE_ERR_REQFIELD_LOINC', -95);
	define('IS_SURVEILLANCE_ERR_REQFIELD_ORGNAME', -94);
	define('IS_SURVEILLANCE_ERR_REQFIELD_TESTRESULT', -93);
	
	define("SHOW_ERRORS", 1);
	define("DEFAULT_ROWS_PER_PAGE", 50);

	define("WHITE_STATUS", 1);
	define("ENTRY_STATUS", 17);
	define("ASSIGNED_STATUS", 14);
	define("GRAY_STATUS", 2);
	define("BLACK_STATUS", 4);
	define("EXCEPTIONS_STATUS", 3);
	define("PENDING_STATUS", 12);
	define("QA_STATUS", 19);
	define("QA_NAME", "QA");
	define("HOLD_STATUS", 99);
	define("PENDING_NAME", "Pending");
	define("ENTRY_NAME", "Entry");
	define("ASSIGNED_NAME", "Assigned");
	define("GRAY_NAME", "Gray ");
	define("BLACK_NAME", "Black");
	define("EXCEPTIONS_NAME", "Exceptions");
	define("HOLD_NAME", "Hold");

	include_once 'session.php';
	
	if ((PHP_SAPI == 'cli') || !isset($_SERVER['ELRAPP_ENVIRONMENT'])) {
		// for scripts run from the command line, check hostname (Apache environment vars not available from CLI)
		if (php_uname('n') == 'hldclxediapptest1') {
			$server_environment = ELR_ENV_TEST;
		} elseif (php_uname('n') == 'hllnedssdev') {
			$server_environment = ELR_ENV_DEV;
		} elseif (php_uname('n') == 'hllelrapp') {
			$server_environment = ELR_ENV_PROD;
		} else {
			die('elrapp Unable to configure server environment.  Please see a system administrator.');
		}
	} elseif (isset($_SERVER['ELRAPP_ENVIRONMENT']) && ($_SERVER['ELRAPP_ENVIRONMENT'] == 'test')) {
		$server_environment = ELR_ENV_TEST;
	} elseif (isset($_SERVER['ELRAPP_ENVIRONMENT']) && ($_SERVER['ELRAPP_ENVIRONMENT'] == 'dev')) {
		$server_environment = ELR_ENV_DEV;
	} elseif (isset($_SERVER['ELRAPP_ENVIRONMENT']) && ($_SERVER['ELRAPP_ENVIRONMENT'] == 'prod')) {
		$server_environment = ELR_ENV_PROD;
	} else {
		die('Unable to configure server environment for elrapp.  Please see a system administrator.');
	}
	
	include_once 'config.php';
	
	include_once 'errors.php'; // sets error reporting level
	include_once 'db_connect.php'; // creates PostgreSQL connection resource $host_pa
	include_once 'master_functions.php';
	include_once 'security.php';

?>