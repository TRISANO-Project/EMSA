<?php

	/**
	 * Error display widget
	 *
	 * @param mixed $msg Error message to display.
	 * @param int $use_pg_error Indicates whether to display latest postgres error.  1 for TRUE, -1 for FALSE.
	 * @param int $fatal Indicates whether the script execution should be halted or allowed to continue.  1 for TRUE, -1 for FALSE.
	 */
	function suicide($msg, $use_pg_error = -1, $fatal = -1) {
debug_print_backtrace(); // TODO Jay
		if ($fatal === 1) {
			if ($use_pg_error === 1) {
				die('<div class="import_widget ui-widget import_error ui-state-error ui-corner-all" style="padding: 5px;"><span class="ui-icon ui-icon-elrerror" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;"><strong>'.$msg.'</strong><br><br><em>Database Error:<br>'.pg_last_error().'</em></p></div>');
			} else {
				die('<div class="import_widget ui-widget import_error ui-state-error ui-corner-all" style="padding: 5px;"><span class="ui-icon ui-icon-elrerror" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;"><strong>'.$msg.'</strong></p></div>');
			}
		} else {
			if ($use_pg_error === 1) {
				echo '<div class="import_widget ui-widget import_error ui-state-error ui-corner-all" style="padding: 5px;"><span class="ui-icon ui-icon-elrerror" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;"><strong>'.$msg.'</strong><br><br><em>Database Error:<br>'.pg_last_error().'</em></p></div>';
			} else {
				echo '<div class="import_widget ui-widget import_error ui-state-error ui-corner-all" style="padding: 5px;"><span class="ui-icon ui-icon-elrerror" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;"><strong>'.$msg.'</strong></p></div>';
			}
		}
	}
	
	
	
	
	/**
	 * Highlighted message/info display widget
	 *
	 * @param mixed $msg Message to highlight.
	 * @param string $icon Class name of the JQueryUI icon to display.  Default "ui-icon-info".
	 */
	function highlight($msg, $icon = 'ui-icon-info') {
		echo '<div class="import_widget ui-widget import_error ui-state-highlight ui-corner-all" style="padding: 5px;"><span class="ui-icon '.$icon.'" style="float: left; margin-right: .3em;"></span><p style="margin-left: 20px;">'.$msg.'</p></div>';
	}
	
	
	
	
	function getResults($db_results, $type) {
		$i = 0;
		$results = array();
		
		while ($res = get_db_row_array($db_results)) {
			$created_at = date("m/d/Y (g:ia)", strtotime($res['created_at']));
			$created_at_long = date(DATE_W3C, strtotime($res['created_at']));
			
			$assigned_at = (($res['assigned_date']) ? date("m/d/Y (g:ia)", strtotime($res['assigned_date'])) : null);
			$assigned_at_long = date(DATE_W3C, strtotime($res['assigned_date']));
			
			$xmlparse = @simplexml_load_string($res['master_xml']);
			
			$result = array();
			
			if (count($xmlparse) > 0) {
				$result['fname'] = $xmlparse->person->first_name;
				$result['lname'] = $xmlparse->person->last_name;
				$result['mname'] = $xmlparse->person->middle_name;
				
				if ($type != BLACK_STATUS) {
					$result['bday'] = ((strlen($xmlparse->person->date_of_birth) > 5) ? date("Y-m-d", strtotime($xmlparse->person->date_of_birth)) : '');
					$result['birth_date'] = ((strlen($xmlparse->person->date_of_birth) > 5) ? date("m/d/Y", strtotime($xmlparse->person->date_of_birth)) : '--');
				} else {
					$result['bday'] = '';
				}
				
				$result['phone'] = ((strlen($xmlparse->person->phone) > 0) ? $xmlparse->person->area_code."-".substr($xmlparse->person->phone, 0, 3)."-".substr($xmlparse->person->phone, 3) : "");
				$result['street'] = $xmlparse->person->street_name;
				$result['unit'] = $xmlparse->person->unit;
				$result['city'] = $xmlparse->person->city;
				$result['state'] = $xmlparse->person->state;
				$result['zip'] = $xmlparse->person->zip;
				$result['county'] = $xmlparse->person->county;
				$result['full_name'] = ((strlen($xmlparse->person->last_name) > 0) ? $xmlparse->person->last_name : '--').', '.((strlen($xmlparse->person->first_name) > 0) ? $xmlparse->person->first_name : '--').((isset($xmlparse->person->middle_name) && !empty($xmlparse->person->middle_name)) ? ' '.$xmlparse->person->middle_name : '');
				$result['sex'] = ((strlen($xmlparse->person->gender) > 0) ? getMasterConceptFromChildCode($xmlparse->person->gender, 'gender', $res['lab_id']) : '--');
				$result['jurisdiction'] = $xmlparse->administrative->jurisdiction_of_residence;
				$result['created_at'] = $created_at;
				$result['created_at_long'] = $created_at_long;
				$result['assigned_at'] = $assigned_at;
				$result['assigned_at_long'] = $assigned_at_long;
				$result['disease'] = $xmlparse->disease->name;  //diseaseByLoinc($res['loinc_code']);
				$result['loinc_code'] = $res['loinc_code'];
				$result['units'] = $xmlparse->labs->units;
				$result['result_value'] = $xmlparse->labs->result_value;
				$result['test_status']= getMasterConceptFromChildCode($xmlparse->labs->test_status, 'test_status', $res['lab_id']);
				$result['test_type'] = $xmlparse->labs->test_type;
				$result['collection_date'] = date("m/d/Y", strtotime($xmlparse->labs->collection_date));
				$result['lab_test_date'] = date("m/d/Y", strtotime($xmlparse->labs->lab_test_date));
				$result['errors'] = $xmlparse->sourceid->exception_status;
				
				if ($xmlparse->person->zip) {
					$result['zip_code'] = $xmlparse->person->zip;
				} elseif ($xmlparse->diagnostic->zipcode) {
					$result['zip_code'] = $xmlparse->diagnostic->zipcode;
				} else {
					$result['zip_code'] = "";
				}
				
				$result['type'] = $res['final_status'];
				$result['id'] = $res['id'];
				$result['lab'] = $xmlparse->labs->lab;
			}
			
			$results[$i] = $result;
			$i++;
		}
		
		return $results;
	}
	
	
	
	
	/**
	 * Returns list of gateway crossreference diseases for the specified Master Condition.
	 *
	 * Accepts a string containing a Master Condition by name and returns an array of TriSano
	 * disease IDs for consumption in Whitelist Rule logic.  The disease corresponding to the
	 * master condition is excluded from the list of returned IDs (i.e. a disease cannot be 
	 * xref'd to itself)
	 * 
	 * @param string $master_condition Disease Master Concept name
	 * @param int $disease_id_master -- the disease corresponding to the master condition
	 * @return array|false
	 */
	function gatewayCrossrefsByName($master_condition, $disease_id_master) {
		global $host_pa, $my_db_schema, $props;
		$return_arr = array();
		
		// get crossrefs from database
		$sql = sprintf("SELECT mc.gateway_xref AS gateway_xref FROM %svocab_master_condition mc
				JOIN %svocab_master_vocab mv ON (mv.id = mc.condition AND mv.category = elr.vocab_category_id('condition') AND mv.concept = '%s');",
				$my_db_schema, $my_db_schema, pg_escape_string(trim($master_condition)));
		$xref_raw = @pg_fetch_result(@pg_query($host_pa, $sql), 0, "gateway_xref");
		if ($xref_raw === false) {
			return false;
		}
		$xref_names_arr = explode(";", gatewayCrossrefIdValues($xref_raw));
		
		// prepare XML for query agent
		$base_xml_string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><health_message><system>TRISANO</system><trisano_health/></health_message>";
		$base_sxe = new SimpleXMLElement($base_xml_string);
		
		foreach ($xref_names_arr as $xref_name) {
			if (strlen(trim($xref_name)) > 0) {
				$disease = $base_sxe->trisano_health->addChild("diseases");
				$disease->addChild("disease_name", trim($xref_name));
			}
		}
		
		$search_xml = $base_sxe->asXML();
		
		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e->getMessage(), -1, 1);
		} catch (SoapFault $f) {
			suicide($f->getMessage(), -1, 1);
		}
		
		if ($client) {
			try {
				$result = $client->findId(array("healthMessage" => $search_xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$return = simplexml_load_string($result->return);
			
			foreach ($return->trisano_health->diseases as $return_disease) {
				$disease_id = intval($return_disease->id);
				if($disease_id != $disease_id_master) {
					$return_arr[] = $disease_id;
				}
			}
		}
		return $return_arr;
	}
	
	
	
	
	/**
	 * Returns list of gateway crossreference diseases for the specified Master Condition.
	 *
	 * Accepts a string containing a Master Condition by name and returns an array of TriSano
	 * disease names for use by People Search
	 * 
	 * @param string $master_condition Disease Master Concept name
	 * @return array|false
	 */
	function gatewayCrossrefNamesByName($master_condition) {
		global $host_pa, $my_db_schema;
		$return_arr = array();
		
		// get crossrefs from database
		$sql = sprintf("SELECT mc.gateway_xref AS gateway_xref FROM %svocab_master_condition mc
				JOIN %svocab_master_vocab mv ON (mv.id = mc.condition AND mv.category = elr.vocab_category_id('condition') AND mv.concept = '%s');",
				$my_db_schema, $my_db_schema, pg_escape_string(trim($master_condition)));
		$xref_raw = @pg_fetch_result(@pg_query($host_pa, $sql), 0, "gateway_xref");
		if ($xref_raw === false) {
			return false;
		}
		$xref_ids = gatewayCrossrefIdValues($xref_raw);
		$xref_names_arr = (($xref_ids !== false) ? explode(";", $xref_ids) : array());
		
		return $xref_names_arr;
	}
	
	
	
	
	/**
	 * Converts a list of gateway crossref condition master vocabulary IDs into a list of verbose condition names.
	 *
	 * Accepts a string containing a semicolon-delimited list of master vocabulary IDs and converts them
	 * to a semicolon-delimited string of verbose condition names for display in UI or use with Whitelist Rules.
	 * 
	 * @param string $gateway_id_list List of master vocabulary IDs, separated by semicolons
	 * @return string|false
	 */
	function gatewayCrossrefIdValues($gateway_id_list) {
		global $host_pa, $my_db_schema;
		
		if (strlen(trim($gateway_id_list)) < 1) {
			return false;
		}
		
		$xref_ids = explode(";", $gateway_id_list);
		if (count($xref_ids) < 1) {
			return false;
		}
		
		foreach ($xref_ids as $xref_id) {
			$sql = sprintf("SELECT concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('gateway_xref') AND id = %d", $my_db_schema, intval(trim($xref_id)));
			$text = @pg_fetch_result(@pg_query($host_pa, $sql), 0, "concept");
			if (strlen(trim($text)) > 0) {
				$xref_vals[] = trim($text);
			} else {
				return false;
			}
		}
		
		$xref_vals_string = implode(";", $xref_vals);
		return $xref_vals_string;
	}
	
	
	
	
	/**
	 * Return the user's full name from TriSano based on the user's UMDID
	 *
	 * @param string $u_id User's UMDID
	 * @return string|false
	 */
	function userRealNameByUmdid($u_id = null) {
		global $props;
		if (!is_null($u_id)) {
			if (filter_var(trim($u_id), FILTER_SANITIZE_STRING) == "9999") {
				return "<em>ELR Service</em>";
			}
			
			try {
				$u_soap = @new SoapClient($props['sqla_wsdl_url']);
			} catch (SoapFault $f) {
				return $f;
			} catch (Exception $e) {
				return $e;
			}
			
			$u_qry = <<<EOQ
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <system>TRISANO</system>
    <trisano_health>
        <users>
            <uid>$u_id</uid>
        </users>
    </trisano_health>
</health_message>
EOQ;
			
			if ($u_soap) {
				$u_result = $u_soap->getUserRoles(array("healthMessage" => $u_qry));
				$u_xml = simplexml_load_string($u_result->return);
				if (trim($u_xml->statusMessage) == "User Not Found.") {
					return "<em title=\"".$u_id."\">Unknown User</em>";
				} else {
					return htmlentities(trim($u_xml->trisano_health->users[1]->firstName) . " " . trim($u_xml->trisano_health->users[1]->lastName), ENT_QUOTES, "UTF-8");
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	
	
	/**
	 * Get back the TriSano Event Type (ContactEvent or AssessmentEvent or MorbidityEvent) for a given Event ID Number
	 * If event not found, returns FALSE.
	 *
	 * @param int $event_id TriSano Event ID
	 * @return string|bool
	 */
	function getTrisanoEventTypeById($event_id = null) {
		global $props;
		if (!is_null($event_id)) {
			$id = intval(trim($event_id));
			try {
				$soap = @new SoapClient($props['sqla_wsdl_url']);
			} catch (Exception $e) {
				return false;
			} catch (SoapFault $f) {
				return false;
			}
			
			$qry = <<<EOQ
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <system>TRISANO</system>
    <trisano_health>
        <events>
            <id>$id</id>
        </events>
    </trisano_health>
</health_message>
EOQ;

			if ($soap) {
				try {
					$result = $soap->findEvent(array("healthMessage" => $qry));
				} catch (Exception $e) {
					return false;
				} catch (SoapFault $f) {
					return false;
				}
				
				$xml = simplexml_load_string($result->return);
				$event_types = $xml->xpath("trisano_health/events/type");
				
				foreach ($event_types as $event_type_obj => $event_type_val) {
					$event_type = trim($event_type_val);
				}
				
				unset($xml);
				unset($result);
				unset($soap);
				
				return $event_type;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	
	
	/**
	 * Return a usable boolean operator for a given operator ID
	 * Returns false if invalid ID is passed or no ID specified
	 *
	 * @param int $operator_id Operator ID
	 * @return string|bool
	 */
	function operatorById($operator_id = null) {
		$o_id = filter_var($operator_id, FILTER_VALIDATE_INT);
		if (!$o_id) {
			return false;
		}
		
		switch ($o_id) {
			case 1:
				return "==";
				break;
			case 2:
				return "!=";
				break;
			case 3:
				return ">";
				break;
			case 4:
				return "<";
				break;
			case 5:
				return ">=";
				break;
			case 6:
				return "<=";
				break;
			default:
				return false;
				break;
		}
	}
	
	
	
	
	/**
	 * Return an operator ID for a given boolean operator symbol
	 * Returns false if invalid operator is passed or no operator specified
	 *
	 * @param string $operator Operator Symbol
	 * @return int|bool
	 */
	function operatorBySymbol($operator = null) {
		if (is_null($operator)) {
			return false;
		}
		
		$o = trim($operator);
		
		switch ($o) {
			case "=":
			case "==":
				return 1;
				break;
			case "!=":
			case "<>":
				return 2;
				break;
			case ">":
				return 3;
				break;
			case "<":
				return 4;
				break;
			case ">=":
				return 5;
				break;
			case "<=":
				return 6;
				break;
			default:
				return false;
				break;
		}
	}
	
	
	
	
	/**
	 * Return a graphical operator symbol for a given operator ID
	 * Returns false if invalid ID is passed or no ID specified
	 *
	 * @param int $operator_id Operator ID
	 * @param resource $host_pa Database connection resource
	 * @param string $my_db_schema Database schema
	 * @return string|bool
	 */
	function graphicalOperatorById($operator_id = null, $host_pa, $my_db_schema) {
		$o_id = filter_var($operator_id, FILTER_VALIDATE_INT);
		if (!$o_id) {
			return false;
		}
		
		$qry = "SELECT graphical FROM ".$my_db_schema."structure_operator WHERE id = ".intval($o_id).";";
		$rs = @pg_query($host_pa, $qry);
		
		if ($rs) {
			return @pg_fetch_result($rs, 0, "graphical");
		} else {
			return false;
		}
	}
	
	
	
	
	/**
	 * Returns the Local Health Department name for a given Jurisdiction ID.  Returns FALSE if no matching role found.
	 *
	 * @param int $jurisdiction_id Jurisdiction ID
	 * @return string|bool
	 */
	function lhdName($jurisdiction_id = 0) {
		global $host_pa, $my_db_schema;
		
		$j_id = ((filter_var($jurisdiction_id, FILTER_VALIDATE_INT) && (intval($jurisdiction_id) > 0)) ? intval(filter_var($jurisdiction_id, FILTER_SANITIZE_NUMBER_INT)) : null);
		if (!is_null($j_id)) {
			$qry = "SELECT health_district FROM ".$my_db_schema."system_districts WHERE system_external_id = ".$j_id.";";
			$rs = @pg_query($host_pa, $qry);
			if ($rs) {
				return trim(@pg_fetch_result($rs, 0, "health_district"));
			} else {
				return false;
			}
			@pg_free_result($rs);
		} else {
			return false;
		}
	}
	
	
	
	
	/**
	 * Returns the "Jurisdiction" name for a given custom Jurisdiction ID (used by E-mail Notification).  Returns FALSE if no matching role found.
	 *
	 * @param int $jurisdiction_id Custom Jurisdiction ID
	 * @return string|bool
	 */
	function customLhdName($jurisdiction_id = 0) {
		global $host_pa, $my_db_schema;
		
		$j_id = ((filter_var($jurisdiction_id, FILTER_VALIDATE_INT) && (intval($jurisdiction_id) > 0)) ? intval(filter_var($jurisdiction_id, FILTER_SANITIZE_NUMBER_INT)) : null);
		if (!is_null($j_id)) {
			$qry = 'SELECT name FROM '.$my_db_schema.'batch_notification_custom_jurisdictions WHERE id = '.$j_id.';';
			$rs = @pg_query($host_pa, $qry);
			if ($rs) {
				return trim(@pg_fetch_result($rs, 0, 'name'));
			} else {
				return false;
			}
			@pg_free_result($rs);
		} else {
			return false;
		}
	}
	
	
	
	
	/**
	 * Returns the Master Disease Name for a given Master LOINC.  Returns FALSE if no matching condition found.
	 *
	 * @param string $loinc Master LOINC Code
	 * @return string|bool
	 */
	function diseaseByLoinc($loinc = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($loinc)) {
			return false;
		}
		
		$sql = "SELECT mv.concept AS concept FROM ".$my_db_schema."vocab_master_vocab mv 
			INNER JOIN ".$my_db_schema."vocab_master_condition mc ON (mv.id = mc.condition)
			INNER JOIN ".$my_db_schema."vocab_master_loinc ml ON (ml.trisano_condition = mc.c_id AND ml.loinc ILIKE '".pg_escape_string(trim($loinc))."') LIMIT 1;";
			//echo $sql;
		$rs = @pg_query($host_pa, $sql);
		if ($rs) {
			$condition = @pg_fetch_result($rs, 0, "concept");
			if (isset($condition) && !is_null($condition) && (strlen(trim($condition)) > 0)) {
				return trim($condition);
			} else {
				return false;
			}
		} else {
			return false;
		}
		
		@pg_free_result($rs);
	}
	
	
	
	
	/**
	 * Accepts an encoded value for Abnormal Flag from the Master XML & returns the decoded Preferred Concept Name
	 *
	 * @param string $abnormal_code Coded Abnormal Flag value
	 * @return string
	 */
	function decodeAbnormalFlag($abnormal_code = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($abnormal_code)) {
			return '';
		} else {
			$flag = trim($abnormal_code);
			if (empty($flag)) {
				return '';
			} else {
				$qry = 'SELECT m.concept AS concept FROM '.$my_db_schema.'vocab_master_vocab m
					INNER JOIN '.$my_db_schema.'vocab_child_vocab c ON (
						c.master_id = m.id AND 
						m.category = elr.vocab_category_id(\'abnormal_flag\') AND 
						c.concept ILIKE \''.pg_escape_string($flag).'\'
					);';
				$rs = @pg_query($host_pa, $qry);
				if (($rs === false) || (@pg_num_rows($rs) < 1)) {
					return '';
				} else {
					$decoded_value = trim(@pg_fetch_result($rs, 0, 'concept'));
					@pg_free_result($rs);
					return $decoded_value;
				}
			}
		}
	}
	
	
	
	
	/**
	 * Returns the Master Preferred Concept value for a given coded Child vocabulary value
	 *
	 * @param string $child_code Coded Child vocabulary value
	 * @param string $category Vocabulary Category
	 * @param int $lab_id Child Lab ID
	 * @return string
	 */
	function getMasterConceptFromChildCode($child_code = null, $category = null, $lab_id = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($child_code) || is_null($category) || is_null($lab_id) || empty($child_code) || empty($category) || empty($lab_id)) {
			return '';
		}
		
		$qry = 'SELECT m.concept AS concept FROM '.$my_db_schema.'vocab_master_vocab m
			INNER JOIN '.$my_db_schema.'structure_category sc ON (
				sc.id = m.category AND 
				sc.label ILIKE \''.pg_escape_string(trim($category)).'\'
			)
			INNER JOIN '.$my_db_schema.'vocab_child_vocab c ON (
				c.master_id = m.id AND 
				c.concept ILIKE \''.pg_escape_string(trim($child_code)).'\'
			);';
		$rs = @pg_query($host_pa, $qry);
		if (($rs === false) || (@pg_num_rows($rs) < 1)) {
			return '';
		} else {
			$decoded_value = trim(@pg_fetch_result($rs, 0, 'concept'));
			@pg_free_result($rs);
			return $decoded_value;
		}
	}
	
	
	
	
	/**
	 * Get the names of ELR roles specified by TriSano role IDs in session.
	 *
	 * @return array Array containing names of ELR roles identified in session.
	 */
	function elrRoleNames() {
		global $host_pa, $my_db_schema;
		
		$role_names = array();
		
		$qry = 'SELECT id, name FROM '.$my_db_schema.'system_roles WHERE master_role_id in ('.implode(',', $_SESSION['user_roles']).') ORDER BY name;';
		$rs = @pg_query($host_pa, $qry);
		
		if ($rs !== false && pg_num_rows($rs) > 0) {
			while ($row = pg_fetch_object($rs)) {
				$role_names[intval($row->id)] = trim($row->name);
			}
		}
		@pg_free_result($rs);
		
		return $role_names;
	}
	
	
	
	
	/**
	 * Get the names of ELR roles specified by TriSano role IDs in session.
	 *
	 * @param int $flag_id Integer representation of the flag's binary ID
	 * @return string Flag name
	 */
	function decodeMessageFlagName($flag_id) {
		global $host_pa, $my_db_schema;
		
		$flag_name = '';
		if (filter_var($flag_id, FILTER_VALIDATE_INT)) {
			$decoded_id = intval(log(intval($flag_id), 2));
			$sql = 'SELECT label FROM '.$my_db_schema.'system_message_flags WHERE id = '.$decoded_id.';';
			$rs = @pg_query($host_pa, $sql);
			if ($rs !== false && @pg_num_rows($rs) > 0) {
				$flag_name = trim(@pg_fetch_result($rs, 0, 'label'));
				@pg_free_result($rs);
			}
		}
		return $flag_name;
	}
	
	
	
	
	/**
	 * Returns the ID of the Master Vocabulary item corresponding to the specified TriSano Result Value name.
	 * Returns FALSE if no vocabulary item found, no name specified, or an error.
	 *
	 * @param string $result_name Application Concept value
	 * @param int $app_id Application ID (Optional, Default TriSano)
	 * @return int|bool
	 */
	function trisanoResultMasterVocabIdByName($result_name = null, $app_id = 1) {
		global $host_pa, $my_db_schema;
		
		if (is_null($result_name) || (strlen(trim($result_name)) < 1)) {
			return false;
		}
		
		$qry = 'SELECT vm.id AS master_id 
			FROM '.$my_db_schema.'vocab_master_vocab vm
			INNER JOIN '.$my_db_schema.'vocab_master2app m2a ON (m2a.master_id = vm.id)
			INNER JOIN '.$my_db_schema.'structure_category sc ON (sc.id = vm.category)
			WHERE 
				sc.label = \'test_result\' AND
				m2a.app_id = '.filter_var($app_id, FILTER_SANITIZE_NUMBER_INT).' AND 
				m2a.coded_value ILIKE \''.pg_escape_string(trim($result_name)).'\';';
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			return intval(@pg_fetch_result($rs, 0, "master_id"));
		} else {
			return false;
		}
		@pg_free_result($rs);
	}
	
	
	
	
	/**
	 * Returns the descriptive text for the specified TriSano coded value
	 * Returns FALSE if no vocabulary item found, no name specified, or an error.
	 *
	 * @param string $coded_value TriSano Code
	 * @param string $table Code table to look up value
	 * @param string $category Code category to filter by
	 * @return string
	 */
	function decodeTrisanoCodedValue($coded_value, $table, $category) {
		global $props;
		if (strlen(trim($coded_value)) < 1) {
			return '';
		}
		
		switch ($table) {
			case 'codes':
				$primary_field = 'the_code';
				$secondary_field = 'code_name';
				break;
			case 'external_codes':
				$primary_field = 'the_code';
				$secondary_field = 'code_name';
				break;
			default:
				return '';
		}
		
		if ((($table == 'codes') || ($table == 'external_codes')) && (strlen(trim($category)) < 1)) {
			return '';
		}
		
		// prepare XML for query agent
		$base_xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><health_message><system>TRISANO</system><trisano_health/></health_message>';
		$base_sxe = new SimpleXMLElement($base_xml_string);
		
		$sub_xml = $base_sxe->trisano_health->addChild($table);
		$sub_xml->addChild($primary_field, trim($coded_value));
		
		if (!is_null($secondary_field)) {
			$sub_xml->addChild($secondary_field, trim($category));
		}
		
		$search_xml = $base_sxe->asXML();
		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e->getMessage(), -1, 1);
		} catch (SoapFault $f) {
			suicide($f->getMessage(), -1, 1);
		}
		
		if ($client) {
			try {
				$result = $client->findId(array("healthMessage" => $search_xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$return = simplexml_load_string($result->return);
			
			foreach ($return->trisano_health->$table as $return_item) {
				$return_val = trim($return_item->code_description);
			}
		}
		return $return_val;
	}
	
	
	
	
	/**
	 * Returns the NEDSS ID for the specified TriSano coded value
	 * Returns NULL if no vocabulary item found, no name specified, or an error.
	 *
	 * @param string $coded_value TriSano Code
	 * @param string $table Code table to look up value
	 * @param string $category Code category to filter by
	 * @return string
	 */
	function getNedssIdByCode($coded_value, $table, $category) {
		if (strlen(trim($coded_value)) < 1) {
			return null;
		}
		
		switch ($table) {
			case 'codes':
				$primary_field = 'the_code';
				$secondary_field = 'code_name';
				break;
			case 'external_codes':
				$primary_field = 'the_code';
				$secondary_field = 'code_name';
				break;
			default:
				return null;
		}
		
		if ((($table == 'codes') || ($table == 'external_codes')) && (strlen(trim($category)) < 1)) {
			return null;
		}
		
		// prepare XML for query agent
		$base_xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><health_message><system>TRISANO</system><trisano_health/></health_message>';
		$base_sxe = new SimpleXMLElement($base_xml_string);
		
		$sub_xml = $base_sxe->trisano_health->addChild($table);
		$sub_xml->addChild($primary_field, trim($coded_value));
		
		if (!is_null($secondary_field)) {
			$sub_xml->addChild($secondary_field, trim($category));
		}
		
		$search_xml = $base_sxe->asXML();
		
		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e->getMessage(), -1, 1);
		} catch (SoapFault $f) {
			suicide($f->getMessage(), -1, 1);
		}
		
		if ($client) {
			try {
				$result = $client->findId(array("healthMessage" => $search_xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$return = simplexml_load_string($result->return);
			
			foreach ($return->trisano_health->$table as $return_item) {
				$return_val = intval(trim($return_item->id));
			}
		}
		return $return_val;
	}
	
	
	
	
	/**
	 * Add (or update) a comment for a given ELR message & message flag
	 *
	 * @param int $system_message_id ELR Message ID
	 * @param int $flag_id ID of the flag being set
	 * @param string $info Comments to be set for this message & flag
	 * @return bool TRUE on success, FALSE on failure
	 */
	function addOrUpdateFlagComment($system_message_id = null, $flag_id = null, $info = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($system_message_id) || is_null($flag_id) || strlen(trim($info)) < 1) {
			return false;
		}
		
		$test_sql = 'SELECT id 
			FROM '.$my_db_schema.'system_message_flag_comments 
			WHERE system_message_id = '.intval($system_message_id).' 
			AND system_message_flag_id = '.intval($flag_id).';';
		$test_rs = @pg_query($host_pa, $test_sql);
		if (($test_rs === false) || (@pg_num_rows($test_rs) === 0)) {
			$test_id = 0;
		} else {
			$test_id = @pg_fetch_result($test_rs, 0, 'id');
		}
		
		if ($test_id > 0) {
			$sql = 'UPDATE '.$my_db_schema.'system_message_flag_comments 
				SET info = \''.@pg_escape_string(trim($info)).'\' 
				WHERE id = '.intval($test_id).';';
		} else {
			$sql = 'INSERT INTO '.$my_db_schema.'system_message_flag_comments 
				(system_message_id, system_message_flag_id, info)
				VALUES ('.intval($system_message_id).', '.intval($flag_id).', \''.@pg_escape_string(trim($info)).'\');';
		}
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false) {
			@pg_free_result($rs);
			return true;
		} else {
			return false;
		}
	}
	
	
	
	
	/**
	 * Add (or update) a comment for a given ELR message & message flag
	 *
	 * @param int $system_message_id ELR Message ID
	 * @param int $flag_id ID of the flag being set
	 * @param string $info Comments to be set for this message & flag
	 * @return void
	 */
	function clearFlagComment($system_message_id = null, $flag_id = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($system_message_id) || is_null($flag_id)) {
			return false;
		}
		
		$test_sql = 'SELECT id 
			FROM '.$my_db_schema.'system_message_flag_comments 
			WHERE system_message_id = '.intval($system_message_id).' 
			AND system_message_flag_id = '.intval($flag_id).';';
		$test_rs = @pg_query($host_pa, $test_sql);
		if (($test_rs === false) || (@pg_num_rows($test_rs) === 0)) {
			$test_id = 0;
		} else {
			$test_id = @pg_fetch_result($test_rs, 0, 'id');
		}
		
		if ($test_id > 0) {
			$sql = 'UPDATE '.$my_db_schema.'system_message_flag_comments 
				SET info = NULL 
				WHERE id = '.intval($test_id).';';
			$rs = @pg_query($host_pa, $sql);
			@pg_free_result($rs);
		}
	}
	
	
	
	
	/**
	 * Gets the comment for a given system message & message flag
	 *
	 * @param int $system_message_id ELR Message ID
	 * @param int $flag_id ID of the flag being set
	 * return string Comments for this message and flag.  Empty string if no comment is set
	 */
	function getMessageFlagComment($system_message_id = null, $flag_id = null) {
		global $host_pa, $my_db_schema;
		
		$comment = '';
		
		if (is_null($system_message_id) || is_null($flag_id)) {
			return $comment;
		}
		
		$sql = 'SELECT info 
			FROM '.$my_db_schema.'system_message_flag_comments 
			WHERE system_message_id = '.intval($system_message_id).' 
			AND system_message_flag_id = '.intval($flag_id).';';
		$rs = @pg_query($host_pa, $sql);
		if (($rs === false) || (@pg_num_rows($rs) === 0)) {
			$comment = '';
		} else {
			$comment = @pg_fetch_result($rs, 0, 'info');
		}
		
		return trim($comment);
	}
	
	
	
	
	/**
	 * Record an Audit Log event for an ELR message using the current logged-in user
	 * Returns an Audit Log ID on success, or FALSE on error.
	 *
	 * @param int $message_id ELR Message ID (from system_messages)
	 * @param int $action_id ID corresponding to the logged action (from system_message_actions)
	 * @param int $status Message queue ID where the action was generated from
	 * @param string $info Optional comments explaining the audited event
	 * @param int $lab_id Lab ID for the specified message (Optional)
	 * @return int|bool
	 */
	function auditMessage($message_id = null, $action_id = null, $status = null, $info = null, $lab_id = 1) {
		global $host_pa, $my_db_schema;
		
		if (is_null($message_id) || is_null($action_id) || is_null($status)) {
			return false;
		}
		
		$clean_info = 'NULL'; // postgresql NULL string
		if (!is_null($info)) {
			$temp_info = filter_var($info, FILTER_SANITIZE_STRING);
			if (($temp_info !== false) && (strlen(trim($temp_info)) > 0)) {
				$clean_info = "'".@pg_escape_string(trim($temp_info))."'";
			}
		}
		
		$qry = "INSERT INTO ".$my_db_schema."system_messages_audits (created_at, user_id, message_action_id, system_message_id, lab_id, fname, lname, system_status_id, info) VALUES (
			CURRENT_TIMESTAMP, 
			'".trim($_SESSION['umdid'])."', 
			".filter_var($action_id, FILTER_SANITIZE_NUMBER_INT).", 
			".filter_var($message_id, FILTER_SANITIZE_NUMBER_INT).", 
			".filter_var($lab_id, FILTER_SANITIZE_NUMBER_INT).", 
			'EMSA', 'EMSA', 
			".filter_var($status, FILTER_SANITIZE_NUMBER_INT).", ".$clean_info.") RETURNING id;";
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			return @pg_fetch_result($rs, 0, "id");
		} else {
			return false;
		}
		
		@pg_free_result($rs);
	}
	
	
	
	
	function getTrisanoReturnStatus($statuslist) {
		$status_data = array();
		$errors = '';
		$status = true;
		$status_message = '';
		
		foreach($statuslist as $key => $status_obj) {
			if (intval($status_obj->status) == 100) {
				$status = $status && true;
			} else {
				$status = $status && false;
				//$errors[] = array('code' => trim($status_obj->status), 'action' => trim($status_obj->action), 'message' => trim($status_obj->error_message));
				$errors .= '['.trim($status_obj->action).'] Error '.trim($status_obj->status).': '.trim($status_obj->error_message).PHP_EOL;
			}
		}
		
		$status_data['status'] = $status;
		$status_data['errors'] = $errors;
		
		return (object) $status_data;
	}
	
	
	
	
	/**
	 * Formats any XML string to properly include line breaks & nesting of nodes for display.
	 * Returns formatted XML string on success, or FALSE on error.
	 *
	 * @param string $xml XML string to be formatted
	 * @return string|bool
	 */
	function formatXml($xml = null) {
		if (is_null($xml)) {
			return false;
		}
		
		$dom = new DOMDocument("1.0");
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$dom->loadXML($xml);
		return $dom->saveXML();
	}
	
	
	
	
	/**
	 * Gets the CSV data type for non-ELR data-related activities
	 *
	 * @param string $type Plain-text data type
	 * @param int $mode Usage mode
	 * @return string
	 */
	function getNonELRDataType($type = null, $mode = 1) {
		if (is_null($type)) {
			return 0;
		}
		
		switch (filter_var($mode, FILTER_SANITIZE_NUMBER_INT)) {
			case 2:
				$salt = md5('importing');
				break;
			case 1:
				$salt = md5('onboarding');
				break;
			default:
				$salt = '';  // nice try... spoiled hash for you!
				break;
		}
		
		$secret_type = hash('haval256,5', $salt.filter_var($type, FILTER_SANITIZE_STRING));
		return $secret_type;
	}
	
	
	
	
	/**
	 * Checks to see if the given string is Base64-encoded or not; if so, returns decoded value
	 *
	 * @param string $input
	 * @return string
	 */
	function decodeIfBase64Encoded($input) {
		if (base64_encode(base64_decode($input)) === $input) {
			return base64_decode($input);
		} else {
			return $input;
		}
	}
	
	
	
	
	/**
	 * Sorts HL7 column names for the onboarding message review generation script
	 * Expects column parameters to be in the format of "SEG_I-J" where...
	 *   SEG = HL7 segment
	 *   I = Collection index of the segment, where more than one of the same segment exists in a single message (optional; if only instance of segment in message, use format "SEG-J"
	 *   J = Field identifier
	 *
	 * @param mixed $a
	 * @param mixed $b
	 * @return bool
	 */
	function hl7ColumnSort($a, $b) {
		$hl7_segments = array(
			'MSH', 
			'SFT', 
			'PID', 
			'NK1', 
			'PV1', 
			'PV2', 
			'ORC', 
			'OBR', 
			'OBX', 
			'SPM', 
			'ZLR', 
			'NTE'
		);
		
		$first_split_a = explode('-', $a);
		$first_split_b = explode('-', $b);
		
		$second_split_a = explode('_', $first_split_a[0]);
		$second_split_b = explode('_', $first_split_b[0]);
		
		$segment_a = trim($second_split_a[0]);
		$segment_b = trim($second_split_b[0]);
		
		if (isset($second_split_a[1])) {
			$segment_coll_order_a = intval($second_split_a[1]);
		} else {
			$segment_coll_order_a = 0;
		}
		if (isset($second_split_b[1])) {
			$segment_coll_order_b = intval($second_split_b[1]);
		} else {
			$segment_coll_order_b = 0;
		}
		
		$segment_id_a = intval($first_split_a[1]);
		$segment_id_b = intval($first_split_b[1]);
		
		$segment_order_a = array_search($segment_a, $hl7_segments);
		$segment_order_b = array_search($segment_b, $hl7_segments);
		
		if ($segment_order_a == $segment_order_b) {
			if ($segment_coll_order_a == $segment_coll_order_b) {
				if ($segment_id_a == $segment_id_b) {
					return 0;
				} else {
					return ($segment_id_a < $segment_id_b) ? -1 : 1;
				}
			} else {
				return ($segment_coll_order_a < $segment_coll_order_b) ? -1 : 1;
			}
		} else {
			return ($segment_order_a < $segment_order_b) ? -1 : 1;
		}
	}
	
	
	
	
	/**
	 * Returns Excel-like column labels back for a give zero-based integer
	 * (0 = 'A', 27 = 'AA', etc.)
	 * [Courtesy http://stackoverflow.com/users/338665/ircmaxell from http://stackoverflow.com/questions/3302857/algorithm-to-get-the-excel-like-column-name-of-a-number]
	 *
	 * @param int $num Integer representing the column index to return (starts at 0 = 'A')
	 * @return string
	 */
	function getExcelColumnLabel($num) {
		$numeric = $num % 26;
		$letter = chr(65 + $numeric);
		$num2 = intval($num / 26);
		if ($num2 > 0) {
			return getExcelColumnLabel($num2 - 1) . $letter;
		} else {
			return $letter;
		}
	}
	
	
	
	
	/**
	 * Returns elapsed time (in seconds) between two Unix timestamps.
	 *
	 * @param int $time1
	 * @param int $time2
	 * @return int Elapsed time (in seconds) between <i>time1</i> and <i>time2</i>.  Returns 0 if times are identical.
	 */
	function elapsedTime($time1 = null, $time2 = null) {
		$time_diff = 0;
		$t1 = filter_var($time1, FILTER_SANITIZE_NUMBER_INT);
		$t2 = filter_var($time2, FILTER_SANITIZE_NUMBER_INT);
		
		if ($t1 !== false && $t2 !== false) {
			$time_diff = abs($t1 - $t2);
		}
		
		return $time_diff;
	}
	
	
	
	
	/**
	 * Formats an elapsed time (in seconds) to a string value.
	 *
	 * @param int $time
	 * @param int $precision Precision to use for resulting string (0 = days, 1 = hours, 2 = minutes, 3 = seconds)
	 * @return string Elapsed time (in days, hours, minutes, and seconds) for given <i>time</i>.  Returns <b>FALSE</b> if <i>time</i> is 0.
	 */
	function elapsedTimeToString($time = null, $precision = 0) {
		$elapsed = '';
		$t = filter_var($time, FILTER_SANITIZE_NUMBER_INT);
		
		switch (filter_var($precision, FILTER_SANITIZE_NUMBER_INT)) {
			case 1:
			case 2:
			case 3:
				$p = $precision;
				break;
			default:
				$p = 0;
				break;
		}
		
		if ($t !== false) {
			$days = intval(floor($t / (3600*24)));
			$hours = intval(floor(($t - ($days*3600*24)) / 3600));
			$mins = intval(floor(($t - ($days*3600*24) - ($hours*3600)) / 60));
			$secs = intval(floor(($t - ($days*3600*24) - ($hours*3600) - ($mins*60)) ));
			
			$elapsed_array = array();
			
			if ($days > 0) {
				$elapsed_array[] = ($days == 1) ? $days.' day' : $days.' days';
			}
			if ($hours > 0 && $p > 0) {
				$elapsed_array[] = ($hours == 1) ? $hours.' hour' : $hours.' hours';
			}
			if ($mins > 0 && $p > 1) {
				$elapsed_array[] = ($mins == 1) ? $mins.' minute' : $mins.' minutes';
			}
			if ($secs > 0 && $p > 2) {
				$elapsed_array[] = ($secs == 1) ? $secs.' second' : $secs.' seconds';
			}
			
			if ($t > 0) {
				$elapsed .= implode(', ', $elapsed_array);
			}
		}
		
		return $elapsed;
	}

?>