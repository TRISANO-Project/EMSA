<?php

//error_log( '>>>>>>>>>>In security.php');
//error_log( '>>>>>>>>>>server env: ' . $server_environment);

//foreach ($_SESSION as $k=>$v)
//{
//	error_log( "session >>>>" . $k . ' ' . $v);
//}

//$_SESSION['umdid'] = 'elr_automation';
$elr_auto_key = $_REQUEST['elr_auto_key'];
//error_log("#1 USER: ".$_SESSION['umdid']);
//error_log("\n#1 RequestX: " . print_r($_REQUEST, true));
//error_log("\nRequest IP: " . $_SERVER['REMOTE_ADDR']);
//error_log("#1 execute logout: ".$execute_logout);
//error_log("#1 elr auto: ".$elr_auto_key);
$login_msg = "";
$elr_username = $_POST['elr_username'];
$elr_password = $_POST['elr_password'];
if(isset($elr_username) || isset($elr_password)){
    $login_msg = "Invalid username/password.";
}
if(isset($elr_auto_key) && $elr_auto_key == $props['elr_auto_key']) {
    $_SESSION['umdid'] = 'elr_automation';
}
if(isset($execute_logout)) {
//error_log("LOGGING OUT");
    if($execute_logout == "retry") {
        $login_msg = "Invalid username/password.";
    }
    unset($_SESSION['umdid']);
    unset($execute_logout);
}
//$_SESSION['umdid'] = 'elr_automation';
if(isset($_SESSION['umdid'])) {
    // do nothing
} else if(isset($elr_username) && isset($elr_password)){
    $shell_cmd = "/var/www/html/elrapp/emsa_login.sh " . escapeshellcmd($elr_username) . " " . escapeshellcmd($elr_password);
    $result = trim(shell_exec($shell_cmd));
    if($result == "Success") {
        $_SESSION['umdid'] = $elr_username;
        header('Location: /elrapp');
        exit;
    } else {
        header('Location: /elrapp?log_out=retry');
        exit;
    }
} else {
    echo "<center>" . $login_msg  . "<br>";
?>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    Username: <input type="text" name="elr_username">
    <p />
    Password: <input type="password" name="elr_password">
    <p />
    <input type="submit" name="submit" value="Log In">
    </center>
<?php
    exit;
}
//error_log("TODO Jay >>>>>>>>PHP_AUTH_USER: " . $_SERVER['PHP_AUTH_USER']);
//error_log("TODO Jay >>>>>>>>IP: " . $_SERVER['REMOTE_ADDR']);
// error_log('$_SERVER: '.json_encode($_SERVER));

// TODO Jay need to set up user credentialing
//	$_SESSION['umdid'] = 'elr_automation';
//	$ignore_permission = true;

//error_log("TODO Jay >>>>>>>> UNKNOWN USER: " . $_SERVER['REMOTE_ADDR']);
//	$_SESSION['umdid'] = 'unknown user';}

/*    if($props['use_ntls_credentialing'] != 'true') {
        // UTAH credentialing
        if ($server_environment == ELR_ENV_DEV) {
            $_SESSION['umdid'] = '9999';  // hard-code to 'ELR' user for demo site
        } else {
		$_SESSION['umdid'] = getenv('HTTP_UID');
        }
    } else {
        // NTLS credentialing for SNHD
	    $domain_uid = $_SERVER['PHP_AUTH_USER'];
		$_SESSION['umdid'] = substr(strrchr($domain_uid, '+'), 1);
    }
*/
//error_log("TODO Jay >>>>>>>>>>XXX provided user: " . $_SESSION['umdid']);

    // define permission-based constants
	define('URIGHTS_DASHBOARD', 1);
	define('URIGHTS_PENDING', 2);
	define('URIGHTS_ENTRY', 3);
	define('URIGHTS_ASSIGNED', 4);
	define('URIGHTS_GRAY', 5);
	define('URIGHTS_ADMIN', 6);
	define('URIGHTS_NONELR', 8);
	define('URIGHTS_TAB_FULL', 17);
	define('URIGHTS_TAB_AUDIT', 18);
	define('URIGHTS_TAB_ERROR', 19);
	define('URIGHTS_TAB_HL7', 20);
	define('URIGHTS_TAB_XML', 21);
	define('URIGHTS_TAB_QA', 24);
	define('URIGHTS_ACTION_MOVE', 22);
	define('URIGHTS_ACTION_DELETE', 23);

	if (!isset($_SESSION) || empty($_SESSION)) {
		doLogOff();
	}

	if (isset($_GET['logoff']) && ($_GET['logoff'] == 1)) {
		doLogOff();
	}

	if (isset($_GET['updateperm']) && ($_GET['updateperm'] == 1)) {
		clearPermissions();
	}

	if (!isset($_SESSION['jurisdictions']) || !isset($_SESSION['trisano_codes']) || !isset($_SESSION['user_system_roles'])) {
		clearPermissions();  // if user for some reason doesn't have these set, reset the permissions
	}

	// error_log('$_SESSION: ' . json_encode($_SESSION));
	if ((!isset($_SESSION['user_roles']) || (count($_SESSION['user_roles']) == 0)) && isset($_SESSION['umdid'])) {
		setPermissions($_SESSION['umdid']);
	}

	if (!checkPermission()) {
		if (isset($ignore_permission) && $ignore_permission) {
			// ignore permission
		} else {
			ob_end_clean();
            // force the user roles to be reloaded
            unset($_SESSION['user_roles']);
			header('Location: '.NOACCESS_URL);
			exit;
		}
	}

	// load TriSano code tables into _SESSION memory
	dumpTable('external_codes');
	dumpTable('diseases');
	dumpTable('organisms');
	dumpTable('common_test_types');

	/**
	 * Log off current user
	 *
	 * @return void
	 */
	function doLogOff() {
		unset($_SESSION);
		ob_end_clean();
		header('Location: '.LOGOUT_URL);
		exit;
	}

	/**
	 * Reset current user's permission (re-query from NEDSS)
	 *
	 * @return void
	 */
	function clearPermissions() {
		unset($_SESSION['user_roles'], $_SESSION['user_system_roles'], $_SESSION['trisano_codes'], $_SESSION['jurisdictions'], $_SESSION['user_role_menus']);
		//ob_end_clean();
		//header('Location: '.MAIN_URL);
		//exit;
	}

	/**
	 * Set permissions for current user
	 *
	 * @param mixed $user_mdid User's UMDID
	 * @return bool
	 */
	function setPermissions($user_mdid = null) {
		global $props;

        if (!class_exists('SoapClient')) {
			return false;
		}
		global $host_pa, $my_db_schema;

		$_SESSION['user_role_menus'] = array();
		$_SESSION['user_roles'] = array();
		$_SESSION['user_system_roles'] = array();
		$_SESSION['jurisdictions'] = array();

		// load jurisdictions into _SESSION
		$j_sql = 'SELECT health_district, system_external_id FROM '.$my_db_schema.'system_districts ORDER BY health_district;';
		$j_rs = @pg_query($host_pa, $j_sql);
		if ($j_rs !== false) {
			while ($j_row = @pg_fetch_object($j_rs)) {
				$_SESSION['jurisdictions'][intval($j_row->system_external_id)] = trim($j_row->health_district);
			}
		}
		@pg_free_result($j_rs);

		if (is_null($user_mdid)) {
			return false;
		}

//error_log('TODO Jay >>>>>>>>>> User and WSDL = ' . $user_mdid . ' ' . $props['sqla_wsdl_url']);
		$xml = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <system>TRISANO</system>
    <trisano_health>
        <users>
            <uid>$user_mdid</uid>
        </users>
    </trisano_health>
</health_message>
EOX;

		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
error_log('TODO Jay >>>>Unable to set user permissions:  Could not connect to TriSano service: '.$e->getMessage());
			suicide('Unable to set user permissions:  Could not connect to TriSano service.<br>'.$e->getMessage(), -1, 1);
			return false;
		} catch (SoapFault $f) {
error_log('TODO Jay >>>>Unable to set user permissions:  SOAP Could not connect to TriSano service: '.$f->getMessage());
			suicide('Unable to set user permissions:  Could not connect to TriSano service.<br>'.$f->getMessage(), -1, 1);
			return false;
		}

		if ($client) {
        try {
//error_log("TODO Jay >>>>>>>> xml sent:".$xml);
				$result = $client->getUserRoles(array("healthMessage" => $xml));
//error_log("TODO Jay >>>>>>>> xml result:".print_r($result, true));
			} catch (Exception $e) {
//error_log('TODO Jay >>>>Error Getting user roles: '.$e->getMessage());
				suicide('11 Unable to set user permissions:  Could not query user roles.<br>'.$e->getMessage()." ".date('Y-m-d H:i:s'), -1, 1);
				return false;
			} catch (SoapFault $f) {
//error_log('TODO Jay >>>>Error Getting user roles SOAP: '.$f->getMessage());
				suicide('22 Unable to set user permissions:  Could not query user roles.<br>'.$f->getMessage(), -1, 1);
				return false;
			}
			$xmlparse = simplexml_load_string($result->return);

			foreach ($xmlparse->trisano_health->role_memberships as $health) {
				if (!in_array(intval($health->role_id), $_SESSION['user_roles']))
					array_push($_SESSION['user_roles'], intval($health->role_id));
			}

// TODO Jay
//	array_push($_SESSION['user_roles'], 8);
//	array_push($_SESSION['user_roles'], 9);
//	array_push($_SESSION['user_roles'], 10);
//

//error_log('TODO Jay >>>>got the following user roles: ' . print_r($_SESSION['user_roles'], true));
			getSystemRoles($_SESSION['user_roles']);
			getUserMenus($_SESSION['user_system_roles']);

			unset($client);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get menus for current user
	 *
	 * @param array $roles Collection of user's role IDs
	 * @return void
	 */
	function getUserMenus($roles) {
		global $host_pa, $my_db_schema;

		if (count($roles) > 0) {
			$user_roles = implode(',', $roles);
			$sql = 'SELECT system_menu_id FROM '.$my_db_schema.'system_role_menus
					WHERE system_role_id IN ('.$user_roles.');';

			$results = get_db_result_set($host_pa, $sql);

			while ($row = get_db_row_array($results)) {
				$_SESSION['user_role_menus'][] = $row['system_menu_id'];
			}
		}
	}

	/**
	 * Get assigned system role IDs for current user
	 *
	 * @param array $master_roles Collection of user's role IDs
	 * @return void
	 */
	function getSystemRoles($master_roles) {
		global $host_pa, $my_db_schema;

		if (count($master_roles) > 0) {
			$user_master_roles = implode(',', $master_roles);
			$sql = 'SELECT id FROM '.$my_db_schema.'system_roles
					WHERE master_role_id IN ('.$user_master_roles.');';

			$results = get_db_result_set($host_pa, $sql);

			while ($row = get_db_row_array($results)) {
				$_SESSION['user_system_roles'][] = $row['id'];
			}
		}
	}

	/**
	 * Check to see if current user has valid permissions
	 *
	 * @param int $menu (Optional) Specific feature to check permission against, e.g. a menu option or action bar button
	 * @return bool
	 */
	function checkPermission($menu = null) {
		// error_log('$_SESSION: ' . json_encode($_SESSION));
//error_log('TODO Jay >>>>>>>>>>count of user_role_menus: ' . count($_SESSION['user_role_menus']));
//error_log('TODO Jay >>>>>>>>>>user_role_menus: ' . print_r($_SESSION['user_role_menus'], true));
		if (count($_SESSION['user_role_menus']) > 0) {
			if (is_null($menu) || empty($menu)) {
				// no menu specified, check permissions for currently-selected page
				// 'home'/'dashboard' don't have a 'selected_page' specified in querystring, so if not set, default to 1
				$test_selected_page = ((isset($_GET['selected_page'])) ? intval(trim($_GET['selected_page'])) : 1);
			} else {
				// specific menu passed to check
				$test_selected_page = ((filter_var($menu, FILTER_VALIDATE_INT)) ? filter_var($menu, FILTER_SANITIZE_NUMBER_INT) : -1);
			}
//error_log('TODO Jay >>>>>>>>>>checkPermissions test page: '.$test_selected_page);

			if (filter_var($test_selected_page, FILTER_VALIDATE_INT) && $test_selected_page > 0) {
				// verify user has access to this menu
				if (in_array($test_selected_page, $_SESSION['user_role_menus'])) {
//error_log('TODO Jay >>>>>>>>>>checkPermissions returning true');
					return true;
				} else {
//error_log('TODO Jay >>>>>>>>>>checkPermissions returning false 1');
					return false;
				}
			} else {
//error_log('TODO Jay >>>>>>>>>>checkPermissions returning false 2');
				return false;
			}
		} else {
//error_log('TODO Jay >>>>>>>>>>checkPermissions returning false 3');
			return false;
		}
	}

	/**
	 * Dumps the specified TriSano code table into _SESSION memory
	 *
	 * @param string $table TriSano code table name
	 * @return void
	 */
	function dumpTable($table) {
		global $props;
		$clean_table = htmlspecialchars($table);
		if (!isset($_SESSION['trisano_codes'][$clean_table]) || count($_SESSION['trisano_codes'][$clean_table]) < 1) {
			$xml = <<<EODT
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health>
		<dump_table>$clean_table</dump_table>
	</trisano_health>
</health_message>
EODT;

			try {
				$client = @new SoapClient($props['sqla_wsdl_url']);
			} catch (Exception $e) {
				suicide('Unable to obtain TriSano Codes:  Could not connect to TriSano service.<br>'.$e->getMessage(), -1, 1);
				return false;
			} catch (SoapFault $f) {
				suicide('Unable to obtain TriSano Codes:  Could not connect to TriSano service.<br>'.$f->getMessage(), -1, 1);
				return false;
			}

			if ($client) {
				try {
					$result = $client->getTableData(array("healthMessage" => $xml));
				} catch (Exception $e) {
					suicide('Unable to obtain TriSano Codes:  Could not query codes table.<br>'.$e->getMessage(), -1, 1);
					return false;
				} catch (SoapFault $f) {
					suicide('Unable to obtain TriSano Codes:  Could not query codes table.<br>'.$f->getMessage(), -1, 1);
					return false;
				}
				$xmlparse = simplexml_load_string($result->return);

				if ($clean_table == 'external_codes') {
					foreach($xmlparse->trisano_health->external_codes as $external_code_node) {
						if (trim($external_code_node->live) == 'true') {
							$_SESSION['trisano_codes']['external_codes'][trim($external_code_node->code_name)][intval(trim($external_code_node->id))] = array(
								'the_code' => trim($external_code_node->the_code),
								'code_description' => trim($external_code_node->code_description)
							);
						}
					}
					#debug print_r($_SESSION);
				} else {
					$dumped_array = explode('|', trim($xmlparse->trisano_health->dump_table));
					$temp_array = array();

					foreach ($dumped_array as $dump_item) {
						unset($this_temp_array);
						$this_temp_array = explode('=', $dump_item);
						$temp_array[intval(trim($this_temp_array[0]))] = trim($this_temp_array[1]);
					}

					if (isset($temp_array[0])) {
						unset($temp_array[0]);
					}

					if (count($temp_array) > 0) {
						$_SESSION['trisano_codes'][$clean_table] = $temp_array;
					}

					#debug print_r($_SESSION);
				}
			}
		}
	}

?>
