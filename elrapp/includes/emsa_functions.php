<?php

	/**
	 * Main function to retrieve ELR messages out of the database for display on EMSA list screens.  Can return a simple message count or a complete list of records.
	 *
	 * @param int $type Message queue identifier
	 * @param int $sqlStart Row offset
	 * @param int $showPerPage Rows per page, default 10
	 * @param int $immediate Flag indicating whether to query only immediately-reportable conditions
	 * @param bool $count_only Boolean flag indicating whether to only return a count (TRUE) or a list of records (FALSE)
	 * @param mixed $exceptions
	 * @param int $task_id
	 * @param int $lab_id If set, restricts query to messages from a specified lab ID
	 * @param int $focus_id If set, retrieve only a single message specified by System Message ID & ignore all other filters (Optional)
	 * @return mixed
	 */
	function getEmsaLists($type = 1, $sqlStart = 1, $showPerPage = 10, $immediate = 0, $count_only = false, $exceptions = '', $task_id = 0, $lab_id = null, $focus_id = null) {
		global $host_pa, $my_db_schema, $props;

		$exception_sql = '';
		$lab_name_sql = '';
		$lab_limiter_sql = '';
		$filter_disease_sql = '';
		$filter_testtype_sql = '';
		$filter_mflag_sql = '';
		$filter_clinician_sql = '';
		$search_sql = '';
		$focus_sql = '';
		$role_sql = '';
		$role_wrapper_sql = '';
		$override_resultcode_restriction = '';
		$role_join = 'AND';
        $date_stmt = '';
        if($type == ASSIGNED_STATUS) {
            $now_minus_30 = time() - 30*24*3600;
            $date_stmt = " AND assigned_date > '".date("Y-m-d", $now_minus_30)."' ";
        }

		// if user selected specific role from drop-down, use only that role;
		// otherwise, use intersection of all roles for message visibility
		if (isset($_SESSION['override_user_role']) && !empty($_SESSION['override_user_role'])) {
			if ((intval(trim($_SESSION['override_user_role'])) === $props['elr_qa_role']) || (intval(trim($_SESSION['override_user_role'])) === ELR_ADMIN_ROLE)) {
				$override_resultcode_restriction = ' OR (
						(co2.id IS NULL)
						AND EXISTS (SELECT spm.id
							FROM '.$my_db_schema.'structure_path_mirth spm
							INNER JOIN '.$my_db_schema.'structure_path sp ON (spm.master_path_id = sp.id)
							WHERE (spm.lab_id = sm.lab_id)
							AND (sp.xpath = \'/health/labs/local_result_value_2\')
						)
					)';  // if member of admin or QA role, allow user to also view msgs with null child SNOMED code
				$role_join = 'OR';
			}
			$role_sql = intval(trim($_SESSION['override_user_role']));
		} else {
			if (count(array_intersect(array($props['elr_qa_role'], ELR_ADMIN_ROLE), $_SESSION['user_system_roles'])) > 0) {
				$override_resultcode_restriction = ' OR (
						(co2.id IS NULL)
						AND EXISTS (SELECT spm.id
							FROM '.$my_db_schema.'structure_path_mirth spm
							INNER JOIN '.$my_db_schema.'structure_path sp ON (spm.master_path_id = sp.id)
							WHERE (spm.lab_id = sm.lab_id)
							AND (sp.xpath = \'/health/labs/local_result_value_2\')
						)
					)';  // if member of admin or QA role, allow user to also view msgs with null child SNOMED code
				$role_join = 'OR';
			}
			$role_sql = implode(',', $_SESSION['user_system_roles']);
		}

		$role_wrapper_sql = '
			AND ((
				(
					(COALESCE(cl.validate_child_snomed, ctc.validate_child_snomed) IS FALSE OR sm.local_result_value_2 IS NULL)
					AND (ml.loinc IN (
						SELECT rd.master_loinc_code
						FROM '.$my_db_schema.'system_role_loincs_by_lab rd
						WHERE (rd.lab_id = sm.lab_id)
						AND (rd.system_role_id IN ('.$role_sql.'))
					))
				)
				OR (
					(
						(co2.id IS NOT NULL)
						AND (COALESCE(cl.validate_child_snomed, ctc.validate_child_snomed) IS TRUE)
						AND (sm.local_result_value_2 IN (
								SELECT rcs.child_snomed
								FROM '.$my_db_schema.'system_role_child_snomeds_by_lab rcs
								WHERE (rcs.lab_id = sm.lab_id)
								AND (rcs.system_role_id IN ('.$role_sql.'))
							)
						)
					)
					'.$override_resultcode_restriction.'
				)
			)
			'.$role_join.' (ml.loinc IN (
				SELECT rd.master_loinc_code
				FROM '.$my_db_schema.'system_role_loincs_by_lab rd
				WHERE (rd.lab_id = sm.lab_id)
				AND (rd.system_role_id IN ('.$role_sql.'))
			))) ';


		if (($_SESSION["emsa_params"]["type"] == PENDING_STATUS) || ($_SESSION["emsa_params"]["type"] == EXCEPTIONS_STATUS)) {
			if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["eflag"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["eflag"]) && (count($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["eflag"]) > 0)) {
				$exception_sql = sprintf(" AND sm.id IN (SELECT DISTINCT system_message_id FROM %ssystem_message_exceptions WHERE exception_id IN (%s))", $my_db_schema, implode(",", $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["eflag"]));  // filter for selected exception types
			}
			if (isset($_SESSION['emsa_params'][$_SESSION['emsa_params']['type']]['filters']['evalue']) && (strlen(trim($_SESSION['emsa_params'][$_SESSION['emsa_params']['type']]['filters']['evalue'])) > 0)) {
				$exception_sql = ' AND sm.id IN (SELECT DISTINCT system_message_id FROM '.$my_db_schema.'system_message_exceptions WHERE info ILIKE \''.pg_escape_string(trim(decodeIfBase64Encoded($_SESSION['emsa_params'][$_SESSION['emsa_params']['type']]['filters']['evalue']))).'\')';  // filter for selected exception types
			}
		}

		// apply search terms...
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_sql"]) && (strlen(trim($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_sql"])) > 0)) {
			$search_clean = pg_escape_string($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["q_sql"]);
			$search_sql = " AND ( (sm.fname ILIKE '%".$search_clean."%') OR (sm.lname ILIKE '%".$search_clean."%') OR (sm.disease ILIKE '%".$search_clean."%') )";
		}

		// apply any lab-specific filters...
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["lab"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["lab"])) {
			$lab_filter = implode(",", $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["lab"]);
			$lab_name_sql = " AND sm.lab_id IN (" . @pg_escape_string($lab_filter) . ") ";
		}

		// limit by lab_id if passed (i.e. for pending queue
		if (isset($lab_id) && !is_null($lab_id) && filter_var($lab_id, FILTER_VALIDATE_INT) && (intval($lab_id) > 0)) {
			$lab_limiter_sql = " AND sm.lab_id = " . intval($lab_id) . " ";
		}

		// apply any user-specified disease filters...
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["disease"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["disease"])) {
			$filter_disease_counter = 0;
			foreach ($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["disease"] as $filter_disease_name) {
				if ($filter_disease_counter == 0) {
					$filter_disease_sql = " AND ((sm.disease ILIKE '" . @pg_escape_string(decodeIfBase64Encoded($filter_disease_name)) . "')";
				} else {
					$filter_disease_sql .= " OR (sm.disease ILIKE '" . @pg_escape_string(decodeIfBase64Encoded($filter_disease_name)) . "')";
				}
				$filter_disease_counter++;
			}
			$filter_disease_sql .= ")";
		}

		// apply any user-specified test type filters...
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["testtype"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["testtype"])) {
			$filter_testtype_sql = ' AND (ml.trisano_test_type IN ('.implode(',', $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["testtype"]).'))';
		}

		// apply any user-specified message flag filters...
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["mflag"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["mflag"])) {
			$filter_mflag_sql = " AND (sm.message_flags & (" . implode(' | ', $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["mflag"]) . ") <> 0)";
		} else {
			$filter_mflag_sql = " AND (sm.message_flags & ".EMSA_FLAG_INVESTIGATION_COMPLETE." = 0)"; // everything that doesn't have "Investigation Completed" set
		}

		// apply any user-specified clinician filters...
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["clinician"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["clinician"])) {
			$filter_clinician_counter = 0;
			foreach ($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["clinician"] as $filter_clinician_name) {
				if ($filter_clinician_counter == 0) {
					$filter_clinician_sql = " AND ((sm.clinician ILIKE '" . @pg_escape_string($filter_clinician_name) . "')";
				} else {
					$filter_clinician_sql .= " OR (sm.clinician ILIKE '" . @pg_escape_string($filter_clinician_name) . "')";
				}
				$filter_clinician_counter++;
			}
			$filter_clinician_sql .= ")";
		}

		// if only getting a rowcount back, select a counter & don't limit
		if ($count_only) {
			$select_stmt = "count(sm.id) AS counter";
			$paging_stmt = "";
		} else {
			$select_stmt = "sm.*, om.created_at, mc.immediate_notify";
			if ($type == ASSIGNED_STATUS && isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"])) {
				// if sort order is specified, use
				// otherwise, sort by assigned_date DESC (most recent at the top)
				// ...with a fallback to submit_date, just in case...
				switch (intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"])) {
					case 6:
						$paging_stmt = "ORDER BY sm.assigned_date DESC NULLS LAST, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 5:
						$paging_stmt = "ORDER BY sm.assigned_date ASC NULLS LAST, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 4:
						$paging_stmt = "ORDER BY sm.lname DESC, sm.fname DESC, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 3:
						$paging_stmt = "ORDER BY sm.lname ASC, sm.fname ASC, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 2:
						$paging_stmt = "ORDER BY om.created_at DESC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 1:
						$paging_stmt = "ORDER BY om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					default:
						$paging_stmt = "ORDER BY sm.assigned_date DESC NULLS LAST, om.created_at DESC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
				}
			} elseif ($type == ASSIGNED_STATUS) {
				// if assigned, sort by assigned_date DESC (most recent at the top)
				// ...with a fallback to submit_date, just in case...
				$paging_stmt = "ORDER BY sm.assigned_date DESC NULLS LAST, om.created_at DESC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
			} elseif ($type == PENDING_STATUS) {
				// sort by immediately notifiable, then submit_date
				$paging_stmt = "ORDER BY mc.immediate_notify DESC, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
			} elseif ($type == GRAY_STATUS && isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"])) {
				// if sort order is specified, use
				// otherwise, sort by submit_date ASC (oldest at the top)
				switch (intval($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["sort_order"])) {
					case 6:
						$paging_stmt = "ORDER BY sm.assigned_date DESC, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 5:
						$paging_stmt = "ORDER BY sm.assigned_date ASC, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 4:
						$paging_stmt = "ORDER BY sm.lname DESC, sm.fname DESC, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 3:
						$paging_stmt = "ORDER BY sm.lname ASC, sm.fname ASC, om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					case 2:
						$paging_stmt = "ORDER BY om.created_at DESC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
					default:
						$paging_stmt = "ORDER BY om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
						break;
				}
			} else {
				// otherwise, sort by submit_date ASC (oldest at the top)
				$paging_stmt = "ORDER BY om.created_at ASC LIMIT ".$showPerPage." OFFSET ".$sqlStart;
			}
		}

		if ($immediate == 1) {
			// restrict to 'immediately reportable' diseases
			$immediate_stmt = "(mc.immediate_notify IS TRUE)";
		} elseif ($immediate == 2) {
			// show all diseases whether immediately reportable or not
			// used when list type does not segregate, such as Assigned or Gray lists
			$immediate_stmt = "(mc.immediate_notify IS NOT UNKNOWN)";
		} else {
			// restrict to 'non-immediately reportable' diseases
			$immediate_stmt = "(mc.immediate_notify IS NOT TRUE)";
		}
		// for SNHD, show ONLY POSITIVE RESULTS or labs with NO RESULT in immediate area
        if ($props['emsa_environment'] == 'SNHD') {
			if ($immediate == 1) {
				// restrict to 'immediately reportable' diseases (POSITIVE or no result)
				$immediate_stmt = "((mc.immediate_notify IS TRUE) AND (sm.master_xml LIKE '%<test_result>POSITIVE</test_result>%' OR sm.master_xml NOT LIKE '%<test_result>%'))";
			} elseif ($immediate != 2) {
				// restrict to 'non-immediately reportable' diseases (or NEGATIVE of no result immediately reportable)
//				$immediate_stmt = "((mc.immediate_notify IS NOT TRUE) OR (sm.master_xml NOT LIKE '%<test_result>POSITIVE</test_result>%' AND sm.master_xml LIKE '%<test_result>%'))";
				$immediate_stmt = "((mc.immediate_notify IS NOT TRUE) OR (sm.master_xml NOT LIKE '%<test_result>POSITIVE</test_result>%'))";
			}
		}

		// check to see if 'show automated' flag is set...
		$automated_stmt = '';
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"])) {
			$automated_stmt = ' AND';
			if (count($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"]) > 1) {
				$automated_stmt .= ' (';
			}
			if (in_array(0, $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"])) {
				// show only automated
				$automated_stmt .= ' EXISTS (SELECT sma.id FROM '.$my_db_schema.'system_messages_audits sma WHERE sma.user_id = \'9999\' AND sma.message_action_id IN (22, 23, 24, 28, 29) AND sma.system_message_id = sm.id) ';
				//$automated_stmt .= ' NOT EXISTS (SELECT sma.id FROM '.$my_db_schema.'system_messages_audits sma WHERE sma.user_id <> \'9999\' AND sma.system_message_id = sm.id) ';
				//$automated_stmt .= ' ((SELECT SUM(CASE WHEN user_id <> \'9999\' THEN 1 ELSE 0 END) AS auto FROM '.$my_db_schema.'system_messages_audits sma1 WHERE sma1.system_message_id = sm.id) = 0) ';
			}
			if (count($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"]) > 1) {
				$automated_stmt .= ' OR ';
			}
			if (in_array(1, $_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"])) {
				// show only manual
				$automated_stmt .= ' NOT EXISTS (SELECT sma.id FROM '.$my_db_schema.'system_messages_audits sma WHERE sma.user_id = \'9999\' AND sma.message_action_id IN (22, 23, 24, 28, 29) AND sma.system_message_id = sm.id) ';
				//$automated_stmt .= ' EXISTS (SELECT sma.id FROM '.$my_db_schema.'system_messages_audits sma WHERE sma.user_id <> \'9999\' AND sma.system_message_id = sm.id) ';
				//$automated_stmt .= ' ((SELECT SUM(CASE WHEN user_id <> \'9999\' THEN 1 ELSE 0 END) AS auto FROM '.$my_db_schema.'system_messages_audits sma1 WHERE sma1.system_message_id = sm.id) > 0) ';
			}
			if (count($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showauto"]) > 1) {
				$automated_stmt .= ') ';
			}
		}

		// check to see if 'show deleted' flag is set...
		if (isset($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showdeleted"]) && is_array($_SESSION["emsa_params"][$_SESSION["emsa_params"]["type"]]["filters"]["showdeleted"])) {
			// show deleted messages, where applicable
			switch ($type) {
				case HOLD_STATUS:
					// 'hold' queue is defined by deleted flag, so it always stays the same
					$deleted_stmt = "(sm.deleted = 2)";
					$status_stmt = "";
					break;
				case PENDING_STATUS:
					// ignore deleted flag completely...
					// since $deleted_stmt is used first to anchor the WHERE clause, there needs
					// to be some expression there, so move queue clause up & blank out $status_stmt
					$deleted_stmt = "(sm.final_status = ".$type.")";
					$status_stmt = "";
					break;
				default:
					// hide 'hold', but show all other deleted types
					$deleted_stmt = "((sm.deleted IS NULL) OR (sm.deleted != 2))";
					$status_stmt = "AND (sm.final_status = ".$type.")";
					break;
			}
		} else {
			// hide deleted messages, where applicable
			switch ($type) {
				case HOLD_STATUS:
					// 'hold' queue is defined by deleted flag, so it always stays the same
					$deleted_stmt = "(sm.deleted = 2)";
					$status_stmt = "";
					break;
				case PENDING_STATUS:
					$deleted_stmt = "((sm.deleted IS NULL) OR (sm.deleted != 1))";
					$status_stmt = "AND (sm.final_status = ".$type.")";
					break;
				default:
					$deleted_stmt = "((sm.deleted IS NULL) OR (sm.deleted NOT IN (1, 2)))";
					$status_stmt = "AND (sm.final_status = ".$type.")";
					break;
			}
		}

		if (isset($focus_id) && !is_null($focus_id) && filter_var($focus_id, FILTER_VALIDATE_INT) && (intval($focus_id) > 0)) {
			$focus_sql = ' (sm.id = '.intval($focus_id).')';
			$immediate_stmt = '';
			$deleted_stmt = '';
			$delete_and_immediate = '';
			$focus_join = 'LEFT';
			$status_stmt = '';
			$exception_sql = '';
			$search_sql = '';
			$lab_name_sql = '';
			$filter_disease_sql = '';
			$filter_clinician_sql = '';
			$filter_mflag_sql = '';
			if (checkPermission(URIGHTS_ADMIN)) {
				$role_wrapper_sql = '';  // if admin user & focus_id set, don't filter by roles -- can accidentally hide orphaned messages
			}
		} else {
			$focus_join = 'INNER';
			$delete_and_immediate = ' AND ';
		}

		if ($type == EXCEPTIONS_STATUS) {
			$sql2 = 'SELECT '.$select_stmt.'
				FROM '.$my_db_schema.'system_messages sm
				INNER JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id)
				LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON ((sm.disease = mv.concept) AND (mv.category = elr.vocab_category_id(\'condition\')))
				LEFT JOIN '.$my_db_schema.'vocab_master_condition mc ON (mv.id = mc.condition)
				LEFT JOIN '.$my_db_schema.'vocab_child_loinc cl ON (
						cl.id IN (COALESCE(
							(
								SELECT MAX(cla.id) FROM '.$my_db_schema.'vocab_child_loinc cla
								WHERE (sm.lab_id = cla.lab_id)
								AND (sm.child_loinc = cla.child_loinc)
								AND (cla.archived IS FALSE)
							),
							(
								SELECT MAX(cla2.id) FROM '.$my_db_schema.'vocab_child_loinc cla2
								WHERE (sm.lab_id = cla2.lab_id)
								AND (sm.child_loinc = cla2.child_loinc)
								AND (cla2.archived IS TRUE)
							)
						))
					)
				LEFT JOIN '.$my_db_schema.'vocab_child_loinc ctc ON (
					ctc.id IN (COALESCE(
							(
								SELECT MAX(ctca.id) FROM '.$my_db_schema.'vocab_child_loinc ctca
								WHERE (sm.lab_id = ctca.lab_id)
								AND (sm.child_loinc = ctca.child_loinc)
								AND (ctca.archived IS FALSE)
							),
							(
								SELECT MAX(ctca2.id) FROM '.$my_db_schema.'vocab_child_loinc ctca2
								WHERE (sm.lab_id = ctca2.lab_id)
								AND (sm.child_loinc = ctca2.child_loinc)
								AND (ctca2.archived IS TRUE)
							)
						))
					)
				LEFT JOIN '.$my_db_schema.'vocab_child_organism co2 ON ((sm.local_result_value_2 = co2.child_code) AND (sm.lab_id = co2.lab_id))
				WHERE
					'.$deleted_stmt.'
					'.$delete_and_immediate.'
					'.$immediate_stmt.'
					'.$status_stmt.'
					'.$filter_disease_sql.'
					'.$filter_mflag_sql.'
					'.$filter_testtype_sql.'
					'.$filter_clinician_sql.'
					'.$search_sql.'
					'.$lab_name_sql.'
					'.$exception_sql.'
					'.$focus_sql.'
				'.$paging_stmt.';';
		} else {
			$sql2 = 'SELECT '.$select_stmt.'
				FROM '.$my_db_schema.'system_messages sm
				INNER JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id)
				LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON ((sm.disease = mv.concept) AND (mv.category = elr.vocab_category_id(\'condition\')))
				LEFT JOIN '.$my_db_schema.'vocab_master_condition mc ON (mv.id = mc.condition)
				'.$focus_join.' JOIN '.$my_db_schema.'vocab_master_loinc ml ON (sm.loinc_code = ml.loinc)
				LEFT JOIN '.$my_db_schema.'vocab_child_loinc cl ON (
						cl.id IN (COALESCE(
							(
								SELECT MAX(cla.id) FROM '.$my_db_schema.'vocab_child_loinc cla
								WHERE (sm.lab_id = cla.lab_id)
								AND (sm.child_loinc = cla.child_loinc)
								AND (cla.archived IS FALSE)
							),
							(
								SELECT MAX(cla2.id) FROM '.$my_db_schema.'vocab_child_loinc cla2
								WHERE (sm.lab_id = cla2.lab_id)
								AND (sm.child_loinc = cla2.child_loinc)
								AND (cla2.archived IS TRUE)
							)
						))
					)
				LEFT JOIN '.$my_db_schema.'vocab_child_loinc ctc ON (
					ctc.id IN (COALESCE(
							(
								SELECT MAX(ctca.id) FROM '.$my_db_schema.'vocab_child_loinc ctca
								WHERE (sm.lab_id = ctca.lab_id)
								AND (sm.child_loinc = ctca.child_loinc)
								AND (ctca.archived IS FALSE)
							),
							(
								SELECT MAX(ctca2.id) FROM '.$my_db_schema.'vocab_child_loinc ctca2
								WHERE (sm.lab_id = ctca2.lab_id)
								AND (sm.child_loinc = ctca2.child_loinc)
								AND (ctca2.archived IS TRUE)
							)
						))
					)
				LEFT JOIN '.$my_db_schema.'vocab_child_organism co2 ON ((sm.local_result_value_2 = co2.child_code) AND (sm.lab_id = co2.lab_id))
				WHERE
					'.$deleted_stmt.'
                    '.$date_stmt.'
					'.$delete_and_immediate.'
					'.$immediate_stmt.'
					'.$status_stmt.'
					'.$filter_disease_sql.'
					'.$filter_mflag_sql.'
					'.$filter_testtype_sql.'
					'.$filter_clinician_sql.'
					'.$search_sql.'
					'.$lab_name_sql.'
					'.$exception_sql.'
					'.$lab_limiter_sql.'
					'.$focus_sql.'
					'.$automated_stmt.'
					'.$role_wrapper_sql.'
				'.$paging_stmt.';';
		}

		#debug
		if (isset($_GET['oz']) && ($_GET['oz'] == 1)) {
			echo '<pre>';
			echo "SQL: ".$sql2."<br>";
			echo '</pre>';
		}
		//exit;

		if ($count_only) {
			$rowcount = @pg_fetch_result(@pg_query($host_pa, $sql2), 0, "counter");
			return $rowcount;
		} else {
			$rs = @pg_query($host_pa, $sql2);
			if ($rs) {
				$results = getResults($rs, $type);
			}
			return $results;
		}
	}




	/**
	 * Checks to see if a specified message has been moved.
	 * If so, returns an HTML element with the name of the previous queue and the
	 * comments left by the user who moved the message.  Otherwise, returns empty string.
	 *
	 * @param int $system_message_id ID number of the message in system_messages
	 * @param int $type ID of the EMSA queue that the message is currently in
	 * @return string
	 */
	function showMessageMoveReason($system_message_id, $type) {
		global $host_pa, $my_db_schema;

		$move_html = '';

		if (!filter_var($system_message_id, FILTER_VALIDATE_INT) || !filter_var($type, FILTER_VALIDATE_INT)) {
			return $move_html;
		}

		$sql = 'SELECT au.info AS reason, ss.name AS queue FROM '.$my_db_schema.'system_messages_audits au
				INNER JOIN '.$my_db_schema.'system_statuses ss ON (au.system_status_id = ss.id)
				WHERE au.message_action_id in (25, 27) AND au.system_message_id = '.intval($system_message_id).'
				ORDER BY au.created_at DESC;';
		$rs = @pg_query($host_pa, $sql);
		if ($rs === false || @pg_num_rows($rs) === 0) {
			@pg_free_result($rs);
		} else {
			$old_queue = @pg_fetch_result($rs, 0, 'queue');
			$move_reason = @pg_fetch_result($rs, 0, 'reason');
			@pg_free_result($rs);
			$move_html =  '<div style="position: relative; display: block; left: '.(($type == EXCEPTIONS_STATUS || $type == PENDING_STATUS) ? '45' : '13').'%; color: darkred; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;">
				<label>Message moved from \''.htmlentities($old_queue, ENT_QUOTES, 'UTF-8').'\'</label> ['.htmlentities($move_reason, ENT_QUOTES, 'UTF-8').']
			</div>';
		}

		return $move_html;
	}




	/**
	 * Return the Queue ID ('final_status') for a message by System Message ID
	 *
	 * Returns Queue ID on success or ENTRY_STATUS if no queue specified/message not found.
	 *
	 * @param int $system_message_id ID number of the message in system_messages
	 * @return mixed
	 */
	function getMessageQueueById($system_message_id) {
		global $host_pa, $my_db_schema;

		if (!filter_var($system_message_id, FILTER_VALIDATE_INT)) {
			return ENTRY_STATUS;
		}

		$qry = 'SELECT final_status FROM '.$my_db_schema.'system_messages WHERE id = '.intval($system_message_id).' AND final_status IS NOT NULL;';
		$rs = @pg_query($host_pa, $qry);
		if ($rs === false) {
			return ENTRY_STATUS;
		} else {
			return intval(@pg_fetch_result($rs, 0, 'final_status'));
		}
	}




	function getQueueNameMenuByTypeAndMsgId($type = null, $system_message_id = null, $is_people_search = false) {
		global $host_pa, $my_db_schema;

		if ($is_people_search) {
			$id_prefix = 'cmr_';
		} else {
			$id_prefix = 'system_';
		}

		if (is_null($type) || empty($type) || is_null($system_message_id) || empty($system_message_id)) {
			$html = '<select class="ui-corner-all" name="system_status_id_'.intval($system_message_id).'" id="'.$id_prefix.'status_id_'.intval($system_message_id).'">';
			$html .= '</select>';
			return $html;
		}

		if (in_array($type, array(EXCEPTIONS_STATUS, PENDING_STATUS)) && wasMessageMovedFromAssigned(intval($system_message_id))) {
			$sql = 'SELECT id, name FROM '.$my_db_schema.'system_statuses WHERE parent_id=0 AND id NOT IN ('.PENDING_STATUS.', '.BLACK_STATUS.', '.QA_STATUS.', '.intval($type).');';
		} else {
			$sql = 'SELECT id, name FROM '.$my_db_schema.'system_statuses WHERE parent_id=0 AND id NOT IN ('.ASSIGNED_STATUS.', '.PENDING_STATUS.', '.BLACK_STATUS.', '.QA_STATUS.', '.intval($type).');';
		}

		$rs = @pg_query($host_pa, $sql);

		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			$html = '<select class="ui-corner-all" name="system_status_id_'.intval($system_message_id).'" id="'.$id_prefix.'status_id_'.intval($system_message_id).'">';
			$html .= '<option value="-1">Choose:</option>';

			while ($row = pg_fetch_object($rs)) {
				if (intval($type)) {
					if (intval($type) == $row->id) {
						$sel = 'selected';
					} else {
						$sel = '';
					}
				} else {
					$sel = '';
				}

				$html .= '<option value="'.$row->id.'" '.$sel.'>'.$row->name.'</option>';
			}

			@pg_free_result($rs);
			$html .= "</select>";
			return $html;
		} else {
			$html = '<select class="ui-corner-all" name="system_status_id_'.intval($system_message_id).'" id="'.$id_prefix.'status_id_'.intval($system_message_id).'">';
			$html .= '</select>';
			return $html;
		}
	}




	function wasMessageMovedFromAssigned($system_message_id = null) {
		global $host_pa, $my_db_schema;

		if (is_null($system_message_id) || empty($system_message_id)) {
			return false;
		}

		$sql = 'SELECT id FROM '.$my_db_schema.'system_messages_audits WHERE system_message_id = '.intval($system_message_id).' AND message_action_id = 25 AND system_status_id = 14;';
		$rs = @pg_query($host_pa, $sql);
		if (($rs !== false) && (@pg_num_rows($rs) > 0)) {
			@pg_free_result($rs);
			return true;
		} else {
			@pg_free_result($rs);
			return false;
		}
	}




	function getEmsaDetail($id) {
		$start_time = microtime(true);
		global $host_pa, $my_db_schema;

		$result = array();
		$sql = 'SELECT
					sm.message_flags,
					sm.id AS id,
					sm.event_id,
					sm.address_is_valid,
					sm.event_record_id,
					sm.master_xml,
					sm.loinc_code,
					sm.lab_id AS lab_id,
					om.message
				FROM '.$my_db_schema.'system_messages sm
				INNER JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id)
				WHERE sm.id = '.$id.';';

		$db_results = @pg_query($host_pa, $sql);

		#debug echo 'query done... '.round(abs(microtime(true)-$start_time), 5).' / ';

		if (($db_results !== false) && (@pg_num_rows($db_results) > 0)) {
			$res = @pg_fetch_object($db_results);

			$result['flags'] = array(
				'investigation_complete' => ((($res->message_flags & EMSA_FLAG_INVESTIGATION_COMPLETE) != 0) ? true : false),
				'de_error' => ((($res->message_flags & EMSA_FLAG_DE_ERROR) != 0) ? true : false),
				'fix_duplicate' => ((($res->message_flags & EMSA_FLAG_FIX_DUPLICATE) != 0) ? true : false),
				'de_other' => ((($res->message_flags & EMSA_FLAG_DE_OTHER) != 0) ? true : false),
				'clean_data' => ((($res->message_flags & EMSA_FLAG_CLEAN_DATA) != 0) ? true : false),
				'qa_mandatory_fields' => ((($res->message_flags & EMSA_FLAG_QA_MANDATORY) != 0) ? true : false),
				'qa_vocab_coding' => ((($res->message_flags & EMSA_FLAG_QA_CODING) != 0) ? true : false),
				'need_fix' => ((($res->message_flags & EMSA_FLAG_DE_NEEDFIX) != 0) ? true : false),
				'qa_mqf' => ((($res->message_flags & EMSA_FLAG_QA_MQF) != 0) ? true : false)
			);

			//$result['hl7'] = str_replace("\\015", PHP_EOL, $res->message);
			$result['record_id'] = $res->id;
			$result['event_id'] = $res->event_id;
			$result['address_is_valid'] = $res->address_is_valid;
			$result['event_record_id'] = $res->event_record_id;

			// pull the CLIA out of the original message
			$result['clia'] = '';
			if(preg_match('/\^([^\^]+)\^CLIA/', $res->message, $matches)) {
				$result['clia'] = $matches[1];
			}
			//$result['master_xml'] = urldecode($res->master_xml);

			#debug echo round(abs(microtime(true)-$start_time), 5).' / ';
			$xmlparse = @simplexml_load_string($res->master_xml);
			#debug echo round(abs(microtime(true)-$start_time), 5).' / ';

			if (count($xmlparse) > 0) {
				$result['fname'] = $xmlparse->person->first_name;
				$result['lname'] = $xmlparse->person->last_name;
				$result['mname'] = $xmlparse->person->middle_name;
				$result['street'] = $xmlparse->person->street_name;
				$result['unit'] = $xmlparse->person->unit;
				$result['city'] = $xmlparse->person->city;
				$result['state'] = $xmlparse->person->state;
				$result['zip'] = $xmlparse->person->zip;
				$result['full_name'] = $xmlparse->person->last_name.', '.$xmlparse->person->first_name.((isset($xmlparse->person->middle_name) && !empty($xmlparse->person->middle_name)) ? ' '.$xmlparse->person->middle_name : '');
				$result['patient_phone'] = ((strlen($xmlparse->person->phone) > 0) ? $xmlparse->person->area_code.'-'.substr($xmlparse->person->phone, 0, 3).'-'.substr($xmlparse->person->phone, 3) : '');

				if (!is_array($xmlparse->person->date_of_birth) && strlen($xmlparse->person->date_of_birth) > 5) {
					$result['birth_date'] = date("m/d/Y", strtotime($xmlparse->person->date_of_birth));
					$result['bday'] = date("Y-m-d", strtotime($xmlparse->person->date_of_birth));
					$result['age'] = birthday($result['bday']);
				} else {
					$result['birth_date'] = '';
					$result['bday'] = '';
					$result['age'] = '';
				}

				if (strlen($xmlparse->administrative->jurisdictionId) > 0) {
					$result['jurisdiction'] = lhdName($xmlparse->administrative->jurisdictionId);
				} else {
					$result['jurisdiction'] = '<strong style="color: red;">[Not Set]</strong>';
				}

				$result['created_at'] = $created_at;
				$result['disease'] = $xmlparse->disease->name;  //diseaseByLoinc($res->loinc_code);

				//lab stuff
				$result['test_type'] = $xmlparse->labs->test_type;
				$result['organism'] = $xmlparse->labs->organism;
				$result['test_result'] = decodeTrisanoCodedValue($xmlparse->labs->test_result, 'external_codes', 'test_result');
				$result['loinc_code'] = $res->loinc_code;
				$result['units'] = $xmlparse->labs->units;
				$result['result_value'] = $xmlparse->labs->result_value;
				$result['comment'] = $xmlparse->labs->comment;
				$result['abnormal_flag'] = decodeAbnormalFlag(trim($xmlparse->labs->abnormal_flag));

				$result['sex'] = getMasterConceptFromChildCode($xmlparse->person->gender, 'gender', $res->lab_id);
				$result['ethnicity'] = getMasterConceptFromChildCode($xmlparse->person->ethnicity, 'ethnicity', $res->lab_id);
				$result['race'] = getMasterConceptFromChildCode($xmlparse->person->race, 'race', $res->lab_id);
				$result['local_test_status'] = getMasterConceptFromChildCode($xmlparse->labs->test_status, 'test_status', $res->lab_id);
				// translated test status duplicates local test status
				// currently, master xml does not display NEDSS-converted test status value
				$result['test_status'] = getMasterConceptFromChildCode($xmlparse->labs->test_status, 'test_status', $res->lab_id);

				$result['local_loinc_code'] = $xmlparse->labs->local_loinc_code;
				$result['local_test_name'] = $xmlparse->labs->local_test_name;
				$result['local_code'] = $xmlparse->labs->local_code;
				$result['local_code_test_name'] = $xmlparse->labs->local_code_test_name;
				$result['local_units'] = $xmlparse->labs->local_result_unit;
				$result['local_result_value'] = $xmlparse->labs->local_result_value;
				$result['local_result_value_2'] = $xmlparse->labs->local_result_value_2;

				$result['medical_record'] = $xmlparse->hospital_info->medical_record;
				$result['hospitalized'] = decodeTrisanoCodedValue($xmlparse->hospital_info->hospitalized, 'external_codes', 'yesno');
				$result['clinician'] = $xmlparse->clinicians->last_name.', '.$xmlparse->clinicians->first_name.' '.$xmlparse->clinicians->middle_name;
				$result['clinician_phone'] = ((strlen(trim($xmlparse->clinicians->area_code)) > 0) ? '('.$xmlparse->clinicians->area_code.') ' : '').$xmlparse->clinicians->phone.((strlen(trim($xmlparse->clinicians->extension)) > 0) ? 'Ext. '.$xmlparse->clinicians->extension : '');
				$result['diagnostic'] = $xmlparse->diagnostic->name;
				$result['diagnostic_street'] = $xmlparse->diagnostic->street_name;
				$result['diagnostic_unit'] = $xmlparse->diagnostic->unit_number;
				$result['diagnostic_city'] = $xmlparse->diagnostic->city;
				$result['diagnostic_state'] = $xmlparse->diagnostic->state;
				$result['diagnostic_zip'] = $xmlparse->diagnostic->zipcode;
				$result['reporting'] = $xmlparse->reporting->agency;
				//$result['reporting_verbose'] = getLabNameByHL7Name($xmlparse->reporting->agency);
				$result['lab'] = $xmlparse->labs->lab;
				$result['lab_id'] = intval($res->lab_id);
				//$result['lab_verbose'] = getLabNameByHL7Name($xmlparse->labs->lab);

				if (strlen($xmlparse->reporting->report_date) > 0) {
					$rdate = date("m/d/Y", strtotime($xmlparse->reporting->report_date));
				} else {
					$rdate = '';
				}
				$result['report_date'] = $rdate;

				if (strlen($xmlparse->labs->reference_range) > 0) {
					$result['local_reference_range'] = '<span style="color: dimgray;">'.$xmlparse->labs->local_reference_range.'</span>';
					$result['reference_range'] = '<span style="color: dimgray;">'.$xmlparse->labs->reference_range.'</span>';
				} elseif (strlen($xmlparse->labs->local_reference_range) > 0){
					$result['local_reference_range'] = '<span style="color: dimgray;">'.$xmlparse->labs->local_reference_range.'</span>';
					$result['reference_range'] = '<span style="color: dimgray;">'.$xmlparse->labs->local_reference_range.'</span>';
				} else {
					$result['local_reference_range'] = '';
					$result['reference_range'] = '';
				}

				$result['specimen_source'] = decodeTrisanoCodedValue($xmlparse->labs->specimen_source, 'external_codes', 'specimen');
				$result['local_specimen_source'] = getMasterConceptFromChildCode($xmlparse->labs->local_specimen_source, 'specimen', $res->lab_id);

				if (!is_array($xmlparse->labs->collection_date) && strlen($xmlparse->labs->collection_date) > 0) {
					$cdate = date("m/d/Y", strtotime($xmlparse->labs->collection_date));
				} else {
					$cdate = '';
				}
				$result['collection_date'] = $cdate;

				if (!is_array($xmlparse->labs->lab_test_date) && strlen($xmlparse->labs->lab_test_date) > 0) {
					$ldate = date("m/d/Y", strtotime($xmlparse->labs->lab_test_date));
				} else {
					$ldate = '';
				}
				$result['lab_test_date'] = $ldate;

				$result['accession_number'] = $xmlparse->labs->accession_number;
				$result['state_case_status'] = decodeTrisanoCodedValue($xmlparse->labs->state_case_status, 'external_codes', 'case');
			}
		}
		#debug echo round(abs(microtime(true)-$start_time), 5).'<br>';
		return $result;
	}




	function birthday($birthday) {
		if (strlen($birthday) > 0) {
			$now = new DateTime();
			try {
				$dob = new DateTime($birthday.' 00:00:00');
			} catch (Exception $e) {
				return 'N/A';
			}
			$diff = $now->diff($dob);
			return $diff->y;
		} else {
			return 'N/A';
		}
	}




	function displayAuditLogById($audit_record_id) {
		global $host_pa, $my_db_schema;

		$sql = sprintf("SELECT au.user_id AS user_id, ac.message AS action, au.info as info, ca.name AS category, au.created_at AS created_at, ss.name AS status, xa.id AS xml_audit_id, xa.previous_xml AS previous_xml, xa.sent_xml AS sent_xml
				FROM %ssystem_messages_audits au
				JOIN %ssystem_message_actions ac ON (au.message_action_id = ac.id)
				JOIN %ssystem_action_categories ca ON (ac.action_category_id = ca.id)
				JOIN %ssystem_statuses ss ON (au.system_status_id = ss.id)
				LEFT JOIN %ssystem_nedss_xml_audits xa ON (xa.system_messages_audits_id = au.id)
				WHERE au.system_message_id = %s
				ORDER BY au.created_at;",
			$my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, $my_db_schema, intval($audit_record_id));
		$rs = @pg_query($host_pa, $sql);
		unset($html);
		$html = "<table class=\"audit_log\"><thead><tr><th>Date/Time</th><th>User</th><th>Event Category</th><th>Event Action</th><th>Event Status</th><th>XML Before Changes</th><th>XML Changes Sent</th></tr></thead><tbody>";
		if (pg_num_rows($rs) > 0) {
			while ($row = @pg_fetch_object($rs)) {
				$html .= sprintf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s%s</td><td>%s</td><td>%s</td><td>%s</td></tr>",
					date("m/d/Y H:i:s", strtotime($row->created_at)),
					userRealNameByUmdid($row->user_id),
					htmlentities($row->category, ENT_QUOTES, "UTF-8"),
					htmlentities($row->action, ENT_QUOTES, "UTF-8"),
					((strlen(trim($row->info)) > 0) ? '<br>Comments: '.htmlentities($row->info, ENT_QUOTES, "UTF-8") : ''),
					htmlentities($row->status, ENT_QUOTES, "UTF-8"),
					((!is_null($row->previous_xml) && !empty($row->previous_xml)) ? '<button title="View event XML before changes" class="audit_view_xml" value=\''.json_encode(array('type' => 1, 'id' => intval($row->xml_audit_id))).'\'>View</button>' : '--'),
					((!is_null($row->sent_xml) && !empty($row->sent_xml)) ? '<button title="View XML changes sent" class="audit_view_xml" value=\''.json_encode(array('type' => 2, 'id' => intval($row->xml_audit_id))).'\'>View</button>' : '--'));
			}
		} else {
			$html .= "<tr><td colspan=\"5\"><em>No events logged</em></td></tr>";
		}
		$html .= "</tbody></table>";
		@pg_free_result($rs);
		return $html;
	}




	function displayQACommentsById($system_message_id) {
		global $host_pa, $my_db_schema;

		$sql = 'SELECT mc.user_id AS user_id, mc.comment AS comment, mc.created_at AS created_at
				FROM '.$my_db_schema.'system_message_comments mc
				WHERE mc.system_message_id = '.intval($system_message_id).'
				ORDER BY mc.created_at;';
		$rs = @pg_query($host_pa, $sql);
		unset($html);
		$html = "<table class=\"audit_log\"><thead><tr><th style=\"width: 15%\">Date/Time</th><th style=\"width: 15%\">User</th><th style=\"width: 70%\">Comment</th></tr></thead><tbody>";
		if (pg_num_rows($rs) > 0) {
			while ($row = @pg_fetch_object($rs)) {
				$html .= sprintf("<tr><td>%s</td><td>%s</td><td>%s</td></tr>",
					date("m/d/Y H:i:s", strtotime($row->created_at)),
					userRealNameByUmdid($row->user_id),
					htmlentities($row->comment, ENT_QUOTES, "UTF-8"));
			}
		} else {
			$html .= "<tr><td colspan=\"3\"><em>No comments</em></td></tr>";
		}
		$html .= "</tbody></table>";
		@pg_free_result($rs);
		return $html;
	}




	/**
	 * Returns the UI name of a lab corresponding to the HL7 value for lab name.
	 *
	 * @param string $hl7_name Lab identifier from HL7
	 * @return string UI name of the facility
	 */
	function getLabNameByHL7Name($hl7_name) {
		global $host_pa, $my_db_schema;

		$lab_name = '';

		$sql = 'SELECT ui_name
				FROM '.$my_db_schema.'structure_labs
				WHERE hl7_name = \''.trim($hl7_name).'\';';
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false) {
			$lab_name = trim(@pg_fetch_result($rs, 0, 'ui_name'));
		}

		return $lab_name;

	}




	/**
	 * Calculate whether an ELR message is within 24-hour Service Level for being processed from Entry queue.
	 * Calculation should only include business days (Mon-Fri), but 24 real hours during those days.  For example:
	 *   - Received at noon on Monday, due by noon Tuesday
	 *   - Recieved at noon on Friday, due by noon Monday
	 * If received over the weekend, adjust to start "overdue" countdown Monday at 8:00 a.m.  For example:
	 *   - Received on Saturday, due by 8:00 a.m. Tuesday
	 *
	 * @param string $created_at Date the ELR message was received, in format accepted by strtotime().
	 * @return bool
	 */
	function isElrMessageOverdue($created_at = null) {
		if (!is_null($created_at)) {
			$business_days = array(
				0 => "Monday",
				1 => "Tuesday",
				2 => "Wednesday",
				3 => "Thursday",
				4 => "Friday");

			if ($created_time = strtotime($created_at)) {
				$created_parsed = getdate($created_time);
				$created_year = intval($created_parsed['year']);
				$created_wday = intval($created_parsed['wday']);

				if ($created_year < 1970) {
					return false;
				}

				if (($created_wday == 0) || ($created_wday == 6)) {
					// if Sunday or Saturday, adjust timestamp to Monday 8am
					$adjusted_time_tmp = strtotime("next Monday 8:00 am", $created_time);
					$adjusted_time = mktime(
						date("H", $adjusted_time_tmp), date("i", $adjusted_time_tmp), date("s", $adjusted_time_tmp),
						date("n", $adjusted_time_tmp), date("j", $adjusted_time_tmp), date("Y", $adjusted_time_tmp));
				} else {
					// otherwise, use existing timestamp
					$adjusted_time = $created_time;
				}

				/**
				 * 24-hour service level
				 * If SLA change is needed, adjust here
				 */
				$overdue_time = strtotime("+1 day", $adjusted_time);
				$overdue_parsed = getdate($overdue_time);
				$overdue_wday = intval($overdue_parsed['wday']);
				if (($overdue_wday == 0) || ($overdue_wday == 6)) {
					// will come due over the weekend, move to be overdue same time next Monday
					$overdue_adjusted_tmp = strtotime("next Monday", $overdue_time);
					$overdue_adjusted = mktime(
						date("H", $overdue_time), date("i", $overdue_time), date("s", $overdue_time),
						date("n", $overdue_adjusted_tmp), date("j", $overdue_adjusted_tmp), date("Y", $overdue_adjusted_tmp));
				} else {
					$overdue_adjusted = $overdue_time;
				}

				if ($overdue_adjusted < time()) {
					// we've passed the due date
					return true;
					#debug return date(DATE_W3C, $overdue_adjusted);
				} else {
					// still within service level
					return false;
					#debug return date(DATE_W3C, $overdue_adjusted);
				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	}




	/**
	 * Get People Search results based on the current message's patient information.
	 *
	 * Returns an array of results, or FALSE on error.
	 *
	 * @param string $fname Patient first name
	 * @param string $lname Patient last name
	 * @param string $dob Patient DOB (yyyy-mm-dd formatted)
	 * @param string $condition_names Limit results to the specified disease name (and gateway conditions, if applicable).  Must match NEDSS disease name. [Optional]
	 * @param int $match_score_threshhold Only return matches with this score or higher (0-100; 0 = no match, 100 = exact match; default 70)
	 * @return mixed
	 */
	function getPeopleSearchResults($fname = null, $lname = null, $dob = null, $condition_name = null, $match_score_threshhold = 70) {
		if (is_null($fname) || is_null($lname) || is_null($dob)) {
			throw new Exception('Missing required fields for People Search');
		}
		global $props;
//error_log('TODO Jay >>>> getPeopleSearchResults	first: '.$fname.' last: '.$lname.' DOB: '.$dob. " condition:".$condition_name);
		$match_score_clean = (((filter_var(trim($match_score_threshhold), FILTER_VALIDATE_INT) !== false) && (intval($match_score_threshhold) >= 0) && (intval($match_score_threshhold) <= 100)) ? intval($match_score_threshhold) : 70);

		$conditions_clean = array();
		$condition_xml = '';

		if (!is_null($condition_name)) {
			$condition_clean = ((strlen(trim($condition_name)) > 0) ? trim($condition_name) : null);
		}
		if (isset($condition_clean) && !empty($condition_clean)) {
			$condition_xml .= '<diseases><disease_name>'.htmlspecialchars($condition_clean).'</disease_name></diseases>'.PHP_EOL;
			$gateway_conditions = gatewayCrossrefNamesByName($condition_clean);
//error_log('TODO >>>> getPeopleSearchResults	condition: '.$condition_clean.' xrefs: '.print_r($gateway_conditions, true));
		}

		if (isset($gateway_conditions) && is_array($gateway_conditions) && (count($gateway_conditions) > 0)) {
			foreach ($gateway_conditions as $gateway_condition) {
				$condition_xml .= '<diseases><disease_name>'.htmlspecialchars($gateway_condition).'</disease_name></diseases>'.PHP_EOL;
			}
		}

		ini_set('default_socket_timeout', '120');

		$xml = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <system>TRISANO</system>
    <trisano_health>
        <interested_party_attributes>
            <person>
                <first_name>$fname</first_name>
                <last_name>$lname</last_name>
                <birth_date>$dob</birth_date>
				<match_score>$match_score_clean</match_score>
            </person>
        </interested_party_attributes>
		$condition_xml
    </trisano_health>
</health_message>
EOX;
		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			throw new Exception('Unable to create SOAP client');
		} catch (SoapFault $f) {
			throw new Exception('Unable to create SOAP client');
		}

		if ($client) {
			try {
				$result = $client->searchPerson(array('healthMessage' => $xml));
			} catch (Exception $e) {
				throw new Exception('Server error/timeout');
			} catch (SoapFault $f) {
				throw new Exception('Server error/timeout');
			}
			$return = simplexml_load_string($result->return);

			foreach($return->trisano_health as $th_obj) {
				unset($this_person_events);
				foreach($th_obj->events as $event) {
					// if (in_array(trim($event->type), array('MorbidityEvent', 'ContactEvent'))) {
					if ((trim($event->type) == 'MorbidityEvent') || (trim($event->type) == 'AssessmentEvent') ||
						((trim($event->type) == 'ContactEvent') && isset($th_obj->interested_party_attributes->person->birth_date) && !empty($th_obj->interested_party_attributes->person->birth_date))) {
						$this_person_events[] = intval($event->id);
						$event_type_t = 'Contact Event';
						if(trim($event->type) == 'MorbidityEvent') {
							$event_type_t = 'Morbidity Event';
						} else if(trim($event->type) == 'AssessmentEvent') {
							$event_type_t = 'Assessment Event';
						}
                        $disease_id_t = intval(trim(@reset($th_obj->xpath('disease_events[event_id=\''.intval($event->id).'\']/disease_id'))));
						$events_by_person_id[intval($th_obj->interested_party_attributes->person->id)][] = array(
							'event_type' => $event_type_t,
							'event_id' => intval($event->id),
							'record_number' => trim($event->record_number),
							'event_date' => date("m/d/Y", strtotime($event->first_reported_PH_date)),
							'disease_id' => $disease_id_t,
							'disease_name' => trim(@reset($th_obj->xpath('diseases[id=\''.$disease_id_t.'\']/disease_name')))
						);
					}
				}

				if (isset($this_person_events) && is_array($this_person_events) && (count($this_person_events) > 0)) {
					unset($data);
					$data = array();
					$data['full_name'] = $th_obj->interested_party_attributes->person->last_name .', '.$th_obj->interested_party_attributes->person->first_name .' '.$th_obj->interested_party_attributes->person->middle_name;
					$this_dob = ((isset($th_obj->interested_party_attributes->person->birth_date) && !is_null($th_obj->interested_party_attributes->person->birth_date)) ? strtotime($th_obj->interested_party_attributes->person->birth_date) : null);
					$data['birth_date'] = ((!is_null($this_dob)) ? date('m/d/Y', $this_dob) : null);
					$data['sex'] = $_SESSION['trisano_codes']['external_codes']['gender'][intval($th_obj->interested_party_attributes->person->birth_gender_id)]['code_description'];
					$data['match_score'] = $th_obj->interested_party_attributes->person->match_score;
					$data['real_score'] = 0.0;
					foreach ($th_obj->addresses as $th_obj_address_item) {
						// for each address present, concat all the elements together & hash it, compare against any previously generated hashes for this person_id
						// only show distinct addresses (case-insensitive)
						unset($this_address_hash);
						unset($this_address_cat);
						// only show addresses associated with CMRs...
						if (isset($th_obj_address_item->event_id) && in_array(intval($th_obj_address_item->event_id), $this_person_events)) {
							$this_address_cat = trim($th_obj_address_item->street_name).trim($th_obj_address_item->unit_number).trim($th_obj_address_item->city).trim($th_obj_address_item->postal_code);
							if (strlen($this_address_cat) > 0) {
								$this_address_hash = md5(strtoupper($this_address_cat));
								if (!isset($result_addresses[intval($th_obj->interested_party_attributes->person->id)][$this_address_hash])) {
									$result_addresses[intval($th_obj->interested_party_attributes->person->id)][$this_address_hash] = true;
									$data['addresses'][] = $th_obj_address_item;
								}
							}
						}
					}

					$telephone_items_found = array();
					$telephone_items = array();
					foreach ($th_obj->interested_party_attributes->telephones as $telephone_item) {
						unset($this_telephone_cat);
						$this_telephone_cat = $telephone_item->area_code.$telephone_item->phone_number;
						if (!in_array($this_telephone_cat, $telephone_items_found) && (strlen($telephone_item->phone_number) > 0)) {
							$telephone_items_found[] = $this_telephone_cat;
							$data['telephones'][] = ((strlen($telephone_item->phone_number) > 0) ? $telephone_item->area_code.((strlen($telephone_item->area_code) > 0) ? '-' : '').substr($telephone_item->phone_number, 0, 3)."-".substr($telephone_item->phone_number, 3) : "");
						}
					}

					//$data['telephones'] = getPeopleSearchTelephoneNumbers(intval($th_obj->interested_party_attributes->person->id)); // placeholder -- need function

					$results[intval($th_obj->interested_party_attributes->person->id)] = $data;
				}
			}

			$five_star = array();
			$four_star = array();
			$sub_four_star = array();

			if (is_array($results)) {
				foreach ($results as $result_preproccess_key => $result_preproccess_item) {
					// no longer need to calculate score here... should be getting done in TriSano
					// calculate scores for all found people
					//unset($temp_score);
					unset($this_score);
//error_log("TODO Jay results: ".print_r($result_preproccess_item, true));
					//$temp_score = floatval( ( (strlen($fname)+strlen($lname)+strlen($dob)-intval($result_preproccess_item['match_score'])) / (strlen($fname)+strlen($lname)+strlen($dob)) ) * 100 );
					//$this_score = (is_null($result_preproccess_item['birth_date'])) ? $temp_score-10.0 : $temp_score;  // no DOB in TriSano, reduce score by 10% (half a star);
					$this_score = floatval($result_preproccess_item['match_score']);


					$results[$result_preproccess_key]['real_score'] = $this_score;

					if ($this_score == 100.0) {
						$five_star[] = $result_preproccess_key;
					} elseif ($this_score >= 80.0) {
						$four_star[] = $result_preproccess_key;
					} else {
						$sub_four_star[] = $result_preproccess_key;
					}
				}

				uasort($results, 'peopleSearchSortByRealScore');  // ensure list is sorted in the correct order, also fall back to name sort if equal scores
				return array(
					'results' => $results,
					'events_by_person_id' => $events_by_person_id,
					'five_star' => $five_star,
					'four_star' => $four_star,
					'sub_four_star' => $sub_four_star
				);
			} else {
				// no results found
				return array(
					'results' => array(),
					'events_by_person_id' => array(),
					'five_star' => $five_star,
					'four_star' => $four_star,
					'sub_four_star' => $sub_four_star
				);
			}
		} else {
			throw new Exception('Unable to establish SOAP connection to web service');
		}
	}




	/**
	 * Sorts People Search Results based on calculated 'Real Score' instead of TriSano-returned Match Score
	 *
	 * @param array $a
	 * @param array $b
	 * @return bool
	 */
	function peopleSearchSortByRealScore($a, $b) {
		if ($a['real_score'] == $b['real_score']) {
			return strcasecmp($a['full_name'], $b['full_name']);
		} else {
			return ($a['real_score'] > $b['real_score']) ? -1 : 1;
		}
	}




	/**
	 * Returns the name of an EMSA message queue ID.
	 *
	 * @param int $type EMSA message queue ID
	 * @return string Name of the EMSA queue.
	 */
	function getEmsaQueueName($type) {
		$type_name = '';

		switch($type) {
			case PENDING_STATUS:
				$type_name = PENDING_NAME;
				break;
			case ENTRY_STATUS:
				$type_name = ENTRY_NAME;
				break;
			case ASSIGNED_STATUS:
				$type_name = ASSIGNED_NAME;
				break;
			case GRAY_STATUS:
				$type_name=GRAY_NAME;
				break;
			case BLACK_STATUS:
				$type_name = BLACK_NAME;
				break;
			case EXCEPTIONS_STATUS:
				$type_name = EXCEPTIONS_NAME;
				break;
			case HOLD_STATUS:
				$type_name = 'HOLDING STATUS';
				break;
			case REVIEW_STATUS:
				$type_name = REVIEW_NAME;
				break;
			case QA_STATUS:
				$type_name = QA_NAME;
				break;
			default:
				$type_name = PENDING_NAME;
				break;
		}

		return $type_name;
	}




	/**
	 * Returns a formatted XML version of an ELR message's Master or NEDSS XML for a specified message ID.
	 * Can also retrieve a message's HL7 text.
	 *
	 * @param int $id System Message ID for the message to retrieve.
	 * @param int $type Field identifier (1 = NEDSS XML, 4 = Master XML, 5 = HL7 Text; Default Master XML)
	 * @return string Formatted XML/HL7 string.  Returns empty string if no message found.
	 */
	function getMessageFieldFormatted($id = null, $type = 4) {
		global $host_pa, $my_db_schema;

		if (empty($id)) {
			return '';
		}

		switch(intval($type)) {
			case 1:
				$field = 'sm.transformed_xml';
				$is_xml = true;
				break;
			case 4:
				$field = 'sm.master_xml';
				$is_xml = true;
				break;
			case 5:
				$field = 'om.message';
				$is_xml = false;
				break;
			default:
				$field = false;
				$is_xml = false;
				break;
		}

		$sql = null;
		if ($field !== false) {
			if ($is_xml) {
				$sql = 'SELECT '.$field.' AS value FROM '.$my_db_schema.'system_messages sm WHERE sm.id = '.intval($id).';';
			} else {
				$sql = 'SELECT '.$field.' AS value FROM '.$my_db_schema.'system_messages sm INNER JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id) WHERE sm.id = '.intval($id).';';
			}
		}

		$html = '';
		if (!empty($sql)) {
			$rs = @pg_query($host_pa, $sql);
			if (($rs !== false) && (count(@pg_num_rows($rs) > 0))) {
				$html = @pg_fetch_result($rs, 0, 'value');
			}
			@pg_free_result($rs);
		}

		if ($is_xml) {
			if (strlen($html) > 0) {
				$dom = new DOMDocument("1.0");
				$dom->preserveWhiteSpace = false;
				$dom->formatOutput = true;
				$dom->loadXML($html);
				return $dom->saveXML();
			} else {
				return '';
			}
		} else {
			if (strlen($html) > 0) {
				return str_replace("\\015", PHP_EOL, $html);
			} else {
				return '';
			}
		}
	}




	/**
	 * Delete an ELR message by ID.
	 *
	 * @param int $message_id System message ID of the message to delete.
	 * @return bool
	 */
	function deleteMessageById($message_id) {
		global $host_pa, $my_db_schema;

		$message = false;

		if (intval($message_id) > 0) {
			$sql = 'UPDATE '.$my_db_schema.'system_messages SET deleted = 1 WHERE id = '.intval($message_id).';';
			$rs = @pg_query($host_pa, $sql);
			if ($rs !== false) {
				$message = true;
			}
			@pg_free_result($rs);
		}
		return $message;
	}

	/**
	 * return true if the value of a property is true.
	 *
	 * @param string $property Property from the property file.
	 * @return bool
	 */
	function isPropertyTrue($property) {
		return isset($property) && (strtolower(substr($property, 0, 1)) == 't');
	}

?>
