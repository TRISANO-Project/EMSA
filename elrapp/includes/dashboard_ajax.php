<?php
	
	// prevent caching...
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');
	
	include 'app_config.php';
	require WEBROOT_URL.'/includes/dashboard_functions.php';
	
	session_write_close(); // done writing to session; prevent blocking
	
	$valid_callbacks = array(
		'getOrphanedMessageCount' => false, 
		'getDashboardSummary' => true, 
		'getAvgCMRCreateTime' => true, 
		'getDashboardNewCase' => true, 
		'getDashboardAppendedCase' => true, 
		'getDashboardDiscardedCase' => true, 
		'getDashboardBlacklistSummary' => true, 
		'getDashboardGraylistSummary' => true, 
		'getDashboardMessageQuality' => true, 
		'getDashboardLab' => true, 
		'getDashboardConditionSummary' => true, 
		'getDashboardAutomationFactor' => true
	);
	
	$callback = ((isset($_REQUEST['callback']) && in_array($_REQUEST['callback'], $valid_callbacks)) ? $_REQUEST['callback'] : null);
	
	if (!is_null($callback)) {
		if ($valid_callbacks[$callback] === true) {
			$data = call_user_func($callback, $_SESSION['dashboard_date_from'], $_SESSION['dashboard_date_to']);
		} else {
			$data = call_user_func($callback);
		}
	} else {
		$data = array();
	}
	
	echo @json_encode($data);
	exit;
	
?>