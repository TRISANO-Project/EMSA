<?php

	/**
	 * For a specified master vocab ID, checks to ensure that a row exists in vocab_master2app for all applications 
	 * prior to a vocabulary update so that adding a previously-blank field will properly save
	 *
	 * @param int $master_id Master Vocabulary ID
	 * @return void
	 */
	function createMissingMasterToAppVocab($master_id) {
		global $host_pa, $my_db_schema;
		
		$app_sql = 'SELECT id, app_name FROM '.$my_db_schema.'vocab_app ORDER BY id;';
		$app_rs = pg_query($host_pa, $app_sql);
		if ($app_rs !== false) {
			while ($app_row = @pg_fetch_object($app_rs)) {
				$master_app_sql = 'SELECT id FROM '.$my_db_schema.'vocab_master2app WHERE app_id = '.intval($app_row->id).' AND master_id = '.intval($master_id).';';
				$master_app_rs = @pg_query($host_pa, $master_app_sql);
				if ($master_app_rs !== false && @pg_num_rows($master_app_rs) < 1) {
					$insert_sql = 'INSERT INTO '.$my_db_schema.'vocab_master2app (app_id, master_id) VALUES ('.intval($app_row->id).', '.intval($master_id).');';
					@pg_query($host_pa, $insert_sql);
				}
				@pg_free_result($master_app_rs);
			}
			@pg_free_result($app_rs);
		}
	}
	
	
	
	
	/**
	 * Returns a comma-separated string containing the selected values of a specified filter.
	 *
	 * @param array $filter_meta Filter Column data indicating database table/field to query for IDs
	 * @param array $values Array containing the coded selected filter values
	 * @return string
	 */
	function verboseFilterValues($filter_meta, $values) {
		global $host_pa, $my_db_schema;
		
		$filter_string = '';
		$filter_vals = array();
		
		if (is_null($filter_meta) || is_null($values) || !is_array($filter_meta) || !is_array($values) || count($filter_meta) < 1 || count($values) < 1) {
			return $filter_string;
		}
		
		if (is_null($filter_meta['itemlabel'])) {
			return trim(implode(', ', $values));
		}
		
		$sql = 'SELECT '.$filter_meta['itemlabel'].' AS label FROM '.$my_db_schema.$filter_meta['fieldtable'].' WHERE '.$filter_meta['itemval'].' IN ('.implode(', ', $values).');';
		$rs = @pg_query($host_pa, $sql);
		if ($rs === false) {
			return $filter_string;
		} else {
			while ($row = @pg_fetch_object($rs)) {
				$filter_vals[] = trim($row->label);
			}
			@pg_free_result($rs);
		}
		
		$filter_string = implode(', ', $filter_vals);
		return $filter_string;
	}
	
	
	
	
	/**
	 * Converts a list of speciemn source master vocabulary IDs into a list of verbose specimen names.
	 *
	 * Accepts a string containing a semicolon-delimited list of master vocabulary IDs and converts them
	 * to a semicolon-delimited string of verbose specimen sources for display in UI.
	 * 
	 * @param string $specimen_id_list List of master vocabulary IDs, separated by semicolons
	 * @return string|false
	 */
	function specimenIdValues($specimen_id_list) {
		global $host_pa, $my_db_schema;
		
		if (strlen(trim($specimen_id_list)) < 1) {
			return false;
		}
		
		$xref_ids = explode(";", $specimen_id_list);
		if (count($xref_ids) < 1) {
			return false;
		}
		
		foreach ($xref_ids as $xref_id) {
			$sql = sprintf("SELECT concept FROM %svocab_master_vocab WHERE category = elr.vocab_category_id('specimen') AND id = %d", $my_db_schema, intval(trim($xref_id)));
			$text = @pg_fetch_result(@pg_query($host_pa, $sql), 0, "concept");
			if (strlen(trim($text)) > 0) {
				$xref_vals[] = trim($text);
			} else {
				return false;
			}
		}
		
		$xref_vals_string = implode(";", $xref_vals);
		return $xref_vals_string;
	}
	
	
	
	
	/**
	 * Returns the application-specific Coded Value for the specified Master Vocabulary item.
	 * Returns an empty string if no vocabulary item found, no name specified, or an error.
	 *
	 * @param int $master_id Master Vocabulary ID
	 * @param int $app_id Application ID (Optional, Default TriSano)
	 * @return string
	 */
	function appCodedValueByMasterID($master_id = null, $app_id = 1) {
		global $host_pa, $my_db_schema;
		
		if (is_null($master_id) || (intval(trim($master_id)) < 1)) {
			return '';
		}
		
		$qry = "SELECT coded_value FROM ".$my_db_schema."vocab_master2app WHERE app_id = ".filter_var($app_id, FILTER_SANITIZE_NUMBER_INT)." AND master_id = ".filter_var($master_id, FILTER_SANITIZE_NUMBER_INT).";";
		$rs = @pg_query($host_pa, $qry);
		if ($rs) {
			return trim(@pg_fetch_result($rs, 0, 'coded_value'));
		} else {
			return '';
		}
		@pg_free_result($rs);
	}
	
	
	
	
	

?>