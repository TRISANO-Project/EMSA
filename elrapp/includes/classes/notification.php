<?php

	include WEBROOT_URL.'/includes/classes/rule_parameter.php';
	
	class NotificationDatabaseException extends Exception {}
	class NotificationValidationException extends Exception {}

	/**
	 * Notification container class
	 * 
	 * Contains all variables used for determining whether a processed ELR message
	 * should generate a notification e-mail, and the properties of the message
	 * to be sent.
	 *
	 * @author Josh Ridderhoff <jridderhoff@utah.gov>
	 */
	class NotificationContainer
	{
		protected $system_message_id;
		protected $nedss_event_id;
		protected $nedss_record_number;
		protected $notification_type;
		protected $is_surveillance;
		protected $is_immediate;
		protected $is_state;
		protected $is_pregnancy;
		protected $is_automated;
		protected $is_new_cmr;  // is a new CMR being created or an existing event being updated
		protected $is_event_closed;
		protected $trigger_for_state;
		protected $trigger_for_local;
		protected $condition;
		protected $jurisdiction;
		protected $test_type;
		protected $test_result;  // e.g. "POSITIVE" or "EQUIVOCAL"
		protected $result_value;
		protected $investigator;
		protected $master_loinc;
		protected $specimen;
		protected $event_type;
		
		
		/**
		 * Set up parameters as instances of RuleParameter class
		 */
		public function __construct() {
			$this->system_message_id	= new RuleParameter('Integer');
			$this->nedss_event_id		= new RuleParameter('Integer');
			$this->nedss_record_number	= new RuleParameter('String');
			$this->notification_type	= new RuleParameter('Integer');
			$this->is_surveillance		= new RuleParameter('Boolean');
			$this->is_immediate			= new RuleParameter('Boolean');
			$this->is_state				= new RuleParameter('Boolean');
			$this->is_pregnancy			= new RuleParameter('Boolean');
			$this->is_automated			= new RuleParameter('Boolean');
			$this->is_new_cmr			= new RuleParameter('Boolean');
			$this->is_event_closed		= new RuleParameter('Boolean');
			$this->trigger_for_state	= new RuleParameter('Boolean');
			$this->trigger_for_local	= new RuleParameter('Boolean');
			$this->condition			= new RuleParameter('String');
			$this->jurisdiction			= new RuleParameter('Integer');
			$this->test_type			= new RuleParameter('String');
			$this->test_result			= new RuleParameter('String');
			$this->result_value			= new RuleParameter('String');
			$this->investigator			= new RuleParameter('String');
			$this->master_loinc			= new RuleParameter('String');
			$this->specimen				= new RuleParameter('String');
			$this->event_type			= new RuleParameter('String');
		}
		
		
		/*
		 * Operand Type constants
		 */
		const OPTYPE_PARAMETER = 1;	// operand type is a reference to a parameter
		const OPTYPE_VALUE = 2;		// operand type is a reference to a user-entered value
		
		/*
		 * Expression Chain Link Type constants
		 */
		const LINKTYPE_LINK = 1;	// link is a comparison expression to evaluate
		const LINKTYPE_CHAIN = 2;	// link is a pointer to another chain
		
		
		
		/*
		 * Magic setter & getter to protect RuleParameter property objects and resolve scope issues 
		 * with RuleParameter::setValue(mixed) & RuleParameter::getvalue() public methods
		 */
		public function __set($param, $value)
		{
			$this->$param->setValue($value);
		}
		public function __get($param)
		{
			return $this->$param->getValue();
		}
		
		
		
		/**
		 * Returns a list of all class property names for rule parameter configuration
		 * 
		 * @return array List of property names
		 */
		public function getPropertyList()
		{
			$var_names = array();
			foreach (get_object_vars($this) as $varname => $varval) {
				$var_names[] = $varname;
			}
			sort($var_names);
			return $var_names;
		}
		
		
		/**
		 * Gets the data type for a specified NotificationContainer property
		 * 
		 * @param string $param Name of the NotificationContainer property
		 * @return string Returns NULL on invalid property name, else property data type
		 */
		public function getDataType($param) {
			if (isset($this->$param) && ($this->$param instanceof RuleParameter)) {
				return $this->$param->getDataType();
			} else {
				return null;
			}
		}
		
		
		/**
		 * Runs notification rules against current notification object to determine
		 * whether criteria are met for generating a notification.  If criteria are
		 * met, a new notification is generated.
		 *
		 * @return bool TRUE if criteria are met, FALSE if not.
		 * @throws NotificationDatabaseException If an error occurs with the notification generation.
		 * @throws NotificationValidationException If required parameters are not complete.
		 */
		public function logNotification()
		{
			global $host_pa, $my_db_schema;
			
			if ($this->validateRequiredParams()) {
				$rule_sql = 'SELECT id, name, send_to_state, send_to_lhd, notification_type FROM '.$my_db_schema.'bn_rules WHERE enabled IS TRUE ORDER BY id;';
				$rule_rs = @pg_query($host_pa, $rule_sql);  // get list of rules
				if (($rule_rs === false) || (@pg_num_rows($rule_rs) < 1)) {
					throw new NotificationDatabaseException('Unable to retrieve list of rules to evaluate.');
				} else {
					while ($rule_row = @pg_fetch_object($rule_rs)) {
						if ($this->evaluateRule(intval($rule_row->id), 0)) {  // for each rule, see if conditions match
							$this->notification_type->setValue($rule_row->notification_type);
							$this->trigger_for_state->setValue(($rule_row->send_to_state == 't') ? true : false);
							$this->trigger_for_local->setValue(($rule_row->send_to_lhd == 't') ? true : false);
							
							#debug echo 'Triggered "'.$rule_row->name.'"<br>';
							if (!$this->generateNotification()) {  // if rule matches, attempt to generate notification
								throw new NotificationDatabaseException('Attempted to generate notification for rule "'.$rule_row->name.'", but encountered a database error.');
							}
						}
					}
				}
				@pg_free_result($rule_rs);
				return true;
			} else {
				throw new NotificationValidationException('Required notification parameters not set.');
			}
		}
		
		
		/**
		 * If notification criteria are met (determined by //), generates a notification record
		 * in batch_notifications to be sent at the scheduled time.
		 *
		 * @return bool TRUE on success, FALSE on failure
		 */
		protected function generateNotification()
		{
			global $host_pa, $my_db_schema;
			// params from old function...
			// $system_message_id = null, $event_id = null, $record_number = null, $notify_type = null, 
			// $condition = null, $test_type = null, $notify_state = false, $notify_lhd = false, 
			// $jurisdiction_id = null, $event_type = 'MorbidityEvent', $investigator = '', $test_result = ''
			
			//var_dump($this);
			
			$is_custom = isCustomNotificationTypeWithId($this->notification_type->getValue());
			if ($is_custom === false) {
				$derived_jurisdiction_id = $this->jurisdiction->getValue();  // not a custom notification, use jurisdiction ID for this event
			} else {
				$derived_jurisdiction_id = $is_custom;  // custom notification, use virtual jurisdiction ID from this notification type
			}
			
			
			$sql = 'INSERT INTO '.$my_db_schema.'batch_notifications ( 
					notification_type, 
					system_message_id, 
					event_id, 
					record_number, 
					condition, 
					test_type, 
					notify_state, 
					notify_lhd, 
					jurisdiction_id, 
					event_type, 
					investigator, 
					custom, 
					test_result 
				) VALUES ( 
					'.$this->notification_type->getValue().', 
					'.$this->system_message_id->getValue().', 
					'.$this->nedss_event_id->getValue().', 
					\''.@pg_escape_string($this->nedss_record_number->getValue()).'\', 
					\''.@pg_escape_string($this->condition->getValue()).'\', 
					\''.@pg_escape_string($this->test_type->getValue()).'\', 
					'.(($this->trigger_for_state->getValue()) ? "TRUE" : "FALSE").', 
					'.(($this->trigger_for_local->getValue()) ? "TRUE" : "FALSE").', 
					'.$derived_jurisdiction_id.', 
					\''.@pg_escape_string($this->event_type->getValue()).'\', 
					'.((strlen(trim($this->investigator->getValue())) > 0) ? "'".@pg_escape_string($this->investigator->getValue())."'" : "NULL").', 
					'.(($is_custom !== false) ? "TRUE" : "FALSE").', 
					'.((strlen(trim($this->test_result->getValue())) > 0) ? "'".@pg_escape_string($this->test_result->getValue())."'" : "NULL").' 
				);';
			#debug var_dump($sql);
			$rs = @pg_query($host_pa, $sql);
			if ($rs !== false) {
				return true;
			} else {
				return false;
			}
		}
		
		
		/**
		 * Verify all required parameters are set for rule evaluation & notification generation
		 * 
		 * @return bool
		 */
		protected function validateRequiredParams()
		{
			$is_valid = true;
			
			/*
			(!isset($this->system_message_id) || empty($this->system_message_id) || (filter_var($this->system_message_id, FILTER_VALIDATE_INT) === false)) 
				? $is_valid = $is_valid && false 
				: $this->system_message_id = intval($this->system_message_id);
			
			(!isset($this->nedss_event_id) || empty($this->nedss_event_id) || (filter_var($this->nedss_event_id, FILTER_VALIDATE_INT) === false)) 
				? $is_valid = $is_valid && false 
				: $this->nedss_event_id = intval($this->nedss_event_id);
			
			(!isset($this->nedss_record_number) || empty($this->nedss_record_number) || !(strlen(trim($this->nedss_record_number)) > 0)) 
				? $is_valid = $is_valid && false 
				: $this->nedss_record_number = filter_var(trim($this->nedss_record_number), FILTER_SANITIZE_STRING);
			
			(!isset($this->condition) || empty($this->condition) || !(strlen(trim($this->condition)) > 0)) 
				? $is_valid = $is_valid && false 
				: $this->condition = filter_var(trim($this->condition), FILTER_SANITIZE_STRING);
			
			(!isset($this->test_type) || empty($this->test_type) || !(strlen(trim($this->test_type)) > 0)) 
				? $is_valid = $is_valid && false 
				: $this->test_type = filter_var(trim($this->test_type), FILTER_SANITIZE_STRING);
			
			(!isset($this->jurisdiction) || empty($this->jurisdiction) || (filter_var($this->jurisdiction, FILTER_VALIDATE_INT) === false)) 
				? $is_valid = $is_valid && false 
				: $this->jurisdiction = intval($this->jurisdiction);
			*/
			
			return $is_valid;
		}
		
		
		/**
		 * Evaluate the specified rule & return whether conditions are met.
		 * 
		 * @param int $rule_id ID of the rule to be evaluated
		 * @param int $parent_chain_id Parent Chain ID of the expression chain to be evaluated
		 * @return bool
		 * @throws NotificationDatabaseException If no expressions are found to evaluate.
		 */
		protected function evaluateRule($rule_id, $parent_chain_id = 0)
		{
			global $host_pa, $my_db_schema;
			$result = false;
			$next_chain_id = $this->getNextChain($rule_id, $parent_chain_id, 0);
			$left_id = $next_chain_id;
			
			#debug echo '<hr>Starting evaluateRule('.$rule_id.', chain '.$parent_chain_id.')<hr>';
			
			do {
				#debug echo "\t".'Evaluating chain ID '.$next_chain_id.'... ';
				$this->evaluateChain($rule_id, $next_chain_id, $result);
				#debug var_dump($result);
				#debug echo '<br>';
				$next_chain_id = $this->getNextChain($rule_id, $parent_chain_id, $left_id);
				$left_id = $next_chain_id;
			} while ($next_chain_id !== false);
			
			#debug echo 'Done with rule '.$rule_id.', chain '.$parent_chain_id.'<br><br>';
			
			return $result;
		}
		
		
		/**
		 * Get ID of next chain in parent chain to be evaluated.
		 * 
		 * @param int $rule_id ID of the rule being evaluated
		 * @param int $parent_chain_id ID of the chain being evaluated
		 * @param int $left_id ID of the last chain that was evaluated
		 * @return mixed ID of next chain to be evaluated if found, FALSE if no more chains exist
		 */
		public function getNextChain($rule_id, $parent_chain_id, $left_id)
		{
			global $host_pa, $my_db_schema;
			$next_chain_id = false;
			
			$sql = 'SELECT id FROM '.$my_db_schema.'bn_expression_chain WHERE rule_id = '.intval($rule_id).' AND parent_chain_id = '.intval($parent_chain_id).' AND left_id = '.intval($left_id).';';
			$rs = @pg_query($host_pa, $sql);
			
			if (($rs === false) || (intval(@pg_num_rows($rs)) !== 1)) {
				$next_chain_id = false;
			} else {
				$next_chain_id = intval(@pg_fetch_result($rs, 0, 'id'));
			}
			
			@pg_free_result($rs);
			return $next_chain_id;
		}
		
		
		/**
		 * Evaluates an expression chain and returns whether it is TRUE or FALSE
		 * 
		 * @param int $rule_id ID of the rule being evaluated
		 * @param int $chain_id ID of the expression chain to evaluate
		 * @param bool $result Reference to result boolean variable
		 * @return void
		 * @throws Exception If chain_id is not provided, chain_id is not found, or chain is not a valid link type
		 */
		protected function evaluateChain($rule_id = null, $chain_id = null, &$result)
		{
			if (empty($chain_id) || empty($rule_id)) {
				throw new Exception('Unable to evaluate expression chain:  Missing Rule or Chain ID.');
				return false;
			} else {
				global $host_pa, $my_db_schema;
				$sql = 'SELECT c.id AS id, c.link_type AS link_type, c.link_id AS link_id, c.left_operator_id AS operator_id, o.label AS operator 
						FROM '.$my_db_schema.'bn_expression_chain c 
						LEFT JOIN '.$my_db_schema.'structure_operator o ON (c.left_operator_id = o.id) 
						WHERE c.id = '.intval($chain_id).';';
				$rs = @pg_query($host_pa, $sql);
				
				if (($rs === false) || (intval(@pg_num_rows($rs)) !== 1)) {
					throw new Exception('Missing logical expression in rule.');
				} else {
					$chain = @pg_fetch_object($rs);
					if (intval($chain->operator_id) === 0) {
						// left-most token in the expression chain... set $result to be result of this link's evaluation
						if (intval($chain->link_type) === NotificationContainer::LINKTYPE_LINK) {
							$result = $this->evaluateLink(intval($chain->link_id));
						} elseif (intval($chain->link_type) === NotificationContainer::LINKTYPE_CHAIN) {
							$result = $this->evaluateRule($rule_id, intval($chain->id));
						} else {
							throw new Exception('Invalid logical expression type.');
						}
					} else {
						// evaluate current token & compare against $result using the defined operator
						if (intval($chain->link_type) === NotificationContainer::LINKTYPE_LINK) {
							#debug echo '[a link] ';
							switch (strtolower(trim($chain->operator))) {
								case 'and':
									$result = ($result && $this->evaluateLink(intval($chain->link_id)));
									break;
								case 'or':
									$result = ($result || $this->evaluateLink(intval($chain->link_id)));
									break;
								default:
									throw new Exception('Invalid operator specified in logical expression.');
							}
						} elseif (intval($chain->link_type) === NotificationContainer::LINKTYPE_CHAIN) {
							#debug echo '[another chain] ';
							switch (strtolower(trim($chain->operator))) {
								case 'and':
									$result = ($result && $this->evaluateRule($rule_id, intval($chain->id)));
									break;
								case 'or':
									$result = ($result || $this->evaluateRule($rule_id, intval($chain->id)));
									break;
								default:
									throw new Exception('Invalid operator specified in logical expression.');
							}
						} else {
							throw new Exception('Invalid logical expression type.');
						}
					}
				}
				
				@pg_free_result($rs);
			}
		}
		
		
		/**
		 * Evaluates an expression link and returns whether it is TRUE or FALSE
		 * 
		 * @param int $link_id ID of the expression link to evaluate
		 * @return bool
		 * @throws Exception If link_id is not provided.
		 */
		protected function evaluateLink($link_id = null)
		{
			$result = false;
			if (empty($link_id)) {
				throw new Exception('Unable to evaluate expression link:  No ID provided.');
				return $result;
			} else {
				global $host_pa, $my_db_schema;
				$sql = 'SELECT l.type_left AS type_left, l.type_right AS type_right, l.operand_left AS operand_left, l.operand_right AS operand_right, o.label AS operator 
						FROM '.$my_db_schema.'bn_expression_link l 
						LEFT JOIN '.$my_db_schema.'structure_operator o ON (l.operator_id = o.id) 
						WHERE l.id = '.intval($link_id).';';
				$rs = @pg_query($host_pa, $sql);
				
				if (($rs === false) || (intval(@pg_num_rows($rs)) !== 1)) {
					throw new Exception('Unable to evaluate expression link:  Link not found.');
					$result = false;
				} else {
					$link = @pg_fetch_object($rs);
					
					$operand_left = $link->operand_left;
					$operand_right = $link->operand_right;
					
					if ((intval($link->type_left) === NotificationContainer::OPTYPE_PARAMETER) && (intval($link->type_right) === NotificationContainer::OPTYPE_PARAMETER)) {
						// parameter to parameter
						switch (strtolower(trim($link->operator))) {
							case 'equal':
								$result = ($this->$operand_left->getValue() == $this->$operand_right->getValue()) ? true : false;
								break;
							case 'not equal':
								$result = ($this->$operand_left->getValue() != $this->$operand_right->getValue()) ? true : false;
								break;
							case 'greater than':
								$result = ($this->$operand_left->getValue() > $this->$operand_right->getValue()) ? true : false;
								break;
							case 'less than':
								$result = ($this->$operand_left->getValue() < $this->$operand_right->getValue()) ? true : false;
								break;
							case 'greater than or equal to':
								$result = ($this->$operand_left->getValue() >= $this->$operand_right->getValue()) ? true : false;
								break;
							case 'less than or equal to':
								$result = ($this->$operand_left->getValue() <= $this->$operand_right->getValue()) ? true : false;
								break;
							case 'in':
								$result = (in_array($this->$operand_left->getValue(), preg_split("/[\s]*[,][\s]*/", $this->$operand_right->getValue()))) ? true : false;
								break;
							case 'contains':
								$result = (stripos($this->$operand_left->getValue(), $this->$operand_right->getValue()) !== false) ? true : false;
								break;
							default:
								throw new Exception('Invalid operator specified in logical expression.');
								$result = false;
						}
					} elseif ((intval($link->type_left) === NotificationContainer::OPTYPE_PARAMETER) && (intval($link->type_right) === NotificationContainer::OPTYPE_VALUE)) {
						// parameter to value
						switch (strtolower(trim($link->operator))) {
							case 'equal':
								$result = ($this->$operand_left->getValue() == $operand_right) ? true : false;
								break;
							case 'not equal':
								$result = ($this->$operand_left->getValue() != $operand_right) ? true : false;
								break;
							case 'greater than':
								$result = ($this->$operand_left->getValue() > $operand_right) ? true : false;
								break;
							case 'less than':
								$result = ($this->$operand_left->getValue() < $operand_right) ? true : false;
								break;
							case 'greater than or equal to':
								$result = ($this->$operand_left->getValue() >= $operand_right) ? true : false;
								break;
							case 'less than or equal to':
								$result = ($this->$operand_left->getValue() <= $operand_right) ? true : false;
								break;
							case 'in':
								$result = (in_array($this->$operand_left->getValue(), preg_split("/[\s]*[,][\s]*/", $operand_right))) ? true : false;
								break;
							case 'contains':
								$result = (stripos($this->$operand_left->getValue(), $operand_right) !== false) ? true : false;
								break;
							default:
								throw new Exception('Invalid operator specified in logical expression.');
								$result = false;
						}
					} else {
						// ain't no way, no how...
						throw new Exception('Unable to evaluate expression link:  Invalid operand types specified.');
						$result = false;
					}
				}
				
				return $result;
			}
		}
		
	}
	
?>