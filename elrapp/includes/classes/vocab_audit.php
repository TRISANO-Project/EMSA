<?php

	/**
	 * Vocabulary Manager auditing functionality
	 * 
	 * Audit changes made to items in the Vocabulary system.
	 *
	 * @author Josh Ridderhoff <jridderhoff@utah.gov>
	 */
	class VocabAudit
	{
	
		public $vocab_id;
		public $table;
		public $action;
		protected $user_id;
		protected $old_vals;
		protected $new_vals;
		
		
		public function __construct()
		{
			$this->vocab_id = null;
			$this->table = null;
			$this->action = null;
			$this->user_id = trim($_SESSION['umdid']);
			$this->old_vals = null;
			$this->new_vals = null;
		}
		
		
		const TABLE_MASTER_VOCAB = 1;
		const TABLE_MASTER_TO_APP = 2;
		const TABLE_CHILD_VOCAB = 3;
		const TABLE_MASTER_LOINC = 4;
		const TABLE_MASTER_CONDITION = 5;
		const TABLE_MASTER_SNOMED = 6;
		const TABLE_CHILD_LOINC = 7;
		const TABLE_CHILD_SNOMED = 8;
		const TABLE_CHILD_TESTRESULT = 9;
		const TABLE_CMR_RULES = 10;
		
		const ACTION_ADD = 1;
		const ACTION_EDIT = 2;
		const ACTION_DELETE = 3;
		
		
		public function resetAudit()
		{
			$this->vocab_id = null;
			$this->table = null;
			$this->action = null;
			$this->user_id = trim($_SESSION['umdid']);
			$this->old_vals = null;
			$this->new_vals = null;
		}
		
		
		public function tableName($table_id = null)
		{
			$name = 'Unknown/Unspecified Table';
			
			if (empty($table_id)) {
				return $name;
			}
			
			switch (intval($table_id)) {
				case 1:
					$name = 'Master Dictionary';
					break;
				case 2:
					$name = 'Master to App Translation';
					break;
				case 3:
					$name = 'Child Dictionary';
					break;
				case 4:
					$name = 'Master LOINC';
					break;
				case 5:
					$name = 'Master Condition';
					break;
				case 6:
					$name = 'Master SNOMED';
					break;
				case 7:
					$name = 'Child LOINC';
					break;
				case 8:
					$name = 'Child SNOMED';
					break;
				case 9:
					$name = 'Child LOINC Interpretive Rule';
					break;
				case 10:
					$name = 'Master LOINC CMR Rule';
					break;
				default:
					$name = 'Unknown/Unspecified Table';
					break;
			}
			
			return $name;
		}
		
		
		public function setOldVals($package = null)
		{
			if (empty($package)) {
				$this->old_vals = '';
				return false;
			}
			
			if (is_object($package) || is_array($package)) {
				$encoded_package = @json_encode($package);
				if ($encoded_package === false) {
					$this->old_vals = '';
					return false;
				} else {
					$this->old_vals = $encoded_package;
					return true;
				}
			} else {
				$obj_check = @json_decode($package);
				if (!is_null($obj_check) && (is_object($obj_check) || is_array($obj_check))) {
					$this->old_vals = $package;
					return true;
				} else {
					$this->old_vals = '';
					return false;
				}
			}
		}
		
		
		public function setNewVals($package = null)
		{
			if (empty($package)) {
				$this->new_vals = '';
				return false;
			}
			
			if (is_object($package) || is_array($package)) {
				$encoded_package = @json_encode($package);
				if ($encoded_package === false) {
					$this->new_vals = '';
					return false;
				} else {
					$this->new_vals = $encoded_package;
					return true;
				}
			} else {
				$obj_check = @json_decode($package);
				if (!is_null($obj_check) && (is_object($obj_check) || is_array($obj_check))) {
					$this->new_vals = $package;
					return true;
				} else {
					$this->new_vals = '';
					return false;
				}
			}
		}
		
		
		public function getRuleParentId($rule_id = null, $table = null)
		{
			global $host_pa, $my_db_schema;
			
			if (empty($rule_id) || empty($table)) {
				return null;
			}
			
			switch (intval($table)) {
				case self::TABLE_CHILD_TESTRESULT:
					$sql = 'SELECT child_loinc_id AS parent_id FROM '.$my_db_schema.'vocab_c2m_testresult WHERE id = '.intval($rule_id).';';
					break;
				case self::TABLE_CMR_RULES:
					$sql = 'SELECT master_loinc_id AS parent_id FROM '.$my_db_schema.'vocab_rules_masterloinc WHERE id = '.intval($rule_id).';';
					break;
				default:
					$sql = null;
			}
			
			if (empty($sql)) {
				return null;
			}
			
			$rs = @pg_query($host_pa, $sql);
			if (($rs === false) || (@pg_num_rows($rs) !== 1)) {
				return null;
			}
			
			return intval(@pg_fetch_result($rs, 0, 'parent_id'));
		}
		
		
		public function getPreviousVals($vocab_id = null, $table = null)
		{
			global $host_pa, $my_db_schema;
			
			if (empty($vocab_id) || empty($table)) {
				return null;
			}
			
			switch (intval($table)) {
				case self::TABLE_MASTER_VOCAB:
					$sql = 'SELECT sc.label AS "Category", mv.codeset AS "Value Set Code", mv.concept AS "Master Concept Name" 
						FROM '.$my_db_schema.'vocab_master_vocab mv 
						INNER JOIN '.$my_db_schema.'structure_category sc ON (mv.category = sc.id) 
						WHERE mv.id = '.intval($vocab_id).';';
					break;
				case self::TABLE_MASTER_TO_APP:
					$sql = 'SELECT a.app_name AS "Application", m2a.coded_value AS "Value" 
						FROM '.$my_db_schema.'vocab_master2app m2a 
						INNER JOIN '.$my_db_schema.'vocab_app a ON (m2a.app_id = a.id) 
						WHERE m2a.master_id = '.intval($vocab_id).';';
					break;
				case self::TABLE_CHILD_VOCAB:
					$sql = 'SELECT l.ui_name AS "Lab", sc.label AS "Category", cv.concept AS "Child Code", mv.concept AS "Master Concept Name" 
						FROM '.$my_db_schema.'vocab_child_vocab cv 
						INNER JOIN '.$my_db_schema.'structure_labs l ON (cv.lab_id = l.id) 
						INNER JOIN '.$my_db_schema.'vocab_master_vocab mv ON (cv.master_id = mv.id) 
						INNER JOIN '.$my_db_schema.'structure_category sc ON (mv.category = sc.id) 
						WHERE cv.id = '.intval($vocab_id).';';
					break;
				case self::TABLE_MASTER_LOINC:
					$sql = 'SELECT ml.loinc AS "LOINC Code", ml.concept_name AS "Concept Name", 
						CASE WHEN ml.condition_from_result IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Look Up Condition?", mv_c.concept AS "Condition", 
						CASE WHEN ml.organism_from_result IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Look Up Organism?", mv_o.concept AS "Organism", 
						mv_tt.concept AS "Test Type", mv_s.concept AS "Specimen Source", ss.name AS "List", ml.gray_rule AS "Graylist Rules" 
						FROM '.$my_db_schema.'vocab_master_loinc ml 
						LEFT JOIN '.$my_db_schema.'vocab_master_condition mc ON (ml.trisano_condition = mc.c_id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_organism mo ON (ml.trisano_organism = mo.o_id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_c ON (mc.condition = mv_c.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_o ON (mo.organism = mv_o.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_tt ON (ml.trisano_test_type = mv_tt.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_s ON (ml.specimen_source = mv_s.id) 
						LEFT JOIN '.$my_db_schema.'system_statuses ss ON (ml.list = ss.id) 
						WHERE ml.l_id = '.intval($vocab_id).';';
					break;
				case self::TABLE_MASTER_CONDITION:
					$sql = 'SELECT 
						mv_dc.concept AS "Disease Category", 
						mv_c.concept AS "Condition Name", 
						mc.valid_specimen AS "Valid Specimens", 
						mc.invalid_specimen AS "Invalid Specimens", 
						mc.white_rule AS "CMR White. Rules", 
						mc.contact_white_rule AS "Contact White. Rules", 
						mc.gateway_xref AS "Gateway Xrefs", 
						CASE WHEN mc.check_xref_first IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Check Xrefs First?",
						CASE WHEN mc.immediate_notify IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Imm. Notifiable?", 
						CASE WHEN mc.require_specimen IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Req. Specimen from Noms?", 
						CASE WHEN mc.new_event IS TRUE THEN \'New\' ELSE \'Same\' END AS "Diff. Species New/Same?", 
						CASE WHEN mc.notify_state IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Notify State?", 
						CASE WHEN mc.autoapproval IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Autoapproval?", 
						sd.health_district AS "Jurisdiction Override" 
						FROM '.$my_db_schema.'vocab_master_condition mc 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_dc ON (mc.disease_category = mv_dc.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_c ON (mc.condition = mv_c.id) 
						LEFT JOIN '.$my_db_schema.'system_districts sd ON (mc.district_override = sd.id) 
						WHERE mc.c_id = '.intval($vocab_id).';';
					break;
				case self::TABLE_MASTER_SNOMED:
					$sql = 'SELECT 
						mv_sc.concept AS "SNOMED Type", 
						mo.snomed AS "SNOMED Code", 
						mo.snomed_alt AS "Alt. SNOMED Code", 
						mv_c.concept AS "Master Condition", 
						mv_o.concept AS "Type Concept Name", 
						ss.name AS "List", 
						mv_t.concept AS "Test Result", 
						mv_s.concept AS "State Case Status", 
						CASE WHEN mo.nom_is_surveillance IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Nom. Surveillance?" 
						FROM '.$my_db_schema.'vocab_master_organism mo 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_sc ON (mo.snomed_category = mv_sc.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_condition mc ON (mo.condition = mc.c_id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_c ON (mc.condition = mv_c.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_o ON (mo.organism = mv_o.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_t ON (mo.test_result = mv_t.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_s ON (mo.status = mv_s.id) 
						LEFT JOIN '.$my_db_schema.'system_statuses ss ON (mo.list = ss.id) 
						WHERE mo.o_id = '.intval($vocab_id).';';
					break;
				case self::TABLE_CHILD_LOINC:
					$sql = 'SELECT
						l.ui_name AS "Lab", 
						cl.child_loinc AS "Child LOINC", 
						CASE WHEN cl.archived IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Archived?", 
						ml.loinc AS "Master LOINC", 
						CASE WHEN cl.interpret_results IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Quantitative?", 
						CASE WHEN cl.semi_auto IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Semi-Auto?", 
						CASE WHEN cl.validate_child_snomed IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Validate SNOMED?", 
						mv_l.concept AS "Result Location", 
						cl.units AS "Units", 
						cl.refrange AS "Reference Range", 
						cl.hl7_refrange AS "HL7 Reference Range", 
						CASE WHEN cl.pregnancy IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Pregnancy?", 
						cl.child_orderable_test_code AS "Orderable Test Code", 
						cl.child_resultable_test_code AS "Resultable Test Code", 
						cl.child_concept_name AS "Child Concept Name", 
						cl.child_alias AS "Child Alias" 
						FROM '.$my_db_schema.'vocab_child_loinc cl 
						INNER JOIN '.$my_db_schema.'structure_labs l ON (cl.lab_id = l.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_loinc ml ON (cl.master_loinc = ml.l_id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_l ON (cl.result_location = mv_l.id) 
						WHERE cl.id = '.intval($vocab_id).';';
					break;
				case self::TABLE_CHILD_SNOMED:
					$sql = 'SELECT
						l.ui_name AS "Lab", 
						co.child_code AS "Child SNOMED Code", 
						mo1.snomed AS "Master Organism", 
						mo2.snomed AS "Master Test Result", 
						co.result_value AS "Result Value", 
						co.comment AS "Comments" 
						FROM '.$my_db_schema.'vocab_child_organism co 
						INNER JOIN '.$my_db_schema.'structure_labs l ON (co.lab_id = l.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_organism mo1 ON (co.organism = mo1.o_id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_organism mo2 ON (co.test_result_id = mo2.o_id) 
						WHERE co.id = '.intval($vocab_id).';';
					break;
				case self::TABLE_CHILD_TESTRESULT:
					$sql = 'SELECT 
						a.app_name AS "Application", 
						c2m.conditions_js AS "Conditions", 
						mv.concept AS "Test Result", 
						c2m.results_to_comments AS "Comments" 
						FROM '.$my_db_schema.'vocab_c2m_testresult c2m 
						INNER JOIN '.$my_db_schema.'vocab_app a ON (c2m.app_id = a.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv ON (mv.id = c2m.master_id) 
						WHERE c2m.id = '.intval($vocab_id).';';
					break;
				case self::TABLE_CMR_RULES:
					$sql = 'SELECT 
						a.app_name AS "Application", 
						cmr.conditions_js AS "Conditions", 
						CASE WHEN cmr.allow_new_cmr IS TRUE THEN \'Yes\' ELSE \'No\' END AS "New CMR?", 
						CASE WHEN cmr.is_surveillance IS TRUE THEN \'Yes\' ELSE \'No\' END AS "Surveillance?", 
						mv_s.concept AS "State Case Status" 
						FROM '.$my_db_schema.'vocab_rules_masterloinc cmr 
						INNER JOIN '.$my_db_schema.'vocab_app a ON (cmr.app_id = a.id) 
						LEFT JOIN '.$my_db_schema.'vocab_master_vocab mv_s ON (cmr.state_case_status_master_id = mv_s.id) 
						WHERE cmr.id = '.intval($vocab_id).';';
					break;
				default:
					$sql = null;
			}
			
			if (empty($sql)) {
				return null;
			}
			
			$rs = @pg_query($host_pa, $sql);
			if (($rs === false) || (@pg_num_rows($rs) !== 1)) {
				return null;
			}
			
			$row = @pg_fetch_assoc($rs);
			$vals = array();
			foreach ($row as $col => $val) {
				if (stripos($col, 'gateway') !== false) {
					$vals[] = array('col' => trim($col), 'val' => trim(gatewayCrossrefIdValues($val)));
				} elseif (stripos($col, 'valid spec') !== false) {
					$vals[] = array('col' => trim($col), 'val' => trim(specimenIdValues($val)));
				} elseif ((intval($table) == self::TABLE_CMR_RULES) && (stripos($col, 'conditions') !== false)) {
					$vals[] = array('col' => trim($col), 'val' => trim($this->verboseCmrConditions($val)));
				} elseif ((intval($table) == self::TABLE_CHILD_TESTRESULT) && (stripos($col, 'conditions') !== false)) {
					$vals[] = array('col' => trim($col), 'val' => trim($this->verboseInterpRuleConditions($val)));
				} else {
					$vals[] = array('col' => trim($col), 'val' => trim($val));
				}
			}
			
			return $vals;
		}
		
		
		public function prepareNewValues($table = null, $newvals = array())
		{
			global $host_pa, $my_db_schema;
			
			if (empty($newvals) || empty($table)) {
				return null;
			}
			
			switch (intval($table)) {
				case self::TABLE_MASTER_VOCAB:
					$sql = 'SELECT sc.label AS "Category", \''.@pg_escape_string(trim($newvals['valueset'])).'\' AS "Value Set Code", \''.@pg_escape_string(trim($newvals['masterconcept'])).'\' AS "Master Concept Name" 
						FROM '.$my_db_schema.'structure_category sc 
						WHERE sc.id = '.intval($newvals['category']).';';
					break;
				case self::TABLE_MASTER_TO_APP:
					$sql = 'SELECT a.app_name AS "Application", \''.@pg_escape_string(trim($newvals['appvalue'])).'\' AS "Value" 
						FROM '.$my_db_schema.'vocab_app a 
						WHERE a.id = '.intval($newvals['app_id']).';';
					break;
				case self::TABLE_CHILD_VOCAB:
					// lab_id, master_id, child_code
					$sql = 'SELECT l.ui_name AS "Lab", sc.label AS "Category", \''.@pg_escape_string(trim($newvals['child_code'])).'\' AS "Child Code", mv.concept AS "Master Concept Name" 
						FROM '.$my_db_schema.'structure_labs l,  
						'.$my_db_schema.'vocab_master_vocab mv 
						INNER JOIN '.$my_db_schema.'structure_category sc ON (mv.category = sc.id) 
						WHERE (l.id = '.intval($newvals['lab_id']).') 
						AND (mv.id = '.intval($newvals['master_id']).');';
					break;
				case self::TABLE_MASTER_LOINC:
					$sql = 'SELECT 
						\''.@pg_escape_string(trim($newvals['loinc'])).'\' AS "LOINC Code", 
						\''.@pg_escape_string(trim($newvals['concept_name'])).'\' AS "Concept Name", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['condition_from_result'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Look Up Condition?", 
						(SELECT mv_c.concept FROM '.$my_db_schema.'vocab_master_vocab mv_c INNER JOIN '.$my_db_schema.'vocab_master_condition mc ON ((mv_c.id = mc.condition) AND (mc.c_id = '.intval($newvals['trisano_condition']).'))) AS "Condition", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['organism_from_result'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Look Up Organism?", 
						(SELECT mv_o.concept FROM '.$my_db_schema.'vocab_master_vocab mv_o INNER JOIN '.$my_db_schema.'vocab_master_organism mo ON ((mv_o.id = mo.organism) AND (mo.o_id = '.intval($newvals['trisano_organism']).'))) AS "Organism", 
						(SELECT mv_tt.concept FROM '.$my_db_schema.'vocab_master_vocab mv_tt WHERE mv_tt.id = '.intval($newvals['trisano_test_type']).') AS "Test Type", 
						(SELECT mv_s.concept FROM '.$my_db_schema.'vocab_master_vocab mv_s WHERE mv_s.id = '.intval($newvals['specimen_source']).') AS "Specimen Source", 
						(SELECT ss.name FROM '.$my_db_schema.'system_statuses ss WHERE ss.id = '.intval($newvals['list']).') AS "List", 
						\''.@pg_escape_string(trim($newvals['gray_rule'])).'\' AS "Graylist Rules";';
					break;
				case self::TABLE_MASTER_CONDITION:
					$sql = 'SELECT 
						(SELECT mv_dc.concept FROM '.$my_db_schema.'vocab_master_vocab mv_dc WHERE mv_dc.id = '.intval($newvals['disease_category']).') AS "Disease Category", 
						(SELECT mv_c.concept FROM '.$my_db_schema.'vocab_master_vocab mv_c WHERE mv_c.id = '.intval($newvals['condition']).') AS "Condition Name", 
						\''.@pg_escape_string(trim(implode(';', $newvals['valid_specimen']))).'\' AS "Valid Specimens", 
						\''.@pg_escape_string(trim(implode(';', $newvals['invalid_specimen']))).'\' AS "Invalid Specimens", 
						\''.@pg_escape_string(trim($newvals['white_rule'])).'\' AS "CMR White. Rules", 
						\''.@pg_escape_string(trim($newvals['contact_white_rule'])).'\' AS "Contact White. Rules", 
						\''.@pg_escape_string(trim(implode(';', $newvals['gateway_xref']))).'\' AS "Gateway Xrefs", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['check_xref_first'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Check Xrefs First?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['immediate_notify'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Imm. Notifiable?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['require_specimen'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Req. Specimen from Noms?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['new_event'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Diff. Species New/Same?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['notify_state'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Notify State?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['autoapproval'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Autoapproval?", 
						(SELECT sd.health_district FROM '.$my_db_schema.'system_districts sd WHERE sd.id = '.intval($newvals['district_override']).') AS "Jurisdiction Override";';
					break;
				case self::TABLE_MASTER_SNOMED:
					$sql = 'SELECT
						(SELECT mv_sc.concept FROM '.$my_db_schema.'vocab_master_vocab mv_sc WHERE mv_sc.id = '.intval($newvals['snomed_category']).') AS "SNOMED Type", 
						\''.@pg_escape_string(trim($newvals['snomed'])).'\' AS "SNOMED Code", 
						\''.@pg_escape_string(trim($newvals['snomed_alt'])).'\' AS "Alt. SNOMED Code", 
						(SELECT mv_c.concept FROM '.$my_db_schema.'vocab_master_vocab mv_c INNER JOIN '.$my_db_schema.'vocab_master_condition mc ON ((mv_c.id = mc.condition) AND (mc.c_id = '.intval($newvals['condition']).'))) AS "Condition", 
						(SELECT mv_o.concept FROM '.$my_db_schema.'vocab_master_vocab mv_o WHERE mv_o.id = '.intval($newvals['organism']).') AS "Type Concept Name", 
						(SELECT ss.name FROM '.$my_db_schema.'system_statuses ss WHERE ss.id = '.intval($newvals['list']).') AS "List", 
						(SELECT mv_t.concept FROM '.$my_db_schema.'vocab_master_vocab mv_t WHERE mv_t.id = '.intval($newvals['test_result']).') AS "Test Result", 
						(SELECT mv_s.concept FROM '.$my_db_schema.'vocab_master_vocab mv_s WHERE mv_s.id = '.intval($newvals['status']).') AS "State Case Status", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['nom_is_surveillance'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Nom. Surveillance?";';
					break;
				case self::TABLE_CHILD_LOINC:
					$sql = 'SELECT
						(SELECT l.ui_name FROM '.$my_db_schema.'structure_labs l WHERE l.id = '.intval($newvals['lab_id']).') AS "Lab", 
						\''.@pg_escape_string(trim($newvals['child_loinc'])).'\' AS "Child LOINC", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['archived'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Archived?", 
						(SELECT ml.loinc FROM '.$my_db_schema.'vocab_master_loinc ml WHERE ml.l_id = '.intval($newvals['master_loinc']).') AS "Master LOINC", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['interpret_results'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Quantitative?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['semi_auto'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Semi-Auto?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['validate_child_snomed'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Validate SNOMED?", 
						(SELECT mv_rl.concept FROM '.$my_db_schema.'vocab_master_vocab mv_rl WHERE mv_rl.id = '.intval($newvals['result_location']).') AS "Result Location", 
						\''.@pg_escape_string(trim($newvals['units'])).'\' AS "Units", 
						\''.@pg_escape_string(trim($newvals['refrange'])).'\' AS "Reference Range", 
						\''.@pg_escape_string(trim($newvals['hl7_refrange'])).'\' AS "HL7 Reference Range", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['pregnancy'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Pregnancy?", 
						\''.@pg_escape_string(trim($newvals['child_orderable_test_code'])).'\' AS "Orderable Test Code", 
						\''.@pg_escape_string(trim($newvals['child_resultable_test_code'])).'\' AS "Resultable Test Code", 
						\''.@pg_escape_string(trim($newvals['child_concept_name'])).'\' AS "Child Concept Name", 
						\''.@pg_escape_string(trim($newvals['child_alias'])).'\' AS "Child Alias";';
					break;
				case self::TABLE_CHILD_SNOMED:
					$sql = 'SELECT 
						(SELECT l.ui_name FROM '.$my_db_schema.'structure_labs l WHERE l.id = '.intval($newvals['lab_id']).') AS "Lab", 
						\''.@pg_escape_string(trim($newvals['child_code'])).'\' AS "Child SNOMED Code", 
						(SELECT mo1.snomed FROM '.$my_db_schema.'vocab_master_organism mo1 WHERE mo1.o_id = '.intval($newvals['organism']).') AS "Master Organism", 
						(SELECT mo2.snomed FROM '.$my_db_schema.'vocab_master_organism mo2 WHERE mo2.o_id = '.intval($newvals['test_result_id']).') AS "Master Test Result", 
						\''.@pg_escape_string(trim($newvals['result_value'])).'\' AS "Result Value", 
						\''.@pg_escape_string(trim($newvals['comment'])).'\' AS "Comments";';
					break;
				case self::TABLE_CHILD_TESTRESULT:
					$sql = 'SELECT 
						(SELECT a.app_name FROM '.$my_db_schema.'vocab_app a WHERE a.id = '.intval($newvals['app_id']).') AS "Application", 
						\''.@pg_escape_string(trim($newvals['conditions'])).'\' AS "Conditions", 
						(SELECT mv.concept FROM '.$my_db_schema.'vocab_master_vocab mv WHERE mv.id = '.intval($newvals['test_result']).') AS "Test Result", 
						\''.@pg_escape_string(trim($newvals['comments'])).'\' AS "Comments";';
					break;
				case self::TABLE_CMR_RULES:
					$sql = 'SELECT
						(SELECT a.app_name FROM '.$my_db_schema.'vocab_app a WHERE a.id = '.intval($newvals['app_id']).') AS "Application", 
						\''.@pg_escape_string(trim($newvals['conditions'])).'\' AS "Conditions", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['new_cmr'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "New CMR?", 
						CASE WHEN \''.@pg_escape_string(trim($newvals['surveillance'])).'\' = \'t\' THEN \'Yes\' ELSE \'No\' END AS "Surveillance?", 
						(SELECT mv_s.concept FROM '.$my_db_schema.'vocab_master_vocab mv_s WHERE mv_s.id = '.intval($newvals['state_case_status_master_id']).') AS "State Case Status";';
					break;
				default:
					$sql = null;
			}
			
			if (empty($sql)) {
				return null;
			}
			
			$rs = @pg_query($host_pa, $sql);
			if (($rs === false) || (@pg_num_rows($rs) !== 1)) {
				return null;
			}
			
			$row = @pg_fetch_assoc($rs);
			$vals = array();
			foreach ($row as $col => $val) {
				if (stripos($col, 'gateway') !== false) {
					$vals[] = array('col' => trim($col), 'val' => trim(gatewayCrossrefIdValues($val)));
				} elseif (stripos($col, 'valid spec') !== false) {
					$vals[] = array('col' => trim($col), 'val' => trim(specimenIdValues($val)));
				} else {
					$vals[] = array('col' => trim($col), 'val' => trim($val));
				}
			}
			
			return $vals;
		}
		
		
		public function auditVocab($vocab_id = null, $table = null, $action = null)
		{
			global $host_pa, $my_db_schema;
			
			if (intval($vocab_id) > 0) {
				$this->vocab_id = intval($vocab_id);
			}
			
			if (intval($table) > 0) {
				$this->table = intval($table);
			}
			
			if (intval($action) > 0) {
				$this->action = intval($action);
			}
			
			if (is_null($this->vocab_id) || is_null($this->table) || is_null($this->action)) {
				return false;
			}
			
			
			$sql = 'INSERT INTO '.$my_db_schema.'vocab_audits ( 
					vocab_id, 
					tbl, 
					user_id, 
					action, 
					old_vals, 
					new_vals 
				) VALUES ( 
					'.intval($this->vocab_id).', 
					'.intval($this->table).', 
					\''.pg_escape_string(trim($this->user_id)).'\', 
					'.intval($this->action).', 
					'.((strlen(trim($this->old_vals)) > 0) ? '\''.pg_escape_string(trim($this->old_vals)).'\'' : 'NULL').', 
					'.((strlen(trim($this->new_vals)) > 0) ? '\''.pg_escape_string(trim($this->new_vals)).'\'' : 'NULL').' 
				);';
			$rs = @pg_query($host_pa, $sql);
			if ($rs === false) {
				throw new Exception('Could not record audit log for this change; a database error occurred.<br><br>'.pg_last_error());
			}
			
			@pg_free_result($rs);
			return true;
		}
		
		
		public function displayVocabAuditById($vocab_id = null, $table = array())
		{
			global $host_pa, $my_db_schema;
			
			$html_prefix = '<table class="emsa_results audit_log">
				<thead>
					<tr>
						<th>Date/Time</th>
						<th>User</th>
						<th>Action</th>
						<th>Old Values</th>
						<th>New Values</th>
					</tr>
				</thead>
				<tbody>';
				
			$html_suffix = '</tbody>
				</table>';
				
			$html_not_found = '<tr>
					<td colspan="5">
						<em>No actions found for this vocabulary entry.</em>
					</td>
				</tr>';
			
			if (empty($vocab_id) || empty($table)) {
				return $html_prefix.$html_not_found.$html_suffix;
			}
			
			if (is_array($table)) {
				$clean_table = array_map('intval', $table);
				$table_list = implode(',', $clean_table);
			} else {
				return $html_prefix.$html_not_found.$html_suffix;
			}
			
			$sql = 'SELECT * FROM '.$my_db_schema.'vocab_audits 
				WHERE (vocab_id = '.intval($vocab_id).') 
				AND (tbl IN ('.$table_list.')) 
				ORDER BY event_time;';
			$rs = @pg_query($host_pa, $sql);
			if (($rs === false) || (@pg_num_rows($rs) < 1)) {
				return $html_prefix.$html_not_found.$html_suffix;
			} else {
				unset($html);
				while ($row = @pg_fetch_object($rs)) {
					unset($this_action);
					switch (intval($row->action)) {
						case self::ACTION_ADD:
							$this_action = 'Added new';
							break;
						case self::ACTION_EDIT:
							$this_action = 'Edited';
							break;
						case self::ACTION_DELETE:
							$this_action = 'Deleted';
							break;
					}
					$html .= '<tr>
							<td>'.date("m/d/Y [g:ia]", strtotime($row->event_time)).'</td>
							<td>'.userRealNameByUmdid($row->user_id).'</td>
							<td><strong>'.$this_action.'</strong> '.$this->tableName(intval($row->tbl)).'</td>
							<td>'.$this->displayJsonVals(trim($row->old_vals)).'</td>
							<td>'.$this->displayJsonVals(trim($row->new_vals)).'</td>
						</tr>';
				}
			}
			@pg_free_result($rs);
			return $html_prefix.$html.$html_suffix;
		}
		
		
		public function displayJsonVals($obj)
		{
			$retval = '';
			
			if (empty($obj)) {
				return $retval;
			}
			
			$decoded = @json_decode($obj);
			//echo '<pre>';
			//var_dump($decoded);
			//echo '</pre>';
			
			if (empty($decoded)) {
				return $retval;
			}
			
			foreach ($decoded as $decoded_ojb) {
				if (strlen(trim($decoded_ojb->val)) > 0) {
					$retval .= '<strong>'.$decoded_ojb->col.':  </strong>'.$decoded_ojb->val.'<br>'.PHP_EOL;
				} else {
					$retval .= '<strong>'.$decoded_ojb->col.':  </strong><span style="font-family: inherit; color: dimgray;">[No Value]</span><br>'.PHP_EOL;
				}
			}
			
			return $retval;
		}
		
		
		public function verboseCmrConditions($condition_raw = null)
		{
			global $host_pa, $my_db_schema;
			$retval = '';
			$trans = array('input' => 'Test Result', '&&' => 'AND');
			
			if (empty($condition_raw)) {
				return $retval;
			}
			
			$sql = 'SELECT id, concept 
				FROM '.$my_db_schema.'vocab_master_vocab 
				WHERE category = '.$my_db_schema.'vocab_category_id(\'test_result\') 
				ORDER BY concept;';
			$rs = @pg_query($host_pa, $sql);
			if ($rs !== false) {
				while ($row = @pg_fetch_object($rs)) {
					$trans[trim($row->id)] = "'".trim($row->concept)."'";
				}
			}
			
			if (count($trans) > 0) {
				$retval = strtr($condition_raw, $trans);
			}
			
			return $retval;
		}
		
		
		public function verboseInterpRuleConditions($condition_raw = null)
		{
			$trans = array('input' => 'Result Value', '&&' => 'AND');
			
			if (empty($condition_raw)) {
				return '';
			}
			
			return strtr($condition_raw, $trans);
		}
		
	}
	
?>