<?php

	/**
	 * Rule parameter class
	 * 
	 * Allows for parameters used in rule classes (e.g. NotificationContainer) to have forced typing
	 *
	 * @author Josh Ridderhoff <jridderhoff@utah.gov>
	 */
	class RuleParameter
	{
		protected $data_type;
		protected $value;
		protected $type_arr = array('Boolean', 'Integer', 'Float', 'String');
		
		/*
		 * 'Parameter value from' types
		 */
		const PARAMVALUE_USER = 1;		// parameter value comes from user-entry
		const PARAMVALUE_LOOKUP = 2;	// parameter value comes from lookup table
		const PARAMVALUE_VOCAB = 3;		// parameter value comes from master vocabulary
		
		public function __construct($type) {
			if (in_array($type, $this->type_arr)) {
				$this->data_type = $type;
				if ($this->data_type == 'Boolean') {
					$this->value = false;
				}
			}
		}
		
		public function __toString() {
			if (is_null($this->value)) {
				return 'NULL';
			} else {
				return trim($this->value);
			}
		}
		
		public function getValue() {
			if (!is_null($this->data_type) && !is_null($this->value)) {
				switch ($this->data_type) {
					case 'Boolean':
						return (bool) $this->value;
						break;
					case 'Integer':
						return intval($this->value);
						break;
					case 'Float':
						return floatval($this->value);
						break;
					default:
						return trim($this->value);
						break;
				}
			} else {
				return null;
			}
		}
		
		public function setValue($input) {
			if (isset($this->data_type) && !empty($this->data_type)) {
				switch ($this->data_type) {
					case 'Boolean':
						$this->value = ((filter_var($input, FILTER_VALIDATE_BOOLEAN)) ? true : false);
						return true;
						break;
					case 'Integer':
						$this->value = intval($input);
						return true;
						break;
					case 'Float':
						$this->value = floatval($input);
						return true;
						break;
					default:
						$this->value = filter_var(trim($input), FILTER_SANITIZE_STRING);
						return true;
						break;
				}
			} else {
				return false;
			}
		}
		
		public function getDataType() {
			return $this->data_type;
		}
	
	}
	
?>