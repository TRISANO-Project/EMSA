<?php

	function displayRule($rule_id, $parent_chain_id = 0) {
		global $host_pa, $my_db_schema, $nc;
		
		$next_chain_id = $nc->getNextChain($rule_id, $parent_chain_id, 0);
		$left_id = $next_chain_id;
		
		echo '<div class="rule_chain ui-corner-all">';
		
		do {
			$chain_sql = 'SELECT 
						c.id AS id, c.left_id AS left_id, c.link_type AS link_type, c.link_id AS link_id, c.left_operator_id AS operator_id, 
						o.label AS operator, 
						l.type_left AS type_left, l.type_right AS type_right, l.operand_left AS operand_left, l.operand_right AS operand_right, l.operator_id AS link_operator_id 
					FROM '.$my_db_schema.'bn_expression_chain c 
					LEFT JOIN '.$my_db_schema.'structure_operator o ON (c.left_operator_id = o.id) 
					LEFT JOIN '.$my_db_schema.'bn_expression_link l ON (c.link_id = l.id) 
					WHERE c.id = '.$next_chain_id.';';
			$chain_rs = @pg_query($host_pa, $chain_sql);
			
			if (($chain_rs === false) || (intval(@pg_num_rows($chain_rs)) !== 1)) {
				echo '<div class="link_wrapper"><span class="ui-icon ui-icon-elrerror" style="display: inline-block; vertical-align: top; margin-right: 5px;"></span>No Conditions found!  Add a new Condition or Condition Group... ';
				echo '<div class="rule_actions">';
				$edit_addlink_params = array(
						'rule_id' => $rule_id,
						'parent_chain_id' => $parent_chain_id,
						'left_id' => 0
					);
				echo '<button type="button" class="chain_action_add_expression" value=\''.@json_encode($edit_addlink_params).'\'>Add new Condition</button>';
				$edit_addgroup_params = array(
						'rule_id' => $rule_id,
						'parent_chain_id' => $parent_chain_id,
						'left_id' => 0
					);
				echo '<button type="button" class="chain_action_add_group" value=\''.@json_encode($edit_addgroup_params).'\'>Add new Condition Group</button>';
				echo '<button type="button" class="chain_action_edit" value="" disabled>Edit Condition</button>';
				echo '<button type="button" class="chain_action_delete" value="" disabled>Delete Condition/Group</button>';
				echo '</div>';
				echo '</div>';
			} else {
				$chain = @pg_fetch_object($chain_rs);
				echo '<div class="link_wrapper">';
				if (intval($chain->operator_id) !== 0) {
					echo '<div class="rule_operator ui-corner-all">'.$chain->operator.'</div>';
				} else {
					echo '<div class="rule_operator ui-corner-all">--</div>';
				}
				if (intval($chain->link_type) === NotificationContainer::LINKTYPE_LINK) {
					echo displayLink(intval($chain->link_id));
				} elseif (intval($chain->link_type) === NotificationContainer::LINKTYPE_CHAIN) {
					displayRule($rule_id, intval($chain->id));
				} else {
					suicide('Invalid logical expression type.');
				}
				echo '<div class="rule_actions">';
				$edit_addlink_params = array(
						'rule_id' => $rule_id,
						'parent_chain_id' => $parent_chain_id,
						'left_id' => intval($chain->id)
					);
				echo '<button type="button" class="chain_action_add_expression" value=\''.@json_encode($edit_addlink_params).'\'>Add new Condition</button>';
				$edit_addgroup_params = array(
						'rule_id' => $rule_id,
						'parent_chain_id' => $parent_chain_id,
						'left_id' => intval($chain->id)
					);
				echo '<button type="button" class="chain_action_add_group" value=\''.@json_encode($edit_addgroup_params).'\'>Add new Condition Group</button>';
				if (intval($chain->link_type) === NotificationContainer::LINKTYPE_LINK) {
					$edit_link_params = array(
						'rule_id' => $rule_id,
						'parent_chain_id' => $parent_chain_id,
						'link_id' => intval($chain->link_id),
						'operator_id' => intval($chain->operator_id),
						'left_id' => intval($chain->left_id), 
						'link_operator_id' => intval($chain->link_operator_id),
						'type_left' => intval($chain->type_left),
						'type_right' => intval($chain->type_right),
						'operand_left' => trim($chain->operand_left),
						'operand_right' => trim($chain->operand_right)
					);
					echo '<button type="button" class="chain_action_edit" value=\''.@json_encode($edit_link_params).'\'>Edit Condition</button>';
					$delete_link_params = array(
							'type' => 'link',
							'rule_id' => $rule_id,
							'link_id' => intval($chain->id),
							'left_id' => intval($chain->left_id)
						);
					echo '<button type="button" class="chain_action_delete" value=\''.@json_encode($delete_link_params).'\'>Delete Condition</button>';
				} else {
					$edit_group_params = array();
					echo '<button type="button" class="chain_action_edit" value=\''.@json_encode($edit_group_params).'\' disabled>Edit Condition</button>';
					$delete_group_params = array(
							'type' => 'chain',
							'rule_id' => $rule_id,
							'parent_chain_id' => intval($chain->id),
							'left_id' => intval($chain->left_id)
						);
					echo '<button type="button" class="chain_action_delete" value=\''.@json_encode($delete_group_params).'\'>Delete Group</button>';
				}
				echo '</div>';
				echo '</div>';
			}
			
			$next_chain_id = $nc->getNextChain($rule_id, $parent_chain_id, $left_id);
			$left_id = $next_chain_id;
		} while ($next_chain_id !== false);
		
		echo '</div>';
	}
	
	function displayLink($link_id = null) {
		global $host_pa, $my_db_schema, $nc;
		
		$link_sql = 'SELECT l.type_left AS type_left, l.type_right AS type_right, l.operand_left AS operand_left, l.operand_right AS operand_right, o.label AS operator 
				FROM '.$my_db_schema.'bn_expression_link l 
				LEFT JOIN '.$my_db_schema.'structure_operator o ON (l.operator_id = o.id) 
				WHERE l.id = '.intval($link_id).';';
		$link_rs = @pg_query($host_pa, $link_sql);
		
		if (($link_rs === false) || (intval(@pg_num_rows($link_rs)) !== 1)) {
			suicide('Unable to evaluate expression link:  Link not found.');
		} else {
			$link = @pg_fetch_object($link_rs);
			
			$operand_left = $link->operand_left;
			$operand_right = $link->operand_right;
			
			echo '<div class="rule_link ui-corner-all">';
			
			if ((intval($link->type_left) === NotificationContainer::OPTYPE_PARAMETER) && (intval($link->type_right) === NotificationContainer::OPTYPE_PARAMETER)) {
				// parameter to parameter
				echo '<label>'.htmlentities(displayParameterLabel($operand_left), ENT_QUOTES, 'UTF-8').'</label>';
				echo '<div class="rule_operator ui-corner-all">'.htmlentities(trim($link->operator), ENT_QUOTES, 'UTF-8').'</div>';
				echo '<label>'.htmlentities(displayParameterLabel($operand_right), ENT_QUOTES, 'UTF-8').'</label>';
			} elseif ((intval($link->type_left) === NotificationContainer::OPTYPE_PARAMETER) && (intval($link->type_right) === NotificationContainer::OPTYPE_VALUE)) {
				// parameter to value
				echo '<label>'.htmlentities(displayParameterLabel($operand_left), ENT_QUOTES, 'UTF-8').'</label>';
				if (($nc->getDataType($operand_left) === 'Boolean') && (intval($operand_right) === 1)) {
					echo '<div class="rule_operator ui-corner-all">is TRUE</div>';
				} elseif (($nc->getDataType($operand_left) === 'Boolean') && (intval($operand_right) === 0)) {
					echo '<div class="rule_operator ui-corner-all">is FALSE</div>';
				} else {
					echo '<div class="rule_operator ui-corner-all">'.htmlentities(trim($link->operator), ENT_QUOTES, 'UTF-8').'</div>';
					echo '&quot;'.htmlentities($operand_right, ENT_QUOTES, 'UTF-8').'&quot;';
				}
			} else {
				// ain't no way, no how...
				suicide('Unable to evaluate expression link:  Invalid operand types specified.');
			}
			echo '</div>';
		}
	}
	
	function displayParameterLabel($varname = null) {
		global $host_pa, $my_db_schema;
		
		if (!empty($varname)) {
			$sql = 'SELECT label FROM '.$my_db_schema.'bn_rule_parameters WHERE varname = \''.pg_escape_string($varname).'\';';
			$rs = @pg_query($host_pa, $sql);
			if ($rs !== false) {
				return trim(@pg_fetch_result($rs, 0, label));
			} else {
				return '';
			}
		} else {
			return '';
		}
	}
	
	function deleteChain($rule_id = null, $parent_chain_id = 0) {
		global $host_pa, $my_db_schema, $nc;
		
		$delete_links = array();
		$delete_chains = array();
		$outer_sql = 'SELECT id, link_id, link_type FROM '.$my_db_schema.'bn_expression_chain 
			WHERE (rule_id = '.intval($rule_id).') AND (parent_chain_id = '.intval($parent_chain_id).');';
		$outer_rs = @pg_query($host_pa, $outer_sql);
		if (($outer_rs !== false) && (@pg_num_rows($outer_rs) > 0)) {
			while ($outer_row = @pg_fetch_object($outer_rs)) {
				if (intval($outer_row->link_type) === NotificationContainer::LINKTYPE_LINK) {
					$delete_links[] = intval($outer_row->link_id);
					$delete_chains[] = intval($outer_row->id);
				} else {
					deleteChain($rule_id, intval($outer_row->id)); // recursively delete descendant chains
				}
			}
		}
		
		$delete_chains[] = intval($parent_chain_id);
		
		if (count($delete_links) > 0) {
			$final_links_sql = 'DELETE FROM '.$my_db_schema.'bn_expression_link WHERE id IN ('.implode(', ', $delete_links).');';
			@pg_query($host_pa, $final_links_sql);
			//echo $final_links_sql.'<br>';
		}
		
		if (count($delete_chains) > 0) {
			$final_chains_sql = 'DELETE FROM '.$my_db_schema.'bn_expression_chain WHERE id IN ('.implode(', ', $delete_chains).');';
			@pg_query($host_pa, $final_chains_sql);
			//echo $final_chains_sql.'<br>';
		}
		
		return true;
	}
	
	
	function deleteLink($rule_id = null, $link_id = 0) {
		global $host_pa, $my_db_schema, $nc;
		
		$delete_links = array();
		$delete_chains = array();
		$outer_sql = 'SELECT id, link_id, link_type FROM '.$my_db_schema.'bn_expression_chain 
			WHERE (rule_id = '.intval($rule_id).') AND (id = '.intval($link_id).');';
		$outer_rs = @pg_query($host_pa, $outer_sql);
		if (($outer_rs !== false) && (@pg_num_rows($outer_rs) > 0)) {
			while ($outer_row = @pg_fetch_object($outer_rs)) {
				if (intval($outer_row->link_type) === NotificationContainer::LINKTYPE_LINK) {
					$delete_links[] = intval($outer_row->link_id);
					$delete_chains[] = intval($outer_row->id);
				} else {
					deleteChain($rule_id, intval($outer_row->id)); // recursively delete descendant chains
				}
			}
		} else {
			return false;
		}
		
		if (count($delete_links) > 0) {
			$final_links_sql = 'DELETE FROM '.$my_db_schema.'bn_expression_link WHERE id IN ('.implode(', ', $delete_links).');';
			@pg_query($host_pa, $final_links_sql);
			//echo $final_links_sql.'<br>';
		}
		
		if (count($delete_chains) > 0) {
			$final_chains_sql = 'DELETE FROM '.$my_db_schema.'bn_expression_chain WHERE id IN ('.implode(', ', $delete_chains).');';
			@pg_query($host_pa, $final_chains_sql);
			//echo $final_chains_sql.'<br>';
		}
		
		return true;
	}
	
	
	function promoteChain($rule_id = -1, $old_left_id = -1, $new_left_id = -1) {
		global $host_pa, $my_db_schema;
		
		if ($new_left_id === 0) {
			$reset_operator_for_leftmost = ', left_operator_id = 0';
		} else {
			$reset_operator_for_leftmost = '';
		}
		
		$sql = 'UPDATE '.$my_db_schema.'bn_expression_chain SET left_id = '.intval($new_left_id).$reset_operator_for_leftmost.' WHERE (rule_id = '.intval($rule_id).') AND (left_id = '.intval($old_left_id).');';
		@pg_query($host_pa, $sql);
		//echo $sql.'<br>';
	}
	
	function demoteChain($rule_id = -1, $parent_chain_id = -1, $old_left_id = -1, $new_left_id = -1) {
		global $host_pa, $my_db_schema;
		
		$sql = 'UPDATE '.$my_db_schema.'bn_expression_chain SET left_id = '.intval($new_left_id).' WHERE (rule_id = '.intval($rule_id).') AND (parent_chain_id = '.intval($parent_chain_id).') AND (left_id = '.intval($old_left_id).') AND (id <> '.intval($new_left_id).');';
		@pg_query($host_pa, $sql);
		//echo $sql.'<br>';
	}

?>