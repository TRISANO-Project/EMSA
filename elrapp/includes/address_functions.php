<?php

	function getValidAddress($id) {
		global $host_pa, $my_db_schema, $props;
		$result = getEmsaDetail($id);
		$return_array = array();
		
		$xml = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <username>9999</username>
    <system>TRISANO</system>
    <trisano_health>
        <addresses>
            <postal_code>{$result['zip']}</postal_code>
            <street_number></street_number>
            <street_name>{$result['street']}</street_name>
			<city>{$result['city']}</city>
			<state>{$result['state']}</state>
        </addresses>
	</trisano_health>
</health_message>
EOX;

		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e->getMessage(), -1, 1);
		} catch (SoapFault $f) {
			suicide($f->getMessage(), -1, 1);
		}
		
		if ($client) {
			try {
				$result = $client->getGeocode(array("healthMessage" => $xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$return = simplexml_load_string($result->return);
			
			$return_array['street_name'] = $return->trisano_health->addresses->street_name;
			$return_array['postal_code'] = $return->trisano_health->addresses->postal_code;
			$return_array['city'] = $return->trisano_health->addresses->city;
			$return_array['state'] = $result['state'];
			$return_array['valid_address'] = $return->trisano_health->addresses->google_address;
			
			return $return_array;
		}
	}
	
	
	
	
	function updateValidatedAddress($id, $address) {
		global $host_pa, $my_db_schema;
		
		$addy = explode(',', $address);
		$statezip = explode(' ', trim($addy[2]));
		$result = getEmsaDetail($id);
		$clean_xml = str_replace("<?xml version=\"1.0\"?>","",$result['master_xml']);
		
		$xml = simplexml_load_string($clean_xml);
		$sxe = new SimpleXMLElement($xml->asXML());
		
		$sxe->person->street = $addy[0];
		$sxe->person->city = $addy[1];
		$sxe->person->state = $statezip[0];
		$sxe->person->zip = $statezip[1];
		
		$xml = $sxe->asXML();
		
		$sql = "UPDATE ".$my_db_schema."system_messages SET master_xml='".pg_escape_string($xml)."',address_is_valid=1 WHERE id=".$id;
		return get_db_result_set($host_pa, $sql);
	}

?>