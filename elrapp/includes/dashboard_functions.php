<?php

	/**
	 * Functions used by Dashboard reports
	 */
	
	
	
	
	/**
	 * Returns Queue summary data for 'ELR Overview' report for Google Charts widget.
	 *
	 * @param string $from_date Start date for reporting period.  If blank, current system time is used.
	 * @param string $to_date End date for reporting period.  If blank, current system time is used.
	 * @return array Array of Queue stats
	 */
	function getDashboardSummary($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$dashboard_summary = array();
		
		$sql = 'SELECT 
					--sum(case when sm.final_status=12 AND (sm.deleted = 0 OR sm.deleted IS NULL)then 1 else 0 end) as pending, 
					sum(case when sm.final_status=14 AND (sm.deleted = 0 OR sm.deleted IS NULL)then 1 else 0 end) as assigned, 
					sum(case when sm.final_status=2 AND (sm.deleted = 0 OR sm.deleted IS NULL)then 1 else 0 end) as gray, 
					sum(case when sm.final_status=17 AND (sm.deleted = 0 OR sm.deleted IS NULL)then 1 else 0 end) as entry, 
					sum(case when sm.final_status=3 AND (sm.deleted = 0 OR sm.deleted IS NULL) then 1 else 0 end) as exception, 
					--sum(case when sm.final_status=19 AND (sm.deleted = 0 OR sm.deleted IS NULL) then 1 else 0 end) as qa, 
					sum(case when (sm.deleted != 0 AND sm.deleted IS NOT NULL) then 1 else 0 end) as deleted, 
					sum(case when (sm.final_status=0 or sm.final_status=12 or sm.final_status=19 or sm.final_status is null) AND (sm.deleted = 0 OR sm.deleted IS NULL) then 1 else 0 end) as orphan, 
					count(sm.id) as total_submit 
				FROM '.$my_db_schema.'system_messages sm 
				LEFT JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id) 
				WHERE '.$lab_filter_stmt.'om.created_at::date >=\''.$clean['from_date'].'\' AND om.created_at::date <=\''.$clean['to_date'].'\' ';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $dashboard_summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$dashboard_summary['Total'] = $row->total_submit;
			$dashboard_summary['Entry'] = $row->entry;
			//$dashboard_summary['Pending'] = $row->pending;
			$dashboard_summary['Assigned'] = $row->assigned;
			$dashboard_summary['Gray'] = $row->gray;
			$dashboard_summary['Exception'] = $row->exception;
			//$dashboard_summary['QA'] = $row->qa;
			$dashboard_summary['Deleted'] = $row->deleted;
			$dashboard_summary['Unknown'] = $row->orphan;
		}
		
		@pg_free_result($rs);
		return $dashboard_summary;
	}
	
	
	
	
	function getDashboardAutomationFactor($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$dashboard_summary = array();
		
		$sql = 'SELECT 
					SUM(CASE WHEN autosub.auto = 0 THEN 1 ELSE 0 END) AS count_auto, 
					SUM(CASE WHEN autosub.auto > 0 THEN 1 ELSE 0 END) AS count_manual
				FROM (
					SELECT SUM(CASE WHEN user_id <> \'9999\' THEN 1 ELSE 0 END) AS auto
					FROM '.$my_db_schema.'system_messages_audits sma1 WHERE sma1.system_message_id IN (
						SELECT DISTINCT sma.system_message_id 
						FROM '.$my_db_schema.'system_messages_audits sma 
						INNER JOIN '.$my_db_schema.'system_messages sm ON (sma.system_message_id = sm.id) 
						INNER JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id) 
						WHERE '.$lab_filter_stmt.'om.created_at::date >= \''.$clean['from_date'].'\' AND om.created_at::date <= \''.$clean['to_date'].'\' 
						AND sma.message_action_id IN (22, 23, 24, 28, 29) 
						AND (sm.deleted IS NULL OR sm.deleted NOT IN (1, 2))
					)
					GROUP BY sma1.system_message_id
				) AS autosub;';
				
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $dashboard_summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$dashboard_summary['Automatically Assigned'] = $row->count_auto;
			$dashboard_summary['Manually Assigned'] = $row->count_manual;
		}
		
		@pg_free_result($rs);
		return $dashboard_summary;
	}
	
	
	
	
	function getDashboardBlacklistSummary($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$summary = array();
		
		$sql = 'SELECT o.connector, count(o.connector) AS counter FROM '.$my_db_schema.'system_original_messages o
			WHERE NOT EXISTS (SELECT m.id FROM '.$my_db_schema.'system_messages m WHERE m.original_message_id = o.id)
			AND o.created_at::date >=\''.$from_date.'\' AND o.created_at::date <=\''.$to_date.'\' 
			GROUP BY o.connector ORDER BY o.connector;';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$summary[$row->connector] = $row->counter;
		}
		
		@pg_free_result($rs);
		return $summary;
	}
	
	
	
	
	function getDashboardMessageQuality($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$flag_array = array(
			EMSA_FLAG_QA_MANDATORY, 
			EMSA_FLAG_QA_CODING, 
			EMSA_FLAG_QA_MQF,
			EMSA_FLAG_DE_ERROR,
			EMSA_FLAG_FIX_DUPLICATE,
			EMSA_FLAG_DE_OTHER, 
			EMSA_FLAG_DE_NEEDFIX
		);
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$summary = array();
		
		$sql = 'SELECT mf.label AS category, count(sm.message_flags) AS counter
				FROM '.$my_db_schema.'system_message_flags mf 
				LEFT JOIN (SELECT sm.* FROM '.$my_db_schema.'system_messages sm 
					INNER JOIN '.$my_db_schema.'system_original_messages o ON sm.original_message_id = o.id 
					WHERE '.$lab_filter_stmt.'o.created_at::date >=\''.$from_date.'\' AND o.created_at::date <=\''.$to_date.'\'
				) sm ON (power(2, mf.id)::integer & sm.message_flags <> 0) 
				WHERE power(2, mf.id) IN ('.implode(',', $flag_array).') 
				GROUP BY 1
				ORDER BY 1;';
		$rs = @pg_query($host_pa, $sql);
		if ($rs === false) {
			return $summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$summary[$row->category] = $row->counter;
		}
		
		@pg_free_result($rs);
		return $summary;
	}
	
	
	
	
	function getDashboardNewCase($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$summary = getBlankDashboardDiseaseCategoryArray();
		
		$category_cdcgroup = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'disease_category\');'), 0, 0));
		$category_condition = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'condition\');'), 0, 0));
		
		$sql = 'SELECT mv_outer.concept AS category, count(sm.id) AS counter 
				FROM '.$my_db_schema.'system_messages sm LEFT JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id), '.$my_db_schema.'vocab_master_vocab mv_outer
				WHERE '.$lab_filter_stmt.'mv_outer.category = '.$category_cdcgroup.' and sm.disease in (
					select mv.concept from '.$my_db_schema.'vocab_master_vocab mv where mv.category = '.$category_condition.' and mv.id in (
						select condition from '.$my_db_schema.'vocab_master_condition mc WHERE mc.disease_category = mv_outer.id
					)
				) 
				and om.created_at::date >=\''.$clean['from_date'].'\' AND om.created_at::date <=\''.$clean['to_date'].'\' 
				and sm.id in (
					select distinct sma.system_message_id from elr.system_messages_audits sma where sma.message_action_id in (23, 24)
				) group by 1 order by 1;';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$summary[$row->category] = $row->counter;
		}
		
		@pg_free_result($rs);
		return $summary;
	}
	
	
	
	
	function getDashboardAppendedCase($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$summary = getBlankDashboardDiseaseCategoryArray();
		
		$category_cdcgroup = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'disease_category\');'), 0, 0));
		$category_condition = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'condition\');'), 0, 0));
		
		$sql = 'SELECT mv_outer.concept AS category, 
					count(distinct sm.lab_result_id) AS total_labs
				FROM '.$my_db_schema.'system_messages sm LEFT JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id), '.$my_db_schema.'vocab_master_vocab mv_outer
				WHERE '.$lab_filter_stmt.'mv_outer.category = '.$category_cdcgroup.' and sm.disease in (
					select mv.concept from '.$my_db_schema.'vocab_master_vocab mv where mv.category = '.$category_condition.' and mv.id in (
						select condition from '.$my_db_schema.'vocab_master_condition mc WHERE mc.disease_category = mv_outer.id
					)
				) 
				and om.created_at::date >=\''.$clean['from_date'].'\' AND om.created_at::date <=\''.$clean['to_date'].'\' 
				and sm.id in (
					select distinct sma.system_message_id from elr.system_messages_audits sma where sma.message_action_id in (22, 28, 29)
				) group by 1 order by 1;';
				//echo '<pre>'.$sql.'</pre>';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$summary[$row->category] = array(
				'labs_updated' => $row->total_labs
			);
		}
		
		@pg_free_result($rs);
		return $summary;
	}
	
	
	
	
	function getDashboardDiscardedCase($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$summary = getBlankDashboardDiseaseCategoryArray();
		
		$category_cdcgroup = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'disease_category\');'), 0, 0));
		$category_condition = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'condition\');'), 0, 0));
		
		$sql = 'SELECT mv_outer.concept AS category, count(sm.id) AS counter 
				FROM '.$my_db_schema.'system_messages sm LEFT JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id), '.$my_db_schema.'vocab_master_vocab mv_outer
				WHERE '.$lab_filter_stmt.'mv_outer.category = '.$category_cdcgroup.' and sm.disease in (
					select mv.concept from '.$my_db_schema.'vocab_master_vocab mv where mv.category = '.$category_condition.' and mv.id in (
						select condition from '.$my_db_schema.'vocab_master_condition mc WHERE mc.disease_category = mv_outer.id
					)
				) 
				and om.created_at::date >=\''.$clean['from_date'].'\' AND om.created_at::date <=\''.$clean['to_date'].'\' 
				and sm.id in (
					select distinct sma.system_message_id from elr.system_messages_audits sma where sma.message_action_id in (7)
				) group by 1 order by 1;';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$summary[$row->category] = $row->counter;
		}
		
		@pg_free_result($rs);
		return $summary;
	}
	
	
	
	
	function getDashboardGraylistSummary($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$summary = getBlankDashboardDiseaseCategoryArray();
		
		$category_cdcgroup = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'disease_category\');'), 0, 0));
		$category_condition = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'condition\');'), 0, 0));
		
		$sql = 'SELECT mv_outer.concept AS category, count(sm.id) AS counter 
				FROM '.$my_db_schema.'system_messages sm LEFT JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id), '.$my_db_schema.'vocab_master_vocab mv_outer
				WHERE '.$lab_filter_stmt.'mv_outer.category = '.$category_cdcgroup.' AND sm.disease IN (
					SELECT mv.concept FROM '.$my_db_schema.'vocab_master_vocab mv WHERE mv.category = '.$category_condition.' AND mv.id IN (
						SELECT condition FROM '.$my_db_schema.'vocab_master_condition mc WHERE mc.disease_category = mv_outer.id
					)
				) 
				AND om.created_at::date >=\''.$clean['from_date'].'\' AND om.created_at::date <=\''.$clean['to_date'].'\' 
				AND sm.final_status = '.GRAY_STATUS.' 
				AND ((sm.deleted IS NULL) OR (sm.deleted NOT IN (1, 2)))
				group by 1 order by 1;';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$summary[$row->category] = $row->counter;
		}
		
		@pg_free_result($rs);
		return $summary;
	}
	
	
	
	
	function getDashboardLab($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = "";
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = "sm.lab_id = ".intval($_SESSION['dashboard_lab_filter'])." AND ";
		}
		
		$dashboard_lab = array();
		$dashboard_labs_received = array();
		
		// get list of labs where messages have been received during the date range
		$labs_sql = "SELECT DISTINCT(sm.lab_id) AS lab_id, l.ui_name AS lab_name FROM ".$my_db_schema."system_messages sm 
			INNER JOIN ".$my_db_schema."system_original_messages om ON (om.id = sm.original_message_id) 
			INNER JOIN ".$my_db_schema."structure_labs l ON (sm.lab_id = l.id) 
			WHERE ".$lab_filter_stmt."om.created_at::date >='".$from_date."' AND om.created_at::date <='".$to_date."';";
		$lab_rs = @pg_query($host_pa, $labs_sql);
		if (!$lab_rs) {
			trigger_error("Unable to connect to database", E_USER_ERROR);
			return $dashboard_lab;
		}
		
		while ($lab_row = @pg_fetch_object($lab_rs)) {
			$dashboard_labs_received[$lab_row->lab_id] = $lab_row->lab_name;
		}
		@pg_free_result($lab_rs);
		
		// stick the Google DataTable column headers into the first element of the array
		$dashboard_lab['headers'] = array("Day");
		array_push($dashboard_lab['headers'], 'Total');
		array_push($dashboard_lab['headers'], 'Certainty');
		foreach($dashboard_labs_received as $dblr_key => $dblr_val) {
			array_push($dashboard_lab['headers'], $dblr_val);
		}
		
		//todo:  populate $dashboard_lab with every date between date range and 0 values, update later in query
		$utime_start = strtotime($from_date);
		$utime_end = strtotime($to_date);
		$utime_temp = $utime_start;
		
		while ($utime_temp <= $utime_end) {
			unset($parsed_utime_temp);
			$parsed_utime_temp = getdate($utime_temp);
			$dashboard_lab[$parsed_utime_temp['year']."-".$parsed_utime_temp['mon']."-".$parsed_utime_temp['mday']] = array('Total' => 0, 'Certainty' => 'false');
			foreach($dashboard_labs_received as $dblr_key => $dblr_val) {
				$dashboard_lab[$parsed_utime_temp['year']."-".$parsed_utime_temp['mon']."-".$parsed_utime_temp['mday']][$dblr_val] = 0;
			}
			$utime_temp = strtotime("+1 day", $utime_temp);
		}
		
		// get results back per-lab and per-day within the date range
		$sql = "SELECT
					extract(year from om.created_at) as created_year, 
					extract(month from om.created_at) as created_month, 
					extract(day from om.created_at) as created_day ";
		foreach($dashboard_labs_received as $dblr_key => $dblr_val) {
			$sql .= ", sum(case when sm.lab_id = ".$dblr_key." then 1 else 0 end) as \"".$dblr_val."_count\"";
		}
		$sql .= "FROM ".$my_db_schema."system_messages sm 
					INNER JOIN ".$my_db_schema."system_original_messages om ON (om.id = sm.original_message_id) 
					INNER JOIN ".$my_db_schema."structure_labs l ON (sm.lab_id = l.id) 
					WHERE ".$lab_filter_stmt."om.created_at::date >='".$from_date."' AND om.created_at::date <='".$to_date."' 
					GROUP BY 1, 2, 3
					ORDER BY 1, 2, 3;";
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			trigger_error("Unable to connect to database", E_USER_ERROR);
			return $dashboard_lab;
		}
		
		while ($row = @pg_fetch_array($rs)) {
			//$dashboard_lab[$row['created_year']."-".$row['created_month']."-".$row['created_day']] = array();
			foreach($dashboard_labs_received as $dblr_key => $dblr_val) {
				$dashboard_lab[$row['created_year']."-".$row['created_month']."-".$row['created_day']][$dblr_val] = intval($row[$dblr_val.'_count']);
			}
		}
		
		foreach ($dashboard_lab as $db_total_key => $db_total_arr) {
			if ($db_total_key != 'headers') {
				$dashboard_lab[$db_total_key]['Total'] = array_sum($db_total_arr);
				$dashboard_lab[$db_total_key]['Certainty'] = 'false';
			}
		}
		
		@pg_free_result($rs);
		return $dashboard_lab;
	}
	
	
	
	
	function getDashboardConditionSummary($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = "";
		$inner_lab_filter_stmt = "";
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = "sm.lab_id = ".intval($_SESSION['dashboard_lab_filter'])." AND ";
			$inner_lab_filter_stmt = "sm3.lab_id = ".intval($_SESSION['dashboard_lab_filter'])." AND ";
			
		}
		
		$dashboard_summary = array();
		
		$sql = "SELECT 
					DISTINCT sm.disease AS condition, 
					count(*) AS total_received, 
					(select count(distinct sm3.lab_result_id) 
						FROM ".$my_db_schema."system_messages sm3 
						INNER JOIN ".$my_db_schema."system_original_messages om3 ON (sm3.original_message_id = om3.id) 
						WHERE ".$inner_lab_filter_stmt."om3.created_at::date >='".$clean['from_date']."' AND om3.created_at::date <='".$clean['to_date']."' AND sm3.disease = sm.disease
						AND sm3.id in (
							select distinct sma.system_message_id from elr.system_messages_audits sma where sma.message_action_id in (22, 28, 29, 23, 24)
						)
					) AS total_labs
				FROM ".$my_db_schema."system_messages sm 
				INNER JOIN ".$my_db_schema."system_original_messages om ON (sm.original_message_id = om.id) 
				WHERE ".$lab_filter_stmt."om.created_at::date >='".$clean['from_date']."' AND om.created_at::date <='".$clean['to_date']."'
				GROUP BY 1 ORDER BY 2 DESC;";
				//echo '<pre>'.$sql.'</pre>';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $dashboard_summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			$dashboard_summary[((strlen($row->condition) > 0) ? $row->condition : "Missing/Unknown")] = array(
				'total_received' => $row->total_received,
				'total_labs' => $row->total_labs
			);
		}
		
		@pg_free_result($rs);
		return $dashboard_summary;
	}
	
	
	
	
	function getBlankDashboardDiseaseCategoryArray() {
		global $host_pa, $my_db_schema;
		
		$template = array();
		
		$sql = 'SELECT concept FROM '.$my_db_schema.'vocab_master_vocab WHERE category = elr.vocab_category_id(\'disease_category\') ORDER BY concept;';
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false && @pg_num_rows($rs) > 0) {
			while ($row = @pg_fetch_object($rs)) {
				$template[trim($row->concept)] = array();
			}
			@pg_free_result($rs);
		}
		
		return $template;
	}
	
	
	
	
	function getAvgCMRCreateTime($from_date, $to_date) {
		global $host_pa, $my_db_schema;
		
		$from_date_utime = strtotime(trim($from_date));
		$to_date_utime = strtotime(trim($to_date));
		if ($from_date_utime !== false) {
			$clean['from_date'] = date("m/d/Y", $from_date_utime);
		} else {
			$clean['from_date'] = date("m/d/Y", time());
		}
		if ($to_date_utime !== false) {
			$clean['to_date'] = date("m/d/Y", $to_date_utime);
		} else {
			$clean['to_date'] = date("m/d/Y", time());
		}
		
		$lab_filter_stmt = '';
		if (isset($_SESSION['dashboard_lab_filter']) && (intval($_SESSION['dashboard_lab_filter']) > 0)) {
			$lab_filter_stmt = 'sm.lab_id = '.intval($_SESSION['dashboard_lab_filter']).' AND ';
		}
		
		$summary = getBlankDashboardDiseaseCategoryArray();
		$summary_temp = getBlankDashboardDiseaseCategoryArray();
		
		$category_cdcgroup = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'disease_category\');'), 0, 0));
		$category_condition = intval(@pg_fetch_result(@pg_query($host_pa, 'SELECT '.$my_db_schema.'vocab_category_id(\'condition\');'), 0, 0));
		
		$sql = 'SELECT mv_outer.concept AS category, sm.master_xml AS master_xml, sm.assigned_date AS assigned_date
				FROM '.$my_db_schema.'system_messages sm LEFT JOIN '.$my_db_schema.'system_original_messages om ON (sm.original_message_id = om.id), '.$my_db_schema.'vocab_master_vocab mv_outer
				WHERE '.$lab_filter_stmt.'mv_outer.category = '.$category_cdcgroup.' and sm.disease in (
					select mv.concept from '.$my_db_schema.'vocab_master_vocab mv where mv.category = '.$category_condition.' and mv.id in (
						select condition from '.$my_db_schema.'vocab_master_condition mc WHERE mc.disease_category = mv_outer.id
					)
				) 
				and om.created_at::date >=\''.$clean['from_date'].'\' AND om.created_at::date <=\''.$clean['to_date'].'\' 
				and sm.id in (
					select distinct sma.system_message_id from elr.system_messages_audits sma where sma.message_action_id in (23, 24)
				) group by 1, 2, 3 order by 1;';
		$rs = @pg_query($host_pa, $sql);
		if (!$rs) {
			return $summary;
		}
		
		while ($row = @pg_fetch_object($rs)) {
			unset($this_master_xml);
			unset($this_specimen_collection_time);
			unset($this_assigned_time);
			unset($this_time_diff);
			$this_master_xml = simplexml_load_string($row->master_xml);
			$this_specimen_collection_time = strtotime(trim($this_master_xml->labs->collection_date));
			$this_assigned_time = strtotime(trim($row->assigned_date));
			$this_time_diff = elapsedTime($this_specimen_collection_time, $this_assigned_time);
			
			if (!is_array($summary_temp[$row->category])) {
				$summary_temp[$row->category] = array();
			}
			
			$summary_temp[$row->category][] = $this_time_diff;
		}
		
		foreach ($summary_temp as $temp_category => $temp_time_diffs) {
			unset($this_cat_time_avg);
			if (is_array($temp_time_diffs) && count($temp_time_diffs) > 0) {
				$this_cat_time_avg = round((array_sum($temp_time_diffs) / count($temp_time_diffs)));
			}
			$summary[$temp_category] = elapsedTimeToString($this_cat_time_avg, 1);
		}
		
		//var_dump($summary);
		
		@pg_free_result($rs);
		return $summary;
	}
	
	
	
	
	/**
	 * Checks for orphaned messages in order to display warning on dashboard
	 * 
	 * @return int Number of messages found
	 */
	function getOrphanedMessageCount() {
		global $host_pa, $my_db_schema;
		
		$count = 0;
		
		$sql = 'SELECT count(sm.id) AS counter
				FROM '.$my_db_schema.'system_messages sm 
				LEFT JOIN '.$my_db_schema.'system_role_loincs_by_lab rd ON ( ( rd.lab_id = sm.lab_id ) AND ( rd.master_loinc_code = sm.loinc_code ) )
				WHERE (rd.master_loinc_code IS NULL) AND (sm.loinc_code IS NOT NULL) AND (
					(sm.deleted = 0) OR (sm.deleted IS NULL)
				) 
				;';

		$rs = @pg_query($host_pa, $sql);
		
		if ($rs !== false) {
			$count = intval(@pg_fetch_result($rs, 0, 'counter'));
		}
		
		@pg_free_result($rs);
		return $count;
		
	}
	
?>