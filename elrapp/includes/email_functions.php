<?php

	/**
	 * Send an e-mail with the specified parameters
	 *
	 * @param string $fromemail Sender's e-mail address
	 * @param string $fromname Sender's name
	 * @param string $mailsubject E-mail subject
	 * @param string $emailbody E-mail message body
	 * @param string $toemail Recipient's e-mail address
	 * @param string $toname Recipient's name
	 * @return bool
	 */
	function sendmail($fromemail, $fromname, $mailsubject, $emailbody, $toemail, $toname, $attach_path = null, $attach_filename = "") {
		//echo "in sendmail: - $fromemail - $fromname - $mailsubject - $mailbody - $toemail - $toname<br>";
		$Mail = new PHPMailer();
			$Mail->Priority = 3;
			$Mail->Encoding = "8bit";
			$Mail->CharSet = "iso-8859-1";
			$Mail->ContentType = "text/html";
			$Mail->From = $fromemail;
			$Mail->FromName = $fromname;
			$Mail->Sender = "";
			$Mail->Subject = $mailsubject;
			$Mail->AltBody = "";
			$Mail->WordWrap = 0;
			$Mail->Host = "send.utah.gov";
			$Mail->Port = 25;
			$Mail->Helo = "health.utah.gov";
			$Mail->SMTPAuth = false;
			$Mail->Username = "";
			$Mail->Password = "";
			$Mail->Sender = $fromemail;
			$Mail->Mailer = "smtp";
			$Mail->AddAddress($toemail, "$toname");
			$Mail->IsHTML(true);
			
			if (!is_null($attach_path)) {
				$Mail->AddAttachment($attach_path, filter_var($attach_filename, FILTER_SANITIZE_STRING));
			}
			
		/*
			if (preg_match('/<img src=["\']?([^"\']*)/i', $emailbody, $matches)) {
				$fn=parse_url($matches[1]);
				$fp="/srv/www" .$fn['path'];
				$cid="img" .$x;
				$Mail->AddEmbeddedImage($fp, $cid);
				$emailbody = preg_replace("/".$images[1][$i]."=\"".preg_quote($url, '/')."\"/Ui", $images[1][$i]."=\"".$cid."\"", $message);
				$emailbody .="<img src='cid:img$x'>";
			}
		*/
		
		$emailbody=str_replace("'","\"",$emailbody);
		preg_match_all("/(src)=\"(.*)\"/Ui", $emailbody, $images);
		
		if (isset($images[2])) {
			foreach ($images[2] as $i => $url) {
				// do not change urls for absolute images (thanks to corvuscorax)
				$filename = "/srv/www/phaccess/uploads/" .basename($url);
				$directory = dirname($url);
				($directory == '.') ? $directory = '' : '';
				$cid = 'cid:' . md5($filename);
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$mimeType  = getmime($filename);
				
				if ( $Mail->AddEmbeddedImage($filename, md5($filename), $filename, 'base64',$mimeType) ) {
					$emailbody = preg_replace("/".$images[1][$i]."=\"".preg_quote($url, '/')."\"/Ui", $images[1][$i]."=\"".$cid."\"", $emailbody);
				}
			}
		}
		
		$Mail->Body = $emailbody;
		//$Mail->MsgHTML($emailbody);
		
		if(!$Mail->Send()) {
			return false;
		} else {
			return true;
		}
		
	}
	
	
	
	
	/**
	 * Get back a list of e-mail addresses for all TriSano users assigned to the "ELR Notification" role.
	 * If no users found, or if an error occurs, returns FALSE.
	 *
	 * @param int $jurisdiction_id Jurisdiction ID to search against
	 * @return array|bool
	 */
	function getEmailAddressesByJurisdiction($jurisdiction_id = null) {
		global $props;
		$email_array = array();
		
		if (!is_null($jurisdiction_id)) {
			$j_id = intval(trim($jurisdiction_id));
			try {
				$j_soap = @new SoapClient($props['sqla_wsdl_url']);
			} catch (Exception $e) {
				return $email_array;
			} catch (SoapFault $f) {
				return $email_array;
			}
			
			$role_id = ELR_NOTIFY_ROLE;
			$qry = <<<EOQ
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <system>TRISANO</system>
    <trisano_health>
        <role_memberships>
            <role_id>$role_id</role_id>
            <jurisdiction_id>$j_id</jurisdiction_id>
        </role_memberships>
    </trisano_health>
</health_message>
EOQ;
			
			if ($j_soap) {
				try {
					$j_result = $j_soap->getUsers(array("healthMessage" => $qry));
				} catch (Exception $e) {
					return $email_array;
				} catch (SoapFault $f) {
					return $email_array;
				}
				
				unset($j_users);
				
				$j_xml = simplexml_load_string($j_result->return);
				$j_addresses = $j_xml->xpath("trisano_health/users_attributes/email_addresses");
				
				foreach ($j_addresses as $j_addresses_obj => $j_addresses_val) {
					$email_array[] = trim($j_addresses_val->email_address);
				}
				
				unset($j_xml);
				unset($j_result);
				unset($j_soap);
				
				return $email_array;
			} else {
				return $email_array;
			}
		} else {
			return $email_array;
		}
	}
	
	
	
	
	/**
	 * Get back a list of e-mail addresses for a custom notification jurisdiction.
	 * If no users found, or if an error occurs, returns FALSE.
	 *
	 * @param int $jurisdiction_id Custom Jurisdiction ID to search against
	 * @return array|bool
	 */
	function getEmailAddressesByCustomJurisdiction($jurisdiction_id = null) {
		global $host_pa, $my_db_schema;
		
		$email_array = array();
		
		if (!is_null($jurisdiction_id)) {
			$j_id = intval(trim($jurisdiction_id));
			
			$sql = 'SELECT recipients FROM '.$my_db_schema.'batch_notification_custom_jurisdictions WHERE id = '.$j_id.';';
			$rs = @pg_query($host_pa, $sql);
			if ($rs !== false) {
				$email_array = explode(';', trim(@pg_fetch_result($rs, 0, 'recipients')));
				@pg_free_result($rs);
			}
		}
		return $email_array;
	}
	
	
	
	
	/**
	 * Returns a boolean TRUE or FALSE indicating whether the custom notification jurisdiction 
	 * (specified by ID) should include a column with a link back to the original ELR message
	 * in the Excel spreadsheet
	 *
	 * @param int $jc_id Custom Jurisdiction ID
	 * @return bool
	 */
	function useLinkToLab($jc_id = null) {
		global $host_pa, $my_db_schema;
		
		if (is_null($jc_id)) {
			return false;
		}
		
		$sql = 'SELECT id FROM '.$my_db_schema.'batch_notification_custom_jurisdictions 
				WHERE id = '.intval($jc_id).'
				AND link_to_lab IS TRUE;';
		$rs = @pg_query($host_pa, $sql);
		if ($rs !== false && (@pg_num_rows($rs) > 0)) {
			@pg_free_result($rs);
			return true;
		} else {
			return false;
		}
	}
	
	
	
	
	

?>