<?php

	function getTrisanoRoles($role_id = 0, $field_name = 'role_id') {
		global $props;
		$xml = <<<EOX
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
    <system>TRISANO</system>
    <trisano_health></trisano_health>
</health_message>
EOX;
		
		try {
			$client = @new SoapClient($props['sqla_wsdl_url']);
		} catch (Exception $e) {
			suicide($e->getMessage(), -1, 1);
		} catch (SoapFault $f) {
			suicide($f->getMessage(), -1, 1);
		}
		
		if ($client) {
			try {
				$result = $client->getRoles(array("healthMessage" => $xml));
			} catch (Exception $e) {
				suicide($e->getMessage(), -1, 1);
			} catch (SoapFault $f) {
				suicide($f->getMessage(), -1, 1);
			}
			$return = simplexml_load_string($result->return);
			
			$html = "<select name=\"".$field_name."\">";
			foreach ($return->trisano_health->roles as $obj) {
				if ($role_id == $obj->id) {
					$sel = "selected=\"selected\"";
				} else {
					$sel = "";
				}
				
				$html .= "<option value=\"".$obj->id."\" ".$sel.">".$obj->roleName."</option>";
			}
			
			$html .= "</select>";
			
			return $html;
		} else {
			return "<em>Unable to retrieve TriSano Roles</em>";
		}
	}
	
	
	
	
	function getSystemMenus($role_id = 0) {
		global $host_pa, $my_db_schema;
		$selected = getSelectedMenus($role_id);
		$menus = get_db_result_set($host_pa, 'SELECT * FROM '.$my_db_schema.'system_menus');
		
		$html = '<select size="8" name="system_menu_id[]" multiple id="system_menu_id">';
		while ($rule = pg_fetch_array($menus)) {
			if (in_array($rule['id'], $selected)) {
				$sel = 'SELECTED';
			} else {
				$sel = '';
			}
			$html .= '<option value="'.$rule['id'].'" '.$sel.'>'.$rule['name'].'</option>';
		}
		$html .= '</select>';
		return $html;
	}
	
	
	
	
	function getSelectedMenus($role_id = 0) {
		global $host_pa, $my_db_schema;
		$selected = array();
		
		if (intval($role_id) > 0) {
			$sql = 'SELECT sm.* FROM '.$my_db_schema.'system_menus sm INNER JOIN '.$my_db_schema.'system_role_menus sr ON (sr.system_menu_id = sm.id) WHERE sr.system_role_id = '.intval($role_id).';';
			$results = get_db_result_set($host_pa, $sql);
			
			while ($menu = get_db_row_array($results)) {
				if (!in_array($menu['id'], $selected)) {
					$selected[] = $menu['id'];
				}
			}
		}
		
		return $selected;
	}
	
	
	
	
	function getRoleDiseases($system_role_id = 0) {
		global $host_pa, $my_db_schema;
		unset($selected);
		$selected = array();
		if (intval($system_role_id) > 0) {
			$sql = 'SELECT lab_id, master_loinc_code FROM '.$my_db_schema.'system_role_loincs_by_lab WHERE system_role_id = '.intval($system_role_id).';';
			$results = @pg_query($host_pa, $sql);
			while ($row = @pg_fetch_object($results)) {
				if (!is_array($selected[$row->lab_id]) || !in_array($row->master_loinc_code, $selected[$row->lab_id])) {
					$selected[$row->lab_id][] = $row->master_loinc_code;
				}
			}
		}
		
		return $selected;
	}
	
	
	
	
	function getRoleChildSNOMEDs($system_role_id = 0) {
		global $host_pa, $my_db_schema;
		unset($selected);
		$selected = array();
		if (intval($system_role_id) > 0) {
			$sql = 'SELECT lab_id, child_snomed FROM '.$my_db_schema.'system_role_child_snomeds_by_lab WHERE system_role_id = '.intval($system_role_id).';';
			$results = @pg_query($host_pa, $sql);
			while ($row = @pg_fetch_object($results)) {
				if (!is_array($selected[$row->lab_id]) || !in_array($row->child_snomed, $selected[$row->lab_id])) {
					$selected[$row->lab_id][] = $row->child_snomed;
				}
			}
		}
		
		return $selected;
	}
	
	
	
	
	/**
	 * Gets the TriSano label for a given Role ID.  Returns FALSE if no matching role found.
	 *
	 * @param int $role_id TriSano Role ID
	 * @return string|bool
	 */
	function getTrisanoRole($role_id = 0) {
		global $props;
		if (filter_var($role_id, FILTER_VALIDATE_INT) && (intval($role_id) > 0)) {
			try {
				$soap = @new SoapClient($props['sqla_wsdl_url']);
			} catch (Exception $e) {
				return false;
			} catch (SoapFault $f) {
				return false;
			}
			
			$qry = <<<EOQ
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<health_message>
	<system>TRISANO</system>
	<trisano_health></trisano_health>
</health_message>
EOQ;
			
			if ($soap) {
				try {
					$result = $soap->getRoles(array("healthMessage" => $qry));
				} catch (Exception $e) {
					return false;
				} catch (SoapFault $f) {
					return false;
				}
				
				$xml = simplexml_load_string($result->return);
				$this_role = $xml->xpath("//roles[id=".intval($role_id)."]/roleName");
				return trim($this_role[0]);
			} else {
				// no soap client
				return false;
			}
		} else {
			// not a valid integer
			return false;
		}
	}
	
	
	
	
	

?>