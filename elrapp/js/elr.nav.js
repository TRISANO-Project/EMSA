$(function() {
	$(".activetab").addClass("current");
	
	$("nav ul li a:not(nav ul li ul li a)").addClass("ui-corner-top");
	
	$("#override_role").change(function() {
		$("#change_elr_view").submit();
	});
	
	$(window).responsinav({ breakpoint: 200 });
});