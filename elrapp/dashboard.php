<?php

	if ($_POST['from_date'] && $_POST['to_date']) {
		// update sessionized date params if updated by user
		$from_unix = strtotime(trim($_POST['from_date']));
		$to_unix = strtotime(trim($_POST['to_date']));
		if (($from_unix !== false) && ($to_unix !== false)) {
			$_SESSION['dashboard_date_from'] = date("m/d/Y", $from_unix);
			$_SESSION['dashboard_date_to'] = date("m/d/Y", $to_unix);
		}
	}
	
	if ($_POST['lab_filter']) {
		if (filter_var($_POST['lab_filter'], FILTER_VALIDATE_INT)) {
			$_SESSION['dashboard_lab_filter'] = filter_var($_POST['lab_filter'], FILTER_SANITIZE_NUMBER_INT);
		}
	}
	
	if (isset($_POST['dashboard_action']) && (trim($_POST['dashboard_action']) == 'reset')) {
		// reset view to default
		unset($_SESSION['dashboard_date_from'], $_SESSION['dashboard_date_to'], $_SESSION['dashboard_lab_filter']);
	}
	
	if (!isset($_SESSION['dashboard_date_from']) || !isset($_SESSION['dashboard_date_to'])) {
		// if no sessionized date params, default to today
		$_SESSION['dashboard_date_from'] = date("m/d/Y", strtotime("-1 week", time()));
		$_SESSION['dashboard_date_to'] = date("m/d/Y");
	}
	
	if (!isset($_SESSION['dashboard_lab_filter'])) {
		$_SESSION['dashboard_lab_filter'] = -1;
	}
	
	session_write_close(); // done writing to session; prevent blocking
	
	$dashboard_orphan_count = getOrphanedMessageCount();

?>

<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type='text/javascript'>
	google.load('visualization', '1', {'packages': ['corechart', 'table']});
	google.setOnLoadCallback(drawSummaryChart);
	google.setOnLoadCallback(drawAvgNewCmrChart);
	google.setOnLoadCallback(drawNewCaseChart);
	google.setOnLoadCallback(drawAppendCaseChart);
	google.setOnLoadCallback(drawDiscardCaseChart);
	google.setOnLoadCallback(drawBlacklistChart);
	google.setOnLoadCallback(drawGraylistChart);
	google.setOnLoadCallback(drawMQFChart);
	google.setOnLoadCallback(drawConditionChart);
	google.setOnLoadCallback(drawAutomationFactorChart);
	google.setOnLoadCallback(drawLabDataChart);
	
	var summaryData,
		summaryOptions,
		summaryChart,
		avgNewCmrData,
		avgNewCmrOptions,
		avgNewChart,
		newCaseData,
		newCaseOptions,
		newCaseChart,
		appendCaseData,
		appendCaseOptions,
		appendCaseChart,
		discardCaseData,
		discardCaseOptions,
		discardCaseChart,
		blacklistData,
		blacklistSummaryOptions,
		blacklistSummaryChart,
		graylistData,
		graylistSummaryOptions,
		graylistSummaryChart,
		mqfData,
		mqfOptions,
		mqfChart,
		conditionData,
		conditionOptions,
		conditionChart,
		autoFactorData,
		autoFactorOptions,
		autoFactorChart,
		labData,
		labOptions,
		labChart;
		
	function drawSummaryChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardSummary' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			summaryData = new google.visualization.DataTable();
			summaryData.addColumn('string', 'Message Queue');
			summaryData.addColumn('number', '# of Messages');
			$.each(jsonData, function(series, val) {
				summaryData.addRow([series+' ['+((typeof(val) != 'object') ? val : '0')+']', parseInt(val)]);
			});
			
			$('#summary_chart_status').hide();
			
			summaryOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "20%", top: 10, height: "90%", width: "75%" },
				colors: ['darkorange'],
				legend: { position: 'none' }
			};
			
			summaryChart = new google.visualization.BarChart(document.getElementById('summary_chart_ajax'));
			summaryChart.draw(summaryData, summaryOptions);
		});
	}
	
	function drawAvgNewCmrChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getAvgCMRCreateTime' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			avgNewCmrData = new google.visualization.DataTable();
			avgNewCmrData.addColumn('string', 'Disease Category');
			avgNewCmrData.addColumn('string', 'Average Time Elapsed');
			$.each(jsonData, function(series, val) {
				avgNewCmrData.addRow([series, val]);
			});
			
			$('#avgnew_chart_status').hide();
			
			avgNewCmrOptions = {
				fontName: 'Open Sans',
				fontSize: 12,
				showRowNumber: false,
				sort: 'disable'
			};
			
			avgNewChart = new google.visualization.Table(document.getElementById('avgnew_chart_ajax'));
			avgNewChart.draw(avgNewCmrData, avgNewCmrOptions);
		});
	}
	
	function drawNewCaseChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardNewCase' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			newCaseData = new google.visualization.DataTable();
			newCaseData.addColumn('string', 'Disease Category');
			newCaseData.addColumn('number', '# of Labs Generated New CMR');
			$.each(jsonData, function(series, val) {
				newCaseData.addRow([series+' ['+((typeof(val) != 'object') ? val : '0')+']', parseInt(val)]);
			});
			
			$('#newcase_chart_status').hide();
			
			newCaseOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "25%", top: 10, height: "90%", width: "70%" },
				colors: ['forestgreen'],
				legend: { position: 'none' }
			};
			
			newCaseChart = new google.visualization.BarChart(document.getElementById('newcase_chart_ajax'));
			newCaseChart.draw(newCaseData, newCaseOptions);
		});
	}
	
	function drawAppendCaseChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardAppendedCase' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			appendCaseData = new google.visualization.DataTable();
			appendCaseData.addColumn('string', 'Disease Category');
			appendCaseData.addColumn('number', '# of Labs Updated Existing Event');
			$.each(jsonData, function(series, val) {
				appendCaseData.addRow([series+' ['+((typeof(val['labs_updated']) != 'undefined') ? val['labs_updated'] : 0)+']', parseInt(val['labs_updated'])]);
			});
			
			$('#appendcase_chart_status').hide();
			
			appendCaseOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "25%", top: 10, height: "90%", width: "70%" },
				colors: ['royalblue', 'teal'],
				legend: { position: 'none' },
				isStacked: true
			};
			
			appendCaseChart = new google.visualization.BarChart(document.getElementById('appendcase_chart_ajax'));
			appendCaseChart.draw(appendCaseData, appendCaseOptions);
		});
	}
	
	function drawDiscardCaseChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardDiscardedCase' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			discardCaseData = new google.visualization.DataTable();
			discardCaseData.addColumn('string', 'Disease Category');
			discardCaseData.addColumn('number', 'Cases Discarded');
			$.each(jsonData, function(series, val) {
				discardCaseData.addRow([series, parseInt(val)]);
			});
			
			$('#discardcase_chart_status').hide();
			
			discardCaseOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "20%", top: 10, height: "90%", width: "75%" },
				colors: ['firebrick'],
				legend: { position: 'none' }
			};
			
			discardCaseChart = new google.visualization.BarChart(document.getElementById('discardcase_chart_ajax'));
			discardCaseChart.draw(discardCaseData, discardCaseOptions);
		});
	}
	
	function drawBlacklistChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardBlacklistSummary' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			blacklistData = new google.visualization.DataTable();
			blacklistData.addColumn('string', 'Lab');
			blacklistData.addColumn('number', 'Messages Blacklisted');
			$.each(jsonData, function(series, val) {
				blacklistData.addRow([series, parseInt(val)]);
			});
			
			$('#blacklist_summary_chart_status').hide();
			
			blacklistSummaryOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "20%", top: 10, height: "90%", width: "75%" },
				colors: ['#333333'],
				legend: { position: 'none' },
				bar: { groupWidth: 20 }
			};
			
			blacklistSummaryChart = new google.visualization.BarChart(document.getElementById('blacklist_summary_chart_ajax'));
			blacklistSummaryChart.draw(blacklistData, blacklistSummaryOptions);
		});
	}
	
	function drawGraylistChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardGraylistSummary' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			graylistData = new google.visualization.DataTable();
			graylistData.addColumn('string', 'Disease Category');
			graylistData.addColumn('number', 'Cases Graylisted');
			$.each(jsonData, function(series, val) {
				graylistData.addRow([series+' ['+((typeof(val) != 'object') ? val : '0')+']', parseInt(val)]);
			});
			
			$('#graylist_summary_chart_status').hide();
			
			graylistSummaryOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "25%", top: 10, height: "90%", width: "70%" },
				colors: ['dimgray'],
				legend: { position: 'none' }
			};
			
			graylistSummaryChart = new google.visualization.BarChart(document.getElementById('graylist_summary_chart_ajax'));
			graylistSummaryChart.draw(graylistData, graylistSummaryOptions);
		});
	}
	
	function drawMQFChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardMessageQuality' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			mqfData = new google.visualization.DataTable();
			mqfData.addColumn('string', 'Quality Issue');
			mqfData.addColumn('number', 'Messages with Issue');
			$.each(jsonData, function(series, val) {
				mqfData.addRow([series, parseInt(val)]);
			});
			
			$('#mqf_chart_status').hide();
			
			mqfOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "30%", top: 10, height: "90%", width: "65%" },
				colors: ['salmon'],
				legend: { position: 'none' },
				bar: { groupWidth: 20 }
			};
			
			mqfChart = new google.visualization.BarChart(document.getElementById('mqf_chart_ajax'));
			mqfChart.draw(mqfData, mqfOptions);
		});
	}
	
	function drawConditionChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardConditionSummary' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			conditionData = new google.visualization.DataTable();
			conditionData.addColumn('string', 'Condition');
			conditionData.addColumn('number', 'Messages Received');
			conditionData.addColumn('number', 'Assigned Labs');
			$.each(jsonData, function(series, val) {
				var conditionSeriesString = "['"+series+"'";
				$.each(val, function(col_key, col_val) {
					conditionSeriesString += ', '+parseInt(col_val);
				});
				conditionSeriesString += "]";
				conditionData.addRow(eval(conditionSeriesString));
			});
			
			$('#condition_chart_status').hide();
			
			var numRows = conditionData.getNumberOfRows();
			var realHeight = (numRows * 35);
			
			conditionOptions = {
				height: realHeight,
				bar: { groupWidth: 20 },
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "25%", top: 25, height: "95%", width: "70%" },
				vAxis: { textPosition: 'out' },
				legend: { position: 'top' }
			};
			
			conditionChart = new google.visualization.BarChart(document.getElementById('condition_chart_ajax'));
			conditionChart.draw(conditionData, conditionOptions);
		});
	}
	
	function drawAutomationFactorChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardAutomationFactor' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			autoFactorData = new google.visualization.DataTable();
			autoFactorData.addColumn('string', 'Auto or Manual?');
			autoFactorData.addColumn('number', 'Number of Messages');
			$.each(jsonData, function(series, val) {
				autoFactorData.addRow([series, parseInt(val)]);
			});
			
			$('#autofactor_chart_status').hide();
			
			autoFactorOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12
			};
			
			autoFactorChart = new google.visualization.PieChart(document.getElementById('autofactor_chart_ajax'));
			autoFactorChart.draw(autoFactorData, autoFactorOptions);
		});
	}
	
	function drawLabDataChart() {
		$.ajax({
			type: 'POST',
			data: { callback: 'getDashboardLab' },
			url: '/elrapp/includes/dashboard_ajax.php',
			dataType: 'json',
			async: true
		}).done(function(data) {
			jsonData = data;
			
			labData = new google.visualization.DataTable();
			$.each(jsonData, function(series, val) {
				if (series == "headers") {
					$.each(val, function(header_key, header_val) {
						if (header_val == "Day") {
							labData.addColumn('string', 'Day');
						} else if (header_val == "Certainty") {
							labData.addColumn({type:'boolean',role:'certainty'});
						} else {
							labData.addColumn('number', header_val);
						}
					});
				} else {
					var series_string = "['"+series+"'";
					$.each(val, function(lab_key, lab_val) {
						if (lab_key == "Certainty") {
							series_string += ', '+lab_val;
						} else {
							series_string += ', '+parseInt(lab_val);
						}
					});
					series_string += "]";
					labData.addRow(eval(series_string));
				}
			});
			
			$('#lab_chart_status').hide();
			
			labOptions = {
				height: 285,
				fontName: 'Open Sans',
				fontSize: 12,
				chartArea: { left: "10%", top: 10, height: "90%" },
				hAxis: { slantedText: false, textStyle: { fontSize: 9 } },
				pointSize: 4,
				legend: { position: 'right' },
				series: {0:{lineWidth: 1, pointSize: 2}}
			};
			
			labChart = new google.visualization.LineChart(document.getElementById('lab_chart_ajax'));
			labChart.draw(labData, labOptions);
		});
	}
</script>
<script type="text/javascript">
	$(function() {
		var xhrPool = [];
		var abortXHR = function() {
			$.each(xhrPool, function(idx, jqXHR) {
				jqXHR.abort();
			});
		};
		
		var oldbeforeunload = window.onbeforeunload;
		window.onbeforeunload = function() {
			var r = oldbeforeunload ? oldbeforeunload() : undefined;
			if (r == undefined) {
				abortXHR();
			}
			return r
		}
		
		$(document).ajaxSend(function(e, jqXHR, options) {
			xhrPool.push(jqXHR);
		});
		
		$(document).ajaxComplete(function(e, jqXHR, options) {
			xhrPool = $.grep(xhrPool, function(x) { return x != jqXHR });
		});
		
		$(".date_range").datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
		
		$("#apply_date").button({
			icons: { primary: "ui-icon-elrretry" }
		});
		
		$("#reset_date").button({
			icons: { primary: "ui-icon-elrcancel" }
		}).click(function() {
			$("#dashboard_action").val("reset");
			$("#date_filter").submit();
		});
		
		$("#view_tabular").button({
			icons: { primary: "ui-icon-elrviewdocument" }
		}).click(function() {
			window.location.href = "<?php echo $main_url; ?>?selected_page=6&submenu=7&report_id=dashboard_tabular";
		});
		
		$("#view_orphaned").button({
			icons: { primary: "ui-icon-elrviewdocument" }
		}).click(function() {
			window.location.href = "<?php echo $main_url; ?>?selected_page=6&submenu=7&report_id=orphaned_messages";
		});
		
		$("#log_out").button().click(function() {
			window.location.href = "<?php echo $main_url; ?>?log_out=true";
		});
		
		$("#lab_filter").val('<?php echo intval($_SESSION['dashboard_lab_filter']); ?>');
	});
</script>	

<h1 class="elrhdg"><span class="ui-icon ui-icon-header ui-icon-elrdashboard"></span>Dashboard</h1>

<form id="date_filter" method="POST">
	<label>Reporting for:</label> 
		<input class="ui-corner-all date_range" type="text" name="from_date" size="10" id="from_date" value="<?php echo $_SESSION['dashboard_date_from']; ?>"> 
		<label>to</label> 
		<input class="ui-corner-all date_range" type="text" name="to_date" size="10" id="to_date" value="<?php echo $_SESSION['dashboard_date_to']; ?>">
	<div class="emsa_toolbar_separator"></div>
	<label>Show:</label> <select title="Filter results by lab" class="ui-corner-all lab_selector" id="lab_filter" name="lab_filter">
		<option value="-1">All Labs</option>
		<?php
			$lab_qry = "SELECT id, ui_name FROM ".$my_db_schema."structure_labs ORDER BY ui_name;";
			$lab_rs = @pg_query($host_pa, $lab_qry);
			if ($lab_rs) {
				while ($lab_row = @pg_fetch_object($lab_rs)) {
					echo "<option value=\"".intval($lab_row->id)."\">".$lab_row->ui_name."</option>".PHP_EOL;
				}
			}
			@pg_free_result($lab_rs);	
		?>
	</select>
	<input type="hidden" name="dashboard_action" id="dashboard_action" value="">
	<div class="emsa_toolbar_separator"></div>
	<button id="apply_date" type="submit" title="Update/refresh using these settings">Update</button>
	<button id="reset_date" type="button" title="Reset to default settings">Defaults</button>
	<button id="view_tabular" type="button" title="View report data in textual format">View Tabular</button>
	<button id="log_out" type="button" title="Log out">Log out</button>
</form>

<?php

	if ($dashboard_orphan_count > 0) {
		$warn_string = '<strong>Orphaned Message Warning:</strong> There are '.trim($dashboard_orphan_count).' inaccessible messages in EMSA.';
		if (checkPermission(URIGHTS_ADMIN)) {
			$warn_string .= '<br><button id="view_orphaned" type="button">View Orphaned Messages</button>';
		} else {
			$warn_string .= '  Please see a system administrator.';
		}
		highlight($warn_string, 'ui-icon-elrerror');
	}

?>
	

<div class="report_widget ui-corner-all">
	<strong class="big_strong">ELR Overview</strong>
	<div id="summary_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="summary_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Messages Received per Lab</strong>
	<div id="lab_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="lab_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all" style="width: 1032px; height: auto;">
	<strong class="big_strong">Disease Summary</strong>
	<div id="condition_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="condition_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Lab Generated New CMR</strong>
	<div id="newcase_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="newcase_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Lab Updated Existing Event</strong>
	<div id="appendcase_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="appendcase_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Duplicate Labs Discarded</strong>
	<div id="discardcase_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="discardcase_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Labs Blacklisted</strong>
	<div id="blacklist_summary_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="blacklist_summary_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Labs Graylisted</strong>
	<div id="graylist_summary_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="graylist_summary_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Avg. Time from Specimen Collection to LHD Routing</strong>
	<div id="avgnew_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="avgnew_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Messages with QA Errors</strong>
	<div id="mqf_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="mqf_chart_ajax"></div>
</div>

<div class="report_widget ui-corner-all">
	<strong class="big_strong">Percent Automated Messages vs. Manual</strong>
	<div id="autofactor_chart_status" style="display: inline-block;"><img style="vertical-align: bottom;" src="/elrapp/img/ajax-loader.gif" height="16" width="16" border="0" /> Updating...</div>
	<div id="autofactor_chart_ajax"></div>
</div>