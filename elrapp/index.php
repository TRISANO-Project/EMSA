<?php

	require_once __DIR__.'/vendor/autoload.php';

	ob_start();

	// prevent caching...
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");

    unset($execute_logout);
    if (isset($_REQUEST['log_out'])) {
       $execute_logout = trim($_REQUEST['log_out']);
    }

	include_once './includes/app_config.php';

	$selected_page = 1;
	$submenu = 0;
	$vocab = 0;
	$cat = 0;
    
	if (isset($_REQUEST['selected_page'])) {
		$selected_page = intval($_REQUEST['selected_page']);
	}
	if (isset($_REQUEST['submenu'])) {
		$submenu = intval($_REQUEST['submenu']);
	}
	if (isset($_REQUEST['vocab'])) {
		$vocab = intval($_REQUEST['vocab']);
	}
	if (isset($_REQUEST['cat'])) {
		$cat = intval($_REQUEST['cat']);
	}

	if (isset($_POST['import_flag'])) {
		if (trim($_POST['import_flag']) == '1') {
			$process_import = TRUE;
		}
	}

	if (isset($_POST['override_role']) && !empty($_POST['override_role'])) {
		include './includes/override_role.php';  // if user manually picks from available roles, set the override role in _SESSION
	}

	include './header.php';

	switch ($selected_page) {
		case 1:
			include_once 'includes/dashboard_functions.php';
			include 'dashboard.php';
			break;
		case 2:
		case 3:
		case 4:
		case 5:
			include './emsa/index.php';
			break;
		case 6:
			include 'admin.php';
			break;
		case 8:
			include 'nonelr/index.php';
			break;
	}

	include './footer.php';

	ob_end_flush();

?>
