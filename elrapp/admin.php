<?php
	if ($submenu < 2 || $submenu == 8) {
		include './emsa/index.php';  // exceptions queue
	} elseif ($submenu == 2) {
		include './etasks/index.php';  // exception dashboard
	} elseif ($submenu == 3) {
		if ($vocab == 6 && $process_import) {
			include './manage/import/import_handler.php';
		} elseif ($vocab == 6) {
			include './manage/import/import.php';
		} elseif ($vocab == 7) {
			include './manage/import/export.php';
		} elseif ($vocab == 8 || $vocab == 9) {
			include './manage/vocab_valueset.php';  // value set management
		} else {
			include './manage/vocabulary.php';  // LOINC/Condition/Organism management
		}
	} elseif ($submenu == 4) {
		if ($cat == 2) {
			include './manage/structure_xml_master.php';
		} elseif ($cat == 3) {
			include './manage/structure_rules_xml.php';
		} elseif ($cat == 4) {
			include './manage/structure_xml_application.php';
		} elseif ($cat == 5) {
			include './manage/structure_hl7.php';
		} elseif ($cat == 6) {
			include './manage/structure_vocab_category.php';
		} elseif ($cat == 7 && $process_import) {
			include './manage/import/structure_import_handler.php';
		} elseif ($cat == 7) {
			include './manage/import/structure_import.php';
		} elseif ($cat == 8) {
			include './manage/import/structure_export.php';
		} else {
			include './manage/lab_manager.php';
		}
	} elseif ($submenu == 5) {
		if ($cat == 2) {
			include './manage/add_hl7_message.php';  // manually push an HL7 message into the ELR process
		} elseif ($cat == 4) {
			include './manage/zipcodes.php';  // manage jurisdiction zip codes
		} elseif ($cat == 5) {
			include './manage/audit_log.php';  // EMSA audit log
		} elseif ($cat == 6) {
			include './manage/pretty_xml.php';  // xml make-pretty-izer
		} elseif ($cat == 7) {
			include './manage/hl7_review_generator.php';  // generate HL7 message review spreadsheet for onboarding
		} elseif (($cat == 8) && ($server_environment == ELR_ENV_DEV)) {
			include './manage/random_hl7.php';  // randomly generate HL7 message (Demo site only)
		} elseif ($cat == 9) {
			include './manage/vocabulary_audit_log.php';  // vocabulary manager audit log
		} else {
			include './roles/index.php';  // manage user roles
		}
	} elseif ($submenu == 6) {
		include './emsa/view_message.php';  // single-message viewer
	} elseif ($submenu == 7) {
		include './reports/index.php';  // reporting ui
	} elseif ($submenu == 9) {
		if ($cat == 2) {
			include './manage/elrnotify_virtual_lhd.php';  // configure 'virtual' jurisdictions
		} elseif ($cat == 3) {
			include './manage/elrnotify_types.php';  // configure notification types
		} elseif ($cat == 7) {
			include './manage/elrnotify_rule_params.php';  // configure notification rule parameters
		} elseif ($cat == 4) {
			include './manage/elrnotify_rules.php';  // configure notification rules
		} elseif ($cat == 5) {
			include './manage/elrnotify_pending.php';  // view pending (unsent) notifications
		} elseif ($cat == 6) {
			include './manage/elrnotify_log.php';  // view recently-sent notifications
		} else {
			include './manage/elrnotify_config.php';  // configure ELR notification settings
		}
	} elseif ($submenu == 10) {
		if ($cat == 2) {
			include './manage/autoepi_rules.php';  // configure AutoEpi rules
		} else {
			include './manage/autoepi_rule_params.php';  // configure AutoEpi rule parameters
		}
	} else {
	
?>
<h1>Admin Menu</h1>
<?php
	}
?>
