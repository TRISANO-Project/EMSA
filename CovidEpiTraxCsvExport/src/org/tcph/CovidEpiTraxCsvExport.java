/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tcph;

import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Logger;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Jay Boyer
 */
public class CovidEpiTraxCsvExport {

    private static boolean fExportSingleEvent = false;
    private static Integer nRecordNumber = 0, nRecordNumberMax = 0;
    private static Properties properties = new Properties();
    private static Logger logger = Logger.getLogger("");
    private static Connection con;
    private static String sRecordingDetail = "COVID_REPORT_TO_STATE";
    private static String sAuditClass = "gov.utah.health.model.PersonCondition";
    private static int idEmsaUser = -1;

    // indices to CSV columns
    private static int iPersonConditionId =0;
    private static int iRecordNumber =1;
    private static int iDob =2;
    private static int iLName =3;
    private static int iFName =4;
    private static int iRecieved =5;
    private static int iSpecimen =6;
    private static int iReportingAgency =7;
    private static int iTestType =8;
    private static int iPCRResult =9;
    private static int iPCRCollectionDate =10;
    private static int iSerologyCollectionDate =11;
    private static int iIggResult =12;
    private static int iIgmResult =13;
    private static int iAntigenCollectionDate =14;
    private static int iAntigenResult =15;
    private static int iOtherCollectionDate =16;
    private static int iOtherResult =17;
    private static int iPhone =18;
    private static int iAddress1 =19;
    private static int iAddress2 =20;
    private static int iCity =21;
    private static int iState =22;
    private static int iZip =23;
    private static int iGender =24;
    private static int iRace =25;
    private static int iEthnicity =26;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] asArg) {

        initLogger();
        int nError = 1;

        // check arguments to see if we are exporting a single events
        if (asArg.length == 1) {
            // parse out the record number
            try {
                nRecordNumber = Integer.parseInt(asArg[0]);
            } catch (Exception ex) {
                nError = 2;
            }
            fExportSingleEvent = true;
        } else if (asArg.length > 1) {
            logger.log(Level.SEVERE, "Exiting " + nError + " problem with argument list");
            System.err.println("Usage: CovidEpiTraxCsvExport <record_number>\n");
            System.exit(nError);
        }
        
        // read Sql url and credentials from the property file
        loadProperties();

        // Ensure we can connect to the database
        con = getConnection();

        // get the EMSA user id
        setEmsaUserId();

// fetch the events of interest
        ArrayList<ArrayList<String>> aCsv = fetchEvents(con);

        // output the xml for the events
        outputCsv(aCsv);
    }

    /**
     *
     */
    private static void initLogger() {
        FileHandler handler;
        try {
            logger.setUseParentHandlers(false);
            handler = new FileHandler("CovidEpiTraxCsvExport.log");
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(new SimpleFormatter());
            handler.setLevel(Level.ALL);
            logger.addHandler(handler);
            logger.log(Level.INFO, "CovidEpiTraxCsvExport beginning logging");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error 3 setting up logger", ex);
            System.err.println("Error setting up logger");
            System.exit(3);
        }
    }


    /**
     *
     */
    private static void loadProperties() {
        File file = new File("covid_epitrax_csv_export.properties");
 
        try {
            FileInputStream in = new FileInputStream(file.getAbsolutePath());
            properties.load(in);
            in.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 2 could not read properties file" + ex.toString());
            System.err.println("Could not read properties file: " + file.getAbsolutePath());
            System.err.println("Properties Exception: " + ex.toString());
            System.exit(4);
        }
    }

    /**
     * @author Jay Boyer 2013
     */
    private static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(
                    properties.getProperty("db_url"),
                    properties.getProperty("db_user"),
                    properties.getProperty("db_password"));
            if (con == null) {
                logger.log(Level.SEVERE, "Exiting 3 database connection returned null");
                System.err.println("Problem connecting to database.");
                System.exit(5);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 4 could not connect to database " + ex.toString());
            System.err.println("Could not connect to database. Exception: " + ex.toString());
            closeConnection();
            System.exit(6);
        }
        return con;
    }

    private static ArrayList<ArrayList<String>> fetchEvents(Connection con) {

        ResultSet rs = null;
        ArrayList<String> asHeader = new ArrayList<String>( 
            Arrays.asList("person_condition_id", "\"EpiTrax.ID\"","\"Date.of.Birth\"","\"Last.Name\"","\"First.Name\""
            ,"\"Date.Received.by.TCPH\"","\"Specimen.ID\"","\"Collection.Site\"","\"Test.Type\"","\"PCR.Test.Result\""
            ,"\"PCR.Specimen.Collection.Date\"","\"Serology.Collection.Date\"","\"IgGResult\"","\"IgMResult\""
            ,"\"Antigen.Collection.Date\"","\"AntigenResult\"","\"Other.Collection.Date\"","\"OtherResult\""
            ,"\"Phone\"","\"Address.of.Residence_Street1\""
            ,"\"Address.of.Residence_Street2\"","\"Address.of.Residence_City\"","\"Address.of.Residence_State\""
            ,"\"Address.of.Residence_Zip\"","\"Gender\"","\"Race\"","\"Ethnicity\""
            )); 
        ArrayList<ArrayList<String>> aCsv = new ArrayList<ArrayList<String>>();
        aCsv.add(asHeader);

        // SQL for finding Covid events not yet reported
        // OR a single specific Covid event (reported or not)
        String sSql = 
            "SELECT DISTINCT ON  (pc.id) pc.id, pc.record_number, TO_CHAR(pe.birth_date, 'YYYY-MM-DD') AS dob, pe.last_name, pe.first_name "
               + ", TO_CHAR(pc.created_at, 'YYYY-MM-DD') AS date_received "
               + ", f.name "
               + ", '(' || pt.area_code || ') ' || pt.phone_number AS telephone "
               + ", pa.street AS address_1  "
               + ", pa.city, pa.state, pa.postal_code "
               + ", g.description "
               + ", r.description "
               + ", e.description "
               + "FROM public.person_condition pc "
               + "INNER JOIN person pe ON pe.id = pc.person_id  "
               + "INNER JOIN person_condition_type pct ON pc.person_condition_type_id = pct.id AND pct.description = 'Morbidity' "
               + "INNER JOIN condition c ON c.id = pc.condition_id AND c.name='2019-nCoV' "
               + "LEFT JOIN (SELECT DISTINCT ON (person_id) * FROM public.person_telephone ORDER BY person_id, id DESC) pt ON pt.person_id = pc.person_id " 
               + "LEFT JOIN facility f ON f.id = pc.reporting_facility_id "
               + "LEFT JOIN (SELECT DISTINCT ON (person_id) * FROM public.person_address ORDER BY person_id, id DESC) pa ON pa.person_id = pc.person_id "
               + "LEFT JOIN gender g ON g.code=pe.birth_gender "
               + "LEFT JOIN person_race pr ON pr.person_id = pe.id "
               + "LEFT JOIN race r ON r.code = pr.race "
               + "LEFT JOIN ethnicity e ON e.code = pe.ethnicity "
            + "WHERE pc.deleted_at IS NULL  ";
        if (fExportSingleEvent) {
            sSql += "AND pc.record_number='" + nRecordNumber + "' ";
        } else {
            sSql += "AND pc.id NOT IN "
                + "(SELECT SUBSTRING(globalid, 39)::INTEGER as person_condition_id "
                + "  FROM public.audit  "
                + "    WHERE changes='" + sRecordingDetail +"' "
                + "      AND globalid LIKE '" + sAuditClass + ":%' "
                + "    ORDER BY person_condition_id)";
        }
        sSql += "ORDER by pc.id; ";
        rs = executeSql(sSql, false /*fUpdate*/);
        int i = 0;
        try {
            while (rs.next()) {
                ArrayList<String> csv = new ArrayList<String>( 
                    Arrays.asList("","","","","","","","","","","","","","","","","","","","","","","","","","", ""));
                csv.set(iPersonConditionId, strNonNull(rs.getString(1)));
                csv.set(iRecordNumber, strNonNull(rs.getString(2)));
                csv.set(iDob, "\"" + strNonNull(rs.getString(3)) + "\"");
                csv.set(iLName, "\"" + strNonNull(rs.getString(4)) + "\"");
                csv.set(iFName, "\"" + strNonNull(rs.getString(5)) + "\"");
                csv.set(iRecieved, "\"" + strNonNull(rs.getString(6)) + "\"");
                csv.set(iReportingAgency, "\"" + strNonNull(rs.getString(7)) + "\"");
                csv.set(iPhone, "\"" + strNonNull(rs.getString(8)) + "\"");
                csv.set(iAddress1, "\"" + strNonNull(rs.getString(9)) + "\"");
                csv.set(iCity, "\"" + strNonNull(rs.getString(10)) + "\"");
                csv.set(iState, "\"" + strNonNull(rs.getString(11)) + "\"");
                csv.set(iZip, "\"" + strNonNull(rs.getString(12)) + "\"");
                csv.set(iGender, "\"" + strNonNull(rs.getString(13)) + "\"");
                csv.set(iRace, "\"" + strNonNull(rs.getString(14)) + "\"");
                csv.set(iEthnicity, "\"" + strNonNull(rs.getString(15)) + "\"");
                
                if (csv.get(iGender) == "\"\"") {
                    csv.set(iGender, "\"Unknown\"");
                }
                if (csv.get(iRace) == "\"\"") {
                    csv.set(iRace, "\"Unknown / Refused to Answer\"");
                }
                if (csv.get(iEthnicity) == "\"\"") {
                    csv.set(iEthnicity, "\"Unknown / Refused to Answer\"");
                }
                
                csv = addLabResultsToRecord(csv);
                aCsv.add(csv);
                if(i == 0) {
                    System.out.print("Building list: "); 
                } else if( i % 20 == 0) {
                    System.out.print(" i:" + i + " " + csv.get(iRecordNumber)); 
                } 
                if( i % 200 == 0) {
                    System.out.println(" ");
                }
                i++;
                // TODO Jay
                //if(i >= 100)
                //    break;
            }
            rs.close();
        } catch (Exception ex) {
            System.err.println("Error getting next result: " + i);
            System.err.println("rs Exception: " + ex.toString());
            closeConnection();
            logger.log(Level.SEVERE, "Exiting 7 " + ex.toString());
            System.exit(7);
        }
        return aCsv;
    }
    
    private static ArrayList<String> addLabResultsToRecord(ArrayList<String> csv) {
        
        try {
            String sSql = "SELECT DISTINCT ON (l.id) l.id, TO_CHAR(collection_date, 'YYYY-MM-DD') as col_date, ss.description AS specimen,  "
                + "ltt.common_name AS test_type, ltr.test_result FROM public.person_condition_lab pcl  "
                    + "INNER JOIN lab l ON pcl.lab_id = l.id "
                    + "LEFT JOIN lab_test lt ON lt.lab_id = l.id "
                    + "LEFT JOIN lab_test_result ltr ON ltr.lab_test_id = lt.id "
                    + "LEFT JOIN lab_test_type ltt ON ltt.id = lt.test_type_id "
                    + "LEFT JOIN specimen_source ss ON ss.id = l.specimen_source_id  "
                    + "WHERE pcl.person_condition_id = " + csv.get(iPersonConditionId) + " "
                    + "ORDER BY l.id; ";
            ResultSet rs = rs = executeSql(sSql, false /*fUpdate*/);
            while (rs.next()) {
                String sCollectionDate = strNonNull(rs.getString(2));
                String sSpecimen = strNonNull(rs.getString(3));
                String sTestType = strNonNull(rs.getString(4));
                String sResult = strNonNull(rs.getString(5));
                csv.set(iSpecimen, "\"" + sSpecimen + "\"");
                if (sTestType.toLowerCase().contains("pcr") 
                        || sTestType.toLowerCase().contains("rna")
                        || sTestType.toLowerCase().contains("naa")
                        || sTestType.toLowerCase().contains("nucleic acid")
                        || sTestType.toLowerCase(). contains("gene") 
                        || sTestType.toLowerCase(). contains("geno") 
                        || sTestType.toLowerCase(). contains("dna")) 
                {
                    // Don't overwrite earlier positive lab with negative result
                    if(!isPositiveResult(csv, iPCRResult)) {
                        csv.set(iPCRResult, "\"" + sResult + "\"");
                        csv.set(iPCRCollectionDate, "\"" + sCollectionDate + "\"");
                    }
                    csv.set(iTestType, "\"PCR\"");
                } else if (sTestType.toLowerCase().contains("igg ")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!isPositiveResult(csv, iIggResult)) {
                        csv.set(iIggResult, "\"" + sResult + "\"");
                        csv.set(iSerologyCollectionDate, "\"" + sCollectionDate + "\"");
                    }
                    csv.set(iTestType, "\"IgG\"");
                } else if (sTestType.toLowerCase().contains("igm ")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!isPositiveResult(csv, iIgmResult)) {
                        csv.set(iIgmResult, "\"" + sResult + "\"");
                        csv.set(iSerologyCollectionDate, "\"" + sCollectionDate + "\"");
                    }
                    csv.set(iTestType, "\"IgM\"");
                } else if (sTestType.toLowerCase().contains("antibody")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!isPositiveResult(csv, iIgmResult)) {
                        csv.set(iIgmResult, "\"" + sResult + "\"");
                        csv.set(iSerologyCollectionDate, "\"" + sCollectionDate + "\"");
                    }
                    csv.set(iTestType, "\"Antibody\"");
                } else if (sTestType.toLowerCase().contains("antigen")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!isPositiveResult(csv, iAntigenResult)) {
                        csv.set(iAntigenResult, "\"" + sResult + "\"");
                        csv.set(iAntigenCollectionDate, "\"" + sCollectionDate + "\"");
                    }
                    csv.set(iTestType, "\"Antigen\"");
                } else {
                    // Don't overwrite earlier positive lab with negative result
                    if(!isPositiveResult(csv, iOtherResult)) {
                        csv.set(iOtherResult, "\"" + sResult + "\"");
                        csv.set(iOtherCollectionDate, "\"" + sCollectionDate + "\"");
                    }
                    if (sTestType.toLowerCase().contains("rapid")) {
                        csv.set(iTestType, "\"Other - Rapid (Nasopharyngeal)\"");
                    } else if (sTestType.toLowerCase().contains("total antibody")) {
                        csv.set(iTestType, "\"Other - Total Antibody\"");
                    } else if (sTestType.toLowerCase().contains("iga ")) {
                        csv.set(iTestType, "\"Other - IgA\"");
                    } else {
                        csv.set(iTestType, "\"Other - Unknown\"");
                    }
                }
            }
            rs.close();
        } catch (Exception ex) {
            System.err.println("Error getting labs for event: " + csv.get(iPersonConditionId));
            System.err.println("rs Exception: " + ex.toString());
            closeConnection();
            logger.log(Level.SEVERE, "Exiting 10 " + ex.toString());
            System.exit(10);
        }
        return csv;
    }

    private static void outputCsv(ArrayList<ArrayList<String>> aCsv) {
        int i = 0;
        ArrayList<String> csv;
        String sDate = new SimpleDateFormat("yyyy-MM-dd_HHmmss").format(new Date());
        String sFileName = sDate + "-Covid-Report.csv";
        try {
            ensureOutputDir();
            FileWriter writer = new FileWriter("output/" + sFileName);
            for (i=0; i < aCsv.size(); i++) {
                csv = aCsv.get(i);
                String sRecord = "";
                for(int iRec=1; iRec < csv.size(); iRec++) {
                    if(iRec > 1) {
                        sRecord += ",";
                    }
                    sRecord += csv.get(iRec);
                }
                if((i-1) == 0) {
                    System.out.println("");
                    System.out.print("Writing list: "); 
                } else if( (i-1) % 20 == 0) {
                    System.out.print(" i:" + (i-1) + " " + csv.get(iRecordNumber)); 
                } 
                if( (i-1) % 200 == 0) {
                    System.out.println(" ");
                }
                writer.write(sRecord + "\n");
                // skip header row (i=1 not 0)
                if(i > 0) {
                    markReportedEvent(csv.get(0));
                }
            }
            writer.flush();
            writer.close();
            
        } catch (Exception ex) {
            System.err.println("Error writing output file: " + sFileName);
            System.err.println("Error writing record number: " + i);
            System.err.println("Exception: " + ex.toString());
            closeConnection();
            logger.log(Level.SEVERE, "Exiting 8 " + ex.toString());
            System.exit(8);
        }
    }

    private static void markReportedEvent(String sPersonConditionId) {
        if(fExportSingleEvent) {
            return;
        }
        String sGlobalId = sAuditClass + ":" + sPersonConditionId;
        String sSql = "";
        try {
            sSql = "INSERT INTO audit (user_id, globalid, changes, timestamp) "
                    + " VALUES (" + idEmsaUser + ", '"
                    + sGlobalId + "', '" + sRecordingDetail + "', now());";
            executeSql(sSql, true /*fUpdate*/);
        } catch (Exception ex) {
            System.err.println("Error marking event as reported: " + sPersonConditionId);
            System.err.println("Exception: " + ex.toString());
            System.err.println("SQL: " + strNonNull(sSql));
            closeConnection();
            logger.log(Level.SEVERE, "Exiting 11 ");
            System.exit(11);
        }
    }
    
    private static boolean isPositiveResult(ArrayList<String> csv, int iCol) {
        String sResult = csv.get(iCol).toLowerCase().trim();
        return (sResult.equals("\"detected\"") || sResult.equals("\"reactive\"") 
                || sResult.contains("positive"));
    }

    /**
     * xml case files are written to a sub-directory to the current dir named
     * output. Create the output sub-directory if it does not exist.
     */
    private static void ensureOutputDir() {

        File file = new File("output");
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private static ResultSet executeSql(String sSql, boolean fUpdate) {
        ResultSet rs = null;
        Statement statement;
        try {
            statement = con.createStatement();
            if(fUpdate) {
                statement.executeUpdate(sSql);
            } else {
                rs = statement.executeQuery(sSql);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 9 error executing SQL: " + sSql);
            logger.log(Level.SEVERE, "SQL error: " + ex.toString());
            System.err.println("Error executing SQL: " + sSql);
            System.err.println("SQL Exception: " + ex.toString());
            closeConnection();
            System.exit(9);
        }
        return rs;
    }

    private static String strNonNull(String s) {
        if (s == null) {
            return "";
        }
        return s.trim();
    }

    private static int intNonNull(Integer n) {
        if (n == null) {
            return 0;
        }
        return n;
    }
    
    private static void closeConnection() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
               System.err.println("SQL Exception: " + ex.toString());
            }
        }
    }
    private static void setEmsaUserId() {
        ResultSet rs = executeSql("SELECT id FROM public.user WHERE uid='9999'", false /*fUpdate*/);
        
        idEmsaUser = -1;
        try {
            if (rs.next()) {
                idEmsaUser = rs.getInt(1);
            }
        } catch (Exception ex) {
        }
        if(idEmsaUser == -1){
            logger.log(Level.SEVERE, "Exiting 11. Could not get EMSA user id");
            System.exit(11);
        }
    }
}
