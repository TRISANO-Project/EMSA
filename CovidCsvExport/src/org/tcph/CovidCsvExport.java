/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tcph;

import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Logger;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Jay Boyer
 */
public class CovidCsvExport {

    private static boolean fExportSingleEvent = false;
    private static Integer nRecordNumber = 0, nRecordNumberMax = 0;
    private static Properties properties = new Properties();
    private static Logger logger = Logger.getLogger("");
    private static Connection con;

    // indices to CSV columns
    private static int iEventId =0;
    private static int iRecordNumber =1;
    private static int iDob =2;
    private static int iLName =3;
    private static int iFName =4;
    private static int iRecieved =5;
    private static int iSpecimen =6;
    private static int iReportingAgency =7;
    private static int iTestType =8;
    private static int iPCRResult =9;
    private static int iPCRCollectionDate =10;
    private static int iSerologyCollectionDate =11;
    private static int iIggResult =12;
    private static int iIgmResult =13;
    private static int iAntigenCollectionDate =14;
    private static int iAntigenResult =15;
    private static int iOtherCollectionDate =16;
    private static int iOtherResult =17;
    private static int iPhone =18;
    private static int iAddress1 =19;
    private static int iAddress2 =20;
    private static int iCity =21;
    private static int iState =22;
    private static int iZip =23;
    private static int iGender =24;
    private static int iRace =25;
    private static int iEthnicity =26;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] asArg) {

        initLogger();
        int nError = 1;

        // check arguments to see if we are exporting a single events
        if (asArg.length == 1) {
            // parse out the record number
            try {
                nRecordNumber = Integer.parseInt(asArg[0]);
            } catch (Exception ex) {
                nError = 2;
            }
            fExportSingleEvent = true;
        } else if (asArg.length > 1) {
            logger.log(Level.SEVERE, "Exiting " + nError + " problem with argument list");
            System.err.println("Usage: CovidCsvExport <event_id>\n");
            System.exit(nError);
        }
        
        // read Sql url and credentials from the property file
        loadProperties();

        // Ensure we can connect to the database
        con = getConnection();

        // fetch the events of interest
        ArrayList<ArrayList<String>> aCsv = fetchEvents(con);

        // output the xml for the events
        outputCsv(aCsv);
    }

    /**
     *
     */
    private static void initLogger() {
        FileHandler handler;
        try {
            logger.setUseParentHandlers(false);
            handler = new FileHandler("CovidCsvExport.log");
            SimpleFormatter formatter = new SimpleFormatter();
            handler.setFormatter(new SimpleFormatter());
            handler.setLevel(Level.ALL);
            logger.addHandler(handler);
            logger.log(Level.INFO, "CovidCsvExport beginning logging");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error 3 setting up logger", ex);
            System.err.println("Error setting up logger");
            System.exit(3);
        }
    }


    /**
     *
     */
    private static void loadProperties() {
        File file = new File("covid_csv_export.properties");
 
        try {
            FileInputStream in = new FileInputStream(file.getAbsolutePath());
            properties.load(in);
            in.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 2 could not read properties file" + ex.toString());
            System.err.println("Could not read properties file: " + file.getAbsolutePath());
            System.err.println("Properties Exception: " + ex.toString());
            System.exit(4);
        }
    }

    /**
     * @author Jay Boyer 2013
     */
    private static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(
                    properties.getProperty("db_url"),
                    properties.getProperty("db_user"),
                    properties.getProperty("db_password"));
            if (con == null) {
                logger.log(Level.SEVERE, "Exiting 3 database connection returned null");
                System.err.println("Problem connecting to database.");
                System.exit(5);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 4 could not connect to database " + ex.toString());
            System.err.println("Could not connect to database. Exception: " + ex.toString());
            closeConnection();
            System.exit(6);
        }
        return con;
    }

    private static ArrayList<ArrayList<String>> fetchEvents(Connection con) {

        ResultSet rs = null;
        ArrayList<String> asHeader = new ArrayList<String>( 
            Arrays.asList("event_id", "\"TriSano.ID\"","\"Date.of.Birth\"","\"Last.Name\"","\"First.Name\""
            ,"\"Date.Received.by.TCPH\"","\"Specimen.ID\"","\"Collection.Site\"","\"Test.Type\"","\"PCR.Test.Result\""
            ,"\"PCR.Specimen.Collection.Date\"","\"Serology.Collection.Date\"","\"IgGResult\"","\"IgMResult\""
            ,"\"Antigen.Collection.Date\"","\"AntigenResult\"","\"Other.Collection.Date\"","\"OtherResult\""
            ,"\"Phone\"","\"Address.of.Residence_Street1\""
            ,"\"Address.of.Residence_Street2\"","\"Address.of.Residence_City\"","\"Address.of.Residence_State\""
            ,"\"Address.of.Residence_Zip\"","\"Gender\"","\"Race\"","\"Ethnicity\""
            )); 
        ArrayList<ArrayList<String>> aCsv = new ArrayList<ArrayList<String>>();
        aCsv.add(asHeader);

        // SQL for finding Covid events not yet reported
        // OR a single specific Covid event (reported or not)
        String sSql = 
            "SELECT DISTINCT ON (e.id) e.id, e.record_number, TO_CHAR(pe.birth_date, 'YYYY-MM-DD') AS dob, pe.last_name, pe.first_name "
               + ", TO_CHAR(e.created_at, 'YYYY-MM-DD') AS date_received  "
               + ", pl.name  "
               + ", '(' || t.area_code || ') ' || t.phone_number AS telephone "
               + ", TRIM(CONCAT(a.street_number, ' ', a.street_name)) AS address_1 "
               + ", a.city, ec1.the_code, a.postal_code "
               + ", ec2.the_code AS gender "
               + ", ec3.the_code AS race "
               + ", ec4.the_code AS ethnicity "
               + "FROM events e "
               + "INNER JOIN disease_events de ON de.event_id = e.id "
               + "INNER JOIN diseases d ON de.disease_id = d.id AND d.disease_name='2019-nCoV' "
               + "INNER JOIN participations p ON p.event_id = e.id AND p.type = 'InterestedParty' "
               + "INNER JOIN people pe ON pe.entity_id = p.primary_entity_id "
               + "LEFT JOIN telephones t ON t.entity_id = pe.entity_id "
               + "LEFT JOIN (SELECT * FROM addresses ORDER BY updated_at DESC) a ON a.entity_id = pe.entity_id "
               + "LEFT JOIN external_codes ec1 ON ec1.id = a.state_id AND ec1.code_name='state' "
               + "LEFT JOIN external_codes ec2 ON ec2.id = pe.birth_gender_id AND ec2.code_name='gender' "
               + "LEFT JOIN external_codes ec3 ON ec3.id = pe.race_id AND ec3.code_name='race' "
               + "LEFT JOIN external_codes ec4 ON ec4.id = pe.ethnicity_id AND ec4.code_name='ethnicity' "
               + "LEFT JOIN participations p1 ON p1.event_id = e.id AND p1.type = 'ReportingAgency' "
               + "LEFT JOIN places pl ON pl.entity_id = p1.secondary_entity_id "
           + "WHERE e.deleted_at IS NULL AND (e.type = 'MorbidityEvent' OR e.type = 'AssessmentEvent')  ";
        if (fExportSingleEvent) {
            sSql += "AND e.record_number='" + nRecordNumber + "' ";
        } else {
            // sSql += "AND e.id NOT IN (SELECT event_id FROM reported_covid_cases) ";
            sSql += "AND e.id >= (SELECT event_id FROM reported_covid_cases ORDER BY event_id DESC LIMIT 1)"; 
        }
        sSql += "ORDER by e.id; ";
        rs = executeSql(sSql, false /*fUpdate*/);
        int i = 0;
        try {
            while (rs.next()) {
                ArrayList<String> csv = new ArrayList<String>( 
                    Arrays.asList("","","","","","","","","","","","","","","","","","","","","","","","","","", ""));
                csv.set(iEventId, strNonNull(rs.getString(1)));
                csv.set(iRecordNumber, strNonNull(rs.getString(2)));
                csv.set(iDob, "\"" + strNonNull(rs.getString(3)) + "\"");
                csv.set(iLName, "\"" + strNonNull(rs.getString(4)) + "\"");
                csv.set(iFName, "\"" + strNonNull(rs.getString(5)) + "\"");
                csv.set(iRecieved, "\"" + strNonNull(rs.getString(6)) + "\"");
                csv.set(iReportingAgency, "\"" + strNonNull(rs.getString(7)) + "\"");
                csv.set(iPhone, "\"" + strNonNull(rs.getString(8)) + "\"");
                csv.set(iAddress1, "\"" + strNonNull(rs.getString(9)) + "\"");
                csv.set(iCity, "\"" + strNonNull(rs.getString(10)) + "\"");
                csv.set(iState, "\"" + strNonNull(rs.getString(11)) + "\"");
                csv.set(iZip, "\"" + strNonNull(rs.getString(12)) + "\"");
                csv.set(iGender, "\"" + strNonNull(rs.getString(13)) + "\"");
                switch (csv.get(iGender)) {
                    case "\"M\"":
                        csv.set(iGender, "\"Male\"");
                        break;
                    case "\"F\"":
                        csv.set(iGender, "\"Female\"");
                        break;
                    default:
                        csv.set(iGender, "\"Unknown\"");
                }
                csv.set(iRace, "\"" + strNonNull(rs.getString(14)) + "\"");
                switch (csv.get(iRace)) {
                    case "\"W\"":
                        csv.set(iRace, "\"White\"");
                        break;
                    case "\"B\"":
                        csv.set(iRace, "\"Black\"");
                        break;
                    case "\"A\"":
                        csv.set(iRace, "\"Asian\"");
                        break;
                    case "\"H\"":
                        csv.set(iRace, "\"Pacific Islander\"");
                        break;
                    case "\"AI_AN\"":
                        csv.set(iRace, "\"Native American\"");
                        break;
                    case "\"OTHER\"":
                        csv.set(iRace, "Other");
                        break;
                    default:
                        csv.set(iRace, "\"Unknown / Refused to Answer\"");
                }
                csv.set(iEthnicity, "\"" + strNonNull(rs.getString(15)) + "\"");
                switch (csv.get(iEthnicity)) {
                    case "\"H\"":
                        csv.set(iEthnicity, "\"Hispanic or Latino\"");
                        break;
                    case "\"NH\"":
                        csv.set(iEthnicity, "\"Non Hispanic or Latino\"");
                        break;
                    default:
                        csv.set(iEthnicity, "\"Unknown / Refused to Answer\"");
                }
                
                csv = addLabResultsToRecord(csv);
                aCsv.add(csv);
                if(i == 0) {
                    System.out.print("Building list: "); 
                } else if( i % 20 == 0) {
                    System.out.print(" i:" + i + " " + csv.get(iRecordNumber)); 
                } 
                if( i % 200 == 0) {
                    System.out.println(" ");
                }
                i++;
                // TODO Jay
                //if(i >= 100)
                //    break;
            }
            rs.close();
        } catch (Exception ex) {
            System.err.println("Error getting next result: " + i);
            System.err.println("rs Exception: " + ex.toString());
            logger.log(Level.SEVERE, "Exiting 7 " + ex.toString());
            closeConnection();
            System.exit(7);
        }
        return aCsv;
    }
    
    private static ArrayList<String> addLabResultsToRecord(ArrayList<String> csv) {
        
        try {
            String sSql = "SELECT TO_CHAR(collection_date, 'YYYY-MM-DD') as col_date, ec.code_description AS specimen, "
                + " ctt.common_name AS test_type, lr.test_result_id FROM lab_results lr "
                    + " INNER JOIN participations p ON p.id = lr.participation_id AND p.event_id=" + csv.get(iEventId) 
                    + " LEFT JOIN external_codes ec ON ec.code_name = 'specimen' AND ec.id = lr.specimen_source_id "
                    + " LEFT JOIN common_test_types ctt ON ctt.id = lr.test_type_id "
                    + " ORDER BY lr.id DESC; ";

            ResultSet rs = rs = executeSql(sSql, false /*fUpdate*/);
            while (rs.next()) {
                String sCollectionDate = strNonNull(rs.getString(1));
                String sSpecimen = strNonNull(rs.getString(2));
                String sTestType = strNonNull(rs.getString(3));
                int nResult = intNonNull(rs.getInt(4));
                String sResult = "Not Detected";
                if(nResult == 204 || nResult == 206) {
                    sResult = "Detected";
                }
                csv.set(iSpecimen, "\"" + sSpecimen + "\"");
                if (sTestType.toLowerCase().contains("pcr") 
                        || sTestType.toLowerCase().contains("rna")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!csv.get(iPCRResult).equals("\"Detected\"")) {
                        csv.set(iPCRResult, "\"" + sResult + "\"");
                    }
                    csv.set(iPCRCollectionDate, "\"" + sCollectionDate + "\"");
                    csv.set(iTestType, "\"PCR\"");
                } else if (sTestType.toLowerCase().contains("igg ")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!csv.get(iIggResult).equals("\"Detected\"")) {
                        csv.set(iIggResult, "\"" + sResult + "\"");
                    }
                    csv.set(iSerologyCollectionDate, "\"" + sCollectionDate + "\"");
                    csv.set(iTestType, "\"IgG\"");
                } else if (sTestType.toLowerCase().contains("igm ")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!csv.get(iIgmResult).equals("\"Detected\"")) {
                        csv.set(iIgmResult, "\"" + sResult + "\"");
                    }
                    csv.set(iSerologyCollectionDate, "\"" + sCollectionDate + "\"");
                    csv.set(iTestType, "\"IgM\"");
                } else if (sTestType.toLowerCase().contains("antigen")) {
                    // Don't overwrite earlier positive lab with negative result
                    if(!csv.get(iAntigenResult).equals("\"Detected\"")) {
                        csv.set(iAntigenResult, "\"" + sResult + "\"");
                    }
                    csv.set(iAntigenCollectionDate, "\"" + sCollectionDate + "\"");
                    csv.set(iTestType, "\"Antigen\"");
                } else {
                    // Don't overwrite earlier positive lab with negative result
                    if(!csv.get(iOtherResult).equals("\"Detected\"")) {
                        csv.set(iOtherResult, "\"" + sResult + "\"");
                    }
                    csv.set(iOtherCollectionDate, "\"" + sCollectionDate + "\"");
                    if (sTestType.toLowerCase().contains("rapid")) {
                        csv.set(iTestType, "\"Other - Rapid (Nasopharyngeal)\"");
                    } else if (sTestType.toLowerCase().contains("total antibody")) {
                        csv.set(iTestType, "\"Other - Total Antibody\"");
                    } else if (sTestType.toLowerCase().contains("iga ")) {
                        csv.set(iTestType, "\"Other - IgA\"");
                    } else {
                        csv.set(iTestType, "\"Other - Unknown\"");
                    }
                }
            }
            rs.close();
        } catch (Exception ex) {
            System.err.println("Error getting labs for event: " + csv.get(iEventId));
            System.err.println("rs Exception: " + ex.toString());
            closeConnection();
            logger.log(Level.SEVERE, "Exiting 10 " + ex.toString());
            System.exit(10);
        }
        return csv;
    }

    private static void outputCsv(ArrayList<ArrayList<String>> aCsv) {
        int i = 0;
        ArrayList<String> csv;
        String sDate = new SimpleDateFormat("yyyy-MM-dd_HHmmss").format(new Date());
        String sFileName = sDate + "-Covid-Report.csv";
        try {
            ensureOutputDir();
            FileWriter writer = new FileWriter("output/" + sFileName);
            for (i=0; i < aCsv.size(); i++) {
                csv = aCsv.get(i);
                String sRecord = "";
                for(int iRec=1; iRec < csv.size(); iRec++) {
                    if(iRec > 1) {
                        sRecord += ",";
                    }
                    sRecord += csv.get(iRec);
                }
                if((i-1) == 0) {
                    System.out.println("");
                    System.out.print("Writing list: "); 
                } else if( (i-1) % 20 == 0) {
                    System.out.print(" i:" + (i-1) + " " + csv.get(iRecordNumber)); 
                } 
                if( (i-1) % 200 == 0) {
                    System.out.println(" ");
                }
                writer.write(sRecord + "\n");
                // skip header row (i=1 not 0)
                if(i > 0) {
                    markReportedEvent(csv.get(0));
                }
            }
            writer.flush();
            writer.close();
            
        } catch (Exception ex) {
            System.err.println("Error writing output file: " + sFileName);
            System.err.println("Error writing record number: " + i);
            System.err.println("Exception: " + ex.toString());
            closeConnection();
            logger.log(Level.SEVERE, "Exiting 8 " + ex.toString());
            System.exit(8);
        }
    }

    private static void markReportedEvent(String sEventId) {
        if(fExportSingleEvent) {
            return;
        }
        String sSql = "";
        try {
            sSql = "INSERT INTO reported_covid_cases (event_id, reported_date) VALUES (" + sEventId + ", now());";
            executeSql(sSql, true /*fUpdate*/);
        } catch (Exception ex) {
            System.err.println("Error marking event as reported: " + sEventId);
            System.err.println("Exception: " + ex.toString());
            System.err.println("SQL: " + strNonNull(sSql));
            closeConnection();
            logger.log(Level.SEVERE, "Exiting 11 ");
            System.exit(11);
        }
    }

    /**
     * xml case files are written to a sub-directory to the current dir named
     * output. Create the output sub-directory if it does not exist.
     */
    private static void ensureOutputDir() {

        File file = new File("output");
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private static ResultSet executeSql(String sSql, boolean fUpdate) {
        ResultSet rs = null;
        Statement statement;
        try {
            statement = con.createStatement();
            if(fUpdate) {
                statement.executeUpdate(sSql);
            } else {
                rs = statement.executeQuery(sSql);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exiting 9 error executing SQL: " + sSql);
            logger.log(Level.SEVERE, "SQL error: " + ex.toString());
            System.err.println("Error executing SQL: " + sSql);
            System.err.println("SQL Exception: " + ex.toString());
            closeConnection();
            System.exit(9);
        }
        return rs;
    }

    private static String strNonNull(String s) {
        if (s == null) {
            return "";
        }
        return s.trim();
    }

    private static int intNonNull(Integer n) {
        if (n == null) {
            return 0;
        }
        return n;
    }
    
    private static void closeConnection() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
               System.err.println("SQL Exception: " + ex.toString());
            }
        }
    }
}
