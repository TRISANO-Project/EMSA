package gov.utah.health.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides access to the name/value pairs in the
 * application.properties file in the classpath.
 *
 * @author UDOH
 */
public class AppProperties extends Properties {

    private static final AppProperties instance = new AppProperties();
    private static Logger logger;

    private AppProperties() {
        logger = Logger.getLogger("AppProperties");

        String strPropsFile = "/var/lib/elr_application.properties";
        File file = new File(strPropsFile);

        try {
            FileInputStream in = new FileInputStream(file.getAbsolutePath());
            this.load(in);
            in.close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Could not read elr_application.properties file: " + file.getAbsolutePath(), ex);
        }
    }

    public static AppProperties getInstance() {
        return instance;
    }

    /* Return a Boolean value of either false or true, any string value staring with "T" or "t"
     * is treated as 'true', anything else is 'false' */
    public Boolean getBooleanProperty(String key) {
        String value = getProperty(key);
        if (value == null) {
            return false;
        }
        value = value.trim();
        if (value.length() < 1 || !value.substring(0, 1).toLowerCase().equals("t")) {
            return false;
        }
        return true;
    }

    /* Return an integer value for the requested property.  If we can parse the 
     * Integer use that value otherwise use a value of -1
     */
    public Integer getIntProperty(String key) {
        String value = getProperty(key);
        if (value != null) {
            value = value.trim();
            if (value.length() > 0) {
                try {
                    Integer n = Integer.parseInt(value);
                    return n;
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Exception trying to parse int for property: " + key, ex);
                }
            }
        }
        return -1;
    }
}